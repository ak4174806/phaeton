﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using ZedGraph;

namespace WFM
{
    public partial class PlotForm : Form
    {
        public PlotForm()
        {
            InitializeComponent();

            // initialise
            pane = zgcPlot.GraphPane;
            colorList = new Color[] { Color.Black, Color.Red, Color.Blue, Color.Purple, Color.Green, Color.HotPink, Color.Indigo, Color.Fuchsia };

            PlotTitle = "Plot";
            XTitle = "x";
            YTitle = "y";
        }

        private GraphPane pane;
        private Color[] colorList;
        
        private void refreshDisplay()
        {
            zgcPlot.AxisChange();
            zgcPlot.Invalidate();
        }
        
        public string PlotTitle
        {
            get { return pane.Title.Text; }
            set
            {
                pane.Title.Text = value;
                this.Text = value;
            }
        }

        public string XTitle
        {
            get { return pane.XAxis.Title.Text; }
            set
            {
                pane.XAxis.Title.Text = value;
            }
        }

        public string YTitle
        {
            get { return pane.YAxis.Title.Text; }
            set
            {
                pane.YAxis.Title.Text = value;
            }
        }

        public ZedGraphControl Graph
        {
            get { return zgcPlot; }
        }


        public int CountPlots()
        {
            return pane.CurveList.Count;
        }

        public void ClearPlots()
        {
            pane.CurveList.Clear();
        }

        // called when data is updated externally
        public void ForceRefreshDisplay()
        {
            refreshDisplay();
        }

        #region AddPlot
        public void AddPlot(double[] x, double[] y, string title, Color color, SymbolType symbol)
        {
            PointPairList list = new PointPairList(x, y);
            LineItem curve = pane.AddCurve(title, list, color, symbol);

            refreshDisplay();
        }

        public void AddPlot(double[] x, double[] y, string title, Color color)
        {
            AddPlot(x, y, title, color, SymbolType.None);
        }
        
        public void AddPlot(double[] x, double[] y, string title)
        {
            AddPlot(x, y, title, colorList[CountPlots() % (colorList.Length)], SymbolType.None);
        }

        public void AddPlot(double[] x, double[] y)
        {
            int count = CountPlots();

            AddPlot(x, y, "Plot " + count, colorList[count % (colorList.Length)], SymbolType.None);
        }

        public void AddPlot(double[] x, double[] y, string title, SymbolType symbol)
        {
            AddPlot(x, y, title, colorList[CountPlots() % (colorList.Length)], symbol);
        }
        #endregion

        #region SetPlot
        public void SetPlot(double[] x, double[] y, string title, Color color, SymbolType symbol)
        {
            ClearPlots();
            AddPlot(x, y, title, color, symbol);
        }

        public void SetPlot(double[] x, double[] y, string title, Color color)
        {
            ClearPlots();
            AddPlot(x, y, title, color);
        }

        public void SetPlot(double[] x, double[] y, string title)
        {
            ClearPlots();
            AddPlot(x, y, title);
        }

        public void SetPlot(double[] x, double[] y)
        {
            ClearPlots();
            AddPlot(x, y);
        }

        public void SetPlot(double[] x, double[] y, string title, SymbolType symbol)
        {
            ClearPlots();
            AddPlot(x, y, title, symbol);
        }
        #endregion
    }
}
