﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using WFM.SettingsControls;
using System.IO;

namespace WFM.Settings
{
    public partial class DataSavingSettingsForm : BaseSettingsForm
    {
        public DataSavingSettingsForm()
        {
            InitializeComponent();
            formID = "DataSavingSettings";
        }

        protected override void addControls()
        {
            settingsControls.Add("filename", new TextboxSettingsControl(txtFilename));
            settingsControls.Add("folder", new TextboxSettingsControl(txtFolder));
        }

        protected override void initialiseForm()
        {
            

            txtFilename.Text = "scan {nnn}.csv";
            txtFolder.Text = @"c:\scans";            
            
            btnBrowseFolder.Click += new EventHandler(delegate(object sender, EventArgs e)
            {
                if (Directory.Exists(txtFolder.Text))
                {
                    folderBrowserDialog1.SelectedPath = txtFolder.Text;
                }

                folderBrowserDialog1.ShowDialog();

                if (folderBrowserDialog1.SelectedPath != "")
                {
                    txtFolder.Text = folderBrowserDialog1.SelectedPath;
                }
            });
        }
    }
}
