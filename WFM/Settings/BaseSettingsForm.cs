﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using WFM.SettingsControls;

namespace WFM.Settings
{
    public partial class BaseSettingsForm : Form
    {
        public delegate void SettingsFormHandler();
        public event SettingsFormHandler SettingsFormClosing;
        public event SettingsFormHandler SettingsFormShown;
        
        // a unique id of the form used when saving the settings to file
        protected string formID = "base";

        // a list of settings in the form
        protected Dictionary<string, SettingsControl> settingsControls;

        public BaseSettingsForm()
        {
            InitializeComponent();

            // set up settings list
            settingsControls = new Dictionary<string, SettingsControl>();
                        
            // events
            btnCancel.Click += new EventHandler(delegate(object sender, EventArgs e)
            {
                this.Close();
            });

            btnOK.Click += new EventHandler(delegate(object sender, EventArgs e)
            {
                this.Close();
            });

            btnSave.Click += new EventHandler(delegate(object sender, EventArgs e)
            {
                if (ValidateSettings())
                {
                    SaveSettingsFromForm();
                }
            });

            this.FormClosing += new FormClosingEventHandler(delegate(object sender, FormClosingEventArgs e)
            {
                if (e.CloseReason == CloseReason.UserClosing || e.CloseReason == CloseReason.None)
                {
                    // prevent closing, just hide it
                    e.Cancel = true;

                    if (this.DialogResult == DialogResult.Cancel)
                    {
                        // todo: check if saved and actual are identical?
                        if (!Modified || confirm("You will lose any unsaved changes. Continue?", "Are you sure you want to cancel"))
                        {
                            // reset form
                            CancelSettings();

                            if (SettingsFormClosing != null)
                                SettingsFormClosing();

                            this.Hide();
                        }
                    }
                    else if (this.DialogResult == DialogResult.OK)
                    {
                        if (ValidateSettings())
                        {
                            SaveSettingsFromForm();

                            if (SettingsFormClosing != null)
                                SettingsFormClosing();

                            this.Hide();
                        }                        
                    }
                    else
                    {
                        if (SettingsFormClosing != null)
                            SettingsFormClosing();

                        this.Hide();
                    }
                }
            });
                        
            // occurs on first run only
            this.Shown += new EventHandler(delegate(object sender, EventArgs e)
            {
                // sets up controls (i.e. drop down lists), sets default values (before saved values loaded)
                initialiseForm();

                // adds controls to be watched and tracked
                addControls();

                // load saved values from file
                loadSavedValuesFromFile();
                
                // adds modified handlers
                // do it last to prevent it getting called when default values set
                settingsInit();
            });

            // instead of Shown, occurs every time
            this.VisibleChanged += new EventHandler(delegate(object sender, EventArgs e)
            {
                if (this.Visible)
                {
                    if (SettingsFormShown != null)
                        SettingsFormShown();

                    Modified = false;
                }
            });
        }


        // needs to be called after settings controls are defined
        protected void settingsInit()
        {
            // add modified events for each setting control
            foreach (string key in settingsControls.Keys)
            {
                settingsControls[key].ValueChanged += new SettingsControl.ValueChangedDelegate(settingModifiedHandler);
            }
        }

        // trys to loads saved values from file
        protected void loadSavedValuesFromFile()
        {
            if ((formID == null || formID == "" || formID == "base") && !this.GetType().ToString().Contains("BaseSettingsForm"))
            {
                throw new Exception("Did not specify form ID in form: " + this.GetType().Name);
            }

            SavedSettings settings = Properties.Settings.Default.savedSettings;
            if (settings == null)
            {
                settings = new SavedSettings();
                Properties.Settings.Default.savedSettings = settings;
            }

            if (settings.HasSavedFormId(formID))
            {
                Dictionary<string, string> formSettings = settings.GetSettings(formID);

                foreach (string key in settingsControls.Keys)
                {
                    if (formSettings.ContainsKey(key))
                    {
                        // try to set the saved value
                        // ignore catch in case value is invalid
                        try
                        {
                            settingsControls[key].SetSavedValue(formSettings[key]);
                            settingsControls[key].SetValue(formSettings[key]);
                        }
                        catch { }
                    }
                }
            }
        }

        // saves from savedvalue of each control
        protected void saveValuesToFile()
        {
            if (formID == null || formID == "")
            {
                throw new Exception("Did not specify form ID in form: " + this.GetType().Name);
            }

            SavedSettings settings = Properties.Settings.Default.savedSettings;
            if (settings == null)
            {
                settings = new SavedSettings();
                Properties.Settings.Default.savedSettings = settings;
            }

            if (!settings.HasSavedFormId(formID))
            {
                settings.AddSavedFormId(formID);
            }

            foreach (string key in settingsControls.Keys)
            {
                settings.SetSetting(formID, key, settingsControls[key].GetSavedValue());
            }

            // save to file
            Properties.Settings.Default.Save();
        }




        public string GetSetting(string label, bool useSaved)
        {
            if (!settingsControls.ContainsKey(label))
            {
                throw new ArgumentException("label", "Could not find a setting labelled '" + label + '"');
            }

            if (useSaved && settingsControls[label].GetSavedValue() != null)
            {
                return settingsControls[label].GetSavedValue();
            }
            else
            {
                return settingsControls[label].GetValue();
            }
        }

        public string GetSetting(string label)
        {
            return GetSetting(label, true);
        }

        public void SetSetting(string label, string value, bool toSaved)
        {
            if (!settingsControls.ContainsKey(label))
            {
                throw new ArgumentException("label", "Could not find a setting labelled '" + label + '"');
            }

            if (toSaved)
            {
                settingsControls[label].SetSavedValue(value);
            }
            else
            {
                settingsControls[label].SetValue(value);
            }
        }

        public void SetSetting(string label, string value)
        {
            SetSetting(label, value, true);
        }


        public void SaveSettingsFromForm()
        {
            foreach (string key in settingsControls.Keys)
            {
                settingsControls[key].Save();
            }

            Modified = false;

            saveValuesToFile();
        }

        public void CancelSettings()
        {
            foreach (string key in settingsControls.Keys)
            {
                settingsControls[key].Cancel();
            }

            Modified = false;
        }

        public bool ValidateSettings()
        {
            bool result = true;

            foreach (string key in settingsControls.Keys)
            {
                ValidationResult res = settingsControls[key].ValidateValue();
                if (!res.IsValid)
                {
                    result = false;
                    if (res.Message.Trim() != "")
                    {
                        // todo: handle
                        displayControlError(key, res.Message);
                    }
                }
            }
            return result;
        }
        

        // todo: add saving / loading from file

        
        protected bool modified = false;
        public bool Modified
        {
            get { return modified; }
            set
            {
                modified = value;
                if (modified)
                {
                    btnSave.Enabled = true;
                    btnOK.Enabled = true;
                    btnCancel.Text = "Cancel";
                }
                else
                {
                    btnSave.Enabled = false;
                    btnOK.Enabled = false;
                    btnCancel.Text = "Close";
                }
            }
        }
        protected void settingModifiedHandler()
        {
            Modified = true;            
        }



        protected virtual bool confirm(string message, string caption)
        {
            return MessageBox.Show(message, caption, MessageBoxButtons.YesNo) == DialogResult.Yes;
        }

        protected virtual void displayControlError(string label, string message)
        {
            MessageBox.Show("Error: " + message + " for control '" + label + "'");
        }

        protected virtual void displayError(string message)
        {
            MessageBox.Show("Error: " + message);
        }



        // runs on first show only
        protected virtual void initialiseForm()
        { }
        
        protected virtual void addControls()
        { }
    }
}
