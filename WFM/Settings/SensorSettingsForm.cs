﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using System.IO.Ports;

using SensorControl;
using SensorControl.Sensors;
using WFM.SettingsControls;

namespace WFM.Settings
{
    public partial class SensorSettingsForm : BaseSettingsForm
    {
        public SensorSettingsForm()
        {
            InitializeComponent();
        }

        protected override void addControls() 
        {
            settingsControls.Add("sensor model", new DropDownListSettingsControl(cmbSensorModel));
            settingsControls.Add("sensor type", new DropDownListSettingsControl(cmbSensorType));
            settingsControls.Add("COM port", new DropDownListSettingsControl(cmbSensorPort));
            settingsControls.Add("baud rate", new DropDownListSettingsControl(cmbSensorBaudRate));
        }
        
        protected override void initialiseForm()
        {
            formID = "SensorSettings";

            // set up sensors
            sensors = new ISensor[3];
            sensors[0] = new ILD2200_MESensor();
            sensors[1] = new ILD1700_MESensor();
            sensors[2] = new IFC2401_Sensor();
            // todo: MEDA IFC

            // populate combo boxes - with appropriate default values
            updateSensorModels();
            updateSensorTypes();
            updateCOMPorts();
            updateBaudRates();

            sensorState = SensorStates.Disconnected;
                        
            // cascade of sensor properties
            cmbSensorModel.SelectedIndexChanged += new EventHandler(delegate(object sender, EventArgs e)
            {
                updateSensorTypes();
                updateBaudRates();
            });

            btnRefreshPort.Click += new EventHandler(delegate(object sender, EventArgs e)
            {
                updateCOMPorts();
            });

            // connection
            btnConnect.Click += new EventHandler(btnConnect_Click);
        }



        #region Connection State
        public enum SensorStates
        {
            Connected,
            Disconnected
        }
        protected SensorStates sensorState;
        public SensorStates SensorState
        {
            get { return sensorState; }
            set
            {
                bool state = (value == SensorStates.Disconnected);
                cmbSensorModel.Enabled = state;
                cmbSensorType.Enabled = state;
                cmbSensorPort.Enabled = state;
                cmbSensorBaudRate.Enabled = state;
                btnRefreshPort.Enabled = state;

                btnConnect.Text = (state ? "Connect" : "Disconnect");
            }
        }
        #endregion

        #region Sensor Access

        private ISensor[] sensors;
        
        /// <summary>
        /// Returns the currently selected sensor, using the form as a guide
        /// </summary>
        public ISensor Sensor
        {
            get
            {
                ISensor sensor = sensors[cmbSensorModel.SelectedIndex];

                if (cmbSensorType.Items.Count > 0)
                {
                    bool contains = false;
                    foreach (string s in sensor.SensorTypes)
                    {
                        if (s == cmbSensorType.SelectedItem.ToString())
                        {
                            contains = true;
                            break;
                        }
                    }

                    if (contains)
                    {
                        sensor.SetSensorType(cmbSensorType.SelectedItem.ToString());
                    }
                }
                return sensor;
            }
        }
        #endregion

        #region Connection

        void btnConnect_Click(object sender, EventArgs e)
        {
            btnConnect.Enabled = false;

            if (SensorState == SensorStates.Disconnected)
            {
                bool connected = ConnectSensor();
                if (connected)
                {
                    // change form state
                    SensorState = SensorStates.Connected;
                }
            }
            else
            {
                bool disconnected = DisconnectSensor();
                if (disconnected)
                {
                    // change form state
                    SensorState = SensorStates.Disconnected;
                }
            }

            btnConnect.Enabled = true;
        }

        // connect using form, also update settings and validate
        public bool ConnectSensor()
        {
            try
            {
                // connect to port
                string portName = GetSetting("COM port", false);

                if (GetSetting("baud rate", false) == baudRateAutoDetectText)
                {
                    // auto-detect baud rate
                    Sensor.Connect(portName);

                    // update selected baud rate
                    SetSetting("baud rate", Sensor.BaudRate.ToString(), false);                    
                }
                else
                {
                    // use selected baud rate
                    int baudRate = Convert.ToInt32(GetSetting("baud rate", false));
                    Sensor.Connect(portName, baudRate);
                }

                // set reading reference to start of measuring range, rather than sensore
                Sensor.ReadingReference = ReadingReferences.SMR;

                // update form
                SaveSettingsFromForm();
            }
            catch (Exception ex)
            {
                displayError("There was an error connecting to the sensor. The sensor responded '" + ex.Message + "'");

                // disconnect if connected
                Sensor.Disconnect();

                return false;
            }

            return true;
        }

        public bool DisconnectSensor()
        {
            try
            {
                Sensor.Disconnect();
                return true;
            }
            catch (Exception ex)
            {
                displayError("Could not disconnect from sensor properly: '" + ex.Message + '"');
                return false;
            }
        }

        #endregion

        #region Update Functions

        private void updateSensorModels()
        {
            cmbSensorModel.Items.Clear();

            // add the sensor models
            foreach (ISensor s in sensors)
            {
                cmbSensorModel.Items.Add(s.Properties.ModelName);
            }

            cmbSensorModel.SelectedIndex = 0;

            // todo: abstract default setting somewhere else
            // select IFC2401 by default
            for (int i = 0; i < cmbSensorModel.Items.Count; i++)
            {
                if (cmbSensorModel.Items[i].ToString() == "IFC2401")
                {
                    cmbSensorModel.SelectedIndex = i;
                    break;
                }
            }
        }

        private void updateSensorTypes()
        {
            cmbSensorType.Items.Clear();

            // add the sensor types
            foreach (string type in Sensor.SensorTypes)
            {
                cmbSensorType.Items.Add(type);
            }

            cmbSensorType.SelectedIndex = 0;
        }

        private void updateCOMPorts()
        {
            string[] ports = SerialPort.GetPortNames();
            Array.Sort(ports);

            cmbSensorPort.Items.Clear();
            cmbSensorPort.Items.AddRange(ports);

            cmbSensorPort.SelectedIndex = 0;
        }

        private string baudRateAutoDetectText = "Auto detect";
        private void updateBaudRates()
        {
            cmbSensorBaudRate.Items.Clear();
            cmbSensorBaudRate.Items.Add(baudRateAutoDetectText);

            foreach (int i in Sensor.BaudRates)
            {
                cmbSensorBaudRate.Items.Add(i.ToString());
            }

            cmbSensorBaudRate.SelectedIndex = 0;
        }

        #endregion
    }
}
