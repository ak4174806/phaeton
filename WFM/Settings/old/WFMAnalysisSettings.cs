﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WFM.Settings
{
    public class WFMAnalysisSettings : ISettings
    {
        public double RPM;
        public int NumberRevolutions;
        public double DriftFactor;
        public double PVS;
        public double LineSpeed;
        public double RollDiameter;
    }
}
