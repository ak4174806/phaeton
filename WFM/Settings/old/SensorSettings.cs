﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WFM.Settings
{
    public class SensorSettings : ISettings
    {
        protected string sensorModel;
        protected string sensorType;
        protected string sensorPort;
        protected int sensorBaudRate;

        public string SensorModel
        {
            get { return sensorModel; }
            set { sensorModel = value; }
        }

        public string SensorType
        {
            get { return sensorType; }
            set { sensorType = value; }
        }

        public string SensorPort
        {
            get { return sensorPort; }
            set { sensorPort = value; }
        }

        public int SensorBaudRate
        {
            get { return sensorBaudRate; }
            set { sensorBaudRate = value; }
        }

        public override bool Equals(object obj)
        {
            // see http://weblogs.asp.net/tgraham/archive/2004/03/23/94870.aspx for appropriate checks
            if (obj == null) return false;

            if (this.GetType() != obj.GetType()) return false;

            // safe because of the GetType check
            SensorSettings s2 = (SensorSettings)obj;

            if (!SensorModel.Equals(s2.SensorModel)) return false;
            if (!sensorType.Equals(s2.sensorType)) return false;
            if (!sensorPort.Equals(s2.sensorPort)) return false;
            if (!sensorBaudRate.Equals(s2.sensorBaudRate)) return false;

            return true;
        }
    }
}
