﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WFM.Settings
{
    public class DataSettings : ISettings
    {
        protected string folderName;
        protected string filePattern;

        public string FolderName
        {
            get { return folderName; }
            set { folderName = value; }
        }

        public string FilePattern
        {
            get { return filePattern; }
            set { filePattern = value; }
        }
    }
}
