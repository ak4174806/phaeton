﻿namespace WFM.Settings
{
    partial class DataSavingSettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.Label lblFilenamePattern;
            System.Windows.Forms.Label lblDataLocation;
            this.txtFilename = new System.Windows.Forms.TextBox();
            this.btnBrowseFolder = new System.Windows.Forms.Button();
            this.txtFolder = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            lblFilenamePattern = new System.Windows.Forms.Label();
            lblDataLocation = new System.Windows.Forms.Label();
            this.pnlContent.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblSettingsTitle
            // 
            this.lblSettingsTitle.Size = new System.Drawing.Size(180, 24);
            this.lblSettingsTitle.Text = "Data Saving Settings";
            // 
            // pnlContent
            // 
            this.pnlContent.Controls.Add(this.label1);
            this.pnlContent.Controls.Add(this.txtFilename);
            this.pnlContent.Controls.Add(lblFilenamePattern);
            this.pnlContent.Controls.Add(this.btnBrowseFolder);
            this.pnlContent.Controls.Add(this.txtFolder);
            this.pnlContent.Controls.Add(lblDataLocation);
            this.pnlContent.Size = new System.Drawing.Size(250, 181);
            // 
            // lblFilenamePattern
            // 
            lblFilenamePattern.AutoSize = true;
            lblFilenamePattern.Location = new System.Drawing.Point(12, 18);
            lblFilenamePattern.Name = "lblFilenamePattern";
            lblFilenamePattern.Size = new System.Drawing.Size(52, 13);
            lblFilenamePattern.TabIndex = 33;
            lblFilenamePattern.Text = "Filename:";
            // 
            // lblDataLocation
            // 
            lblDataLocation.AutoSize = true;
            lblDataLocation.Location = new System.Drawing.Point(12, 48);
            lblDataLocation.Name = "lblDataLocation";
            lblDataLocation.Size = new System.Drawing.Size(39, 13);
            lblDataLocation.TabIndex = 30;
            lblDataLocation.Text = "Folder:";
            // 
            // txtFilename
            // 
            this.txtFilename.Location = new System.Drawing.Point(79, 15);
            this.txtFilename.Name = "txtFilename";
            this.txtFilename.Size = new System.Drawing.Size(156, 20);
            this.txtFilename.TabIndex = 34;
            // 
            // btnBrowseFolder
            // 
            this.btnBrowseFolder.Location = new System.Drawing.Point(79, 71);
            this.btnBrowseFolder.Name = "btnBrowseFolder";
            this.btnBrowseFolder.Size = new System.Drawing.Size(75, 23);
            this.btnBrowseFolder.TabIndex = 32;
            this.btnBrowseFolder.Text = "Browse";
            this.btnBrowseFolder.UseVisualStyleBackColor = true;
            // 
            // txtFolder
            // 
            this.txtFolder.Location = new System.Drawing.Point(79, 45);
            this.txtFolder.Name = "txtFolder";
            this.txtFolder.ReadOnly = true;
            this.txtFolder.Size = new System.Drawing.Size(156, 20);
            this.txtFolder.TabIndex = 31;
            this.txtFolder.Text = "c:\\";
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(7, 110);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(228, 61);
            this.label1.TabIndex = 35;
            this.label1.Text = "Instructions:\r\n\r\nUse {nnn} to include a sequential 3-digit scan number in the fil" +
                "ename.";
            // 
            // DataSavingSettingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(250, 288);
            this.Name = "DataSavingSettingsForm";
            this.Text = "Data Saving Settings";
            this.pnlContent.ResumeLayout(false);
            this.pnlContent.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox txtFilename;
        private System.Windows.Forms.Button btnBrowseFolder;
        private System.Windows.Forms.TextBox txtFolder;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
    }
}