﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WFM.Settings
{
    public class SavedSettings
    {
        // 2-dimensional
        // access through settings[formID][controlName]
        protected Dictionary<string, Dictionary<string, string>> settings;

        public SavedSettings()
        {
            settings = new Dictionary<string, Dictionary<string, string>>();
        }

        public string[] GetSavedFormIds()
        {
            string[] ids = new string[settings.Keys.Count];
            int i = 0;
            foreach (string key in settings.Keys)
            {
                ids[i] = key;
                i++;
            }
                
            return ids;
        }

        public bool HasSavedFormId(string formId)
        {
            foreach (string key in settings.Keys)
            {
                if (formId == key)
                {
                    return settings[formId].Count > 0;
                }
            }

            return false;
        }

        public void AddSavedFormId(string formId)
        {
            settings.Add(formId, new Dictionary<string, string>());
        }
        

        public Dictionary<string, string> GetSettings(string formId)
        {
            return settings[formId];
        }

        public void SetSetting(string formId, string controlName, string controlValue)
        {
            settings[formId][controlName] = controlValue;
        }
    }
}
