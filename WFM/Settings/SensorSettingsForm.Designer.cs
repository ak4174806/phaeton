﻿namespace WFM.Settings
{
    partial class SensorSettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.Label lblSensorModel;
            System.Windows.Forms.Label lblBaudRate;
            System.Windows.Forms.Label lblSensorType;
            System.Windows.Forms.Label lblCOM;
            this.btnConnect = new System.Windows.Forms.Button();
            this.cmbSensorBaudRate = new System.Windows.Forms.ComboBox();
            this.cmbSensorModel = new System.Windows.Forms.ComboBox();
            this.btnRefreshPort = new System.Windows.Forms.Button();
            this.cmbSensorType = new System.Windows.Forms.ComboBox();
            this.cmbSensorPort = new System.Windows.Forms.ComboBox();
            lblSensorModel = new System.Windows.Forms.Label();
            lblBaudRate = new System.Windows.Forms.Label();
            lblSensorType = new System.Windows.Forms.Label();
            lblCOM = new System.Windows.Forms.Label();
            this.pnlContent.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblSettingsTitle
            // 
            this.lblSettingsTitle.Size = new System.Drawing.Size(141, 24);
            this.lblSettingsTitle.Text = "Sensor Settings";
            // 
            // pnlContent
            // 
            this.pnlContent.Controls.Add(this.btnConnect);
            this.pnlContent.Controls.Add(lblSensorModel);
            this.pnlContent.Controls.Add(this.cmbSensorBaudRate);
            this.pnlContent.Controls.Add(this.cmbSensorModel);
            this.pnlContent.Controls.Add(lblBaudRate);
            this.pnlContent.Controls.Add(lblSensorType);
            this.pnlContent.Controls.Add(this.btnRefreshPort);
            this.pnlContent.Controls.Add(this.cmbSensorType);
            this.pnlContent.Controls.Add(this.cmbSensorPort);
            this.pnlContent.Controls.Add(lblCOM);
            this.pnlContent.Size = new System.Drawing.Size(264, 174);
            // 
            // lblSensorModel
            // 
            lblSensorModel.AutoSize = true;
            lblSensorModel.Location = new System.Drawing.Point(12, 17);
            lblSensorModel.Name = "lblSensorModel";
            lblSensorModel.Size = new System.Drawing.Size(75, 13);
            lblSensorModel.TabIndex = 16;
            lblSensorModel.Text = "Sensor Model:";
            // 
            // lblBaudRate
            // 
            lblBaudRate.AutoSize = true;
            lblBaudRate.Location = new System.Drawing.Point(13, 113);
            lblBaudRate.Name = "lblBaudRate";
            lblBaudRate.Size = new System.Drawing.Size(61, 13);
            lblBaudRate.TabIndex = 21;
            lblBaudRate.Text = "Baud Rate:";
            // 
            // lblSensorType
            // 
            lblSensorType.AutoSize = true;
            lblSensorType.Location = new System.Drawing.Point(12, 49);
            lblSensorType.Name = "lblSensorType";
            lblSensorType.Size = new System.Drawing.Size(34, 13);
            lblSensorType.TabIndex = 18;
            lblSensorType.Text = "Type:";
            // 
            // lblCOM
            // 
            lblCOM.AutoSize = true;
            lblCOM.Location = new System.Drawing.Point(14, 82);
            lblCOM.Name = "lblCOM";
            lblCOM.Size = new System.Drawing.Size(56, 13);
            lblCOM.TabIndex = 20;
            lblCOM.Text = "COM Port:";
            // 
            // btnConnect
            // 
            this.btnConnect.Location = new System.Drawing.Point(103, 141);
            this.btnConnect.Name = "btnConnect";
            this.btnConnect.Size = new System.Drawing.Size(85, 23);
            this.btnConnect.TabIndex = 25;
            this.btnConnect.Text = "Connect";
            this.btnConnect.UseVisualStyleBackColor = true;
            // 
            // cmbSensorBaudRate
            // 
            this.cmbSensorBaudRate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSensorBaudRate.FormattingEnabled = true;
            this.cmbSensorBaudRate.Location = new System.Drawing.Point(103, 110);
            this.cmbSensorBaudRate.Name = "cmbSensorBaudRate";
            this.cmbSensorBaudRate.Size = new System.Drawing.Size(148, 21);
            this.cmbSensorBaudRate.TabIndex = 24;
            // 
            // cmbSensorModel
            // 
            this.cmbSensorModel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSensorModel.FormattingEnabled = true;
            this.cmbSensorModel.Location = new System.Drawing.Point(103, 13);
            this.cmbSensorModel.Name = "cmbSensorModel";
            this.cmbSensorModel.Size = new System.Drawing.Size(148, 21);
            this.cmbSensorModel.TabIndex = 17;
            // 
            // btnRefreshPort
            // 
            this.btnRefreshPort.Location = new System.Drawing.Point(188, 77);
            this.btnRefreshPort.Name = "btnRefreshPort";
            this.btnRefreshPort.Size = new System.Drawing.Size(63, 23);
            this.btnRefreshPort.TabIndex = 23;
            this.btnRefreshPort.Text = "Refresh";
            this.btnRefreshPort.UseVisualStyleBackColor = true;
            // 
            // cmbSensorType
            // 
            this.cmbSensorType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSensorType.FormattingEnabled = true;
            this.cmbSensorType.Location = new System.Drawing.Point(103, 46);
            this.cmbSensorType.Name = "cmbSensorType";
            this.cmbSensorType.Size = new System.Drawing.Size(148, 21);
            this.cmbSensorType.TabIndex = 19;
            // 
            // cmbSensorPort
            // 
            this.cmbSensorPort.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSensorPort.FormattingEnabled = true;
            this.cmbSensorPort.Location = new System.Drawing.Point(103, 77);
            this.cmbSensorPort.Name = "cmbSensorPort";
            this.cmbSensorPort.Size = new System.Drawing.Size(75, 21);
            this.cmbSensorPort.TabIndex = 22;
            // 
            // SensorSettingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(264, 281);
            this.ControlBox = true;
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(280, 319);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(280, 319);
            this.Name = "SensorSettingsForm";
            this.Text = "Sensor Settings";
            this.pnlContent.ResumeLayout(false);
            this.pnlContent.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnConnect;
        private System.Windows.Forms.ComboBox cmbSensorBaudRate;
        private System.Windows.Forms.ComboBox cmbSensorModel;
        private System.Windows.Forms.Button btnRefreshPort;
        private System.Windows.Forms.ComboBox cmbSensorType;
        private System.Windows.Forms.ComboBox cmbSensorPort;
    }
}