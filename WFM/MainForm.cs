﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using WFM.Settings;
using Analysis;

namespace WFM
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();

            SensorSettingsForm sensorSettingsForm = new SensorSettingsForm();
            btnSensorSettings.Click += new EventHandler(delegate(object sender, EventArgs e)
            {
                sensorSettingsForm.Show();
            });

            sensorSettingsForm.SettingsFormClosing += new BaseSettingsForm.SettingsFormHandler(delegate()
            {
                btnConnect.Text = (sensorSettingsForm.SensorState == SensorSettingsForm.SensorStates.Connected ? "Disconnect" : "Connect");
            });


            DataSavingSettingsForm dataSavingSettingsForm = new DataSavingSettingsForm();
            btnDataSavingSettings.Click += new EventHandler(delegate(object sender, EventArgs e)
            {
                dataSavingSettingsForm.Show();
            });
        }
    }
}
