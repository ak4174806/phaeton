﻿namespace WFM
{
    partial class WFMMockUp
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.PictureBox picLogo;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WFMMockUp));
            System.Windows.Forms.Label lblReferenceValue;
            System.Windows.Forms.GroupBox grpOutput;
            System.Windows.Forms.GroupBox grpZeroing;
            System.Windows.Forms.GroupBox grpMeasurements;
            System.Windows.Forms.GroupBox grpSensorSelection;
            System.Windows.Forms.Label lblSensorType;
            System.Windows.Forms.Label lblSensorModel;
            System.Windows.Forms.Label lblConnectionBaudRate;
            System.Windows.Forms.GroupBox grpConnection;
            System.Windows.Forms.Label lblCOMPort;
            System.Windows.Forms.GroupBox grpAcquisitionSettings;
            System.Windows.Forms.Label lblFiltering;
            System.Windows.Forms.Label lblChooseDataFolder;
            System.Windows.Forms.Label lblCopyright;
            System.Windows.Forms.Label label6;
            System.Windows.Forms.Label label7;
            System.Windows.Forms.Label label8;
            System.Windows.Forms.Label label9;
            System.Windows.Forms.Label label10;
            System.Windows.Forms.Label lblHeading;
            this.txtOutput = new System.Windows.Forms.RichTextBox();
            this.btnSaveOutput = new System.Windows.Forms.Button();
            this.btnClearOutput = new System.Windows.Forms.Button();
            this.chkInvertGraph = new System.Windows.Forms.CheckBox();
            this.numReferenceValue = new System.Windows.Forms.NumericUpDown();
            this.btnResetOffset = new System.Windows.Forms.Button();
            this.btnZeroReadings = new System.Windows.Forms.Button();
            this.chkAutoSaveData = new System.Windows.Forms.CheckBox();
            this.btnSaveData = new System.Windows.Forms.Button();
            this.btnStop = new System.Windows.Forms.Button();
            this.btnStart = new System.Windows.Forms.Button();
            this.btnCancelSensorSelection = new System.Windows.Forms.Button();
            this.btnUpdateSensorSelection = new System.Windows.Forms.Button();
            this.cmbSensorType = new System.Windows.Forms.ComboBox();
            this.cmbSensorModel = new System.Windows.Forms.ComboBox();
            this.btnDisconnect = new System.Windows.Forms.Button();
            this.btnConnect = new System.Windows.Forms.Button();
            this.cmbConnectionBaudRate = new System.Windows.Forms.ComboBox();
            this.btnRefreshCOMPorts = new System.Windows.Forms.Button();
            this.cmbCOMPorts = new System.Windows.Forms.ComboBox();
            this.numFiltering = new System.Windows.Forms.NumericUpDown();
            this.btnChooseDataFolder = new System.Windows.Forms.Button();
            this.txtDataFolder = new System.Windows.Forms.TextBox();
            this.txtReading = new System.Windows.Forms.TextBox();
            this.pnlRightCol = new System.Windows.Forms.Panel();
            this.grpGraphSettings = new System.Windows.Forms.GroupBox();
            this.numericUpDown2 = new System.Windows.Forms.NumericUpDown();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.radioButton4 = new System.Windows.Forms.RadioButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            picLogo = new System.Windows.Forms.PictureBox();
            lblReferenceValue = new System.Windows.Forms.Label();
            grpOutput = new System.Windows.Forms.GroupBox();
            grpZeroing = new System.Windows.Forms.GroupBox();
            grpMeasurements = new System.Windows.Forms.GroupBox();
            grpSensorSelection = new System.Windows.Forms.GroupBox();
            lblSensorType = new System.Windows.Forms.Label();
            lblSensorModel = new System.Windows.Forms.Label();
            lblConnectionBaudRate = new System.Windows.Forms.Label();
            grpConnection = new System.Windows.Forms.GroupBox();
            lblCOMPort = new System.Windows.Forms.Label();
            grpAcquisitionSettings = new System.Windows.Forms.GroupBox();
            lblFiltering = new System.Windows.Forms.Label();
            lblChooseDataFolder = new System.Windows.Forms.Label();
            lblCopyright = new System.Windows.Forms.Label();
            label6 = new System.Windows.Forms.Label();
            label7 = new System.Windows.Forms.Label();
            label8 = new System.Windows.Forms.Label();
            label9 = new System.Windows.Forms.Label();
            label10 = new System.Windows.Forms.Label();
            lblHeading = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(picLogo)).BeginInit();
            grpOutput.SuspendLayout();
            grpZeroing.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numReferenceValue)).BeginInit();
            grpMeasurements.SuspendLayout();
            grpSensorSelection.SuspendLayout();
            grpConnection.SuspendLayout();
            grpAcquisitionSettings.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numFiltering)).BeginInit();
            this.pnlRightCol.SuspendLayout();
            this.grpGraphSettings.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // picLogo
            // 
            picLogo.Image = ((System.Drawing.Image)(resources.GetObject("picLogo.Image")));
            picLogo.Location = new System.Drawing.Point(26, 22);
            picLogo.Name = "picLogo";
            picLogo.Size = new System.Drawing.Size(198, 102);
            picLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            picLogo.TabIndex = 24;
            picLogo.TabStop = false;
            // 
            // lblReferenceValue
            // 
            lblReferenceValue.AutoSize = true;
            lblReferenceValue.Location = new System.Drawing.Point(2, 90);
            lblReferenceValue.Name = "lblReferenceValue";
            lblReferenceValue.Size = new System.Drawing.Size(135, 13);
            lblReferenceValue.TabIndex = 25;
            lblReferenceValue.Text = "Reference Value (microns):";
            // 
            // grpOutput
            // 
            grpOutput.Controls.Add(this.txtOutput);
            grpOutput.Controls.Add(this.btnSaveOutput);
            grpOutput.Controls.Add(this.btnClearOutput);
            grpOutput.Location = new System.Drawing.Point(6, 476);
            grpOutput.Name = "grpOutput";
            grpOutput.Size = new System.Drawing.Size(228, 111);
            grpOutput.TabIndex = 5;
            grpOutput.TabStop = false;
            grpOutput.Text = "Output/Status";
            // 
            // txtOutput
            // 
            this.txtOutput.DetectUrls = false;
            this.txtOutput.Location = new System.Drawing.Point(10, 19);
            this.txtOutput.Name = "txtOutput";
            this.txtOutput.ReadOnly = true;
            this.txtOutput.Size = new System.Drawing.Size(200, 50);
            this.txtOutput.TabIndex = 27;
            this.txtOutput.Text = "";
            // 
            // btnSaveOutput
            // 
            this.btnSaveOutput.Location = new System.Drawing.Point(33, 77);
            this.btnSaveOutput.Name = "btnSaveOutput";
            this.btnSaveOutput.Size = new System.Drawing.Size(75, 23);
            this.btnSaveOutput.TabIndex = 28;
            this.btnSaveOutput.Text = "Save to file";
            this.btnSaveOutput.UseVisualStyleBackColor = true;
            // 
            // btnClearOutput
            // 
            this.btnClearOutput.Location = new System.Drawing.Point(114, 77);
            this.btnClearOutput.Name = "btnClearOutput";
            this.btnClearOutput.Size = new System.Drawing.Size(75, 23);
            this.btnClearOutput.TabIndex = 29;
            this.btnClearOutput.Text = "Clear";
            this.btnClearOutput.UseVisualStyleBackColor = true;
            // 
            // grpZeroing
            // 
            grpZeroing.Controls.Add(this.chkInvertGraph);
            grpZeroing.Controls.Add(this.numReferenceValue);
            grpZeroing.Controls.Add(lblReferenceValue);
            grpZeroing.Controls.Add(this.btnResetOffset);
            grpZeroing.Controls.Add(this.btnZeroReadings);
            grpZeroing.Location = new System.Drawing.Point(12, 198);
            grpZeroing.Name = "grpZeroing";
            grpZeroing.Size = new System.Drawing.Size(226, 134);
            grpZeroing.TabIndex = 17;
            grpZeroing.TabStop = false;
            grpZeroing.Text = "Setting baseline";
            // 
            // chkInvertGraph
            // 
            this.chkInvertGraph.AutoSize = true;
            this.chkInvertGraph.Location = new System.Drawing.Point(8, 109);
            this.chkInvertGraph.Name = "chkInvertGraph";
            this.chkInvertGraph.Size = new System.Drawing.Size(83, 17);
            this.chkInvertGraph.TabIndex = 27;
            this.chkInvertGraph.Text = "Invert graph";
            this.chkInvertGraph.UseVisualStyleBackColor = true;
            // 
            // numReferenceValue
            // 
            this.numReferenceValue.DecimalPlaces = 2;
            this.numReferenceValue.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.numReferenceValue.Location = new System.Drawing.Point(144, 87);
            this.numReferenceValue.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.numReferenceValue.Minimum = new decimal(new int[] {
            1000000,
            0,
            0,
            -2147483648});
            this.numReferenceValue.Name = "numReferenceValue";
            this.numReferenceValue.Size = new System.Drawing.Size(76, 20);
            this.numReferenceValue.TabIndex = 26;
            // 
            // btnResetOffset
            // 
            this.btnResetOffset.Location = new System.Drawing.Point(26, 53);
            this.btnResetOffset.Name = "btnResetOffset";
            this.btnResetOffset.Size = new System.Drawing.Size(185, 24);
            this.btnResetOffset.TabIndex = 19;
            this.btnResetOffset.Text = "Remove baseline (clears graph)";
            this.btnResetOffset.UseVisualStyleBackColor = true;
            // 
            // btnZeroReadings
            // 
            this.btnZeroReadings.Location = new System.Drawing.Point(26, 23);
            this.btnZeroReadings.Name = "btnZeroReadings";
            this.btnZeroReadings.Size = new System.Drawing.Size(185, 24);
            this.btnZeroReadings.TabIndex = 18;
            this.btnZeroReadings.Text = "Set baseline (clears graph)";
            this.btnZeroReadings.UseVisualStyleBackColor = true;
            // 
            // grpMeasurements
            // 
            grpMeasurements.Controls.Add(this.chkAutoSaveData);
            grpMeasurements.Controls.Add(this.btnSaveData);
            grpMeasurements.Controls.Add(this.btnStop);
            grpMeasurements.Controls.Add(this.btnStart);
            grpMeasurements.Location = new System.Drawing.Point(13, 339);
            grpMeasurements.Name = "grpMeasurements";
            grpMeasurements.Size = new System.Drawing.Size(218, 127);
            grpMeasurements.TabIndex = 15;
            grpMeasurements.TabStop = false;
            grpMeasurements.Text = "Control";
            // 
            // chkAutoSaveData
            // 
            this.chkAutoSaveData.AutoSize = true;
            this.chkAutoSaveData.Location = new System.Drawing.Point(41, 90);
            this.chkAutoSaveData.Name = "chkAutoSaveData";
            this.chkAutoSaveData.Size = new System.Drawing.Size(143, 17);
            this.chkAutoSaveData.TabIndex = 26;
            this.chkAutoSaveData.Text = "Auto-save bufffered data";
            this.chkAutoSaveData.UseVisualStyleBackColor = true;
            // 
            // btnSaveData
            // 
            this.btnSaveData.Enabled = false;
            this.btnSaveData.Location = new System.Drawing.Point(40, 60);
            this.btnSaveData.Name = "btnSaveData";
            this.btnSaveData.Size = new System.Drawing.Size(146, 24);
            this.btnSaveData.TabIndex = 25;
            this.btnSaveData.Text = "Save buffered data now";
            this.btnSaveData.UseVisualStyleBackColor = true;
            // 
            // btnStop
            // 
            this.btnStop.Enabled = false;
            this.btnStop.Location = new System.Drawing.Point(112, 27);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(90, 24);
            this.btnStop.TabIndex = 24;
            this.btnStop.Text = "Stop";
            this.btnStop.UseVisualStyleBackColor = true;
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(15, 27);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(91, 24);
            this.btnStart.TabIndex = 23;
            this.btnStart.Text = "Start";
            this.btnStart.UseVisualStyleBackColor = true;
            // 
            // grpSensorSelection
            // 
            grpSensorSelection.Controls.Add(this.btnCancelSensorSelection);
            grpSensorSelection.Controls.Add(this.btnUpdateSensorSelection);
            grpSensorSelection.Controls.Add(this.cmbSensorType);
            grpSensorSelection.Controls.Add(lblSensorType);
            grpSensorSelection.Controls.Add(this.cmbSensorModel);
            grpSensorSelection.Controls.Add(lblSensorModel);
            grpSensorSelection.Location = new System.Drawing.Point(17, 158);
            grpSensorSelection.Name = "grpSensorSelection";
            grpSensorSelection.Size = new System.Drawing.Size(219, 136);
            grpSensorSelection.TabIndex = 27;
            grpSensorSelection.TabStop = false;
            grpSensorSelection.Text = "Sensor Selection";
            // 
            // btnCancelSensorSelection
            // 
            this.btnCancelSensorSelection.Enabled = false;
            this.btnCancelSensorSelection.Location = new System.Drawing.Point(114, 92);
            this.btnCancelSensorSelection.Name = "btnCancelSensorSelection";
            this.btnCancelSensorSelection.Size = new System.Drawing.Size(75, 23);
            this.btnCancelSensorSelection.TabIndex = 4;
            this.btnCancelSensorSelection.Text = "Cancel";
            this.btnCancelSensorSelection.UseVisualStyleBackColor = true;
            // 
            // btnUpdateSensorSelection
            // 
            this.btnUpdateSensorSelection.Enabled = false;
            this.btnUpdateSensorSelection.Location = new System.Drawing.Point(27, 92);
            this.btnUpdateSensorSelection.Name = "btnUpdateSensorSelection";
            this.btnUpdateSensorSelection.Size = new System.Drawing.Size(75, 23);
            this.btnUpdateSensorSelection.TabIndex = 3;
            this.btnUpdateSensorSelection.Text = "Select";
            this.btnUpdateSensorSelection.UseVisualStyleBackColor = true;
            // 
            // cmbSensorType
            // 
            this.cmbSensorType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSensorType.Enabled = false;
            this.cmbSensorType.FormattingEnabled = true;
            this.cmbSensorType.Items.AddRange(new object[] {
            "IFC2401-3"});
            this.cmbSensorType.Location = new System.Drawing.Point(87, 57);
            this.cmbSensorType.Name = "cmbSensorType";
            this.cmbSensorType.Size = new System.Drawing.Size(121, 21);
            this.cmbSensorType.TabIndex = 2;
            // 
            // lblSensorType
            // 
            lblSensorType.AutoSize = true;
            lblSensorType.Location = new System.Drawing.Point(6, 60);
            lblSensorType.Name = "lblSensorType";
            lblSensorType.Size = new System.Drawing.Size(34, 13);
            lblSensorType.TabIndex = 2;
            lblSensorType.Text = "Type:";
            // 
            // cmbSensorModel
            // 
            this.cmbSensorModel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSensorModel.Enabled = false;
            this.cmbSensorModel.FormattingEnabled = true;
            this.cmbSensorModel.Items.AddRange(new object[] {
            "IFC2401"});
            this.cmbSensorModel.Location = new System.Drawing.Point(87, 29);
            this.cmbSensorModel.Name = "cmbSensorModel";
            this.cmbSensorModel.Size = new System.Drawing.Size(121, 21);
            this.cmbSensorModel.TabIndex = 1;
            // 
            // lblSensorModel
            // 
            lblSensorModel.AutoSize = true;
            lblSensorModel.Location = new System.Drawing.Point(6, 32);
            lblSensorModel.Name = "lblSensorModel";
            lblSensorModel.Size = new System.Drawing.Size(75, 13);
            lblSensorModel.TabIndex = 0;
            lblSensorModel.Text = "Sensor Model:";
            // 
            // lblConnectionBaudRate
            // 
            lblConnectionBaudRate.AutoSize = true;
            lblConnectionBaudRate.Location = new System.Drawing.Point(8, 56);
            lblConnectionBaudRate.Name = "lblConnectionBaudRate";
            lblConnectionBaudRate.Size = new System.Drawing.Size(61, 13);
            lblConnectionBaudRate.TabIndex = 3;
            lblConnectionBaudRate.Text = "Baud Rate:";
            // 
            // grpConnection
            // 
            grpConnection.Controls.Add(this.btnDisconnect);
            grpConnection.Controls.Add(this.btnConnect);
            grpConnection.Controls.Add(this.cmbConnectionBaudRate);
            grpConnection.Controls.Add(lblConnectionBaudRate);
            grpConnection.Controls.Add(this.btnRefreshCOMPorts);
            grpConnection.Controls.Add(this.cmbCOMPorts);
            grpConnection.Controls.Add(lblCOMPort);
            grpConnection.Location = new System.Drawing.Point(17, 314);
            grpConnection.Name = "grpConnection";
            grpConnection.Size = new System.Drawing.Size(219, 136);
            grpConnection.TabIndex = 25;
            grpConnection.TabStop = false;
            grpConnection.Text = "Connection";
            // 
            // btnDisconnect
            // 
            this.btnDisconnect.Location = new System.Drawing.Point(114, 92);
            this.btnDisconnect.Name = "btnDisconnect";
            this.btnDisconnect.Size = new System.Drawing.Size(75, 23);
            this.btnDisconnect.TabIndex = 9;
            this.btnDisconnect.Text = "Disconnect";
            this.btnDisconnect.UseVisualStyleBackColor = true;
            // 
            // btnConnect
            // 
            this.btnConnect.Enabled = false;
            this.btnConnect.Location = new System.Drawing.Point(21, 92);
            this.btnConnect.Name = "btnConnect";
            this.btnConnect.Size = new System.Drawing.Size(75, 23);
            this.btnConnect.TabIndex = 8;
            this.btnConnect.Text = "Connect";
            this.btnConnect.UseVisualStyleBackColor = true;
            // 
            // cmbConnectionBaudRate
            // 
            this.cmbConnectionBaudRate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbConnectionBaudRate.Enabled = false;
            this.cmbConnectionBaudRate.FormattingEnabled = true;
            this.cmbConnectionBaudRate.Items.AddRange(new object[] {
            "Autodetect"});
            this.cmbConnectionBaudRate.Location = new System.Drawing.Point(78, 53);
            this.cmbConnectionBaudRate.Name = "cmbConnectionBaudRate";
            this.cmbConnectionBaudRate.Size = new System.Drawing.Size(121, 21);
            this.cmbConnectionBaudRate.TabIndex = 7;
            // 
            // btnRefreshCOMPorts
            // 
            this.btnRefreshCOMPorts.Enabled = false;
            this.btnRefreshCOMPorts.Location = new System.Drawing.Point(150, 20);
            this.btnRefreshCOMPorts.Name = "btnRefreshCOMPorts";
            this.btnRefreshCOMPorts.Size = new System.Drawing.Size(63, 23);
            this.btnRefreshCOMPorts.TabIndex = 6;
            this.btnRefreshCOMPorts.Text = "Refresh";
            this.btnRefreshCOMPorts.UseVisualStyleBackColor = true;
            // 
            // cmbCOMPorts
            // 
            this.cmbCOMPorts.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCOMPorts.Enabled = false;
            this.cmbCOMPorts.FormattingEnabled = true;
            this.cmbCOMPorts.Items.AddRange(new object[] {
            "COM3"});
            this.cmbCOMPorts.Location = new System.Drawing.Point(69, 21);
            this.cmbCOMPorts.Name = "cmbCOMPorts";
            this.cmbCOMPorts.Size = new System.Drawing.Size(75, 21);
            this.cmbCOMPorts.TabIndex = 5;
            // 
            // lblCOMPort
            // 
            lblCOMPort.AutoSize = true;
            lblCOMPort.Location = new System.Drawing.Point(7, 25);
            lblCOMPort.Name = "lblCOMPort";
            lblCOMPort.Size = new System.Drawing.Size(56, 13);
            lblCOMPort.TabIndex = 0;
            lblCOMPort.Text = "COM Port:";
            // 
            // grpAcquisitionSettings
            // 
            grpAcquisitionSettings.Controls.Add(this.numFiltering);
            grpAcquisitionSettings.Controls.Add(lblFiltering);
            grpAcquisitionSettings.Controls.Add(this.btnChooseDataFolder);
            grpAcquisitionSettings.Controls.Add(this.txtDataFolder);
            grpAcquisitionSettings.Controls.Add(lblChooseDataFolder);
            grpAcquisitionSettings.Location = new System.Drawing.Point(17, 470);
            grpAcquisitionSettings.Name = "grpAcquisitionSettings";
            grpAcquisitionSettings.Size = new System.Drawing.Size(219, 110);
            grpAcquisitionSettings.TabIndex = 29;
            grpAcquisitionSettings.TabStop = false;
            grpAcquisitionSettings.Text = "Data Settings";
            // 
            // numFiltering
            // 
            this.numFiltering.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numFiltering.Location = new System.Drawing.Point(128, 79);
            this.numFiltering.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numFiltering.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numFiltering.Name = "numFiltering";
            this.numFiltering.Size = new System.Drawing.Size(74, 20);
            this.numFiltering.TabIndex = 15;
            this.numFiltering.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // lblFiltering
            // 
            lblFiltering.AutoSize = true;
            lblFiltering.Location = new System.Drawing.Point(14, 81);
            lblFiltering.Name = "lblFiltering";
            lblFiltering.Size = new System.Drawing.Size(108, 13);
            lblFiltering.TabIndex = 19;
            lblFiltering.Text = "Save every nth point:";
            // 
            // btnChooseDataFolder
            // 
            this.btnChooseDataFolder.Location = new System.Drawing.Point(131, 44);
            this.btnChooseDataFolder.Name = "btnChooseDataFolder";
            this.btnChooseDataFolder.Size = new System.Drawing.Size(75, 23);
            this.btnChooseDataFolder.TabIndex = 14;
            this.btnChooseDataFolder.Text = "Choose";
            this.btnChooseDataFolder.UseVisualStyleBackColor = true;
            // 
            // txtDataFolder
            // 
            this.txtDataFolder.Location = new System.Drawing.Point(15, 47);
            this.txtDataFolder.Name = "txtDataFolder";
            this.txtDataFolder.ReadOnly = true;
            this.txtDataFolder.Size = new System.Drawing.Size(106, 20);
            this.txtDataFolder.TabIndex = 13;
            this.txtDataFolder.Text = "c:\\";
            // 
            // lblChooseDataFolder
            // 
            lblChooseDataFolder.AutoSize = true;
            lblChooseDataFolder.Location = new System.Drawing.Point(12, 29);
            lblChooseDataFolder.Name = "lblChooseDataFolder";
            lblChooseDataFolder.Size = new System.Drawing.Size(77, 13);
            lblChooseDataFolder.TabIndex = 0;
            lblChooseDataFolder.Text = "Data Location:";
            // 
            // lblCopyright
            // 
            lblCopyright.AutoSize = true;
            lblCopyright.ForeColor = System.Drawing.SystemColors.ControlText;
            lblCopyright.Location = new System.Drawing.Point(23, 590);
            lblCopyright.Name = "lblCopyright";
            lblCopyright.Size = new System.Drawing.Size(156, 13);
            lblCopyright.TabIndex = 31;
            lblCopyright.Text = "Copyright 2009 Wolf Innovation";
            // 
            // txtReading
            // 
            this.txtReading.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.txtReading.Font = new System.Drawing.Font("Verdana", 28F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtReading.ForeColor = System.Drawing.Color.Black;
            this.txtReading.Location = new System.Drawing.Point(273, 71);
            this.txtReading.Multiline = true;
            this.txtReading.Name = "txtReading";
            this.txtReading.ReadOnly = true;
            this.txtReading.Size = new System.Drawing.Size(458, 60);
            this.txtReading.TabIndex = 30;
            this.txtReading.Text = "19.9  microns";
            // 
            // pnlRightCol
            // 
            this.pnlRightCol.Controls.Add(grpOutput);
            this.pnlRightCol.Controls.Add(grpZeroing);
            this.pnlRightCol.Controls.Add(grpMeasurements);
            this.pnlRightCol.Controls.Add(this.grpGraphSettings);
            this.pnlRightCol.Location = new System.Drawing.Point(773, 12);
            this.pnlRightCol.Name = "pnlRightCol";
            this.pnlRightCol.Size = new System.Drawing.Size(246, 595);
            this.pnlRightCol.TabIndex = 32;
            // 
            // grpGraphSettings
            // 
            this.grpGraphSettings.Controls.Add(this.comboBox2);
            this.grpGraphSettings.Controls.Add(this.groupBox2);
            this.grpGraphSettings.Controls.Add(this.radioButton4);
            this.grpGraphSettings.Controls.Add(label10);
            this.grpGraphSettings.Controls.Add(this.radioButton3);
            this.grpGraphSettings.Controls.Add(this.numericUpDown2);
            this.grpGraphSettings.Controls.Add(label7);
            this.grpGraphSettings.Controls.Add(label6);
            this.grpGraphSettings.Location = new System.Drawing.Point(13, 10);
            this.grpGraphSettings.Name = "grpGraphSettings";
            this.grpGraphSettings.Size = new System.Drawing.Size(234, 178);
            this.grpGraphSettings.TabIndex = 16;
            this.grpGraphSettings.TabStop = false;
            this.grpGraphSettings.Text = "Monitor Mode";
            // 
            // label6
            // 
            label6.AutoSize = true;
            label6.Location = new System.Drawing.Point(12, 27);
            label6.Name = "label6";
            label6.Size = new System.Drawing.Size(120, 13);
            label6.TabIndex = 17;
            label6.Text = "Paint Volume Solids (%):";
            // 
            // numericUpDown2
            // 
            this.numericUpDown2.DecimalPlaces = 2;
            this.numericUpDown2.Increment = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.numericUpDown2.Location = new System.Drawing.Point(142, 23);
            this.numericUpDown2.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.numericUpDown2.Minimum = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.numericUpDown2.Name = "numericUpDown2";
            this.numericUpDown2.Size = new System.Drawing.Size(65, 20);
            this.numericUpDown2.TabIndex = 20;
            this.numericUpDown2.Value = new decimal(new int[] {
            50,
            0,
            0,
            0});
            // 
            // label7
            // 
            label7.AutoSize = true;
            label7.Location = new System.Drawing.Point(207, 27);
            label7.Name = "label7";
            label7.Size = new System.Drawing.Size(15, 13);
            label7.TabIndex = 17;
            label7.Text = "%";
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.Checked = true;
            this.radioButton3.Location = new System.Drawing.Point(23, 56);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(87, 17);
            this.radioButton3.TabIndex = 21;
            this.radioButton3.TabStop = true;
            this.radioButton3.Text = "Manual Input";
            this.radioButton3.UseVisualStyleBackColor = true;
            // 
            // radioButton4
            // 
            this.radioButton4.AutoSize = true;
            this.radioButton4.Location = new System.Drawing.Point(124, 56);
            this.radioButton4.Name = "radioButton4";
            this.radioButton4.Size = new System.Drawing.Size(95, 17);
            this.radioButton4.TabIndex = 21;
            this.radioButton4.TabStop = true;
            this.radioButton4.Text = "Electrical Input";
            this.radioButton4.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.textBox4);
            this.groupBox2.Controls.Add(this.textBox3);
            this.groupBox2.Controls.Add(label9);
            this.groupBox2.Controls.Add(label8);
            this.groupBox2.Location = new System.Drawing.Point(13, 79);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(212, 66);
            this.groupBox2.TabIndex = 22;
            this.groupBox2.TabStop = false;
            // 
            // label8
            // 
            label8.AutoSize = true;
            label8.Location = new System.Drawing.Point(7, 16);
            label8.Name = "label8";
            label8.Size = new System.Drawing.Size(112, 13);
            label8.TabIndex = 17;
            label8.Text = "Applicator Roll Speed:";
            // 
            // label9
            // 
            label9.AutoSize = true;
            label9.Location = new System.Drawing.Point(8, 43);
            label9.Name = "label9";
            label9.Size = new System.Drawing.Size(65, 13);
            label9.TabIndex = 17;
            label9.Text = "Strip Speed:";
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(123, 13);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(77, 20);
            this.textBox3.TabIndex = 18;
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(123, 39);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(77, 20);
            this.textBox4.TabIndex = 18;
            // 
            // label10
            // 
            label10.AutoSize = true;
            label10.Location = new System.Drawing.Point(21, 153);
            label10.Name = "label10";
            label10.Size = new System.Drawing.Size(76, 13);
            label10.TabIndex = 17;
            label10.Text = "Coating Mode:";
            // 
            // comboBox2
            // 
            this.comboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Items.AddRange(new object[] {
            "Forward Coaring"});
            this.comboBox2.Location = new System.Drawing.Point(114, 150);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(102, 21);
            this.comboBox2.TabIndex = 23;
            // 
            // lblHeading
            // 
            lblHeading.Font = new System.Drawing.Font("Verdana", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            lblHeading.ForeColor = System.Drawing.Color.SteelBlue;
            lblHeading.Location = new System.Drawing.Point(268, 15);
            lblHeading.Name = "lblHeading";
            lblHeading.Size = new System.Drawing.Size(463, 37);
            lblHeading.TabIndex = 34;
            lblHeading.Text = "Wolf Innovation: Wet Film Monitor";
            lblHeading.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::WFM.Properties.Resources.scan_axes;
            this.pictureBox1.Location = new System.Drawing.Point(245, 167);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(518, 440);
            this.pictureBox1.TabIndex = 35;
            this.pictureBox1.TabStop = false;
            // 
            // WFMMockUp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1031, 618);
            this.Controls.Add(lblCopyright);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(lblHeading);
            this.Controls.Add(this.txtReading);
            this.Controls.Add(this.pnlRightCol);
            this.Controls.Add(grpSensorSelection);
            this.Controls.Add(grpConnection);
            this.Controls.Add(grpAcquisitionSettings);
            this.Controls.Add(picLogo);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "WFMMockUp";
            this.Text = "Wolf Innovation: Wet Film Monitor - WFM1";
            ((System.ComponentModel.ISupportInitialize)(picLogo)).EndInit();
            grpOutput.ResumeLayout(false);
            grpZeroing.ResumeLayout(false);
            grpZeroing.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numReferenceValue)).EndInit();
            grpMeasurements.ResumeLayout(false);
            grpMeasurements.PerformLayout();
            grpSensorSelection.ResumeLayout(false);
            grpSensorSelection.PerformLayout();
            grpConnection.ResumeLayout(false);
            grpConnection.PerformLayout();
            grpAcquisitionSettings.ResumeLayout(false);
            grpAcquisitionSettings.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numFiltering)).EndInit();
            this.pnlRightCol.ResumeLayout(false);
            this.grpGraphSettings.ResumeLayout(false);
            this.grpGraphSettings.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox chkInvertGraph;
        private System.Windows.Forms.NumericUpDown numReferenceValue;
        private System.Windows.Forms.Button btnResetOffset;
        private System.Windows.Forms.Button btnZeroReadings;
        private System.Windows.Forms.TextBox txtReading;
        private System.Windows.Forms.RichTextBox txtOutput;
        private System.Windows.Forms.Button btnSaveOutput;
        private System.Windows.Forms.Button btnClearOutput;
        private System.Windows.Forms.Panel pnlRightCol;
        private System.Windows.Forms.GroupBox grpGraphSettings;
        private System.Windows.Forms.CheckBox chkAutoSaveData;
        private System.Windows.Forms.Button btnSaveData;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Button btnDisconnect;
        private System.Windows.Forms.Button btnConnect;
        private System.Windows.Forms.ComboBox cmbConnectionBaudRate;
        private System.Windows.Forms.Button btnCancelSensorSelection;
        private System.Windows.Forms.Button btnUpdateSensorSelection;
        private System.Windows.Forms.ComboBox cmbSensorType;
        private System.Windows.Forms.ComboBox cmbSensorModel;
        private System.Windows.Forms.Button btnRefreshCOMPorts;
        private System.Windows.Forms.ComboBox cmbCOMPorts;
        private System.Windows.Forms.NumericUpDown numFiltering;
        private System.Windows.Forms.Button btnChooseDataFolder;
        private System.Windows.Forms.TextBox txtDataFolder;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton radioButton4;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.NumericUpDown numericUpDown2;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}