﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace WFM
{
    public partial class WFMMockUp : Form
    {
        public WFMMockUp()
        {
            InitializeComponent();

            cmbCOMPorts.SelectedIndex = 0;
            cmbConnectionBaudRate.SelectedIndex = 0;
            cmbSensorModel.SelectedIndex = 0;
            cmbSensorType.SelectedIndex = 0;

            comboBox2.SelectedIndex = 0;
        }
    }
}
