﻿namespace WFM
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.GroupBox grpDataSource;
            System.Windows.Forms.Label label2;
            System.Windows.Forms.Label txtHeading;
            this.radDataSourceFile = new System.Windows.Forms.RadioButton();
            this.radDataSourceSensor = new System.Windows.Forms.RadioButton();
            this.btnSelectDataSource = new System.Windows.Forms.Button();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnDataAnalysisSettings = new System.Windows.Forms.Button();
            this.btnDataSavingSettings = new System.Windows.Forms.Button();
            this.chkAutoSaveData = new System.Windows.Forms.CheckBox();
            this.btnConnect = new System.Windows.Forms.Button();
            this.btnSensorSettings = new System.Windows.Forms.Button();
            this.txtDisplay = new System.Windows.Forms.TextBox();
            this.zgcGraph = new ZedGraph.ZedGraphControl();
            grpDataSource = new System.Windows.Forms.GroupBox();
            label2 = new System.Windows.Forms.Label();
            txtHeading = new System.Windows.Forms.Label();
            grpDataSource.SuspendLayout();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpDataSource
            // 
            grpDataSource.Controls.Add(this.radDataSourceFile);
            grpDataSource.Controls.Add(this.radDataSourceSensor);
            grpDataSource.Controls.Add(this.btnSelectDataSource);
            grpDataSource.Controls.Add(label2);
            grpDataSource.Location = new System.Drawing.Point(14, 254);
            grpDataSource.Name = "grpDataSource";
            grpDataSource.Size = new System.Drawing.Size(197, 118);
            grpDataSource.TabIndex = 15;
            grpDataSource.TabStop = false;
            grpDataSource.Text = "Data Source";
            // 
            // radDataSourceFile
            // 
            this.radDataSourceFile.AutoSize = true;
            this.radDataSourceFile.Location = new System.Drawing.Point(58, 42);
            this.radDataSourceFile.Name = "radDataSourceFile";
            this.radDataSourceFile.Size = new System.Drawing.Size(85, 17);
            this.radDataSourceFile.TabIndex = 1;
            this.radDataSourceFile.Text = "data from file";
            this.radDataSourceFile.UseVisualStyleBackColor = true;
            // 
            // radDataSourceSensor
            // 
            this.radDataSourceSensor.AutoSize = true;
            this.radDataSourceSensor.Checked = true;
            this.radDataSourceSensor.Location = new System.Drawing.Point(58, 19);
            this.radDataSourceSensor.Name = "radDataSourceSensor";
            this.radDataSourceSensor.Size = new System.Drawing.Size(103, 17);
            this.radDataSourceSensor.TabIndex = 1;
            this.radDataSourceSensor.TabStop = true;
            this.radDataSourceSensor.Text = "data from sensor";
            this.radDataSourceSensor.UseVisualStyleBackColor = true;
            // 
            // btnSelectDataSource
            // 
            this.btnSelectDataSource.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSelectDataSource.Location = new System.Drawing.Point(33, 68);
            this.btnSelectDataSource.Name = "btnSelectDataSource";
            this.btnSelectDataSource.Size = new System.Drawing.Size(125, 35);
            this.btnSelectDataSource.TabIndex = 14;
            this.btnSelectDataSource.Text = "Go / Select Data Files";
            this.btnSelectDataSource.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new System.Drawing.Point(13, 22);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(29, 13);
            label2.TabIndex = 0;
            label2.Text = "Use:";
            // 
            // txtHeading
            // 
            txtHeading.AutoSize = true;
            txtHeading.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            txtHeading.Location = new System.Drawing.Point(31, 18);
            txtHeading.Name = "txtHeading";
            txtHeading.Size = new System.Drawing.Size(159, 25);
            txtHeading.TabIndex = 8;
            txtHeading.Text = "Wet Film Monitor";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.IsSplitterFixed = true;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(grpDataSource);
            this.splitContainer1.Panel1.Controls.Add(this.btnClose);
            this.splitContainer1.Panel1.Controls.Add(this.btnDataAnalysisSettings);
            this.splitContainer1.Panel1.Controls.Add(this.btnDataSavingSettings);
            this.splitContainer1.Panel1.Controls.Add(this.chkAutoSaveData);
            this.splitContainer1.Panel1.Controls.Add(this.btnConnect);
            this.splitContainer1.Panel1.Controls.Add(this.btnSensorSettings);
            this.splitContainer1.Panel1.Controls.Add(txtHeading);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.txtDisplay);
            this.splitContainer1.Panel2.Controls.Add(this.zgcGraph);
            this.splitContainer1.Size = new System.Drawing.Size(789, 431);
            this.splitContainer1.SplitterDistance = 222;
            this.splitContainer1.TabIndex = 7;
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnClose.Location = new System.Drawing.Point(68, 384);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(89, 35);
            this.btnClose.TabIndex = 14;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            // 
            // btnDataAnalysisSettings
            // 
            this.btnDataAnalysisSettings.Location = new System.Drawing.Point(12, 194);
            this.btnDataAnalysisSettings.Name = "btnDataAnalysisSettings";
            this.btnDataAnalysisSettings.Size = new System.Drawing.Size(141, 35);
            this.btnDataAnalysisSettings.TabIndex = 13;
            this.btnDataAnalysisSettings.Text = "Data Analysis Settings";
            this.btnDataAnalysisSettings.UseVisualStyleBackColor = true;
            // 
            // btnDataSavingSettings
            // 
            this.btnDataSavingSettings.Location = new System.Drawing.Point(12, 141);
            this.btnDataSavingSettings.Name = "btnDataSavingSettings";
            this.btnDataSavingSettings.Size = new System.Drawing.Size(141, 35);
            this.btnDataSavingSettings.TabIndex = 12;
            this.btnDataSavingSettings.Text = "Data Saving Settings";
            this.btnDataSavingSettings.UseVisualStyleBackColor = true;
            // 
            // chkAutoSaveData
            // 
            this.chkAutoSaveData.AutoSize = true;
            this.chkAutoSaveData.Checked = true;
            this.chkAutoSaveData.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkAutoSaveData.Location = new System.Drawing.Point(14, 118);
            this.chkAutoSaveData.Name = "chkAutoSaveData";
            this.chkAutoSaveData.Size = new System.Drawing.Size(139, 17);
            this.chkAutoSaveData.TabIndex = 11;
            this.chkAutoSaveData.Text = "Save data automatically";
            this.chkAutoSaveData.UseVisualStyleBackColor = true;
            // 
            // btnConnect
            // 
            this.btnConnect.Location = new System.Drawing.Point(121, 63);
            this.btnConnect.Name = "btnConnect";
            this.btnConnect.Size = new System.Drawing.Size(89, 35);
            this.btnConnect.TabIndex = 9;
            this.btnConnect.Text = "Connect";
            this.btnConnect.UseVisualStyleBackColor = true;
            // 
            // btnSensorSettings
            // 
            this.btnSensorSettings.Location = new System.Drawing.Point(12, 63);
            this.btnSensorSettings.Name = "btnSensorSettings";
            this.btnSensorSettings.Size = new System.Drawing.Size(89, 35);
            this.btnSensorSettings.TabIndex = 10;
            this.btnSensorSettings.Text = "Sensor Settings";
            this.btnSensorSettings.UseVisualStyleBackColor = true;
            // 
            // txtDisplay
            // 
            this.txtDisplay.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDisplay.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDisplay.Location = new System.Drawing.Point(15, 18);
            this.txtDisplay.Name = "txtDisplay";
            this.txtDisplay.Size = new System.Drawing.Size(536, 45);
            this.txtDisplay.TabIndex = 8;
            // 
            // zgcGraph
            // 
            this.zgcGraph.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.zgcGraph.Location = new System.Drawing.Point(15, 81);
            this.zgcGraph.Name = "zgcGraph";
            this.zgcGraph.ScrollGrace = 0;
            this.zgcGraph.ScrollMaxX = 0;
            this.zgcGraph.ScrollMaxY = 0;
            this.zgcGraph.ScrollMaxY2 = 0;
            this.zgcGraph.ScrollMinX = 0;
            this.zgcGraph.ScrollMinY = 0;
            this.zgcGraph.ScrollMinY2 = 0;
            this.zgcGraph.Size = new System.Drawing.Size(536, 338);
            this.zgcGraph.TabIndex = 7;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(789, 431);
            this.Controls.Add(this.splitContainer1);
            this.MinimumSize = new System.Drawing.Size(734, 469);
            this.Name = "MainForm";
            this.Text = "Wet Film Monitor";
            grpDataSource.ResumeLayout(false);
            grpDataSource.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Button btnSelectDataSource;
        private System.Windows.Forms.Button btnDataAnalysisSettings;
        private System.Windows.Forms.Button btnDataSavingSettings;
        private System.Windows.Forms.CheckBox chkAutoSaveData;
        private System.Windows.Forms.Button btnConnect;
        private System.Windows.Forms.Button btnSensorSettings;
        private System.Windows.Forms.TextBox txtDisplay;
        private ZedGraph.ZedGraphControl zgcGraph;
        private System.Windows.Forms.RadioButton radDataSourceFile;
        private System.Windows.Forms.RadioButton radDataSourceSensor;
        private System.Windows.Forms.Button btnClose;

    }
}

