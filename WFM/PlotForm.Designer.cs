﻿namespace WFM
{
    partial class PlotForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.zgcPlot = new ZedGraph.ZedGraphControl();
            this.SuspendLayout();
            // 
            // zgcPlot
            // 
            this.zgcPlot.Dock = System.Windows.Forms.DockStyle.Fill;
            this.zgcPlot.Location = new System.Drawing.Point(0, 0);
            this.zgcPlot.Name = "zgcPlot";
            this.zgcPlot.ScrollGrace = 0;
            this.zgcPlot.ScrollMaxX = 0;
            this.zgcPlot.ScrollMaxY = 0;
            this.zgcPlot.ScrollMaxY2 = 0;
            this.zgcPlot.ScrollMinX = 0;
            this.zgcPlot.ScrollMinY = 0;
            this.zgcPlot.ScrollMinY2 = 0;
            this.zgcPlot.Size = new System.Drawing.Size(558, 463);
            this.zgcPlot.TabIndex = 0;
            // 
            // PlotForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(558, 463);
            this.Controls.Add(this.zgcPlot);
            this.Name = "PlotForm";
            this.Text = "Plot";
            this.ResumeLayout(false);

        }

        #endregion

        private ZedGraph.ZedGraphControl zgcPlot;


    }
}