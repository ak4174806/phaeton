﻿using System;
using System.Collections.Generic;
using System.Text;

using System.Windows.Forms;

namespace WFM.SettingsControls
{
    class NumUpDownSettingsControl : SettingsControl
    {
        public NumUpDownSettingsControl(Control control)
            : base(control)
        {
            NumericUpDown c = control as NumericUpDown;
            c.ValueChanged += ValueChangedTrigger;
        }

        public NumUpDownSettingsControl(Control control, string value)
            : base(control, value)
        {
            NumericUpDown c = control as NumericUpDown;
            c.ValueChanged += ValueChangedTrigger;
        }

        public override string GetValue()
        {
            NumericUpDown c = control as NumericUpDown;
            return c.Value.ToString();
        }

        public override void SetValue(string value)
        {
            NumericUpDown c = control as NumericUpDown;
            c.Value = Convert.ToDecimal(value);
        }
    }
}
