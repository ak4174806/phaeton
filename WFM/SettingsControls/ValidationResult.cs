﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WFM.SettingsControls
{
    public class ValidationResult
    {
        public ValidationResult(bool isValid)
        {
            IsValid = isValid;
        }

        public ValidationResult(bool isValid, string message)
        {
            IsValid = isValid;
            Message = message;
        }

        protected bool isValid;
        /// <summary>
        /// Whether the control was deemed valid
        /// </summary>
        public bool IsValid
        {
            get { return isValid; }
            set { isValid = value; }
        }

        protected string message;
        /// <summary>
        /// The error message if the control was deemed invalid
        /// </summary>
        public string Message
        {
            get { return message; }
            set { message = value; }
        }
    }
}
