﻿using System;
using System.Collections.Generic;
using System.Text;

using System.Windows.Forms;

namespace WFM.SettingsControls
{
    abstract public class SettingsControl
    {
        protected Control control;
        protected string savedValue;

        public delegate void ValueChangedDelegate();
        public event ValueChangedDelegate ValueChanged;

        public delegate ValidationResult ValidatingDelegate();
        public event ValidatingDelegate Validating;
        
        protected void ValueChangedTrigger(object sender, EventArgs e) 
        {
            if (GetValue() != GetSavedValue())
            {
                if (ValueChanged != null)
                {
                    ValueChanged();
                }
            }
        }
        
        public SettingsControl(Control control)
        {
            this.control = control;
        }

        public SettingsControl(Control control, string value)
        {
            this.control = control;
            SetSavedValue(savedValue);
            SetValue(savedValue);
        }

        public Control GetControl()
        {
            return control;
        }

        public string GetSavedValue()
        {
            return savedValue;
        }

        public void SetSavedValue(string value)
        {
            savedValue = value;
        }
                
        public void Save()
        {
            SetSavedValue(GetValue());
        }

        public void Cancel()
        {
            if (GetSavedValue() != null)
                SetValue(GetSavedValue());
        }
        
        public ValidationResult ValidateValue()
        {
            if (Validating != null)
            {
                return Validating();
            }

            return new ValidationResult(true);
        }


        abstract public string GetValue();

        abstract public void SetValue(string value);
    }
}
