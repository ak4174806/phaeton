﻿using System;
using System.Collections.Generic;
using System.Text;

using System.Windows.Forms;

namespace WFM.SettingsControls
{
    class TextboxSettingsControl : SettingsControl
    {
        public TextboxSettingsControl(Control control)
            : base(control) 
        {
            control.TextChanged += ValueChangedTrigger;
        }

        public TextboxSettingsControl(Control control, string value)
            : base(control, value)
        {
            control.TextChanged += ValueChangedTrigger;
        }

        public override string GetValue()
        {
            return control.Text;
        }

        public override void SetValue(string value)
        {
            control.Text = value;
        }
    }
}
