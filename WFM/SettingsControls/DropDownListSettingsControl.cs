﻿using System;
using System.Collections.Generic;
using System.Text;

using System.Windows.Forms;

namespace WFM.SettingsControls
{
    class DropDownListSettingsControl : SettingsControl
    {
        public DropDownListSettingsControl(Control control)
            : base(control)
        {
            ComboBox c = control as ComboBox; 
            c.SelectedIndexChanged += ValueChangedTrigger;
        }

        public DropDownListSettingsControl(Control control, string value)
            : base(control, value)
        {
            ComboBox c = control as ComboBox;
            c.SelectedIndexChanged += ValueChangedTrigger;
        }

        public override string GetValue()
        {
            ComboBox c = control as ComboBox;
            return c.SelectedItem.ToString();
        }

        public override void SetValue(string value)
        {
            ComboBox c = control as ComboBox;
            for (int i = 0; i < c.Items.Count; i++)
            {
                if (c.Items[i].ToString() == value)
                {
                    c.SelectedIndex = i;
                    return;
                }
            }

            throw new ArgumentOutOfRangeException("value");
        }
    }
}
