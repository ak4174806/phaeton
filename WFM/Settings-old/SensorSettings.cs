﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WFM.Settings
{
    public class SensorSettings : ISettings
    {
        protected string sensorType;
        protected string sensorModel;
        protected string sensorPort;
        protected int sensorBaudRate;

        public string SensorType
        {
            get { return sensorType; }
            set { sensorType = value; }
        }

        public string SensorModel
        {
            get { return sensorModel; }
            set { sensorModel = value; }
        }

        public string SensorPort
        {
            get { return sensorPort; }
            set { sensorPort = value; }
        }

        public int SensorBaudRate
        {
            get { return sensorBaudRate; }
            set { sensorBaudRate = value; }
        }

        #region ISettings Members

        public bool Validate(out SettingsErrors[] errors)
        {
            throw new NotImplementedException();
            return true;
        }

        #endregion
    }
}
