﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WFM.Settings
{
    public class SettingsErrors
    {
        private string setting;
        private string message;
        private SettingsErrorLevels errorLevel;

        public string Setting
        {
            get { return setting; }
            set { setting = value; }
        }

        public string Message
        {
            get { return message; }
            set { message = value; }
        }

        public SettingsErrorLevels ErrorLevel
        {
            get { return errorLevel; }
            set { errorLevel = value; }
        }
    }
}
