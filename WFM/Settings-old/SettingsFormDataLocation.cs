﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WFM.Settings
{
    public enum SettingsFormDataLocation
    {
        Saved,
        CurrentForm
    }
}
