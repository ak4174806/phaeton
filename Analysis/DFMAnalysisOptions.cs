﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Analysis
{
    public class DFMAnalysisOptions
    {
        private double thresholdParameter;
        public double ThresholdParameter
        {
            get { return thresholdParameter; }
            set { thresholdParameter = value; }
        }

        private int smoothingWidth;
        public int SmoothingWidth
        {
            get { return smoothingWidth; }
            set { smoothingWidth = value; }
        }

        private int minPitWidth;
        public int MinPitWidth
        {
            get { return minPitWidth; }
            set { minPitWidth = value; }
        }

        private double compression;
        public double Compression
        {
            get { return compression; }
            set { compression = value; }
        }

        private double substrateRoughness;
        public double SubstrateRoughness
        {
            get { return substrateRoughness; }
            set { substrateRoughness = value; }
        }

        // the number of points in the pit to average
        private int pitPoints;
        public int PitPoints
        {
            get { return pitPoints; }
            set { pitPoints = value; }
        }
    }
}
