﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace Analysis
{
    public class DFMAnalysis
    {
        private DFMAnalysisOptions options;
        private DFMData data;

        public DFMAnalysis()
        {
            this.data = new DFMData();
            this.options = new DFMAnalysisOptions();
        }

        #region Properties

        /// <summary>
        /// The parameters used to analyse the data
        /// </summary>
        public DFMAnalysisOptions Options
        {
            get { return options; }
            set { options = value; }
        }

        /// <summary>
        /// Stores data and results
        /// </summary>
        public DFMData Data
        {
            get { return data; }
        }

        #endregion
        
        /// <summary>
        /// Sets the data 
        /// </summary>
        /// <param name="data_x"></param>
        /// <param name="data_y"></param>
        public void SetData(double[] data_x, double[] data_y)
        {
            // all sensor data is in mm, want to work in microns
            // hence scale by 1000 below

            ClearData();

            // skip first 20 data points
            int length = data_x.Length - 20;
            if (length <= 0)
            {
                return;
            }
            
            Data.Data_X = new double[length];
            Data.Data_Y = new double[length];
            Data.Length = length;

            for (int i = 0; i < length; i++)
            {
                //Data.Data_X[i] = i + 1;
                Data.Data_X[i] = data_x[i + 20];
                Data.Data_Y[i] = data_y[i + 20] * 1000;
            }
        }

        /// <summary>
        /// Clears all data stored
        /// </summary>
        public void ClearData()
        {
            Data.Data_X = null;
            Data.Data_Y = null;
	    	data = new DFMData();
        }

        /// <summary>
        /// Analyses data and stores results in Data
        /// </summary>
        public void Analyse()
        {
            if (Data.Data_X == null)
            {
                return;
            }

            // smooth data with a quadratic Savitsky-Golay low-pass filter of order up to SmoothingWidth
            // (ie fits a quadratic to the (2*Width+1) points centred on each data point
            // smaller widths are used near the ends of the data

            int width = Options.SmoothingWidth;

            int length = Data.Length;
            double A, B;
            int k;

            // unweighted smoothing
            double[] uw_smoothed = new double[length];
            for (int i = 0; i < length; i++)
            {
                k = width;

                if (k > i) k = i;
                if (k > length - i - 1) k = length - i - 1;

                A = 2 * k - 1;
                B = 3.0 / (A * (A + 2) * (A + 4));
                A = B * (3 * k * (k + 1) - 1);
                B = 5 * B;

                uw_smoothed[i] = 0;
                for (int j = -k; j <= k; j++)
                {
                    uw_smoothed[i] += (A - B * j * j) * Data.Data_Y[i + j];
                }
            }
            // export smoothed
            Data.Unweighted_Smoothed_Y = uw_smoothed;

            // weights
            double[] weights = new double[length];
            double weight;
            for (int i = 0; i < length; i++)
            {
                weight = Options.Compression * (uw_smoothed[i] - Data.Data_Y[i]);
                // to prevent overflow/have a maximum
                if (weight > 30)
                    weight = 30;
                weights[i] = Math.Exp(weight);
            }
            Data.Weights = weights;

            // weighted smoothing
            double[] w_smoothed = new double[length];
            double weight_total = 0;
            for (int i = 0; i < length; i++)
            {
                k = width;

                if (k > i) k = i;
                if (k > length - i - 1) k = length - i - 1;

                A = 2 * k - 1;
                B = 3.0 / (A * (A + 2) * (A + 4));
                A = B * (3 * k * (k + 1) - 1);
                B = 5 * B;

                w_smoothed[i] = 0;
                weight_total = 0;
                for (int j = -k; j <= k; j++)
                {
                    weight = (A - B * j * j) * weights[i + j];
                    w_smoothed[i] += weight * Data.Data_Y[i+j];
                    weight_total += weight;
                }
                w_smoothed[i] /= weight_total;
            }
            Data.Weighted_Smoothed_Y = w_smoothed;

            // calculate normalised data, and max/min
            double normMin = 0.0, normMax = 0.0;
            double[] norm = new double[length];
            for (int i = 0; i < length; i++)
            {
                norm[i] = Data.Data_Y[i] - w_smoothed[i];
                if (i == 0)
                {
                    normMax = normMin = norm[0];
                }
                else
                {
                    if (norm[i] < normMin) normMin = norm[i];
                    if (norm[i] > normMax) normMax = norm[i];
                }
            }
            Data.Normalised_Y = norm;


            // find peaks and statistics
            List<double> peaks = new List<double>();
           // List<double> peaksIndex = new List<double>();
            int peakCount = 0;
            int peakWidth = 0;
            double peakSum = 0.0;
            double peakSumSq = 0.0;
            double peakMax = 0.0;

            // first, find peak threshold           
            double threshold = normMin + Options.ThresholdParameter * (normMax - normMin);

            // store values in this pit so we can find and average max N later
            List<double> pitValues = new List<double>();
            List<double> pitValueIndex = new List<double>();

            // now scan for peaks
            if (norm[0] > threshold) // need to separate so can access norm[i-1]
            {
                peakMax = norm[0];
                peakWidth = Options.MinPitWidth;
                pitValues.Add(norm[0]);
                pitValueIndex.Add(data.Data_X[0]);//new -- for LeftMiddleRight options
            }
            for (int i = 1; i < length; i++)
            {
                if (norm[i] > threshold)
                {
                    if ((norm[i - 1] < threshold && peakWidth <= 0) || peakMax < norm[i])
                    {
                        peakMax = norm[i];
                    }
                    peakWidth = Options.MinPitWidth;
                    pitValues.Add(norm[i]);
                 //  pitValueIndex.Add(data.Data_X[i]);//new -- for LeftMiddleRight options
                }
                else
                {
                    peakWidth--;
                }

                if (peakWidth == 0 || (i == length && peakWidth > 0))
                {
                    double output = peakMax;

                    if (Options.PitPoints <= 1)
                    {
                        // substrate roughness change
                        output -= options.SubstrateRoughness;
                    }
                    else
                    {
                        output = 0.0;

                        // find top N points in pit
                        pitValues.Sort();

                        // and average them
                        int N = pitValues.Count;
                        int L = Math.Min(N, Options.PitPoints);
                        for (int x = 0; x < L; x++)
                        {
                            output += pitValues[N-1 - x];
                        }

                        output /= L;
                        output -= options.SubstrateRoughness;
                    }

                    peakCount++;
                    peaks.Add(output);
                    pitValueIndex.Add(data.Data_X[i]);//new -- for LeftMiddleRight options
                    peakSum += output;
                    peakSumSq += output * output;

                    pitValues.Clear();
                }
            }
            A = peakSum / peakCount;
            B = peakSumSq / peakCount;

            Data.PeakCount = peakCount;
            Data.Peaks = peaks.ToArray();
            data.PeakIndex = pitValueIndex.ToArray();//new -- for LeftMiddleRight options
            Data.PeakMean = A;
            Data.PeakSD = Math.Sqrt(B - A * A);
        }
    }
}
