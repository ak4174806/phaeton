﻿using System;
using System.Collections.Generic;
using System.Text;

using System.IO;
using NLog;

namespace Analysis
{
    public class DataIO
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public enum Modes
        {
            Read,
            Write,
            ReadWrite
        }

        private string filename;
        public string Filename
        {
            get { return filename; }
            set { filename = value; }
        }

        private Modes mode;
        public Modes Mode
        {
            get { return mode; }
            set { mode = value; }
        }

        public DataIO(string filename, Modes mode)
            : this()
        {
            Filename = filename;
            Mode = mode;
        }

        public DataIO()
        { }

        public void ReadData(out double[] data_x, out double[] data_y)
        {
            if (Mode != Modes.Read && Mode != Modes.ReadWrite)
            {
                logger.Error("Attempted to read data when file was opened as " + Mode.ToString());
                throw new ApplicationException("Attempted to read data when file was opened as " + Mode.ToString());
            }

            StreamReader sr = new StreamReader(filename);
            List<double> list_x = new List<double>();
            List<double> list_y = new List<double>();

            try
            {
                string[] line;

                while (!sr.EndOfStream)
                {
                    line = sr.ReadLine().Split(',');
                    if (line.Length > 0)
                    {
                        list_x.Add(Convert.ToDouble(line[0]));
                        list_y.Add(Convert.ToDouble(line[1]));
                    }
                }

                data_x = list_x.ToArray();
                data_y = list_y.ToArray();
            }
            catch (Exception ex)
            {
                logger.ErrorException("Could not read from '" + filename + "': " + ex.Message, ex);
                throw new ApplicationException("Could not read from '" + filename + "': " + ex.Message);
            }
            finally
            {
                sr.Close();
            }
        }

        public void WriteData(double[] data_x, double[] data_y)
        {
            logger.Trace("In write data");

            if (Mode != Modes.Write && Mode != Modes.ReadWrite)
            {
                logger.Error("Attempted to write data when file was opened as " + Mode.ToString());
                throw new ApplicationException("Attempted to write data when file was opened as " + Mode.ToString());
            }

            // open file
            StreamWriter sw = null;
            try
            {
                sw = new StreamWriter(filename, false);
                
                // no header, no errors
                for (int i = 0; i < data_x.Length; i++)
                {
                    sw.WriteLine(data_x[i].ToString("0.0000") + "," + data_y[i].ToString("0.00000000"));
                }
                
                sw.Flush();
                sw.Close();
            }
            catch (Exception ex)
            {
                logger.ErrorException("Could not write to '" + filename + "': " + ex.Message, ex);
                throw new ApplicationException("Could not write to '" + filename + "': " + ex.Message);
            }
            finally
            {
                if (sw != null)
                    sw.Close();
            }
        }
    }
}
