﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Analysis
{
    public class PVSData
    {
        public int Length;

        public double[] Data_X; // raw x-values
        public double[] Centred_X; // centred x-values
        public double[] Normalised_X; // centred and normalised width of 100
        
        public double[] Data_Y;
        public double[] Smoothed_Y;
        public double[] Diff_Y;

		public double[] Norm_Data_Y;
        public double[] Norm_Smoothed_Y;
	    
        // 0 and 1 are left baseline
        // 3 and 4 are feature
        // 6 and 7 are right baseline
        // 2 and 5 are left/right edge
        public int[] BaselineSpec = new int[8];

        // analysis details
        public bool IsRejected = false;
        public bool IsWarning = true;

        // possible statuses of each check
        public enum CheckStatus
        {
            Valid,
            Warning,
            Rejected
        }

        // each check applied to data
        public struct Check
        {
            public string ID;
            public CheckStatus Status;
            public double Value;
            public string Message;
            public double Min;
            public double Max;            
        }

        // the results of all the checks applied
        public Dictionary<string, Check> Checks;


        public double MeanThickness;
        public double SDThickness;
        public double MaxThickness;

        public double Volume;
        public double FeatureWidth;
    }
}
