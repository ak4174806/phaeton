﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Analysis
{
    public class DFMData
    {
        public int Length;

        public double[] Data_X; // raw x-values
        //public double[] Centred_X; // centred x-values
        //public double[] Normalised_X; // centred and normalised width of 100

        public double[] Data_Y;
        public double[] Unweighted_Smoothed_Y;
        public double[] Weights;
        public double[] Weighted_Smoothed_Y;
        public double[] Normalised_Y;

        public int PeakCount;
        public double[] Peaks;
        public double[] PeakIndex;
        public double PeakMean;
        public double PeakSD;
    }
}
