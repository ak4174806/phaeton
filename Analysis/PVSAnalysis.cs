﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Analysis
{
    public class PVSAnalysis
    {
        private PVSAnalysisOptions options;

        // vertical inversion to get feature right way up
        private bool verticallyInverted;
        private States state;

        private PVSData data;

        // horizontal inversion to align wet and dry
        private bool horizontallyInverted;

        public PVSAnalysis(bool horizontallyInverted, bool verticallyInverted)
        {
            this.verticallyInverted = verticallyInverted;
            this.horizontallyInverted = horizontallyInverted;
            this.data = new PVSData();
            this.options = new PVSAnalysisOptions();

            state = States.NoData;
        }

        public PVSAnalysis(bool horizontallyInverted) 
            : this(horizontallyInverted, true)
        { }

        #region Enums

        /// <summary>
        /// The state of analysis
        /// </summary>
        public enum States
        {
            NoData,
            DataLoaded,
            DataAnalysed
        }

        #endregion

        #region Properties

        /// <summary>
        /// The parameters used to analyse the data
        /// </summary>
        public PVSAnalysisOptions Options
        {
            get { return options; }
            set 
            { 
                options = value;

                if (State == States.DataAnalysed)
                {
                    Analyse();
                }
            }
        }
        
        /// <summary>
        /// The state of analysis
        /// </summary>
        public States State
        {
            get { return state; }
        }

        /// <summary>
        /// Stores data and results
        /// </summary>
        public PVSData Data
        {
            get { return data; }
        }

        public bool HorizontallyInverted
        {
            get { return horizontallyInverted; }
            set
            {
                bool old = horizontallyInverted;
                horizontallyInverted = value;

                // re-analyse if changed
                if (old != horizontallyInverted)
                {
                    if (State != States.NoData)
                    {
                        Array.Reverse(Data.Data_Y);
                    }

                    if (State == States.DataAnalysed)
                    {
                        Analyse();
                    }
                }
            }
        }
                
        /// <summary>
        /// Whether the data file should be vertically inverted, if changed, will re-analyse data. Gets updated automatically if wrong way when analysing
        /// </summary>
        public bool VerticallyInverted
        {
            get { return verticallyInverted; }
            set
            {
                bool old = verticallyInverted;
                verticallyInverted = value;

                // re-analyse if changed
                if (old != verticallyInverted)
                {
                    if (State != States.NoData)
                    {
                        for (int i = 0, length = Data.Data_Y.Length; i < length; i++)
                        {
                            Data.Data_Y[i] *= -1;
                        }
                    }

                    if (State == States.DataAnalysed)
                    {
                        Analyse();
                    }
                }
            }
        }

        #endregion
        
        /// <summary>
        /// Sets the data 
        /// </summary>
        /// <param name="data_x"></param>
        /// <param name="data_y"></param>
        public void SetData(double[] data_x, double[] data_y)
        {
            ClearData();

            // set scaled and vertically inverted? data
            int length = data_x.Length;
            Data.Data_X = new double[length];
            Data.Data_Y = new double[length];
            Data.Length = length;

            double y_scale = 1000;
            if (VerticallyInverted)
                y_scale *= -1;

            bool hInvert = HorizontallyInverted;

            // replace x with i/100, scale y and invert if necessary
            for (int i = 0; i < length; i++)
            {
                Data.Data_X[i] = 0.01 * (i + 1); // if change scale here, change normalisation in analyseVolume
                if (!hInvert)
                    Data.Data_Y[i] = data_y[i] * y_scale;
                else
                    Data.Data_Y[length - i - 1] = data_y[i] * y_scale;
            }
            
            state = States.DataLoaded;
        }

        /// <summary>
        /// Clears all data stored
        /// </summary>
        public void ClearData()
        {
            Data.Data_X = null;
            Data.Data_Y = null;
	    	data = new PVSData();

            state = States.NoData;
        }

        /// <summary>
        /// Analyses data and stores results in Data
        /// </summary>
        public void Analyse()
        {
            // smooth data with a quadratic Savitsky-Golay low-pass filter of order up to SmoothingWidth
            // (ie fits a quadratic to the (2*Width+1) points centred on each data point
            // smaller widths are used near the ends of the data

            int width = Options.SmoothingWidth;

            int length = Data.Length;
            double A, B;
            int k;

            double[] smoothed = new double[length];
            for (int i = 0; i < length; i++)
            {
                k = width;

                if (k > i) k = i;
                if (k > length - i - 1) k = length - i - 1;

                A = 2 * k - 1;
                B = 3.0 / (A * (A + 2) * (A + 4));
                A = B * (3 * k * (k + 1) - 1);
                B = 5 * B;

                smoothed[i] = 0;
                for (int j = -k; j <= k; j++)
                {
                    smoothed[i] += (A - B * j * j) * Data.Data_Y[i + j];
                }
            }
            // export smoothed
            Data.Smoothed_Y = smoothed;

            // differentiate smoothed data
            double[] diff = new double[length];
            diff[0] = diff[length - 1] = 0;

            for (int i = 1; i < length - 1; i++)
            {
                k = width;

                if (k > i) k = i;
                if (k > length - i - 1) k = length - i - 1;

                B = 3.0 / (k * (k + 1) * (2 * k + 1)); // avoid integer division as k is an int
                diff[i] = 0;
                for (int j = -k; j <= k; j++)
                {
                    diff[i] += j * Data.Data_Y[i + j];
                }
                diff[i] *= B;
            }
            // flatten (crop) ends of data range for diff
            int halfWidth = width / 2;
            for (int i = 0; i < halfWidth; i++)
            {
                diff[i] = diff[halfWidth - 1]; // changed from diff[halfWidth] ...
                diff[length - i - 1] = diff[length - halfWidth]; // and diff[length - halfWidth - 1] to match excel program
            }
            // export diff
            Data.Diff_Y = diff;

            // find derivative extrema
            int lowEdge = 0, highEdge = 0;
            double min, max;
            min = max = diff[0];
            for (int i = 1; i < length; i++)
            {
                if (diff[i] < min)
                {
                    highEdge = i;
                    min = diff[i];
                }
                if (diff[i] > max)
                {
                    lowEdge = i;
                    max = diff[i];
                }
            }

            // work out baseline stuff

            // check for data inversion or multiple peaks
            if (highEdge <= lowEdge)
            {
                for (int i = 0; i < 8; i++)
                {
                    Data.BaselineSpec[i] = 0;
                }

                Data.BaselineSpec[2] = lowEdge;
                Data.BaselineSpec[5] = highEdge;
            }
            else
            {
                // preliminary normalisation of derivative
                double check = (highEdge - lowEdge) / 2 + 1;
                
                Data.BaselineSpec[0] = (int)(lowEdge - check);
                if (Data.BaselineSpec[0] < 0)
                    Data.BaselineSpec[0] = 0;

                Data.BaselineSpec[1] = Data.BaselineSpec[2] = lowEdge;
                Data.BaselineSpec[5] = Data.BaselineSpec[6] = highEdge;

                Data.BaselineSpec[7] = (int)(highEdge + check - 1);
                if (Data.BaselineSpec[7] > length - 1)
                    Data.BaselineSpec[7] = length - 1;

                double meanDerivative = 0.0;
                int j = 0;
                for (int i = Data.BaselineSpec[0]; i <= Data.BaselineSpec[1]; i++)
                {
                    meanDerivative += diff[i];
                    j++;
                }
                for (int i = Data.BaselineSpec[6]; i <= Data.BaselineSpec[7]; i++)
                {
                    meanDerivative += diff[i];
                    j++;
                }
                meanDerivative /= j;

                // use normalised derivative values to improve baseline specs
                // exclude data for which the magnitude of the derivative
                // is greater than ThresholdParameter * derivative extemum
                A = meanDerivative;
                max = A + (max - A) * (double)Options.ThresholdParameter;
                min = A + (min - A) * (double)Options.ThresholdParameter;

                j = Data.BaselineSpec[2];
                while (j > Data.BaselineSpec[0] && diff[j] > max)
                    j--;
                Data.BaselineSpec[1] = j;

                j = Data.BaselineSpec[2];
                while (j < Data.BaselineSpec[5] && diff[j] > max)
                    j++;
                Data.BaselineSpec[3] = j;

                j = Data.BaselineSpec[5];
                while (j > Data.BaselineSpec[2] && diff[j] < min)
                    j--;
                Data.BaselineSpec[4] = j;

                j = Data.BaselineSpec[5];
                while (j < Data.BaselineSpec[7] && diff[j] < min)
                    j++;
                Data.BaselineSpec[6] = j;
            }

            // analyse the volume
            analyseVolume();
        }


        private void analyseVolume()
        {
            // copy so can modify, and easier to type/read
            double[] data_x = (double[])Data.Data_X.Clone();
            double[] data = (double[])Data.Data_Y.Clone();
            double[] smoothed = (double[])Data.Smoothed_Y.Clone();
            double[] diff = (double[])Data.Diff_Y.Clone();

            int[] baselineSpec = (int[])Data.BaselineSpec.Clone();
            int length = Data.Length;

            // stuff to find and then copy into Data
            bool isWarning = false;
            bool isRejected = false;
            Dictionary<string, PVSData.Check> checks = new Dictionary<string, PVSData.Check>();
            
            
            // check for data inversion/multiple peaks
            int featureWidth = baselineSpec[5] - baselineSpec[2]; // 1 is magic, to match excel
            Data.FeatureWidth = featureWidth;
            
            PVSData.Check check;
            
            if (featureWidth <= 0)
            {
                // automatically invert if inverted
                VerticallyInverted = !VerticallyInverted;

                // re-analyse
                Analyse();
            }
            else
            {
                // check for feature width
                check = new PVSData.Check();
                check.ID = "FeatureWidth";
                check.Status = PVSData.CheckStatus.Valid;
                check.Min = 2 * Options.SmoothingWidth;
                check.Value = featureWidth;

                if (check.Value < check.Min)
                {
                    // feature is too narrow - warn
                    isWarning = true;
                    check.Status = PVSData.CheckStatus.Warning;
                    check.Message = "Feature too narrow. Feature should be wider than " + check.Min.ToString();
                }
                checks.Add(check.ID, check);


                // reduced width for checking baselines on each side
                int reducedWidth = featureWidth / 3 + 1;


                // check for adequate left baseline width
                check = new PVSData.Check();
                check.ID = "LeftBaselineWidth";
                check.Status = PVSData.CheckStatus.Valid;
                check.Min = reducedWidth;
                check.Value = baselineSpec[1] - baselineSpec[0];

                if (check.Value < check.Min)
                {
                    // left baseline too narrow - warn
                    isWarning = true;
                    check.Status = PVSData.CheckStatus.Warning;
                    check.Message = "Left baseline too narrow. Baseline should be wider than " + check.Min.ToString();
                }
                checks.Add(check.ID, check);


                // check for adequate right baseline width
                check = new PVSData.Check();
                check.ID = "RightBaselineWidth";
                check.Status = PVSData.CheckStatus.Valid;
                check.Min = reducedWidth;
                check.Value = baselineSpec[7] - baselineSpec[6];

                if (check.Value < check.Min)
                {
                    // right baseline too narrow - warn
                    isWarning = true;
                    check.Status = PVSData.CheckStatus.Warning;
                    check.Message = "Right baseline too narrow. Baseline should be wider than " + check.Min.ToString();
                }
                checks.Add(check.ID, check);


                // normalise linear trend in Data, Smoothed and Diff based on linear fit of baseline
                double slope, intercept;
                double sum_x = 0, sum_y = 0, sum_xx = 0, sum_xy = 0;

                // calculate moments
                int j = 0;
                for (int i = baselineSpec[0]; i <= baselineSpec[1]; i++)
                {
                    sum_x += data_x[i];
                    sum_y += data[i];
                    sum_xx += data_x[i] * data_x[i];
                    sum_xy += data_x[i] * data[i];
                    j++;
                }
                for (int i = baselineSpec[6]; i <= baselineSpec[7]; i++)
                {
                    sum_x += data_x[i];
                    sum_y += data[i];
                    sum_xx += data_x[i] * data_x[i];
                    sum_xy += data_x[i] * data[i];
                    j++;
                }
                // convert to means
                sum_x /= j;
                sum_y /= j;
                sum_xx /= j;
                sum_xy /= j;

                // calculate linear fit
                slope = (sum_xy - sum_x * sum_y) / (sum_xx - sum_x * sum_x);
                intercept = (sum_xx * sum_y - sum_xy * sum_x) / (sum_xx - sum_x * sum_x);

                // create x-value's for smoothed & diff - exist in excel version, used to centre graph at 0
                Data.Centred_X = new double[length];
                Data.Normalised_X = new double[length];

                double x_offset = (baselineSpec[6] + baselineSpec[1]) / 2;
                double x_scale = (double)100.0 / (baselineSpec[6] - baselineSpec[1]);
                // normalise data, smoothed data and derivate by subtracting linear fit
                for (int i = 0; i < length; i++)
                {
                    data[i] -= intercept + slope * data_x[i];
                    smoothed[i] -= intercept + slope * data_x[i];
                    diff[i] -= slope * 0.01; // as x_scale in SetData is 0.01 = derivative of data_x

                    // centred x-values
                    Data.Centred_X[i] = (i - x_offset) * 0.01; // as x_scale in SetData is 0.01
                    // centred and normalised to a width of 100 x-values
                    Data.Normalised_X[i] = (i - x_offset) * x_scale;
                }

				// export normalised data - used in graphs
                Data.Norm_Data_Y = (double[])data.Clone();
                Data.Norm_Smoothed_Y = (double[])smoothed.Clone();

                // check for adequate left feature profile
                check = new PVSData.Check();
                check.ID = "LeftFeatureProfile";
                check.Status = PVSData.CheckStatus.Valid;
                check.Value = smoothed[baselineSpec[1]];

                double thresholdParameter = (double)Options.ThresholdParameter;
                double max = smoothed[baselineSpec[2]]
                    * (1 + Options.A) * thresholdParameter * thresholdParameter 
                    / (1 + Options.A * thresholdParameter * thresholdParameter);
                max += Options.C;
                check.Max = max;

                double min = smoothed[baselineSpec[2]]
                    * (1 - Options.A) * thresholdParameter * thresholdParameter
                    / (1 + Options.A * thresholdParameter * thresholdParameter);
                min -= Options.C;
                check.Min = min;

                if (check.Value < min || check.Value > max)
                {
                    // left feature profile unsuitable - reject
                    isRejected = true;
                    check.Status = PVSData.CheckStatus.Rejected;
                    check.Message = "Left feature profile unsuitable. Value at foot of profile should be between " + min.ToString() + " and " + max.ToString();
                }
                checks.Add(check.ID, check);


                // check for adequate right feature profile
                check = new PVSData.Check();
                check.ID = "RightFeatureProfile";
                check.Status = PVSData.CheckStatus.Valid;
                check.Value = smoothed[baselineSpec[6]];

                max = smoothed[baselineSpec[5]]
                    * (1 + Options.A) * thresholdParameter * thresholdParameter
                    / (1 + Options.A * thresholdParameter * thresholdParameter);
                max += Options.C;
                check.Max = max;

                min = smoothed[baselineSpec[5]]
                    * (1 - Options.A) * thresholdParameter * thresholdParameter
                    / (1 + Options.A * thresholdParameter * thresholdParameter);
                min -= Options.C;
                check.Min = min;

                if (check.Value < min || check.Value > max)
                {
                    // right feature profile unsuitable - reject
                    isRejected = true;
                    check.Status = PVSData.CheckStatus.Rejected;
                    check.Message = "Right feature profile unsuitable. Value at foot of profile should be between " + min.ToString() + " and " + max.ToString();
                }
                checks.Add(check.ID, check);


                // evaluate curvature (bow) of baseline
                // estimated the error in the thickness due to a bow in the substrate
                sum_x = sum_y = sum_xx = sum_xy = 0;

                // fit a linear fit to diff
                j = 0;
                for (int i = baselineSpec[0]; i <= baselineSpec[1]; i++)
                {
                    sum_x += i;
                    sum_y += diff[i];
                    sum_xx += i * i;
                    sum_xy += i * diff[i];
                    j++;
                }
                for (int i = baselineSpec[6]; i <= baselineSpec[7]; i++)
                {
                    sum_x += i;
                    sum_y += diff[i];
                    sum_xx += i * i;
                    sum_xy += i * diff[i];
                    j++;
                }
                // convert to means
                sum_x /= j;
                sum_y /= j;
                sum_xx /= j;
                sum_xy /= j;

                // calculate linear fit, nb: slope is a double derivative
                slope = (sum_xy - sum_x * sum_y) / (sum_xx - sum_x * sum_x);
                intercept = (sum_xx * sum_y - sum_xy * sum_x) / (sum_xx - sum_x * sum_x);

                double B = baselineSpec[7] + baselineSpec[6] - baselineSpec[1] - baselineSpec[0]; // todo: check this
                double A = slope * B * B / 32;

                // check for excessive substrate bow
                check = new PVSData.Check();
                check.ID = "SubstrateBow";
                check.Status = PVSData.CheckStatus.Valid;
                check.Value = A;
                check.Max = Options.B;

                if (Math.Abs(A) > check.Max)
                {
                    // excessive substrate bow - reject
                    isRejected = true;
                    check.Status = PVSData.CheckStatus.Rejected;
                    check.Message = "Excessive substrate bow. Estimated error in measured thickness exceeds " + check.Max.ToString();
                }
                checks.Add(check.ID, check);


                // calculuate mean feature thickness and thickness range
                min = max = smoothed[baselineSpec[3]];
                sum_y = 0;
                double sum_yy = 0;
                j = 0;

                for (int i = baselineSpec[3] + 1; i <= baselineSpec[4]; i++)
                {
                    if (smoothed[i] < min)
                        min = smoothed[i];
                    if (smoothed[i] > max)
                        max = smoothed[i];

                    sum_y += data[i];
                    sum_yy += data[i] * data[i];
                    j++;
                }
                sum_y /= j;
                sum_yy /= j;
                                
                // export mean & sd
                Data.MeanThickness = sum_y;
                Data.SDThickness = Math.Sqrt(sum_yy - sum_y * sum_y);
                Data.MaxThickness = max - min;

                // calculate volume
                double volume = 0;
                for (int i = baselineSpec[1]; i <= baselineSpec[6]; i++)
                {
                    volume += data[i];
                }
                // multiply by 0.01 to scale it
                Data.Volume = volume * 0.01;


                // check thickness variation
                check = new PVSData.Check();
                check.ID = "ThicknessVariation1";
                check.Status = PVSData.CheckStatus.Valid;
                check.Value = Data.MaxThickness;
                check.Max = Options.D;

                if (check.Value > check.Max)
                {
                    // excessive thickness - warn
                    isWarning = true;
                    check.Status = PVSData.CheckStatus.Warning;
                    check.Message = "Thickness variation exceeds " + check.Max.ToString();
                }
                checks.Add(check.ID, check);

                // check another aspect of thickness variation
                check = new PVSData.Check();
                check.ID = "ThicknessVariation2";
                check.Status = PVSData.CheckStatus.Valid;
                check.Value = Data.MaxThickness;
                check.Max = 0.5 * max; // max = maximum value of normalised smoothed

                if (check.Value > check.Max)
                {
                    // excessive thickness - reject
                    isRejected = true;
                    check.Status = PVSData.CheckStatus.Rejected;
                    check.Message = "Thickness variation exceeds " + check.Max.ToString();
                }
                checks.Add(check.ID, check);


                // check sd of smoothed data over (1) baseline and (2) feature
                check = new PVSData.Check();
                check.ID = "BaselineRoughness";
                check.Status = PVSData.CheckStatus.Valid;
                
                for (int i = 0; i < length; i++)
                {
                    data[i] -= smoothed[i]; // how rough data was
                }

                sum_y = sum_yy = 0;
                j = 0;
                for (int i = baselineSpec[0]; i <= baselineSpec[1]; i++)
                {
                    sum_y += data[i];
                    sum_yy += data[i] * data[i];
                    j++;
                }
                for (int i = baselineSpec[6]; i <= baselineSpec[7]; i++)
                {
                    sum_y += data[i];
                    sum_yy += data[i] * data[i];
                    j++;
                }
                sum_y /= j;
                sum_yy /= j;

                check.Value = Math.Sqrt(sum_yy - sum_y * sum_y);
                check.Max = Options.C;
                if (check.Value > check.Max)
                {
                    // excessive baseline roughness - warn
                    isWarning = true;
                    check.Status = PVSData.CheckStatus.Warning;
                    check.Message = "Baseline roughness exceeds " + check.Max.ToString();
                }
                checks.Add(check.ID, check);


                // now check sd over feature
                check = new PVSData.Check();
                check.ID = "FeatureRoughness";
                check.Status = PVSData.CheckStatus.Valid;
                
                sum_y = sum_yy = 0;
                j = 0;
                for (int i = baselineSpec[3]; i <= baselineSpec[4]; i++)
                {
                    sum_y += data[i];
                    sum_yy += data[i] * data[i];
                    j++;
                }
                sum_y /= j;
                sum_yy /= j;

                check.Value = Math.Sqrt(sum_yy - sum_y * sum_y);
                check.Max = Options.C;
                if (check.Value > check.Max)
                {
                    // excessive feature roughness - warn
                    isWarning = true;
                    check.Status = PVSData.CheckStatus.Warning;
                    check.Message = "Feature roughness exceeds " + check.Max.ToString();
                }
                checks.Add(check.ID, check);
            }
            
            // export stuff
            Data.Checks = checks;
            Data.IsRejected = isRejected;
            Data.IsWarning = isWarning;

            // mark state
            state = States.DataAnalysed;
        }
    }
}
