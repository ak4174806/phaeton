﻿using System;
using System.Collections.Generic;
using System.Text;

using System.Xml;
using PaintAnalysis.Common;

namespace Analysis
{
    public class PVSAnalysisOptions : IOptionsGroup
    {
        private decimal thresholdParameter;
        public decimal ThresholdParameter
        {
            get { return thresholdParameter; }
            set { thresholdParameter = value; }
        }

        private int smoothingWidth;
        public int SmoothingWidth
        {
            get { return smoothingWidth; }
            set { smoothingWidth = value; }
        }

        private double a;
        public double A
        {
            get { return a; }
            set { a = value; }
        }

        private double b;
        public double B
        {
            get { return b; }
            set { b = value; }
        }

        private double c;
        public double C
        {
            get { return c; }
            set { c = value; }
        }

        private double d;
        public double D
        {
            get { return d; }
            set { d = value; }
        }

        private double thermalFactor;
        public double ThermalFactor
        {
            get { return thermalFactor; }
            set { thermalFactor = value; }
        }

        private double calibrationFactor;
        public double CalibrationFactor
        {
            get { return calibrationFactor; }
            set { calibrationFactor = value; }
        }

        #region Interface Stuff
        public IOptionsGroup DeepCopy()
        {
            return (IOptionsGroup)MemberwiseClone();
        }

        public System.Xml.Schema.XmlSchema GetSchema() { return null; }

        public void ReadXml(System.Xml.XmlReader reader)
        {
            if (reader.MoveToContent() == XmlNodeType.Element && reader.LocalName == "PVSAnalysisOptions")
            {
                ThresholdParameter = Convert.ToDecimal(reader["ThresholdParameter"]);
                SmoothingWidth = Convert.ToInt32(reader["SmoothingWidth"]);
                A = Convert.ToDouble(reader["A"]);
                B = Convert.ToDouble(reader["B"]);
                C = Convert.ToDouble(reader["C"]);
                D = Convert.ToDouble(reader["D"]);
                ThermalFactor = Convert.ToDouble(reader["ThermalFactor"]);
                CalibrationFactor = Convert.ToDouble(reader["CalibrationFactor"]);

                reader.Read();
            }
        }

        public void WriteXml(System.Xml.XmlWriter writer)
        {
            writer.WriteAttributeString("ThresholdParameter", ThresholdParameter.ToString());
            writer.WriteAttributeString("SmoothingWidth", SmoothingWidth.ToString());
            writer.WriteAttributeString("A", A.ToString());
            writer.WriteAttributeString("B", B.ToString());
            writer.WriteAttributeString("C", C.ToString());
            writer.WriteAttributeString("D", D.ToString());
            writer.WriteAttributeString("ThermalFactor", ThermalFactor.ToString());
            writer.WriteAttributeString("CalibrationFactor", CalibrationFactor.ToString());
        }
        #endregion
    }
}
