﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Analysis
{
    public class NoiseFilter
    {
        public static void Smooth(double[] data, out double[] output, int width)
        {
            // smooth data with a quadratic Savitsky-Golay low-pass filter of order up to SmoothingWidth
            // (ie fits a quadratic to the (2*Width+1) points centred on each data point
            // smaller widths are used near the ends of the data

            int length = data.Length;
            double A, B;
            int k;

            // unweighted smoothing
            output = new double[length];
            for (int i = 0; i < length; i++)
            {
                k = width;

                if (k > i) k = i;
                if (k > length - i - 1) k = length - i - 1;

                A = 2 * k - 1;
                B = 3.0 / (A * (A + 2) * (A + 4));
                A = B * (3 * k * (k + 1) - 1);
                B = 5 * B;

                output[i] = 0;
                for (int j = -k; j <= k; j++)
                {
                    output[i] += (A - B * j * j) * data[i + j];
                }
            }
        }

        
        public static bool Filter(double[] data, out double[] output, double threshold, double hysteresis, int smoothingWidth, int maxPeakWidth)
        {
            int count = data.Length;
            output = new double[count];

            if (count <= 1)
            {
                return true;
            }

            // smooth data
            double[] smoothed;
            NoiseFilter.Smooth(data, out smoothed, smoothingWidth);

            // calculate difference array
            double[] diff = new double[count];

            for (int i = 0; i < count; i++)
            {
                diff[i] = Math.Abs(data[i] - smoothed[i]);

                // copy to output
                output[i] = data[i];
            }

            // look for differences (peaks) above threshold
            bool threshold_flag = false;
            int threshold_start = -1;
            for (int i = 0; i < count; i++)
            {
                // start
                if (!threshold_flag && diff[i] > threshold)
                {
                    threshold_flag = true;
                    threshold_start = i;
                }

                // end
                if (threshold_flag && diff[i] < threshold)
                {
                    threshold_flag = false;

                    int width = i - threshold_start;

                    // extrapolate and zero that region
                    int j = threshold_start;
                    while (j >= 0 && (maxPeakWidth == -1 || (threshold_start - j) < (maxPeakWidth - width) / 2))
                    {
                        // todo: magic number => 0.8 = scaling factor
                        if (diff[j] < hysteresis)
                        {
                            break;
                        }
                        output[j] = smoothed[j];
                        j--;
                    }

                    j = i;
                    while (j < count && (maxPeakWidth == -1 || (j - i) < (maxPeakWidth - width) / 2))
                    {
                        if (diff[j] < hysteresis)
                        {
                            break;
                        }
                        output[j] = smoothed[j];
                        j++;
                    }

                    // skip ahead
                    i = j;
                }
            }

            return true;
        }
        



        /*public static bool Filter(double[] data, out double[] output, double threshold)
        {
            int startWindow = 100;
            double ratioThreshold = 0.05;

            int count = data.Length;
            output = new double[count];

            if (count <= 1)
            {
                return true;
            }

            // find starting hold value
            int window = Math.Min(startWindow, count);
            double[] diffArr = new double[window - 1];
            double hold = -1.0;

            for (int i = 0; i < window; i++)
            {
                diffArr[i] = data[i + 1] - data[i];

                if (Math.Abs(diffArr[i]) < threshold)
                {
                    hold = data[i];
                    break;
                }

                if (i > 0)
                {
                    if (diffArr[i] * diffArr[i - 1] < 0)
                    {
                        // changed direction, both above threshold
                        // must be a peak
                        // look at relative magnitudes
                        double ratio1 = Math.Abs(Math.Abs(diffArr[i] / diffArr[i - 1]) - 2.0);
                        double ratio2 = Math.Abs(Math.Abs(diffArr[i - 1] / diffArr[i]) - 2.0);
                        if (ratio1 < ratioThreshold){
                            // second one = 2x first one
                            hold = data[i - 1];
                            break;
                        }
                        else if (ratio2 < ratioThreshold)
                        {
                            hold = data[i + 1];
                            break;
                        }
                    }
                }
            }

            if (hold == -1.0)
            {
                double average = 0.0;
                for (int i = 0; i < window; i++)
                {
                    average += window;
                }
                average /= window;

                // find first data point where diff < threshold
                for (int i = 0; i < count; i++)
                {
                    if (Math.Abs(data[i] - average) < threshold)
                    {
                        hold = data[i];
                        break;
                    }
                }
            }

            // look for 2 successive diffs > threshold, in opposite direction
            // then interpolate points
            // hold is last valid value

            output[0] = hold;
            double diff = 0.0;
            double diffPrev = data[1] - data[0];
            int lastValid = 0;
            double slope;
            for (int i = 1; i < count - 1; i++)
            {
                diff = data[i + 1] - data[i];
                if (Math.Abs(diffPrev) > threshold && Math.Abs(diff) > threshold && (diff * diffPrev < 0))
                {
                    // need to interpolate these later
                }
                else
                {
                    if (i - lastValid > 1)
                    {
                        slope = (data[i] - data[lastValid]) / (i - lastValid);
                        // interpolate
                        for (int j = lastValid + 1; j < i; j++)
                        {
                            output[j] = output[j - 1] + slope;
                        }
                    }
                    output[i] = data[i];

                    lastValid = i;
                }

                diffPrev = diff;
            }

            if (Math.Abs(diffPrev) <= threshold) {
                output[count - 1] = data[count - 1];
            }
            else if (lastValid == count - 2)
            {
                // so diffPrev > threshold, and lastValid is previous one
                // no slope info
                // copy prev data
                output[count - 1] = data[count - 2];
            }
            else
            {
                // so diffPrev > threshold and lastValid earlier
                // need to hold all values from lastValid
                for (int j = lastValid + 1; j < count; j++)
                {
                    output[j] = data[lastValid];
                }
            }
            

            return true;
        }*/
    }
}
