﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PaintAnalysis.Common
{
    public class UnitConverter
    {
        // # of mm in 1 unit
        // mm / x
        private static Dictionary<string, double> distance;

        static UnitConverter()
        {
            distance = new Dictionary<string, double>();
            distance["mm"] = 1.0;
            distance["micron"] = 1e-3;
            distance["mil"] = 0.0254;
            distance["inch"] = 25.4;
        }

        public static double ConvertDistance(string sourceUnit, string destUnit, double input)
        {
            return input * distance[sourceUnit] / distance[destUnit];
        }

        public static Converter<double, double> GetDistanceConverter(string sourceUnit, string destUnit)
        {
            return new Converter<double, double>(delegate(double input)
            {
                return ConvertDistance(sourceUnit, destUnit, input);
            });
        }
    }
}
