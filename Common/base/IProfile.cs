﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PaintAnalysis.Common
{
    public interface IProfile : IOptionsGroup
    {
        string ProfileName { get; set; }

        IProfile GetNew();
    }
}
