﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace PaintAnalysis.Common
{
    public class SelectableProfileList<TProfile> : List<TProfile>, IXmlSerializable, IOptionsGroup
        where TProfile : class, IProfile, new()
    {
        protected int selectedIndex = -1;
        /// <summary>
        /// Returns the index of the last profile marked as selected or -1 if it doesn't exist, or sets the selected profile. Index is non-definitive.
        /// </summary>
        public virtual int SelectedIndex
        {
            get
            {
                if (selectedProfileName == String.Empty)
                    return -1;
                
                if (selectedIndex > -1 && selectedIndex < Count)
                {
                    // if index is valid, check it matches the profile name
                    // acts as a cached index
                    if (this[selectedIndex].ProfileName == selectedProfileName)
                    {
                        return selectedIndex;
                    }
                }

                // search for profile with correct name and return index of it
                for (int i = 0; i < Count; i++)
                {
                    if (this[i].ProfileName == selectedProfileName)
                    {
                        selectedIndex = i;
                        return i;
                    }
                }

                selectedIndex = -1;
                selectedProfileName = String.Empty;
                return -1;
            }
            set
            {
                if (value == -1)
                {
                    selectedIndex = -1;
                    selectedProfileName = String.Empty;
                }
                else if (value < this.Count)
                {
                    selectedIndex = value;
                    selectedProfileName = this[value].ProfileName;
                }
                else
                    throw new ArgumentOutOfRangeException("Selected index not within range of list.");
            }
        }
        
        protected string selectedProfileName = String.Empty;
        /// <summary>
        /// Returns or sets the name of the last profile marked as selected. Name is definitive. Empty string if no profile set.
        /// </summary>
        public virtual string SelectedProfileName
        {
            get
            {
                // check profile actually exists
                int localSelectedIndex = SelectedIndex;
                if (localSelectedIndex == -1)
                {
                    return String.Empty;
                }

                return selectedProfileName;
            }
            set
            {
                // check name exists
                for (int i = 0; i < Count; i++)
                {
                    if (this[i].ProfileName == value)
                    {
                        selectedProfileName = value;
                        selectedIndex = i;
                        return;
                    }
                }

                throw new ArgumentOutOfRangeException("Selected profile name does not exist in list.");
            }
        }

        /// <summary>
        /// Returns the last selected profile or null.
        /// </summary>
        public virtual TProfile SelectedProfile
        {
            get
            {
                int localSelectedIndex = SelectedIndex;
                if (localSelectedIndex == -1)
                {
                    return null;
                }

                return this[localSelectedIndex];
            }
        }
        

        #region IXmlSerializable

        public XmlSchema GetSchema() { return null; }

        public void ReadXml(XmlReader reader)
        {
            if (reader.MoveToContent() == XmlNodeType.Element && reader.LocalName == "SelectableProfileList")
            {
                // set to backing store to avoid sanity checks until after profiles populated
                selectedProfileName = reader["SelectedProfileName"];
                string tProfile = reader["TProfile"];

                string elementType = typeof(TProfile).ToString();
                elementType = elementType.Substring(elementType.LastIndexOf('.') + 1);

                if (tProfile == elementType && reader.ReadToDescendant(elementType))
                {
                    while (reader.MoveToContent() == XmlNodeType.Element && reader.LocalName == elementType)
                    {
                        TProfile profile = new TProfile();
                        profile.ReadXml(reader);
                        this.Add(profile);
                    }
                }
                reader.Read();
            }
        }

        public void WriteXml(XmlWriter writer)
        {
            writer.WriteAttributeString("SelectedProfileName", SelectedProfileName);

            string elementType = typeof(TProfile).ToString();
            elementType = elementType.Substring(elementType.LastIndexOf('.') + 1);

            foreach (TProfile profile in this)
            {
                writer.WriteStartElement(elementType);
                profile.WriteXml(writer);
                writer.WriteEndElement();
            }
        }

        #endregion

        public IOptionsGroup DeepCopy()
        {
            SelectableProfileList<TProfile> copy = new SelectableProfileList<TProfile>();
            foreach (TProfile item in this)
            {
                copy.Add((TProfile)item.DeepCopy());
            }
            copy.SelectedIndex = SelectedIndex;

            return copy;
        }
    }
}
