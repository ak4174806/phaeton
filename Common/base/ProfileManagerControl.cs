﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace PaintAnalysis.Common
{
    public partial class ProfileManagerControl<TProfile, TProfileControl> : BaseOptionsGroupControl
        where TProfile : class, IProfile, new()
        where TProfileControl : BaseOptionsGroupControl, new()
    {
        public ProfileManagerControl()
        {
            InitializeComponent();

            // add profile controls
            profileControls = new TProfileControl();
            this.profileControls.Location = new System.Drawing.Point(0, 0);
            this.profileControls.Name = "profileControls";
            this.profileControls.TabIndex = 5;
            this.profileControls.OptionsChanged += new EventHandler(profileControls_OptionsChanged);
            this.pnlControls.Controls.Add(this.profileControls);

            // resize control to match
            Size panelSize = pnlControls.Size;
            Size controlSize = this.Size;
            Size profileSize = profileControls.Size;

            Size newControlSize = new Size();
            // min width = width of control now
            newControlSize.Width = Math.Max(controlSize.Width, controlSize.Width - panelSize.Width + profileSize.Width);
            newControlSize.Height = controlSize.Height - panelSize.Height + profileSize.Height;

            this.Size = newControlSize;
            this.RecommendedSize = newControlSize;

            // when a profile control changes, or the combo box changes, mark modified
            this.profileControls.OptionsChanged += new EventHandler(raiseOptionsChanged);
            this.cmbProfile.SelectedIndexChanged += new EventHandler(raiseOptionsChanged);
        }

        private SelectableProfileList<TProfile> profileList;
        /// <summary>
        /// Exposes the list of profiles and the currently selected profile.
        /// </summary>
        public SelectableProfileList<TProfile> ProfileList
        {
            get
            {
                if (profileList == null)
                    profileList = new SelectableProfileList<TProfile>();

                return profileList;
            }
            set
            {
                profileList = value;
                updateNeeded = true;
                UpdateProfileDisplay();
            }
        }

        // pointa directly to ProfileList
        public override IOptionsGroup CurrentOptions
        {
            get
            {
                return ProfileList;
            }
            set
            {
                ProfileList = (SelectableProfileList<TProfile>)value;
            }
        }

        public override bool Valid
        {
            get
            {
                return profileControls.Valid;
            }
        }


        private TProfileControl profileControls;
        public TProfileControl ProfileControls
        {
            get { return profileControls; }
        }

        public enum ControlStates
        {
            NewProfile,
            SelectedProfile,
            CustomProfile
        }
        private ControlStates controlState;
        /// <summary>
        /// Gets or sets the current state of the form.
        /// </summary>
        public ControlStates ControlState
        {
            get { return controlState; }
            set
            {
                if (controlState != value)
                {
                    updateNeeded = true;
                }

                btnCancelChanges.Enabled = (value == ControlStates.CustomProfile); 
                btnDeleteProfile.Enabled = (value == ControlStates.SelectedProfile);

                // note: enable cancel for newprofile when modified

                controlState = value;
            }
        }


        // call after changing number of profiles, also called in ControlState
        private bool updateNeeded = false;
        public void UpdateProfileDisplay()
        {
            if (!updateNeeded) return;

            // add all items
            cmbProfile.Items.Clear();
            for (int i = 0; i < ProfileList.Count; i++)
            {
                cmbProfile.Items.Add(ProfileList[i].ProfileName);
            }

            // add <new> marker if empty
            if (ProfileList.Count == 0 && ControlState != ControlStates.NewProfile)
            {
                ControlState = ControlStates.NewProfile;
            }
            // separate to above as changing control state calls this function
            if (ProfileList.Count == 0)
            {
                cmbProfile.Items.Add("<new>");
                cmbProfile.SelectedIndex = 0;
            }
            else
            {
                if (ControlState == ControlStates.CustomProfile)
                {
                    cmbProfile.Items.Insert(0, "<custom>");
                    cmbProfile.SelectedIndex = 0;
                }
                else
                {
                    // should be a selected profile
                    // set correct index if one set
                    int index = ProfileList.SelectedIndex;

                    // in the start-up case when no selected index is set, this can occur
                    if (index > -1)
                        cmbProfile.SelectedIndex = index;
                    else
                        cmbProfile.SelectedIndex = 0;
                }
            }
            
            updateNeeded = false;
        }


        private void LoadProfile()
        {
            TProfile profile = ProfileList.SelectedProfile;
            if (profileControls == null)
            {
                throw new ApplicationException("No profile selected when LoadProfile called");
            }

            // load the options
            profileControls.CurrentOptions = profile;

            // change form state
            ControlState = ControlStates.SelectedProfile;
        }

        private void LoadProfileNew()
        {
            profileControls.CurrentOptions = new TProfile().GetNew();
            btnCancelChanges.Enabled = false;
        }


        public override void Init()
        {
            base.Init();
        
            // set up profile controls
            profileControls.Init();
        }

        public override void InitData()
        {
            base.InitData();

            profileControls.InitData();

            ControlState = ControlStates.SelectedProfile;
            UpdateProfileDisplay();
        }

        public void SetProfile(int selectedIndex)
        {
            if (ControlState == ControlStates.CustomProfile) selectedIndex += 1;

            // move combo box to correct place
            cmbProfile.SelectedIndex = selectedIndex;
        }
        
        private void profileControls_OptionsChanged(object sender, EventArgs e)
        {
            // change form state
            if (ControlState == ControlStates.SelectedProfile)
            {
                ControlState = ControlStates.CustomProfile;
                UpdateProfileDisplay();
            }

            // only enable cancel once changes made
            if (ControlState == ControlStates.NewProfile)
            {
                btnCancelChanges.Enabled = true;
            }
        }
     
        private void cmbProfile_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbProfile.Items.Count == 0)
                return;

            // <new>, <custom> or profile name
            string item = cmbProfile.SelectedItem.ToString();

            if (item == "<new>")
            {
                // should be the only item in the list
                // no need to confirm as no other profiles exist
                ProfileList.SelectedIndex = -1;
                LoadProfileNew();
            }
            else if (item == "<custom>")
            {
                // do nothing
            }
            else
            {
                // moved to another profile
                // can be either when a profile is selected or in custom, or new and a profile is added, in custom - need to confirm first

                // confirm for custom
                if (ControlState == ControlStates.CustomProfile)
                {
                    // confirm before change
                    if (MessageBox.Show("You will lose any unsaved changes. Continue?", "Confirm changing profiles?", MessageBoxButtons.YesNo) == DialogResult.No)
                    {
                        // cancel
                        cmbProfile.SelectedIndex = 0; // to the <custom> entry
                        return;
                    }

                    // need to remove <custom> entry
                    ControlState = ControlStates.SelectedProfile;
                    updateNeeded = true;

                    // load new selected profile
                    ProfileList.SelectedProfileName = item;
                    UpdateProfileDisplay();

                    LoadProfile();
                    return;
                }

                // load new selected profile
                ControlState = ControlStates.SelectedProfile;
                ProfileList.SelectedProfileName = item;

                LoadProfile();
            }
        }

        private void btnSaveProfile_Click(object sender, EventArgs e)
        {
            // set up form
            ProfileSaveAsForm saveForm = new ProfileSaveAsForm();
            saveForm.NewProfileValidator = ProfileSaveAsForm.DefaultProfileValidator;
            
            string[] profileList = new string[ProfileList.Count];
            for (int i = 0; i < ProfileList.Count; i++)
            {
                profileList[i] = ProfileList[i].ProfileName;
            }
            saveForm.OverwriteProfileList = profileList;

            if (ControlState == ControlStates.NewProfile)
            {
                saveForm.DefaultMode = ProfileSaveAsForm.ProfileSaveMode.NewProfile;
                saveForm.DisableSaveModeChange = true;
                saveForm.NewProfileDefault = "profile";
                saveForm.OverwriteProfileDefault = "";
            }
            else
            {
                if (ControlState == ControlStates.CustomProfile)
                    saveForm.DefaultMode = ProfileSaveAsForm.ProfileSaveMode.OverwriteProfile;
                else
                    saveForm.DefaultMode = ProfileSaveAsForm.ProfileSaveMode.NewProfile;

                saveForm.DisableSaveModeChange = false;
                saveForm.NewProfileDefault = "profile";
                saveForm.OverwriteProfileDefault = ProfileList.SelectedProfileName;
            }

            // show dialog
            DialogResult result = saveForm.ShowDialog();
            if (result == System.Windows.Forms.DialogResult.OK)
            {
                TProfile profile = (TProfile)profileControls.CurrentOptions;
                profile.ProfileName = saveForm.ProfileName;

                if (saveForm.SaveMode == ProfileSaveAsForm.ProfileSaveMode.NewProfile)
                {
                    // name has been validated already
                    ProfileList.Add(profile);
                    ProfileList.SelectedProfileName = profile.ProfileName;

                    ControlState = ControlStates.SelectedProfile;

                    // update display and state, then select correct option
                    updateNeeded = true; 
                    UpdateProfileDisplay();

                    cmbProfile.SelectedIndex = ProfileList.SelectedIndex;
                }
                else
                {
                    // overwrite
                    ProfileList.SelectedProfileName = profile.ProfileName;
                    ProfileList[ProfileList.SelectedIndex] = profile;

                    // update cmb
                    ControlState = ControlStates.SelectedProfile;

                    // update display and state, then select correct option
                    updateNeeded = true; 
                    UpdateProfileDisplay();
                    
                    cmbProfile.SelectedIndex = ProfileList.SelectedIndex;
                }
            }
        }

        private void btnDeleteProfile_Click(object sender, EventArgs e)
        {
            if (ControlState != ControlStates.SelectedProfile)
            {
                throw new ApplicationException("Tried to delete a new or custom profile.");
            }

            int index = ProfileList.SelectedIndex;
            if (index == -1)
                throw new ApplicationException("Tried to delete a profile when no profile was selected.");

            // confirm
            if (MessageBox.Show("Are you sure you want to delete?", "Confirm deletion", MessageBoxButtons.YesNo) == DialogResult.No)
            {
                // cancel
                return;
            }

            // decide new selected index
            int newIndex = index - 1;
            if (newIndex == -1 && ProfileList.Count >= 2)
            {
                newIndex = 0;
            }
            
            // fix profile list
            ProfileList.RemoveAt(index);
            ProfileList.SelectedIndex = newIndex;

            // fix cmb and load new stuff
            cmbProfile.Items.RemoveAt(index);

            updateNeeded = true;
            if (newIndex == -1)
            {
                ControlState = ControlStates.NewProfile;
            }
            else
            {
                ControlState = ControlStates.SelectedProfile;
                cmbProfile.SelectedIndex = newIndex;
            }
            UpdateProfileDisplay();
        }

        private void btnCancelChanges_Click(object sender, EventArgs e)
        {
            // confirm before change
            if (MessageBox.Show("You will lose any unsaved changes. Continue?", "Confirm cancelling profile changes", MessageBoxButtons.YesNo) == DialogResult.No)
            {
                // cancel
                return;
            }

            switch (ControlState)
            {
                case ControlStates.SelectedProfile:
                    throw new ApplicationException("Tried to cancel when profile was selected.");

                case ControlStates.NewProfile:
                    LoadProfileNew();
                    break;

                case ControlStates.CustomProfile:
                    LoadProfile();
                    break;
            }

            updateNeeded = true;
            UpdateProfileDisplay();
        }

    }
}
