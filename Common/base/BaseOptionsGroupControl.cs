﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace PaintAnalysis.Common
{
    public partial class BaseOptionsGroupControl : UserControl
    {
        public BaseOptionsGroupControl()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Raised whenever a control within the Options Group changes. sender is the class raising the event.
        /// </summary>
        public event EventHandler OptionsChanged;

        private bool suppressOptionsChanged = false;
        /// <summary>
        /// Stops events being raised when called through raiseOptionsChanged
        /// </summary>
        protected bool SuppressOptionsChanged
        {
            get { return suppressOptionsChanged; }
            set { suppressOptionsChanged = value; }
        }

        /// <summary>
        /// Helper method that invokes the OptionsChanged event
        /// </summary>
        protected virtual void raiseOptionsChanged()
        {
            if (!SuppressOptionsChanged && OptionsChanged != null)
            {
                OptionsChanged(this, new EventArgs());
            }
        }
        protected virtual void raiseOptionsChanged(object sender, EventArgs e)
        {
            raiseOptionsChanged();
        }

        /// <summary>
        /// Gets or sets the values of the option group controls
        /// </summary>
        public virtual IOptionsGroup CurrentOptions
        {
            get;
            set;
        }

        public virtual void Init()
        { }

        public virtual void InitData()
        { }


        private Size recommendedSize;
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false)]
        public virtual Size RecommendedSize
        {
            get
            {
                if (recommendedSize == null)
                    recommendedSize = this.Size;
                return recommendedSize;
            }
            set { recommendedSize = value; }
        }

        // todo: validation
        public virtual bool Valid
        {
            get { return true; }
        }
    }
}
