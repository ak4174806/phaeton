﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace PaintAnalysis.Common
{
    public interface IOptionsGroup : IXmlSerializable
    {
        IOptionsGroup DeepCopy();
    }
}
