﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace PaintAnalysis.Common
{
    public partial class ProfileSaveAsForm : Form
    {
        public ProfileSaveAsForm()
        {
            InitializeComponent();
        }
        
        /// <summary>
        /// Delegate for validating profile names
        /// </summary>
        /// <param name="profileName">The profile name to test</param>
        /// <param name="errorMessage">The error message for the name</param>
        /// <returns>Whether it is valid</returns>
        public delegate bool ValidateProfileName(string profileName, out string errorMessage);

        /// <summary>
        /// A default validator that requires a non-empty string with alphanumeric characters, spaces and -, _ and ,
        /// </summary>
        public static ValidateProfileName DefaultProfileValidator = new ValidateProfileName(delegate(string profileName, out string errorMessage)
            {
                errorMessage = "";

                Regex r = new Regex(@"^[a-zA-Z \-_\.0-9]+$");
                if (!r.IsMatch(profileName))
                {
                    errorMessage = "Please enter a profile name containing only letters, numbers, spaces and the following special characters: -, _ and .";
                    return false;
                }

                return true;
            });

        public enum ProfileSaveMode
        {
            NewProfile,
            OverwriteProfile
        }

        #region Properties

        #region Inputs

        private ProfileSaveMode defaultMode = ProfileSaveMode.NewProfile;
        /// <summary>
        /// Input: Sets the start-up mode of the save profile dialog
        /// </summary>
        public ProfileSaveMode DefaultMode
        {
            get { return defaultMode; }
            set
            {
                defaultMode = value;

                if (value == ProfileSaveMode.NewProfile)
                    radNewProfile.Checked = true;
                else
                    radOverwriteProfile.Checked = true;

                UpdateDisabledRadioButtons();
            }
        }

        private string newProfileDefault = String.Empty;
        /// <summary>
        /// Input: Sets the default value of the box for saving a new profile
        /// </summary>
        public string NewProfileDefault
        {
            get { return newProfileDefault; }
            set
            {
                newProfileDefault = value;
                txtNewProfile.Text = value;
            }
        }

        private string[] overwriteProfileList;
        /// <summary>
        /// Input: The list of profiles available to overwrite
        /// </summary>
        public string[] OverwriteProfileList
        {
            get { return overwriteProfileList; }
            set
            {
                overwriteProfileList = value;

                cmbOverwriteProfile.Items.Clear();
                if (value.Length > 0)
                {
                    cmbOverwriteProfile.Items.AddRange(value);
                }
                else
                {
                    // if no profiles set, then fix it to new profiles only
                    cmbOverwriteProfile.Items.Add("no profiles");
                    DefaultMode = ProfileSaveMode.NewProfile;
                    DisableSaveModeChange = true;
                }

                UpdateSelectedOverwriteProfile();
            }
        }

        private string overwriteProfileDefault = String.Empty;
        /// <summary>
        /// Input: The default selected profile to overwrite
        /// </summary>
        public string OverwriteProfileDefault
        {
            get { return overwriteProfileDefault; }
            set
            {
                overwriteProfileDefault = value;
                UpdateSelectedOverwriteProfile();
            }
        }

        private bool disableSaveModeChange = false;
        /// <summary>
        /// Input: Whether to disable save state changing.
        /// </summary>
        public bool DisableSaveModeChange
        {
            get { return disableSaveModeChange; }
            set
            {
                disableSaveModeChange = value;
                UpdateDisabledRadioButtons();
            }
        }

        private ValidateProfileName newProfileValidator;
        /// <summary>
        /// Input: Used to validate the new profile name.
        /// </summary>
        public ValidateProfileName NewProfileValidator
        {
            get { return newProfileValidator; }
            set { newProfileValidator = value; }
        }

        #endregion

        #region Outputs

        /// <summary>
        /// Output: Gets or sets the currently selected save mode.
        /// </summary>
        public ProfileSaveMode SaveMode
        {
            get 
            {
                if (radNewProfile.Checked)
                    return ProfileSaveMode.NewProfile;
                else
                    return ProfileSaveMode.OverwriteProfile;
            }
            set
            {
                if (SaveMode != value)
                {
                    if (value == ProfileSaveMode.NewProfile)
                        radNewProfile.Checked = true;
                    else
                        radOverwriteProfile.Checked = true;
                }
            }
        }

        /// <summary>
        /// Output: Gets the current selected profile name
        /// </summary>
        public string ProfileName
        {
            get
            {
                if (SaveMode == ProfileSaveMode.NewProfile)
                {
                    return txtNewProfile.Text;
                }
                else
                {
                    return cmbOverwriteProfile.SelectedItem.ToString();
                }
            }
        }

        #endregion

        #endregion

        /// <summary>
        /// Selects the appropriate value in the overwrite profile combo box
        /// </summary>
        private void UpdateSelectedOverwriteProfile()
        {
            if (OverwriteProfileList != null)
            {
                if (OverwriteProfileDefault == String.Empty)
                {
                    cmbOverwriteProfile.SelectedIndex = 0;
                }
                else
                {
                    for (int i = 0; i < OverwriteProfileList.Length; i++)
                    {
                        if (OverwriteProfileDefault == OverwriteProfileList[i])
                        {
                            cmbOverwriteProfile.SelectedIndex = i;
                            return;
                        }
                    }

                    cmbOverwriteProfile.SelectedIndex = 0;
                }
            }
        }

        /// <summary>
        /// Updates the disable state of the radio buttons based on whether save state changing is disabled.
        /// </summary>
        private void UpdateDisabledRadioButtons()
        {
            if (DisableSaveModeChange)
            {
                radNewProfile.Enabled = (DefaultMode == ProfileSaveMode.NewProfile);
                radOverwriteProfile.Enabled = (DefaultMode == ProfileSaveMode.OverwriteProfile);
            }
            else
            {
                radNewProfile.Enabled = true;
                radOverwriteProfile.Enabled = true;
            }
        }

        private void ProfileSaveAsForm_Load(object sender, EventArgs e)
        {
            // testing input combinations
            //DefaultMode = ProfileSaveMode.NewProfile;
            //DefaultMode = ProfileSaveMode.OverwriteProfile;
            //NewProfileDefault = "profile 1";
            //NewProfileDefault = "";
            //NewProfileDefault = "taken";
            //OverwriteProfileDefault = "taken";
            //OverwriteProfileDefault = "";
            //OverwriteProfileDefault = "unrelated";
            //OverwriteProfileList = new string[] { "taken", "taken 1", "taken 2" };
            //OverwriteProfileList = new string[] { "taken 1", "taken", "taken 2" };
            //OverwriteProfileList = new string[] { };
            //DisableSaveModeChange = true;
            /*NewProfileValidator = new ValidateProfileName(delegate(string profileName, out string errorMessage)
            {
                errorMessage = "";

                Regex r = new Regex(@"^[a-zA-Z \-_\.0-9]+$");
                if (!r.IsMatch(profileName))
                {
                    errorMessage = "Please enter a profile name containing only letters, numbers, spaces and the following special characters: -, _ and .";
                    return false;
                }

                return true;
            });*/
            
            // verify inputs that need to be set
            if (OverwriteProfileList == null)
            {
                OverwriteProfileList = new string[] { };
            }

            // fix the side-effects of the default mode - ie: enable the correct input
            bool is_new = (DefaultMode == ProfileSaveMode.NewProfile);
            txtNewProfile.Enabled = is_new;
            cmbOverwriteProfile.Enabled = !is_new;

            // default focus
            // apparently, can't focus many controls - todo: check
            /*if (is_new)
                txtNewProfile.Focus();
            else
                cmbOverwriteProfile.Focus();*/
        }

        private void radNewProfile_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton rad = (RadioButton)sender;

            // only happen on checked to avoid 2 events
            if (rad.Checked)
            {
                txtNewProfile.Enabled = true;
                cmbOverwriteProfile.Enabled = false;
                txtNewProfile.Select();
            }
        }

        private void radOverwriteProfile_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton rad = (RadioButton)sender;

            // only happen on checked to avoid 2 events
            if (rad.Checked)
            {
                txtNewProfile.Enabled = false;
                cmbOverwriteProfile.Enabled = true;
                cmbOverwriteProfile.Select();
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (SaveMode == ProfileSaveMode.NewProfile)
            {
                // check that it is new
                if (OverwriteProfileList.Length > 0)
                {
                    string name = ProfileName;

                    // validate name
                    if (NewProfileValidator != null)
                    {
                        string errorMessage;
                        bool result = NewProfileValidator(name, out errorMessage);

                        if (!result)
                        {
                            MessageBox.Show(errorMessage);
                            return;
                        }
                    }

                    foreach (string overwriteProfileName in OverwriteProfileList)
                    {
                        if (name == overwriteProfileName)
                        {
                            MessageBox.Show("Please enter a profile name that is different to any that already exist.", "Invalid new profile name");
                            return;
                        }
                    }
                }
            }

            DialogResult = System.Windows.Forms.DialogResult.OK;
            
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.Cancel;

            this.Close();
        }
    }
}
