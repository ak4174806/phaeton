﻿namespace PaintAnalysis.Common
{
    partial class ProfileSaveAsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.radNewProfile = new System.Windows.Forms.RadioButton();
            this.radOverwriteProfile = new System.Windows.Forms.RadioButton();
            this.txtNewProfile = new System.Windows.Forms.TextBox();
            this.cmbOverwriteProfile = new System.Windows.Forms.ComboBox();
            this.btnOK = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // radNewProfile
            // 
            this.radNewProfile.AutoSize = true;
            this.radNewProfile.Checked = true;
            this.radNewProfile.Location = new System.Drawing.Point(12, 13);
            this.radNewProfile.Name = "radNewProfile";
            this.radNewProfile.Size = new System.Drawing.Size(76, 17);
            this.radNewProfile.TabIndex = 0;
            this.radNewProfile.TabStop = true;
            this.radNewProfile.Text = "new profile";
            this.radNewProfile.UseVisualStyleBackColor = true;
            this.radNewProfile.CheckedChanged += new System.EventHandler(this.radNewProfile_CheckedChanged);
            // 
            // radOverwriteProfile
            // 
            this.radOverwriteProfile.AutoSize = true;
            this.radOverwriteProfile.Location = new System.Drawing.Point(12, 48);
            this.radOverwriteProfile.Name = "radOverwriteProfile";
            this.radOverwriteProfile.Size = new System.Drawing.Size(99, 17);
            this.radOverwriteProfile.TabIndex = 2;
            this.radOverwriteProfile.Text = "overwrite profile";
            this.radOverwriteProfile.UseVisualStyleBackColor = true;
            this.radOverwriteProfile.CheckedChanged += new System.EventHandler(this.radOverwriteProfile_CheckedChanged);
            // 
            // txtNewProfile
            // 
            this.txtNewProfile.Location = new System.Drawing.Point(144, 11);
            this.txtNewProfile.Name = "txtNewProfile";
            this.txtNewProfile.Size = new System.Drawing.Size(121, 20);
            this.txtNewProfile.TabIndex = 1;
            // 
            // cmbOverwriteProfile
            // 
            this.cmbOverwriteProfile.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbOverwriteProfile.FormattingEnabled = true;
            this.cmbOverwriteProfile.Location = new System.Drawing.Point(144, 44);
            this.cmbOverwriteProfile.Name = "cmbOverwriteProfile";
            this.cmbOverwriteProfile.Size = new System.Drawing.Size(121, 21);
            this.cmbOverwriteProfile.TabIndex = 3;
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(48, 85);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 4;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(153, 85);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 5;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // ProfileSaveAsForm
            // 
            this.AcceptButton = this.btnOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(285, 123);
            this.ControlBox = false;
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.cmbOverwriteProfile);
            this.Controls.Add(this.txtNewProfile);
            this.Controls.Add(this.radOverwriteProfile);
            this.Controls.Add(this.radNewProfile);
            this.MaximumSize = new System.Drawing.Size(293, 167);
            this.MinimumSize = new System.Drawing.Size(293, 167);
            this.Name = "ProfileSaveAsForm";
            this.Text = "Save profile as ...";
            this.Load += new System.EventHandler(this.ProfileSaveAsForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton radNewProfile;
        private System.Windows.Forms.RadioButton radOverwriteProfile;
        private System.Windows.Forms.TextBox txtNewProfile;
        private System.Windows.Forms.ComboBox cmbOverwriteProfile;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnCancel;
    }
}