﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace PaintAnalysis.Common
{
    public partial class BaseOptionsForm<TOptions, TOptionsControls> : Form
        where TOptions : class, IOptionsGroup, new()
        where TOptionsControls : BaseOptionsGroupControl, new()
    {
        public BaseOptionsForm()
        {
            InitializeComponent();
        }

        #region Events
        /// <summary>
        /// Called when the options form is closed.
        /// </summary>
        public event EventHandler OptionsFormClosed;

        /// <summary>
        /// Called when the options form is shown.
        /// </summary>
        public event EventHandler OptionsFormShown;
        #endregion

        /// <summary>
        /// The title of the options form
        /// </summary>
        public virtual string Title
        {
            get { return Text; }
            set
            {
                Text = value;
                lblSettingsTitle.Text = value;
            }
        }


        /// <summary>
        /// Gets the options set on this form.
        /// </summary>
        public virtual TOptions CurrentOptions
        {
            get
            {
                if (OptionsControls == null)
                    return new TOptions();
                return (TOptions)OptionsControls.CurrentOptions;
            }
            protected set
            {
                OptionsControls.CurrentOptions = value;
            }
        }

        private TOptions savedOptions;
        /// <summary>
        /// Gets the saved options set on this form.
        /// </summary>
        public virtual TOptions SavedOptions
        {
            get { return savedOptions; }
            protected set { savedOptions = value; }
        }




        // point it at the options controls added in base forms
        // used as backing store for CurrentOptions
        public virtual TOptionsControls OptionsControls
        {
            get { return null; }
        }
                
        /// <summary>
        /// Persists from CurrentOptions to file.
        /// </summary>
        public virtual void SaveOptions()
        { }

        /// <summary>
        /// Persists from file to CurrentOptions.
        /// </summary>
        public virtual void LoadOptions()
        { }

        /// <summary>
        /// Runs validation and returns true if it passes.
        /// </summary>
        /// <returns></returns>
        public virtual bool ValidateSettings()
        {
            return true;
        }



        private bool modified;
        /// <summary>
        /// Whether any of the options have been modified
        /// </summary>
        protected virtual bool Modified
        {
            get { return modified; }
            set
            {
                modified = value;

                if (modified)
                {
                    btnSave.Enabled = true;
                    btnOK.Enabled = true;
                    btnCancel.Text = "Cancel";
                }
                else
                {
                    btnSave.Enabled = false;
                    btnOK.Enabled = false;
                    btnCancel.Text = "Close";
                }
            }
        }

        /// <summary>
        /// Event handler to mark a control as modifed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void onModified(object sender, EventArgs e)
        {
            Modified = true;
        }


        public void Init()
        {
            // init twice - added here so combo boxes etc are set up so can actually load
            if (OptionsControls != null) OptionsControls.Init();

            // load saved values from file
            // do first as need to load data before initing OptionsControls
            LoadOptions();

            // sets up controls (i.e. drop down lists), sets default values (before saved values loaded)
            if (OptionsControls != null) OptionsControls.InitData();

            // add modified handler
            // do it last to prevent it getting called when default values set
            if (OptionsControls != null) OptionsControls.OptionsChanged += new EventHandler(onModified);

            // default saved values - need to clone
            if (SavedOptions == null)
            {
                TOptions opt = CurrentOptions;
                SavedOptions = (TOptions)(opt.DeepCopy());
            }
        }


        private void BaseOptionsForm_VisibleChanged(object sender, EventArgs e)
        {
            if (this.Visible)
            {
                if (OptionsFormShown != null)
                    OptionsFormShown(this, new EventArgs());

                Modified = false;
            }
        }

        private void BaseOptionsForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            // todo: validate

            if (e.CloseReason == CloseReason.UserClosing || e.CloseReason == CloseReason.None)
            {
                // prevent closing, just hide it
                e.Cancel = true;

                if (this.DialogResult == DialogResult.Cancel)
                {
                    if (!Modified || MessageBox.Show("You will lose any changes. Continue?", "Cancelling options changes", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        // reset form
                        if (Modified && SavedOptions != null)
                            CurrentOptions = SavedOptions;
                        Modified = false;

                        hideForm();
                    }
                }
                else if (this.DialogResult == DialogResult.OK)
                {
                    if (ValidateSettings())
                    {
                        SavedOptions = CurrentOptions;
                        SaveOptions();
                        Modified = false;

                        hideForm();
                    }
                }
                else
                {
                    // close for some other reason - then treat it as a cancel
                    if (Modified && SavedOptions != null)
                        CurrentOptions = SavedOptions;
                    Modified = false;

                    hideForm();
                }
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            // todo: validate
            
            SavedOptions = CurrentOptions;
            SaveOptions();
            Modified = false;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void hideForm()
        {
            if (OptionsFormClosed != null)
                OptionsFormClosed(this, new EventArgs());

            this.Hide();
        }
    }
}
