﻿using System;
using System.Collections.Generic;
using System.Text;

using System.Windows.Forms;
using System.Drawing;

namespace PaintAnalysis.Common
{
    public class OutputManager
    {
        protected RichTextBox output;

        public enum Level
        {
            INFO,
            SUCCESS,
            ERROR
        }

        public OutputManager(RichTextBox txtOutput)
        {
            output = txtOutput;
        }


        public void Write(string message)
        {
            Write(message, Level.INFO);
        }

        public void WriteLine(string message)
        {
            WriteLine(message, Level.INFO);
        }
        
        public void Write(string message, Level level)
        {
            output.SelectionStart = 0;
            output.SelectionLength = 0;

            switch (level)
            {
                case Level.ERROR:
                    output.SelectionColor = Color.Red;
                    break;

                case Level.SUCCESS:
                    output.SelectionColor = Color.Green;
                    break;

                case Level.INFO:
                    output.SelectionColor = Color.Black;
                    break;
            }

            output.SelectedText = message;
        }


        public void WriteLine(string message, Level level)
        {
            if (output.Text.Length > 0)
            {
                Write(message + "\n\r", level);
            }
            else
            {
                Write(message, level);
            }
        }


        public void Clear()
        {
            output.Text = "";
        }

        public string GetText()
        {
            return output.Text;
        }

    }
}
