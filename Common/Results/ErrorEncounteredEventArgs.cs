﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PaintAnalysis.Common.Results
{
    public class ErrorEncounteredEventArgs : EventArgs
    {
        public String Description { get; private set; }

        public ErrorEncounteredEventArgs(String description)
        {
            Description = description;
        }
    }
}
