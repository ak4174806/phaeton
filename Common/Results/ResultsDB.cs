﻿using System;
using System.Collections.Generic;
using System.Text;

using System.Data;
using System.Data.SQLite;
using System.IO;
using PaintAnalysis.Common;

namespace PaintAnalysis.Common.Results
{
    public class ResultsDB
    {
        public event EventHandler<ErrorEncounteredEventArgs> ErrorEncountered;
        protected void OnErrorEncountered(ErrorEncounteredEventArgs e)
        {
            if (ErrorEncountered != null)
                ErrorEncountered(this, e);
        }

        public SQLiteDatabase Database
        {
            get;
            private set;
        }

        public string DatabaseVersion
        {
            get;
            private set;
        }

        public string SqlScriptPath
        {
            get;
            private set;
        }

        public string DatabasePath
        {
            get;
            private set;
        }

        public DataSet CombinedDataSet { get; private set; }

        public ResultsDB(string databasePath, string databasePassword, string databaseVersion, string sqlScriptPath)
        {
            DatabasePath = databasePath;
            DatabaseVersion = databaseVersion;
            SqlScriptPath = sqlScriptPath;

            CombinedDataSet = new DataSet();

            // if it doesn't exist, need to create schema for it
            bool databaseExists = File.Exists(databasePath);

            Dictionary<string, string> options = new Dictionary<string, string>();
            options["Data Source"] = databasePath;

            if (databaseExists)
            {
                // check it works
                if (databasePassword != "") options["Password"] = databasePassword;
                Database = new SQLiteDatabase(options);
                bool valid = Database.TestConnection();

                if (valid)
                {
                    try
                    {
                        GetDatabaseVersion(Database);

                        // handle database updates
                        UpgradeDatabase();
                        return;
                    }
                    catch { }
                    
                }

                // otherwise backup old database and create a new one

                // backup old file and make a new one to recreate the database
                string databaseName = Path.GetFileName(databasePath);
                string databaseFolder = Directory.GetParent(databasePath).FullName;
                if (!Directory.Exists(Path.Combine(databaseFolder, "db_bak")))
                {
                    Directory.CreateDirectory(Path.Combine(databaseFolder, "db_bak"));
                }

                File.Copy(databasePath, Path.Combine(Path.Combine(databaseFolder, "db_bak"), databaseName + ".bak"), true);
                File.Delete(databasePath);
            }

            try
            {
                // open without a password
                options.Remove("Password");

                Database = new SQLiteDatabase(options);
                SQLiteConnection cnn = Database.GetConnection();

                // change password to new database
                cnn.ChangePassword(databasePassword);

                // create database structure
                CreateSchema(cnn);
            }
            catch (Exception ex)
            {
                OnErrorEncountered(new ErrorEncounteredEventArgs(ex.Message));
                // todo: handle
                throw;
            }
        }

        private void CreateSchema(SQLiteConnection cnn)
        {
            string schema = Path.Combine(SqlScriptPath, "schema-" + DatabaseVersion + ".sql");
            if (File.Exists(schema))
            {
                using (SQLiteCommand command = cnn.CreateCommand())
                using (SQLiteTransaction transaction = cnn.BeginTransaction())
                {
                    command.Transaction = transaction;
                    command.CommandText = File.ReadAllText(schema);
                    command.ExecuteNonQuery();
                    transaction.Commit();
                }
            }
            else
            {
                OnErrorEncountered(new ErrorEncounteredEventArgs("Could not find DFM database schema"));
                throw new ApplicationException("Could not find DFM database schema");
            }
        }

        private void UpgradeDatabase()
        {
            string prev_db_version = "";
            string db_version = "";
            string upgrade_file;
            SQLiteConnection cnn = Database.GetConnection();

            // create new transaction scope
            using (SQLiteCommand command = cnn.CreateCommand())
            using (SQLiteTransaction transaction = cnn.BeginTransaction())
            {
                try
                {
                    do
                    {
                        prev_db_version = db_version;
                        db_version = GetDatabaseVersion(Database);

                        if (db_version == DatabaseVersion)
                            break;

                        upgrade_file = Path.Combine(SqlScriptPath, Path.Combine("upgrade", "upgrade-" + db_version + ".sql"));
                        if (!File.Exists(upgrade_file))
                            break;

                        command.Transaction = transaction;
                        command.CommandText = File.ReadAllText(upgrade_file);
                        command.ExecuteNonQuery();
                    }
                    while (db_version != prev_db_version);

                    if (db_version != DatabaseVersion)
                        throw new ApplicationException("Could not update DB to latest version.");
                }
                catch (Exception ex)
                {
                    OnErrorEncountered(new ErrorEncounteredEventArgs(ex.Message));
                    transaction.Rollback();
                    throw;
                }

                transaction.Commit();
            }
        }

        private string GetDatabaseVersion(SQLiteDatabase db)
        {
            return db.ExecuteScalar("SELECT [value] FROM [keystore] WHERE [key] = 'database_version';");
        }
    }
}
