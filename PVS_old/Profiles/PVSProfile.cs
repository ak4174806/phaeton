﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

using Analysis;
using PaintAnalysis.Common;

namespace PVS
{
    public class PVSProfile : IProfile
    {
        public PVSProfile(string profileName) : this()
        {
            ProfileName = profileName;
        }

        public PVSProfile() { }
          

        private string profileName;
        public string ProfileName
        {
            get { return profileName; }
            set { profileName = value; }
        }

        #region Members

        #region Sensor Connection

        private string sensorModel;
        /// <summary>
        /// The model of the sensor being used
        /// </summary>
        public string SensorModel
        {
            get { return sensorModel; }
            set { sensorModel = value; }
        }

        private string sensorType;
        /// <summary>
        /// The type of the sensor being used
        /// </summary>
        public string SensorType
        {
            get { return sensorType; }
            set { sensorType = value; }
        }

        private string comPort;
        /// <summary>
        /// The COM port for connecting to the sensor
        /// </summary>
        public string ComPort
        {
            get { return comPort; }
            set { comPort = value; }
        }

        private string sensorIPAddress;
        /// <summary>
        /// The IP address for connecting ethernet sensors
        /// </summary>
        public string SensorIPAddress
        {
            get { return sensorIPAddress; }
            set { sensorIPAddress = value; }
        }

        private int baudRate;
        /// <summary>
        /// The baudrate for connecting to the sensor, 0 => autodetect
        /// </summary>
        public int BaudRate
        {
            get { return baudRate; }
            set { baudRate = value; }
        }

        #endregion

        #region Analysis Options

        private PVSAnalysisOptions analysisOptions;
        /// <summary>
        /// The options for analysis
        /// </summary>
        public PVSAnalysisOptions AnalysisOptions
        {
            get
            {
                if (analysisOptions == null)
                    analysisOptions = new PVSAnalysisOptions();
                return analysisOptions;
            }
            set { analysisOptions = value; }
        }

        // whether to discard a certain amount of time worth of data from each end
        private bool timeDiscarded;
        public bool TimeDiscarded
        {
            get { return timeDiscarded; }
            set { timeDiscarded = value; }
        }

        // the amount of time in seconds of data to discard
        private decimal discardTime;
        public decimal DiscardTime
        {
            get { return discardTime; }
            set { discardTime = value; }
        }

        // whether to discard a percentage of the total from each end
        private bool totalDiscarded;
        public bool TotalDiscarded
        {
            get { return totalDiscarded; }
            set { totalDiscarded = value; }
        }

        // the percentage (between 0 and 100) to discard from each end
        private decimal discardTotal;
        public decimal DiscardTotal
        {
            get { return discardTotal; }
            set { discardTotal = value; }
        }


        // in microns
        private decimal peakThreshold;
        public decimal PeakThreshold
        {
            get { return peakThreshold; }
            set { peakThreshold = value; }
        }

        private bool filterPeaks;
        public bool FilterPeaks
        {
            get { return filterPeaks; }
            set { filterPeaks = value; }
        }
        

        public enum VolumeCalculationMethods
        {
            Volume,
            Thickness
        }
        private VolumeCalculationMethods volumeCalculationMethod;
        public VolumeCalculationMethods VolumeCalculationMethod
        {
            get { return volumeCalculationMethod; }
            set { volumeCalculationMethod = value; }
        }

        #endregion

        #region Scan Options

        private string measurementFrequency;
        /// <summary>
        /// The frequency of measurements taken by the sensor
        /// </summary>
        public string MeasurementFrequency
        {
            get { return measurementFrequency; }
            set { measurementFrequency = value; }
        }

        private int downscaleFactor;
        /// <summary>
        /// The factor by which to downscale the frequency if DownscaleFrequency is true
        /// </summary>
        public int DownscaleFactor
        {
            get { return downscaleFactor; }
            set { downscaleFactor = value; }
        }

        /// <summary>
        /// The methods which are available to downscale the frequency.
        /// Averaging - Average in blocks of size downscaleFactor
        /// Filtering - Only keep every downscaleFactor'th point
        /// None - Don't downscale
        /// </summary>
        public enum DownscaleMethods
        {
            Averaging,
            Filtering,
            None
        }

        private DownscaleMethods downscaleMethod;
        /// <summary>
        /// The method used to downscale frequency
        /// </summary>
        public DownscaleMethods DownscaleMethod
        {
            get { return downscaleMethod; }
            set { downscaleMethod = value; }
        }

        private string filenamePattern;
        /// <summary>
        /// A pattern to use when saving each file
        /// </summary>
        public string FilenamePattern
        {
            get { return filenamePattern; }
            set { filenamePattern = value; }
        }

        private string dataLocation;
        /// <summary>
        /// The folder in which to save data files
        /// </summary>
        public string DataLocation
        {
            get { return dataLocation; }
            set { dataLocation = value; }
        }

        private int numberScans;
        /// <summary>
        /// The number of wet and dry scans to run
        /// </summary>
        public int NumberScans
        {
            get { return numberScans; }
            set { numberScans = value; }
        }

        private int numberReadingsAveraged;
        /// <summary>
        /// The number of readings to average and display at a time in auto mode
        /// </summary>
        public int NumberReadingsAveraged
        {
            get { return numberReadingsAveraged; }
            set { numberReadingsAveraged = value; }
        }

        #endregion

        #region Graph Display Options

        /// <summary>
        /// Whether to show the raw data, or the processed profile on the graphs
        /// </summary>
        public enum GraphDataSourceOptions
        {
            Data,
            Profile
        }

        private GraphDataSourceOptions graphDataSource;
        /// <summary>
        /// Gets or sets the data source used for the graph: the raw data or the processed profile
        /// </summary>
        public GraphDataSourceOptions GraphDataSource
        {
            get { return graphDataSource; }
            set { graphDataSource = value; }
        }
        
        /// <summary>
        /// Whether to show only wet, dry or both scans
        /// </summary>
        public enum GraphDataTypeOptions
        {
            Wet,
            Dry,
            Both
        }

        private GraphDataTypeOptions graphDataType;
        /// <summary>
        /// Gets or sets the type of scans to show on the graph: wet, dry or both
        /// </summary>
        public GraphDataTypeOptions GraphDataType
        {
            get { return graphDataType; }
            set { graphDataType = value; }
        }


        private bool invertFirstGraph;
        /// <summary>
        /// Whether to horizontally invert the first graph displayed
        /// </summary>
        public bool InvertFirstGraph
        {
            get { return invertFirstGraph; }
            set { invertFirstGraph = value; }
        }

        private bool invertAlternateGraphs;
        /// <summary>
        /// Whether to horizontally invert alternate graphs
        /// </summary>
        public bool InvertAlternateGraphs
        {
            get { return invertAlternateGraphs; }
            set { invertAlternateGraphs = value; }
        }

        private bool smoothGraphs;
        /// <summary>
        /// Whether to display smooth or raw graphs
        /// </summary>
        public bool SmoothGraphs
        {
            get { return smoothGraphs; }
            set { smoothGraphs = value; }
        }
        
        #endregion

        #endregion

        #region IXmlSerializable

        public XmlSchema GetSchema() { return null; }

        public void ReadXml(XmlReader reader)
        {
            if (reader.MoveToContent() == XmlNodeType.Element && reader.LocalName == "PVSProfile")
            {
                ProfileName = reader["ProfileName"];

                SensorModel = reader["SensorModel"];
                SensorType = reader["SensorType"];
                ComPort = reader["ComPort"];
                SensorIPAddress = reader["SensorIPAddress"];
                BaudRate = Convert.ToInt32(reader["BaudRate"]);

                TimeDiscarded = Convert.ToBoolean(reader["TimeDiscarded"]);
                DiscardTime = Convert.ToDecimal(reader["DiscardTime"]);
                TotalDiscarded = Convert.ToBoolean(reader["TotalDiscarded"]);
                DiscardTotal = Convert.ToDecimal(reader["DiscardTotal"]);
                PeakThreshold = Convert.ToDecimal(reader["PeakThreshold"]);
                FilterPeaks = Convert.ToBoolean(reader["FilterPeaks"]);
                VolumeCalculationMethod = (VolumeCalculationMethods)Enum.Parse(typeof(VolumeCalculationMethods), reader["VolumeCalculationMethod"]);

                MeasurementFrequency = reader["MeasurementFrequency"];
                DownscaleFactor = Convert.ToInt32(reader["DownscaleFactor"]);
                DownscaleMethod = (DownscaleMethods)Enum.Parse(typeof(DownscaleMethods), reader["DownscaleMethod"]);
                FilenamePattern = reader["FilenamePattern"];
                DataLocation = reader["DataLocation"];
                NumberScans = Convert.ToInt32(reader["NumberScans"]);
                NumberReadingsAveraged = Convert.ToInt32(reader["NumberReadingsAveraged"]);

                GraphDataSource = (GraphDataSourceOptions)Enum.Parse(typeof(GraphDataSourceOptions), reader["GraphDataSource"]);
                GraphDataType = (GraphDataTypeOptions)Enum.Parse(typeof(GraphDataTypeOptions), reader["GraphDataType"]);
                InvertFirstGraph = Convert.ToBoolean(reader["InvertFirstGraph"]);
                InvertAlternateGraphs = Convert.ToBoolean(reader["InvertAlternateGraphs"]);
                SmoothGraphs = Convert.ToBoolean(reader["SmoothGraphs"]);

                // analysis options
                if (reader.ReadToDescendant("PVSAnalysisOptions"))
                {
                    if (reader.MoveToContent() == XmlNodeType.Element && reader.LocalName == "PVSAnalysisOptions")
                    {
                        AnalysisOptions = new PVSAnalysisOptions();
                        AnalysisOptions.ReadXml(reader);
                    }
                }

                reader.Read();
            }
        }

        public void WriteXml(XmlWriter writer)
        {
            writer.WriteAttributeString("ProfileName", ProfileName);

            writer.WriteAttributeString("SensorModel", SensorModel);
            writer.WriteAttributeString("SensorType", SensorType);
            writer.WriteAttributeString("ComPort", ComPort);
            writer.WriteAttributeString("SensorIPAddress", SensorIPAddress);
            writer.WriteAttributeString("BaudRate", BaudRate.ToString());

            writer.WriteAttributeString("TimeDiscarded", TimeDiscarded.ToString());
            writer.WriteAttributeString("DiscardTime", DiscardTime.ToString());
            writer.WriteAttributeString("TotalDiscarded", TotalDiscarded.ToString());
            writer.WriteAttributeString("DiscardTotal", DiscardTotal.ToString());
            writer.WriteAttributeString("PeakThreshold", PeakThreshold.ToString());
            writer.WriteAttributeString("FilterPeaks", FilterPeaks.ToString());
            writer.WriteAttributeString("VolumeCalculationMethod", VolumeCalculationMethod.ToString());

            writer.WriteAttributeString("MeasurementFrequency", MeasurementFrequency);
            writer.WriteAttributeString("DownscaleFactor", DownscaleFactor.ToString());
            writer.WriteAttributeString("DownscaleMethod", DownscaleMethod.ToString());
            writer.WriteAttributeString("FilenamePattern", FilenamePattern);
            writer.WriteAttributeString("DataLocation", DataLocation);
            writer.WriteAttributeString("NumberScans", NumberScans.ToString());
            writer.WriteAttributeString("NumberReadingsAveraged", NumberReadingsAveraged.ToString());

            writer.WriteAttributeString("GraphDataSource", GraphDataSource.ToString());
            writer.WriteAttributeString("GraphDataType", GraphDataType.ToString());
            writer.WriteAttributeString("InvertFirstGraph", InvertFirstGraph.ToString());
            writer.WriteAttributeString("InvertAlternateGraphs", InvertAlternateGraphs.ToString());
            writer.WriteAttributeString("SmoothGraphs", SmoothGraphs.ToString());
            
            // analysis options
            writer.WriteStartElement("PVSAnalysisOptions");
            AnalysisOptions.WriteXml(writer);
            writer.WriteEndElement();
        }

        #endregion

        public IOptionsGroup DeepCopy()
        {
            PVSProfile copy = (PVSProfile)MemberwiseClone();
            copy.AnalysisOptions = (PVSAnalysisOptions)AnalysisOptions.DeepCopy();
            return copy;
        }


        public IProfile GetNew()
        {
            PVSProfile currentSettings = new PVSProfile();

            currentSettings.AnalysisOptions = new PVSAnalysisOptions();
            currentSettings.AnalysisOptions.ThresholdParameter = (decimal)0.2;
            currentSettings.AnalysisOptions.SmoothingWidth = 70;
            currentSettings.AnalysisOptions.A = 1;
            currentSettings.AnalysisOptions.B = 1;
            currentSettings.AnalysisOptions.C = 2;
            currentSettings.AnalysisOptions.D = 5;
            currentSettings.AnalysisOptions.ThermalFactor = 1;
            currentSettings.AnalysisOptions.CalibrationFactor = 1;

            currentSettings.DiscardTime = (decimal)1;
            currentSettings.TimeDiscarded = true;
            currentSettings.DiscardTotal = (decimal)15;
            currentSettings.TotalDiscarded = true;
            currentSettings.FilterPeaks = false;
            currentSettings.PeakThreshold = (decimal)10.0;
            currentSettings.VolumeCalculationMethod = PVSProfile.VolumeCalculationMethods.Thickness;

            currentSettings.DownscaleMethod = PVSProfile.DownscaleMethods.None;
            currentSettings.DownscaleFactor = 1;
            currentSettings.FilenamePattern = "{type}-{x} {nnnn}.csv";
            currentSettings.DataLocation = @"c:\";
            currentSettings.NumberScans = 1;
            currentSettings.NumberReadingsAveraged = 2;

            currentSettings.SensorModel = "";
            currentSettings.SensorType = "";
            currentSettings.ComPort = "";
            currentSettings.SensorIPAddress = "";
            currentSettings.BaudRate = 0;
            currentSettings.MeasurementFrequency = "";

            currentSettings.GraphDataSource = GraphDataSourceOptions.Profile;
            currentSettings.GraphDataType = GraphDataTypeOptions.Both;
            currentSettings.InvertFirstGraph = false;
            currentSettings.InvertAlternateGraphs = true;
            currentSettings.SmoothGraphs = true;

            return currentSettings;
        }
    }
}
