﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using PaintAnalysis.Common;

namespace PVS
{
    public partial class PVSOptionsForm : PVSOptionsFormIntermediary
    {
        public PVSOptionsForm()
        {
            InitializeComponent();

            // set title
            Title = "PVS Settings";

            ResizeForm();
        }

        private void ResizeForm()
        {
            if (pvsOptionsControl.RecommendedSize.Width == 0 || pvsOptionsControl.RecommendedSize.Height == 0)
                return; 

            Size newSize = new Size(Math.Max(Width, Width - panel1.Width + pvsOptionsControl.RecommendedSize.Width),
                Height - panel1.Height + pvsOptionsControl.RecommendedSize.Height);

            this.Size = newSize;
            this.panel1.Size = pvsOptionsControl.RecommendedSize;
            pvsOptionsControl.Size = pvsOptionsControl.RecommendedSize;
        }


        public override PVSOptionsControl OptionsControls
        {
            get
            {
                return pvsOptionsControl;
            }
        }

        public PVSProfileControl ProfileControls
        {
            get { return OptionsControls.ProfileManager.ProfileControls; }
        }


        public override void SaveOptions()
        {
            Properties.Settings.Default.PVSOptions = CurrentOptions;
            Properties.Settings.Default.Save();
        }

        public override void LoadOptions()
        {
            PVSOptions options = Properties.Settings.Default.PVSOptions;
            if (options == null)
                options = new PVSOptions();

            CurrentOptions = options;
        }

        public override bool ValidateSettings()
        {
            return pvsOptionsControl.Valid;
        }
    }

    // intermediary class to work around designer bug http://stackoverflow.com/questions/2546699/illegal-characters-in-path-visual-studio-winform-design-view
    public class PVSOptionsFormIntermediary : BaseOptionsForm<PVSOptions, PVSOptionsControl>
    { }
}
