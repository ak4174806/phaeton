﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

using PaintAnalysis.Common;

namespace PVS
{
    public partial class PVSOptionsControl : BaseOptionsGroupControl
    {
        public PVSOptionsControl()
        {
            InitializeComponent();

            profileManager = new ProfileManagerControl<PVSProfile, PVSProfileControl>();
            profileManager.Location = new Point(0, 0);
            profileManager.Name = "profileManager";
            Size = profileManager.RecommendedSize;
            RecommendedSize = Size;
            profileManager.Size = profileManager.RecommendedSize;
            this.Controls.Add(profileManager);
        }

        ProfileManagerControl<PVSProfile, PVSProfileControl> profileManager;

        public override void Init()
        {
            base.Init();

            profileManager.Init();
            profileManager.OptionsChanged += new EventHandler(raiseOptionsChanged);
        }

        public override void InitData()
        {
            base.InitData();

            profileManager.InitData();
        }

        public override IOptionsGroup CurrentOptions
        {
            get
            {
                PVSOptions options = new PVSOptions();
                options.ProfileList = profileManager.ProfileList;
                return options;
            }
            set
            {
                PVSOptions options = (PVSOptions)value;
                profileManager.ProfileList = options.ProfileList;
            }
        }

        public override bool Valid
        {
            get
            {
                return profileManager.Valid;
            }
        }


        public ProfileManagerControl<PVSProfile, PVSProfileControl> ProfileManager
        {
            get { return profileManager; }
        }
    }
}
