﻿namespace PVS
{
    partial class PVSOptionsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            PVS.PVSOptions pvsOptions1 = new PVS.PVSOptions();
            this.pvsOptionsControl = new PVS.PVSOptionsControl();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblSettingsTitle
            // 
            this.lblSettingsTitle.Size = new System.Drawing.Size(157, 24);
            this.lblSettingsTitle.Text = "PVSOptionsForm";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.pvsOptionsControl);
            this.panel1.Size = new System.Drawing.Size(289, 125);
            // 
            // pvsOptionsControl
            // 
            this.pvsOptionsControl.CurrentOptions = pvsOptions1;
            this.pvsOptionsControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pvsOptionsControl.Location = new System.Drawing.Point(0, 0);
            this.pvsOptionsControl.Name = "pvsOptionsControl";
            this.pvsOptionsControl.Size = new System.Drawing.Size(289, 125);
            this.pvsOptionsControl.TabIndex = 0;
            // 
            // PVSOptionsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(301, 209);
            this.Name = "PVSOptionsForm";
            this.Text = "PVSOptionsForm";
            this.Title = "PVSOptionsForm";
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private PVSOptionsControl pvsOptionsControl;

    }
}