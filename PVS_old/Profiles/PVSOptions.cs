﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

using PaintAnalysis.Common;

namespace PVS
{
    public class PVSOptions : IOptionsGroup
    {
        private SelectableProfileList<PVSProfile> profileList;
        public SelectableProfileList<PVSProfile> ProfileList
        {
            get
            {
                if (profileList == null)
                    profileList = new SelectableProfileList<PVSProfile>();

                return profileList;
            }
            set
            {
                profileList = value;
            }
        }

        #region IXmlSerializable

        public XmlSchema GetSchema() { return null; }

        public void ReadXml(XmlReader reader)
        {
            if (reader.MoveToContent() == XmlNodeType.Element && reader.LocalName == "PVSOptions")
            {
                if (reader.ReadToDescendant("SelectableProfileList"))
                {
                    if (reader.MoveToContent() == XmlNodeType.Element && reader.LocalName == "SelectableProfileList" && reader["TProfile"] == "PVSProfile")
                    {
                        profileList = new SelectableProfileList<PVSProfile>();
                        profileList.ReadXml(reader);
                    }
                }
                reader.Read();
            }
        }

        public void WriteXml(XmlWriter writer)
        {
            // need to expand tag as can't have <T> in tag
            writer.WriteStartElement("SelectableProfileList");
            writer.WriteAttributeString("TProfile", "PVSProfile");
            ProfileList.WriteXml(writer);
            writer.WriteEndElement();
        }

        #endregion

        public IOptionsGroup DeepCopy()
        {
            PVSOptions copy = new PVSOptions();
            copy.ProfileList = (SelectableProfileList<PVSProfile>)ProfileList.DeepCopy();
            return copy;
        }
    }
}
