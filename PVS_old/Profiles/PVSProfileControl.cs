﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

using Analysis;
using SensorControl;
using SensorControl.Sensors;
using System.IO;
using System.IO.Ports;
using PaintAnalysis.Common;

namespace PVS
{
    public partial class PVSProfileControl : BaseOptionsGroupControl
    {
        public PVSProfileControl()
        {
            InitializeComponent();
        }

        public override IOptionsGroup CurrentOptions
        {
            get
            {
                PVSProfile profile = new PVSProfile();
                
                // no profile name

                profile.SensorModel = cmbSensorModel.SelectedItem.ToString();
                profile.SensorType = cmbSensorType.SelectedItem.ToString();
                profile.ComPort = cmbCOM.SelectedItem.ToString();
                profile.SensorIPAddress = txtSensorAddress.Text;
                if (cmbBaudRate.SelectedIndex == 0)
                    profile.BaudRate = 0;
                else
                    profile.BaudRate = Convert.ToInt32(cmbBaudRate.SelectedItem.ToString());

                profile.AnalysisOptions = new PVSAnalysisOptions();
                profile.AnalysisOptions.SmoothingWidth = (int)numSmoothingWidth.Value;
                profile.AnalysisOptions.ThresholdParameter = numThresholdParam.Value;
                profile.AnalysisOptions.A = (double)numParamA.Value;
                profile.AnalysisOptions.B = (double)numParamB.Value;
                profile.AnalysisOptions.C = (double)numParamC.Value;
                profile.AnalysisOptions.D = (double)numParamD.Value;
                profile.AnalysisOptions.ThermalFactor = (double)numThermalFactor.Value;
                profile.AnalysisOptions.CalibrationFactor = (double)numCalibrationFactor.Value;

                profile.DiscardTime = numDiscardTime.Value;
                profile.DiscardTotal = numDiscardTotal.Value;
                profile.TimeDiscarded = chkDiscardTime.Checked;
                profile.TotalDiscarded = chkDiscardTotal.Checked;
                profile.PeakThreshold = numPeakThreshold.Value;
                profile.FilterPeaks = chkFilterPeaks.Checked;
                if (cmbVolumeMethod.SelectedItem.ToString().ToLower() == "volume")
                    profile.VolumeCalculationMethod = PVSProfile.VolumeCalculationMethods.Volume;
                else
                    profile.VolumeCalculationMethod = PVSProfile.VolumeCalculationMethods.Thickness;

                profile.MeasurementFrequency = cmbMeasurementFrequency.SelectedItem.ToString();
                if (!chkDownscaleFrequency.Checked)
                {
                    profile.DownscaleMethod = PVSProfile.DownscaleMethods.None;
                }
                else if (radDownscaleAveraging.Checked)
                {
                    profile.DownscaleMethod = PVSProfile.DownscaleMethods.Averaging;
                }
                else
                {
                    profile.DownscaleMethod = PVSProfile.DownscaleMethods.Filtering;
                }
                profile.DownscaleFactor = (int)numDownscaleFactor.Value;

                profile.FilenamePattern = txtFilenamePattern.Text;
                profile.DataLocation = txtDataLocation.Text;
                profile.NumberScans = (chkMultiScan.Checked ? 3 : 1);
                profile.NumberReadingsAveraged = Convert.ToInt32(numReadingsAveraged.Value);

                if (radShowProfile.Checked)
                    profile.GraphDataSource = PVSProfile.GraphDataSourceOptions.Profile;
                else
                    profile.GraphDataSource = PVSProfile.GraphDataSourceOptions.Data;
                
                if (radShowWet.Checked)
                    profile.GraphDataType = PVSProfile.GraphDataTypeOptions.Wet;
                else if (radShowDry.Checked)
                    profile.GraphDataType = PVSProfile.GraphDataTypeOptions.Dry;
                else
                    profile.GraphDataType = PVSProfile.GraphDataTypeOptions.Both;

                profile.InvertFirstGraph = chkInvertFirst.Checked;
                profile.InvertAlternateGraphs = chkInvertAlternate.Checked;
                profile.SmoothGraphs = chkSmoothGraphs.Checked;

                return profile;
            }
            set
            {
                // pause options changed notifications
                SuppressOptionsChanged = true;

                PVSProfile profile = value as PVSProfile;
                if (profile == null)
                {
                    throw new ArgumentException("Did not receive a PVSProfile");
                }

                cmbSensorModel.SelectedItem = profile.SensorModel;
                cmbSensorType.SelectedItem = profile.SensorType;
                cmbCOM.SelectedItem = profile.ComPort;
                txtSensorAddress.Text = profile.SensorIPAddress;
                if (cmbBaudRate.Items.Count > 0)
                {
                    if (profile.BaudRate == 0)
                        cmbBaudRate.SelectedIndex = 0;
                    else
                        cmbBaudRate.SelectedItem = profile.BaudRate.ToString();
                }
                // update measurement frequencies

                updateSelectedAddressType();

                numSmoothingWidth.Value = (decimal)profile.AnalysisOptions.SmoothingWidth;
                numThresholdParam.Value = (decimal)profile.AnalysisOptions.ThresholdParameter;
                numParamA.Value = (decimal)profile.AnalysisOptions.A;
                numParamB.Value = (decimal)profile.AnalysisOptions.B;
                numParamC.Value = (decimal)profile.AnalysisOptions.C;
                numParamD.Value = (decimal)profile.AnalysisOptions.D;
                numThermalFactor.Value = (decimal)profile.AnalysisOptions.ThermalFactor;
                numCalibrationFactor.Value = (decimal)profile.AnalysisOptions.CalibrationFactor;

                numDiscardTime.Value = profile.DiscardTime;
                numDiscardTotal.Value = profile.DiscardTotal;
                chkDiscardTime.Checked = profile.TimeDiscarded;
                chkDiscardTotal.Checked = profile.TotalDiscarded;

                numDiscardTime.Enabled = profile.TimeDiscarded;
                numDiscardTotal.Enabled = profile.TotalDiscarded;

                numPeakThreshold.Value = profile.PeakThreshold;
                chkFilterPeaks.Checked = profile.FilterPeaks;
                numPeakThreshold.Enabled = chkFilterPeaks.Checked;

                if (cmbVolumeMethod.Items.Count > 0)
                {
                    if (profile.VolumeCalculationMethod == PVSProfile.VolumeCalculationMethods.Thickness)
                    {
                        cmbVolumeMethod.SelectedIndex = 0;
                    }
                    else
                    {
                        cmbVolumeMethod.SelectedIndex = 1;
                    }
                }

                cmbMeasurementFrequency.SelectedItem = profile.MeasurementFrequency;
                switch (profile.DownscaleMethod)
                {
                    case PVSProfile.DownscaleMethods.Averaging:
                        chkDownscaleFrequency.Checked = true;
                        radDownscaleAveraging.Checked = true;
                        radDownscaleFiltering.Checked = false;

                        radDownscaleAveraging.Enabled = true;
                        radDownscaleFiltering.Enabled = true;
                        break;

                    case PVSProfile.DownscaleMethods.Filtering:
                        chkDownscaleFrequency.Checked = true;
                        radDownscaleAveraging.Checked = false;
                        radDownscaleFiltering.Checked = true;

                        radDownscaleAveraging.Enabled = true;
                        radDownscaleFiltering.Enabled = true;
                        break;

                    case PVSProfile.DownscaleMethods.None:
                        chkDownscaleFrequency.Checked = false;

                        radDownscaleAveraging.Enabled = false;
                        radDownscaleFiltering.Enabled = false;
                        break;
                }
                radDownscaleAveraging.Enabled = chkDownscaleFrequency.Checked;
                radDownscaleFiltering.Enabled = chkDownscaleFrequency.Checked;

                numDownscaleFactor.Value = (decimal)profile.DownscaleFactor;

                txtFilenamePattern.Text = profile.FilenamePattern;
                txtDataLocation.Text = profile.DataLocation;
                chkMultiScan.Checked = (profile.NumberScans == 3);
                numReadingsAveraged.Value = Math.Max(numReadingsAveraged.Minimum, Math.Min(numReadingsAveraged.Maximum, profile.NumberReadingsAveraged));

                if (profile.GraphDataSource == PVSProfile.GraphDataSourceOptions.Profile)
                    radShowProfile.Checked = true;
                else
                    radShowData.Checked = true;

                if (profile.GraphDataType == PVSProfile.GraphDataTypeOptions.Both)
                    radShowBoth.Checked = true;
                else if (profile.GraphDataType == PVSProfile.GraphDataTypeOptions.Dry)
                    radShowDry.Checked = true;
                else
                    radShowWet.Checked = true;

                chkInvertFirst.Checked = profile.InvertFirstGraph;
                chkInvertAlternate.Checked = profile.InvertAlternateGraphs;
                chkSmoothGraphs.Checked = profile.SmoothGraphs;

                // resume events
                SuppressOptionsChanged = false;
            }
        }

        private void PVSProfileControl_Load(object sender, EventArgs e)
        {
            // add change handling events
            cmbSensorModel.SelectedIndexChanged += new EventHandler(raiseOptionsChanged);
            cmbSensorType.SelectedIndexChanged += new EventHandler(raiseOptionsChanged);
            cmbCOM.SelectedIndexChanged += new EventHandler(raiseOptionsChanged);
            txtSensorAddress.TextChanged += new EventHandler(raiseOptionsChanged);
            cmbBaudRate.SelectedIndexChanged += new EventHandler(raiseOptionsChanged);

            numThresholdParam.ValueChanged += new EventHandler(raiseOptionsChanged);
            numSmoothingWidth.ValueChanged += new EventHandler(raiseOptionsChanged);
            numParamA.ValueChanged += new EventHandler(raiseOptionsChanged);
            numParamB.ValueChanged += new EventHandler(raiseOptionsChanged);
            numParamC.ValueChanged += new EventHandler(raiseOptionsChanged);
            numParamD.ValueChanged += new EventHandler(raiseOptionsChanged);
            numThermalFactor.ValueChanged += new EventHandler(raiseOptionsChanged);
            numCalibrationFactor.ValueChanged += new EventHandler(raiseOptionsChanged);

            chkDiscardTotal.CheckedChanged += new EventHandler(raiseOptionsChanged);
            chkDiscardTime.CheckedChanged += new EventHandler(raiseOptionsChanged);
            numDiscardTime.ValueChanged += new EventHandler(raiseOptionsChanged);
            numDiscardTotal.ValueChanged += new EventHandler(raiseOptionsChanged);
            chkFilterPeaks.CheckedChanged += new EventHandler(raiseOptionsChanged);
            numPeakThreshold.ValueChanged += new EventHandler(raiseOptionsChanged);
            cmbVolumeMethod.SelectedIndexChanged += new EventHandler(raiseOptionsChanged);

            cmbMeasurementFrequency.SelectedIndexChanged += new EventHandler(raiseOptionsChanged);
            chkDownscaleFrequency.CheckedChanged += new EventHandler(raiseOptionsChanged);
            chkDownscaleFrequency.CheckedChanged += new EventHandler(raiseOptionsChanged);
            radDownscaleAveraging.CheckedChanged += new EventHandler(raiseOptionsChanged);
            radDownscaleFiltering.CheckedChanged += new EventHandler(raiseOptionsChanged);
            numDownscaleFactor.ValueChanged += new EventHandler(raiseOptionsChanged);

            txtFilenamePattern.TextChanged += new EventHandler(raiseOptionsChanged);
            txtDataLocation.TextChanged += new EventHandler(raiseOptionsChanged);
            chkMultiScan.CheckedChanged += new EventHandler(raiseOptionsChanged);
            numReadingsAveraged.ValueChanged += new EventHandler(raiseOptionsChanged);

            radShowData.CheckedChanged += new EventHandler(raiseOptionsChanged);
            radShowProfile.CheckedChanged += new EventHandler(raiseOptionsChanged);
            radShowWet.CheckedChanged += new EventHandler(raiseOptionsChanged);
            radShowDry.CheckedChanged += new EventHandler(raiseOptionsChanged);
            radShowBoth.CheckedChanged += new EventHandler(raiseOptionsChanged);
            chkInvertFirst.CheckedChanged += new EventHandler(raiseOptionsChanged);
            chkInvertAlternate.CheckedChanged += new EventHandler(raiseOptionsChanged);
            chkSmoothGraphs.CheckedChanged += new EventHandler(raiseOptionsChanged);

            // add other events
            btnRefreshCOM.Click += new EventHandler(btnRefreshCOM_Click);
            btnDataLocationBrowse.Click += new EventHandler(btnDataLocationBrowse_Click);
            btnConnect.Click += new EventHandler(btnConnect_Click);
            cmbSensorModel.SelectedIndexChanged += new EventHandler(cmbSensorModel_SelectedIndexChanged);
            chkDownscaleFrequency.CheckedChanged += new EventHandler(chkDownscaleFrequency_CheckedChanged);
            chkDiscardTime.CheckedChanged += new EventHandler(chkDiscardTime_CheckedChanged);
            chkDiscardTotal.CheckedChanged += new EventHandler(chkDiscardTotal_CheckedChanged);
            chkFilterPeaks.CheckedChanged += new EventHandler(delegate(object sender2, EventArgs e2)
            {
                numPeakThreshold.Enabled = chkFilterPeaks.Checked;
            });
            btnScanFields.Click += new EventHandler(btnScanFields_Click);

            // graph change events
            radShowData.CheckedChanged += new EventHandler(rad_graphDisplayChanged);
            radShowProfile.CheckedChanged += new EventHandler(rad_graphDisplayChanged);
            radShowWet.CheckedChanged += new EventHandler(rad_graphDisplayChanged);
            radShowDry.CheckedChanged += new EventHandler(rad_graphDisplayChanged);
            radShowBoth.CheckedChanged += new EventHandler(rad_graphDisplayChanged);
            chkInvertFirst.CheckedChanged += new EventHandler(chk_graphDisplayChanged);
            chkInvertAlternate.CheckedChanged += new EventHandler(chk_graphDisplayChanged);
            chkSmoothGraphs.CheckedChanged += new EventHandler(chk_graphDisplayChanged);
            
            // disable unused stuff - for now
            btnFilenamePatternHelp.Enabled = false;
        }

        public override void Init()
        {
            base.Init();

            // set up sensors
            string debugFile = Path.Combine(Path.GetDirectoryName(Application.ExecutablePath), "debug.txt");
            bool isDebugging = File.Exists(debugFile);
            if (!isDebugging)
            {
                sensors = new ISensor[3];
                sensors[0] = new ILD2200_MESensor();
                sensors[1] = new ILD1700_MESensor();
                sensors[2] = new ILD2300_MESensor();
            }
            else
            {
                sensors = new ISensor[4];
                sensors[0] = new ILD2200_MESensor();
                sensors[1] = new ILD1700_MESensor();
                sensors[2] = new ILD2300_MESensor();
                sensors[3] = new MockSensorFileScans(File.ReadAllText(debugFile));            
            }

            // populate combo boxes - all defaulted to first option
            updateSensorModels();
            updateSensorTypes();
            updateCOMPorts();
            updateBaudRates();
            updateMeasurementFrequencies();               

            cmbVolumeMethod.Items.Clear();
            cmbVolumeMethod.Items.AddRange(new string[] { "Thickness", "Volume" });
            cmbVolumeMethod.SelectedIndex = 0;

            // set defaults - later from saved stuff
            PVSProfile currentSettings = (PVSProfile)(new PVSProfile().GetNew());

            // to update the sensor models, etc which were all defaulted above, necessary to avoid assigning nulls when updating form
            currentSettings.SensorModel = cmbSensorModel.SelectedItem.ToString();
            currentSettings.SensorType = cmbSensorType.SelectedItem.ToString();
            
            if (cmbCOM.Items.Count > 0) currentSettings.ComPort = cmbCOM.SelectedItem.ToString();
            if (cmbBaudRate.SelectedIndex == 0)
            {
                currentSettings.BaudRate = 0;
            }
            else
            {
                currentSettings.BaudRate = Convert.ToInt32(cmbBaudRate.SelectedItem);
            }
            currentSettings.MeasurementFrequency = cmbMeasurementFrequency.SelectedItem.ToString();

            // update form from these defaults
            CurrentOptions = currentSettings;
            updateSelectedAddressType();     

            // match connected/disconnected state of parent form
            if (OpeningForm != null && ((PVS_Form)OpeningForm).ConnectionState == PVS_Form.ConnectionStates.Connected)
            {
                State = States.Connected;
            }
            else
            {
                State = States.Disconnected;
            }
        }
        
        public override void InitData()
        {
            base.InitData();
        }

        #region Helper Functions

        public PVS_Form OpeningForm;

        public override bool Valid
        {
            get { return validate(); }
        }
        
        #region State

        public enum States
        {
            Connected,
            Disconnected
        }
        private States state;
        public States State
        {
            get { return state; }
            set
            {
                state = value;
                if (value == States.Connected)
                {
                    btnConnect.Text = "Disconnect";
                    cmbSensorModel.Enabled = false;
                    cmbSensorType.Enabled = false;
                    cmbCOM.Enabled = false;
                    txtSensorAddress.Enabled = false;
                    cmbBaudRate.Enabled = false;
                    btnRefreshCOM.Enabled = false;
                }
                else
                {
                    btnConnect.Text = "Connect";
                    cmbSensorModel.Enabled = true;
                    cmbSensorType.Enabled = true;
                    cmbCOM.Enabled = true;
                    txtSensorAddress.Enabled = true;
                    cmbBaudRate.Enabled = true;
                    btnRefreshCOM.Enabled = true;
                }
            }
        }

        #endregion
        
        #region Sensor

        private ISensor[] sensors;
        /// <summary>
        /// Returns the currently selected sensor, using the form as a guide
        /// </summary>
        public ISensor Sensor
        {
            get
            {
                if (cmbSensorModel.SelectedIndex == -1) return null;

                ISensor sensor = sensors[cmbSensorModel.SelectedIndex];

                if (cmbSensorType.Items.Count > 0)
                {
                    bool contains = false;
                    foreach (string s in sensor.SensorTypes)
                    {
                        if (s == cmbSensorType.SelectedItem.ToString())
                        {
                            contains = true;
                            break;
                        }
                    }

                    if (contains)
                    {
                        sensor.SetSensorType(cmbSensorType.SelectedItem.ToString());
                    }
                }
                return sensor;
            }
        }
        
        // connect using form, also update settings and validate
        public bool ConnectSensor()
        {
            return ConnectSensor(false);
        }
        public bool ConnectSensor(bool suppressMessages)
        {
            // validate
            if (!Valid)
            {
                if (!suppressMessages) MessageBox.Show("There are some invalid settings. Correct them before connecting");
                return false;
            }

            try
            {
                PVSProfile settings = (PVSProfile)CurrentOptions;

                // connect to port
                string portName = settings.ComPort;
                if (SensorAddressType == SensorAddressTypes.IP_Address)
                    portName = settings.SensorIPAddress;

                if (settings.BaudRate == 0)
                {
                    // auto-detect baud rate
                    Sensor.Connect(portName);

                    // update selected baud rate
                    settings.BaudRate = Sensor.BaudRate;

                    // update form
                    CurrentOptions = settings;
                }
                else
                {
                    // use selected baud rate
                    int baudRate = settings.BaudRate;
                    Sensor.Connect(portName, baudRate);
                }

                // set reading reference to start of measuring range, rather than sensore
                Sensor.ReadingReference = ReadingReferences.SMR;
            }
            catch (Exception ex)
            {
                if (!suppressMessages) MessageBox.Show("There was an error connecting to the sensor. Try changing the COM port or baud rate. The sensor responded '" + ex.Message + "'");

                // disconnect if connected
                Sensor.Disconnect();

                return false;
            }

            return true;
        }

        public bool DisconnectSensor()
        {
            try
            {
                Sensor.Disconnect();
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Could not disconnect from sensor properly: " + ex.Message);
                return false;
            }
        }

        #endregion

        #region Update Functions

        private void updateSensorModels()
        {
            cmbSensorModel.Items.Clear();

            // add the sensor models
            foreach (ISensor s in sensors)
            {
                cmbSensorModel.Items.Add(s.Properties.ModelName);
            }

            cmbSensorModel.SelectedIndex = 0;

            // select ILD1700 by default
            for (int i = 0; i < cmbSensorModel.Items.Count; i++)
            {
                if (cmbSensorModel.Items[i].ToString() == "ILD1700")
                {
                    cmbSensorModel.SelectedIndex = i;
                    break;
                }
            }
        }

        private void updateSensorTypes()
        {
            cmbSensorType.Items.Clear();

            // add the sensor types
            foreach (string type in Sensor.SensorTypes)
            {
                cmbSensorType.Items.Add(type);
            }

            cmbSensorType.SelectedIndex = 0;
        }

        private void updateCOMPorts()
        {
            string[] ports = SerialPort.GetPortNames();
            Array.Sort(ports);

            cmbCOM.Items.Clear();
            cmbCOM.Items.AddRange(ports);
            
            if (ports.Length > 0)
                cmbCOM.SelectedIndex = 0;

            // select COM5 by default
            for (int i = 0; i < cmbCOM.Items.Count; i++)
            {
                if (cmbCOM.Items[i].ToString() == "COM5")
                {
                    cmbCOM.SelectedIndex = i;
                    break;
                }
            }
        }

        private void updateBaudRates()
        {
            cmbBaudRate.Items.Clear();
            cmbBaudRate.Items.Add("Auto detect");

            foreach (int i in Sensor.BaudRates)
            {
                cmbBaudRate.Items.Add(i.ToString());
            }

            cmbBaudRate.SelectedIndex = 0;
        }

        private void updateMeasurementFrequencies()
        {
            cmbMeasurementFrequency.Items.Clear();
            cmbMeasurementFrequency.Items.AddRange(Sensor.Frequencies);
            cmbMeasurementFrequency.SelectedIndex = 0;
        }
        
        private void updateSelectedAddressType()
        {
            ISensor selected = Sensor;
            if (selected != null)
            {
                SensorAddressType = selected.SensorAddressType;
            }
        }

        private SensorAddressTypes sensorAddressType;
        protected SensorAddressTypes SensorAddressType
        {
            get { return sensorAddressType; }
            set
            {
                sensorAddressType = value;
                if (sensorAddressType == SensorAddressTypes.IP_Address)
                {
                    txtSensorAddress.Visible = true;
                    cmbCOM.Visible = false;
                    lblSensorPort.Text = "IP Address";
                }
                else
                {
                    txtSensorAddress.Visible = false;
                    cmbCOM.Visible = true;
                    lblSensorPort.Text = "Port";
                }
            }
        }            

        #endregion
        
        #region Validation

        private bool validate()
        {
            bool valid = true;
            int firstErrorTab = tabControlScanSettings.TabCount;

            if (cmbSensorModel.SelectedItem.ToString().Trim() == "")
            {
                errorProvider.SetError(cmbSensorModel, "Please select a sensor model");
                valid = false;

                // focus tab 1
                tabControlScanSettings.SelectedIndex = 0;

                return valid; // exit condition
            }

            if (cmbSensorType.SelectedItem.ToString().Trim() == "")
            {
                errorProvider.SetError(cmbSensorType, "Please select a valid sensor type");
                valid = false;
                firstErrorTab = Math.Min(firstErrorTab, 0);
            }
            if (cmbCOM.SelectedItem.ToString().Trim() == "")
            {
                errorProvider.SetError(cmbCOM, "Please select a COM port for the sensor");
                valid = false;
                firstErrorTab = Math.Min(firstErrorTab, 0);
            }
            /*if (cmbBaudRate.SelectedItem.ToString().Trim() == "" || cmbBaudRate.SelectedIndex != 0)
            {
                errorProvider.SetError(cmbBaudRate, "Please select a valid baud rate");
                valid = false;
                firstErrorTab = Math.Min(firstErrorTab, 0);
            }*/
            if (cmbMeasurementFrequency.SelectedItem.ToString().Trim() == "")
            {
                errorProvider.SetError(cmbMeasurementFrequency, "Please select a valid frequency");
                valid = false;
                firstErrorTab = Math.Min(firstErrorTab, 2);
            }

            if (chkDownscaleFrequency.Checked && !radDownscaleAveraging.Checked && !radDownscaleFiltering.Checked)
            {
                errorProvider.SetError(chkDownscaleFrequency, "Please choose a downscaling method");
                valid = false;
                firstErrorTab = Math.Min(firstErrorTab, 2);
            }

            if (txtFilenamePattern.Text.Trim() == "")
            {
                errorProvider.SetError(txtFilenamePattern, "Please enter a filename pattern");
                valid = false;
                firstErrorTab = Math.Min(firstErrorTab, 2);
            }
            if (txtDataLocation.Text.Trim() == "")
            {
                errorProvider.SetError(txtDataLocation, "Please select a data location");
                valid = false;
                firstErrorTab = Math.Min(firstErrorTab, 2);
            }
            else if (!Directory.Exists(txtDataLocation.Text))
            {
                try
                {
                    Directory.CreateDirectory(txtDataLocation.Text);
                }
                catch
                {
                    errorProvider.SetError(txtDataLocation, "Could not create directory. Try another directory");
                    valid = false;
                    firstErrorTab = Math.Min(firstErrorTab, 2);
                }
            }


            // focus first error
            tabControlScanSettings.SelectedIndex = firstErrorTab;

            return valid;
        }

        private void clearErrors()
        {
            errorProvider.Clear();
        }

        #endregion

        #endregion

        #region Events

        void btnConnect_Click(object sender, EventArgs e)
        {
            if (!Valid)
            {
                MessageBox.Show("There are errors in your settings. Please correct them first");
                return;
            }

            if (State == States.Disconnected)
            {
                btnConnect.Enabled = false;

                bool connected = ConnectSensor();
                if (connected)
                {
                    // change form state
                    State = States.Connected;
                }

                btnConnect.Enabled = true;
            }
            else
            {
                btnConnect.Enabled = false;

                bool disconnected = DisconnectSensor();
                if (disconnected)
                {
                    // change form state
                    State = States.Disconnected;
                }

                btnConnect.Enabled = true;
            }
        }

        void chkDiscardTotal_CheckedChanged(object sender, EventArgs e)
        {
            numDiscardTotal.Enabled = chkDiscardTotal.Checked;
        }

        void chkDiscardTime_CheckedChanged(object sender, EventArgs e)
        {
            numDiscardTime.Enabled = chkDiscardTime.Checked;
        }

        void chkDownscaleFrequency_CheckedChanged(object sender, EventArgs e)
        {
            radDownscaleAveraging.Enabled = chkDownscaleFrequency.Checked;
            radDownscaleFiltering.Enabled = chkDownscaleFrequency.Checked;
        }

        void cmbSensorModel_SelectedIndexChanged(object sender, EventArgs e)
        {
            updateSensorTypes();
            updateBaudRates();
            updateMeasurementFrequencies();
            updateSelectedAddressType();
        }
        
        void btnRefreshCOM_Click(object sender, EventArgs e)
        {
            updateCOMPorts();
        }

        void btnDataLocationBrowse_Click(object sender, EventArgs e)
        {
            folderBrowser.ShowDialog();

            if (folderBrowser.SelectedPath != "")
            {
                txtDataLocation.Text = folderBrowser.SelectedPath;
            }
        }

        void chk_graphDisplayChanged(object sender, EventArgs e)
        {
            if (OpeningForm != null && ((PVS_Form)OpeningForm).ScanState == PVS_Form.ScanStates.ScanLoaded)
            {
                ((PVS_Form)OpeningForm).DrawGraphs();
            }                        
        }

        void rad_graphDisplayChanged(object sender, EventArgs e)
        {
            // so only fires once per set of radio buttons
            if (((RadioButton)sender).Checked == false) return;

            if (OpeningForm != null && ((PVS_Form)OpeningForm).ScanState == PVS_Form.ScanStates.ScanLoaded)
            {
                ((PVS_Form)OpeningForm).DrawGraphs();
            }
        }
        
        void btnScanFields_Click(object sender, EventArgs e)
        {
            ((PVS_Form)OpeningForm).OpenScanFieldsForm();
        }
                
        #endregion
    }
}
