﻿namespace PVS
{
    partial class SettingsDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label lblBaudRate;
            System.Windows.Forms.Label lblCOM;
            System.Windows.Forms.Label lblSensorType;
            System.Windows.Forms.Label lblSensorModel;
            System.Windows.Forms.Label lblParamD;
            System.Windows.Forms.Label lblParamC;
            System.Windows.Forms.Label lblParamB;
            System.Windows.Forms.Label lblParamA;
            System.Windows.Forms.Label lblSmoothingWidth;
            System.Windows.Forms.Label lblThresholdParam;
            System.Windows.Forms.Label lblDataLocation;
            System.Windows.Forms.GroupBox grpFrequency;
            System.Windows.Forms.Panel pnlDownscaleMethod;
            System.Windows.Forms.Label label2;
            System.Windows.Forms.Label label1;
            System.Windows.Forms.Label lblMeasurementFrequency;
            System.Windows.Forms.Label lblFilenamePattern;
            System.Windows.Forms.Label label4;
            System.Windows.Forms.Label label5;
            System.Windows.Forms.Label lblNumberScans;
            System.Windows.Forms.Label lblPeakThreshold;
            System.Windows.Forms.Label lblThermalFactor;
            System.Windows.Forms.Label lblCalibrationFactor;
            this.radDownscaleFiltering = new System.Windows.Forms.RadioButton();
            this.radDownscaleAveraging = new System.Windows.Forms.RadioButton();
            this.numDownscaleFactor = new System.Windows.Forms.NumericUpDown();
            this.chkDownscaleFrequency = new System.Windows.Forms.CheckBox();
            this.cmbMeasurementFrequency = new System.Windows.Forms.ComboBox();
            this.tabControlOuter = new System.Windows.Forms.TabControl();
            this.tabScanProfile = new System.Windows.Forms.TabPage();
            this.btnRestoreDefaults = new System.Windows.Forms.Button();
            this.btnDeleteProfile = new System.Windows.Forms.Button();
            this.btnSaveProfile = new System.Windows.Forms.Button();
            this.tabControlScanSettings = new System.Windows.Forms.TabControl();
            this.tabSensorConnection = new System.Windows.Forms.TabPage();
            this.btnConnect = new System.Windows.Forms.Button();
            this.cmbBaudRate = new System.Windows.Forms.ComboBox();
            this.cmbSensorModel = new System.Windows.Forms.ComboBox();
            this.btnRefreshCOM = new System.Windows.Forms.Button();
            this.cmbSensorType = new System.Windows.Forms.ComboBox();
            this.cmbCOM = new System.Windows.Forms.ComboBox();
            this.tabAnalysisOptions = new System.Windows.Forms.TabPage();
            this.chkFilterPeaks = new System.Windows.Forms.CheckBox();
            this.numPeakThreshold = new System.Windows.Forms.NumericUpDown();
            this.numDiscardTotal = new System.Windows.Forms.NumericUpDown();
            this.numDiscardTime = new System.Windows.Forms.NumericUpDown();
            this.chkDiscardTotal = new System.Windows.Forms.CheckBox();
            this.chkDiscardTime = new System.Windows.Forms.CheckBox();
            this.numParamD = new System.Windows.Forms.NumericUpDown();
            this.numParamC = new System.Windows.Forms.NumericUpDown();
            this.numParamB = new System.Windows.Forms.NumericUpDown();
            this.numCalibrationFactor = new System.Windows.Forms.NumericUpDown();
            this.numThermalFactor = new System.Windows.Forms.NumericUpDown();
            this.numParamA = new System.Windows.Forms.NumericUpDown();
            this.numSmoothingWidth = new System.Windows.Forms.NumericUpDown();
            this.numThresholdParam = new System.Windows.Forms.NumericUpDown();
            this.tabScanOptions = new System.Windows.Forms.TabPage();
            this.numNumberScans = new System.Windows.Forms.NumericUpDown();
            this.btnFilenamePatternHelp = new System.Windows.Forms.Button();
            this.txtFilenamePattern = new System.Windows.Forms.TextBox();
            this.btnDataLocationBrowse = new System.Windows.Forms.Button();
            this.txtDataLocation = new System.Windows.Forms.TextBox();
            this.radCustomProfile = new System.Windows.Forms.RadioButton();
            this.cmbProfile = new System.Windows.Forms.ComboBox();
            this.radUseProfile = new System.Windows.Forms.RadioButton();
            this.tabDatabase = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.btnOK = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.folderBrowser = new System.Windows.Forms.FolderBrowserDialog();
            this.errorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            lblBaudRate = new System.Windows.Forms.Label();
            lblCOM = new System.Windows.Forms.Label();
            lblSensorType = new System.Windows.Forms.Label();
            lblSensorModel = new System.Windows.Forms.Label();
            lblParamD = new System.Windows.Forms.Label();
            lblParamC = new System.Windows.Forms.Label();
            lblParamB = new System.Windows.Forms.Label();
            lblParamA = new System.Windows.Forms.Label();
            lblSmoothingWidth = new System.Windows.Forms.Label();
            lblThresholdParam = new System.Windows.Forms.Label();
            lblDataLocation = new System.Windows.Forms.Label();
            grpFrequency = new System.Windows.Forms.GroupBox();
            pnlDownscaleMethod = new System.Windows.Forms.Panel();
            label2 = new System.Windows.Forms.Label();
            label1 = new System.Windows.Forms.Label();
            lblMeasurementFrequency = new System.Windows.Forms.Label();
            lblFilenamePattern = new System.Windows.Forms.Label();
            label4 = new System.Windows.Forms.Label();
            label5 = new System.Windows.Forms.Label();
            lblNumberScans = new System.Windows.Forms.Label();
            lblPeakThreshold = new System.Windows.Forms.Label();
            lblThermalFactor = new System.Windows.Forms.Label();
            lblCalibrationFactor = new System.Windows.Forms.Label();
            grpFrequency.SuspendLayout();
            pnlDownscaleMethod.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numDownscaleFactor)).BeginInit();
            this.tabControlOuter.SuspendLayout();
            this.tabScanProfile.SuspendLayout();
            this.tabControlScanSettings.SuspendLayout();
            this.tabSensorConnection.SuspendLayout();
            this.tabAnalysisOptions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numPeakThreshold)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numDiscardTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numDiscardTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numParamD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numParamC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numParamB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numCalibrationFactor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numThermalFactor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numParamA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSmoothingWidth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numThresholdParam)).BeginInit();
            this.tabScanOptions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numNumberScans)).BeginInit();
            this.tabDatabase.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            this.SuspendLayout();
            // 
            // lblBaudRate
            // 
            lblBaudRate.AutoSize = true;
            lblBaudRate.Location = new System.Drawing.Point(19, 118);
            lblBaudRate.Name = "lblBaudRate";
            lblBaudRate.Size = new System.Drawing.Size(61, 13);
            lblBaudRate.TabIndex = 11;
            lblBaudRate.Text = "Baud Rate:";
            // 
            // lblCOM
            // 
            lblCOM.AutoSize = true;
            lblCOM.Location = new System.Drawing.Point(20, 87);
            lblCOM.Name = "lblCOM";
            lblCOM.Size = new System.Drawing.Size(56, 13);
            lblCOM.TabIndex = 10;
            lblCOM.Text = "COM Port:";
            // 
            // lblSensorType
            // 
            lblSensorType.AutoSize = true;
            lblSensorType.Location = new System.Drawing.Point(18, 54);
            lblSensorType.Name = "lblSensorType";
            lblSensorType.Size = new System.Drawing.Size(34, 13);
            lblSensorType.TabIndex = 2;
            lblSensorType.Text = "Type:";
            // 
            // lblSensorModel
            // 
            lblSensorModel.AutoSize = true;
            lblSensorModel.Location = new System.Drawing.Point(18, 22);
            lblSensorModel.Name = "lblSensorModel";
            lblSensorModel.Size = new System.Drawing.Size(75, 13);
            lblSensorModel.TabIndex = 0;
            lblSensorModel.Text = "Sensor Model:";
            // 
            // lblParamD
            // 
            lblParamD.AutoSize = true;
            lblParamD.Location = new System.Drawing.Point(227, 59);
            lblParamD.Name = "lblParamD";
            lblParamD.Size = new System.Drawing.Size(18, 13);
            lblParamD.TabIndex = 20;
            lblParamD.Text = "D:";
            // 
            // lblParamC
            // 
            lblParamC.AutoSize = true;
            lblParamC.Location = new System.Drawing.Point(227, 36);
            lblParamC.Name = "lblParamC";
            lblParamC.Size = new System.Drawing.Size(17, 13);
            lblParamC.TabIndex = 19;
            lblParamC.Text = "C:";
            // 
            // lblParamB
            // 
            lblParamB.AutoSize = true;
            lblParamB.Location = new System.Drawing.Point(227, 13);
            lblParamB.Name = "lblParamB";
            lblParamB.Size = new System.Drawing.Size(17, 13);
            lblParamB.TabIndex = 16;
            lblParamB.Text = "B:";
            // 
            // lblParamA
            // 
            lblParamA.AutoSize = true;
            lblParamA.Location = new System.Drawing.Point(6, 61);
            lblParamA.Name = "lblParamA";
            lblParamA.Size = new System.Drawing.Size(17, 13);
            lblParamA.TabIndex = 15;
            lblParamA.Text = "A:";
            // 
            // lblSmoothingWidth
            // 
            lblSmoothingWidth.AutoSize = true;
            lblSmoothingWidth.Location = new System.Drawing.Point(6, 37);
            lblSmoothingWidth.Name = "lblSmoothingWidth";
            lblSmoothingWidth.Size = new System.Drawing.Size(91, 13);
            lblSmoothingWidth.TabIndex = 12;
            lblSmoothingWidth.Text = "Smoothing Width:";
            // 
            // lblThresholdParam
            // 
            lblThresholdParam.AutoSize = true;
            lblThresholdParam.Location = new System.Drawing.Point(6, 13);
            lblThresholdParam.Name = "lblThresholdParam";
            lblThresholdParam.Size = new System.Drawing.Size(108, 13);
            lblThresholdParam.TabIndex = 11;
            lblThresholdParam.Text = "Threshold Parameter:";
            // 
            // lblDataLocation
            // 
            lblDataLocation.AutoSize = true;
            lblDataLocation.Location = new System.Drawing.Point(16, 154);
            lblDataLocation.Name = "lblDataLocation";
            lblDataLocation.Size = new System.Drawing.Size(77, 13);
            lblDataLocation.TabIndex = 21;
            lblDataLocation.Text = "Data Location:";
            // 
            // grpFrequency
            // 
            grpFrequency.Controls.Add(pnlDownscaleMethod);
            grpFrequency.Controls.Add(this.numDownscaleFactor);
            grpFrequency.Controls.Add(label1);
            grpFrequency.Controls.Add(this.chkDownscaleFrequency);
            grpFrequency.Controls.Add(this.cmbMeasurementFrequency);
            grpFrequency.Controls.Add(lblMeasurementFrequency);
            grpFrequency.Location = new System.Drawing.Point(18, 7);
            grpFrequency.Name = "grpFrequency";
            grpFrequency.Size = new System.Drawing.Size(303, 108);
            grpFrequency.TabIndex = 28;
            grpFrequency.TabStop = false;
            grpFrequency.Text = "Frequency";
            // 
            // pnlDownscaleMethod
            // 
            pnlDownscaleMethod.Controls.Add(this.radDownscaleFiltering);
            pnlDownscaleMethod.Controls.Add(this.radDownscaleAveraging);
            pnlDownscaleMethod.Controls.Add(label2);
            pnlDownscaleMethod.Location = new System.Drawing.Point(15, 65);
            pnlDownscaleMethod.Name = "pnlDownscaleMethod";
            pnlDownscaleMethod.Size = new System.Drawing.Size(268, 28);
            pnlDownscaleMethod.TabIndex = 37;
            // 
            // radDownscaleFiltering
            // 
            this.radDownscaleFiltering.AutoSize = true;
            this.radDownscaleFiltering.Location = new System.Drawing.Point(149, 6);
            this.radDownscaleFiltering.Name = "radDownscaleFiltering";
            this.radDownscaleFiltering.Size = new System.Drawing.Size(58, 17);
            this.radDownscaleFiltering.TabIndex = 34;
            this.radDownscaleFiltering.Text = "filtering";
            this.radDownscaleFiltering.UseVisualStyleBackColor = true;
            // 
            // radDownscaleAveraging
            // 
            this.radDownscaleAveraging.AutoSize = true;
            this.radDownscaleAveraging.Checked = true;
            this.radDownscaleAveraging.Location = new System.Drawing.Point(31, 6);
            this.radDownscaleAveraging.Name = "radDownscaleAveraging";
            this.radDownscaleAveraging.Size = new System.Drawing.Size(72, 17);
            this.radDownscaleAveraging.TabIndex = 33;
            this.radDownscaleAveraging.TabStop = true;
            this.radDownscaleAveraging.Text = "averaging";
            this.radDownscaleAveraging.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new System.Drawing.Point(4, 7);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(18, 13);
            label2.TabIndex = 32;
            label2.Text = "by";
            // 
            // numDownscaleFactor
            // 
            this.numDownscaleFactor.Location = new System.Drawing.Point(228, 46);
            this.numDownscaleFactor.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.numDownscaleFactor.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numDownscaleFactor.Name = "numDownscaleFactor";
            this.numDownscaleFactor.Size = new System.Drawing.Size(55, 20);
            this.numDownscaleFactor.TabIndex = 36;
            this.numDownscaleFactor.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new System.Drawing.Point(159, 50);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(63, 13);
            label1.TabIndex = 35;
            label1.Text = "by factor of:";
            // 
            // chkDownscaleFrequency
            // 
            this.chkDownscaleFrequency.AutoSize = true;
            this.chkDownscaleFrequency.Location = new System.Drawing.Point(16, 49);
            this.chkDownscaleFrequency.Name = "chkDownscaleFrequency";
            this.chkDownscaleFrequency.Size = new System.Drawing.Size(132, 17);
            this.chkDownscaleFrequency.TabIndex = 34;
            this.chkDownscaleFrequency.Text = "Downscale Frequency";
            this.chkDownscaleFrequency.UseVisualStyleBackColor = true;
            // 
            // cmbMeasurementFrequency
            // 
            this.cmbMeasurementFrequency.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbMeasurementFrequency.FormattingEnabled = true;
            this.cmbMeasurementFrequency.Location = new System.Drawing.Point(162, 17);
            this.cmbMeasurementFrequency.Name = "cmbMeasurementFrequency";
            this.cmbMeasurementFrequency.Size = new System.Drawing.Size(121, 21);
            this.cmbMeasurementFrequency.TabIndex = 32;
            // 
            // lblMeasurementFrequency
            // 
            lblMeasurementFrequency.AutoSize = true;
            lblMeasurementFrequency.Cursor = System.Windows.Forms.Cursors.SizeNESW;
            lblMeasurementFrequency.Location = new System.Drawing.Point(14, 20);
            lblMeasurementFrequency.Name = "lblMeasurementFrequency";
            lblMeasurementFrequency.Size = new System.Drawing.Size(127, 13);
            lblMeasurementFrequency.TabIndex = 33;
            lblMeasurementFrequency.Text = "Measurement Frequency:";
            // 
            // lblFilenamePattern
            // 
            lblFilenamePattern.AutoSize = true;
            lblFilenamePattern.Location = new System.Drawing.Point(16, 124);
            lblFilenamePattern.Name = "lblFilenamePattern";
            lblFilenamePattern.Size = new System.Drawing.Size(89, 13);
            lblFilenamePattern.TabIndex = 26;
            lblFilenamePattern.Text = "Filename Pattern:";
            // 
            // label4
            // 
            label4.Location = new System.Drawing.Point(6, 117);
            label4.Name = "label4";
            label4.Size = new System.Drawing.Size(68, 31);
            label4.TabIndex = 23;
            label4.Text = "Discard minimum of:";
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.Location = new System.Drawing.Point(253, 116);
            label5.Name = "label5";
            label5.Size = new System.Drawing.Size(75, 13);
            label5.TabIndex = 26;
            label5.Text = "from each end";
            // 
            // lblNumberScans
            // 
            lblNumberScans.AutoSize = true;
            lblNumberScans.Location = new System.Drawing.Point(16, 183);
            lblNumberScans.Name = "lblNumberScans";
            lblNumberScans.Size = new System.Drawing.Size(129, 13);
            lblNumberScans.TabIndex = 30;
            lblNumberScans.Text = "Number of wet/dry scans:";
            lblNumberScans.Visible = false;
            // 
            // lblPeakThreshold
            // 
            lblPeakThreshold.AutoSize = true;
            lblPeakThreshold.Location = new System.Drawing.Point(126, 173);
            lblPeakThreshold.Name = "lblPeakThreshold";
            lblPeakThreshold.Size = new System.Drawing.Size(127, 13);
            lblPeakThreshold.TabIndex = 27;
            lblPeakThreshold.Text = "Peak Threshold (microns)";
            // 
            // lblThermalFactor
            // 
            lblThermalFactor.AutoSize = true;
            lblThermalFactor.Location = new System.Drawing.Point(6, 85);
            lblThermalFactor.Name = "lblThermalFactor";
            lblThermalFactor.Size = new System.Drawing.Size(81, 13);
            lblThermalFactor.TabIndex = 15;
            lblThermalFactor.Text = "Thermal Factor:";
            // 
            // lblCalibrationFactor
            // 
            lblCalibrationFactor.AutoSize = true;
            lblCalibrationFactor.Location = new System.Drawing.Point(170, 86);
            lblCalibrationFactor.Name = "lblCalibrationFactor";
            lblCalibrationFactor.Size = new System.Drawing.Size(92, 13);
            lblCalibrationFactor.TabIndex = 15;
            lblCalibrationFactor.Text = "Calibration Factor:";
            // 
            // tabControlOuter
            // 
            this.tabControlOuter.Controls.Add(this.tabScanProfile);
            this.tabControlOuter.Controls.Add(this.tabDatabase);
            this.tabControlOuter.Location = new System.Drawing.Point(11, 12);
            this.tabControlOuter.Name = "tabControlOuter";
            this.tabControlOuter.SelectedIndex = 0;
            this.tabControlOuter.Size = new System.Drawing.Size(389, 359);
            this.tabControlOuter.TabIndex = 35;
            // 
            // tabScanProfile
            // 
            this.tabScanProfile.Controls.Add(this.btnRestoreDefaults);
            this.tabScanProfile.Controls.Add(this.btnDeleteProfile);
            this.tabScanProfile.Controls.Add(this.btnSaveProfile);
            this.tabScanProfile.Controls.Add(this.tabControlScanSettings);
            this.tabScanProfile.Controls.Add(this.radCustomProfile);
            this.tabScanProfile.Controls.Add(this.cmbProfile);
            this.tabScanProfile.Controls.Add(this.radUseProfile);
            this.tabScanProfile.Location = new System.Drawing.Point(4, 22);
            this.tabScanProfile.Name = "tabScanProfile";
            this.tabScanProfile.Padding = new System.Windows.Forms.Padding(3);
            this.tabScanProfile.Size = new System.Drawing.Size(381, 333);
            this.tabScanProfile.TabIndex = 0;
            this.tabScanProfile.Text = "Scan Profile";
            this.tabScanProfile.UseVisualStyleBackColor = true;
            // 
            // btnRestoreDefaults
            // 
            this.btnRestoreDefaults.Location = new System.Drawing.Point(254, 295);
            this.btnRestoreDefaults.Name = "btnRestoreDefaults";
            this.btnRestoreDefaults.Size = new System.Drawing.Size(104, 23);
            this.btnRestoreDefaults.TabIndex = 4;
            this.btnRestoreDefaults.Text = "Restore Defaults";
            this.btnRestoreDefaults.UseVisualStyleBackColor = true;
            // 
            // btnDeleteProfile
            // 
            this.btnDeleteProfile.Location = new System.Drawing.Point(125, 295);
            this.btnDeleteProfile.Name = "btnDeleteProfile";
            this.btnDeleteProfile.Size = new System.Drawing.Size(92, 23);
            this.btnDeleteProfile.TabIndex = 4;
            this.btnDeleteProfile.Text = "Delete Profile";
            this.btnDeleteProfile.UseVisualStyleBackColor = true;
            // 
            // btnSaveProfile
            // 
            this.btnSaveProfile.Location = new System.Drawing.Point(24, 295);
            this.btnSaveProfile.Name = "btnSaveProfile";
            this.btnSaveProfile.Size = new System.Drawing.Size(89, 23);
            this.btnSaveProfile.TabIndex = 4;
            this.btnSaveProfile.Text = "Save as Profile";
            this.btnSaveProfile.UseVisualStyleBackColor = true;
            // 
            // tabControlScanSettings
            // 
            this.tabControlScanSettings.Controls.Add(this.tabSensorConnection);
            this.tabControlScanSettings.Controls.Add(this.tabAnalysisOptions);
            this.tabControlScanSettings.Controls.Add(this.tabScanOptions);
            this.tabControlScanSettings.Location = new System.Drawing.Point(17, 55);
            this.tabControlScanSettings.Name = "tabControlScanSettings";
            this.tabControlScanSettings.SelectedIndex = 0;
            this.tabControlScanSettings.Size = new System.Drawing.Size(348, 234);
            this.tabControlScanSettings.TabIndex = 3;
            // 
            // tabSensorConnection
            // 
            this.tabSensorConnection.Controls.Add(this.btnConnect);
            this.tabSensorConnection.Controls.Add(lblSensorModel);
            this.tabSensorConnection.Controls.Add(this.cmbBaudRate);
            this.tabSensorConnection.Controls.Add(this.cmbSensorModel);
            this.tabSensorConnection.Controls.Add(lblBaudRate);
            this.tabSensorConnection.Controls.Add(lblSensorType);
            this.tabSensorConnection.Controls.Add(this.btnRefreshCOM);
            this.tabSensorConnection.Controls.Add(this.cmbSensorType);
            this.tabSensorConnection.Controls.Add(this.cmbCOM);
            this.tabSensorConnection.Controls.Add(lblCOM);
            this.tabSensorConnection.Location = new System.Drawing.Point(4, 22);
            this.tabSensorConnection.Name = "tabSensorConnection";
            this.tabSensorConnection.Padding = new System.Windows.Forms.Padding(3);
            this.tabSensorConnection.Size = new System.Drawing.Size(340, 208);
            this.tabSensorConnection.TabIndex = 0;
            this.tabSensorConnection.Text = "Sensor Connection";
            this.tabSensorConnection.UseVisualStyleBackColor = true;
            // 
            // btnConnect
            // 
            this.btnConnect.Location = new System.Drawing.Point(109, 146);
            this.btnConnect.Name = "btnConnect";
            this.btnConnect.Size = new System.Drawing.Size(75, 23);
            this.btnConnect.TabIndex = 15;
            this.btnConnect.Text = "Connect";
            this.btnConnect.UseVisualStyleBackColor = true;
            // 
            // cmbBaudRate
            // 
            this.cmbBaudRate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbBaudRate.FormattingEnabled = true;
            this.cmbBaudRate.Location = new System.Drawing.Point(109, 115);
            this.cmbBaudRate.Name = "cmbBaudRate";
            this.cmbBaudRate.Size = new System.Drawing.Size(148, 21);
            this.cmbBaudRate.TabIndex = 14;
            // 
            // cmbSensorModel
            // 
            this.cmbSensorModel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSensorModel.FormattingEnabled = true;
            this.cmbSensorModel.Location = new System.Drawing.Point(109, 18);
            this.cmbSensorModel.Name = "cmbSensorModel";
            this.cmbSensorModel.Size = new System.Drawing.Size(148, 21);
            this.cmbSensorModel.TabIndex = 1;
            // 
            // btnRefreshCOM
            // 
            this.btnRefreshCOM.Location = new System.Drawing.Point(194, 82);
            this.btnRefreshCOM.Name = "btnRefreshCOM";
            this.btnRefreshCOM.Size = new System.Drawing.Size(63, 23);
            this.btnRefreshCOM.TabIndex = 13;
            this.btnRefreshCOM.Text = "Refresh";
            this.btnRefreshCOM.UseVisualStyleBackColor = true;
            // 
            // cmbSensorType
            // 
            this.cmbSensorType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSensorType.FormattingEnabled = true;
            this.cmbSensorType.Location = new System.Drawing.Point(109, 51);
            this.cmbSensorType.Name = "cmbSensorType";
            this.cmbSensorType.Size = new System.Drawing.Size(148, 21);
            this.cmbSensorType.TabIndex = 2;
            // 
            // cmbCOM
            // 
            this.cmbCOM.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCOM.FormattingEnabled = true;
            this.cmbCOM.Location = new System.Drawing.Point(109, 82);
            this.cmbCOM.Name = "cmbCOM";
            this.cmbCOM.Size = new System.Drawing.Size(75, 21);
            this.cmbCOM.TabIndex = 12;
            // 
            // tabAnalysisOptions
            // 
            this.tabAnalysisOptions.Controls.Add(this.chkFilterPeaks);
            this.tabAnalysisOptions.Controls.Add(lblPeakThreshold);
            this.tabAnalysisOptions.Controls.Add(this.numPeakThreshold);
            this.tabAnalysisOptions.Controls.Add(label5);
            this.tabAnalysisOptions.Controls.Add(this.numDiscardTotal);
            this.tabAnalysisOptions.Controls.Add(this.numDiscardTime);
            this.tabAnalysisOptions.Controls.Add(this.chkDiscardTotal);
            this.tabAnalysisOptions.Controls.Add(this.chkDiscardTime);
            this.tabAnalysisOptions.Controls.Add(label4);
            this.tabAnalysisOptions.Controls.Add(this.numParamD);
            this.tabAnalysisOptions.Controls.Add(this.numParamC);
            this.tabAnalysisOptions.Controls.Add(lblParamD);
            this.tabAnalysisOptions.Controls.Add(lblParamC);
            this.tabAnalysisOptions.Controls.Add(this.numParamB);
            this.tabAnalysisOptions.Controls.Add(this.numCalibrationFactor);
            this.tabAnalysisOptions.Controls.Add(this.numThermalFactor);
            this.tabAnalysisOptions.Controls.Add(this.numParamA);
            this.tabAnalysisOptions.Controls.Add(lblCalibrationFactor);
            this.tabAnalysisOptions.Controls.Add(lblParamB);
            this.tabAnalysisOptions.Controls.Add(lblThermalFactor);
            this.tabAnalysisOptions.Controls.Add(lblParamA);
            this.tabAnalysisOptions.Controls.Add(this.numSmoothingWidth);
            this.tabAnalysisOptions.Controls.Add(this.numThresholdParam);
            this.tabAnalysisOptions.Controls.Add(lblSmoothingWidth);
            this.tabAnalysisOptions.Controls.Add(lblThresholdParam);
            this.tabAnalysisOptions.Location = new System.Drawing.Point(4, 22);
            this.tabAnalysisOptions.Name = "tabAnalysisOptions";
            this.tabAnalysisOptions.Padding = new System.Windows.Forms.Padding(3);
            this.tabAnalysisOptions.Size = new System.Drawing.Size(340, 208);
            this.tabAnalysisOptions.TabIndex = 1;
            this.tabAnalysisOptions.Text = "Analysis Options";
            this.tabAnalysisOptions.UseVisualStyleBackColor = true;
            // 
            // chkFilterPeaks
            // 
            this.chkFilterPeaks.AutoSize = true;
            this.chkFilterPeaks.Location = new System.Drawing.Point(11, 172);
            this.chkFilterPeaks.Name = "chkFilterPeaks";
            this.chkFilterPeaks.Size = new System.Drawing.Size(81, 17);
            this.chkFilterPeaks.TabIndex = 29;
            this.chkFilterPeaks.Text = "Filter Peaks";
            this.chkFilterPeaks.UseVisualStyleBackColor = true;
            // 
            // numPeakThreshold
            // 
            this.numPeakThreshold.DecimalPlaces = 1;
            this.numPeakThreshold.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numPeakThreshold.Location = new System.Drawing.Point(259, 169);
            this.numPeakThreshold.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.numPeakThreshold.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numPeakThreshold.Name = "numPeakThreshold";
            this.numPeakThreshold.Size = new System.Drawing.Size(59, 20);
            this.numPeakThreshold.TabIndex = 28;
            this.numPeakThreshold.Value = new decimal(new int[] {
            40,
            0,
            0,
            0});
            // 
            // numDiscardTotal
            // 
            this.numDiscardTotal.DecimalPlaces = 1;
            this.numDiscardTotal.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numDiscardTotal.Location = new System.Drawing.Point(91, 141);
            this.numDiscardTotal.Maximum = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.numDiscardTotal.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numDiscardTotal.Name = "numDiscardTotal";
            this.numDiscardTotal.Size = new System.Drawing.Size(54, 20);
            this.numDiscardTotal.TabIndex = 25;
            this.numDiscardTotal.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // numDiscardTime
            // 
            this.numDiscardTime.DecimalPlaces = 2;
            this.numDiscardTime.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numDiscardTime.Location = new System.Drawing.Point(91, 115);
            this.numDiscardTime.Maximum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numDiscardTime.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numDiscardTime.Name = "numDiscardTime";
            this.numDiscardTime.Size = new System.Drawing.Size(54, 20);
            this.numDiscardTime.TabIndex = 25;
            this.numDiscardTime.Value = new decimal(new int[] {
            2,
            0,
            0,
            65536});
            // 
            // chkDiscardTotal
            // 
            this.chkDiscardTotal.AutoSize = true;
            this.chkDiscardTotal.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkDiscardTotal.Checked = true;
            this.chkDiscardTotal.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkDiscardTotal.Location = new System.Drawing.Point(149, 142);
            this.chkDiscardTotal.Name = "chkDiscardTotal";
            this.chkDiscardTotal.Size = new System.Drawing.Size(87, 17);
            this.chkDiscardTotal.TabIndex = 24;
            this.chkDiscardTotal.Text = "%   total data";
            this.chkDiscardTotal.UseVisualStyleBackColor = true;
            // 
            // chkDiscardTime
            // 
            this.chkDiscardTime.AutoSize = true;
            this.chkDiscardTime.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkDiscardTime.Checked = true;
            this.chkDiscardTime.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkDiscardTime.Location = new System.Drawing.Point(170, 116);
            this.chkDiscardTime.Name = "chkDiscardTime";
            this.chkDiscardTime.Size = new System.Drawing.Size(66, 17);
            this.chkDiscardTime.TabIndex = 24;
            this.chkDiscardTime.Text = "seconds";
            this.chkDiscardTime.UseVisualStyleBackColor = true;
            // 
            // numParamD
            // 
            this.numParamD.DecimalPlaces = 2;
            this.numParamD.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numParamD.Location = new System.Drawing.Point(250, 58);
            this.numParamD.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numParamD.Name = "numParamD";
            this.numParamD.Size = new System.Drawing.Size(78, 20);
            this.numParamD.TabIndex = 21;
            this.numParamD.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // numParamC
            // 
            this.numParamC.DecimalPlaces = 2;
            this.numParamC.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numParamC.Location = new System.Drawing.Point(250, 36);
            this.numParamC.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numParamC.Name = "numParamC";
            this.numParamC.Size = new System.Drawing.Size(78, 20);
            this.numParamC.TabIndex = 22;
            this.numParamC.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            // 
            // numParamB
            // 
            this.numParamB.DecimalPlaces = 2;
            this.numParamB.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numParamB.Location = new System.Drawing.Point(250, 10);
            this.numParamB.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numParamB.Name = "numParamB";
            this.numParamB.Size = new System.Drawing.Size(78, 20);
            this.numParamB.TabIndex = 17;
            this.numParamB.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // numCalibrationFactor
            // 
            this.numCalibrationFactor.DecimalPlaces = 4;
            this.numCalibrationFactor.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numCalibrationFactor.Location = new System.Drawing.Point(264, 83);
            this.numCalibrationFactor.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numCalibrationFactor.Name = "numCalibrationFactor";
            this.numCalibrationFactor.Size = new System.Drawing.Size(62, 20);
            this.numCalibrationFactor.TabIndex = 18;
            this.numCalibrationFactor.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // numThermalFactor
            // 
            this.numThermalFactor.DecimalPlaces = 4;
            this.numThermalFactor.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numThermalFactor.Location = new System.Drawing.Point(93, 83);
            this.numThermalFactor.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numThermalFactor.Name = "numThermalFactor";
            this.numThermalFactor.Size = new System.Drawing.Size(70, 20);
            this.numThermalFactor.TabIndex = 18;
            this.numThermalFactor.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // numParamA
            // 
            this.numParamA.DecimalPlaces = 2;
            this.numParamA.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numParamA.Location = new System.Drawing.Point(129, 59);
            this.numParamA.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numParamA.Name = "numParamA";
            this.numParamA.Size = new System.Drawing.Size(78, 20);
            this.numParamA.TabIndex = 18;
            this.numParamA.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // numSmoothingWidth
            // 
            this.numSmoothingWidth.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numSmoothingWidth.Location = new System.Drawing.Point(129, 34);
            this.numSmoothingWidth.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numSmoothingWidth.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numSmoothingWidth.Name = "numSmoothingWidth";
            this.numSmoothingWidth.Size = new System.Drawing.Size(78, 20);
            this.numSmoothingWidth.TabIndex = 13;
            this.numSmoothingWidth.Value = new decimal(new int[] {
            70,
            0,
            0,
            0});
            // 
            // numThresholdParam
            // 
            this.numThresholdParam.DecimalPlaces = 2;
            this.numThresholdParam.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numThresholdParam.Location = new System.Drawing.Point(129, 11);
            this.numThresholdParam.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numThresholdParam.Name = "numThresholdParam";
            this.numThresholdParam.Size = new System.Drawing.Size(78, 20);
            this.numThresholdParam.TabIndex = 14;
            this.numThresholdParam.Value = new decimal(new int[] {
            2,
            0,
            0,
            65536});
            // 
            // tabScanOptions
            // 
            this.tabScanOptions.Controls.Add(this.numNumberScans);
            this.tabScanOptions.Controls.Add(lblNumberScans);
            this.tabScanOptions.Controls.Add(grpFrequency);
            this.tabScanOptions.Controls.Add(this.btnFilenamePatternHelp);
            this.tabScanOptions.Controls.Add(this.txtFilenamePattern);
            this.tabScanOptions.Controls.Add(lblFilenamePattern);
            this.tabScanOptions.Controls.Add(this.btnDataLocationBrowse);
            this.tabScanOptions.Controls.Add(this.txtDataLocation);
            this.tabScanOptions.Controls.Add(lblDataLocation);
            this.tabScanOptions.Location = new System.Drawing.Point(4, 22);
            this.tabScanOptions.Name = "tabScanOptions";
            this.tabScanOptions.Size = new System.Drawing.Size(340, 208);
            this.tabScanOptions.TabIndex = 2;
            this.tabScanOptions.Text = "Scan Options";
            this.tabScanOptions.UseVisualStyleBackColor = true;
            // 
            // numNumberScans
            // 
            this.numNumberScans.Increment = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.numNumberScans.Location = new System.Drawing.Point(179, 181);
            this.numNumberScans.Maximum = new decimal(new int[] {
            7,
            0,
            0,
            0});
            this.numNumberScans.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numNumberScans.Name = "numNumberScans";
            this.numNumberScans.Size = new System.Drawing.Size(58, 20);
            this.numNumberScans.TabIndex = 31;
            this.numNumberScans.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numNumberScans.Visible = false;
            // 
            // btnFilenamePatternHelp
            // 
            this.btnFilenamePatternHelp.Location = new System.Drawing.Point(243, 119);
            this.btnFilenamePatternHelp.Name = "btnFilenamePatternHelp";
            this.btnFilenamePatternHelp.Size = new System.Drawing.Size(75, 23);
            this.btnFilenamePatternHelp.TabIndex = 29;
            this.btnFilenamePatternHelp.Text = "Help";
            this.btnFilenamePatternHelp.UseVisualStyleBackColor = true;
            // 
            // txtFilenamePattern
            // 
            this.txtFilenamePattern.Location = new System.Drawing.Point(116, 121);
            this.txtFilenamePattern.Name = "txtFilenamePattern";
            this.txtFilenamePattern.Size = new System.Drawing.Size(121, 20);
            this.txtFilenamePattern.TabIndex = 27;
            // 
            // btnDataLocationBrowse
            // 
            this.btnDataLocationBrowse.Location = new System.Drawing.Point(243, 150);
            this.btnDataLocationBrowse.Name = "btnDataLocationBrowse";
            this.btnDataLocationBrowse.Size = new System.Drawing.Size(75, 23);
            this.btnDataLocationBrowse.TabIndex = 25;
            this.btnDataLocationBrowse.Text = "Browse";
            this.btnDataLocationBrowse.UseVisualStyleBackColor = true;
            // 
            // txtDataLocation
            // 
            this.txtDataLocation.Location = new System.Drawing.Point(116, 151);
            this.txtDataLocation.Name = "txtDataLocation";
            this.txtDataLocation.ReadOnly = true;
            this.txtDataLocation.Size = new System.Drawing.Size(121, 20);
            this.txtDataLocation.TabIndex = 23;
            this.txtDataLocation.Text = "c:\\";
            // 
            // radCustomProfile
            // 
            this.radCustomProfile.AutoSize = true;
            this.radCustomProfile.Location = new System.Drawing.Point(265, 20);
            this.radCustomProfile.Name = "radCustomProfile";
            this.radCustomProfile.Size = new System.Drawing.Size(60, 17);
            this.radCustomProfile.TabIndex = 2;
            this.radCustomProfile.Text = "Custom";
            this.radCustomProfile.UseVisualStyleBackColor = true;
            // 
            // cmbProfile
            // 
            this.cmbProfile.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbProfile.FormattingEnabled = true;
            this.cmbProfile.Location = new System.Drawing.Point(101, 19);
            this.cmbProfile.Name = "cmbProfile";
            this.cmbProfile.Size = new System.Drawing.Size(116, 21);
            this.cmbProfile.TabIndex = 1;
            // 
            // radUseProfile
            // 
            this.radUseProfile.AutoSize = true;
            this.radUseProfile.Checked = true;
            this.radUseProfile.Location = new System.Drawing.Point(20, 20);
            this.radUseProfile.Name = "radUseProfile";
            this.radUseProfile.Size = new System.Drawing.Size(75, 17);
            this.radUseProfile.TabIndex = 0;
            this.radUseProfile.TabStop = true;
            this.radUseProfile.Text = "Use profile";
            this.radUseProfile.UseVisualStyleBackColor = true;
            // 
            // tabDatabase
            // 
            this.tabDatabase.Controls.Add(this.groupBox1);
            this.tabDatabase.Controls.Add(this.checkBox1);
            this.tabDatabase.Location = new System.Drawing.Point(4, 22);
            this.tabDatabase.Name = "tabDatabase";
            this.tabDatabase.Padding = new System.Windows.Forms.Padding(3);
            this.tabDatabase.Size = new System.Drawing.Size(381, 333);
            this.tabDatabase.TabIndex = 1;
            this.tabDatabase.Text = "Database";
            this.tabDatabase.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Location = new System.Drawing.Point(17, 47);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(348, 242);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Database Connection";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(21, 31);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(111, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "TODO: database stuff";
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(27, 17);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(143, 17);
            this.checkBox1.TabIndex = 0;
            this.checkBox1.Text = "Save results to database";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // btnOK
            // 
            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOK.Location = new System.Drawing.Point(234, 377);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 36;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(321, 377);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 37;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // errorProvider
            // 
            this.errorProvider.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.NeverBlink;
            this.errorProvider.ContainerControl = this;
            // 
            // SettingsDialog
            // 
            this.AcceptButton = this.btnOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(416, 412);
            this.ControlBox = false;
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.tabControlOuter);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SettingsDialog";
            this.ShowInTaskbar = false;
            this.Text = "Settings";
            grpFrequency.ResumeLayout(false);
            grpFrequency.PerformLayout();
            pnlDownscaleMethod.ResumeLayout(false);
            pnlDownscaleMethod.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numDownscaleFactor)).EndInit();
            this.tabControlOuter.ResumeLayout(false);
            this.tabScanProfile.ResumeLayout(false);
            this.tabScanProfile.PerformLayout();
            this.tabControlScanSettings.ResumeLayout(false);
            this.tabSensorConnection.ResumeLayout(false);
            this.tabSensorConnection.PerformLayout();
            this.tabAnalysisOptions.ResumeLayout(false);
            this.tabAnalysisOptions.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numPeakThreshold)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numDiscardTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numDiscardTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numParamD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numParamC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numParamB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numCalibrationFactor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numThermalFactor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numParamA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSmoothingWidth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numThresholdParam)).EndInit();
            this.tabScanOptions.ResumeLayout(false);
            this.tabScanOptions.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numNumberScans)).EndInit();
            this.tabDatabase.ResumeLayout(false);
            this.tabDatabase.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox cmbSensorType;
        private System.Windows.Forms.ComboBox cmbSensorModel;
        private System.Windows.Forms.RadioButton radCustomProfile;
        private System.Windows.Forms.ComboBox cmbProfile;
        private System.Windows.Forms.RadioButton radUseProfile;
        private System.Windows.Forms.Button btnConnect;
        private System.Windows.Forms.ComboBox cmbBaudRate;
        private System.Windows.Forms.Button btnRefreshCOM;
        private System.Windows.Forms.ComboBox cmbCOM;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnRestoreDefaults;
        private System.Windows.Forms.Button btnDeleteProfile;
        private System.Windows.Forms.Button btnSaveProfile;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.NumericUpDown numParamD;
        private System.Windows.Forms.NumericUpDown numParamC;
        private System.Windows.Forms.NumericUpDown numParamB;
        private System.Windows.Forms.NumericUpDown numParamA;
        private System.Windows.Forms.NumericUpDown numSmoothingWidth;
        private System.Windows.Forms.NumericUpDown numThresholdParam;
        private System.Windows.Forms.TextBox txtFilenamePattern;
        private System.Windows.Forms.Button btnDataLocationBrowse;
        private System.Windows.Forms.TextBox txtDataLocation;
        private System.Windows.Forms.RadioButton radDownscaleFiltering;
        private System.Windows.Forms.RadioButton radDownscaleAveraging;
        private System.Windows.Forms.NumericUpDown numDownscaleFactor;
        private System.Windows.Forms.CheckBox chkDownscaleFrequency;
        private System.Windows.Forms.ComboBox cmbMeasurementFrequency;
        private System.Windows.Forms.Button btnFilenamePatternHelp;
        private System.Windows.Forms.FolderBrowserDialog folderBrowser;
        private System.Windows.Forms.ErrorProvider errorProvider;
        private System.Windows.Forms.TabControl tabControlOuter;
        private System.Windows.Forms.TabPage tabScanProfile;
        private System.Windows.Forms.TabControl tabControlScanSettings;
        private System.Windows.Forms.TabPage tabSensorConnection;
        private System.Windows.Forms.TabPage tabAnalysisOptions;
        private System.Windows.Forms.TabPage tabScanOptions;
        private System.Windows.Forms.TabPage tabDatabase;
        private System.Windows.Forms.NumericUpDown numDiscardTotal;
        private System.Windows.Forms.NumericUpDown numDiscardTime;
        private System.Windows.Forms.CheckBox chkDiscardTotal;
        private System.Windows.Forms.CheckBox chkDiscardTime;
        private System.Windows.Forms.NumericUpDown numNumberScans;
        private System.Windows.Forms.NumericUpDown numPeakThreshold;
        private System.Windows.Forms.CheckBox chkFilterPeaks;
        private System.Windows.Forms.NumericUpDown numThermalFactor;
        private System.Windows.Forms.NumericUpDown numCalibrationFactor;
    }
}