﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using PVS.Scans;
using System.IO;

namespace PVS
{
    public partial class LoadScanDialog : Form
    {
        ScanManager scanManager;

        public LoadScanDialog()
        {
            InitializeComponent();

            scanManager = new ScanManager("", "");

            btnDataLocationBrowse.Click += new EventHandler(btnDataLocationBrowse_Click);
            radCustomScanId.CheckedChanged += new EventHandler(radScanId_CheckedChanged);
            radLastScanId.CheckedChanged += new EventHandler(radScanId_CheckedChanged);
            btnRefreshScanId.Click += new EventHandler(btnRefreshScanId_Click);

            btnOK.Click += new EventHandler(btnOK_Click);
            btnCancel.Click += new EventHandler(btnCancel_Click);
            
            Shown += new EventHandler(LoadScanDialog_Shown);
                
            CustomScanId = false;
        }

        void LoadScanDialog_Shown(object sender, EventArgs e)
        {
            if (radCustomScanId.Checked)
                RefreshScanIds();
        }

        private bool ValidateForm()
        {
            if (!Directory.Exists(Path))
            {
                MessageBox.Show("Please select a valid directory.");
                return false;
            }

            if (FilenamePattern.Trim() == "")
            {
                MessageBox.Show("Please select a filename pattern.");
                return false;
            }

            return true;
        }

        private void RefreshScanIds()
        {
            if (ValidateForm())
            {
                scanManager.Path = Path;
                scanManager.Template = FilenamePattern;

                int[] ids = scanManager.GetScanIds();
                int length = ids.Length;

                if (length == 0)
                {
                    MessageBox.Show("Could not find any scans.");
                    return;
                }
                else
                {
                    Array.Sort(ids);
                    cmbScanId.Items.Clear();
                    foreach (int i in ids)
                    {
                        cmbScanId.Items.Add(i);
                    }
                    cmbScanId.SelectedIndex = length - 1;
                }
            }
        }

        private void UpdateScanIdEnabledStates()
        {
            cmbScanId.Enabled = radCustomScanId.Checked;
            btnRefreshScanId.Enabled = radCustomScanId.Checked;

            if (radCustomScanId.Checked)
            {
                RefreshScanIds();
            }
        }
                
        #region Properties

        public string FilenamePattern
        {
            get { return txtFilenamePattern.Text; }
            set 
            {
                scanManager.Template = value;
                txtFilenamePattern.Text = value; 
            }
        }

        public string Path
        {
            get { return txtDataLocation.Text; }
            set 
            {
                scanManager.Path = value;
                txtDataLocation.Text = value;
            }
        }

        public int ScanId
        {
            get { return (cmbScanId.Text.Length > 0 ? Convert.ToInt32(cmbScanId.Text) : -1); }
        }

        public bool CustomScanId
        {
            get { return radCustomScanId.Checked; }
            set
            {
                if (value)
                {
                    radCustomScanId.Checked = true;
                    if (cmbScanId.Items.Count > 0)
                    {
                        cmbScanId.SelectedIndex = cmbScanId.Items.Count - 1;
                    }
                }
                else
                {
                    radLastScanId.Checked = true;
                }

                UpdateScanIdEnabledStates();
            }
        }

        #endregion

        void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        void btnOK_Click(object sender, EventArgs e)
        {
            if (ValidateForm())
            {
                if (radLastScanId.Checked || cmbScanId.Items.Count > 0)
                {
                    this.Close();
                }
                else
                {
                    MessageBox.Show("No scan id selected");
                    this.DialogResult = DialogResult.None; 
                }
            }
            else 
            {
                this.DialogResult = DialogResult.None;
            }
        }

        void btnDataLocationBrowse_Click(object sender, EventArgs e)
        {
            folderBrowser.ShowDialog();

            if (folderBrowser.SelectedPath != "")
            {
                txtDataLocation.Text = folderBrowser.SelectedPath;
            }

            if (radCustomScanId.Checked)
            {
                RefreshScanIds();
            }
        }

        void radScanId_CheckedChanged(object sender, EventArgs e)
        {
            // to avoid it being called multiple times
            if (((RadioButton)sender).Checked)
            {
                UpdateScanIdEnabledStates();
            }
        }

        void btnRefreshScanId_Click(object sender, EventArgs e)
        {
            RefreshScanIds();
        }               
    }
}
