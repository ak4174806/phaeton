﻿using System;
using System.Collections.Generic;
using System.Text;

using SensorControl;
using SensorControl.Sensors;
using Analysis;

namespace PVS
{
    public class SettingsProfile
    {
        private string profileName = "";
        /// <summary>
        /// The name of the profile, an empty string if unnamed
        /// </summary>
        public string ProfileName
        {
            get { return profileName; }
            set { profileName = value; }
        }

        #region Sensor Connection

        private string sensorModel;
        /// <summary>
        /// The model of the sensor being used
        /// </summary>
        public string SensorModel
        {
            get { return sensorModel; }
            set { sensorModel = value; }
        }

        private string sensorType;
        /// <summary>
        /// The type of the sensor being used
        /// </summary>
        public string SensorType
        {
            get { return sensorType; }
            set { sensorType = value; }
        }

        private string comPort;
        /// <summary>
        /// The COM port for connecting to the sensor
        /// </summary>
        public string ComPort
        {
            get { return comPort; }
            set { comPort = value; }
        }

        private int baudRate;
        /// <summary>
        /// The baudrate for connecting to the sensor, 0 => autodetect
        /// </summary>
        public int BaudRate
        {
            get { return baudRate; }
            set { baudRate = value; }
        }

        #endregion

        #region Analysis Options

        private PVSAnalysisOptions analysisOptions;
        /// <summary>
        /// The options for analysis
        /// </summary>
        public PVSAnalysisOptions AnalysisOptions
        {
            get { return analysisOptions; }
            set { analysisOptions = value; }
        }
        
        // whether to discard a certain amount of time worth of data from each end
        public bool TimeDiscarded;
        // the amount of time in seconds of data to discard
        public decimal DiscardTime;

        // whether to discard a percentage of the total from each end
        public bool TotalDiscarded;
        // the percentage (between 0 and 100) to discard from each end
        public decimal DiscardTotal;

        // in microns
        public decimal PeakThreshold;
        public bool FilterPeaks;

        #endregion

        #region Scan Options

        private string measurementFrequency;
        /// <summary>
        /// The frequency of measurements taken by the sensor
        /// </summary>
        public string MeasurementFrequency
        {
            get { return measurementFrequency; }
            set { measurementFrequency = value; }
        }

        private int downscaleFactor;
        /// <summary>
        /// The factor by which to downscale the frequency if DownscaleFrequency is true
        /// </summary>
        public int DownscaleFactor
        {
            get { return downscaleFactor; }
            set { downscaleFactor = value; }
        }

        /// <summary>
        /// The methods which are available to downscale the frequency.
        /// Averaging - Average in blocks of size downscaleFactor
        /// Filtering - Only keep every downscaleFactor'th point
        /// None - Don't downscale
        /// </summary>
        public enum DownscaleMethods
        {
            Averaging,
            Filtering,
            None
        }
        
        private DownscaleMethods downscaleMethod;
        /// <summary>
        /// The method used to downscale frequency
        /// </summary>
        public DownscaleMethods DownscaleMethod
        {
            get { return downscaleMethod; }
            set { downscaleMethod = value; }
        }

        private string filenamePattern;
        /// <summary>
        /// A pattern to use when saving each file
        /// </summary>
        public string FilenamePattern
        {
            get { return filenamePattern; }
            set { filenamePattern = value; }
        }

        private string dataLocation;
        /// <summary>
        /// The folder in which to save data files
        /// </summary>
        public string DataLocation
        {
            get { return dataLocation; }
            set { dataLocation = value; }
        }

        private int numberScans;
        /// <summary>
        /// The number of wet and dry scans to run
        /// </summary>
        public int NumberScans
        {
            get { return numberScans; }
            set { numberScans = value; }
        }

        private int numberReadingsAveraged;
        /// <summary>
        /// The number of readings to average and display at a time in auto mode
        /// </summary>
        public int NumberReadingsAveraged
        {
            get { return numberReadingsAveraged; }
            set { numberReadingsAveraged = value; }
        }

        #endregion
    }
}
