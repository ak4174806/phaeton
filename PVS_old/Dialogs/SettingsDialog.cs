﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using System.IO;
using System.IO.Ports;

using SensorControl;
using SensorControl.Sensors;
using Analysis;

namespace PVS
{
    public partial class SettingsDialog : Form
    {
        private bool suppressEvents = false;

        public new PVS_Form ParentForm;

        private SettingsProfile currentSettings;
        public SettingsProfile CurrentSettings
        {
            get 
            {
                // parse form for settings
                if (FormChanged)
                    updateSettings();
                return currentSettings;
            }
            set 
            { 
                currentSettings = value;
                // update form from settings
                updateForm();
            }
        }

        private ISensor[] sensors;
        /// <summary>
        /// Returns the currently selected sensor, using the form as a guide
        /// </summary>
        public ISensor Sensor
        {
            get
            {
                ISensor sensor = sensors[cmbSensorModel.SelectedIndex];
                
                if (cmbSensorType.Items.Count > 0)
                {
                    bool contains = false;
                    foreach (string s in sensor.SensorTypes)
                    {
                        if (s == cmbSensorType.SelectedItem.ToString())
                        {
                            contains = true;
                            break;
                        }
                    }

                    if (contains)
                    {
                        sensor.SetSensorType(cmbSensorType.SelectedItem.ToString());
                    }
                }
                return sensor;
            }
        }

        public bool Valid
        {
            get { return validate(); }
        }

        /// <summary>
        /// Whether the form and settings are different
        /// </summary>
        public bool Modified
        {
            get
            {
                bool not_modified =
                    (currentSettings.SensorModel == cmbSensorModel.SelectedItem.ToString()) &&
                    (currentSettings.SensorType == cmbSensorType.SelectedItem.ToString()) &&
                    (currentSettings.ComPort == cmbCOM.SelectedItem.ToString()) &&
                    (
                        currentSettings.BaudRate.ToString() == cmbBaudRate.SelectedItem.ToString() ||
                        currentSettings.BaudRate == 0 && cmbBaudRate.SelectedIndex == 0
                    ) &&

                    (currentSettings.AnalysisOptions.SmoothingWidth == (int)numSmoothingWidth.Value) &&
                    (currentSettings.AnalysisOptions.ThresholdParameter == numThresholdParam.Value) &&
                    (currentSettings.AnalysisOptions.A == (double)numParamA.Value) &&
                    (currentSettings.AnalysisOptions.B == (double)numParamB.Value) &&
                    (currentSettings.AnalysisOptions.C == (double)numParamC.Value) &&
                    (currentSettings.AnalysisOptions.D == (double)numParamD.Value) &&
                    (currentSettings.AnalysisOptions.ThermalFactor == (double)numThermalFactor.Value) &&
                    (currentSettings.AnalysisOptions.CalibrationFactor == (double)numCalibrationFactor.Value) &&

                    (currentSettings.FilterPeaks == chkFilterPeaks.Checked) &&
                    (!chkFilterPeaks.Checked || currentSettings.PeakThreshold == numPeakThreshold.Value) &&

                    (currentSettings.TimeDiscarded == chkDiscardTime.Checked) &&
                    (currentSettings.TotalDiscarded == chkDiscardTotal.Checked) &&
                    (!chkDiscardTime.Checked || numDiscardTime.Value == currentSettings.DiscardTime) && 
                    (!chkDiscardTotal.Checked || numDiscardTotal.Value == currentSettings.DiscardTotal) &&
                    
                    (
                        (currentSettings.DownscaleMethod == SettingsProfile.DownscaleMethods.None && !chkDownscaleFrequency.Checked) ||
                        (currentSettings.DownscaleMethod == SettingsProfile.DownscaleMethods.Averaging && chkDownscaleFrequency.Checked && radDownscaleAveraging.Checked) ||
                        (currentSettings.DownscaleMethod == SettingsProfile.DownscaleMethods.Filtering && chkDownscaleFrequency.Checked && radDownscaleFiltering.Checked)
                    ) &&
                    (currentSettings.DownscaleFactor == (int)numDownscaleFactor.Value) &&

                    (currentSettings.FilenamePattern == txtFilenamePattern.Text) &&
                    (currentSettings.DataLocation == txtDataLocation.Text) &&
                    (currentSettings.NumberScans == (int)numNumberScans.Value);

                return !not_modified;
            }
        }
        
        private bool formChanged = false;
        /// <summary>
        /// Whether the form value has formChanged, used to avoid unnecessarily updating settings when fetching CurrentSettings
        /// </summary>
        public bool FormChanged
        {
            get { return formChanged; }
        }
		
		
		public enum States 
		{
			Connected,
			Disconnected
		}
		private States state;
		public States State 
		{
			get { return state; }
			set 
			{ 
				state = value; 
				if (value == States.Connected)
				{
					btnConnect.Text = "Disconnect";
					cmbSensorModel.Enabled = false;
					cmbSensorType.Enabled = false;
					cmbCOM.Enabled = false;
					cmbBaudRate.Enabled = false;
                    btnRefreshCOM.Enabled = false;
				}
				else 
				{
					btnConnect.Text = "Connect";
					cmbSensorModel.Enabled = true;
					cmbSensorType.Enabled = true;
					cmbCOM.Enabled = true;
					cmbBaudRate.Enabled = true;
                    btnRefreshCOM.Enabled = true;
				}
			}
		}
		

        // todo: look at releasing form
        
        public SettingsDialog()
        {
            InitializeComponent();

            // set up sensors
            sensors = new ISensor[2];
            sensors[0] = new ILD2200_MESensor();
            sensors[1] = new ILD1700_MESensor();
            
            // populate combo boxes - all defaulted to first option
            updateSensorModels();
            updateSensorTypes();
            updateCOMPorts();
            updateBaudRates();
            updateMeasurementFrequencies();

            // set defaults - later from saved stuff
            currentSettings = new SettingsProfile();

            currentSettings.AnalysisOptions = new PVSAnalysisOptions();
            currentSettings.AnalysisOptions.ThresholdParameter = (decimal)0.2;
            currentSettings.AnalysisOptions.SmoothingWidth = 70;
            currentSettings.AnalysisOptions.A = 1;
            currentSettings.AnalysisOptions.B = 1;
            currentSettings.AnalysisOptions.C = 2;
            currentSettings.AnalysisOptions.D = 5;
            currentSettings.AnalysisOptions.ThermalFactor = 1;
            currentSettings.AnalysisOptions.CalibrationFactor = 1;

            currentSettings.DiscardTime = (decimal)1;
            currentSettings.TimeDiscarded = true;
            currentSettings.DiscardTotal = (decimal)15;
            currentSettings.TotalDiscarded = true;

            currentSettings.FilterPeaks = false;
            currentSettings.PeakThreshold = (decimal)10.0;

            currentSettings.DownscaleMethod = SettingsProfile.DownscaleMethods.None;
            currentSettings.DownscaleFactor = 1;
            currentSettings.FilenamePattern = "{type}-{x} {nnnn}.csv";
            currentSettings.DataLocation = @"c:\";
            currentSettings.NumberScans = 1;

            // to update the sensor models, etc which were all defaulted above, necessary to avoid assigning nulls when updating form
            currentSettings.SensorModel = cmbSensorModel.SelectedItem.ToString();
            currentSettings.SensorType = cmbSensorType.SelectedItem.ToString();
            currentSettings.ComPort = cmbCOM.SelectedItem.ToString();
            if (cmbBaudRate.SelectedIndex == 0)
            {
                currentSettings.BaudRate = 0;
            }
            else
            {
                currentSettings.BaudRate = Convert.ToInt32(cmbBaudRate.SelectedItem);
            }
            currentSettings.MeasurementFrequency = cmbMeasurementFrequency.SelectedItem.ToString();

            // update form from these defaults
            updateForm();            

            // set state
			State = States.Disconnected;
			

            // add events
            btnOK.Click += new EventHandler(btnOK_Click);
            btnCancel.Click += new EventHandler(btnCancel_Click);
            btnRefreshCOM.Click += new EventHandler(btnRefreshCOM_Click);
            btnDataLocationBrowse.Click += new EventHandler(btnDataLocationBrowse_Click);
            btnConnect.Click += new EventHandler(btnConnect_Click);

            this.Shown += new EventHandler(SettingsDialog_Shown);
            this.FormClosing += new FormClosingEventHandler(SettingsDialog_FormClosing);
            cmbSensorModel.SelectedIndexChanged += new EventHandler(cmbSensorModel_SelectedIndexChanged);
            chkDownscaleFrequency.CheckedChanged += new EventHandler(chkDownscaleFrequency_CheckedChanged);

            chkDiscardTime.CheckedChanged += new EventHandler(chkDiscardTime_CheckedChanged);
            chkDiscardTotal.CheckedChanged += new EventHandler(chkDiscardTotal_CheckedChanged);

            chkFilterPeaks.CheckedChanged += new EventHandler(delegate(object sender, EventArgs e)
            {
                numPeakThreshold.Enabled = chkFilterPeaks.Checked;
            });

            // add change handlers
            cmbSensorModel.SelectedIndexChanged += new EventHandler(Setting_Changed);
            cmbSensorType.SelectedIndexChanged += new EventHandler(Setting_Changed);
            cmbCOM.SelectedIndexChanged += new EventHandler(Setting_Changed);
            cmbBaudRate.SelectedIndexChanged += new EventHandler(Setting_Changed);

            numThresholdParam.ValueChanged += new EventHandler(Setting_Changed);
            numSmoothingWidth.ValueChanged += new EventHandler(Setting_Changed);
            numParamA.ValueChanged += new EventHandler(Setting_Changed);
            numParamB.ValueChanged += new EventHandler(Setting_Changed);
            numParamC.ValueChanged += new EventHandler(Setting_Changed);
            numParamD.ValueChanged += new EventHandler(Setting_Changed);
            numThermalFactor.ValueChanged += new EventHandler(Setting_Changed);
            numCalibrationFactor.ValueChanged += new EventHandler(Setting_Changed);

            chkDiscardTotal.CheckedChanged += new EventHandler(Setting_Changed);
            chkDiscardTime.CheckedChanged += new EventHandler(Setting_Changed);
            numDiscardTime.ValueChanged += new EventHandler(Setting_Changed); 
            numDiscardTotal.ValueChanged += new EventHandler(Setting_Changed);

            chkFilterPeaks.CheckedChanged += new EventHandler(Setting_Changed);
            numPeakThreshold.ValueChanged += new EventHandler(Setting_Changed);

            cmbMeasurementFrequency.SelectedIndexChanged += new EventHandler(Setting_Changed);
            chkDownscaleFrequency.CheckedChanged += new EventHandler(Setting_Changed);
            chkDownscaleFrequency.CheckedChanged += new EventHandler(Setting_Changed);
            radDownscaleAveraging.CheckedChanged += new EventHandler(Setting_Changed);
            radDownscaleFiltering.CheckedChanged += new EventHandler(Setting_Changed);
            numDownscaleFactor.ValueChanged += new EventHandler(Setting_Changed);

            txtFilenamePattern.TextChanged += new EventHandler(Setting_Changed);
            txtDataLocation.TextChanged += new EventHandler(Setting_Changed);
            numNumberScans.ValueChanged += new EventHandler(Setting_Changed);

            suppressEvents = false;


            // disable unused stuff - for now
            radUseProfile.Enabled = false;
            cmbProfile.Enabled = false;
            radCustomProfile.Enabled = false;

            btnSaveProfile.Enabled = false;
            btnDeleteProfile.Enabled = false;
            btnRestoreDefaults.Enabled = false;

            btnFilenamePatternHelp.Enabled = false;
        }

		
		// connect using form, also update settings and validate
		public bool ConnectSensor()
		{
			// updates settings if any changes, from form
			SettingsProfile settings = CurrentSettings;
			
			// validate
			if (!Valid)
			{
				MessageBox.Show("There are some invalid settings. Correct them before connecting");
				return false;
			}
			
			try
			{
				// connect to port
                string portName = settings.ComPort;
				
                if (settings.BaudRate == 0)
                {
                    // auto-detect baud rate
                    Sensor.Connect(portName);

                    // update selected baud rate
                    settings.BaudRate = Sensor.BaudRate;
					
					// update form
					CurrentSettings = settings;
                }
                else
                {
                    // use selected baud rate
                    int baudRate = settings.BaudRate;
                    Sensor.Connect(portName, baudRate);
                }

                // set reading reference to start of measuring range, rather than sensore
                Sensor.ReadingReference = ReadingReferences.SMR;
            }
            catch (Exception ex)
            {
                MessageBox.Show("There was an error connecting to the sensor. Try changing the COM port or baud rate. The sensor responded '" + ex.Message + "'");
                
                // disconnect if connected
                Sensor.Disconnect();

                return false;
            }
			
			return true;
		}

		public bool DisconnectSensor()
		{
			try 
			{
				Sensor.Disconnect();
				return true;
			}
			catch (Exception ex)
			{
				MessageBox.Show("Could not disconnect from sensor properly: " + ex.Message);
				return false;
			}
		}
		
        #region Events

        void btnConnect_Click(object sender, EventArgs e)
        {
            if (!Valid)
            {
                MessageBox.Show("There are errors in your settings. Please correct them first");
                return;
            }

			if (State == States.Disconnected) 
			{			
				btnConnect.Enabled = false;
			
            	bool connected = ConnectSensor();
				if (connected)
				{					
					// change form state
					State = States.Connected;
				}
			
				btnConnect.Enabled = true;
			}
			else 
			{
				btnConnect.Enabled = false;
			
            	bool disconnected = DisconnectSensor();
				if (disconnected)
				{					
					// change form state
					State = States.Disconnected;
				}
			
				btnConnect.Enabled = true;
			}
        }

        void Setting_Changed(object sender, EventArgs e)
        {
            formChanged = true;
        }

        void chkDiscardTotal_CheckedChanged(object sender, EventArgs e)
        {
            if (suppressEvents) return;

            numDiscardTotal.Enabled = chkDiscardTotal.Checked;
        }

        void chkDiscardTime_CheckedChanged(object sender, EventArgs e)
        {
            if (suppressEvents) return;

            numDiscardTime.Enabled = chkDiscardTime.Checked;
        }

        void chkDownscaleFrequency_CheckedChanged(object sender, EventArgs e)
        {
            if (suppressEvents) return;

            radDownscaleAveraging.Enabled = chkDownscaleFrequency.Checked;
            radDownscaleFiltering.Enabled = chkDownscaleFrequency.Checked;
        }

        void cmbSensorModel_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (suppressEvents) return;

            updateSensorTypes();
            updateBaudRates();
            updateMeasurementFrequencies();
        }

        void SettingsDialog_Shown(object sender, EventArgs e)
        {
            clearErrors();
            updateForm();

            // match connected/disconnected state of parent form
            if (ParentForm.ConnectionState == PVS_Form.ConnectionStates.Connected)
			{
				State = States.Connected;
			}
			else {
				State = States.Disconnected;
			}
        }

        void btnOK_Click(object sender, EventArgs e)
        {
            if (Valid || MessageBox.Show("There were errors. Save and close anyway?", "There are some errors in your settings", MessageBoxButtons.YesNo)
                    == DialogResult.Yes)
            {
                updateSettings();
                this.Close();
            }
            else
            {
                // prevent form closing
                this.DialogResult = DialogResult.None;
            }
        }
        // todo test cancelling/ok


        void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close(); // triggers form closing
        }

        void btnRefreshCOM_Click(object sender, EventArgs e)
        {
            updateCOMPorts();
        }

        void btnDataLocationBrowse_Click(object sender, EventArgs e)
        {
            folderBrowser.ShowDialog();

            if (folderBrowser.SelectedPath != "")
            {
                txtDataLocation.Text = folderBrowser.SelectedPath;
            }
        }

        // handles cancels only
        void SettingsDialog_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing || e.CloseReason == CloseReason.None)
            {
                // todo: prevent closing, just hide it
                e.Cancel = true;

                if (this.DialogResult == DialogResult.Cancel)
                {
                    if (!Modified || MessageBox.Show("You will lose any unsaved changes. Continue?", "You have modified the settings and you will lose unsaved changes", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        // disconnect sensor as well
                        if (State == States.Connected)
                            DisconnectSensor();

                        // use values in currentSettings
                        updateForm();
                        this.Hide();
                    }
                }
                else
                {
                    this.Hide();
                }
            }
        }

        #endregion

        #region Update Functions

        /// <summary>
        /// Updates currentSettings, using the form as a source
        /// </summary>
        private void updateSettings()
        {
            // todo: update profile name as well if not custom

            currentSettings.SensorModel = cmbSensorModel.SelectedItem.ToString();
            currentSettings.SensorType = cmbSensorType.SelectedItem.ToString();
            currentSettings.ComPort = cmbCOM.SelectedItem.ToString();
            if (cmbBaudRate.SelectedIndex == 0)
            {
                currentSettings.BaudRate = 0;
            }
            else
            {
                currentSettings.BaudRate = Convert.ToInt32(cmbBaudRate.SelectedItem.ToString());
            }

            currentSettings.AnalysisOptions = new PVSAnalysisOptions();
            currentSettings.AnalysisOptions.SmoothingWidth = (int)numSmoothingWidth.Value;
            currentSettings.AnalysisOptions.ThresholdParameter = numThresholdParam.Value;            
            currentSettings.AnalysisOptions.A = (double)numParamA.Value;
            currentSettings.AnalysisOptions.B = (double)numParamB.Value;
            currentSettings.AnalysisOptions.C = (double)numParamC.Value;
            currentSettings.AnalysisOptions.D = (double)numParamD.Value;
            currentSettings.AnalysisOptions.ThermalFactor = (double)numThermalFactor.Value;
            currentSettings.AnalysisOptions.CalibrationFactor = (double)numCalibrationFactor.Value;

            currentSettings.DiscardTime = numDiscardTime.Value;
            currentSettings.DiscardTotal = numDiscardTotal.Value;
            currentSettings.TimeDiscarded = chkDiscardTime.Checked;
            currentSettings.TotalDiscarded = chkDiscardTotal.Checked;

            currentSettings.PeakThreshold = numPeakThreshold.Value;
            currentSettings.FilterPeaks = chkFilterPeaks.Checked;

            currentSettings.MeasurementFrequency = cmbMeasurementFrequency.SelectedItem.ToString();
            if (!chkDownscaleFrequency.Checked)
            {
                currentSettings.DownscaleMethod = SettingsProfile.DownscaleMethods.None;
            }
            else if (radDownscaleAveraging.Checked)
            {
                currentSettings.DownscaleMethod = SettingsProfile.DownscaleMethods.Averaging;
            }
            else
            {
                currentSettings.DownscaleMethod = SettingsProfile.DownscaleMethods.Filtering;
            }

            currentSettings.DownscaleFactor = (int)numDownscaleFactor.Value;

            currentSettings.FilenamePattern = txtFilenamePattern.Text;
            currentSettings.DataLocation = txtDataLocation.Text;
            currentSettings.NumberScans = (int)numNumberScans.Value;

                        
            formChanged = false;
        }

        /// <summary>
        /// Updates the form, using the current settings as a source
        /// </summary>
        private void updateForm()
        {            
            // prevent events temporarily
            // todo: is it necessary?
            suppressEvents = true;

            // todo: profile field should be already set so ignore - can set as events prevented

            cmbSensorModel.SelectedItem = currentSettings.SensorModel;
            cmbSensorType.SelectedItem = currentSettings.SensorType;
            cmbCOM.SelectedItem = currentSettings.ComPort;
            cmbBaudRate.SelectedItem = currentSettings.BaudRate.ToString();

            // update measurement frequencies

            numSmoothingWidth.Value = (decimal)currentSettings.AnalysisOptions.SmoothingWidth;
            numThresholdParam.Value = (decimal)currentSettings.AnalysisOptions.ThresholdParameter;
            numParamA.Value = (decimal)currentSettings.AnalysisOptions.A;
            numParamB.Value = (decimal)currentSettings.AnalysisOptions.B;
            numParamC.Value = (decimal)currentSettings.AnalysisOptions.C;
            numParamD.Value = (decimal)currentSettings.AnalysisOptions.D;
            numThermalFactor.Value = (decimal)currentSettings.AnalysisOptions.ThermalFactor;
            numCalibrationFactor.Value = (decimal)currentSettings.AnalysisOptions.CalibrationFactor;

            numDiscardTime.Value = currentSettings.DiscardTime;
            numDiscardTotal.Value = currentSettings.DiscardTotal;
            chkDiscardTime.Checked = currentSettings.TimeDiscarded;
            chkDiscardTotal.Checked = currentSettings.TotalDiscarded;

            numDiscardTime.Enabled = currentSettings.TimeDiscarded;
            numDiscardTotal.Enabled = currentSettings.TotalDiscarded;

            numPeakThreshold.Value = currentSettings.PeakThreshold;
            chkFilterPeaks.Checked = currentSettings.FilterPeaks;
            numPeakThreshold.Enabled = chkFilterPeaks.Checked;

            cmbMeasurementFrequency.SelectedItem = currentSettings.MeasurementFrequency;
            switch (currentSettings.DownscaleMethod)
            {
                case SettingsProfile.DownscaleMethods.Averaging:
                    chkDownscaleFrequency.Checked = true;
                    radDownscaleAveraging.Checked = true;
                    radDownscaleFiltering.Checked = false;

                    radDownscaleAveraging.Enabled = true;
                    radDownscaleFiltering.Enabled = true;
                    break;

                case SettingsProfile.DownscaleMethods.Filtering:
                    chkDownscaleFrequency.Checked = true;
                    radDownscaleAveraging.Checked = false;
                    radDownscaleFiltering.Checked = true;

                    radDownscaleAveraging.Enabled = true;
                    radDownscaleFiltering.Enabled = true;
                    break;

                case SettingsProfile.DownscaleMethods.None:
                    chkDownscaleFrequency.Checked = false;

                    radDownscaleAveraging.Enabled = false;
                    radDownscaleFiltering.Enabled = false;
                    break;
            }
            radDownscaleAveraging.Enabled = chkDownscaleFrequency.Checked;
            radDownscaleFiltering.Enabled = chkDownscaleFrequency.Checked;

            numDownscaleFactor.Value = (decimal)currentSettings.DownscaleFactor;

            txtFilenamePattern.Text = currentSettings.FilenamePattern;
            txtDataLocation.Text = currentSettings.DataLocation;
            numNumberScans.Value = (decimal)currentSettings.NumberScans;

            suppressEvents = false;
            formChanged = false;
        }
        
        private void updateSensorModels()
        {
            cmbSensorModel.Items.Clear();

            // add the sensor models
            foreach (ISensor s in sensors)
            {
                cmbSensorModel.Items.Add(s.Properties.ModelName);
            }

            cmbSensorModel.SelectedIndex = 0;

            // select ILD1700 by default
            for (int i = 0; i < cmbSensorModel.Items.Count; i++)
            {
                if (cmbSensorModel.Items[i].ToString() == "ILD1700")
                {
                    cmbSensorModel.SelectedIndex = i;
                    break;
                }
            }
        }

        private void updateSensorTypes()
        {
            cmbSensorType.Items.Clear();

            // add the sensor types
            foreach (string type in Sensor.SensorTypes)
            {
                cmbSensorType.Items.Add(type);
            }

            cmbSensorType.SelectedIndex = 0;
        }

        private void updateCOMPorts()
        {
            string[] ports = SerialPort.GetPortNames();
            Array.Sort(ports);

            cmbCOM.Items.Clear();
            cmbCOM.Items.AddRange(ports);

            cmbCOM.SelectedIndex = 0;

            // select COM5 by default
            for (int i = 0; i < cmbCOM.Items.Count; i++)
            {
                if (cmbCOM.Items[i].ToString() == "COM5")
                {
                    cmbCOM.SelectedIndex = i;
                    break;
                }
            }
        }

        private void updateBaudRates()
        {
            cmbBaudRate.Items.Clear();
            cmbBaudRate.Items.Add("Auto detect");

            foreach (int i in Sensor.BaudRates)
            {
                cmbBaudRate.Items.Add(i.ToString());
            }

            cmbBaudRate.SelectedIndex = 0;
        }

        private void updateMeasurementFrequencies()
        {
            cmbMeasurementFrequency.Items.Clear();
            cmbMeasurementFrequency.Items.AddRange(Sensor.Frequencies);
            cmbMeasurementFrequency.SelectedIndex = 0;
        }

        private bool validate()
        {
            bool valid = true;
            int firstErrorTab = tabControlScanSettings.TabCount;

            if (cmbSensorModel.SelectedItem.ToString().Trim() == "")
            {
                errorProvider.SetError(cmbSensorModel, "Please select a sensor model");
                valid = false;
                
                // focus tab 1
                tabControlScanSettings.SelectedIndex = 0;

                return valid; // exit condition
            }

            if (cmbSensorType.SelectedItem.ToString().Trim() == "")
            {
                errorProvider.SetError(cmbSensorType, "Please select a valid sensor type");
                valid = false;
                firstErrorTab = Math.Min(firstErrorTab, 0);
            }
            if (cmbCOM.SelectedItem.ToString().Trim() == "")
            {
                errorProvider.SetError(cmbCOM, "Please select a COM port for the sensor");
                valid = false;
                firstErrorTab = Math.Min(firstErrorTab, 0);
            }
            /*if (cmbBaudRate.SelectedItem.ToString().Trim() == "" || cmbBaudRate.SelectedIndex != 0)
            {
                errorProvider.SetError(cmbBaudRate, "Please select a valid baud rate");
                valid = false;
                firstErrorTab = Math.Min(firstErrorTab, 0);
            }*/
            if (cmbMeasurementFrequency.SelectedItem.ToString().Trim() == "")
            {
                errorProvider.SetError(cmbMeasurementFrequency, "Please select a valid frequency");
                valid = false;
                firstErrorTab = Math.Min(firstErrorTab, 2);
            }

            if (chkDownscaleFrequency.Checked && !radDownscaleAveraging.Checked && !radDownscaleFiltering.Checked)
            {
                errorProvider.SetError(chkDownscaleFrequency, "Please choose a downscaling method");
                valid = false;
                firstErrorTab = Math.Min(firstErrorTab, 2);
            }

            if (txtFilenamePattern.Text.Trim() == "")
            {
                errorProvider.SetError(txtFilenamePattern, "Please enter a filename pattern");
                valid = false;
                firstErrorTab = Math.Min(firstErrorTab, 2);
            }
            if (txtDataLocation.Text.Trim() == "")
            {
                errorProvider.SetError(txtDataLocation, "Please select a data location");
                valid = false;
                firstErrorTab = Math.Min(firstErrorTab, 2);
            }
            else if (!Directory.Exists(txtDataLocation.Text))
            {
                try
                {
                    Directory.CreateDirectory(txtDataLocation.Text);
                }
                catch
                {
                    errorProvider.SetError(txtDataLocation, "Could not create directory. Try another directory");
                    valid = false;
                    firstErrorTab = Math.Min(firstErrorTab, 2);
                }
            }
            

            // focus first error
            tabControlScanSettings.SelectedIndex = firstErrorTab;

            return valid;
        }

        private void clearErrors()
        {
            errorProvider.Clear();
        }

        #endregion
    }
}
