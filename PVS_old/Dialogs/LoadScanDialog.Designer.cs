﻿namespace PVS
{
    partial class LoadScanDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.Label lblFilenamePattern;
            System.Windows.Forms.Label lblDataLocation;
            System.Windows.Forms.GroupBox grpScanId;
            System.Windows.Forms.Label lblScanId;
            this.btnRefreshScanId = new System.Windows.Forms.Button();
            this.cmbScanId = new System.Windows.Forms.ComboBox();
            this.radCustomScanId = new System.Windows.Forms.RadioButton();
            this.radLastScanId = new System.Windows.Forms.RadioButton();
            this.txtFilenamePattern = new System.Windows.Forms.TextBox();
            this.txtDataLocation = new System.Windows.Forms.TextBox();
            this.btnOK = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnDataLocationBrowse = new System.Windows.Forms.Button();
            this.folderBrowser = new System.Windows.Forms.FolderBrowserDialog();
            lblFilenamePattern = new System.Windows.Forms.Label();
            lblDataLocation = new System.Windows.Forms.Label();
            grpScanId = new System.Windows.Forms.GroupBox();
            lblScanId = new System.Windows.Forms.Label();
            grpScanId.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblFilenamePattern
            // 
            lblFilenamePattern.AutoSize = true;
            lblFilenamePattern.Location = new System.Drawing.Point(20, 21);
            lblFilenamePattern.Name = "lblFilenamePattern";
            lblFilenamePattern.Size = new System.Drawing.Size(89, 13);
            lblFilenamePattern.TabIndex = 54;
            lblFilenamePattern.Text = "Filename Pattern:";
            // 
            // lblDataLocation
            // 
            lblDataLocation.AutoSize = true;
            lblDataLocation.Location = new System.Drawing.Point(20, 47);
            lblDataLocation.Name = "lblDataLocation";
            lblDataLocation.Size = new System.Drawing.Size(77, 13);
            lblDataLocation.TabIndex = 52;
            lblDataLocation.Text = "Data Location:";
            // 
            // grpScanId
            // 
            grpScanId.Controls.Add(lblScanId);
            grpScanId.Controls.Add(this.btnRefreshScanId);
            grpScanId.Controls.Add(this.cmbScanId);
            grpScanId.Controls.Add(this.radCustomScanId);
            grpScanId.Controls.Add(this.radLastScanId);
            grpScanId.Location = new System.Drawing.Point(17, 104);
            grpScanId.Name = "grpScanId";
            grpScanId.Size = new System.Drawing.Size(218, 90);
            grpScanId.TabIndex = 51;
            grpScanId.TabStop = false;
            grpScanId.Text = "Choose Scan ID";
            // 
            // lblScanId
            // 
            lblScanId.AutoSize = true;
            lblScanId.Location = new System.Drawing.Point(94, 25);
            lblScanId.Name = "lblScanId";
            lblScanId.Size = new System.Drawing.Size(49, 13);
            lblScanId.TabIndex = 5;
            lblScanId.Text = "Scan ID:";
            // 
            // btnRefreshScanId
            // 
            this.btnRefreshScanId.Location = new System.Drawing.Point(97, 47);
            this.btnRefreshScanId.Name = "btnRefreshScanId";
            this.btnRefreshScanId.Size = new System.Drawing.Size(101, 23);
            this.btnRefreshScanId.TabIndex = 4;
            this.btnRefreshScanId.Text = "Refresh";
            this.btnRefreshScanId.UseVisualStyleBackColor = true;
            // 
            // cmbScanId
            // 
            this.cmbScanId.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbScanId.FormattingEnabled = true;
            this.cmbScanId.Location = new System.Drawing.Point(149, 20);
            this.cmbScanId.Name = "cmbScanId";
            this.cmbScanId.Size = new System.Drawing.Size(49, 21);
            this.cmbScanId.TabIndex = 3;
            // 
            // radCustomScanId
            // 
            this.radCustomScanId.AutoSize = true;
            this.radCustomScanId.Location = new System.Drawing.Point(15, 53);
            this.radCustomScanId.Name = "radCustomScanId";
            this.radCustomScanId.Size = new System.Drawing.Size(59, 17);
            this.radCustomScanId.TabIndex = 1;
            this.radCustomScanId.TabStop = true;
            this.radCustomScanId.Text = "custom";
            this.radCustomScanId.UseVisualStyleBackColor = true;
            // 
            // radLastScanId
            // 
            this.radLastScanId.AutoSize = true;
            this.radLastScanId.Checked = true;
            this.radLastScanId.Location = new System.Drawing.Point(15, 25);
            this.radLastScanId.Name = "radLastScanId";
            this.radLastScanId.Size = new System.Drawing.Size(41, 17);
            this.radLastScanId.TabIndex = 0;
            this.radLastScanId.TabStop = true;
            this.radLastScanId.Text = "last";
            this.radLastScanId.UseVisualStyleBackColor = true;
            // 
            // txtFilenamePattern
            // 
            this.txtFilenamePattern.Location = new System.Drawing.Point(118, 18);
            this.txtFilenamePattern.Name = "txtFilenamePattern";
            this.txtFilenamePattern.Size = new System.Drawing.Size(117, 20);
            this.txtFilenamePattern.TabIndex = 55;
            // 
            // txtDataLocation
            // 
            this.txtDataLocation.Location = new System.Drawing.Point(23, 64);
            this.txtDataLocation.Name = "txtDataLocation";
            this.txtDataLocation.ReadOnly = true;
            this.txtDataLocation.Size = new System.Drawing.Size(122, 20);
            this.txtDataLocation.TabIndex = 53;
            this.txtDataLocation.Text = "c:\\";
            // 
            // btnOK
            // 
            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOK.Location = new System.Drawing.Point(46, 205);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 56;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(140, 205);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 56;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // btnDataLocationBrowse
            // 
            this.btnDataLocationBrowse.Location = new System.Drawing.Point(160, 61);
            this.btnDataLocationBrowse.Name = "btnDataLocationBrowse";
            this.btnDataLocationBrowse.Size = new System.Drawing.Size(75, 23);
            this.btnDataLocationBrowse.TabIndex = 57;
            this.btnDataLocationBrowse.Text = "Browse";
            this.btnDataLocationBrowse.UseVisualStyleBackColor = true;
            // 
            // LoadScanDialog
            // 
            this.AcceptButton = this.btnOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(251, 239);
            this.Controls.Add(this.btnDataLocationBrowse);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.txtFilenamePattern);
            this.Controls.Add(lblFilenamePattern);
            this.Controls.Add(this.txtDataLocation);
            this.Controls.Add(lblDataLocation);
            this.Controls.Add(grpScanId);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "LoadScanDialog";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "Choose scan to load";
            grpScanId.ResumeLayout(false);
            grpScanId.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnRefreshScanId;
        private System.Windows.Forms.ComboBox cmbScanId;
        private System.Windows.Forms.RadioButton radCustomScanId;
        private System.Windows.Forms.RadioButton radLastScanId;
        private System.Windows.Forms.TextBox txtFilenamePattern;
        private System.Windows.Forms.TextBox txtDataLocation;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnDataLocationBrowse;
        private System.Windows.Forms.FolderBrowserDialog folderBrowser;

    }
}