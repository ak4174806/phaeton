﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using PVS.Results;

namespace PVS
{
    public partial class ScanFieldLabelsForm : Form
    {
        private PVSResults results;
        private bool suppressEvents = false;

        public ScanFieldLabelsForm(PVSResults results)
        {
            InitializeComponent();

            // get db wrapper
            this.results = results;

            // events
            this.Load += new EventHandler(ScanFieldLabelsForm_Load);
            this.VisibleChanged += new EventHandler(ScanFieldLabelsForm_VisibleChanged);

            this.cmbFieldList.SelectedIndexChanged += new EventHandler(cmbFieldList_SelectedIndexChanged);
            this.btnSave.Click += new EventHandler(btnSave_Click);
            this.btnCancel.Click += new EventHandler(btnCancel_Click);

            this.txtFieldName.TextChanged += new EventHandler(editField_TextChanged);
            this.txtFieldDefaults.TextChanged += new EventHandler(editField_TextChanged);
        }

        private void LoadField()
        {
            txtFieldName.Text = cmbFieldList.SelectedItem.ToString();
            
            // add 1 to get the field_id which is {1, 2, 3, 4}
            string[] defaults = results.GetFieldDefaults(cmbFieldList.SelectedIndex + 1);
            txtFieldDefaults.Text = String.Join(System.Environment.NewLine, defaults);
        }

        void ScanFieldLabelsForm_Load(object sender, EventArgs e)
        {
            // nothing
        }

        void ScanFieldLabelsForm_VisibleChanged(object sender, EventArgs e)
        {
            // every time shown
            if (Visible)
            {
                suppressEvents = true;

                string[] fields = results.GetScanFieldNames();
                cmbFieldList.Items.Clear();
                cmbFieldList.Items.AddRange(fields);

                // load first field
                cmbFieldList.SelectedIndex = 0;
                LoadField();
                                
                btnSave.Enabled = false;
                btnCancel.Enabled = false;

                suppressEvents = false;                
            }
        }

        void btnSave_Click(object sender, EventArgs e)
        {
            int field_id = cmbFieldList.SelectedIndex + 1;
            string defaults_all = txtFieldDefaults.Text.Replace("\r\n", "\n").Replace("\n\r", "\n").Replace("\r", "\n");
            string[] defaults = defaults_all.Split('\n');

            results.UpdateScanField(field_id, txtFieldName.Text, defaults);
            cmbFieldList.Items[cmbFieldList.SelectedIndex] = txtFieldName.Text;

            btnSave.Enabled = false;
            btnCancel.Enabled = false;
        }
        
        void btnCancel_Click(object sender, EventArgs e)
        {
            LoadField();
            
            btnSave.Enabled = false;
            btnCancel.Enabled = false;
        }

        void cmbFieldList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (suppressEvents) return;

            LoadField();

            btnSave.Enabled = false;
            btnCancel.Enabled = false;
        }

        void editField_TextChanged(object sender, EventArgs e)
        {
            if (suppressEvents) return;

            btnSave.Enabled = true;
            btnCancel.Enabled = true;
        }
    }
}
