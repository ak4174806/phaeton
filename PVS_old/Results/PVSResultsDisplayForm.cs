﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using PVS.Results;
using System.IO;
using PaintAnalysis.Common;
using PaintAnalysis.Common.Results;

namespace PVS.Results
{
    public partial class PVSResultsDisplayForm : Form
    {
        PVSResults results;

        public PVSResultsDisplayForm(PVSResults results)
        {
            InitializeComponent();

            this.results = results;
            
            // We manually added the columns, do not auto add any more..
            resultsGrid.AutoGenerateColumns = false;
            btnCancel.Enabled = false;
            btnSave.Enabled = false;
            btnRefresh.Enabled = true;

            resultsGrid.CellFormatting += new DataGridViewCellFormattingEventHandler(resultsGrid_CellFormatting);
            resultsGrid.CellParsing += new DataGridViewCellParsingEventHandler(resultsGrid_CellParsing);
            resultsGrid.DataError += new DataGridViewDataErrorEventHandler(resultsGrid_DataError);

            resultsGrid.CellClick += new DataGridViewCellEventHandler(resultsGrid_CellClick);
            resultsGrid.CellValueChanged += new DataGridViewCellEventHandler(resultsGrid_CellValueChanged);
            resultsGrid.UserAddedRow += new DataGridViewRowEventHandler(resultsGrid_UserAddedRow);
            resultsGrid.UserDeletedRow += new DataGridViewRowEventHandler(resultsGrid_UserDeletedRow);

            resultsGrid.CellValidating += new DataGridViewCellValidatingEventHandler(resultsGrid_CellValidating);

            btnExport.Click += new EventHandler(btnExport_Click);

            // see http://social.msdn.microsoft.com/forums/en-US/winformsdatacontrols/thread/dc2e02c1-0ef7-4721-a075-75e1b37ba1a9/
            //resultsGrid.EditingControlShowing += new DataGridViewEditingControlShowingEventHandler(resultsGrid_EditingControlShowing);
            // alternative: http://social.msdn.microsoft.com/forums/en-US/winformsdatacontrols/thread/d7347bfd-3392-4c82-8ca7-b60e1515b2e3/ at end with faux column
        }
        

        void resultsGrid_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            resultsGrid.Rows[e.RowIndex].ErrorText = "";

            // don't validate a new row until it is finished
            if (resultsGrid.Rows[e.RowIndex].IsNewRow) return;

            DateTime date;
            decimal pvs;
            int scanId;
            if (e.ColumnIndex == 1 && !DateTime.TryParse(e.FormattedValue.ToString(), out date))
            {
                // date
                e.Cancel = true;
                resultsGrid.Rows[e.RowIndex].ErrorText = "Please enter a valid date.";
            }
            else if (e.ColumnIndex == 2 && !Decimal.TryParse(e.FormattedValue.ToString(), out pvs))
            {
                // pvs
                e.Cancel = true;
                resultsGrid.Rows[e.RowIndex].ErrorText = "Please enter a valid pvs.";
            }
            else if (e.ColumnIndex == 3 && !Directory.Exists(e.FormattedValue.ToString()))
            {
                // scan path
                e.Cancel = true;
                resultsGrid.Rows[e.RowIndex].ErrorText = "Please enter a directory that exists.";
            }
            else if (e.ColumnIndex == 4 && !Int32.TryParse(e.FormattedValue.ToString(), out scanId))
            {
                // scan id
                e.Cancel = true;
                resultsGrid.Rows[e.RowIndex].ErrorText = "Please enter a valid scan id.";
            }
        }

        void results_ErrorEncountered(object sender, ErrorEncounteredEventArgs e)
        {
            MessageBox.Show(e.Description, "Error", MessageBoxButtons.OK);
        }

        void resultsGrid_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            handleDataGridChanged();
        }
        void resultsGrid_UserDeletedRow(object sender, DataGridViewRowEventArgs e)
        {
            handleDataGridChanged();
        }

        void resultsGrid_UserAddedRow(object sender, DataGridViewRowEventArgs e)
        {
            handleDataGridChanged();
        }
        private void handleDataGridChanged()
        {
            btnCancel.Enabled = true;
            btnSave.Enabled = true;
            btnRefresh.Enabled = false;
        }


        void resultsGrid_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == -1 || e.ColumnIndex >= resultsGrid.Columns.Count) return;

            if (e.RowIndex < 0) return;

            // look for delete button click
            if (e.ColumnIndex == resultsGrid.Columns["deleteColumn"].Index)
            {
                resultsGrid.Rows.RemoveAt(e.RowIndex);
                resultsGrid_CellValueChanged(sender, e);
            }
        }
        
        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {
                results.ResultsDatabase.ErrorEncountered += new EventHandler<ErrorEncounteredEventArgs>(results_ErrorEncountered);
                RefreshDisplay();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        void resultsGrid_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            if (e.ColumnIndex == 1)
                MessageBox.Show("Please enter a valid date");
            else if (e.ColumnIndex == 2)
                MessageBox.Show("Please enter a valid PVS value");
            else if (e.ColumnIndex == 3)
                MessageBox.Show("Please enter a valid scan directory");
            else if (e.ColumnIndex == 4)
                MessageBox.Show("Please enter a valid scan ID");
            else if (e.ColumnIndex >= 5 && e.ColumnIndex <= 8)
            {
                // validated elsewhere
                return;
            }
            else
                MessageBox.Show("Please enter valid data");
            
            e.Cancel = true;
        }

        private void resultsGrid_CellParsing(object sender, DataGridViewCellParsingEventArgs e)
        {
            if (e.ColumnIndex == -1 || e.ColumnIndex >= resultsGrid.Columns.Count || e.RowIndex == -1) return;
            
            // looking for date column
            if (resultsGrid.Columns[e.ColumnIndex].Name == "dateColumn")
            {
                if (e != null && e.Value != null)
                {
                    try
                    {
                        e.Value = DateTime.Parse(e.Value.ToString()).ToBinary();
                        e.ParsingApplied = true;
                    }
                    catch (Exception)
                    {
                        e.ParsingApplied = false;
                    }
                }
            }
        }

        private void resultsGrid_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.ColumnIndex == -1 || e.ColumnIndex >= resultsGrid.Columns.Count) return;

            // looking for date column
            if (resultsGrid.Columns[e.ColumnIndex].Name == "dateColumn")
            {
                if (e!= null && e.Value != null && e.Value != e.CellStyle.DataSourceNullValue)
                {
                    try
                    {
                        e.Value = DateTime.FromBinary((long)e.Value).ToString("g");
                        e.FormattingApplied = true;                        
                    }
                    catch (FormatException)
                    {
                        e.FormattingApplied = false;
                    }
                }
            }
            else if (resultsGrid.Columns[e.ColumnIndex].Name == "pvsColumn")
            {
                if (e != null && e.Value != null && e.Value != e.CellStyle.DataSourceNullValue)
                {
                    try
                    {
                        e.Value = (Convert.ToDecimal(e.Value)).ToString("F2");
                        e.FormattingApplied = true;
                    }
                    catch (FormatException)
                    {
                        e.FormattingApplied = false;
                    }
                }
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            bool saved = results.SaveChanges();

            if (saved)
            {
                MessageBox.Show("Changes saved successfully");

                btnCancel.Enabled = false;
                btnSave.Enabled = false;
                btnRefresh.Enabled = true;
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            results.CancelChanges();

            btnCancel.Enabled = false;
            btnSave.Enabled = false;
            btnRefresh.Enabled = true;
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            RefreshDisplay();
        }

        private void RefreshDisplay()
        {
            resultsGrid.DataSource = new DataView(results.LoadData());

            // update field names
            string[] fieldNames = results.GetScanFieldNames();
            if (fieldNames.Length > 0) field1Column.HeaderText = fieldNames[0];
            if (fieldNames.Length > 1) field2Column.HeaderText = fieldNames[1];
            if (fieldNames.Length > 2) field3Column.HeaderText = fieldNames[2];
            if (fieldNames.Length > 3) field4Column.HeaderText = fieldNames[3];

            // bound to defaults
            /*field1Column.DataSource = results.GetFieldDefaults(1);
            field2Column.DataSource = results.GetFieldDefaults(2);
            field3Column.DataSource = results.GetFieldDefaults(3);
            field4Column.DataSource = results.GetFieldDefaults(4);*/
        }



        void btnExport_Click(object sender, EventArgs e)
        {
            saveExportFile.ShowDialog();
            if (saveExportFile.FileName != "")
            {
                string filename = saveExportFile.FileName;

                // open file
                StreamWriter sw = null;
                try
                {
                    sw = new StreamWriter(filename, false);
                    sw.WriteLine(String.Join(",", new string[] { "Date", "Time", "PVS", "Scan ID", field1Column.HeaderText, field2Column.HeaderText, field3Column.HeaderText, field4Column.HeaderText }));

                    foreach (DataGridViewRow row in resultsGrid.Rows)
                    {
                        if (row.IsNewRow) continue;
                        sw.Write("{0},{1},{2},{3},{4},{5},{6},{7}",
                            DateTime.Parse(row.Cells["dateColumn"].FormattedValue.ToString()).Date.ToShortDateString(),
                            DateTime.Parse(row.Cells["dateColumn"].FormattedValue.ToString()).TimeOfDay.ToString(),
                            row.Cells["pvsColumn"].FormattedValue,
                            row.Cells["scanIdColumn"].FormattedValue,
                            row.Cells["field1Column"].FormattedValue,
                            row.Cells["field2Column"].FormattedValue,
                            row.Cells["field3Column"].FormattedValue,
                            row.Cells["field4Column"].FormattedValue
                            );
                        sw.WriteLine();
                    }

                    sw.Flush();
                    sw.Close();
                }
                catch (Exception ex)
                {
                    throw new ApplicationException("Could not write to '" + filename + "': " + ex.Message);
                }
                finally
                {
                    if (sw != null)
                        sw.Close();
                }
            }
        }
    }
}
