﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PVS.Results
{
    public class PVSResult
    {
        private int resultId;
        public int ResultId
        {
            get { return resultId; }
            set { resultId = value; }
        }

        private DateTime date;
        public DateTime Date
        {
            get { return date; }
            set { date = value; }
        }

        private double pvs;
        public double PVS
        {
            get { return pvs; }
            set { pvs = value; }
        }

        private string scanPath;
        public string ScanPath
        {
            get { return scanPath; }
            set { scanPath = value; }
        }

        private int scanId;
        public int ScanId
        {
            get { return scanId; }
            set { scanId = value; }
        }

        private string field1;
        public string Field1
        {
            get { return field1; }
            set { field1 = value; }
        }

        private string field2;
        public string Field2
        {
            get { return field2; }
            set { field2 = value; }
        }

        private string field3;
        public string Field3
        {
            get { return field3; }
            set { field3 = value; }
        }

        private string field4;
        public string Field4
        {
            get { return field4; }
            set { field4 = value; }
        }
    }
}
