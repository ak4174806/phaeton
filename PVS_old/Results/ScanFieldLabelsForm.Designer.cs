﻿namespace PVS
{
    partial class ScanFieldLabelsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.Label lblChooseField;
            System.Windows.Forms.GroupBox grpEditField;
            System.Windows.Forms.Label lblFieldName;
            System.Windows.Forms.Label lblFieldDefaults;
            this.cmbFieldList = new System.Windows.Forms.ComboBox();
            this.txtFieldName = new System.Windows.Forms.TextBox();
            this.txtFieldDefaults = new System.Windows.Forms.TextBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            lblChooseField = new System.Windows.Forms.Label();
            grpEditField = new System.Windows.Forms.GroupBox();
            lblFieldName = new System.Windows.Forms.Label();
            lblFieldDefaults = new System.Windows.Forms.Label();
            grpEditField.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblChooseField
            // 
            lblChooseField.AutoSize = true;
            lblChooseField.Location = new System.Drawing.Point(12, 18);
            lblChooseField.Name = "lblChooseField";
            lblChooseField.Size = new System.Drawing.Size(68, 13);
            lblChooseField.TabIndex = 0;
            lblChooseField.Text = "Choose field:";
            // 
            // cmbFieldList
            // 
            this.cmbFieldList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbFieldList.FormattingEnabled = true;
            this.cmbFieldList.Location = new System.Drawing.Point(98, 15);
            this.cmbFieldList.Name = "cmbFieldList";
            this.cmbFieldList.Size = new System.Drawing.Size(121, 21);
            this.cmbFieldList.TabIndex = 1;
            // 
            // grpEditField
            // 
            grpEditField.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            grpEditField.Controls.Add(this.txtFieldDefaults);
            grpEditField.Controls.Add(this.txtFieldName);
            grpEditField.Controls.Add(lblFieldDefaults);
            grpEditField.Controls.Add(lblFieldName);
            grpEditField.Location = new System.Drawing.Point(15, 48);
            grpEditField.Name = "grpEditField";
            grpEditField.Size = new System.Drawing.Size(215, 160);
            grpEditField.TabIndex = 2;
            grpEditField.TabStop = false;
            grpEditField.Text = "Edit";
            // 
            // lblFieldName
            // 
            lblFieldName.AutoSize = true;
            lblFieldName.Location = new System.Drawing.Point(6, 25);
            lblFieldName.Name = "lblFieldName";
            lblFieldName.Size = new System.Drawing.Size(63, 13);
            lblFieldName.TabIndex = 0;
            lblFieldName.Text = "Field Name:";
            // 
            // lblFieldDefaults
            // 
            lblFieldDefaults.AutoSize = true;
            lblFieldDefaults.Location = new System.Drawing.Point(7, 51);
            lblFieldDefaults.Name = "lblFieldDefaults";
            lblFieldDefaults.Size = new System.Drawing.Size(74, 26);
            lblFieldDefaults.TabIndex = 1;
            lblFieldDefaults.Text = "Field Defaults:\r\n(one per line)";
            // 
            // txtFieldName
            // 
            this.txtFieldName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFieldName.Location = new System.Drawing.Point(87, 22);
            this.txtFieldName.Name = "txtFieldName";
            this.txtFieldName.Size = new System.Drawing.Size(118, 20);
            this.txtFieldName.TabIndex = 2;
            // 
            // txtFieldDefaults
            // 
            this.txtFieldDefaults.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFieldDefaults.Location = new System.Drawing.Point(87, 51);
            this.txtFieldDefaults.Multiline = true;
            this.txtFieldDefaults.Name = "txtFieldDefaults";
            this.txtFieldDefaults.Size = new System.Drawing.Size(117, 100);
            this.txtFieldDefaults.TabIndex = 3;
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSave.Location = new System.Drawing.Point(21, 217);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 3;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Location = new System.Drawing.Point(144, 217);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 3;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // ScanFieldLabelsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(241, 250);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(grpEditField);
            this.Controls.Add(this.cmbFieldList);
            this.Controls.Add(lblChooseField);
            this.MinimumSize = new System.Drawing.Size(249, 227);
            this.Name = "ScanFieldLabelsForm";
            this.Text = "Edit Scan Fields";
            grpEditField.ResumeLayout(false);
            grpEditField.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cmbFieldList;
        private System.Windows.Forms.TextBox txtFieldDefaults;
        private System.Windows.Forms.TextBox txtFieldName;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnCancel;
    }
}