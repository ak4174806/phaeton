﻿namespace PVS.Results
{
    partial class PVSResultsDisplayForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.resultsGrid = new System.Windows.Forms.DataGridView();
            this.resultIdColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dateColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pvsColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.scanPathColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.scanIdColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.field1Column = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.field2Column = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.field3Column = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.field4Column = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.deleteColumn = new System.Windows.Forms.DataGridViewButtonColumn();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.btnExport = new System.Windows.Forms.Button();
            this.saveExportFile = new System.Windows.Forms.SaveFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.resultsGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // resultsGrid
            // 
            this.resultsGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.resultsGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.resultsGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.resultIdColumn,
            this.dateColumn,
            this.pvsColumn,
            this.scanPathColumn,
            this.scanIdColumn,
            this.field1Column,
            this.field2Column,
            this.field3Column,
            this.field4Column,
            this.deleteColumn});
            this.resultsGrid.Location = new System.Drawing.Point(0, 0);
            this.resultsGrid.Name = "resultsGrid";
            this.resultsGrid.Size = new System.Drawing.Size(949, 130);
            this.resultsGrid.TabIndex = 0;
            // 
            // resultIdColumn
            // 
            this.resultIdColumn.DataPropertyName = "result_id";
            this.resultIdColumn.HeaderText = "Result ID";
            this.resultIdColumn.Name = "resultIdColumn";
            this.resultIdColumn.ReadOnly = true;
            this.resultIdColumn.Visible = false;
            // 
            // dateColumn
            // 
            this.dateColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dateColumn.DataPropertyName = "date";
            this.dateColumn.HeaderText = "Date";
            this.dateColumn.Name = "dateColumn";
            this.dateColumn.Width = 55;
            // 
            // pvsColumn
            // 
            this.pvsColumn.DataPropertyName = "pvs";
            this.pvsColumn.HeaderText = "PVS";
            this.pvsColumn.Name = "pvsColumn";
            // 
            // scanPathColumn
            // 
            this.scanPathColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.scanPathColumn.DataPropertyName = "scan_path";
            this.scanPathColumn.HeaderText = "Scan Path";
            this.scanPathColumn.Name = "scanPathColumn";
            this.scanPathColumn.Width = 82;
            // 
            // scanIdColumn
            // 
            this.scanIdColumn.DataPropertyName = "scan_id";
            this.scanIdColumn.HeaderText = "Scan ID";
            this.scanIdColumn.Name = "scanIdColumn";
            // 
            // field1Column
            // 
            this.field1Column.DataPropertyName = "field1";
            this.field1Column.HeaderText = "Column1";
            this.field1Column.Name = "field1Column";
            this.field1Column.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // field2Column
            // 
            this.field2Column.DataPropertyName = "field2";
            this.field2Column.HeaderText = "Column2";
            this.field2Column.Name = "field2Column";
            this.field2Column.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // field3Column
            // 
            this.field3Column.DataPropertyName = "field3";
            this.field3Column.HeaderText = "Column3";
            this.field3Column.Name = "field3Column";
            this.field3Column.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // field4Column
            // 
            this.field4Column.DataPropertyName = "field4";
            this.field4Column.HeaderText = "Column4";
            this.field4Column.Name = "field4Column";
            this.field4Column.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // deleteColumn
            // 
            this.deleteColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.deleteColumn.HeaderText = "Delete Result";
            this.deleteColumn.Name = "deleteColumn";
            this.deleteColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.deleteColumn.Text = "Delete";
            this.deleteColumn.UseColumnTextForButtonValue = true;
            this.deleteColumn.Width = 96;
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Location = new System.Drawing.Point(805, 139);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(133, 23);
            this.btnCancel.TabIndex = 4;
            this.btnCancel.Text = "Cancel Changes";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Location = new System.Drawing.Point(670, 138);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(129, 23);
            this.btnSave.TabIndex = 3;
            this.btnSave.Text = "Save Changes";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnRefresh
            // 
            this.btnRefresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnRefresh.Location = new System.Drawing.Point(12, 139);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(129, 23);
            this.btnRefresh.TabIndex = 3;
            this.btnRefresh.Text = "Refresh";
            this.btnRefresh.UseVisualStyleBackColor = true;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // btnExport
            // 
            this.btnExport.Location = new System.Drawing.Point(160, 139);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(108, 22);
            this.btnExport.TabIndex = 6;
            this.btnExport.Text = "Export Database";
            this.btnExport.UseVisualStyleBackColor = true;
            // 
            // saveExportFile
            // 
            this.saveExportFile.DefaultExt = "csv";
            this.saveExportFile.FileName = "pvs_results.csv";
            this.saveExportFile.Filter = "CSV Files|*.csv|All files|*.*";
            // 
            // PVSResultsDisplayForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(950, 171);
            this.Controls.Add(this.btnExport);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnRefresh);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.resultsGrid);
            this.Name = "PVSResultsDisplayForm";
            this.Text = "PVS Results";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.resultsGrid)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView resultsGrid;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.DataGridViewTextBoxColumn resultIdColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dateColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn pvsColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn scanPathColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn scanIdColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn field1Column;
        private System.Windows.Forms.DataGridViewTextBoxColumn field2Column;
        private System.Windows.Forms.DataGridViewTextBoxColumn field3Column;
        private System.Windows.Forms.DataGridViewTextBoxColumn field4Column;
        private System.Windows.Forms.DataGridViewButtonColumn deleteColumn;
        private System.Windows.Forms.Button btnExport;
        private System.Windows.Forms.SaveFileDialog saveExportFile;


    }
}