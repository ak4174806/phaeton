﻿using System;
using System.Collections.Generic;
using System.Text;

using System.Data;
using System.Data.SQLite;
using System.IO;
using PaintAnalysis.Common.Results;

namespace PVS.Results
{
    public class PVSResults
    {
        // get instance
        /*private static PVSResults instance = null;
        public static PVSResults GetInstance()
        {
            if (instance == null)
                instance = new PVSResults();

            return instance;
        }*/

        public event EventHandler<ErrorEncounteredEventArgs> ErrorEncountered;
        protected void OnErrorEncountered(ErrorEncounteredEventArgs e)
        {
            if (ErrorEncountered != null)
                ErrorEncountered(this, e);
        }

        
        private SQLiteDataAdapter resultsDataAdapter;
        public DataTable ResultsTable { get; private set; }
        public ResultsDB ResultsDatabase { get; private set; }

        public PVSResults(ResultsDB ResultsDb)
        {
            ResultsDatabase = ResultsDb;
            ResultsTable = ResultsDatabase.CombinedDataSet.Tables.Add("scan_results");

            // forward errors from resultsdb
            ResultsDatabase.ErrorEncountered += new EventHandler<ErrorEncounteredEventArgs>(delegate(object sender, ErrorEncounteredEventArgs e)
            {
                OnErrorEncountered(e);
            });            
        }

        public bool SaveChanges()
        {
            try
            {
                resultsDataAdapter.Update(ResultsTable);
                return true;
            }
            catch (Exception ex)
            {
                OnErrorEncountered(new ErrorEncounteredEventArgs(ex.Message));
                return false;
            }
        }

        public void CancelChanges()
        {
            ResultsTable.RejectChanges();
        }

        public bool AddResultIfNew(PVSResult result)
        {
            // check if it is new
            string query = "SELECT COUNT([result_id]) FROM [scan_results] WHERE [scan_path]='" + result.ScanPath + "' AND [scan_id]=" + result.ScanId;
            string res = ResultsDatabase.Database.ExecuteScalar(query);
            if (res != "")
            {
                // don't handle case when res == ""
                int iRes;
                bool success = Int32.TryParse(res, out iRes);
                if (success && iRes > 0)
                {
                    // this scan already exists here
                    return true;
                }
            }

            if (result.Date == null)
                result.Date = DateTime.Now;

            Dictionary<string, string> data = new Dictionary<string,string>();
            // don't set result_id
            data["date"] = result.Date.ToBinary().ToString();
            data["pvs"] = result.PVS.ToString();
            data["scan_path"] = result.ScanPath;
            data["scan_id"] = result.ScanId.ToString();
            data["field1"] = result.Field1;
            data["field2"] = result.Field2;
            data["field3"] = result.Field3;
            data["field4"] = result.Field4;

            return ResultsDatabase.Database.Insert("scan_results", data);
        }
        
        public DataTable LoadData()
        {
            SQLiteConnection cnn = ResultsDatabase.Database.GetConnection();
            
            // command to get data
            SQLiteCommand command = cnn.CreateCommand();
            command.CommandText = "SELECT [result_id], [date], [pvs], [scan_path], [scan_id], [field1], [field2], [field3], [field4] FROM scan_results";

            // Create the data adapter using our select command
            resultsDataAdapter = new SQLiteDataAdapter(command);

            // The command builder will take care of our update, insert, deletion commands
            SQLiteCommandBuilder commandBuilder = new SQLiteCommandBuilder(resultsDataAdapter);

            // Create a dataset and fill it with our records
            ResultsTable.Clear();
            resultsDataAdapter.Fill(ResultsTable);

            return ResultsTable;
        }
        
        public string[] GetScanFieldNames()
        {
            // command to get data
            DataTable table = ResultsDatabase.Database.GetDataTable("SELECT [field_name] FROM [scan_field_names] ORDER BY [field_id]");
            string[] names = new string[table.Rows.Count];

            int i = 0;
            foreach (DataRow row in table.Rows)
            {
                names[i] = (string)row["field_name"];
                i++;
            }

            return names;
        }

        public DataTable GetFieldDefaultsTable(int field_id)
        {
            return ResultsDatabase.Database.GetDataTable("SELECT [field_default_value] FROM [field_defaults] WHERE [field_id]=" + field_id.ToString());
        }

        public string[] GetFieldDefaults(int field_id)
        {
            DataTable dt = GetFieldDefaultsTable(field_id);
            string[] defaults = new string[dt.Rows.Count];
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                defaults[i] = (string)dt.Rows[i]["field_default_value"];
            }

            return defaults;
        }

        public bool UpdateScanField(int field_id, string field_name, string[] field_defaults)
        {
            SQLiteConnection cnn = ResultsDatabase.Database.GetConnection();

            // create new transaction scope
            using (SQLiteCommand command = cnn.CreateCommand())
            using (SQLiteTransaction transaction = cnn.BeginTransaction())
            {
                try
                {
                    command.Transaction = transaction;
                    
                    // update scan name
                    command.CommandText = "UPDATE [scan_field_names] SET [field_name]=@field_name WHERE [field_id]=@field_id";
                    command.Parameters.Add("@field_name", DbType.String).Value = field_name;
                    command.Parameters.Add("@field_id", DbType.Int32).Value = field_id;
                    command.ExecuteNonQuery();

                    // update default values
                    command.CommandText = "DELETE FROM [field_defaults] WHERE [field_id]=@field_id";
                    command.Parameters.RemoveAt(0); // remove field_name
                    command.ExecuteNonQuery();

                    command.CommandText = "INSERT INTO [field_defaults] ([field_id], [field_default_value]) VALUES (@field_id, @field_default)";
                    command.Parameters.Add("@field_default", DbType.String);
                    foreach (string s in field_defaults)
                    {
                        command.Parameters["@field_default"].Value = s;
                        command.ExecuteNonQuery();
                    }
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    OnErrorEncountered(new ErrorEncounteredEventArgs(ex.Message));
                    return false;
                }

                transaction.Commit();
                return true;
            }
        }
    }
}
