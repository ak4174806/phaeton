-- reanmed table
ALTER TABLE results RENAME TO scan_results;
ALTER TABLE scan_results ADD COLUMN field1 TEXT;
ALTER TABLE scan_results ADD COLUMN field2 TEXT;
ALTER TABLE scan_results ADD COLUMN field3 TEXT;
ALTER TABLE scan_results ADD COLUMN field4 TEXT;

-- added in v 1.1
DROP TABLE IF EXISTS scan_field_names;
DROP TABLE IF EXISTS field_defaults; 

-- maps names of field to each field
CREATE TABLE scan_field_names
(
	field_id INTEGER PRIMARY KEY,
	field_name TEXT NOT NULL	
);

-- allows defaults of each field to be saved
CREATE TABLE field_defaults
(
	field_id INTEGER NOT NULL,
	field_default_value TEXT NOT NULL
);

-- default scan field names
INSERT INTO scan_field_names VALUES ( 1, 'Paint Type' );
INSERT INTO scan_field_names VALUES ( 2, 'Colour and Gloss' );
INSERT INTO scan_field_names VALUES ( 3, 'Batch Number' );
INSERT INTO scan_field_names VALUES ( 4, 'Reference Number' );

-- update version number
UPDATE keystore SET [value]='1.1' WHERE [key]='database_version';