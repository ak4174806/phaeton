DROP TABLE IF EXISTS results;
DROP TABLE IF EXISTS scan_results;
DROP TABLE IF EXISTS keystore; 
-- renamed in 1.1
CREATE TABLE scan_results 
(
	result_id INTEGER PRIMARY KEY, -- unique identifier of scan
	date INTEGER NOT NULL, 
        pvs TEXT NOT NULL, 
	scan_path TEXT NOT NULL, 
	scan_id INTEGER NOT NULL, -- scan id as used in filename of scan
	-- added in 1.1
	field1 TEXT,
	field2 TEXT,
	field3 TEXT,
	field4 TEXT
);

CREATE TABLE keystore
(
	key TEXT PRIMARY KEY,
	value TEXT
);

INSERT INTO keystore VALUES 
(
	'database_version', '1.1'
);

-- added in v 1.1
DROP TABLE IF EXISTS scan_field_names;
DROP TABLE IF EXISTS field_defaults; 

-- maps names of field to each field
CREATE TABLE scan_field_names
(
	field_id INTEGER PRIMARY KEY,
	field_name TEXT NOT NULL	
);

-- allows defaults of each field to be saved
CREATE TABLE field_defaults
(
	field_id INTEGER NOT NULL,
	field_default_value TEXT NOT NULL
);

-- default scan field names
INSERT INTO scan_field_names VALUES ( 1, 'Paint Type' );
INSERT INTO scan_field_names VALUES ( 2, 'Colour and Gloss' );
INSERT INTO scan_field_names VALUES ( 3, 'Batch Number' );
INSERT INTO scan_field_names VALUES ( 4, 'Reference Number' );