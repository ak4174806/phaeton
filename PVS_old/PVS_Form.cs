﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using System.Threading;
using System.IO;
using System.IO.Ports;
using System.Text.RegularExpressions;

using NLog;
using PaintAnalysis.Common;
using SensorControl;
using SensorControl.Sensors;
using ZedGraph;
using PVS.Scans;
using Analysis;
using PVS.Results;
using PaintAnalysis.Common.Results;

namespace PVS
{
    public partial class PVS_Form : Form
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        
		// settings form instance - release in destroyer
        // wraps settings as well, also sensor
        PVSOptionsForm optionsDialog;
        LoadScanDialog loadScanDialog;
        ScanFieldLabelsForm scanFieldsDialog;
        PVSResults pvsResults;

        // store currently loaded analysis
        List<double[]> savedPrev;        
        private PVSAnalysis[] AnalysisWet;
        private PVSAnalysis[] AnalysisDry;
        private int wetCount = -1;
        private int dryCount = -1;

        // cache so DrawGraphs doesn't need to recalculate values every time
        bool cInvertFirst;
        bool cInvertAlternate;

        // used to pause events for graph display
        private bool suppressEvents = false;

        #region States
        public enum ConnectionStates
        {
            Connected,
            Disconnected
        }
        private ConnectionStates connectionState;
        public ConnectionStates ConnectionState
        {
            get { return connectionState; }
            set
            {
                connectionState = value;

                // update form
                if (value == ConnectionStates.Connected)
                {
                    btnConnect.Text = "Disconnect";
                    btnStartScan.Enabled = (ScanState != ScanStates.Scanning);

                    txtSampleID1.Enabled = (ScanState == ScanStates.NoScan);
                    txtSampleID2.Enabled = (ScanState == ScanStates.NoScan);
                    radSampleGenerateTimestamp.Enabled = (ScanState == ScanStates.NoScan);
                    radSampleCustomTimestamp.Enabled = (ScanState == ScanStates.NoScan);
                    datSampleCustomTimestamp.Enabled = (ScanState == ScanStates.NoScan && radSampleCustomTimestamp.Checked);

                    cmbSelectedProfile.Enabled = false;
                }
                else
                {
                    btnConnect.Text = "Connect";
                    btnStartScan.Enabled = false;

                    txtSampleID1.Enabled = false;
                    txtSampleID2.Enabled = false;
                    radSampleGenerateTimestamp.Enabled = false;
                    radSampleCustomTimestamp.Enabled = false;
                    datSampleCustomTimestamp.Enabled = false;

                    cmbSelectedProfile.Enabled = (ScanState == ScanStates.NoScan);
                }
            }
        }

        public enum ScanStates
        {
            Scanning,
            ScanLoaded,
            NoScan
        }
        private ScanStates scanState;
        public ScanStates ScanState
        {
            get { return scanState; }
            set
            {
                scanState = value;

                if (value == ScanStates.NoScan)
                {
                    btnStartScan.Enabled = (ConnectionState == ConnectionStates.Connected);
                    btnStartScan.Text = "Start Scan";
                    btnLoadScan.Enabled = true;
                    btnClearScan.Enabled = false;
                    btnConnect.Enabled = true;
                    btnSettings.Enabled = true;
                    btnPastResults.Enabled = true;
                    btnExport.Enabled = false;

                    // todo: sample ID
                    txtSampleID1.Enabled = (ConnectionState == ConnectionStates.Connected);
                    txtSampleID2.Enabled = (ConnectionState == ConnectionStates.Connected);
                    radSampleGenerateTimestamp.Enabled = (ConnectionState == ConnectionStates.Connected);
                    radSampleCustomTimestamp.Enabled = (ConnectionState == ConnectionStates.Connected);
                    datSampleCustomTimestamp.Enabled = (ConnectionState == ConnectionStates.Connected && radSampleCustomTimestamp.Checked);

                    btnAnalysis.Enabled = false;
                                        
                    radAutoScanNumbers.Enabled = false;
                    radCustomScanNumbers.Enabled = false;
                    chkAllScans.Enabled = false;
                    cmbWetScanNumber.Enabled = false;
                    cmbDryScanNumber.Enabled = false;

                    cmbSelectedProfile.Enabled = (ConnectionState == ConnectionStates.Disconnected);
                    cmbField1.Enabled = true;
                    cmbField2.Enabled = true;
                    cmbField3.Enabled = true;
                    cmbField4.Enabled = true;
                }
                else if (value == ScanStates.ScanLoaded)
                {
                    btnStartScan.Enabled = (ConnectionState == ConnectionStates.Connected);
                    btnStartScan.Text = "Start Scan";
                    btnLoadScan.Enabled = true;
                    btnClearScan.Enabled = true;
                    btnConnect.Enabled = true;
                    btnSettings.Enabled = true;
                    btnPastResults.Enabled = true;
                    btnExport.Enabled = true;

                    txtSampleID1.Enabled = false;
                    txtSampleID2.Enabled = false;
                    radSampleGenerateTimestamp.Enabled = false;
                    radSampleCustomTimestamp.Enabled = false;
                    datSampleCustomTimestamp.Enabled = false;

                    btnAnalysis.Enabled = true;

                    radAutoScanNumbers.Enabled = true;
                    radCustomScanNumbers.Enabled = true;
                    chkAllScans.Enabled = radCustomScanNumbers.Checked;
                    cmbWetScanNumber.Enabled = radCustomScanNumbers.Checked && !chkAllScans.Checked;
                    cmbDryScanNumber.Enabled = radCustomScanNumbers.Checked && !chkAllScans.Checked;

                    cmbSelectedProfile.Enabled = (ConnectionState == ConnectionStates.Disconnected);
                    cmbField1.Enabled = true;
                    cmbField2.Enabled = true;
                    cmbField3.Enabled = true;
                    cmbField4.Enabled = true;
                }
                else
                {
                    // scanning
                    btnStartScan.Enabled = true;
                    btnStartScan.Text = "Stop Scan";
                    btnLoadScan.Enabled = false;
                    btnClearScan.Enabled = false;
                    btnConnect.Enabled = false;
                    btnSettings.Enabled = false;
                    btnPastResults.Enabled = true;
                    btnExport.Enabled = ScanLoaded;
                    
                    txtSampleID1.Enabled = false;
                    txtSampleID2.Enabled = false;
                    radSampleGenerateTimestamp.Enabled = false;
                    radSampleCustomTimestamp.Enabled = false;
                    datSampleCustomTimestamp.Enabled = false;

                    btnAnalysis.Enabled = false;

                    radAutoScanNumbers.Enabled = false;
                    radCustomScanNumbers.Enabled = false;
                    chkAllScans.Enabled = false;
                    cmbWetScanNumber.Enabled = false;
                    cmbDryScanNumber.Enabled = false;

                    cmbSelectedProfile.Enabled = false;
                    cmbField1.Enabled = false;
                    cmbField2.Enabled = false;
                    cmbField3.Enabled = false;
                    cmbField4.Enabled = false;
                }
            }
        }
        #endregion        

        // manages output to textbox
        private OutputManager output;
        
		// worker to take scans
        private BackgroundWorker worker;		
	    
        public PVS_Form()
        {
            InitializeComponent();

            // output manager
            output = new OutputManager(txtOutput);

            // initialise graph
            CreateGraph();

            // set up reading averaging list
            savedPrev = new List<double[]>();

            // todo: set defaults, ie: checked states
            suppressEvents = true;
            radAutoScanNumbers.Checked = true;
            suppressEvents = false;

            handleNumScanChange(1);

            // set states
            ConnectionState = ConnectionStates.Disconnected;
            ScanState = ScanStates.NoScan;

            // db results
            ResultsDB resultsDb = new ResultsDB(Path.Combine(Directory.GetCurrentDirectory(), PVS.Properties.Resources.DBName),
                                              PVS.Properties.Resources.DBPassword,
                                              PVS.Properties.Resources.DBVersion,
                                              Path.Combine(Directory.GetCurrentDirectory(), "sql"));
            pvsResults = new PVSResults(resultsDb);

            // create auxilliary forms - after setting connection state as optionsDialog needs it
            optionsDialog = new PVSOptionsForm();
            optionsDialog.ProfileControls.OpeningForm = this;
            optionsDialog.Init();

            // cache values
            PVSProfile settings = (PVSProfile)optionsDialog.ProfileControls.CurrentOptions;
            cInvertFirst = settings.InvertFirstGraph;
            cInvertAlternate = settings.InvertAlternateGraphs;

            loadScanDialog = new LoadScanDialog();


            // scan field names stuff
            scanFieldsDialog = new ScanFieldLabelsForm(pvsResults);
            UpdateScanFieldNames();
            scanFieldsDialog.FormClosing += new FormClosingEventHandler(delegate(object sender, FormClosingEventArgs e)
                {
                    UpdateScanFieldNames();
                    scanFieldsDialog.Hide();
                    e.Cancel = true;
                });
                        
            // set saved values
            cmbField1.Text = PVS.Properties.Settings.Default.Field1;
            cmbField2.Text = PVS.Properties.Settings.Default.Field2;
            cmbField3.Text = PVS.Properties.Settings.Default.Field3;
            cmbField4.Text = PVS.Properties.Settings.Default.Field4;
            numWFTInput.Value = Convert.ToDecimal(Properties.Settings.Default.WFT);
            numDFTInput.Value = Convert.ToDecimal(Properties.Settings.Default.DFT);
            numSFInput.Value = Convert.ToDecimal(Properties.Settings.Default.SF);
            chkAutoRepeat.Checked = PVS.Properties.Settings.Default.AutoRepeat;
            saveFile.InitialDirectory = PVS.Properties.Settings.Default.LastSaveLocation;

            // profile list stuff
            UpdateSelectedProfile();


            // for worker
            worker = new BackgroundWorker();
            worker.WorkerReportsProgress = true;
            worker.WorkerSupportsCancellation = true;

            // events
            // worker
            worker.ProgressChanged += new ProgressChangedEventHandler(worker_ProgressChanged);
            worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(worker_RunWorkerCompleted);
            worker.DoWork += new DoWorkEventHandler(worker_DoWork);

            // form closing
            btnClose.Click += new EventHandler(btnClose_Click);
            this.FormClosing += new FormClosingEventHandler(PVS_Form_FormClosing);

            // output
            btnSaveOutput.Click += new EventHandler(btnSaveOutput_Click);
            btnClearOutput.Click += new EventHandler(btnClearOutput_Click);
            
            // main buttons
            btnSettings.Click += new EventHandler(btnSettings_Click);
            btnConnect.Click += new EventHandler(btnConnect_Click);
            btnStartScan.Click += new EventHandler(btnStartScan_Click);
            btnLoadScan.Click += new EventHandler(btnLoadScan_Click);
            btnClearScan.Click += new EventHandler(btnClearScan_Click);
            btnPastResults.Click += new EventHandler(btnPastResults_Click);
            btnExport.Click += new EventHandler(btnExport_Click);

            // sample ID
            radSampleGenerateTimestamp.CheckedChanged += new EventHandler(radSampleTimestamp_CheckedChanged);
            radSampleCustomTimestamp.CheckedChanged += new EventHandler(radSampleTimestamp_CheckedChanged);

            // results
            btnAnalysis.Click += new EventHandler(btnAnalysis_Click);

            // remove unnecessary stuff from graph context menu
            zgcScan.ContextMenuBuilder += new ZedGraphControl.ContextMenuBuilderEventHandler(zgcScan_ContextMenuBuilder);
            
            // scan number stuff
            radAutoScanNumbers.CheckedChanged += new EventHandler(rad_scanNumberChanged);
            radCustomScanNumbers.CheckedChanged += new EventHandler(rad_scanNumberChanged);
            
            chkAllScans.CheckedChanged += new EventHandler(chk_graphDisplayChanged);
            chkAllScans.CheckedChanged += new EventHandler(chkAllScans_CheckedChanged);
            cmbWetScanNumber.SelectedIndexChanged += new EventHandler(cmb_graphDisplayChanged);
            cmbDryScanNumber.SelectedIndexChanged += new EventHandler(cmb_graphDisplayChanged);

            // misc
            cmbSelectedProfile.SelectedIndexChanged += new EventHandler(HandleProfileChange);

            // try connect on start-up
            if (optionsDialog.CurrentOptions.ProfileList.SelectedIndex > -1)
            {
                try
                {
                    bool connected = optionsDialog.ProfileControls.ConnectSensor(true);
                    if (connected)
                    {
                        ConnectionState = ConnectionStates.Connected;
                        optionsDialog.ProfileControls.State = PVSProfileControl.States.Connected;
                    }
                }
                catch { }
            }
        }

        #region Update Functions

        private void BeforeCloseForm()
        {
            // stop any running scans
            if (ScanState == ScanStates.Scanning)
            {
                StopScan();
            }

            // disconnect from any open ports
            if (ConnectionState == ConnectionStates.Connected)
            {
                optionsDialog.ProfileControls.DisconnectSensor();
                ConnectionState = ConnectionStates.Disconnected;
            }

            // todo: check?
            loadScanDialog.Dispose();
            optionsDialog.Dispose();

            // save default values for fields
            PVS.Properties.Settings.Default.Field1 = cmbField1.Text;
            PVS.Properties.Settings.Default.Field2 = cmbField2.Text;
            PVS.Properties.Settings.Default.Field3 = cmbField3.Text;
            PVS.Properties.Settings.Default.Field4 = cmbField4.Text;

            PVS.Properties.Settings.Default.WFT = Convert.ToDouble(numWFTInput.Value);
            PVS.Properties.Settings.Default.DFT = Convert.ToDouble(numDFTInput.Value);
            PVS.Properties.Settings.Default.SF = Convert.ToDouble(numSFInput.Value);

            PVS.Properties.Settings.Default.AutoRepeat = chkAutoRepeat.Checked;
            PVS.Properties.Settings.Default.LastSaveLocation = saveFile.InitialDirectory;
            PVS.Properties.Settings.Default.Save();
        }

        #region Scans

        private void loadSavedResults(int which)
        {
            double[] saved;
            if (which > 0) saved = savedPrev[which];
            else saved = savedPrev[savedPrev.Count + which];

            txtWet2.Text = saved[0].ToString("0.00");
            txtDry2.Text = saved[1].ToString("0.00");
            txtPvs2.Text = saved[2].ToString("0.00");
        }

        private void handleNumScanChange(int numScans)
        {
            if (numScans == 1)
            {
                lblScan3.Text = "Average";
            }
            else
            {
                lblScan3.Text = "Scan 3";
            }
        }

        public void OpenScanFieldsForm()
        {
            scanFieldsDialog.Show();
        }

        private void UpdateScanFieldNames()
        {
            string[] fieldNames = pvsResults.GetScanFieldNames();
            if (fieldNames.Length > 0) lblField1.Text = fieldNames[0];
            if (fieldNames.Length > 1) lblField2.Text = fieldNames[1];
            if (fieldNames.Length > 2) lblField3.Text = fieldNames[2];
            if (fieldNames.Length > 3) lblField4.Text = fieldNames[3];

            // update options in combo boxes
            cmbField1.Items.Clear();
            cmbField1.Items.AddRange(pvsResults.GetFieldDefaults(1));

            cmbField2.Items.Clear();
            cmbField2.Items.AddRange(pvsResults.GetFieldDefaults(2));

            cmbField3.Items.Clear();
            cmbField3.Items.AddRange(pvsResults.GetFieldDefaults(3));

            cmbField4.Items.Clear();
            cmbField4.Items.AddRange(pvsResults.GetFieldDefaults(4));
        }

        private void StartScan()
        {
            // will alert if errors
            if (!optionsDialog.ProfileControls.Valid)
                return;

            if (ConnectionState != ConnectionStates.Connected)
            {
                MessageBox.Show("You need to connect to the sensor before you can start a scan");
                return;
            }
            
            // set measurement frequency of sensor
            PVSProfile settings = (PVSProfile)(optionsDialog.ProfileControls.CurrentOptions);
            if (optionsDialog.ProfileControls.Sensor.Parameters["frequency"].CanSet)
            {
                optionsDialog.ProfileControls.Sensor.Parameters["frequency"].Set(settings.MeasurementFrequency);
            }

            // make sure readings are prepared to be taken
            optionsDialog.ProfileControls.Sensor.EnsureReadingsInitialised();

            // start the async operation
            worker.RunWorkerAsync();

            // change state
            ScanState = ScanStates.Scanning;
        }

        private void StopScan()
        {
            // stop worker
            if (worker.IsBusy)
            {
                // wait until it does
                worker.CancelAsync();
            }

            // set ScanState in worker completed
        }
        
        // utility method to select between the two volume calculation methods
        private double getVolume(PVSData data, PVSProfile.VolumeCalculationMethods method)
        {
            if (method == PVSProfile.VolumeCalculationMethods.Thickness)
                return data.MeanThickness;
            else
                return data.Volume;
        }

        private void LoadScan(ScanFile[][] filenames)
        {
            PVSProfile settings = (PVSProfile)optionsDialog.ProfileControls.CurrentOptions;
                        
            // update caches
            cInvertFirst = settings.InvertFirstGraph;
            cInvertAlternate = settings.InvertAlternateGraphs;

            int count;
            bool horizontallyInverted = cInvertFirst;

            if ((filenames[0].Length != 3 || filenames[1].Length != 3) && (filenames[0].Length != 1 || filenames[1].Length != 1))
            {
                // invalid
                MessageBox.Show("Can only load scans with 1 or 3 scans for wet/dry");
                return;
            }

            // clear stuff
            ClearScan(true);
            // load saved readings unless reached number of readings after which to clear
            if (savedPrev.Count > 0)
            {
                if (savedPrev.Count < settings.NumberReadingsAveraged)
                    loadSavedResults(-1);
                else
                    savedPrev.Clear();
            }

            DataIO reader = new DataIO();
            reader.Mode = DataIO.Modes.Read;

            string filename;
            double[] data_x, data_y;

            count = filenames[0].Length;
            AnalysisWet = new PVSAnalysis[count];

            for (int i = 0; i < count; i++)
            {
                AnalysisWet[i] = new PVSAnalysis(horizontallyInverted);
                AnalysisWet[i].Options = settings.AnalysisOptions;

                filename = filenames[0][i].FindFilename();
                if (filename == "")
                {
                    MessageBox.Show("Error finding data files");
                    return;
                }
                reader.Filename = filenames[0][i].Path + filename;

                reader.ReadData(out data_x, out data_y);
                AnalysisWet[i].SetData(data_x, data_y);
                if (data_x.Length == 0 || data_y.Length == 0)
                {
                    MessageBox.Show("Data file " + filename + " has no data in it");
                    return;
                }

                AnalysisWet[i].Analyse();
                
                if (cInvertAlternate) horizontallyInverted = !horizontallyInverted; // toggle inversion of scan

                // add scan number
                cmbWetScanNumber.Items.Add(filenames[0][i].ScanNumber);
            }
            wetCount = count;

            count = filenames[1].Length;
            AnalysisDry = new PVSAnalysis[count];

            for (int i = 0; i < count; i++)
            {
                AnalysisDry[i] = new PVSAnalysis(horizontallyInverted);
                AnalysisDry[i].Options = settings.AnalysisOptions;

                filename = filenames[1][i].FindFilename();
                if (filename == "")
                {
                    MessageBox.Show("Error finding data files");
                    return;
                }
                reader.Filename = filenames[1][i].Path + filename;

                reader.ReadData(out data_x, out data_y);
                AnalysisDry[i].SetData(data_x, data_y);
                if (data_x.Length == 0 || data_y.Length == 0)
                {
                    MessageBox.Show("Data file " + filename + " has no data in it");
                    return;
                }

                AnalysisDry[i].Analyse();

                if (cInvertAlternate) horizontallyInverted = !horizontallyInverted; // toggle inversion of scan

                // add scan number
                cmbDryScanNumber.Items.Add(filenames[1][i].ScanNumber);
            }
            dryCount = count;
            
            // default scans
            cmbWetScanNumber.SelectedIndex = 0;
            cmbDryScanNumber.SelectedIndex = cmbDryScanNumber.Items.Count - 1;

            double thermalFactor = settings.AnalysisOptions.ThermalFactor;
            // 4 Nov - added calbration factor - multiplicatio factor
            double calibrationFactor = settings.AnalysisOptions.CalibrationFactor;

            double pvsSum = 0.0;
            double[] pvs = new double[count];
            
            
            // get volume method
            PVSProfile.VolumeCalculationMethods volumeMethod = settings.VolumeCalculationMethod;
           
            
            // output information
            if (count >= 1)
            {
                pvs[0] = (getVolume(AnalysisDry[0].Data, volumeMethod) / thermalFactor) / getVolume(AnalysisWet[0].Data, volumeMethod) * 100 * calibrationFactor;
                pvsSum += pvs[0];
                txtWet1.Text = getVolume(AnalysisWet[0].Data, volumeMethod).ToString("0.00");
                txtDry1.Text = (getVolume(AnalysisDry[0].Data, volumeMethod) / thermalFactor).ToString("0.00");
                txtPvs1.Text = pvs[0].ToString("0.00");
            }
            
            // save current data to use next time
            if (count == 1)
            {
                savedPrev.Add(new double[] { getVolume(AnalysisWet[0].Data, volumeMethod), getVolume(AnalysisDry[0].Data, volumeMethod), pvs[0] });
            }
            else savedPrev.Clear();

            if (count == 3)
            {
                pvs[1] = (getVolume(AnalysisDry[1].Data, volumeMethod) / thermalFactor) / getVolume(AnalysisWet[1].Data, volumeMethod) * 100 * calibrationFactor;
                pvsSum += pvs[1];
                txtWet2.Text = getVolume(AnalysisWet[1].Data, volumeMethod).ToString("0.00");
                txtDry2.Text = (getVolume(AnalysisDry[1].Data, volumeMethod) / thermalFactor).ToString("0.00");
                txtPvs2.Text = pvs[1].ToString("0.00");

                pvs[2] = (getVolume(AnalysisDry[2].Data, volumeMethod) / thermalFactor) / getVolume(AnalysisWet[2].Data, volumeMethod) * 100 * calibrationFactor;
                pvsSum += pvs[2];
                txtWet3.Text = getVolume(AnalysisWet[2].Data, volumeMethod).ToString("0.00");
                txtDry3.Text = (getVolume(AnalysisDry[2].Data, volumeMethod) / thermalFactor).ToString("0.00");
                txtPvs3.Text = pvs[2].ToString("0.00");                
            }
            else if (count == 1 && savedPrev.Count > 1)
            {
                // load previous results
                loadSavedResults(-2);

                // averages
                double[] av = new double[3];
                for (int i = 0; i < savedPrev.Count; i++)
                {
                    av[0] += savedPrev[i][0];
                    av[1] += savedPrev[i][1];
                    av[2] += savedPrev[i][2];
                }

                txtWet3.Text = (av[0] / savedPrev.Count).ToString("0.00");
                txtDry3.Text = (av[1] / savedPrev.Count).ToString("0.00");
                txtPvs3.Text = (av[2] / savedPrev.Count).ToString("0.00");
            }

            // change up labels
            handleNumScanChange(count);

            
            if (count > 0) {
                double av = pvsSum / count;
                /*double sd = 0.0;
                for (int i = 0; i < count; i++)
                {
                    sd += (pvs[i] - av) * (pvs[i] - av);
                }
                sd = Math.Sqrt(sd / count);*/
                txtPvs.Text = av.ToString("0.00") + "%";
                txtReading.Text = av.ToString("0.00") + "%";

                double wft = Convert.ToDouble(numWFTInput.Value);
                double dft = Convert.ToDouble(numDFTInput.Value);
                double sf = Convert.ToDouble(numSFInput.Value);

                // avoid dividing by zero
                if (sf == 0.0) sf = 1;

                txtDFTOutput.Text = (wft * av / 100 / sf).ToString("0.00");
                txtWFTOutput.Text = (dft / (av / 100) / sf).ToString("0.00");
                txtPAOutput.Text = (1 * dft / 1000 / (av / 100) * sf).ToString("0.000");

                if (dft == 0) txtWFTOutput.Text = "";
                if (wft == 0) txtDFTOutput.Text = "";

                // add data to database
                PVSResult nResult = new PVSResult();
                nResult.Date = DateTime.Now;
                nResult.PVS = av;
                nResult.ScanPath = filenames[0][0].Path;
                nResult.ScanId = filenames[0][0].ScanId;
                nResult.Field1 = cmbField1.Text;
                nResult.Field2 = cmbField2.Text;
                nResult.Field3 = cmbField3.Text;
                nResult.Field4 = cmbField4.Text;

                pvsResults.AddResultIfNew(nResult);
            }

            
            // automatic axes
            GraphPane pane = zgcScan.GraphPane;
            pane.XAxis.Scale.MaxAuto = true;
            pane.XAxis.Scale.MinAuto = true;
            pane.YAxis.Scale.MaxAuto = true;
            pane.YAxis.Scale.MinAuto = true;
            
            // draw graphs
            DrawGraphs();

            // update state
            ScanState = ScanStates.ScanLoaded;
        }

        private bool LoadScanResults()
        {
            PVSProfile settings = (PVSProfile)optionsDialog.ProfileControls.CurrentOptions;

            // get folder
            ScanManager scanManager = new ScanManager(settings.DataLocation, settings.FilenamePattern);
            int lastScanId = scanManager.GetLastScanId();
            if (lastScanId == -1)
            {
                return false;
            }

            return LoadScanResults(lastScanId);
        }

        private bool LoadScanResults(int scanId)
        {
            PVSProfile settings = (PVSProfile)optionsDialog.ProfileControls.CurrentOptions;

            ScanManager scanManager = new ScanManager(settings.DataLocation, settings.FilenamePattern);

            // work out filenames
            ScanFile[][] filenames = { scanManager.FindScans(ScanTypes.Wet, scanId),
                                         scanManager.FindScans(ScanTypes.Dry, scanId) };

            if (filenames[0].Length == 0 || filenames[1].Length == 0)
            {
                return false;
            }

            // todo: prompt for files
            LoadScan(filenames);

            return true;
        }

        private bool ScanLoaded
        {
            get
            {
                return txtPvs1.Text != "" || zgcScan.GraphPane.CurveList.Count > 0;
            }
        }

        private void ClearScan(bool screenOnly = false)
        {
            // clear graphs
            ClearGraph();

            // reset load scan dialog
            loadScanDialog.CustomScanId = false;
            
            // clear scan numbers
            cmbWetScanNumber.Items.Clear();
            cmbDryScanNumber.Items.Clear();

            // clear results
            AnalysisWet = null;
            AnalysisDry = null;

            // clear data
            txtWet1.Text = "";
            txtWet2.Text = "";
            txtWet3.Text = "";
            txtDry1.Text = "";
            txtDry2.Text = "";
            txtDry3.Text = "";
            txtPvs1.Text = "";
            txtPvs2.Text = "";
            txtPvs3.Text = "";

            if (!screenOnly) savedPrev.Clear();
            
            txtReading.Text = "";
            txtPvs.Text = "";

            txtWFTOutput.Text = txtDFTOutput.Text = txtPAOutput.Text = "";

            // avoid killing a running scan
            if (ScanState == ScanStates.ScanLoaded) ScanState = ScanStates.NoScan;
        }

        private void ExportResult()
        {
            PVSProfile settings = (PVSProfile)optionsDialog.ProfileControls.CurrentOptions;

            // get folder
            ScanManager scanManager = new ScanManager(settings.DataLocation, settings.FilenamePattern);
            int lastScanId = scanManager.GetLastScanId();
            if (lastScanId == -1)
            {
                MessageBox.Show("Could not find last scan ID to export result");
                return;
            }

            string pvsLocation = Path.Combine(settings.DataLocation, "results");
            if (!Directory.Exists(pvsLocation)) 
            {
                try {
                    Directory.CreateDirectory(pvsLocation);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Could not create directory 'results' in " + settings.DataLocation);
                    logger.ErrorException("Could not create directory 'results' in " + settings.DataLocation, ex);
                    return;
                }
            }
            
            string filename = String.Format("pvs {0}.csv", lastScanId);
            int ind = 1;
            while (File.Exists(Path.Combine(pvsLocation, filename))) 
                filename = String.Format("pvs {0}-{1}.csv", lastScanId, ind++);

            try
            {
                StreamWriter sw = new StreamWriter(Path.Combine(pvsLocation, filename), false);
                sw.WriteLine(String.Format("{0},{1},{2},{3},{4},{5},{6},{7}", "Date",                  "PVS",        "Wet",        "Dry",        lblField1.Text, lblField2.Text, lblField3.Text, lblField4.Text));
                if (txtPvs3.Text == "")
                    sw.WriteLine(String.Format("{0},{1},{2},{3},{4},{5},{6},{7}", DateTime.Now.ToString(), txtPvs1.Text, txtWet1.Text, txtDry1.Text, cmbField1.Text, cmbField2.Text, cmbField3.Text, cmbField4.Text));
                else
                    sw.WriteLine(String.Format("{0},{1},{2},{3},{4},{5},{6},{7}", DateTime.Now.ToString(), txtPvs3.Text, txtWet3.Text, txtDry3.Text, cmbField1.Text, cmbField2.Text, cmbField3.Text, cmbField4.Text));
                sw.Flush();
                sw.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Could not save results to " + Path.Combine(pvsLocation, filename));
                logger.ErrorException("Could not save results to " + Path.Combine(pvsLocation, filename), ex);
            }
        }

        #endregion

        #region Graph Stuff

        // todo - set states of graph updates labels, axes, etc

        // called on form load for initial display of graph
        private void CreateGraph()
        {
            GraphPane pane = zgcScan.GraphPane;

            // set axis labels
            pane.Title.Text = "Scan Profiles";
            pane.XAxis.Title.Text = "Time";
            pane.YAxis.Title.Text = "Distance";
        }
               
        /// <summary>
        /// Clears any curves on the graph
        /// </summary>
        private void ClearGraph(bool skipRefresh = false)
        {
            GraphPane pane = zgcScan.GraphPane;
            pane.CurveList.Clear();

            // redraw
            if (!skipRefresh)
            {
                zgcScan.AxisChange();
                zgcScan.Invalidate();
            }
        }

        public void DrawGraphs()
        {
            PVSProfile settings = (PVSProfile)optionsDialog.ProfileControls.CurrentOptions;

            // no data set
            if (AnalysisWet == null || AnalysisDry == null) return;

            // draw graphs
            ClearGraph();

            // update if inverting has changed, will only re-analyse if it has changed
            if (cInvertFirst != settings.InvertFirstGraph)
            {
                for (int i = 0; i < wetCount; i++) AnalysisWet[i].HorizontallyInverted = !AnalysisWet[i].HorizontallyInverted;
                for (int i = 0; i < dryCount; i++) AnalysisDry[i].HorizontallyInverted = !AnalysisDry[i].HorizontallyInverted;
            }
            if (cInvertAlternate != settings.InvertAlternateGraphs)
            {
                int i = 0;
                while (i < wetCount + dryCount)
                {
                    // invert every odd numbered one
                    if (i % 2 == 1)
                    {
                        if (i < wetCount) AnalysisWet[i].HorizontallyInverted = !AnalysisWet[i].HorizontallyInverted;
                        else AnalysisDry[i - wetCount].HorizontallyInverted = !AnalysisDry[i - wetCount].HorizontallyInverted;
                    }
                    i++;
                }
            }

            // update caches
            cInvertFirst = settings.InvertFirstGraph;
            cInvertAlternate = settings.InvertAlternateGraphs;


            bool showWet = (settings.GraphDataType == PVSProfile.GraphDataTypeOptions.Both) || (settings.GraphDataType == PVSProfile.GraphDataTypeOptions.Wet);
            bool showDry = (settings.GraphDataType == PVSProfile.GraphDataTypeOptions.Both) || (settings.GraphDataType == PVSProfile.GraphDataTypeOptions.Dry);
                        
            // handle show all
            int[] wetIs, dryIs;
            if (radAutoScanNumbers.Checked || (!chkAllScans.Checked && radCustomScanNumbers.Checked))
            {
                if (radAutoScanNumbers.Checked)
                {
                    wetIs = new int[] { AnalysisWet.Length - 1 };
                    dryIs = new int[] { 0 };
                }
                else
                {
                    // custom scan numbers
                    wetIs = new int[] { cmbWetScanNumber.SelectedIndex };
                    dryIs = new int[] { cmbDryScanNumber.SelectedIndex };
                }
            }
            else
            {
                wetIs = new int[cmbWetScanNumber.Items.Count];
                for (int i = 0; i < cmbWetScanNumber.Items.Count; i++) { wetIs[i] = i; }

                dryIs = new int[cmbDryScanNumber.Items.Count];
                for (int i = 0; i < cmbDryScanNumber.Items.Count; i++) { dryIs[i] = i; }
            }


            Color[] colors = new Color[] { Color.Blue, Color.Red, Color.Green, Color.Indigo, Color.Maroon, Color.DeepSkyBlue, Color.DarkOrange };
            int numColors = colors.Length;
            GraphPane pane = zgcScan.GraphPane;
            int c = 0;

            double[] wet_x; PVSData wetA;
            if (showWet)
            {
                for (int i = 0; i < wetIs.Length; i++)
                {
                    int wetI = wetIs[i];
                    string wetScanString = " Scan " + cmbWetScanNumber.Items[wetI].ToString();

                    wetA = AnalysisWet[wetI].Data;
                    wet_x = wetA.Centred_X;

                    if (settings.SmoothGraphs)
                    {
                        if (settings.GraphDataSource == PVSProfile.GraphDataSourceOptions.Profile)
                        {
                            pane.AddCurve("Smoothed Wet Profile" + wetScanString, wet_x, wetA.Norm_Smoothed_Y, colors[(c++) % numColors], SymbolType.None);
                            pane.Title.Text = "Smoothed Scan Profiles";
                        }
                        else
                        {
                            pane.AddCurve("Smoothed Wet Data" + wetScanString, wet_x, wetA.Smoothed_Y, colors[(c++) % numColors], SymbolType.None);
                            pane.Title.Text = "Smoothed Scan Data";
                        }
                    }
                    else
                    {
                        if (settings.GraphDataSource == PVSProfile.GraphDataSourceOptions.Profile)
                        {
                            pane.AddCurve("Wet Profile" + wetScanString, wet_x, wetA.Norm_Data_Y, colors[(c++) % numColors], SymbolType.None);
                            pane.Title.Text = "Scan Profiles";
                        }
                        else
                        {
                            pane.AddCurve("Wet Data" + wetScanString, wet_x, wetA.Data_Y, colors[(c++) % numColors], SymbolType.None);
                            pane.Title.Text = "Scan Data";
                        }
                    }
                }
            }

            double[] dry_x; PVSData dryA;
            if (showDry)
            {
                for (int i = 0; i < dryIs.Length; i++)
                {
                    int dryI = dryIs[i];
                    string dryScanString = " Scan " + cmbDryScanNumber.Items[dryI].ToString();

                    dryA = AnalysisDry[dryI].Data;
                    dry_x = dryA.Centred_X;

                    if (settings.SmoothGraphs)
                    {
                        if (settings.GraphDataSource == PVSProfile.GraphDataSourceOptions.Profile)
                        {
                            pane.AddCurve("Smoothed Dry Profile" + dryScanString, dry_x, dryA.Norm_Smoothed_Y, colors[(c++) % numColors], SymbolType.None);
                        }
                        else
                        {
                            pane.AddCurve("Smoothed Dry Data" + dryScanString, dry_x, dryA.Smoothed_Y, colors[(c++) % numColors], SymbolType.None);
                        }
                    }
                    else
                    {
                        if (settings.GraphDataSource == PVSProfile.GraphDataSourceOptions.Profile)
                        {
                            pane.AddCurve("Dry Profile" + dryScanString, dry_x, dryA.Norm_Data_Y, colors[(c++) % numColors], SymbolType.None);
                        }
                        else
                        {
                            pane.AddCurve("Dry Data" + dryScanString, dry_x, dryA.Data_Y, colors[(c++) % numColors], SymbolType.None);
                        }
                    }
                }
            }
            
            zgcScan.AxisChange();
            zgcScan.Invalidate();
        }

        #endregion

        #region Selected Profile Stuff

        private void UpdateSelectedProfile()
        {
            suppressProfileChange = true;

            bool doEnable = (ScanState == ScanStates.NoScan) && (ConnectionState == ConnectionStates.Disconnected);
            bool isNew = optionsDialog.OptionsControls.ProfileManager.ControlState == ProfileManagerControl<PVSProfile, PVSProfileControl>.ControlStates.NewProfile;
            bool isCustom = optionsDialog.OptionsControls.ProfileManager.ControlState == ProfileManagerControl<PVSProfile, PVSProfileControl>.ControlStates.CustomProfile;


            cmbSelectedProfile.Items.Clear();
            if (isNew)
            {
                cmbSelectedProfile.Items.Add("no profiles");
                cmbSelectedProfile.SelectedIndex = 0;
                doEnable = false;
            }
            else
            {
                if (isCustom)
                {
                    cmbSelectedProfile.Items.Add("custom profile");
                    cmbSelectedProfile.SelectedIndex = 0;
                }

                for (int i = 0; i < optionsDialog.CurrentOptions.ProfileList.Count; i++)
                {
                    cmbSelectedProfile.Items.Add(optionsDialog.CurrentOptions.ProfileList[i].ProfileName);
                }

                if (!isCustom)
                {
                    cmbSelectedProfile.SelectedIndex = optionsDialog.CurrentOptions.ProfileList.SelectedIndex;
                }
            }

            cmbSelectedProfile.Enabled = doEnable;
            suppressProfileChange = false;
        }

        private bool suppressProfileChange = false;
        private void HandleProfileChange()
        {
            if (suppressProfileChange) return;
            if (cmbSelectedProfile.Items.Count == 0) return;

            suppressProfileChange = true;

            if (cmbSelectedProfile.SelectedItem.ToString() == "no profiles")
            {
                // do nothing
            }
            else if (cmbSelectedProfile.SelectedItem.ToString() == "custom profile")
            {
                // do nothing
            }
            else if (cmbSelectedProfile.Items[0].ToString() == "custom profile")
            {
                int selectedIndex = cmbSelectedProfile.SelectedIndex - 1;
                
                // update options dialog
                // confirms
                optionsDialog.OptionsControls.ProfileManager.SetProfile(selectedIndex);

                // if confirm failed:
                if (optionsDialog.OptionsControls.ProfileManager.ControlState == ProfileManagerControl<PVSProfile, PVSProfileControl>.ControlStates.CustomProfile)
                {
                    cmbSelectedProfile.SelectedIndex = 0;
                }
                else
                {
                    cmbSelectedProfile.Items.RemoveAt(0);
                    cmbSelectedProfile.SelectedIndex = selectedIndex;
                }
            }
            else
            {
                optionsDialog.OptionsControls.ProfileManager.SetProfile(cmbSelectedProfile.SelectedIndex);
            }

            suppressProfileChange = false;
        }

        private void HandleProfileChange(object sender, EventArgs e)
        {
            HandleProfileChange();
        }

        #endregion

        #endregion

        #region Events

        #region Misc

        void zgcScan_ContextMenuBuilder(ZedGraphControl sender, ContextMenuStrip menuStrip, Point mousePt, ZedGraphControl.ContextMenuObjectState objState)
        {
            string[] tagsToRemove = new string[] { "set_default", "unzoom" };
            foreach (string tagToRemove in tagsToRemove)
            {
                foreach (ToolStripMenuItem item in menuStrip.Items)
                {
                    string tag = item.Tag.ToString();
                    if (tag == tagToRemove)
                    {
                        menuStrip.Items.Remove(item);
                        break;
                    }
                }
            }
        }

        void btnAnalysis_Click(object sender, EventArgs e)
        {
            // todo
            MessageBox.Show("Disabled for now");
        }

        void radSampleTimestamp_CheckedChanged(object sender, EventArgs e)
        {
            datSampleCustomTimestamp.Enabled = radSampleCustomTimestamp.Checked;
        }

        
        void rad_scanNumberChanged(object sender, EventArgs e)
        {
            if (suppressEvents) return;
            
            // to avoid it being called multiple times
            if (((RadioButton)sender).Checked)
            {
                chkAllScans.Enabled = radCustomScanNumbers.Checked;
                cmbWetScanNumber.Enabled = radCustomScanNumbers.Checked;
                cmbDryScanNumber.Enabled = radCustomScanNumbers.Checked;

                DrawGraphs();
            }
        }

        void chk_graphDisplayChanged(object sender, EventArgs e)
        {
            if (suppressEvents) return;

            DrawGraphs();
        }

        void cmb_graphDisplayChanged(object sender, EventArgs e)
        {
            if (suppressEvents) return;

            if (ScanState == ScanStates.ScanLoaded)
            {
                DrawGraphs();
            }
        }

        void chkAllScans_CheckedChanged(object sender, EventArgs e)
        {
            cmbWetScanNumber.Enabled = cmbDryScanNumber.Enabled = !chkAllScans.Checked;
        }

        #endregion

        #region Control Buttons

        void btnLoadScan_Click(object sender, EventArgs e)
        {
            PVSProfile settings = (PVSProfile)optionsDialog.ProfileControls.CurrentOptions;

            loadScanDialog.Path = settings.DataLocation;
            loadScanDialog.FilenamePattern = settings.FilenamePattern;
            
            DialogResult result = loadScanDialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                // sync details
                settings.DataLocation = loadScanDialog.Path;
                settings.FilenamePattern = loadScanDialog.FilenamePattern;
                
                // for saving
                int index = optionsDialog.OptionsControls.ProfileManager.ProfileList.SelectedIndex;
                if (index > -1)
                {
                    optionsDialog.OptionsControls.ProfileManager.ProfileList[index].DataLocation = loadScanDialog.Path;
                    optionsDialog.OptionsControls.ProfileManager.ProfileList[index].FilenamePattern = loadScanDialog.FilenamePattern;
                }

                // to update controls
                optionsDialog.ProfileControls.CurrentOptions = settings;
                optionsDialog.SaveOptions();

                if (loadScanDialog.CustomScanId)
                {
                    int scanId = loadScanDialog.ScanId;
                    if (scanId == -1)
                    {
                        MessageBox.Show("No scan result was selected");
                    }

                    if (!LoadScanResults(scanId))
                    {
                        MessageBox.Show("Could not load selected scan");
                    }
                }
                else
                {
                    if (!LoadScanResults())
                    {
                        MessageBox.Show("No scan results could be found");
                    }
                }
            }
        }

        void btnClearScan_Click(object sender, EventArgs e)
        {
            ClearScan();
        }

        void btnStartScan_Click(object sender, EventArgs e)
        {
            if (ScanState == ScanStates.Scanning)
            {
                // stop scan
                StopScan();
            }
            else
            {
                // start scan
                StartScan();
            }
        }

        void btnConnect_Click(object sender, EventArgs e)
        {
            if (!optionsDialog.ProfileControls.Valid)
            {
                MessageBox.Show("There are errors in your settings. Please correct them first");
                return;
            }

            if (ConnectionState == ConnectionStates.Disconnected)
            {
                btnConnect.Enabled = false;

                bool connected = optionsDialog.ProfileControls.ConnectSensor();
                if (connected)
                {
                    // change form state
                    ConnectionState = ConnectionStates.Connected;
                    optionsDialog.ProfileControls.State = PVSProfileControl.States.Connected;
                }

                btnConnect.Enabled = true;
            }
            else
            {
                btnConnect.Enabled = false;

                bool disconnected = optionsDialog.ProfileControls.DisconnectSensor();
                if (disconnected)
                {
                    // change form state
                    ConnectionState = ConnectionStates.Disconnected;
                    optionsDialog.ProfileControls.State = PVSProfileControl.States.Disconnected;
                }

                btnConnect.Enabled = true;
            }
        }

        void btnSettings_Click(object sender, EventArgs e)
        {
            // show as dialog, can ignore DialogResult
            optionsDialog.ShowDialog();
            
            // update connection state
            if (optionsDialog.ProfileControls.State == PVSProfileControl.States.Connected)
            {
                ConnectionState = ConnectionStates.Connected;
            }
            else
            {
                ConnectionState = ConnectionStates.Disconnected;
            }

            // update profile list
            UpdateSelectedProfile();

            // update scan labels if currently empty
            if (txtWet1.Text == "")
            {
                PVSProfile settings = (PVSProfile)optionsDialog.ProfileControls.CurrentOptions;
                handleNumScanChange(settings.NumberScans);
            }
        }


        void btnPastResults_Click(object sender, EventArgs e)
        {
            PVSResultsDisplayForm frm = new PVSResultsDisplayForm(pvsResults);
            frm.Show();
        }

        void btnExport_Click(object sender, EventArgs e)
        {
            ExportResult();
        }        

        #endregion

        #region Output Panel

        void btnSaveOutput_Click(object sender, EventArgs e)
        {
            saveFile.ShowDialog();

            if (saveFile.FileName != "")
            {
                // save file
                try
                {
                    // TODO: not writing correctly

                    // write output, overwriting any files
                    StreamWriter sw = new StreamWriter(saveFile.FileName, false);
                    sw.Write(output.GetText());
                    sw.Flush();
                    sw.Close();

                    output.WriteLine("Output successfully saved to " + saveFile.FileName);
                }
                catch
                {
                    output.WriteLine("Error saving output", OutputManager.Level.INFO);
                }

            }
        }

        void btnClearOutput_Click(object sender, EventArgs e)
        {
            output.Clear();
        }

        #endregion

        #region Form Closing

        void PVS_Form_FormClosing(object sender, FormClosingEventArgs e)
        {
            BeforeCloseForm();
        }

        void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        
        #endregion

        #region Worker

        void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            // indicates an exception was raised
            if (e.Error != null)
            {
                MessageBox.Show("Error taking scan: " + e.Error.Message);
                if (ScanLoaded) ScanState = ScanStates.ScanLoaded;
                else ScanState = ScanStates.NoScan;
                return;
            }

            if (e.Cancelled)
            {
                if (ScanLoaded) ScanState = ScanStates.ScanLoaded;
                else ScanState = ScanStates.NoScan;
                return;
            }

            if (!LoadScanResults())
            {
                MessageBox.Show("There was an error loading the scan results");
                return;
            }

            // auto repeat
            if (chkAutoRepeat.Checked) StartScan();
        }

        void worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            string message = e.UserState.ToString();
            txtReading.Text = message;
            output.WriteLine(message);
        }

        void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = sender as BackgroundWorker;

            worker.ReportProgress(-1, "Starting scans");

            PVSProfile settings = (PVSProfile)optionsDialog.ProfileControls.CurrentOptions;
            
            ScanManager scanManager = new ScanManager(settings.DataLocation, settings.FilenamePattern);
            int nextScanId = scanManager.GetNextScanId();
            string folder = scanManager.Path; // makes consistent trailing slash by doing it from here
            
            // number of scans
            int numberScans = settings.NumberScans;            

            // work out filenames
            ScanFile[][] filenames = { scanManager.GetScans(ScanTypes.Wet, nextScanId, numberScans),
                                       scanManager.GetScans(ScanTypes.Dry, nextScanId, numberScans) };
            
            // file I/O
            DataIO dataIO = new DataIO();
            dataIO.Mode = DataIO.Modes.Write;
            
                                    
            // time between reading blocks to look for sample in ms
            int blockTime = 200;
            // time between each file, to prevent earlier scan triggering next one
            int pauseTime = 1000;
            SensorReading[] buffer;
            List<SensorReading> readings;

            // shortcut to sensor object
            ISensor sensor = optionsDialog.ProfileControls.Sensor;

            double frequency = Convert.ToDouble(settings.MeasurementFrequency);

            // number of sequential readings error/non-error before can determine an edge
            // will require a maximum of 0.5 seconds of errors/non-errors to find edge
            int threshold = Math.Min(200, (int)frequency / 2);

            // buffer to store readings which make up the edges, so they can be included
            SensorReading[] edgeBuffer = new SensorReading[threshold];

            // files mock sensor needs to terminate at end of each file rather than when get errors
            bool debug = settings.SensorModel == "Mock Sensor Files";

            // set up graph stuff
            GraphPane pane = zgcScan.GraphPane;

            // add wet & dry curves
            PointPairList[] wet_lists = new PointPairList[numberScans];
            PointPairList[] dry_lists = new PointPairList[numberScans];
            LineItem[] wet_curves = new LineItem[numberScans];
            LineItem[] dry_curves = new LineItem[numberScans];
            Color[] colors = new Color[] { Color.Blue, Color.Red, Color.Green, Color.Indigo, Color.Maroon, Color.DeepSkyBlue, Color.DarkOrange };
            int numColors = colors.Length;

            for (int i = 0; i < numberScans; i++)
            {
                wet_lists[i] = new PointPairList();
                dry_lists[i] = new PointPairList();
            }            

            PointPairList currentList;
            bool graphSetUp = false;

            for (int i = 0; i < filenames.Length; i++)
            {
                for (int n = 0; n < (numberScans % 2 == 1 ? numberScans + 1 : numberScans); n++)
                {
                    // the current list to which to add readings
                    if (n >= numberScans) currentList = new PointPairList();
                    else if (i == 0) currentList = wet_lists[n];
                    else currentList = dry_lists[n];
                                        
                    // stores readings that are valid
                    readings = new List<SensorReading>();

                    if (n < numberScans)
                    {
                        worker.ReportProgress(-1, "Waiting for scan" + " #" + (n + 1).ToString());
                    }
                    else
                    {
                        // reset scan heading
                        worker.ReportProgress(-1, "");
                    }

                    // reset times, both in seconds
                    sensor.CurrentTime = 0;
                    sensor.TimeIncrement = 1 / frequency;

                    // clear buffers
                    sensor.PortBuffer.Clear();

                    // automatically buffer data temporarily to prevent filling serial ports buffer
                    sensor.PortBuffer.AutoUpdate(50);

                    // find start and end of sample
                    int count; // count of successive errors/non-errors
                    int readingCount = 0; // count of readings so far
                    for (int j = 0; j < 2; j++)
                    {
                        // restart counter
                        count = 0;

                        while (true)
                        {
                            // cancel if necessary
                            if (worker.CancellationPending)
                            {
                                sensor.PortBuffer.StopAutoUpdate();
                                e.Cancel = true;
                                return;
                            }

                            buffer = sensor.TakeReadings();

                            // errors indicate out of range
                            foreach (SensorReading r in buffer)
                            {
                                if (j == 0)
                                {
                                    // if finding start
                                    if (count >= threshold)
                                    {
                                        // update graph display to new set of graphs
                                        if (!graphSetUp)
                                        {
                                            // clear old curves
                                            ClearGraph(true);
                                            for (int x = 0; x < numberScans; x++)
                                            {
                                                wet_curves[x] = pane.AddCurve("Wet Data Scan " + (x + 1).ToString(), wet_lists[x], colors[x % numColors], SymbolType.None);
                                                wet_curves[x].Label.IsVisible = true;
                                                dry_curves[x] = pane.AddCurve("Dry Data Scan " + (x + 1).ToString(), dry_lists[x], colors[(numberScans + x) % numColors], SymbolType.None);
                                                dry_curves[x].Label.IsVisible = true;
                                            }

                                            // use appropriate vertical scale from sensor details
                                            // as ReadingReference set to SMR in SettingsDialog.Connect, vertical range is 0 to Sensor.Properties.MR
                                            // 5% grace
                                            double grace = optionsDialog.ProfileControls.Sensor.Properties.MR * 0.05;
                                            pane.YAxis.Scale.Min = Math.Round(-grace);
                                            pane.YAxis.Scale.Max = Math.Round(optionsDialog.ProfileControls.Sensor.Properties.MR + grace);

                                            // start horizontal scale at 0, showing 1000 points
                                            double horizontalSize = 10; // represents 1000 points as x = index * 0.01
                                            pane.XAxis.Scale.Min = 0;
                                            pane.XAxis.Scale.Max = horizontalSize;
            
                                            // redraw graph
                                            zgcScan.AxisChange();
                                            zgcScan.Invalidate();

                                            graphSetUp = true;
                                        }

                                        // found start
                                        if (count == threshold)
                                        {
                                            // copy edge buffer into readings and graph them
                                            foreach (SensorReading edgeR in edgeBuffer)
                                            {
                                                readings.Add(edgeR);
                                                currentList.Add((readingCount++ * 0.01), edgeR.Value);
                                            }

                                            // prevent this if happening again
                                            count++;
                                        }

                                        readings.Add(r);
                                        currentList.Add((readingCount++ * 0.01), r.Value);
                                    }
                                    if (count < threshold && !r.IsError)
                                    {
                                        edgeBuffer[count] = r;
                                        count++;
                                    }
                                    if (count < threshold && r.IsError)
                                    {
                                        // error resets search for start
                                        count = 0;
                                    }
                                }
                                else
                                {
                                    // finding end
                                    if (count >= threshold)
                                    {
                                        // found end
                                        break;
                                    }
                                    if (r.IsError)
                                    {
                                        count++;
                                    }
                                    if (!r.IsError)
                                    {
                                        readings.Add(r);
                                        currentList.Add((readingCount++ * 0.01), r.Value);
                                        count = 0;
                                    }
                                }
                            } // end processing buffer

                            // test x-axis is big enough
                            if (readingCount * 0.01 + 2 > pane.XAxis.Scale.Max)
                            {
                                pane.XAxis.Scale.Max = readingCount * 0.01 + 2;
                            }

                            // redraw graph
                            zgcScan.AxisChange();
                            zgcScan.Invalidate();

                            // check if reached end/start of scan
                            if (count >= threshold)
                                break;

                            // pause between each buffering block
                            Thread.Sleep(blockTime);

                        } // end look for start/end

                        if (n < numberScans)
                        {
                            if (j == 0) worker.ReportProgress(-1, "Found scan" + " #" + (n + 1).ToString());
                            else worker.ReportProgress(-1, "Scan" + " #" + (n + 1).ToString() + " finished");
                        }

                    } // end reading data loop

                    // stop buffering
                    sensor.PortBuffer.StopAutoUpdate();

                    // if not "ignore scan" scan, then skip it
                    if (n < numberScans)
                    {
                        // get and process readings
                        SensorReading[] results_all = readings.ToArray();
                        SensorReading[] results;

                        // discard min of 5% and 0.2 seconds worth of data from each end
                        int discardReadings = -1;
                        int tempDiscard;

                        // allow for time discarding
                        if (settings.TimeDiscarded)
                        {
                            tempDiscard = (int)((double)settings.DiscardTime * frequency);
                            // discard minimum
                            if (discardReadings == -1 || discardReadings > tempDiscard)
                            {
                                discardReadings = tempDiscard;
                            }
                        }

                        // allow for % of total discarding
                        if (settings.TotalDiscarded)
                        {
                            tempDiscard = (int)((double)settings.DiscardTotal / 100 * results_all.Length);
                            // discard minimum
                            if (discardReadings == -1 || discardReadings > tempDiscard)
                            {
                                discardReadings = tempDiscard;
                            }
                        }

                        int new_length = results_all.Length - 2 * discardReadings;
                        int start = discardReadings;

                        if (new_length <= 0)
                        {
                            discardReadings = (int)(0.05 * results_all.Length);
                            new_length = results_all.Length - 2 * discardReadings;
                            start = discardReadings;
                        }

                        // discard some at start and end
                        results = new SensorReading[new_length];
                        Array.Copy(results_all, start, results, 0, new_length);


                        // filter peaks
                        if (settings.FilterPeaks)
                        {
                            double[] result_temp = new double[new_length];
                            for (int k = 0; k < new_length; k++)
                            {
                                result_temp[k] = results[k].Value;
                            }
                            double[] result_temp2;
                            NoiseFilter.Filter(result_temp, out result_temp2, (double)settings.PeakThreshold / 1000, (double)settings.PeakThreshold / 1000 / 2, settings.AnalysisOptions.SmoothingWidth, -1); // todo: fix magic numbers

                            for (int k = 0; k < new_length; k++)
                            {
                                results[k].Value = result_temp2[k];
                            }
                        }


                        // apply downscaling
                        if (settings.DownscaleMethod == PVSProfile.DownscaleMethods.Averaging)
                        {
                            results = DataAnalysis.BlockAverageData(results, settings.DownscaleFactor);
                        }
                        else if (settings.DownscaleMethod == PVSProfile.DownscaleMethods.Filtering)
                        {
                            results = DataAnalysis.FilterData(results, settings.DownscaleFactor);
                        }

                        // check cancellation
                        if (worker.CancellationPending)
                        {
                            e.Cancel = true;
                            return;
                        }


                        // create x-values and y_values to write to file, after adjusted for frequency
                        int rLen = results.Length;
                        double[] data_x = new double[rLen];
                        double[] data_y = new double[rLen];
                        for (int j = 0; j < rLen; j++)
                        {
                            data_x[j] = results[j].Time;
                            data_y[j] = results[j].Value;
                        }

                        // write data
                        dataIO.Filename = folder + filenames[i][n].Filename;
                        dataIO.WriteData(data_x, data_y);
                    }

                    // wait after taking reading to prevent triggering twice from same signal
                    Thread.Sleep(pauseTime);
                }
            } // end entire file loop            
        }

        #endregion

        private void PVS_Form_Load(object sender, EventArgs e)
        {

        }

        #endregion
    }
}
