﻿using System;
using System.Collections.Generic;
using System.Text;

using System.Text.RegularExpressions;
using System.IO;

namespace PVS.Scans
{
    class ScanFile : IComparable<ScanFile>
    {
        #region Basic Properties
        // for caching
        bool changed = true;

        // folder that file is stored in, trailing slash
        private string path;
        public string Path
        {
            get { return path; }
            set
            {
                path = value.TrimEnd('/', '\\') + "\\";
                changed = true;
            }
        }

        // template for file
        private string template;
        public string Template
        {
            get { return template; }
            set { template = value; changed = true; }
        }

        // id of file
        private int scanId;
        public int ScanId
        {
            get { return scanId; }
            set { scanId = value; changed = true; }
        }

        // which scan number it is ie: wet-1 or wet-2
        private int scanNumber;
        public int ScanNumber
        {
            get { return scanNumber; }
            set { scanNumber = value; changed = true; }
        }

        // whether it is wet or dry
        private ScanTypes scanType;
        public ScanTypes ScanType
        {
            get { return scanType; }
            set { scanType = value; changed = true; }
        }

        #endregion

        Regex rScanId; // for scan id
        Regex rScanNumber; // for scan number
        Regex rScanType; // for scan type
        Regex rFileTest; // for testing files
        
        public ScanFile(string path, string template)
        {
            this.Path = path;
            this.Template = template;

            rScanId = new Regex(@"\{n+\}", RegexOptions.IgnoreCase); // for scan id
            rScanNumber = new Regex(@"\{x+\}", RegexOptions.IgnoreCase); // for scan number
            rScanType = new Regex(@"\{type\}", RegexOptions.IgnoreCase); // for scan type
            rFileTest = new Regex("^" + rScanType.Replace(rScanNumber.Replace(rScanId.Replace(Template, @"(?<id>\d+)"), @"(?<number>\d+)"), @"(?<type>[a-z]+)") + "$", RegexOptions.IgnoreCase);
        }

        public ScanFile(string path, string template, ScanTypes scanType, int scanId, int scanNumber)
            : this(path, template)
        {
            this.ScanType = scanType;
            this.ScanId = scanId;
            this.ScanNumber = scanNumber;
        }

        public ScanFile(string path, string template, string templateFilename)
            : this(path, template)
        {
            this.TemplateFilename = templateFilename;
        }


        // the externally set filename or ""
        private string actualFilename = "";
        /// <summary>
        /// Returns either an externally set filename applied via TemplateFilename, or TemplateFilename
        /// </summary>
        public string Filename
        {
            get 
            { 
                return (actualFilename != "" ? actualFilename : TemplateFilename);
            }
        }
        
        // cached version of applied template
        private string cachedName;
        /// <summary>
        /// Returns the template with scan parameters applied, no checks for if file exists in Path.
        /// When setting, parses filename and updates actualFilename, ScanID, ScanNumber and ScanType, will throw an exception if invalid
        /// </summary>
        public string TemplateFilename
        {
            get
            {
                if (!changed) return cachedName;
                
                string output = ScanFile.ApplyScanType(Template, ScanType);
                output = ScanFile.ApplyScanId(output, ScanId);
                output = ScanFile.ApplyScanNumber(output, ScanNumber);

                cachedName = output;
                changed = false;

                return output;
            }
            set
            {
                // automatically parse and fill parameters                
                Match m = rFileTest.Match(value);
                
                string type; int id; int number;
                if (m.Success)
                {
                    type = m.Groups["type"].Value.ToLower();
                    id = Convert.ToInt32(m.Groups["id"].Value);
                    Group a = m.Groups["number"];
                    bool b = a.Success;
                    number = (m.Groups["number"].Success ? Convert.ToInt32(m.Groups["number"].Value) : -1);

                    ScanId = id;
                    ScanNumber = number;
                    if (type == "wet") ScanType = ScanTypes.Wet;
                    else ScanType = ScanTypes.Dry;

                    actualFilename = value;
                }
                else
                {
                    throw new ApplicationException("Passed filename does not match template for TemplateFilename setter");
                }
            }
        }
        
        /// <summary>
        /// Finds a file in Path that matches, or returns ""
        /// Tries (1) the cached name (2) the template (3) searching the Path for a file that matches (padding issues)
        /// </summary>
        /// <returns></returns>
        public string FindFilename()
        {
            string filename = Filename;
            if (filename != "")
            {
                // check externally applied filename for if it exists
                if (File.Exists(Path + filename))
                {
                    return filename;
                }

                // check template filename for if it exists - necessary even though Filename will return TemplateFilename if actualFilename = "", as actualFilename might be an invalid file
                // not inefficient as TemplateFilename will cache result
                filename = TemplateFilename;
                if (File.Exists(Path + filename))
                {
                    return filename;
                }
            }

            // both Filename and TemplateFilename are invalid files
            // try searching, as sometimes TemplateFilename will have padding/case issues

            string fileFilter = rScanType.Replace(rScanNumber.Replace(rScanId.Replace(Template, "*"), "*"), "*");
         
            // returns entire paths, not just filenames
            string[] matchedFiles = Directory.GetFiles(Path, fileFilter);
            
            int pathLength = Path.Length;
            Match m;
            string type; int id; int number;
            string file;
            foreach (string filePath in matchedFiles)
            {
                // need to ignore path
                file = filePath.Substring(pathLength);
                m = rFileTest.Match(file);
                if (m.Success)
                {                    
                    type = m.Groups["type"].Value.ToLower();
                    id = Convert.ToInt32(m.Groups["id"].Value);
                    number = (m.Groups["number"] == null ? -1 : Convert.ToInt32(m.Groups["number"].Value));

                    if (id == ScanId && 
                        (number == ScanNumber || number == -1) && 
                        ((type == "wet" && ScanType == ScanTypes.Wet) || 
                         (type == "dry" && ScanType == ScanTypes.Dry) || 
                          type == ScanType.ToString().ToLower()))
                    {
                        return file;
                    }
                }
            }
            
            // error - could not find a matching file
            return "";
        }


        /// <summary>
        /// Implement comparisons, order by scan id, then scan number, then scan type
        /// </summary>
        /// <param name="scanFile2"></param>
        /// <returns></returns>
        public int CompareTo(ScanFile scanFile2)
        {
            if (this.ScanId < scanFile2.ScanId) return -1;
            if (this.ScanId > scanFile2.ScanId) return 1;

            if (this.ScanNumber < scanFile2.ScanNumber) return -1;
            if (this.ScanNumber > scanFile2.ScanNumber) return 1;

            if (this.ScanType == scanFile2.ScanType) return 0;
            if (this.ScanType == ScanTypes.Wet && scanFile2.ScanType == ScanTypes.Dry) return -1;
            
            return 1;
        }


        public static string ApplyScanType(string template, ScanTypes scanType)
        {
            string aType = "";
            if (scanType == ScanTypes.Wet) aType = "Wet";
            else if (scanType == ScanTypes.Dry) aType = "Dry";
            else aType = scanType.ToString();

            // replace types
            template = template.Replace("{type}", aType.ToLower());
            template = template.Replace("{Type}", aType);
            template = template.Replace("{TYPE}", aType.ToUpper());

            return template;
        }

        public static string ApplyScanId(string template, int scanId)
        {
            Regex r1 = new Regex(@"\{(n+)\}", RegexOptions.IgnoreCase); // for scan id
            Match m1 = r1.Match(template);
            int digits;
            if (m1.Success)
            {
                digits = m1.Groups[1].Length;
                template = r1.Replace(template, scanId.ToString().PadLeft(digits, '0'));
            }
            return template;
        }

        public static string ApplyScanNumber(string template, int scanNumber)
        {
            Regex r2 = new Regex(@"\{(x+)\}", RegexOptions.IgnoreCase); // for scan number
            Match m2 = r2.Match(template);
            int digits;
            if (m2.Success)
            {
                digits = m2.Groups[1].Length;
                template = r2.Replace(template, scanNumber.ToString().PadLeft(digits, '0'));
            }
            return template;
        }

    }
}
