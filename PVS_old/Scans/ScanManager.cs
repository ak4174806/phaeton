﻿using System;
using System.Collections.Generic;
using System.Text;

using System.Text.RegularExpressions;
using System.IO;

using PaintAnalysis.Common;

namespace PVS.Scans
{
    class ScanManager
    {
        #region Properties

        // folder that file is stored in, trailing slash
        private string path;
        public string Path
        {
            get { return path; }
            set { path = value.TrimEnd('/', '\\') + "\\"; }
        }

        // template for file
        private string template;
        public string Template
        {
            get { return template; }
            set { template = value; }
        }

        #endregion

        Regex rScanId;
        Regex rScanNumber;
        Regex rScanType;        

        public ScanManager(string path, string template)
        {
            this.Path = path;
            this.Template = template;

            rScanId = new Regex(@"\{n+\}", RegexOptions.IgnoreCase); // for scan id
            rScanNumber = new Regex(@"\{x+\}", RegexOptions.IgnoreCase); // for scan number
            rScanType = new Regex(@"\{type\}", RegexOptions.IgnoreCase); // for scan type            
        }

        public int[] GetScanIds(ScanTypes scanType)
        {
            Set<int> ids = new Set<int>();

            string aType = "";
            if (scanType == ScanTypes.Dry) aType = "dry";
            else if (scanType == ScanTypes.Wet) aType = "wet";
            else aType = scanType.ToString();

            Regex rFileTest = new Regex("^" + rScanType.Replace(rScanNumber.Replace(rScanId.Replace(Template, @"(?<id>\d+)"), @"(?<number>\d+)"), aType) + "$", RegexOptions.IgnoreCase);

            string fileFilter = rScanType.Replace(rScanNumber.Replace(rScanId.Replace(Template, "*"), "*"), "*"); // can't replace rScanType with scanType to maintain case-insensitivity

            // returns entire paths, not just filenames
            string[] matchedFiles = Directory.GetFiles(Path, fileFilter);

            int pathLength = Path.Length;
            Match m; int id;           
            foreach (string filePath in matchedFiles)
            {
                // need to ignore path
                m = rFileTest.Match(filePath.Substring(pathLength));
                if (m.Success)
                {
                    id = Convert.ToInt32(m.Groups["id"].Value);
                    ids.Add(id);
                }
            }

            int[] ids_a = new int[ids.Count];
            for (int i = 0; i < ids.Count; i++)
            {
                ids_a[i] = ids[i];
            }
            Array.Sort(ids_a);
            return ids_a;
        }

        public int[] GetScanIds()
        {
            Set<int> ids = new Set<int>(GetScanIds(ScanTypes.Dry));

            Set<int> idsSet = ids.Intersect(new Set<int>(GetScanIds(ScanTypes.Wet)));
            int[] idsArray = new int[idsSet.Count];
            for (int i = 0; i < idsSet.Count; i++)
            {
                idsArray[i] = idsSet[i];
            }

            Array.Sort(idsArray);
            return idsArray;
        }

        public int GetLastScanId(ScanTypes scanType)
        {
            int[] ids = GetScanIds(scanType); // can rely on it being sorted
            return (ids.Length > 0 ? ids[ids.Length - 1] : -1);
        }

        public int GetLastScanId()
        {
            int[] ids = GetScanIds(); // can rely on it being sorted
            return (ids.Length > 0 ? ids[ids.Length - 1] : -1);
        }

        public int GetNextScanId(ScanTypes scanType)
        {
            int last = GetLastScanId(scanType);
            return (last == -1 ? 1 : last + 1);
        }

        public int GetNextScanId()
        {
            int last = Math.Max(GetLastScanId(ScanTypes.Wet), GetLastScanId(ScanTypes.Dry));
            return (last == -1 ? 1 : last + 1);
        }
                
        /// <summary>
        /// Finds ordered scans by scanNumber
        /// </summary>
        /// <param name="scanId"></param>
        /// <returns></returns>
        public ScanFile[] FindScans(ScanTypes scanType, int scanId)
        {
            List<ScanFile> scanFiles = new List<ScanFile>();
            
            string aType = "";
            if (scanType == ScanTypes.Dry) aType = "dry";
            else if (scanType == ScanTypes.Wet) aType = "wet";
            else aType = scanType.ToString();

            Regex rFileTest = new Regex("^" + rScanType.Replace(rScanNumber.Replace(rScanId.Replace(Template, @"(?<id>\d+)"), @"(?<number>\d+)") + "$", aType), RegexOptions.IgnoreCase);

            string fileFilter = rScanType.Replace(rScanNumber.Replace(rScanId.Replace(Template, "*"), "*"), "*"); // can't replaec rScanType with scanType to maintain case-insensitivity

            // returns entire paths, not just filenames
            string[] matchedFiles = Directory.GetFiles(Path, fileFilter);

            int pathLength = Path.Length;
            Match m; 
            int id;
            string file;
            foreach (string filePath in matchedFiles)
            {
                // need to ignore path
                file = filePath.Substring(pathLength);
                m = rFileTest.Match(file);
                if (m.Success)
                {
                    id = Convert.ToInt32(m.Groups["id"].Value);
                    
                    if (id == scanId)
                    {
                        ScanFile scanFile = new ScanFile(Path, Template, file);
                        scanFiles.Add(scanFile);
                    }
                }
            }

            scanFiles.Sort();
            return scanFiles.ToArray();
        }

        /// <summary>
        /// Returns the file scan objects for scanCount scans
        /// </summary>
        /// <param name="scanType"></param>
        /// <param name="scanId"></param>
        /// <param name="scanCount"></param>
        /// <returns></returns>
        public ScanFile[] GetScans(ScanTypes scanType, int scanId, int scanCount)
        {
            ScanFile[] scanFiles = new ScanFile[scanCount];
            for (int i = 0; i < scanCount; i++)
            {
                scanFiles[i] = new ScanFile(Path, Template, scanType, scanId, i + 1);
            }

            return scanFiles;
        }
    }
}
