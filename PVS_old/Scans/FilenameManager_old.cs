﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PVS;
using System.Text.RegularExpressions;
using System.IO;

namespace PVS
{
    class FilenameManager_old
    {
        /*private Regex r1; // for id
        private Regex r2; // for scan

        public FilenameManager_old(string folder, string template)
        {
            Folder = folder;
            Template = template;

            r1 = new Regex(@"\{(n+)\}"); // for id
            r2 = new Regex(@"\{(x+)\}"); // for scan
        }

        #region Basic Properties

        // folder file is stored in, trailing slash
        private string folder;
        public string Folder
        {
            get { return folder; }
            set
            {
                folder = value.TrimEnd('/', '\\') + "\\";
            }
        }

        // template for file
        private string template;
        public string Template
        {
            get { return template; }
            set { template = value; }
        }

        #endregion


        public Filename_old Get(int id, PVSAnalysis.ScanTypes type, int scan)
        {
            Filename_old file = new Filename_old(Folder, Template);
            file.ID = id;
            file.Type = type;
            file.Scan = scan;

            return file;
        }

        /// <summary>
        /// Returns all scans for a particular id and type
        /// </summary>
        /// <param name="id"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public Filename_old[] Get(int id, PVSAnalysis.ScanTypes type)
        {
            string template = Filename_old.ApplyID(Filename_old.ApplyType(Template, type), id);
            
            string fileFilter = r2.Replace(template, "*");
            string[] files = Directory.GetFiles(folder, fileFilter);
            Regex test = new Regex(r2.Replace(template, @"(\d+)"));
            
            List<Filename_old> filenames = new List<Filename_old>();
            int i = 0;
            Match m;
            foreach (string file in files)
            {
                m = test.Match(file);
                if (m.Success)
                {
                    filenames[i] = new Filename_old(Folder, Template);
                    filenames[i].ID = id;
                    filenames[i].Type = type;
                    filenames[i].Scan = Convert.ToInt32(m.Groups[1].Value);
                }
                i++;
            }

            return filenames.ToArray();
        }
        
     
        public int[] GetIDs(PVSAnalysis.ScanTypes type)
        {
            string template = Filename_old.ApplyType(Template, type);

            string fileFilter = r1.Replace(r2.Replace(template, "*"), "*");
            string[] files = Directory.GetFiles(folder, fileFilter);
            Regex test = new Regex(r1.Replace(r2.Replace(template, @"(?<scan>\d+)"), @"(?<id>\d+)"));

            List<int> ids = new List<int>();
            int i = 0;
            Match m;
            foreach (string file in files)
            {
                m = test.Match(file);
                if (m.Success)
                {
                    ids[i] = Convert.ToInt32(m.Groups["id"].Value);
                }
                i++;
            }

            return ids.ToArray();
        }

        public Filename_old[] GetLast(PVSAnalysis.ScanTypes type)
        {
            return Get(GetLastID(type), type);
        }

        public int GetLastID(PVSAnalysis.ScanTypes type)
        {
            int[] ids = GetIDs(type);
            int maxId = -1;
            foreach (int id in ids)
            {
                if (id > maxId)
                    maxId = id;
            }

            return maxId;
        }

        public int GetNextID()
        {
            return Math.Max(GetLastID(PVSAnalysis.ScanTypes.Wet), GetLastID(PVSAnalysis.ScanTypes.Dry)) + 1;
        }*/
    }
}
