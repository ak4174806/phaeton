﻿namespace PVS.Scans
{
    /// <summary>
    /// Possible scan types
    /// </summary> 
    public enum ScanTypes
    {
        Wet,
        Dry
    }
}