﻿namespace PVS
{
    partial class PVS_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label lblHeading;
            System.Windows.Forms.GroupBox grpOutput;
            System.Windows.Forms.Label lblPVS;
            System.Windows.Forms.GroupBox grpResults;
            System.Windows.Forms.Label label4;
            System.Windows.Forms.Label label3;
            System.Windows.Forms.Label label2;
            System.Windows.Forms.Label lblScan2;
            System.Windows.Forms.Label lblScan1;
            System.Windows.Forms.GroupBox grpSampleID;
            System.Windows.Forms.Label lblSampleID2;
            System.Windows.Forms.GroupBox grpSampleTimestamp;
            System.Windows.Forms.Label label7;
            System.Windows.Forms.Label label8;
            System.Windows.Forms.GroupBox grpScanNumbers;
            System.Windows.Forms.Label lblDryScanNumber;
            System.Windows.Forms.Label lblWetScanNumber;
            System.Windows.Forms.Label lblDisplayScanNumbers;
            System.Windows.Forms.Label lblCopyright;
            System.Windows.Forms.PictureBox picLogo;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PVS_Form));
            System.Windows.Forms.GroupBox grpScanMetadata;
            System.Windows.Forms.GroupBox grpSettingsProfile;
            System.Windows.Forms.GroupBox groupBox1;
            System.Windows.Forms.Label label11;
            System.Windows.Forms.Label label10;
            System.Windows.Forms.Label label9;
            System.Windows.Forms.GroupBox groupBox2;
            System.Windows.Forms.Label label12;
            System.Windows.Forms.Label label13;
            System.Windows.Forms.Label label14;
            this.txtOutput = new System.Windows.Forms.RichTextBox();
            this.btnSaveOutput = new System.Windows.Forms.Button();
            this.btnClearOutput = new System.Windows.Forms.Button();
            this.txtPvs = new System.Windows.Forms.TextBox();
            this.txtPvs3 = new System.Windows.Forms.TextBox();
            this.txtPvs2 = new System.Windows.Forms.TextBox();
            this.txtPvs1 = new System.Windows.Forms.TextBox();
            this.txtDry3 = new System.Windows.Forms.TextBox();
            this.txtDry2 = new System.Windows.Forms.TextBox();
            this.txtDry1 = new System.Windows.Forms.TextBox();
            this.txtWet3 = new System.Windows.Forms.TextBox();
            this.txtWet2 = new System.Windows.Forms.TextBox();
            this.txtWet1 = new System.Windows.Forms.TextBox();
            this.btnExport = new System.Windows.Forms.Button();
            this.lblScan3 = new System.Windows.Forms.Label();
            this.txtSampleID2 = new System.Windows.Forms.TextBox();
            this.txtSampleID1 = new System.Windows.Forms.TextBox();
            this.lblSampleID1 = new System.Windows.Forms.Label();
            this.radSampleGenerateTimestamp = new System.Windows.Forms.RadioButton();
            this.datSampleCustomTimestamp = new System.Windows.Forms.DateTimePicker();
            this.radSampleCustomTimestamp = new System.Windows.Forms.RadioButton();
            this.cmbDryScanNumber = new System.Windows.Forms.ComboBox();
            this.cmbWetScanNumber = new System.Windows.Forms.ComboBox();
            this.chkAllScans = new System.Windows.Forms.CheckBox();
            this.radCustomScanNumbers = new System.Windows.Forms.RadioButton();
            this.radAutoScanNumbers = new System.Windows.Forms.RadioButton();
            this.cmbField4 = new System.Windows.Forms.ComboBox();
            this.cmbField3 = new System.Windows.Forms.ComboBox();
            this.cmbField2 = new System.Windows.Forms.ComboBox();
            this.cmbField1 = new System.Windows.Forms.ComboBox();
            this.lblField4 = new System.Windows.Forms.Label();
            this.lblField3 = new System.Windows.Forms.Label();
            this.lblField2 = new System.Windows.Forms.Label();
            this.lblField1 = new System.Windows.Forms.Label();
            this.cmbSelectedProfile = new System.Windows.Forms.ComboBox();
            this.numDFTInput = new System.Windows.Forms.NumericUpDown();
            this.numSFInput = new System.Windows.Forms.NumericUpDown();
            this.numWFTInput = new System.Windows.Forms.NumericUpDown();
            this.txtPAOutput = new System.Windows.Forms.TextBox();
            this.txtDFTOutput = new System.Windows.Forms.TextBox();
            this.txtWFTOutput = new System.Windows.Forms.TextBox();
            this.btnAnalysis = new System.Windows.Forms.Button();
            this.txtReading = new System.Windows.Forms.TextBox();
            this.btnConnect = new System.Windows.Forms.Button();
            this.btnStartScan = new System.Windows.Forms.Button();
            this.btnSettings = new System.Windows.Forms.Button();
            this.zgcScan = new ZedGraph.ZedGraphControl();
            this.btnClearScan = new System.Windows.Forms.Button();
            this.btnLoadScan = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.saveFile = new System.Windows.Forms.SaveFileDialog();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.btnPastResults = new System.Windows.Forms.Button();
            this.chkAutoRepeat = new System.Windows.Forms.CheckBox();
            lblHeading = new System.Windows.Forms.Label();
            grpOutput = new System.Windows.Forms.GroupBox();
            lblPVS = new System.Windows.Forms.Label();
            grpResults = new System.Windows.Forms.GroupBox();
            label4 = new System.Windows.Forms.Label();
            label3 = new System.Windows.Forms.Label();
            label2 = new System.Windows.Forms.Label();
            lblScan2 = new System.Windows.Forms.Label();
            lblScan1 = new System.Windows.Forms.Label();
            grpSampleID = new System.Windows.Forms.GroupBox();
            lblSampleID2 = new System.Windows.Forms.Label();
            grpSampleTimestamp = new System.Windows.Forms.GroupBox();
            label7 = new System.Windows.Forms.Label();
            label8 = new System.Windows.Forms.Label();
            grpScanNumbers = new System.Windows.Forms.GroupBox();
            lblDryScanNumber = new System.Windows.Forms.Label();
            lblWetScanNumber = new System.Windows.Forms.Label();
            lblDisplayScanNumbers = new System.Windows.Forms.Label();
            lblCopyright = new System.Windows.Forms.Label();
            picLogo = new System.Windows.Forms.PictureBox();
            grpScanMetadata = new System.Windows.Forms.GroupBox();
            grpSettingsProfile = new System.Windows.Forms.GroupBox();
            groupBox1 = new System.Windows.Forms.GroupBox();
            label11 = new System.Windows.Forms.Label();
            label10 = new System.Windows.Forms.Label();
            label9 = new System.Windows.Forms.Label();
            groupBox2 = new System.Windows.Forms.GroupBox();
            label12 = new System.Windows.Forms.Label();
            label13 = new System.Windows.Forms.Label();
            label14 = new System.Windows.Forms.Label();
            grpOutput.SuspendLayout();
            grpResults.SuspendLayout();
            grpSampleID.SuspendLayout();
            grpSampleTimestamp.SuspendLayout();
            grpScanNumbers.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(picLogo)).BeginInit();
            grpScanMetadata.SuspendLayout();
            grpSettingsProfile.SuspendLayout();
            groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numDFTInput)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSFInput)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numWFTInput)).BeginInit();
            groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblHeading
            // 
            lblHeading.AutoSize = true;
            lblHeading.Font = new System.Drawing.Font("Verdana", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            lblHeading.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(83)))), ((int)(((byte)(84)))));
            lblHeading.Location = new System.Drawing.Point(393, 8);
            lblHeading.Name = "lblHeading";
            lblHeading.Size = new System.Drawing.Size(210, 32);
            lblHeading.TabIndex = 22;
            lblHeading.Text = "PVS Analysis";
            // 
            // grpOutput
            // 
            grpOutput.Controls.Add(this.txtOutput);
            grpOutput.Controls.Add(this.btnSaveOutput);
            grpOutput.Controls.Add(this.btnClearOutput);
            grpOutput.Location = new System.Drawing.Point(721, 590);
            grpOutput.Name = "grpOutput";
            grpOutput.Size = new System.Drawing.Size(228, 104);
            grpOutput.TabIndex = 29;
            grpOutput.TabStop = false;
            grpOutput.Text = "Output/Status";
            grpOutput.Visible = false;
            // 
            // txtOutput
            // 
            this.txtOutput.DetectUrls = false;
            this.txtOutput.Location = new System.Drawing.Point(15, 27);
            this.txtOutput.Name = "txtOutput";
            this.txtOutput.ReadOnly = true;
            this.txtOutput.Size = new System.Drawing.Size(195, 39);
            this.txtOutput.TabIndex = 21;
            this.txtOutput.Text = global::PVS.Properties.Resources.DBPassword;
            // 
            // btnSaveOutput
            // 
            this.btnSaveOutput.Location = new System.Drawing.Point(34, 72);
            this.btnSaveOutput.Name = "btnSaveOutput";
            this.btnSaveOutput.Size = new System.Drawing.Size(75, 23);
            this.btnSaveOutput.TabIndex = 22;
            this.btnSaveOutput.Text = "Save to file";
            this.btnSaveOutput.UseVisualStyleBackColor = true;
            // 
            // btnClearOutput
            // 
            this.btnClearOutput.Location = new System.Drawing.Point(133, 72);
            this.btnClearOutput.Name = "btnClearOutput";
            this.btnClearOutput.Size = new System.Drawing.Size(75, 23);
            this.btnClearOutput.TabIndex = 23;
            this.btnClearOutput.Text = "Clear";
            this.btnClearOutput.UseVisualStyleBackColor = true;
            // 
            // lblPVS
            // 
            lblPVS.AutoSize = true;
            lblPVS.Location = new System.Drawing.Point(6, 118);
            lblPVS.Name = "lblPVS";
            lblPVS.Size = new System.Drawing.Size(28, 13);
            lblPVS.TabIndex = 41;
            lblPVS.Text = "PVS";
            // 
            // grpResults
            // 
            grpResults.Controls.Add(this.txtPvs);
            grpResults.Controls.Add(this.txtPvs3);
            grpResults.Controls.Add(this.txtPvs2);
            grpResults.Controls.Add(this.txtPvs1);
            grpResults.Controls.Add(this.txtDry3);
            grpResults.Controls.Add(this.txtDry2);
            grpResults.Controls.Add(this.txtDry1);
            grpResults.Controls.Add(this.txtWet3);
            grpResults.Controls.Add(this.txtWet2);
            grpResults.Controls.Add(this.txtWet1);
            grpResults.Controls.Add(this.btnExport);
            grpResults.Controls.Add(lblPVS);
            grpResults.Controls.Add(label4);
            grpResults.Controls.Add(label3);
            grpResults.Controls.Add(label2);
            grpResults.Controls.Add(this.lblScan3);
            grpResults.Controls.Add(lblScan2);
            grpResults.Controls.Add(lblScan1);
            grpResults.ForeColor = System.Drawing.SystemColors.ControlText;
            grpResults.Location = new System.Drawing.Point(9, 7);
            grpResults.Name = "grpResults";
            grpResults.Size = new System.Drawing.Size(225, 140);
            grpResults.TabIndex = 39;
            grpResults.TabStop = false;
            grpResults.Text = "Results";
            // 
            // txtPvs
            // 
            this.txtPvs.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPvs.Location = new System.Drawing.Point(50, 113);
            this.txtPvs.Name = "txtPvs";
            this.txtPvs.ReadOnly = true;
            this.txtPvs.Size = new System.Drawing.Size(76, 23);
            this.txtPvs.TabIndex = 45;
            // 
            // txtPvs3
            // 
            this.txtPvs3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPvs3.Location = new System.Drawing.Point(165, 82);
            this.txtPvs3.Name = "txtPvs3";
            this.txtPvs3.ReadOnly = true;
            this.txtPvs3.Size = new System.Drawing.Size(52, 23);
            this.txtPvs3.TabIndex = 43;
            // 
            // txtPvs2
            // 
            this.txtPvs2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPvs2.Location = new System.Drawing.Point(165, 58);
            this.txtPvs2.Name = "txtPvs2";
            this.txtPvs2.ReadOnly = true;
            this.txtPvs2.Size = new System.Drawing.Size(52, 23);
            this.txtPvs2.TabIndex = 43;
            // 
            // txtPvs1
            // 
            this.txtPvs1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPvs1.Location = new System.Drawing.Point(165, 34);
            this.txtPvs1.Name = "txtPvs1";
            this.txtPvs1.ReadOnly = true;
            this.txtPvs1.Size = new System.Drawing.Size(52, 23);
            this.txtPvs1.TabIndex = 43;
            // 
            // txtDry3
            // 
            this.txtDry3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDry3.Location = new System.Drawing.Point(108, 82);
            this.txtDry3.Name = "txtDry3";
            this.txtDry3.ReadOnly = true;
            this.txtDry3.Size = new System.Drawing.Size(52, 23);
            this.txtDry3.TabIndex = 43;
            // 
            // txtDry2
            // 
            this.txtDry2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDry2.Location = new System.Drawing.Point(108, 58);
            this.txtDry2.Name = "txtDry2";
            this.txtDry2.ReadOnly = true;
            this.txtDry2.Size = new System.Drawing.Size(52, 23);
            this.txtDry2.TabIndex = 43;
            // 
            // txtDry1
            // 
            this.txtDry1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDry1.Location = new System.Drawing.Point(108, 34);
            this.txtDry1.Name = "txtDry1";
            this.txtDry1.ReadOnly = true;
            this.txtDry1.Size = new System.Drawing.Size(52, 23);
            this.txtDry1.TabIndex = 43;
            // 
            // txtWet3
            // 
            this.txtWet3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtWet3.Location = new System.Drawing.Point(50, 82);
            this.txtWet3.Name = "txtWet3";
            this.txtWet3.ReadOnly = true;
            this.txtWet3.Size = new System.Drawing.Size(52, 23);
            this.txtWet3.TabIndex = 43;
            // 
            // txtWet2
            // 
            this.txtWet2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtWet2.Location = new System.Drawing.Point(50, 58);
            this.txtWet2.Name = "txtWet2";
            this.txtWet2.ReadOnly = true;
            this.txtWet2.Size = new System.Drawing.Size(52, 23);
            this.txtWet2.TabIndex = 43;
            // 
            // txtWet1
            // 
            this.txtWet1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtWet1.Location = new System.Drawing.Point(50, 34);
            this.txtWet1.Name = "txtWet1";
            this.txtWet1.ReadOnly = true;
            this.txtWet1.Size = new System.Drawing.Size(52, 23);
            this.txtWet1.TabIndex = 43;
            // 
            // btnExport
            // 
            this.btnExport.BackColor = System.Drawing.Color.Transparent;
            this.btnExport.Enabled = false;
            this.btnExport.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExport.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnExport.Location = new System.Drawing.Point(146, 110);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(72, 25);
            this.btnExport.TabIndex = 22;
            this.btnExport.Text = "Export";
            this.btnExport.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Location = new System.Drawing.Point(177, 15);
            label4.Name = "label4";
            label4.Size = new System.Drawing.Size(28, 13);
            label4.TabIndex = 0;
            label4.Text = "PVS";
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Location = new System.Drawing.Point(119, 16);
            label3.Name = "label3";
            label3.Size = new System.Drawing.Size(30, 13);
            label3.TabIndex = 0;
            label3.Text = "DRY";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new System.Drawing.Point(62, 16);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(32, 13);
            label2.TabIndex = 0;
            label2.Text = "WET";
            // 
            // lblScan3
            // 
            this.lblScan3.AutoSize = true;
            this.lblScan3.Location = new System.Drawing.Point(1, 87);
            this.lblScan3.Name = "lblScan3";
            this.lblScan3.Size = new System.Drawing.Size(41, 13);
            this.lblScan3.TabIndex = 0;
            this.lblScan3.Text = "Scan 3";
            // 
            // lblScan2
            // 
            lblScan2.AutoSize = true;
            lblScan2.Location = new System.Drawing.Point(1, 63);
            lblScan2.Name = "lblScan2";
            lblScan2.Size = new System.Drawing.Size(41, 13);
            lblScan2.TabIndex = 0;
            lblScan2.Text = "Scan 2";
            // 
            // lblScan1
            // 
            lblScan1.AutoSize = true;
            lblScan1.Location = new System.Drawing.Point(2, 39);
            lblScan1.Name = "lblScan1";
            lblScan1.Size = new System.Drawing.Size(41, 13);
            lblScan1.TabIndex = 0;
            lblScan1.Text = "Scan 1";
            // 
            // grpSampleID
            // 
            grpSampleID.Controls.Add(this.txtSampleID2);
            grpSampleID.Controls.Add(lblSampleID2);
            grpSampleID.Controls.Add(this.txtSampleID1);
            grpSampleID.Controls.Add(this.lblSampleID1);
            grpSampleID.Location = new System.Drawing.Point(704, 611);
            grpSampleID.Name = "grpSampleID";
            grpSampleID.Size = new System.Drawing.Size(225, 72);
            grpSampleID.TabIndex = 45;
            grpSampleID.TabStop = false;
            grpSampleID.Text = "Sample Identification";
            grpSampleID.Visible = false;
            // 
            // txtSampleID2
            // 
            this.txtSampleID2.Location = new System.Drawing.Point(64, 39);
            this.txtSampleID2.Name = "txtSampleID2";
            this.txtSampleID2.Size = new System.Drawing.Size(141, 20);
            this.txtSampleID2.TabIndex = 9;
            // 
            // lblSampleID2
            // 
            lblSampleID2.AutoSize = true;
            lblSampleID2.Location = new System.Drawing.Point(15, 42);
            lblSampleID2.Name = "lblSampleID2";
            lblSampleID2.Size = new System.Drawing.Size(30, 13);
            lblSampleID2.TabIndex = 8;
            lblSampleID2.Text = "ID 2:";
            // 
            // txtSampleID1
            // 
            this.txtSampleID1.Location = new System.Drawing.Point(64, 14);
            this.txtSampleID1.Name = "txtSampleID1";
            this.txtSampleID1.Size = new System.Drawing.Size(141, 20);
            this.txtSampleID1.TabIndex = 10;
            // 
            // lblSampleID1
            // 
            this.lblSampleID1.AutoSize = true;
            this.lblSampleID1.Location = new System.Drawing.Point(15, 17);
            this.lblSampleID1.Name = "lblSampleID1";
            this.lblSampleID1.Size = new System.Drawing.Size(30, 13);
            this.lblSampleID1.TabIndex = 7;
            this.lblSampleID1.Text = "ID 1:";
            // 
            // grpSampleTimestamp
            // 
            grpSampleTimestamp.Controls.Add(this.radSampleGenerateTimestamp);
            grpSampleTimestamp.Controls.Add(this.datSampleCustomTimestamp);
            grpSampleTimestamp.Controls.Add(this.radSampleCustomTimestamp);
            grpSampleTimestamp.Location = new System.Drawing.Point(666, 599);
            grpSampleTimestamp.Name = "grpSampleTimestamp";
            grpSampleTimestamp.Size = new System.Drawing.Size(207, 74);
            grpSampleTimestamp.TabIndex = 6;
            grpSampleTimestamp.TabStop = false;
            grpSampleTimestamp.Text = "Date/Time";
            grpSampleTimestamp.Visible = false;
            // 
            // radSampleGenerateTimestamp
            // 
            this.radSampleGenerateTimestamp.AutoSize = true;
            this.radSampleGenerateTimestamp.Checked = true;
            this.radSampleGenerateTimestamp.Location = new System.Drawing.Point(9, 19);
            this.radSampleGenerateTimestamp.Name = "radSampleGenerateTimestamp";
            this.radSampleGenerateTimestamp.Size = new System.Drawing.Size(133, 17);
            this.radSampleGenerateTimestamp.TabIndex = 0;
            this.radSampleGenerateTimestamp.TabStop = true;
            this.radSampleGenerateTimestamp.Text = "Generate automatically";
            this.radSampleGenerateTimestamp.UseVisualStyleBackColor = true;
            // 
            // datSampleCustomTimestamp
            // 
            this.datSampleCustomTimestamp.CustomFormat = "dd/MM/yyyy HH:mm:ss";
            this.datSampleCustomTimestamp.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.datSampleCustomTimestamp.Location = new System.Drawing.Point(72, 44);
            this.datSampleCustomTimestamp.Name = "datSampleCustomTimestamp";
            this.datSampleCustomTimestamp.Size = new System.Drawing.Size(129, 20);
            this.datSampleCustomTimestamp.TabIndex = 2;
            // 
            // radSampleCustomTimestamp
            // 
            this.radSampleCustomTimestamp.AutoSize = true;
            this.radSampleCustomTimestamp.Location = new System.Drawing.Point(9, 46);
            this.radSampleCustomTimestamp.Name = "radSampleCustomTimestamp";
            this.radSampleCustomTimestamp.Size = new System.Drawing.Size(60, 17);
            this.radSampleCustomTimestamp.TabIndex = 1;
            this.radSampleCustomTimestamp.TabStop = true;
            this.radSampleCustomTimestamp.Text = "Custom";
            this.radSampleCustomTimestamp.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            label7.AutoSize = true;
            label7.Location = new System.Drawing.Point(428, 590);
            label7.Name = "label7";
            label7.Size = new System.Drawing.Size(153, 13);
            label7.TabIndex = 42;
            label7.Text = "invert scan data automatically?";
            label7.Visible = false;
            // 
            // label8
            // 
            label8.AutoSize = true;
            label8.Location = new System.Drawing.Point(374, 590);
            label8.Name = "label8";
            label8.Size = new System.Drawing.Size(65, 13);
            label8.TabIndex = 43;
            label8.Text = "scan mode?";
            label8.Visible = false;
            // 
            // grpScanNumbers
            // 
            grpScanNumbers.Controls.Add(this.cmbDryScanNumber);
            grpScanNumbers.Controls.Add(this.cmbWetScanNumber);
            grpScanNumbers.Controls.Add(lblDryScanNumber);
            grpScanNumbers.Controls.Add(lblWetScanNumber);
            grpScanNumbers.Controls.Add(this.chkAllScans);
            grpScanNumbers.Controls.Add(lblDisplayScanNumbers);
            grpScanNumbers.Controls.Add(this.radCustomScanNumbers);
            grpScanNumbers.Controls.Add(this.radAutoScanNumbers);
            grpScanNumbers.ForeColor = System.Drawing.SystemColors.ControlText;
            grpScanNumbers.Location = new System.Drawing.Point(9, 339);
            grpScanNumbers.Name = "grpScanNumbers";
            grpScanNumbers.Size = new System.Drawing.Size(228, 99);
            grpScanNumbers.TabIndex = 51;
            grpScanNumbers.TabStop = false;
            grpScanNumbers.Text = "Choose Scan Numbers";
            // 
            // cmbDryScanNumber
            // 
            this.cmbDryScanNumber.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDryScanNumber.FormattingEnabled = true;
            this.cmbDryScanNumber.Location = new System.Drawing.Point(162, 66);
            this.cmbDryScanNumber.Name = "cmbDryScanNumber";
            this.cmbDryScanNumber.Size = new System.Drawing.Size(58, 21);
            this.cmbDryScanNumber.TabIndex = 16;
            // 
            // cmbWetScanNumber
            // 
            this.cmbWetScanNumber.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbWetScanNumber.FormattingEnabled = true;
            this.cmbWetScanNumber.Location = new System.Drawing.Point(68, 63);
            this.cmbWetScanNumber.Name = "cmbWetScanNumber";
            this.cmbWetScanNumber.Size = new System.Drawing.Size(58, 21);
            this.cmbWetScanNumber.TabIndex = 15;
            // 
            // lblDryScanNumber
            // 
            lblDryScanNumber.AutoSize = true;
            lblDryScanNumber.Location = new System.Drawing.Point(171, 50);
            lblDryScanNumber.Name = "lblDryScanNumber";
            lblDryScanNumber.Size = new System.Drawing.Size(47, 13);
            lblDryScanNumber.TabIndex = 6;
            lblDryScanNumber.Text = "dry scan";
            // 
            // lblWetScanNumber
            // 
            lblWetScanNumber.AutoSize = true;
            lblWetScanNumber.Location = new System.Drawing.Point(14, 66);
            lblWetScanNumber.Name = "lblWetScanNumber";
            lblWetScanNumber.Size = new System.Drawing.Size(50, 13);
            lblWetScanNumber.TabIndex = 6;
            lblWetScanNumber.Text = "wet scan";
            // 
            // chkAllScans
            // 
            this.chkAllScans.AutoSize = true;
            this.chkAllScans.Location = new System.Drawing.Point(70, 43);
            this.chkAllScans.Name = "chkAllScans";
            this.chkAllScans.Size = new System.Drawing.Size(67, 17);
            this.chkAllScans.TabIndex = 14;
            this.chkAllScans.Text = "all scans";
            this.chkAllScans.UseVisualStyleBackColor = true;
            // 
            // lblDisplayScanNumbers
            // 
            lblDisplayScanNumbers.AutoSize = true;
            lblDisplayScanNumbers.Location = new System.Drawing.Point(20, 45);
            lblDisplayScanNumbers.Name = "lblDisplayScanNumbers";
            lblDisplayScanNumbers.Size = new System.Drawing.Size(44, 13);
            lblDisplayScanNumbers.TabIndex = 4;
            lblDisplayScanNumbers.Text = "Display:";
            // 
            // radCustomScanNumbers
            // 
            this.radCustomScanNumbers.AutoSize = true;
            this.radCustomScanNumbers.Location = new System.Drawing.Point(73, 19);
            this.radCustomScanNumbers.Name = "radCustomScanNumbers";
            this.radCustomScanNumbers.Size = new System.Drawing.Size(59, 17);
            this.radCustomScanNumbers.TabIndex = 13;
            this.radCustomScanNumbers.Text = "custom";
            this.radCustomScanNumbers.UseVisualStyleBackColor = true;
            // 
            // radAutoScanNumbers
            // 
            this.radAutoScanNumbers.AutoSize = true;
            this.radAutoScanNumbers.Checked = true;
            this.radAutoScanNumbers.Location = new System.Drawing.Point(18, 19);
            this.radAutoScanNumbers.Name = "radAutoScanNumbers";
            this.radAutoScanNumbers.Size = new System.Drawing.Size(46, 17);
            this.radAutoScanNumbers.TabIndex = 12;
            this.radAutoScanNumbers.TabStop = true;
            this.radAutoScanNumbers.Text = "auto";
            this.radAutoScanNumbers.UseVisualStyleBackColor = true;
            // 
            // lblCopyright
            // 
            lblCopyright.AutoSize = true;
            lblCopyright.ForeColor = System.Drawing.SystemColors.ControlText;
            lblCopyright.Location = new System.Drawing.Point(634, 560);
            lblCopyright.Name = "lblCopyright";
            lblCopyright.Size = new System.Drawing.Size(156, 13);
            lblCopyright.TabIndex = 30;
            lblCopyright.Text = "Copyright 2009 Wolf Innovation";
            // 
            // picLogo
            // 
            picLogo.Image = ((System.Drawing.Image)(resources.GetObject("picLogo.Image")));
            picLogo.Location = new System.Drawing.Point(240, 15);
            picLogo.Name = "picLogo";
            picLogo.Size = new System.Drawing.Size(139, 63);
            picLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            picLogo.TabIndex = 23;
            picLogo.TabStop = false;
            // 
            // grpScanMetadata
            // 
            grpScanMetadata.Controls.Add(this.cmbField4);
            grpScanMetadata.Controls.Add(this.cmbField3);
            grpScanMetadata.Controls.Add(this.cmbField2);
            grpScanMetadata.Controls.Add(this.cmbField1);
            grpScanMetadata.Controls.Add(this.lblField4);
            grpScanMetadata.Controls.Add(this.lblField3);
            grpScanMetadata.Controls.Add(this.lblField2);
            grpScanMetadata.Controls.Add(this.lblField1);
            grpScanMetadata.ForeColor = System.Drawing.SystemColors.ControlText;
            grpScanMetadata.Location = new System.Drawing.Point(9, 158);
            grpScanMetadata.Name = "grpScanMetadata";
            grpScanMetadata.Size = new System.Drawing.Size(228, 119);
            grpScanMetadata.TabIndex = 51;
            grpScanMetadata.TabStop = false;
            grpScanMetadata.Text = "Scan Metadata";
            // 
            // cmbField4
            // 
            this.cmbField4.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cmbField4.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbField4.FormattingEnabled = true;
            this.cmbField4.Location = new System.Drawing.Point(110, 92);
            this.cmbField4.Name = "cmbField4";
            this.cmbField4.Size = new System.Drawing.Size(108, 21);
            this.cmbField4.TabIndex = 7;
            // 
            // cmbField3
            // 
            this.cmbField3.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cmbField3.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbField3.FormattingEnabled = true;
            this.cmbField3.Location = new System.Drawing.Point(110, 69);
            this.cmbField3.Name = "cmbField3";
            this.cmbField3.Size = new System.Drawing.Size(108, 21);
            this.cmbField3.TabIndex = 6;
            // 
            // cmbField2
            // 
            this.cmbField2.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cmbField2.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbField2.FormattingEnabled = true;
            this.cmbField2.Location = new System.Drawing.Point(110, 45);
            this.cmbField2.Name = "cmbField2";
            this.cmbField2.Size = new System.Drawing.Size(108, 21);
            this.cmbField2.TabIndex = 5;
            // 
            // cmbField1
            // 
            this.cmbField1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cmbField1.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbField1.FormattingEnabled = true;
            this.cmbField1.Location = new System.Drawing.Point(110, 22);
            this.cmbField1.Name = "cmbField1";
            this.cmbField1.Size = new System.Drawing.Size(108, 21);
            this.cmbField1.TabIndex = 4;
            // 
            // lblField4
            // 
            this.lblField4.AutoSize = true;
            this.lblField4.Location = new System.Drawing.Point(8, 95);
            this.lblField4.Name = "lblField4";
            this.lblField4.Size = new System.Drawing.Size(41, 13);
            this.lblField4.TabIndex = 0;
            this.lblField4.Text = "Field 4:";
            // 
            // lblField3
            // 
            this.lblField3.AutoSize = true;
            this.lblField3.Location = new System.Drawing.Point(8, 72);
            this.lblField3.Name = "lblField3";
            this.lblField3.Size = new System.Drawing.Size(41, 13);
            this.lblField3.TabIndex = 0;
            this.lblField3.Text = "Field 3:";
            // 
            // lblField2
            // 
            this.lblField2.AutoSize = true;
            this.lblField2.Location = new System.Drawing.Point(8, 48);
            this.lblField2.Name = "lblField2";
            this.lblField2.Size = new System.Drawing.Size(41, 13);
            this.lblField2.TabIndex = 0;
            this.lblField2.Text = "Field 2:";
            // 
            // lblField1
            // 
            this.lblField1.AutoSize = true;
            this.lblField1.Location = new System.Drawing.Point(8, 25);
            this.lblField1.Name = "lblField1";
            this.lblField1.Size = new System.Drawing.Size(41, 13);
            this.lblField1.TabIndex = 0;
            this.lblField1.Text = "Field 1:";
            // 
            // grpSettingsProfile
            // 
            grpSettingsProfile.Controls.Add(this.cmbSelectedProfile);
            grpSettingsProfile.Location = new System.Drawing.Point(12, 284);
            grpSettingsProfile.Name = "grpSettingsProfile";
            grpSettingsProfile.Size = new System.Drawing.Size(225, 47);
            grpSettingsProfile.TabIndex = 52;
            grpSettingsProfile.TabStop = false;
            grpSettingsProfile.Text = "Select Settings Profile";
            // 
            // cmbSelectedProfile
            // 
            this.cmbSelectedProfile.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSelectedProfile.FormattingEnabled = true;
            this.cmbSelectedProfile.Location = new System.Drawing.Point(16, 18);
            this.cmbSelectedProfile.Name = "cmbSelectedProfile";
            this.cmbSelectedProfile.Size = new System.Drawing.Size(121, 21);
            this.cmbSelectedProfile.TabIndex = 0;
            // 
            // groupBox1
            // 
            groupBox1.Controls.Add(this.numDFTInput);
            groupBox1.Controls.Add(label11);
            groupBox1.Controls.Add(this.numSFInput);
            groupBox1.Controls.Add(label10);
            groupBox1.Controls.Add(this.numWFTInput);
            groupBox1.Controls.Add(label9);
            groupBox1.Location = new System.Drawing.Point(9, 481);
            groupBox1.Name = "groupBox1";
            groupBox1.Size = new System.Drawing.Size(102, 92);
            groupBox1.TabIndex = 53;
            groupBox1.TabStop = false;
            groupBox1.Text = "Inputs";
            // 
            // numDFTInput
            // 
            this.numDFTInput.DecimalPlaces = 1;
            this.numDFTInput.Location = new System.Drawing.Point(43, 37);
            this.numDFTInput.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numDFTInput.Name = "numDFTInput";
            this.numDFTInput.Size = new System.Drawing.Size(51, 20);
            this.numDFTInput.TabIndex = 1;
            // 
            // label11
            // 
            label11.AutoSize = true;
            label11.Location = new System.Drawing.Point(8, 39);
            label11.Name = "label11";
            label11.Size = new System.Drawing.Size(28, 13);
            label11.TabIndex = 0;
            label11.Text = "DFT";
            // 
            // numSFInput
            // 
            this.numSFInput.DecimalPlaces = 1;
            this.numSFInput.Location = new System.Drawing.Point(43, 63);
            this.numSFInput.Name = "numSFInput";
            this.numSFInput.Size = new System.Drawing.Size(51, 20);
            this.numSFInput.TabIndex = 1;
            // 
            // label10
            // 
            label10.AutoSize = true;
            label10.Location = new System.Drawing.Point(8, 65);
            label10.Name = "label10";
            label10.Size = new System.Drawing.Size(20, 13);
            label10.TabIndex = 0;
            label10.Text = "SF";
            // 
            // numWFTInput
            // 
            this.numWFTInput.DecimalPlaces = 1;
            this.numWFTInput.Location = new System.Drawing.Point(43, 14);
            this.numWFTInput.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numWFTInput.Name = "numWFTInput";
            this.numWFTInput.Size = new System.Drawing.Size(51, 20);
            this.numWFTInput.TabIndex = 1;
            // 
            // label9
            // 
            label9.AutoSize = true;
            label9.Location = new System.Drawing.Point(6, 16);
            label9.Name = "label9";
            label9.Size = new System.Drawing.Size(31, 13);
            label9.TabIndex = 0;
            label9.Text = "WFT";
            // 
            // groupBox2
            // 
            groupBox2.Controls.Add(this.txtPAOutput);
            groupBox2.Controls.Add(this.txtDFTOutput);
            groupBox2.Controls.Add(this.txtWFTOutput);
            groupBox2.Controls.Add(label12);
            groupBox2.Controls.Add(label13);
            groupBox2.Controls.Add(label14);
            groupBox2.Location = new System.Drawing.Point(117, 481);
            groupBox2.Name = "groupBox2";
            groupBox2.Size = new System.Drawing.Size(120, 92);
            groupBox2.TabIndex = 53;
            groupBox2.TabStop = false;
            groupBox2.Text = "Outputs";
            // 
            // txtPAOutput
            // 
            this.txtPAOutput.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPAOutput.Location = new System.Drawing.Point(45, 63);
            this.txtPAOutput.Name = "txtPAOutput";
            this.txtPAOutput.ReadOnly = true;
            this.txtPAOutput.Size = new System.Drawing.Size(52, 23);
            this.txtPAOutput.TabIndex = 44;
            // 
            // txtDFTOutput
            // 
            this.txtDFTOutput.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDFTOutput.Location = new System.Drawing.Point(45, 15);
            this.txtDFTOutput.Name = "txtDFTOutput";
            this.txtDFTOutput.ReadOnly = true;
            this.txtDFTOutput.Size = new System.Drawing.Size(52, 23);
            this.txtDFTOutput.TabIndex = 44;
            // 
            // txtWFTOutput
            // 
            this.txtWFTOutput.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtWFTOutput.Location = new System.Drawing.Point(45, 38);
            this.txtWFTOutput.Name = "txtWFTOutput";
            this.txtWFTOutput.ReadOnly = true;
            this.txtWFTOutput.Size = new System.Drawing.Size(52, 23);
            this.txtWFTOutput.TabIndex = 44;
            // 
            // label12
            // 
            label12.AutoSize = true;
            label12.Location = new System.Drawing.Point(8, 20);
            label12.Name = "label12";
            label12.Size = new System.Drawing.Size(28, 13);
            label12.TabIndex = 0;
            label12.Text = "DFT";
            // 
            // label13
            // 
            label13.AutoSize = true;
            label13.Location = new System.Drawing.Point(8, 68);
            label13.Name = "label13";
            label13.Size = new System.Drawing.Size(21, 13);
            label13.TabIndex = 0;
            label13.Text = "PA";
            // 
            // label14
            // 
            label14.AutoSize = true;
            label14.Location = new System.Drawing.Point(6, 43);
            label14.Name = "label14";
            label14.Size = new System.Drawing.Size(31, 13);
            label14.TabIndex = 0;
            label14.Text = "WFT";
            // 
            // btnAnalysis
            // 
            this.btnAnalysis.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAnalysis.Location = new System.Drawing.Point(431, 615);
            this.btnAnalysis.Name = "btnAnalysis";
            this.btnAnalysis.Size = new System.Drawing.Size(112, 25);
            this.btnAnalysis.TabIndex = 42;
            this.btnAnalysis.Text = "Detailed Analysis";
            this.btnAnalysis.UseVisualStyleBackColor = true;
            this.btnAnalysis.Visible = false;
            // 
            // txtReading
            // 
            this.txtReading.BackColor = System.Drawing.Color.AliceBlue;
            this.txtReading.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtReading.Location = new System.Drawing.Point(388, 47);
            this.txtReading.Multiline = true;
            this.txtReading.Name = "txtReading";
            this.txtReading.ReadOnly = true;
            this.txtReading.Size = new System.Drawing.Size(222, 47);
            this.txtReading.TabIndex = 3;
            // 
            // btnConnect
            // 
            this.btnConnect.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConnect.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnConnect.Location = new System.Drawing.Point(685, 4);
            this.btnConnect.Name = "btnConnect";
            this.btnConnect.Size = new System.Drawing.Size(103, 29);
            this.btnConnect.TabIndex = 19;
            this.btnConnect.Text = "Connect";
            this.btnConnect.UseVisualStyleBackColor = true;
            // 
            // btnStartScan
            // 
            this.btnStartScan.BackColor = System.Drawing.Color.Transparent;
            this.btnStartScan.Enabled = false;
            this.btnStartScan.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStartScan.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnStartScan.Location = new System.Drawing.Point(616, 36);
            this.btnStartScan.Name = "btnStartScan";
            this.btnStartScan.Size = new System.Drawing.Size(83, 29);
            this.btnStartScan.TabIndex = 20;
            this.btnStartScan.Text = "Start Scan";
            this.btnStartScan.UseVisualStyleBackColor = true;
            // 
            // btnSettings
            // 
            this.btnSettings.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSettings.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnSettings.Location = new System.Drawing.Point(705, 36);
            this.btnSettings.Name = "btnSettings";
            this.btnSettings.Size = new System.Drawing.Size(83, 29);
            this.btnSettings.TabIndex = 1;
            this.btnSettings.Text = "Settings";
            this.btnSettings.UseVisualStyleBackColor = true;
            // 
            // zgcScan
            // 
            this.zgcScan.Location = new System.Drawing.Point(248, 103);
            this.zgcScan.Name = "zgcScan";
            this.zgcScan.ScrollGrace = 0D;
            this.zgcScan.ScrollMaxX = 0D;
            this.zgcScan.ScrollMaxY = 0D;
            this.zgcScan.ScrollMaxY2 = 0D;
            this.zgcScan.ScrollMinX = 0D;
            this.zgcScan.ScrollMinY = 0D;
            this.zgcScan.ScrollMinY2 = 0D;
            this.zgcScan.Size = new System.Drawing.Size(543, 452);
            this.zgcScan.TabIndex = 46;
            // 
            // btnClearScan
            // 
            this.btnClearScan.BackColor = System.Drawing.Color.Transparent;
            this.btnClearScan.Enabled = false;
            this.btnClearScan.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClearScan.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnClearScan.Location = new System.Drawing.Point(616, 69);
            this.btnClearScan.Name = "btnClearScan";
            this.btnClearScan.Size = new System.Drawing.Size(83, 29);
            this.btnClearScan.TabIndex = 22;
            this.btnClearScan.Text = "Clear Scan";
            this.btnClearScan.UseVisualStyleBackColor = true;
            // 
            // btnLoadScan
            // 
            this.btnLoadScan.BackColor = System.Drawing.Color.Transparent;
            this.btnLoadScan.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLoadScan.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnLoadScan.Location = new System.Drawing.Point(123, 444);
            this.btnLoadScan.Name = "btnLoadScan";
            this.btnLoadScan.Size = new System.Drawing.Size(91, 29);
            this.btnLoadScan.TabIndex = 21;
            this.btnLoadScan.Text = "Load Scan";
            this.btnLoadScan.UseVisualStyleBackColor = true;
            // 
            // btnClose
            // 
            this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnClose.Location = new System.Drawing.Point(704, 69);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(83, 29);
            this.btnClose.TabIndex = 2;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            // 
            // imageList1
            // 
            this.imageList1.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.imageList1.ImageSize = new System.Drawing.Size(16, 16);
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // btnPastResults
            // 
            this.btnPastResults.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.btnPastResults.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnPastResults.Location = new System.Drawing.Point(18, 444);
            this.btnPastResults.Name = "btnPastResults";
            this.btnPastResults.Size = new System.Drawing.Size(91, 29);
            this.btnPastResults.TabIndex = 17;
            this.btnPastResults.Text = "Past Results";
            this.btnPastResults.UseVisualStyleBackColor = true;
            // 
            // chkAutoRepeat
            // 
            this.chkAutoRepeat.AutoSize = true;
            this.chkAutoRepeat.Location = new System.Drawing.Point(619, 11);
            this.chkAutoRepeat.Name = "chkAutoRepeat";
            this.chkAutoRepeat.Size = new System.Drawing.Size(47, 17);
            this.chkAutoRepeat.TabIndex = 14;
            this.chkAutoRepeat.Text = "auto";
            this.chkAutoRepeat.UseVisualStyleBackColor = true;
            // 
            // PVS_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(794, 587);
            this.Controls.Add(groupBox2);
            this.Controls.Add(groupBox1);
            this.Controls.Add(grpSettingsProfile);
            this.Controls.Add(grpScanMetadata);
            this.Controls.Add(this.chkAutoRepeat);
            this.Controls.Add(grpScanNumbers);
            this.Controls.Add(this.btnSettings);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnAnalysis);
            this.Controls.Add(lblCopyright);
            this.Controls.Add(this.btnClearScan);
            this.Controls.Add(this.btnLoadScan);
            this.Controls.Add(grpSampleTimestamp);
            this.Controls.Add(this.zgcScan);
            this.Controls.Add(grpSampleID);
            this.Controls.Add(label8);
            this.Controls.Add(label7);
            this.Controls.Add(grpResults);
            this.Controls.Add(this.btnStartScan);
            this.Controls.Add(this.btnPastResults);
            this.Controls.Add(this.btnConnect);
            this.Controls.Add(this.txtReading);
            this.Controls.Add(grpOutput);
            this.Controls.Add(picLogo);
            this.Controls.Add(lblHeading);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "PVS_Form";
            this.Text = "Wolf Innovation: PVS Analysis";
            this.Load += new System.EventHandler(this.PVS_Form_Load);
            grpOutput.ResumeLayout(false);
            grpResults.ResumeLayout(false);
            grpResults.PerformLayout();
            grpSampleID.ResumeLayout(false);
            grpSampleID.PerformLayout();
            grpSampleTimestamp.ResumeLayout(false);
            grpSampleTimestamp.PerformLayout();
            grpScanNumbers.ResumeLayout(false);
            grpScanNumbers.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(picLogo)).EndInit();
            grpScanMetadata.ResumeLayout(false);
            grpScanMetadata.PerformLayout();
            grpSettingsProfile.ResumeLayout(false);
            groupBox1.ResumeLayout(false);
            groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numDFTInput)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSFInput)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numWFTInput)).EndInit();
            groupBox2.ResumeLayout(false);
            groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox txtOutput;
        private System.Windows.Forms.Button btnSaveOutput;
        private System.Windows.Forms.Button btnClearOutput;
        private System.Windows.Forms.TextBox txtReading;
        private System.Windows.Forms.Button btnConnect;
        private System.Windows.Forms.Button btnStartScan;
        private System.Windows.Forms.Button btnAnalysis;
        private System.Windows.Forms.Button btnSettings;
        private System.Windows.Forms.TextBox txtSampleID2;
        private System.Windows.Forms.TextBox txtSampleID1;
        private System.Windows.Forms.Label lblSampleID1;
        private System.Windows.Forms.RadioButton radSampleGenerateTimestamp;
        private System.Windows.Forms.DateTimePicker datSampleCustomTimestamp;
        private System.Windows.Forms.RadioButton radSampleCustomTimestamp;
        private System.Windows.Forms.TextBox txtPvs;
        private ZedGraph.ZedGraphControl zgcScan;
        private System.Windows.Forms.Button btnClearScan;
        private System.Windows.Forms.Button btnLoadScan;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.SaveFileDialog saveFile;
        private System.Windows.Forms.RadioButton radCustomScanNumbers;
        private System.Windows.Forms.RadioButton radAutoScanNumbers;
        private System.Windows.Forms.CheckBox chkAllScans;
        private System.Windows.Forms.ComboBox cmbDryScanNumber;
        private System.Windows.Forms.ComboBox cmbWetScanNumber;
        private System.Windows.Forms.TextBox txtPvs3;
        private System.Windows.Forms.TextBox txtPvs2;
        private System.Windows.Forms.TextBox txtPvs1;
        private System.Windows.Forms.TextBox txtDry3;
        private System.Windows.Forms.TextBox txtDry2;
        private System.Windows.Forms.TextBox txtDry1;
        private System.Windows.Forms.TextBox txtWet3;
        private System.Windows.Forms.TextBox txtWet2;
        private System.Windows.Forms.TextBox txtWet1;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.Button btnPastResults;
        private System.Windows.Forms.Label lblField4;
        private System.Windows.Forms.Label lblField3;
        private System.Windows.Forms.Label lblField2;
        private System.Windows.Forms.Label lblField1;
        private System.Windows.Forms.ComboBox cmbField4;
        private System.Windows.Forms.ComboBox cmbField3;
        private System.Windows.Forms.ComboBox cmbField2;
        private System.Windows.Forms.ComboBox cmbField1;
        private System.Windows.Forms.ComboBox cmbSelectedProfile;
        private System.Windows.Forms.NumericUpDown numDFTInput;
        private System.Windows.Forms.NumericUpDown numSFInput;
        private System.Windows.Forms.NumericUpDown numWFTInput;
        private System.Windows.Forms.TextBox txtPAOutput;
        private System.Windows.Forms.TextBox txtDFTOutput;
        private System.Windows.Forms.TextBox txtWFTOutput;
        private System.Windows.Forms.Label lblScan3;
        private System.Windows.Forms.CheckBox chkAutoRepeat;
        private System.Windows.Forms.Button btnExport;
    }
}

