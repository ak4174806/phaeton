﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

using SensorControl;
using PVS.Scans;
using PaintAnalysis.Common;
using Analysis;

using System.Data.SQLite;

namespace PVS
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new PVS_Form());

            /*DFMAnalysis analysis = new DFMAnalysis();
            analysis.Options = new DFMAnalysisOptions();
            analysis.Options.SmoothingWidth = 100;
            analysis.Options.ThresholdParameter = 0.65;
            analysis.Options.MinPitWidth = 20;
            analysis.Options.Compression = 0.12;
            
            DataIO reader = new DataIO();
            reader.Mode = DataIO.Modes.Read;
            reader.Filename = @"C:\SCAN.CSV";

            double[] data_x, data_y;
            reader.ReadData(out data_x, out data_y);

            analysis.SetData(data_x, data_y);
            analysis.Analyse();

            Console.WriteLine("Press any key ...");
            Console.Read();*/
        }
    }
}
