DROP TABLE IF EXISTS results;
DROP TABLE IF EXISTS keystore; 
CREATE TABLE results 
(
	result_id INTEGER PRIMARY KEY,
	date INTEGER NOT NULL, 
        pvs TEXT NOT NULL, 
	scan_path TEXT NOT NULL, 
	scan_id INTEGER NOT NULL
);

CREATE TABLE keystore
(
	key TEXT PRIMARY KEY,
	value TEXT
);

INSERT INTO keystore VALUES 
(
	'database_version', '1.0'
);