﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using System.IO;
using System.Threading;
using System.IO.Ports;
using System.Text.RegularExpressions;
using ZedGraph;

using PaintAnalysis.Common;
using SensorControl;
using SensorControl.Sensors;

namespace MovingAveraging
{
    public partial class MovingAveraging_Form : Form
    {
        /// <summary>
        /// Time to keep data buffered for in seconds
        /// </summary>
        public static int dataRecordTime = 30;

        private Log log;

        #region Private Members

        /// <summary>
        /// The available sensors
        /// </summary>
        private Sensor[] sensors;

        /// <summary>
        /// The current sensor
        /// </summary>
        private Sensor sensor;

        /// <summary>
        /// The current state of the form
        /// </summary>
        private States state;

        /// <summary>
        /// Manages output
        /// </summary>
        private OutputManager output;

        /// <summary>
        /// Used to determine when changes are made, what to "cancel" to
        /// </summary>
        private int currentSelectedSensorModel;

        /// <summary>
        /// The currently selected sensor type
        /// </summary>
        private int currentSelectedSensorType;

        /// <summary>
        /// The number of points to skip when saving data (ie: filteringNumber = 10 means save every 10th point)
        /// </summary>
        private int filteringNumber;

        /// <summary>
        /// Whether (moving) averaging is enabled
        /// </summary>
        private bool enableAveraging;

        /// <summary>
        /// What averaging number to use
        /// </summary>
        private int averagingNumber;

        /// <summary>
        /// The time between refreshing graph in ms
        /// </summary>
        private int graphRefreshTime;

        /// <summary>
        /// The position of the graph from each corner in order (l, t, r, b)
        /// </summary>
        private int[] graphPosition;

        /// <summary>
        /// The distance of left edge of right column from right edge of form
        /// </summary>
        private int[] rightColPosition;

        /// <summary>
        /// Buffers readings from the sensor to use for averaging
        /// </summary>
        private RollingArrayList readingsBuffer;

        /// <summary>
        /// Buffers averaged readings before saving to file
        /// </summary>
        private RollingArrayList readingsFileBuffer;

        /// <summary>
        /// Keeps track of sum of some of the previous measurements, used in moving averages
        /// </summary>
        private double runningSum;

        /// <summary>
        /// Whether there is an offset
        /// </summary>
        private bool hasOffset;

        /// <summary>
        /// The reference value in mm when the offset was set
        /// </summary>
        private double offsetReference;

        /// <summary>
        /// Whether to invert readings
        /// </summary>
        private bool invertReadings;

        /// <summary>
        /// The systime when last reference was taken - used for accounting for drift
        /// </summary>
        private DateTime referenceTime;

        /// <summary>
        /// The drift rate in micron/min
        /// </summary>
        private double driftFactor;
        #endregion        

        public MovingAveraging_Form()
        {
            InitializeComponent();

            log = new Log("log-movingaveraging.txt");            

            // set up sensors
            sensors = new Sensor[3];
            sensors[0] = new IFC2401_Sensor();
            sensors[1] = new ILD2200_Sensor();
            sensors[2] = new ILD1700_Sensor();

            // create output manager
            output = new OutputManager(txtOutput);
                        
            // update models and types
            UpdateSensorModels();
            UpdateSensorTypes();

            // default averaging settings
            enableAveraging = true;
            averagingNumber = 100;
            graphRefreshTime = 50;
            filteringNumber = 1;
            hasOffset = false;
            invertReadings = chkInvertGraph.Checked;

            // set state
            State = States.SELECT_SENSOR;

            // set some more defaults
            readingsBuffer = new RollingArrayList();
            readingsFileBuffer = new RollingArrayList();
            runningSum = 0.0;
            
            // add event handlers
            // form closing
            this.btnClose.Click += new EventHandler(btnClose_Click);
            this.FormClosing += new FormClosingEventHandler(MovingAveraging_Form_FormClosing);

            // for sensor selection
            this.cmbSensorModel.SelectedIndexChanged += new EventHandler(cmbSensorModel_SelectedIndexChanged);
            this.cmbSensorType.SelectedIndexChanged += new EventHandler(cmbSensorType_SelectedIndexChanged);

            this.btnUpdateSensorSelection.Click += new EventHandler(btnUpdateSensorSelection_Click);
            this.btnCancelSensorSelection.Click += new EventHandler(btnCancelSensorSelection_Click);

            // for connection
            this.btnRefreshCOMPorts.Click += new EventHandler(btnRefreshCOMPorts_Click);
            this.btnConnect.Click += new EventHandler(btnConnect_Click);
            this.btnDisconnect.Click += new EventHandler(btnDisconnect_Click);

            // for sensor parameters
            this.cmbFrequency.SelectedIndexChanged += new EventHandler(cmbSensorOption_Changed);
            
            this.btnUpdateOptions.Click += new EventHandler(btnUpdateOptions_Click);
            this.btnCancelOptions.Click += new EventHandler(btnCancelOptions_Click);

            // for output panel
            this.btnSaveOutput.Click += new EventHandler(btnSaveOutput_Click);
            this.btnClearOutput.Click += new EventHandler(btnClearOutput_Click);

            // for data settings
            this.btnChooseDataFolder.Click += new EventHandler(btnChooseDataFolder_Click);
            this.numFiltering.ValueChanged += new EventHandler(numFiltering_ValueChanged);
            
            // for graph settings
            this.chkEnableAveraging.CheckStateChanged += new EventHandler(chkEnableAveraging_CheckStateChanged);
            this.cmbScaleMode.SelectedIndexChanged += new EventHandler(cmbScaleMode_SelectedIndexChanged);

            // for measurements section
            this.btnStart.Click += new EventHandler(btnStart_Click);
            this.btnStop.Click += new EventHandler(btnStop_Click);

            this.btnSaveData.Click += new EventHandler(btnSaveData_Click);
            this.chkAutoSaveData.Click += new EventHandler(chkAutoSaveData_Click);

            // for offsets
            this.btnZeroReadings.Click += new EventHandler(btnZeroReadings_Click);
            this.btnResetOffset.Click += new EventHandler(btnResetOffset_Click);

            // for graph
            this.Load += new EventHandler(MovingAveraging_Form_Load);
            this.Resize += new EventHandler(MovingAveraging_Form_Resize);

            // remove unnecessary stuff from graph context menu
            this.zgcData.ContextMenuBuilder += new ZedGraphControl.ContextMenuBuilderEventHandler(zgcData_ContextMenuBuilder);

            this.chkInvertGraph.CheckedChanged += new EventHandler(chkInvertGraph_CheckedChanged);

            // timer
            timerGraph.Tick += new EventHandler(timerGraph_Tick);
            timerSaveData.Tick += new EventHandler(timerSaveData_Tick);

            // default scale mode
            cmbScaleMode.SelectedIndex = 0;
        }
                                
        #region State Handling

        public enum States
        {
            SELECT_SENSOR,
            PORT_DISCONNECTED,
            PORT_CONNECTED,
            MEASUREMENT_RUNNING
        };

        public States State
        {
            get { return state; }
            set
            {
                switch (value)
                {
                    case States.SELECT_SENSOR:
                        cmbSensorModel.Enabled = true;
                        cmbSensorType.Enabled = true;
                        btnUpdateSensorSelection.Enabled = true;
                        btnCancelSensorSelection.Enabled = false;

                        cmbCOMPorts.Enabled = false;
                        btnRefreshCOMPorts.Enabled = false;
                        cmbConnectionBaudRate.Enabled = false;
                        btnConnect.Enabled = false;
                        btnDisconnect.Enabled = false;

                        cmbFrequency.Enabled = false;
                        btnUpdateOptions.Enabled = false;
                        btnCancelOptions.Enabled = false;
                        
                        btnChooseDataFolder.Enabled = false;
                        numFiltering.Enabled = false;

                        numFactor.Enabled = false;
                        numDriftFactor.Enabled = false;

                        numGraphRefreshTime.Enabled = false;
                        chkEnableAveraging.Enabled = false;
                        numAveragingNumber.Enabled = false;
                        cmbScaleMode.Enabled = false;
                        numVerticalRange.Enabled = false;
                        
                        btnStart.Enabled = false;
                        btnStop.Enabled = false;
                        btnSaveData.Enabled = false;
                        chkAutoSaveData.Enabled = false;

                        txtReading.Text = "Select a sensor";
                        zgcData.Enabled = false;

                        btnZeroReadings.Enabled = false;
                        btnResetOffset.Enabled = false;
                        numReferenceValue.Enabled = false;
                        chkInvertGraph.Enabled = false;
                        break;

                    case States.PORT_DISCONNECTED:
                        cmbSensorModel.Enabled = true;
                        cmbSensorType.Enabled = true;
                        btnUpdateSensorSelection.Enabled = false;
                        btnCancelSensorSelection.Enabled = false;

                        cmbCOMPorts.Enabled = true;
                        btnRefreshCOMPorts.Enabled = true;
                        cmbConnectionBaudRate.Enabled = true;
                        btnConnect.Enabled = true;
                        btnDisconnect.Enabled = false;

                        cmbFrequency.Enabled = false;
                        btnUpdateOptions.Enabled = false;
                        btnCancelOptions.Enabled = false;

                        btnChooseDataFolder.Enabled = false;
                        numFiltering.Enabled = false;

                        numFactor.Enabled = false;
                        numDriftFactor.Enabled = false;

                        numGraphRefreshTime.Enabled = false;
                        chkEnableAveraging.Enabled = false;
                        numAveragingNumber.Enabled = false;
                        cmbScaleMode.Enabled = false;
                        numVerticalRange.Enabled = false;
                        
                        btnStart.Enabled = false;
                        btnStop.Enabled = false;
                        btnSaveData.Enabled = false;
                        chkAutoSaveData.Enabled = false;

                        txtReading.Text = "Configure connection";
                        zgcData.Enabled = false;

                        btnZeroReadings.Enabled = false;
                        btnResetOffset.Enabled = false;
                        numReferenceValue.Enabled = false;
                        chkInvertGraph.Enabled = false;
                        break;

                    case States.PORT_CONNECTED:
                        cmbSensorModel.Enabled = false;
                        cmbSensorType.Enabled = false;
                        btnUpdateSensorSelection.Enabled = false;
                        btnCancelSensorSelection.Enabled = false;

                        cmbCOMPorts.Enabled = false;
                        btnRefreshCOMPorts.Enabled = false;
                        cmbConnectionBaudRate.Enabled = false;
                        btnConnect.Enabled = false;
                        btnDisconnect.Enabled = true;

                        cmbFrequency.Enabled = true;
                        btnUpdateOptions.Enabled = false;
                        btnCancelOptions.Enabled = false;

                        btnChooseDataFolder.Enabled = true;
                        numFiltering.Enabled = true;

                        numFactor.Enabled = true;
                        numDriftFactor.Enabled = true;

                        numGraphRefreshTime.Enabled = true;
                        chkEnableAveraging.Enabled = true;
                        numAveragingNumber.Enabled = chkEnableAveraging.Checked;
                        cmbScaleMode.Enabled = true;
                        numVerticalRange.Enabled = (cmbScaleMode.SelectedItem.ToString() == "Manual");
                        
                        btnStart.Enabled = true;
                        btnStop.Enabled = false;
                        // btnSaveData.Enabled = true;
                        chkAutoSaveData.Enabled = true;

                        txtReading.Text = "Choose graph settings";
                        zgcData.Enabled = true;

                        btnZeroReadings.Enabled = true;
                        btnResetOffset.Enabled = true;
                        numReferenceValue.Enabled = true;
                        chkInvertGraph.Enabled = true;
                        break;

                    case States.MEASUREMENT_RUNNING:
                        cmbSensorModel.Enabled = false;
                        cmbSensorType.Enabled = false;
                        btnUpdateSensorSelection.Enabled = false;
                        btnCancelSensorSelection.Enabled = false;

                        cmbCOMPorts.Enabled = false;
                        btnRefreshCOMPorts.Enabled = false;
                        cmbConnectionBaudRate.Enabled = false;
                        btnConnect.Enabled = false;
                        btnDisconnect.Enabled = false;

                        cmbFrequency.Enabled = false;
                        btnUpdateOptions.Enabled = false;
                        btnCancelOptions.Enabled = false;

                        btnChooseDataFolder.Enabled = false;
                        numFiltering.Enabled = true;

                        numFactor.Enabled = false;
                        numDriftFactor.Enabled = false;

                        numGraphRefreshTime.Enabled = false;
                        chkEnableAveraging.Enabled = false;
                        numAveragingNumber.Enabled = false;
                        cmbScaleMode.Enabled = false;
                        numVerticalRange.Enabled = false;
                        
                        btnStart.Enabled = false;
                        btnStop.Enabled = true;
                        // btnSaveData.Enabled = true;
                        chkAutoSaveData.Enabled = true;

                        txtReading.Text = "";
                        zgcData.Enabled = true;

                        btnZeroReadings.Enabled = true;
                        btnResetOffset.Enabled = true;
                        numReferenceValue.Enabled = true;
                        chkInvertGraph.Enabled = true;
                        break;

                    default:
                        throw new ApplicationException("Tried to set invalid state");
                }

                state = value;
            }
        }

        #endregion      
        
        #region Update Functions

        #region Sensor Selection

        /// <summary>
        /// Updates the models in sensor model list
        /// </summary>
        private void UpdateSensorModels()
        {
            cmbSensorModel.Items.Clear();

            // add the sensor models
            foreach (Sensor s in sensors)
            {
                cmbSensorModel.Items.Add(s.Properties.ModelName);
            }

            cmbSensorModel.SelectedIndex = 0;
            currentSelectedSensorModel = 0;
        }

        /// <summary>
        /// Updates the types in sensor type list
        /// </summary>
        private void UpdateSensorTypes()
        {
            cmbSensorType.Items.Clear();

            // get selected sensor
            Sensor selectedSensor = sensors[cmbSensorModel.SelectedIndex];

            // add the sensor types
            foreach (string type in selectedSensor.SensorTypes)
            {
                cmbSensorType.Items.Add(type);
            }

            cmbSensorType.SelectedIndex = 0;
            currentSelectedSensorType = 0;
        }

        #endregion
        
        #region Selecting COM Port

        /// <summary>
        /// Updates COM ports available
        /// </summary>
        private void UpdateCOMPorts()
        {
            string[] ports = SerialPort.GetPortNames();
            Array.Sort(ports);

            cmbCOMPorts.Items.Clear();
            cmbCOMPorts.Items.AddRange(ports);
        }

        /// <summary>
        /// Updates the list of baud rates for the connection
        /// </summary>
        private void UpdateConnectionBaudRates()
        {
            cmbConnectionBaudRate.Items.Clear();
            cmbConnectionBaudRate.Items.Add("Auto detect");

            foreach (int i in sensor.BaudRates)
            {
                cmbConnectionBaudRate.Items.Add(i.ToString());
            }
        }

        #endregion        

        #region Sensor Parameters

        /// <summary>
        /// Updates the frequency selection list
        /// </summary>
        private void UpdateFrequencies()
        {
            cmbFrequency.Items.Clear();
            cmbFrequency.Items.AddRange(sensor.Frequencies);
        }

        /// <summary>
        /// Loads current values of parameters and sets them in form (or uses defaults)
        /// </summary>
        private void ResetSensorOptions()
        {
            // update the frequency list
            UpdateFrequencies();

            // set selected item
            double frequency = Convert.ToDouble(sensor.Parameters["frequency"].Get());
            for (int i = 0; i < sensor.Frequencies.Length; i++)
            {
                if (Convert.ToDouble(sensor.Frequencies[i]) == frequency)
                {
                    cmbFrequency.SelectedIndex = i;
                    break;
                }
            }

            // update averaging stuff
            chkEnableAveraging.Checked = enableAveraging;
            numAveragingNumber.Value = averagingNumber;
            numAveragingNumber.Enabled = enableAveraging;

            // update filtering options/refresh rate
            numFiltering.Value = filteringNumber;
            numGraphRefreshTime.Value = graphRefreshTime;

            // disable buttons
            btnCancelOptions.Enabled = false;
            btnUpdateOptions.Enabled = false;
        }

        #endregion
        
        #region Closing Form

        /// <summary>
        /// Should be called before closing the form
        /// </summary>
        private void BeforeCloseForm()
        {
            // stop any running measurments
            if (State == States.MEASUREMENT_RUNNING)
            {
                StopMeasurements();
            }

            // disconnect from any open ports
            if (State == States.PORT_CONNECTED)
            {
                DisconnectCOMPort();
            }
        }

        #endregion
        
        #region Connection/Disconnection

        /// <summary>
        /// Connects to COM port and updates form
        /// </summary>
        private void ConnectCOMPort()
        {
            // disable connect button
            btnConnect.Enabled = false;
            
            try
            {
                output.WriteLine("Connecting to sensor ...", OutputManager.Level.INFO);

                // connect to port
                string portName = cmbCOMPorts.SelectedItem.ToString();
                if (cmbConnectionBaudRate.SelectedIndex == 0)
                {
                    // auto-detect baud rate
                    sensor.Connect(portName);

                    // update selected item
                    cmbConnectionBaudRate.SelectedItem = sensor.BaudRate.ToString();

                    output.WriteLine("Determined baud rate: " + sensor.BaudRate.ToString(), OutputManager.Level.SUCCESS);
                }
                else
                {
                    // find selected baud rate
                    int baudRate = Convert.ToInt32(cmbConnectionBaudRate.SelectedItem);
                    sensor.Connect(portName, baudRate);
                }
            }
            catch (Exception ex)
            {
                string message = "There was an error connecting to the sensor. Try changing the COM port or baud rate. The sensor responded '" + ex.Message + "'";
                output.WriteLine(message, OutputManager.Level.ERROR);
                
                // disconnect if connected
                sensor.Disconnect();

                // enable connect button
                btnConnect.Enabled = true;

                return;
            }

            // update graph with sensor information, ie: scales
            UpdateGraphSensorInfo();

            output.WriteLine("Connected to sensor", OutputManager.Level.SUCCESS);

            // change form state
            State = States.PORT_CONNECTED;

            sensor.ReadingReference = ReadingReferences.SMR;

            // reset sensor parameters
            ResetSensorOptions();
        }

        /// <summary>
        /// Disconnects from COM port and updates form
        /// </summary>
        private void DisconnectCOMPort()
        {
            try
            {
                // disconnect from sensor
                sensor.Disconnect();
                output.WriteLine("Disconnected from sensor", OutputManager.Level.SUCCESS);
            }
            catch (Exception ex)
            {
                output.WriteLine("Could not disconnect from sensor properly: " + ex.Message);
            }

            // change state of form
            State = States.PORT_DISCONNECTED;

            // update selected baud rate
            cmbConnectionBaudRate.SelectedItem = sensor.Parameters["baudrate"];
        }

        #endregion

        #region Graph Updates
        
        /// <summary>
        /// Stores initial positions of some stuff to use when resizing the form
        /// </summary>
        private void UpdateGraphPosition()
        {
            graphPosition = new int[4];
            graphPosition[0] = zgcData.Location.X;
            graphPosition[1] = zgcData.Location.Y;
            graphPosition[2] = ClientSize.Width - (zgcData.Location.X + zgcData.Size.Width);
            graphPosition[3] = ClientSize.Height - (zgcData.Location.Y + zgcData.Size.Height);

            rightColPosition = new int[] { ClientSize.Width - pnlRightCol.Location.X };
        }

        /// <summary>
        /// Handles scaling graph when resizing form
        /// </summary>
        private void ResizeGraph()
        {
            if (graphPosition == null)
                return;

            zgcData.Location = new Point(graphPosition[0], graphPosition[1]);
            zgcData.Size = new Size(ClientSize.Width - graphPosition[2] - graphPosition[0], ClientSize.Height - graphPosition[3] - graphPosition[1]);

            pnlRightCol.Location = new Point(ClientSize.Width - rightColPosition[0], pnlRightCol.Location.Y);
        }

        /// <summary>
        /// Initialises graph once
        /// </summary>
        private void CreateGraph()
        {
            log.Write("Creating graph");

            GraphPane pane = zgcData.GraphPane;

            // empty list for now
            RollingPointPairList list = new RollingPointPairList(1);
            
            // add the curve
            LineItem curve = pane.AddCurve("distance", list, Color.Black, SymbolType.None);
            
            // hide legend
            curve.Label.IsVisible = false;
            
            // set axis labels
            pane.Title.Text = "Distance";
            pane.XAxis.Title.Text = "Time (s)";
            pane.YAxis.Title.Text = "Distance (microns)";
            
            // reset graph to initial state
            ResetGraphData();
            ResetHorizontalAxis();
        }

        /// <summary>
        /// Updates graph once sensor is chosen
        /// </summary>
        private void UpdateGraphSensorInfo()
        {
            log.Write("Updating sensor info");

            zgcData.GraphPane.Title.Text = "Readings from " + sensor.Properties.ModelName;

            UpdateVerticalAxis();
        }

        /// <summary>
        /// Updates the scales on the vertical axis when zoomed
        /// </summary>
        private void UpdateVerticalAxis()
        {
            if (log != null) log.Write("Updating vertical axis");

            GraphPane pane = zgcData.GraphPane;

            pane.Title.Text = "Readings from " + sensor.Properties.ModelName;
            
            // scale y axis
            Scale yScale = pane.YAxis.Scale;

            double reference = (double)numReferenceValue.Value; // in microns

            if (log != null)
            {
                log.Write("\tScale Mode: " + cmbScaleMode.SelectedItem.ToString());
                log.Write("\tHas Offset: " + hasOffset.ToString());
            }

            double factor = (double)numFactor.Value;

            // if no offset or in "full" mode
            if (cmbScaleMode.SelectedItem.ToString() == "Full" || (!hasOffset && cmbScaleMode.SelectedItem.ToString() == "Manual"))
            {
                // full mode

                double grace = sensor.Properties.MR * 0.05;

                // TODO: removed reference from here

                if (!invertReadings)
                {
                    // set scales in microns - handles both inverted/non-inverted as Z changes
                    yScale.Min = Math.Round((-grace - sensor.ZeroOffset) * 1000, 0) * factor;
                    yScale.Max = Math.Round((+grace - sensor.ZeroOffset + sensor.Properties.MR) * 1000, 0) * factor;
                }
                else
                {
                    // if inverted
                    yScale.Min = Math.Round((-grace + sensor.ZeroOffset) * 1000, 0) * factor;
                    yScale.Max = Math.Round((+grace + sensor.ZeroOffset + sensor.Properties.MR) * 1000, 0) * factor;
                }
            }
            else if (cmbScaleMode.SelectedItem.ToString() == "Manual")
            {
                // offset and manual mode
                double range = (double)numVerticalRange.Value;
                
                // set scales in microns
                yScale.Min = reference - range / 2;
                yScale.Max = reference + range / 2;

                //// NO FACTOR ABOVE
            }
            else
            {
                // auto mode
                yScale.MinGrace = 0.05;
                yScale.MaxGrace = 0.05;

                yScale.MinAuto = true;
                yScale.MaxAuto = true;
                yScale.MajorStepAuto = true;
                yScale.MinorStepAuto = true;
            }
            
            pane.AxisChange();
        }

        /// <summary>
        /// Adds next point to graph
        /// </summary>
        private void GraphUpdateTick()
        {
            // get curve
            LineItem curve = zgcData.GraphPane.CurveList[0] as LineItem;
            
            // get point list
            IPointListEdit list = curve.Points as IPointListEdit;
            
            // if null, indicates non-editable
            if (list == null)
                return;

            // get readings
            SensorReading[] readings = sensor.TakeReadings();

            double value = -1.0, averagedValue = -1.0, time = -1.0;
            int count = -1;
            double driftValue = 0;

            // when to update display - will be at least 1
            // so update every nth reading
            int displayUpdateRate = (int)Math.Ceiling(500.0 / graphRefreshTime);

            // go through each reading
            for (int i = 0; i < readings.Length; i += 1)
            {
                time = readings[i].Time;

                if (!readings[i].IsError)
                {
                    // now in micron
                    driftValue = System.DateTime.Now.Subtract(referenceTime).TotalMinutes * driftFactor;

                    // TODO:
                    // convert from mm to microns
                    // this is F(V - Z) + D = F(V - (V0-R)) + D = F(R + (V-V0)) + D
                    value = readings[i].Value * 1000 * (double)numFactor.Value + driftValue;

                    if (invertReadings)
                    {
                        // this is F*MR - [F(V-Z) + D] = F(MR - (V-Z)) - D = F(MR - (V-(V0 + R - MR))) - D = F(MR - V + V0 + R - MR) = F(R - (V-V0)) - D
                        value = sensor.Properties.MR * 1000 * (double)numFactor.Value - value;
                    }

                    // for non-averaging
                    if (!enableAveraging)
                    {
                        list.Add(time, value);
                        readingsFileBuffer.Add(new SensorReading(value, time));
                    }
                    else
                    {
                        // keep track for moving average
                        runningSum += value;
                        // add this value to moving average buffer
                        readingsBuffer.Add(value);
                        // the length of the averaging buffer
                        count = readingsBuffer.Count;

                        if (count > averagingNumber)
                        {
                            // so this is sum of last n elements only
                            runningSum -= (double)readingsBuffer[count - averagingNumber - 1];
                        }

                        // if buffer is full 
                        if (count >= averagingNumber)
                        {
                            averagedValue = runningSum / averagingNumber;

                            // graph point
                            list.Add(time, averagedValue);

                            // store point in file buffer
                            readingsFileBuffer.Add(new SensorReading(averagedValue, time));
                        }
                    }
                }
                
                // update display every 500 ms
                if (i % displayUpdateRate == 0)
                {
                    if (readings[i].IsError)
                    {
                        // display error message
                        txtReading.Text = "Error";
                    }
                    else if (enableAveraging)
                    {
                        if (count > averagingNumber)
                        {
                            // round to nearest 0.2
                            double v = Math.Round(averagedValue * 5) / 5;
                            string s = v.ToString();

                            if (v == Math.Round(v))
                            {
                                s += ".0";
                            }

                            // add a positive sign to positive numbers
                            if (s[0] != '-')
                            {
                                s = "+" + s;
                            }

                            // display averaged value
                            txtReading.Text = s;
                        }
                        else
                        {
                            txtReading.Text = "Building buffer";
                        }
                    }
                    else
                    {
                        // display actual value
                        txtReading.Text = value.ToString();
                    }
                }
            }

            // TODO:

            // roll x-axis - keep it with 1 major step between max x-value and end of axis
            Scale xScale = zgcData.GraphPane.XAxis.Scale;
            if (time > xScale.Max - xScale.MajorStep)
            {
                xScale.Max = time + xScale.MajorStep;
                xScale.Min = xScale.Max - 15.0;
            }

            // update axes
            zgcData.AxisChange();

            // force redraw
            zgcData.Invalidate();

            // enable data save button
            btnSaveData.Enabled = true;
        }

        /// <summary>
        /// Resets graph's axes and clears data
        /// </summary>
        private void ResetGraphData()
        {
            GraphPane pane = zgcData.GraphPane;

            if (log != null) log.Write("Resetting graph data");

            // clear data
            LineItem curve = pane.CurveList[0] as LineItem;
            IPointListEdit list = curve.Points as IPointListEdit;
            if (list != null)
                list.Clear();

            // redraw
            zgcData.AxisChange();
            zgcData.Invalidate();
        }

        /// <summary>
        /// Resets the horizontal scale to 0
        /// </summary>
        private void ResetHorizontalAxis()
        {
            GraphPane pane = zgcData.GraphPane;

            // TODO:

            // reset x-axis
            pane.XAxis.Scale.Min = 0;
            pane.XAxis.Scale.Max = 15.0;
            pane.XAxis.Scale.MinorStep = 1;
            pane.XAxis.Scale.MajorStep = 2;

            if (log != null) log.Write("Resetting horizontal axis");
        }

        #endregion

        #region Data Saving

        /// <summary>
        /// Saves any data buffered
        /// </summary>
        private bool DataSave()
        {
            bool savedData = false;

            log.Write("Saving data: " + System.DateTime.Now.ToString());

            int count = readingsFileBuffer.Count;
            
            // save if there is some data, need to leave at least averaging number entries if data acquisition is still running
            if (count > filteringNumber)
            {
                output.WriteLine("Saving data ...", OutputManager.Level.INFO);

                // number of data points to save
                int data_count = count;
                                
                // account for filtering, integer division
                data_count /= filteringNumber;

                // we have some data to save, select it
                SensorReading[] data = new SensorReading[data_count];
                lock (readingsFileBuffer)
                {
                    for (int i = 0; i < data_count; i++)
                    {
                        // transfer readings to data
                        data[i] = (SensorReading)readingsFileBuffer[filteringNumber * i];
                    }

                    // remove from buffer
                    readingsFileBuffer.RemoveRange(0, data_count * filteringNumber);
                }

                // now write data to file
                WriteToFile(data, "Moving_Average_[xxxx].csv");

                savedData = true;
            }

            btnSaveData.Enabled = false;

            return savedData;
        }

        /// <summary>
        /// Writes results to filename
        /// </summary>
        /// <param name="results"></param>
        /// <param name="filename"></param>
        private void WriteToFile(SensorReading[] results, string filename)
        {
            // get folder, remove slashes
            string folderPath = txtDataFolder.Text.TrimEnd(new char[] { '\\', '/' }); ;

            // process the filename
            filename = ProcessFilename(filename, folderPath);

            // now open file
            StreamWriter sw;
            try
            {
                sw = new StreamWriter(folderPath + "\\" + filename, false);
                // write header
                sw.WriteLine(System.DateTime.Now.ToString());
                sw.WriteLine("Sensor," + sensor.Properties.ModelName);
                if (enableAveraging)
                    sw.WriteLine("Averaging," + averagingNumber.ToString());
                else
                    sw.WriteLine("Averaging,1");
                sw.WriteLine("Frequency," + cmbFrequency.SelectedItem.ToString());
                sw.WriteLine("Data filtering," + numFiltering.Value.ToString());
                sw.WriteLine("Time (ms),Reading (microns)");

                string error, line;
                for (int i = 0; i < results.Length; i++)
                {
                    if (results[i].IsError)
                    {
                        error = SensorReading.ReadingErrorMessages[results[i].Error];
                        line = "," + error;
                        sw.WriteLine(line);
                        // don't write
                    }
                    else
                    {
                        line = results[i].Time.ToString() + "," + results[i].Value.ToString();
                        sw.WriteLine(line);
                    }
                }

                sw.Flush();
                sw.Close();

                output.WriteLine("Saved data to " + folderPath + "\\" + filename, OutputManager.Level.SUCCESS);
                log.Write("Saved data to " + folderPath + "\\" + filename);
            }
            catch (Exception ex)
            {
                output.WriteLine("Could not write to file: " + folderPath + "\\" + filename + ". Error message was: " + ex.Message, OutputManager.Level.ERROR);
                log.Write("Could not save data to " + folderPath + "\\" + filename);
            }
        }

        /// <summary>
        /// Replaces [xxxx]'s with an appropriate index, folder should have no trailing slash
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="folder"></param>
        /// <returns></returns>
        public static string ProcessFilename(string filename, string folder)
        {
            Regex r1 = new Regex(@"\[(x+)\]");
            Match m1 = r1.Match(filename);

            // if no need to substitute stuff
            if (!m1.Success) return filename;

            // filter to use when searching for files
            string fileFilter = r1.Replace(filename, "*");

            // find file to write to
            string[] files = Directory.GetFiles(folder + "\\", fileFilter);

            // number of x's
            int digitCount = m1.Groups[1].Length;

            // index stores highest data_xxxx.csv that exists already
            int index = 0;

            Regex r2 = new Regex(r1.Replace(filename, @"(\d+)"));
            Match m2;
            int newIndex;
            foreach (string file in files)
            {
                m2 = r2.Match(file);
                if (m2.Success)
                {
                    newIndex = Convert.ToInt32(m2.Groups[1].ToString());
                    if (newIndex > index)
                    {
                        index = newIndex;
                    }
                }
            }

            // now use index + 1
            filename = r1.Replace(filename, (index + 1).ToString().PadLeft(digitCount, '0'));

            return filename;
        }

        #endregion

        #region Measurements

        /// <summary>
        /// Prepares measurements to be taken, returns success. Validates and makes sure readings are initialised
        /// </summary>
        private bool PrepareMeasurements()
        {
            // cancel if timer is running
            if (timerGraph.Enabled) return false;

            if (btnCancelOptions.Enabled)
            {
                MessageBox.Show("Please save or cancel your changes to the sensor parameters first");
                output.WriteLine("There are unsaved changes to the sensor parameters", OutputManager.Level.ERROR);
                return false;
            }
            
            if (txtDataFolder.Text == "" || !Directory.Exists(txtDataFolder.Text))
            {
                MessageBox.Show("Please select a valid data location");
                output.WriteLine("No valid data location selected", OutputManager.Level.ERROR);
                return false;
            }

            log.Write("Preparing graph: " + System.DateTime.Now.ToString());

            // save settings
            enableAveraging = chkEnableAveraging.Checked;
            averagingNumber = (int)numAveragingNumber.Value;
            graphRefreshTime = (int)numGraphRefreshTime.Value;
            // unnecessary as event does it - no harm though
            filteringNumber = (int)numFiltering.Value;

            // in micron/min
            driftFactor = (double)numDriftFactor.Value;
            // store systime in startmeasurements

            // get frequency
            double frequency = Convert.ToDouble(sensor.Parameters["frequency"].Get());

            // set time increment
            sensor.TimeIncrement = 1.0 / frequency;

            // make sure reading init has been called - improves timing
            sensor.EnsureReadingsInitialised();

            // update timer interval
            timerGraph.Interval = graphRefreshTime;

            // save data automatically when buffer is 70% full
            timerSaveData.Interval = (int)(MovingAveraging_Form.dataRecordTime * frequency * 0.7);

            // update curve's list to make sure it is large enough
            zgcData.GraphPane.CurveList[0].Points = new RollingPointPairList((int)(MovingAveraging_Form.dataRecordTime * frequency));
                        
            // reset stuff
            // number of readings to remember to use for averaging
            readingsBuffer = new RollingArrayList();
            readingsBuffer.MaxCapacity = (averagingNumber + 20);

            // number of readings to keep in memory that could be saved to file
            readingsFileBuffer = new RollingArrayList();
            readingsFileBuffer.MaxCapacity = (int)(MovingAveraging_Form.dataRecordTime * frequency);

            // reset averaging
            runningSum = 0;
            
            // clear data, put x-axis back to 0
            //ResetGraphData();
            //ResetHorizontalAxis();

            // zero time
            //sensor.CurrentTime = 0;

            // update vertical axis - in case changed scale mode
            UpdateVerticalAxis();

            log.Write("Enable Averaging: " + enableAveraging.ToString());
            log.Write("Averaging Number: " + averagingNumber.ToString());
            log.Write("Refresh Time: " + graphRefreshTime.ToString());
            log.Write("Filtering Number: " + filteringNumber.ToString());
            log.Write("Drift Factor: " + driftFactor.ToString());
            log.Write("Frequency: " + frequency.ToString());
            log.Write("Save Data Interval: " + timerSaveData.Interval.ToString());

            txtReading.Font = new Font(FontFamily.GenericMonospace, 70); 

            return true;
        }

        /// <summary>
        /// Starts recording measurements to graph
        /// </summary>
        private void StartMeasurements()
        {
            // cancel if graph timer is already running
            if (timerGraph.Enabled) return;

            // set reference time for drift factor
            referenceTime = System.DateTime.Now;

            // change state
            State = States.MEASUREMENT_RUNNING;

            // clear buffers
            sensor.PortBuffer.Clear();
            readingsBuffer.Clear();
            runningSum = 0.0;

            // start serial port buffers autoupdating
            sensor.PortBuffer.AutoUpdate(100);

            // start timer
            timerGraph.Start();

            // start data save timer as well
            if (chkAutoSaveData.Checked)
                timerSaveData.Start();
        }
        
        /// <summary>
        /// Stops any running measurements
        /// </summary>
        private void StopMeasurements()
        {
            // stop graph updating
            timerGraph.Stop();
            timerSaveData.Stop();

            txtReading.Font = new Font(FontFamily.GenericSansSerif, 40);

            // stop auto updating buffers
            sensor.PortBuffer.StopAutoUpdate();

            // save data if autosave still
            if (chkAutoSaveData.Checked)
            {
                DataSave();
                // clear any residual data (ie: if count < filtering number, there will be some left)
                readingsFileBuffer.Clear();
            }

            // clear other buffers
            sensor.PortBuffer.Clear();
            readingsBuffer.Clear();
            runningSum = 0.0;

            // change state
            State = States.PORT_CONNECTED;

            log.Write("Stopped measurements: " + System.DateTime.Now.ToString());
            log.Write("");
            log.Flush();
        }

        #endregion

        #region Zeroing

        /// <summary>
        /// Sets current reading to the zero offset, and adjusts scales
        /// </summary>
        private void ZeroReadingOffset()
        {
            // NB: reading offset is in mm

            // disable button temporarily
            btnZeroReadings.Enabled = false;

            double zeroOffset = 0;

            output.WriteLine("Changing baseline", OutputManager.Level.INFO);

            log.Write("Zeroing reading");

            // update averaging number
            enableAveraging = chkEnableAveraging.Checked;
            averagingNumber = (int)numAveragingNumber.Value;

            log.Write("Enable Averaging: " + enableAveraging.ToString());
            log.Write("Averaging Number: " + averagingNumber.ToString());

            SensorReading reading;
            
            // clear sensor buffers to get accurate average
            sensor.PortBuffer.Clear();
            runningSum = 0.0;
            readingsBuffer.Clear();

            // need to get a new averaged reading
            if (!enableAveraging)
            {
                reading = sensor.TakeReading();
                reading.Value *= (double)numFactor.Value;
            }
            else
            {
                output.WriteLine("Analysing baseline", OutputManager.Level.INFO);
                txtReading.Text = "Analysing baseline";

                // take enough readings
                SensorReading[] readings = sensor.TakeReadings(averagingNumber);
                
                // take average
                // overwrite old running sum
                runningSum = 0.0;
                int count = 0;
                double value;
                foreach (SensorReading r in readings)
                {
                    if (!r.IsError)
                    {
                        //// DO NOT MULTIPLY BY FACTOR
                        value = r.Value;
                        // add new value
                        runningSum += value;
                        count += 1;
                        readingsBuffer.Add(value);
                    }
                }

                // if found some readings
                if (count > 0)
                {
                    log.Write("Found " + count.ToString() + " readings, looking for: " + averagingNumber.ToString() + ". Average is: " + (runningSum / count).ToString());
                    reading = new SensorReading(runningSum / count);
                }
                else
                {
                    log.Write("Could not get a valid reading, first error = " + readings[0].Error.ToString());
                    reading = new SensorReading(SensorReading.DigitalErrors.INVALID_READING);
                }
            }

            // clear blue box
            txtReading.Text = "";
            
            if (reading != null && !reading.IsError)
            {
                double factor = (double)numFactor.Value;
                // store offset reference in mm, divide by factor so later when multiplying by factor, it cancels out
                offsetReference = (double)numReferenceValue.Value / 1000 / factor;

                if (invertReadings)
                {
                    // then z = vo + r - mr
                    // use as zero offset, convert reference to mm
                    zeroOffset = reading.Value + offsetReference - sensor.Properties.MR;
                }
                else
                {
                    // then z = vo - r
                    zeroOffset = reading.Value - offsetReference;
                }

                // need to mark offset before SetOffset is called (which calls UpdateVerticalAxis)
                hasOffset = true; 

                // should be in mm as takereadings returns mm
                SetOffset(zeroOffset);
                
                output.WriteLine("Set baseline to " + (sensor.ZeroOffset * 1000).ToString() + " microns", OutputManager.Level.SUCCESS);
            }
            else
            {
                output.WriteLine("Could not get a valid reading to set the baseline", OutputManager.Level.ERROR);
            }
            
            btnZeroReadings.Enabled = true;
        }

        /// <summary>
        /// Sets offset to 0
        /// </summary>
        private void ResetReadingOffset()
        {
            SetOffset(-1 * sensor.ZeroOffset);
            hasOffset = false;
            output.WriteLine("Reset offset to 0", OutputManager.Level.SUCCESS);
        }

        /// <summary>
        /// Sets the offset to offset, and resets graph/buffers
        /// </summary>
        /// <param name="offset"></param>
        private void SetOffset(double zeroOffset)
        {
            // NB: reading offset is in mm

            log.Write("Setting offset from " + sensor.ZeroOffset.ToString() + " to " + (sensor.ZeroOffset + zeroOffset).ToString());

            // autosave if necessary
            if (chkAutoSaveData.Checked)
            {
                DataSave();
            }
            readingsFileBuffer.Clear();

            // adjust offset - add as the "zeroOffset" is reading - sensor.ZeroOffset
            sensor.ZeroOffset += zeroOffset;

            // zero time
            sensor.CurrentTime = 0;

            // reset reference time
            referenceTime = System.DateTime.Now;

            // clear buffers from when determining offset
            readingsBuffer.Clear();
            sensor.PortBuffer.Clear();
            runningSum = 0.0;
            
            // then reset graph
            UpdateVerticalAxis();
            ResetHorizontalAxis();
            ResetGraphData();
        }

        #endregion

        #region Invert Changed
        /// <summary>
        /// Handles when the invert state changes
        /// </summary>
        private void InvertReadingsChanged()
        {
            bool old = invertReadings;
            invertReadings = chkInvertGraph.Checked;
            
            // need to change zero offset value if invert state has changed
            if (hasOffset && old != invertReadings)
            {
                double factor = (double)numFactor.Value;
                double offsetChange = offsetReference + (double)numReferenceValue.Value / 1000 / factor - sensor.Properties.MR;
                // if now inverting
                if (invertReadings == true)
                {
                    // convert z from z = v0 - r_old to z = v0 + r_new - mr
                    sensor.ZeroOffset += offsetChange;
                }
                else
                {
                    // no restoring, convert from z = vo + r_old - mr to z = v0 - r
                    sensor.ZeroOffset -= offsetChange;
                }

                // update r_old
                offsetReference = (double)numReferenceValue.Value / 1000 / factor;
            }

            // if graph is made, then clear it
            if (State == States.PORT_CONNECTED || State == States.MEASUREMENT_RUNNING)
            {
                UpdateVerticalAxis();
            }
        }
        #endregion

        #endregion

        #region Event Handlers

        #region Form Closing

        void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        void MovingAveraging_Form_FormClosing(object sender, FormClosingEventArgs e)
        {
            BeforeCloseForm();
        }

        #endregion

        #region Sensor Selection

        void cmbSensorModel_SelectedIndexChanged(object sender, EventArgs e)
        {
            // when sensor model changes
            if (cmbSensorModel.SelectedIndex != currentSelectedSensorModel)
            {
                btnUpdateSensorSelection.Enabled = true;
                btnCancelSensorSelection.Enabled = true;
            }

            UpdateSensorTypes();                
        }

        void cmbSensorType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbSensorType.SelectedIndex != currentSelectedSensorType)
            {
                btnUpdateSensorSelection.Enabled = true;
                btnCancelSensorSelection.Enabled = true;
            }
        }

        void btnUpdateSensorSelection_Click(object sender, EventArgs e)
        {
            // when update sensor button is clicked
            // update sensor
            sensor = sensors[cmbSensorModel.SelectedIndex];

            // update currently selected model/type
            currentSelectedSensorModel = cmbSensorModel.SelectedIndex;
            currentSelectedSensorType = cmbSensorType.SelectedIndex;

            // update state
            State = States.PORT_DISCONNECTED;

            // initialise connection
            UpdateCOMPorts();
            UpdateConnectionBaudRates();

            cmbCOMPorts.SelectedIndex = 0;
            cmbConnectionBaudRate.SelectedIndex = 0;

            // focus next field
            cmbCOMPorts.Focus();
        }

        void btnCancelSensorSelection_Click(object sender, EventArgs e)
        {
            // when cancel sensor change button is clicked

            // reset combo boxes
            cmbSensorModel.SelectedIndex = currentSelectedSensorModel;
            cmbSensorType.SelectedIndex = currentSelectedSensorType;

            btnCancelSensorSelection.Enabled = false;

            // focus next field
            cmbSensorModel.Focus();
        }

        #endregion

        #region COM Port Selection

        void btnRefreshCOMPorts_Click(object sender, EventArgs e)
        {
            UpdateCOMPorts();
            cmbCOMPorts.SelectedIndex = 0;
            cmbCOMPorts.Focus();
        }

        void btnConnect_Click(object sender, EventArgs e)
        {
            ConnectCOMPort();
        }

        void btnDisconnect_Click(object sender, EventArgs e)
        {
            // make sure nothing is running
            if (State == States.MEASUREMENT_RUNNING)
            {
                output.WriteLine("Can not disconnect while a process is running. Click Stop to force it to stop", OutputManager.Level.ERROR);
                return;
            }

            DisconnectCOMPort();
        }

        #endregion

        #region Sensor Parameters

        void cmbSensorOption_Changed(object sender, EventArgs e)
        {
            btnCancelOptions.Enabled = true;
            btnUpdateOptions.Enabled = true;
        }        

        void btnUpdateOptions_Click(object sender, EventArgs e)
        {
            output.WriteLine("Updating sensor properties ...", OutputManager.Level.INFO);

            btnUpdateOptions.Enabled = false;
            btnCancelOptions.Enabled = false;

            try
            {
                sensor.Parameters["frequency"].Set(cmbFrequency.SelectedItem.ToString());
            }
            catch (Exception ex)
            {
                output.WriteLine("Error updating sensor properties: " + ex.Message, OutputManager.Level.ERROR);

                btnUpdateOptions.Enabled = true;
                btnCancelOptions.Enabled = true;
            }

            output.WriteLine("Updated sensor properties", OutputManager.Level.SUCCESS);
        }

        void btnCancelOptions_Click(object sender, EventArgs e)
        {
            ResetSensorOptions();
        }

        #endregion

        #region Graph Settings

        void chkEnableAveraging_CheckStateChanged(object sender, EventArgs e)
        {
            numAveragingNumber.Enabled = chkEnableAveraging.Checked;
        }
        
        void timerGraph_Tick(object sender, EventArgs e)
        {
            GraphUpdateTick();
        }

        void timerSaveData_Tick(object sender, EventArgs e)
        {
            DataSave();
        }

        void zgcData_ContextMenuBuilder(ZedGraphControl sender, ContextMenuStrip menuStrip, Point mousePt, ZedGraphControl.ContextMenuObjectState objState)
        {
            string[] tagsToRemove = new string[] { "set_default", "unzoom", "undo_all" };
            foreach (string tagToRemove in tagsToRemove)
            {
                foreach (ToolStripMenuItem item in menuStrip.Items)
                {
                    string tag = item.Tag.ToString();
                    if (tag == tagToRemove)
                    {
                        menuStrip.Items.Remove(item);
                        break;
                    }
                }
            }
        }

        void cmbScaleMode_SelectedIndexChanged(object sender, EventArgs e)
        {
            numVerticalRange.Enabled = (cmbScaleMode.SelectedItem.ToString() == "Manual");
        }

        #endregion

        #region Output Panel

        void btnSaveOutput_Click(object sender, EventArgs e)
        {
            saveFile.ShowDialog();
            
            if (saveFile.FileName != "")
            {
                // save file
                try
                {
                    // write output, overwriting any files
                    StreamWriter sw = new StreamWriter(saveFile.FileName, false);
                    sw.Write(output.GetText());
                    sw.Flush();
                    sw.Close();

                    output.WriteLine("Output successfully saved to " + saveFile.FileName);
                }
                catch
                {
                    output.WriteLine("Error saving output", OutputManager.Level.INFO);
                }

            }
        }

        void btnClearOutput_Click(object sender, EventArgs e)
        {
            output.Clear();
        }

        #endregion
                
        #region DAQ Settings

        void btnChooseDataFolder_Click(object sender, EventArgs e)
        {
            folderBrowser.ShowDialog();

            if (folderBrowser.SelectedPath != "")
            {
                txtDataFolder.Text = folderBrowser.SelectedPath;
            }

            output.WriteLine("Set data folder to " + txtDataFolder.Text, OutputManager.Level.SUCCESS);
        }

        // needs to be separate as can be changed while measurement is running
        void numFiltering_ValueChanged(object sender, EventArgs e)
        {
            filteringNumber = (int)numFiltering.Value;
        }

        #endregion
        
        #region Form Events (for graph), and timer events

        void MovingAveraging_Form_Resize(object sender, EventArgs e)
        {
            ResizeGraph();
        }

        void MovingAveraging_Form_Load(object sender, EventArgs e)
        {
            CreateGraph();
            UpdateGraphPosition();
        }

        #endregion
        
        #region Measurement Buttons

        void chkAutoSaveData_Click(object sender, EventArgs e)
        {
            if (chkAutoSaveData.Checked)
            {
                // run it only if currently running measurements
                if (State == States.MEASUREMENT_RUNNING)
                {
                    DataSave();
                    timerSaveData.Start();
                }
            }
            else
            {
                timerSaveData.Stop();
            }
        }

        void btnSaveData_Click(object sender, EventArgs e)
        {
            bool response = DataSave();
            
            // in here as this is only time we care if DataSave fails
            if (!response)
            {
                output.WriteLine("No data to save", OutputManager.Level.ERROR);
            }
        }

        void btnStart_Click(object sender, EventArgs e)
        {
            if (PrepareMeasurements())
            {
                StartMeasurements();
            }
        }

        void btnStop_Click(object sender, EventArgs e)
        {
            StopMeasurements();
        }

        #endregion

        #region Zeroing

        void btnZeroReadings_Click(object sender, EventArgs e)
        {
            ZeroReadingOffset();
        }

        void btnResetOffset_Click(object sender, EventArgs e)
        {
            ResetReadingOffset();
        }


        #endregion

        void chkInvertGraph_CheckedChanged(object sender, EventArgs e)
        {
            if (invertReadings != chkInvertGraph.Checked)
            {
                InvertReadingsChanged();
            }
        }
        
        #endregion
    }
}
