﻿namespace MovingAveraging
{
    partial class MovingAveraging_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.GroupBox grpOutput;
            System.Windows.Forms.GroupBox grpConnection;
            System.Windows.Forms.Label lblConnectionBaudRate;
            System.Windows.Forms.Label lblCOMPort;
            System.Windows.Forms.Label lblHeading;
            System.Windows.Forms.GroupBox grpSensorSelection;
            System.Windows.Forms.Label lblSensorType;
            System.Windows.Forms.Label lblSensorModel;
            System.Windows.Forms.GroupBox grpSensorParameters;
            System.Windows.Forms.Label lblFrequency;
            System.Windows.Forms.Label lblAveragingNumber;
            System.Windows.Forms.GroupBox grpAcquisitionSettings;
            System.Windows.Forms.Label lblFiltering;
            System.Windows.Forms.Label lblChooseDataFolder;
            System.Windows.Forms.Label lblGraphRefreshTime;
            System.Windows.Forms.GroupBox grpMeasurements;
            System.Windows.Forms.GroupBox grpZeroing;
            System.Windows.Forms.Label lblReferenceValue;
            System.Windows.Forms.GroupBox grpInputs;
            System.Windows.Forms.Label lblScaleMode;
            System.Windows.Forms.Label lblVerticalRange;
            System.Windows.Forms.Label lblFactor;
            System.Windows.Forms.Label lblDriftFactor;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MovingAveraging_Form));
            this.txtOutput = new System.Windows.Forms.RichTextBox();
            this.btnSaveOutput = new System.Windows.Forms.Button();
            this.btnClearOutput = new System.Windows.Forms.Button();
            this.btnDisconnect = new System.Windows.Forms.Button();
            this.btnConnect = new System.Windows.Forms.Button();
            this.cmbConnectionBaudRate = new System.Windows.Forms.ComboBox();
            this.btnRefreshCOMPorts = new System.Windows.Forms.Button();
            this.cmbCOMPorts = new System.Windows.Forms.ComboBox();
            this.btnCancelSensorSelection = new System.Windows.Forms.Button();
            this.btnUpdateSensorSelection = new System.Windows.Forms.Button();
            this.cmbSensorType = new System.Windows.Forms.ComboBox();
            this.cmbSensorModel = new System.Windows.Forms.ComboBox();
            this.btnCancelOptions = new System.Windows.Forms.Button();
            this.btnUpdateOptions = new System.Windows.Forms.Button();
            this.cmbFrequency = new System.Windows.Forms.ComboBox();
            this.numFiltering = new System.Windows.Forms.NumericUpDown();
            this.btnChooseDataFolder = new System.Windows.Forms.Button();
            this.txtDataFolder = new System.Windows.Forms.TextBox();
            this.chkAutoSaveData = new System.Windows.Forms.CheckBox();
            this.btnSaveData = new System.Windows.Forms.Button();
            this.btnStop = new System.Windows.Forms.Button();
            this.btnStart = new System.Windows.Forms.Button();
            this.chkInvertGraph = new System.Windows.Forms.CheckBox();
            this.numReferenceValue = new System.Windows.Forms.NumericUpDown();
            this.btnResetOffset = new System.Windows.Forms.Button();
            this.btnZeroReadings = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.label3 = new System.Windows.Forms.Label();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.pnlRightCol = new System.Windows.Forms.Panel();
            this.numDriftFactor = new System.Windows.Forms.NumericUpDown();
            this.numFactor = new System.Windows.Forms.NumericUpDown();
            this.grpGraphSettings = new System.Windows.Forms.GroupBox();
            this.cmbScaleMode = new System.Windows.Forms.ComboBox();
            this.numVerticalRange = new System.Windows.Forms.NumericUpDown();
            this.numAveragingNumber = new System.Windows.Forms.NumericUpDown();
            this.numGraphRefreshTime = new System.Windows.Forms.NumericUpDown();
            this.chkEnableAveraging = new System.Windows.Forms.CheckBox();
            this.btnClose = new System.Windows.Forms.Button();
            this.folderBrowser = new System.Windows.Forms.FolderBrowserDialog();
            this.saveFile = new System.Windows.Forms.SaveFileDialog();
            this.zgcData = new ZedGraph.ZedGraphControl();
            this.txtReading = new System.Windows.Forms.TextBox();
            this.timerGraph = new System.Windows.Forms.Timer(this.components);
            this.timerSaveData = new System.Windows.Forms.Timer(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            grpOutput = new System.Windows.Forms.GroupBox();
            grpConnection = new System.Windows.Forms.GroupBox();
            lblConnectionBaudRate = new System.Windows.Forms.Label();
            lblCOMPort = new System.Windows.Forms.Label();
            lblHeading = new System.Windows.Forms.Label();
            grpSensorSelection = new System.Windows.Forms.GroupBox();
            lblSensorType = new System.Windows.Forms.Label();
            lblSensorModel = new System.Windows.Forms.Label();
            grpSensorParameters = new System.Windows.Forms.GroupBox();
            lblFrequency = new System.Windows.Forms.Label();
            lblAveragingNumber = new System.Windows.Forms.Label();
            grpAcquisitionSettings = new System.Windows.Forms.GroupBox();
            lblFiltering = new System.Windows.Forms.Label();
            lblChooseDataFolder = new System.Windows.Forms.Label();
            lblGraphRefreshTime = new System.Windows.Forms.Label();
            grpMeasurements = new System.Windows.Forms.GroupBox();
            grpZeroing = new System.Windows.Forms.GroupBox();
            lblReferenceValue = new System.Windows.Forms.Label();
            grpInputs = new System.Windows.Forms.GroupBox();
            lblScaleMode = new System.Windows.Forms.Label();
            lblVerticalRange = new System.Windows.Forms.Label();
            lblFactor = new System.Windows.Forms.Label();
            lblDriftFactor = new System.Windows.Forms.Label();
            grpOutput.SuspendLayout();
            grpConnection.SuspendLayout();
            grpSensorSelection.SuspendLayout();
            grpSensorParameters.SuspendLayout();
            grpAcquisitionSettings.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numFiltering)).BeginInit();
            grpMeasurements.SuspendLayout();
            grpZeroing.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numReferenceValue)).BeginInit();
            grpInputs.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.pnlRightCol.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numDriftFactor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numFactor)).BeginInit();
            this.grpGraphSettings.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numVerticalRange)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numAveragingNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numGraphRefreshTime)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpOutput
            // 
            grpOutput.Controls.Add(this.txtOutput);
            grpOutput.Controls.Add(this.btnSaveOutput);
            grpOutput.Controls.Add(this.btnClearOutput);
            grpOutput.Location = new System.Drawing.Point(15, 535);
            grpOutput.Name = "grpOutput";
            grpOutput.Size = new System.Drawing.Size(228, 97);
            grpOutput.TabIndex = 5;
            grpOutput.TabStop = false;
            grpOutput.Text = "Output/Status";
            // 
            // txtOutput
            // 
            this.txtOutput.DetectUrls = false;
            this.txtOutput.Location = new System.Drawing.Point(10, 19);
            this.txtOutput.Name = "txtOutput";
            this.txtOutput.ReadOnly = true;
            this.txtOutput.Size = new System.Drawing.Size(180, 45);
            this.txtOutput.TabIndex = 27;
            this.txtOutput.Text = "";
            // 
            // btnSaveOutput
            // 
            this.btnSaveOutput.Location = new System.Drawing.Point(14, 68);
            this.btnSaveOutput.Name = "btnSaveOutput";
            this.btnSaveOutput.Size = new System.Drawing.Size(75, 23);
            this.btnSaveOutput.TabIndex = 28;
            this.btnSaveOutput.Text = "Save to file";
            this.btnSaveOutput.UseVisualStyleBackColor = true;
            this.btnSaveOutput.Visible = false;
            // 
            // btnClearOutput
            // 
            this.btnClearOutput.Location = new System.Drawing.Point(115, 68);
            this.btnClearOutput.Name = "btnClearOutput";
            this.btnClearOutput.Size = new System.Drawing.Size(75, 23);
            this.btnClearOutput.TabIndex = 29;
            this.btnClearOutput.Text = "Clear";
            this.btnClearOutput.UseVisualStyleBackColor = true;
            this.btnClearOutput.Visible = false;
            // 
            // grpConnection
            // 
            grpConnection.Controls.Add(this.btnDisconnect);
            grpConnection.Controls.Add(this.btnConnect);
            grpConnection.Controls.Add(this.cmbConnectionBaudRate);
            grpConnection.Controls.Add(lblConnectionBaudRate);
            grpConnection.Controls.Add(this.btnRefreshCOMPorts);
            grpConnection.Controls.Add(this.cmbCOMPorts);
            grpConnection.Controls.Add(lblCOMPort);
            grpConnection.Location = new System.Drawing.Point(7, 309);
            grpConnection.Name = "grpConnection";
            grpConnection.Size = new System.Drawing.Size(219, 136);
            grpConnection.TabIndex = 6;
            grpConnection.TabStop = false;
            grpConnection.Text = "Connection";
            // 
            // btnDisconnect
            // 
            this.btnDisconnect.Location = new System.Drawing.Point(114, 92);
            this.btnDisconnect.Name = "btnDisconnect";
            this.btnDisconnect.Size = new System.Drawing.Size(75, 23);
            this.btnDisconnect.TabIndex = 9;
            this.btnDisconnect.Text = "Disconnect";
            this.btnDisconnect.UseVisualStyleBackColor = true;
            // 
            // btnConnect
            // 
            this.btnConnect.Location = new System.Drawing.Point(21, 92);
            this.btnConnect.Name = "btnConnect";
            this.btnConnect.Size = new System.Drawing.Size(75, 23);
            this.btnConnect.TabIndex = 8;
            this.btnConnect.Text = "Connect";
            this.btnConnect.UseVisualStyleBackColor = true;
            // 
            // cmbConnectionBaudRate
            // 
            this.cmbConnectionBaudRate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbConnectionBaudRate.FormattingEnabled = true;
            this.cmbConnectionBaudRate.Location = new System.Drawing.Point(78, 53);
            this.cmbConnectionBaudRate.Name = "cmbConnectionBaudRate";
            this.cmbConnectionBaudRate.Size = new System.Drawing.Size(121, 21);
            this.cmbConnectionBaudRate.TabIndex = 7;
            // 
            // lblConnectionBaudRate
            // 
            lblConnectionBaudRate.AutoSize = true;
            lblConnectionBaudRate.Location = new System.Drawing.Point(8, 56);
            lblConnectionBaudRate.Name = "lblConnectionBaudRate";
            lblConnectionBaudRate.Size = new System.Drawing.Size(61, 13);
            lblConnectionBaudRate.TabIndex = 3;
            lblConnectionBaudRate.Text = "Baud Rate:";
            // 
            // btnRefreshCOMPorts
            // 
            this.btnRefreshCOMPorts.Location = new System.Drawing.Point(150, 20);
            this.btnRefreshCOMPorts.Name = "btnRefreshCOMPorts";
            this.btnRefreshCOMPorts.Size = new System.Drawing.Size(63, 23);
            this.btnRefreshCOMPorts.TabIndex = 6;
            this.btnRefreshCOMPorts.Text = "Refresh";
            this.btnRefreshCOMPorts.UseVisualStyleBackColor = true;
            // 
            // cmbCOMPorts
            // 
            this.cmbCOMPorts.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCOMPorts.FormattingEnabled = true;
            this.cmbCOMPorts.Location = new System.Drawing.Point(69, 21);
            this.cmbCOMPorts.Name = "cmbCOMPorts";
            this.cmbCOMPorts.Size = new System.Drawing.Size(75, 21);
            this.cmbCOMPorts.TabIndex = 5;
            // 
            // lblCOMPort
            // 
            lblCOMPort.AutoSize = true;
            lblCOMPort.Location = new System.Drawing.Point(7, 25);
            lblCOMPort.Name = "lblCOMPort";
            lblCOMPort.Size = new System.Drawing.Size(56, 13);
            lblCOMPort.TabIndex = 0;
            lblCOMPort.Text = "COM Port:";
            // 
            // lblHeading
            // 
            lblHeading.Font = new System.Drawing.Font("Verdana", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            lblHeading.ForeColor = System.Drawing.Color.SteelBlue;
            lblHeading.Location = new System.Drawing.Point(17, 9);
            lblHeading.Name = "lblHeading";
            lblHeading.Size = new System.Drawing.Size(192, 100);
            lblHeading.TabIndex = 9;
            lblHeading.Text = "Wet Film Monitor - WFM1";
            lblHeading.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // grpSensorSelection
            // 
            grpSensorSelection.Controls.Add(this.btnCancelSensorSelection);
            grpSensorSelection.Controls.Add(this.btnUpdateSensorSelection);
            grpSensorSelection.Controls.Add(this.cmbSensorType);
            grpSensorSelection.Controls.Add(lblSensorType);
            grpSensorSelection.Controls.Add(this.cmbSensorModel);
            grpSensorSelection.Controls.Add(lblSensorModel);
            grpSensorSelection.Location = new System.Drawing.Point(7, 153);
            grpSensorSelection.Name = "grpSensorSelection";
            grpSensorSelection.Size = new System.Drawing.Size(219, 136);
            grpSensorSelection.TabIndex = 12;
            grpSensorSelection.TabStop = false;
            grpSensorSelection.Text = "Sensor Selection";
            // 
            // btnCancelSensorSelection
            // 
            this.btnCancelSensorSelection.Location = new System.Drawing.Point(114, 92);
            this.btnCancelSensorSelection.Name = "btnCancelSensorSelection";
            this.btnCancelSensorSelection.Size = new System.Drawing.Size(75, 23);
            this.btnCancelSensorSelection.TabIndex = 4;
            this.btnCancelSensorSelection.Text = "Cancel";
            this.btnCancelSensorSelection.UseVisualStyleBackColor = true;
            // 
            // btnUpdateSensorSelection
            // 
            this.btnUpdateSensorSelection.Location = new System.Drawing.Point(27, 92);
            this.btnUpdateSensorSelection.Name = "btnUpdateSensorSelection";
            this.btnUpdateSensorSelection.Size = new System.Drawing.Size(75, 23);
            this.btnUpdateSensorSelection.TabIndex = 3;
            this.btnUpdateSensorSelection.Text = "Select";
            this.btnUpdateSensorSelection.UseVisualStyleBackColor = true;
            // 
            // cmbSensorType
            // 
            this.cmbSensorType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSensorType.FormattingEnabled = true;
            this.cmbSensorType.Location = new System.Drawing.Point(87, 57);
            this.cmbSensorType.Name = "cmbSensorType";
            this.cmbSensorType.Size = new System.Drawing.Size(121, 21);
            this.cmbSensorType.TabIndex = 2;
            // 
            // lblSensorType
            // 
            lblSensorType.AutoSize = true;
            lblSensorType.Location = new System.Drawing.Point(6, 60);
            lblSensorType.Name = "lblSensorType";
            lblSensorType.Size = new System.Drawing.Size(34, 13);
            lblSensorType.TabIndex = 2;
            lblSensorType.Text = "Type:";
            // 
            // cmbSensorModel
            // 
            this.cmbSensorModel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSensorModel.FormattingEnabled = true;
            this.cmbSensorModel.Location = new System.Drawing.Point(87, 29);
            this.cmbSensorModel.Name = "cmbSensorModel";
            this.cmbSensorModel.Size = new System.Drawing.Size(121, 21);
            this.cmbSensorModel.TabIndex = 1;
            // 
            // lblSensorModel
            // 
            lblSensorModel.AutoSize = true;
            lblSensorModel.Location = new System.Drawing.Point(6, 32);
            lblSensorModel.Name = "lblSensorModel";
            lblSensorModel.Size = new System.Drawing.Size(75, 13);
            lblSensorModel.TabIndex = 0;
            lblSensorModel.Text = "Sensor Model:";
            // 
            // grpSensorParameters
            // 
            grpSensorParameters.Controls.Add(this.btnCancelOptions);
            grpSensorParameters.Controls.Add(this.btnUpdateOptions);
            grpSensorParameters.Controls.Add(this.cmbFrequency);
            grpSensorParameters.Controls.Add(lblFrequency);
            grpSensorParameters.Location = new System.Drawing.Point(7, 460);
            grpSensorParameters.Name = "grpSensorParameters";
            grpSensorParameters.Size = new System.Drawing.Size(213, 88);
            grpSensorParameters.TabIndex = 13;
            grpSensorParameters.TabStop = false;
            grpSensorParameters.Text = "Sensor Parameters";
            // 
            // btnCancelOptions
            // 
            this.btnCancelOptions.Location = new System.Drawing.Point(114, 53);
            this.btnCancelOptions.Name = "btnCancelOptions";
            this.btnCancelOptions.Size = new System.Drawing.Size(75, 23);
            this.btnCancelOptions.TabIndex = 12;
            this.btnCancelOptions.Text = "Cancel";
            this.btnCancelOptions.UseVisualStyleBackColor = true;
            // 
            // btnUpdateOptions
            // 
            this.btnUpdateOptions.Location = new System.Drawing.Point(27, 52);
            this.btnUpdateOptions.Name = "btnUpdateOptions";
            this.btnUpdateOptions.Size = new System.Drawing.Size(75, 23);
            this.btnUpdateOptions.TabIndex = 11;
            this.btnUpdateOptions.Text = "Update";
            this.btnUpdateOptions.UseVisualStyleBackColor = true;
            // 
            // cmbFrequency
            // 
            this.cmbFrequency.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbFrequency.FormattingEnabled = true;
            this.cmbFrequency.Location = new System.Drawing.Point(109, 23);
            this.cmbFrequency.Name = "cmbFrequency";
            this.cmbFrequency.Size = new System.Drawing.Size(98, 21);
            this.cmbFrequency.TabIndex = 10;
            // 
            // lblFrequency
            // 
            lblFrequency.AutoSize = true;
            lblFrequency.Cursor = System.Windows.Forms.Cursors.SizeNESW;
            lblFrequency.Location = new System.Drawing.Point(9, 26);
            lblFrequency.Name = "lblFrequency";
            lblFrequency.Size = new System.Drawing.Size(101, 13);
            lblFrequency.TabIndex = 17;
            lblFrequency.Text = "Measurement Freq.:";
            // 
            // lblAveragingNumber
            // 
            lblAveragingNumber.AutoSize = true;
            lblAveragingNumber.Location = new System.Drawing.Point(11, 86);
            lblAveragingNumber.Name = "lblAveragingNumber";
            lblAveragingNumber.Size = new System.Drawing.Size(96, 13);
            lblAveragingNumber.TabIndex = 15;
            lblAveragingNumber.Text = "Averaging number:";
            // 
            // grpAcquisitionSettings
            // 
            grpAcquisitionSettings.Controls.Add(this.numFiltering);
            grpAcquisitionSettings.Controls.Add(lblFiltering);
            grpAcquisitionSettings.Controls.Add(this.btnChooseDataFolder);
            grpAcquisitionSettings.Controls.Add(this.txtDataFolder);
            grpAcquisitionSettings.Controls.Add(lblChooseDataFolder);
            grpAcquisitionSettings.Location = new System.Drawing.Point(7, 562);
            grpAcquisitionSettings.Name = "grpAcquisitionSettings";
            grpAcquisitionSettings.Size = new System.Drawing.Size(219, 110);
            grpAcquisitionSettings.TabIndex = 14;
            grpAcquisitionSettings.TabStop = false;
            grpAcquisitionSettings.Text = "Data Settings";
            // 
            // numFiltering
            // 
            this.numFiltering.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numFiltering.Location = new System.Drawing.Point(128, 79);
            this.numFiltering.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numFiltering.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numFiltering.Name = "numFiltering";
            this.numFiltering.Size = new System.Drawing.Size(74, 20);
            this.numFiltering.TabIndex = 15;
            this.numFiltering.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // lblFiltering
            // 
            lblFiltering.AutoSize = true;
            lblFiltering.Location = new System.Drawing.Point(14, 81);
            lblFiltering.Name = "lblFiltering";
            lblFiltering.Size = new System.Drawing.Size(108, 13);
            lblFiltering.TabIndex = 19;
            lblFiltering.Text = "Save every nth point:";
            // 
            // btnChooseDataFolder
            // 
            this.btnChooseDataFolder.Location = new System.Drawing.Point(131, 44);
            this.btnChooseDataFolder.Name = "btnChooseDataFolder";
            this.btnChooseDataFolder.Size = new System.Drawing.Size(75, 23);
            this.btnChooseDataFolder.TabIndex = 14;
            this.btnChooseDataFolder.Text = "Choose";
            this.btnChooseDataFolder.UseVisualStyleBackColor = true;
            // 
            // txtDataFolder
            // 
            this.txtDataFolder.Location = new System.Drawing.Point(15, 47);
            this.txtDataFolder.Name = "txtDataFolder";
            this.txtDataFolder.ReadOnly = true;
            this.txtDataFolder.Size = new System.Drawing.Size(106, 20);
            this.txtDataFolder.TabIndex = 13;
            this.txtDataFolder.Text = "c:\\";
            // 
            // lblChooseDataFolder
            // 
            lblChooseDataFolder.AutoSize = true;
            lblChooseDataFolder.Location = new System.Drawing.Point(12, 29);
            lblChooseDataFolder.Name = "lblChooseDataFolder";
            lblChooseDataFolder.Size = new System.Drawing.Size(77, 13);
            lblChooseDataFolder.TabIndex = 0;
            lblChooseDataFolder.Text = "Data Location:";
            // 
            // lblGraphRefreshTime
            // 
            lblGraphRefreshTime.AutoSize = true;
            lblGraphRefreshTime.Location = new System.Drawing.Point(6, 32);
            lblGraphRefreshTime.Name = "lblGraphRefreshTime";
            lblGraphRefreshTime.Size = new System.Drawing.Size(118, 13);
            lblGraphRefreshTime.TabIndex = 17;
            lblGraphRefreshTime.Text = "Graph refresh time (ms):";
            // 
            // grpMeasurements
            // 
            grpMeasurements.Controls.Add(this.chkAutoSaveData);
            grpMeasurements.Controls.Add(this.btnSaveData);
            grpMeasurements.Controls.Add(this.btnStop);
            grpMeasurements.Controls.Add(this.btnStart);
            grpMeasurements.Location = new System.Drawing.Point(10, 414);
            grpMeasurements.Name = "grpMeasurements";
            grpMeasurements.Size = new System.Drawing.Size(228, 117);
            grpMeasurements.TabIndex = 15;
            grpMeasurements.TabStop = false;
            grpMeasurements.Text = "Control";
            // 
            // chkAutoSaveData
            // 
            this.chkAutoSaveData.AutoSize = true;
            this.chkAutoSaveData.Location = new System.Drawing.Point(41, 90);
            this.chkAutoSaveData.Name = "chkAutoSaveData";
            this.chkAutoSaveData.Size = new System.Drawing.Size(143, 17);
            this.chkAutoSaveData.TabIndex = 26;
            this.chkAutoSaveData.Text = "Auto-save bufffered data";
            this.chkAutoSaveData.UseVisualStyleBackColor = true;
            // 
            // btnSaveData
            // 
            this.btnSaveData.Location = new System.Drawing.Point(40, 60);
            this.btnSaveData.Name = "btnSaveData";
            this.btnSaveData.Size = new System.Drawing.Size(146, 24);
            this.btnSaveData.TabIndex = 25;
            this.btnSaveData.Text = "Save buffered data now";
            this.btnSaveData.UseVisualStyleBackColor = true;
            // 
            // btnStop
            // 
            this.btnStop.Location = new System.Drawing.Point(112, 27);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(90, 24);
            this.btnStop.TabIndex = 24;
            this.btnStop.Text = "Stop";
            this.btnStop.UseVisualStyleBackColor = true;
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(15, 27);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(91, 24);
            this.btnStart.TabIndex = 23;
            this.btnStart.Text = "Start";
            this.btnStart.UseVisualStyleBackColor = true;
            // 
            // grpZeroing
            // 
            grpZeroing.Controls.Add(this.chkInvertGraph);
            grpZeroing.Controls.Add(this.numReferenceValue);
            grpZeroing.Controls.Add(lblReferenceValue);
            grpZeroing.Controls.Add(this.btnResetOffset);
            grpZeroing.Controls.Add(this.btnZeroReadings);
            grpZeroing.Location = new System.Drawing.Point(3, 73);
            grpZeroing.Name = "grpZeroing";
            grpZeroing.Size = new System.Drawing.Size(226, 134);
            grpZeroing.TabIndex = 17;
            grpZeroing.TabStop = false;
            grpZeroing.Text = "Setting baseline";
            // 
            // chkInvertGraph
            // 
            this.chkInvertGraph.AutoSize = true;
            this.chkInvertGraph.Location = new System.Drawing.Point(8, 109);
            this.chkInvertGraph.Name = "chkInvertGraph";
            this.chkInvertGraph.Size = new System.Drawing.Size(83, 17);
            this.chkInvertGraph.TabIndex = 27;
            this.chkInvertGraph.Text = "Invert graph";
            this.chkInvertGraph.UseVisualStyleBackColor = true;
            // 
            // numReferenceValue
            // 
            this.numReferenceValue.DecimalPlaces = 2;
            this.numReferenceValue.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.numReferenceValue.Location = new System.Drawing.Point(144, 87);
            this.numReferenceValue.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.numReferenceValue.Minimum = new decimal(new int[] {
            1000000,
            0,
            0,
            -2147483648});
            this.numReferenceValue.Name = "numReferenceValue";
            this.numReferenceValue.Size = new System.Drawing.Size(76, 20);
            this.numReferenceValue.TabIndex = 26;
            // 
            // lblReferenceValue
            // 
            lblReferenceValue.AutoSize = true;
            lblReferenceValue.Location = new System.Drawing.Point(2, 90);
            lblReferenceValue.Name = "lblReferenceValue";
            lblReferenceValue.Size = new System.Drawing.Size(135, 13);
            lblReferenceValue.TabIndex = 25;
            lblReferenceValue.Text = "Reference Value (microns):";
            // 
            // btnResetOffset
            // 
            this.btnResetOffset.Location = new System.Drawing.Point(26, 53);
            this.btnResetOffset.Name = "btnResetOffset";
            this.btnResetOffset.Size = new System.Drawing.Size(185, 24);
            this.btnResetOffset.TabIndex = 19;
            this.btnResetOffset.Text = "Remove baseline (clears graph)";
            this.btnResetOffset.UseVisualStyleBackColor = true;
            // 
            // btnZeroReadings
            // 
            this.btnZeroReadings.Location = new System.Drawing.Point(26, 23);
            this.btnZeroReadings.Name = "btnZeroReadings";
            this.btnZeroReadings.Size = new System.Drawing.Size(185, 24);
            this.btnZeroReadings.TabIndex = 18;
            this.btnZeroReadings.Text = "Set baseline (clears graph)";
            this.btnZeroReadings.UseVisualStyleBackColor = true;
            // 
            // grpInputs
            // 
            grpInputs.Controls.Add(this.groupBox1);
            grpInputs.Controls.Add(this.comboBox1);
            grpInputs.Controls.Add(this.label5);
            grpInputs.Controls.Add(this.radioButton2);
            grpInputs.Controls.Add(this.radioButton1);
            grpInputs.Controls.Add(this.label3);
            grpInputs.Controls.Add(this.numericUpDown1);
            grpInputs.Controls.Add(this.label2);
            grpInputs.Location = new System.Drawing.Point(265, 43);
            grpInputs.Name = "grpInputs";
            grpInputs.Size = new System.Drawing.Size(269, 172);
            grpInputs.TabIndex = 23;
            grpInputs.TabStop = false;
            grpInputs.Text = "Monitor Mode";
            grpInputs.Visible = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.textBox2);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.textBox1);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(74, 19);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(241, 62);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(128, 38);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(100, 20);
            this.textBox2.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(8, 41);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(63, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "Strip speed:";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(128, 13);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 20);
            this.textBox1.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(105, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Applicator roll speed:";
            // 
            // comboBox1
            // 
            this.comboBox1.DisplayMember = "0";
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "Reverse coating",
            "Forward coating"});
            this.comboBox1.Location = new System.Drawing.Point(135, 143);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(121, 21);
            this.comboBox1.TabIndex = 8;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(20, 145);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(75, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "Coating mode:";
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(147, 54);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(95, 17);
            this.radioButton2.TabIndex = 6;
            this.radioButton2.Text = "Electrical Input";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Checked = true;
            this.radioButton1.Location = new System.Drawing.Point(23, 55);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(87, 17);
            this.radioButton1.TabIndex = 5;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "Manual Input";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(208, 24);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(15, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "%";
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.DecimalPlaces = 2;
            this.numericUpDown1.Location = new System.Drawing.Point(148, 21);
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(61, 20);
            this.numericUpDown1.TabIndex = 3;
            this.numericUpDown1.Value = new decimal(new int[] {
            50,
            0,
            0,
            0});
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(22, 24);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(120, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Paint Volume Solids (%):";
            // 
            // lblScaleMode
            // 
            lblScaleMode.AutoSize = true;
            lblScaleMode.Location = new System.Drawing.Point(9, 113);
            lblScaleMode.Name = "lblScaleMode";
            lblScaleMode.Size = new System.Drawing.Size(101, 13);
            lblScaleMode.TabIndex = 25;
            lblScaleMode.Text = "Zeroed scale mode:";
            // 
            // lblVerticalRange
            // 
            lblVerticalRange.AutoSize = true;
            lblVerticalRange.Location = new System.Drawing.Point(10, 140);
            lblVerticalRange.Name = "lblVerticalRange";
            lblVerticalRange.Size = new System.Drawing.Size(125, 13);
            lblVerticalRange.TabIndex = 23;
            lblVerticalRange.Text = "Vertical Range (microns):";
            // 
            // lblFactor
            // 
            lblFactor.AutoSize = true;
            lblFactor.Location = new System.Drawing.Point(9, 7);
            lblFactor.Name = "lblFactor";
            lblFactor.Size = new System.Drawing.Size(104, 13);
            lblFactor.TabIndex = 32;
            lblFactor.Text = "Multiplication Factor:";
            // 
            // lblDriftFactor
            // 
            lblDriftFactor.AutoSize = true;
            lblDriftFactor.Location = new System.Drawing.Point(-1, 34);
            lblDriftFactor.Name = "lblDriftFactor";
            lblDriftFactor.Size = new System.Drawing.Size(128, 13);
            lblDriftFactor.TabIndex = 32;
            lblDriftFactor.Text = "Drift Factor (microns/min):";
            // 
            // pnlRightCol
            // 
            this.pnlRightCol.Controls.Add(this.btnClose);
            this.pnlRightCol.Controls.Add(lblDriftFactor);
            this.pnlRightCol.Controls.Add(this.numDriftFactor);
            this.pnlRightCol.Controls.Add(lblFactor);
            this.pnlRightCol.Controls.Add(this.numFactor);
            this.pnlRightCol.Controls.Add(grpOutput);
            this.pnlRightCol.Controls.Add(grpZeroing);
            this.pnlRightCol.Controls.Add(this.grpGraphSettings);
            this.pnlRightCol.Controls.Add(grpMeasurements);
            this.pnlRightCol.Location = new System.Drawing.Point(763, 19);
            this.pnlRightCol.Name = "pnlRightCol";
            this.pnlRightCol.Size = new System.Drawing.Size(243, 671);
            this.pnlRightCol.TabIndex = 22;
            // 
            // numDriftFactor
            // 
            this.numDriftFactor.DecimalPlaces = 3;
            this.numDriftFactor.Increment = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            this.numDriftFactor.Location = new System.Drawing.Point(127, 32);
            this.numDriftFactor.Maximum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numDriftFactor.Minimum = new decimal(new int[] {
            5,
            0,
            0,
            -2147483648});
            this.numDriftFactor.Name = "numDriftFactor";
            this.numDriftFactor.Size = new System.Drawing.Size(106, 20);
            this.numDriftFactor.TabIndex = 31;
            // 
            // numFactor
            // 
            this.numFactor.DecimalPlaces = 3;
            this.numFactor.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numFactor.Location = new System.Drawing.Point(127, 3);
            this.numFactor.Maximum = new decimal(new int[] {
            7,
            0,
            0,
            0});
            this.numFactor.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            196608});
            this.numFactor.Name = "numFactor";
            this.numFactor.Size = new System.Drawing.Size(106, 20);
            this.numFactor.TabIndex = 31;
            this.numFactor.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // grpGraphSettings
            // 
            this.grpGraphSettings.Controls.Add(this.cmbScaleMode);
            this.grpGraphSettings.Controls.Add(lblScaleMode);
            this.grpGraphSettings.Controls.Add(this.numVerticalRange);
            this.grpGraphSettings.Controls.Add(lblVerticalRange);
            this.grpGraphSettings.Controls.Add(this.numAveragingNumber);
            this.grpGraphSettings.Controls.Add(this.numGraphRefreshTime);
            this.grpGraphSettings.Controls.Add(lblAveragingNumber);
            this.grpGraphSettings.Controls.Add(lblGraphRefreshTime);
            this.grpGraphSettings.Controls.Add(this.chkEnableAveraging);
            this.grpGraphSettings.Location = new System.Drawing.Point(12, 236);
            this.grpGraphSettings.Name = "grpGraphSettings";
            this.grpGraphSettings.Size = new System.Drawing.Size(228, 169);
            this.grpGraphSettings.TabIndex = 16;
            this.grpGraphSettings.TabStop = false;
            this.grpGraphSettings.Text = "Graph Settings";
            // 
            // cmbScaleMode
            // 
            this.cmbScaleMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbScaleMode.FormattingEnabled = true;
            this.cmbScaleMode.Items.AddRange(new object[] {
            "Full",
            "Manual",
            "Auto"});
            this.cmbScaleMode.Location = new System.Drawing.Point(129, 107);
            this.cmbScaleMode.Name = "cmbScaleMode";
            this.cmbScaleMode.Size = new System.Drawing.Size(87, 21);
            this.cmbScaleMode.TabIndex = 26;
            // 
            // numVerticalRange
            // 
            this.numVerticalRange.Increment = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.numVerticalRange.Location = new System.Drawing.Point(140, 137);
            this.numVerticalRange.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.numVerticalRange.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numVerticalRange.Name = "numVerticalRange";
            this.numVerticalRange.Size = new System.Drawing.Size(76, 20);
            this.numVerticalRange.TabIndex = 24;
            this.numVerticalRange.Value = new decimal(new int[] {
            50,
            0,
            0,
            0});
            // 
            // numAveragingNumber
            // 
            this.numAveragingNumber.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.numAveragingNumber.Location = new System.Drawing.Point(140, 79);
            this.numAveragingNumber.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.numAveragingNumber.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numAveragingNumber.Name = "numAveragingNumber";
            this.numAveragingNumber.Size = new System.Drawing.Size(76, 20);
            this.numAveragingNumber.TabIndex = 22;
            this.numAveragingNumber.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // numGraphRefreshTime
            // 
            this.numGraphRefreshTime.Increment = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.numGraphRefreshTime.Location = new System.Drawing.Point(142, 30);
            this.numGraphRefreshTime.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.numGraphRefreshTime.Minimum = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.numGraphRefreshTime.Name = "numGraphRefreshTime";
            this.numGraphRefreshTime.Size = new System.Drawing.Size(74, 20);
            this.numGraphRefreshTime.TabIndex = 20;
            this.numGraphRefreshTime.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // chkEnableAveraging
            // 
            this.chkEnableAveraging.AutoSize = true;
            this.chkEnableAveraging.Checked = true;
            this.chkEnableAveraging.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkEnableAveraging.Location = new System.Drawing.Point(15, 59);
            this.chkEnableAveraging.Name = "chkEnableAveraging";
            this.chkEnableAveraging.Size = new System.Drawing.Size(138, 17);
            this.chkEnableAveraging.TabIndex = 21;
            this.chkEnableAveraging.Text = "Enable moving average";
            this.chkEnableAveraging.UseVisualStyleBackColor = true;
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(147, 638);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(76, 27);
            this.btnClose.TabIndex = 30;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            // 
            // saveFile
            // 
            this.saveFile.DefaultExt = "txt";
            this.saveFile.FileName = "output.txt";
            this.saveFile.RestoreDirectory = true;
            // 
            // zgcData
            // 
            this.zgcData.EditModifierKeys = System.Windows.Forms.Keys.None;
            this.zgcData.IsEnableHPan = false;
            this.zgcData.IsEnableHZoom = false;
            this.zgcData.IsEnableVPan = false;
            this.zgcData.IsEnableVZoom = false;
            this.zgcData.IsEnableWheelZoom = false;
            this.zgcData.Location = new System.Drawing.Point(237, 155);
            this.zgcData.Name = "zgcData";
            this.zgcData.ScrollGrace = 0;
            this.zgcData.ScrollMaxX = 0;
            this.zgcData.ScrollMaxY = 0;
            this.zgcData.ScrollMaxY2 = 0;
            this.zgcData.ScrollMinX = 0;
            this.zgcData.ScrollMinY = 0;
            this.zgcData.ScrollMinY2 = 0;
            this.zgcData.Size = new System.Drawing.Size(515, 517);
            this.zgcData.TabIndex = 17;
            // 
            // txtReading
            // 
            this.txtReading.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.txtReading.Font = new System.Drawing.Font("Verdana", 40F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtReading.ForeColor = System.Drawing.Color.Black;
            this.txtReading.Location = new System.Drawing.Point(237, 7);
            this.txtReading.Multiline = true;
            this.txtReading.Name = "txtReading";
            this.txtReading.ReadOnly = true;
            this.txtReading.Size = new System.Drawing.Size(512, 142);
            this.txtReading.TabIndex = 16;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(grpInputs);
            this.panel1.Location = new System.Drawing.Point(427, 510);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(37, 25);
            this.panel1.TabIndex = 23;
            this.panel1.Visible = false;
            // 
            // MovingAveraging_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1018, 692);
            this.Controls.Add(this.pnlRightCol);
            this.Controls.Add(this.txtReading);
            this.Controls.Add(grpAcquisitionSettings);
            this.Controls.Add(this.zgcData);
            this.Controls.Add(lblHeading);
            this.Controls.Add(this.panel1);
            this.Controls.Add(grpSensorSelection);
            this.Controls.Add(grpConnection);
            this.Controls.Add(grpSensorParameters);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MovingAveraging_Form";
            this.Text = "Wet Film Monitor - WFM1";
            grpOutput.ResumeLayout(false);
            grpConnection.ResumeLayout(false);
            grpConnection.PerformLayout();
            grpSensorSelection.ResumeLayout(false);
            grpSensorSelection.PerformLayout();
            grpSensorParameters.ResumeLayout(false);
            grpSensorParameters.PerformLayout();
            grpAcquisitionSettings.ResumeLayout(false);
            grpAcquisitionSettings.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numFiltering)).EndInit();
            grpMeasurements.ResumeLayout(false);
            grpMeasurements.PerformLayout();
            grpZeroing.ResumeLayout(false);
            grpZeroing.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numReferenceValue)).EndInit();
            grpInputs.ResumeLayout(false);
            grpInputs.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.pnlRightCol.ResumeLayout(false);
            this.pnlRightCol.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numDriftFactor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numFactor)).EndInit();
            this.grpGraphSettings.ResumeLayout(false);
            this.grpGraphSettings.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numVerticalRange)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numAveragingNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numGraphRefreshTime)).EndInit();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox txtOutput;
        private System.Windows.Forms.Button btnSaveOutput;
        private System.Windows.Forms.Button btnClearOutput;
        private System.Windows.Forms.Button btnDisconnect;
        private System.Windows.Forms.Button btnConnect;
        private System.Windows.Forms.ComboBox cmbConnectionBaudRate;
        private System.Windows.Forms.Button btnRefreshCOMPorts;
        private System.Windows.Forms.ComboBox cmbCOMPorts;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnCancelSensorSelection;
        private System.Windows.Forms.Button btnUpdateSensorSelection;
        private System.Windows.Forms.ComboBox cmbSensorType;
        private System.Windows.Forms.ComboBox cmbSensorModel;
        private System.Windows.Forms.Button btnChooseDataFolder;
        private System.Windows.Forms.TextBox txtDataFolder;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Button btnCancelOptions;
        private System.Windows.Forms.Button btnUpdateOptions;
        private System.Windows.Forms.NumericUpDown numAveragingNumber;
        private System.Windows.Forms.CheckBox chkEnableAveraging;
        private System.Windows.Forms.ComboBox cmbFrequency;
        private System.Windows.Forms.FolderBrowserDialog folderBrowser;
        private System.Windows.Forms.SaveFileDialog saveFile;
        private ZedGraph.ZedGraphControl zgcData;
        private System.Windows.Forms.Button btnSaveData;
        private System.Windows.Forms.NumericUpDown numGraphRefreshTime;
        private System.Windows.Forms.NumericUpDown numFiltering;
        private System.Windows.Forms.CheckBox chkAutoSaveData;
        private System.Windows.Forms.TextBox txtReading;
        private System.Windows.Forms.Panel pnlRightCol;
        private System.Windows.Forms.Timer timerGraph;
        private System.Windows.Forms.GroupBox grpGraphSettings;
        private System.Windows.Forms.Timer timerSaveData;
        private System.Windows.Forms.Button btnZeroReadings;
        private System.Windows.Forms.Button btnResetOffset;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown numVerticalRange;
        private System.Windows.Forms.ComboBox cmbScaleMode;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.NumericUpDown numFactor;
        private System.Windows.Forms.NumericUpDown numReferenceValue;
        private System.Windows.Forms.CheckBox chkInvertGraph;
        private System.Windows.Forms.NumericUpDown numDriftFactor;
    }
}