﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

using System.Threading;
using System.IO.Ports;
using System.Text;

using SensorControl;
using SensorControl.Sensors;

namespace MovingAveraging
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MovingAveraging_Form());


            /*ILD2200_Sensor sensor = new ILD2200_Sensor();
            sensor.Connect("COM7", 691200);

            sensor.Port.DiscardInBuffer();
            sensor.Port.DiscardOutBuffer();

            Thread.Sleep(500);

            SensorReading[] readings  = sensor.TakeReadings();

            foreach (SensorReading r in readings) {
                Console.Write("(" + r.Time.ToString("0.0") + ", " + r.Value.ToString("0.0") + "\t");
            }
             */
            /*
            SerialPort port = new SerialPort("COM7", 691200);
            port.Encoding = Encoding.GetEncoding(28591);
            // port.ReadTimeout = 3;
            // port.WriteTimeout = 5;
            port.Open();
            Thread.Sleep(200);

            port.DiscardInBuffer();
            port.DiscardOutBuffer();

            Thread.Sleep(100);

            port.Write("\x2b\x2b\x2b\x0d\x49\x4c\x44\x31\x20\x77\x00\x02");

            Thread.Sleep(500);

            string s = port.ReadExisting();

            int i = 0;
            foreach (char c in s)
            {
                Console.Write(((byte)c).ToString("000 "));
                if (i == 17)
                {
                    i = 0;
                    Console.Write("\n");
                }
                else
                {
                    i++;
                }
            }

            Console.WriteLine("\n\n");

            Console.WriteLine(s);

            Console.WriteLine("enter ...");
            Console.Read();*/
        }
    }
}
