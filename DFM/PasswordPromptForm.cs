﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace DFM
{
    public partial class PasswordPromptForm : Form
    {
        public PasswordPromptForm()
        {
            InitializeComponent();
        }

        public string Password
        {
            get { return txtPassword.Text; }
        }
    }
}
