﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using System.Threading;
using System.IO;
using System.IO.Ports;

using DFM.Laser;

namespace DFM
{
    public partial class LaserTestForm : Form
    {
        ILaser laser;

        public LaserTestForm()
        {
            InitializeComponent();

            // set up defaults
            cmbCOM.Items.Clear();
            updateCOMPorts();
            
            btnConnect.Text = "Connect";

            laser = new PLCLaser();
            State = States.Disconnected;
            
            // events
            btnRefreshCOM.Click += new EventHandler(btnRefreshCOM_Click);
            btnConnect.Click += new EventHandler(btnConnect_Click);
            btnRun.Click += new EventHandler(btnRun_Click);
            
            this.FormClosing += new FormClosingEventHandler(SensorTestForm_FormClosing);
        }
                
        private void Connect(string portName)
        {
            btnConnect.Enabled = false;

            try
            {            
                laser.Connect(portName);
            }
            catch (Exception ex)
            {
                btnConnect.Enabled = true;
                MessageBox.Show("Error connecting: " + ex.Message);
                return;
            }

            State = States.Connected;
        }

        private void Disconnect()
        {
            laser.Disconnect();
            State = States.Disconnected;
        }

        private void Run()
        {
            State = States.Started;
            bool result = laser.Run(txtCommand.Text);
            MessageBox.Show("Worked?: " + result.ToString());
            State = States.Connected;
        }
        
        private void updateCOMPorts()
        {
            string[] ports = SerialPort.GetPortNames();
            Array.Sort(ports);

            cmbCOM.Items.Clear();
            cmbCOM.Items.AddRange(ports);

            if (cmbCOM.Items.Count > 0) cmbCOM.SelectedIndex = 0;
        }

        #region State

        protected enum States
        {
            Disconnected,
            Connected,
            Started
        }

        protected States state;

        protected States State
        {
            get { return state; }
            set
            {
                state = value;
                bool isDc = (state == States.Disconnected);
                bool isC = (state == States.Connected);
                bool isS = (state == States.Started);

                cmbCOM.Enabled = isDc;
                btnRefreshCOM.Enabled = isDc;
                btnRun.Enabled = isC;
                txtCommand.Enabled = isC;

                btnConnect.Enabled = isDc || isC;
                btnConnect.Text = (isDc ? "Connect" : "Disconnect");
            }
        }

        #endregion
        
        #region Events

        void btnRun_Click(object sender, EventArgs e)
        {
            Run();
        }

        void btnRefreshCOM_Click(object sender, EventArgs e)
        {
            updateCOMPorts();
        }

        void btnConnect_Click(object sender, EventArgs e)
        {
            if (State == States.Disconnected)
            {
                string port = cmbCOM.SelectedItem.ToString();                
                Connect(port);
            }
            else Disconnect();
        }
                
        void SensorTestForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (State == States.Connected)
            {
                Disconnect();
            }
        }

        #endregion
    }
}
