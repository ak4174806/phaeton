﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using DFM.Settings;
using DFM.Settings.Bindable;
using System.Xml.Serialization;
using System.IO;
using NLog;

namespace DFM
{
    public partial class SettingsTestForm : Form
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();        

        public enum FruitEnum
        {
            Apple,
            Banana,
            Orange
        }

        SettingsGroup grp;

        BindableBoolSetting setting;
        BindableSettingValue<string, TextBox> setting2;
        BindableEnumSetting<FruitEnum, TextBox> setting3;
        BindableEnumSettingComboBox<FruitEnum> setting4;
        BindableEnumSettingComboBox<FruitEnum> setting5;
        BindableEnumSettingRadioButtons<FruitEnum> setting6;
        BindableStringArraySetting setting7;
        HybridSetting hybrid;

        UserSettings savedSettings;

        public SettingsTestForm()
        {
            InitializeComponent();
            FormClosing += new FormClosingEventHandler(SettingsTestForm_FormClosing);
        }

        void SettingsTestForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            // wrapped because don't want form closing to be cancelled
            try
            {
                grp.Save();
                savedSettings.Settings = grp;
                savedSettings.Save();
            }
            catch (Exception)
            { }
        }

        private void chkBindingEnabled_CheckedChanged(object sender, EventArgs e)
        {
            if (setting == null) return;
            setting.BindingEnabled = ((CheckBox)sender).Checked;
            setting2.BindingEnabled = ((CheckBox)sender).Checked;
            setting3.BindingEnabled = ((CheckBox)sender).Checked;
            setting4.BindingEnabled = ((CheckBox)sender).Checked;
            setting5.BindingEnabled = ((CheckBox)sender).Checked;
            setting6.BindingEnabled = ((CheckBox)sender).Checked;
            setting7.BindingEnabled = ((CheckBox)sender).Checked;

            foreach (BaseSetting s in hybrid.Settings)
            {
                if (s is IBaseBindableSettingValue)
                {                    
                    ((IBaseBindableSettingValue)s).BindingEnabled = ((CheckBox)sender).Checked;
                }
            }
        }

        private void SettingsTestForm_Load(object sender, EventArgs e)
        {
            try
            {
                chkBindingEnabled.Checked = true;

                setting = new BindableBoolSetting(chkSetting, "bool1", true);
                setting.BindingEnabled = chkBindingEnabled.Checked;
                setting.Bind(chkFollower);

                setting.ValueChanged += new EventHandler<ValueChangedEventArgs<bool>>(setting_ValueChanged);
                txtDisplay.TextChanged += new EventHandler(txtDisplay_TextChanged);
                txtDisplay.Text = setting.Value.ToString();

                setting2 = new BindableSettingValue<string, TextBox>(txtDisplay, "text1", setting.Value.ToString());
                setting2.BindingEnabled = chkBindingEnabled.Checked;
                setting2.Bind(txtFollower);
                setting2.AddValidator(new SettingValidator<string>(delegate(ISettingValue<string> csetting, ValidationResult<string> ce)
                {
                    if (ce.Value.Length < 3)
                    {
                        ce.IsError = true;
                        ce.ErrorMessage = "Needs to be a bit longer";
                    }
                }));

                setting3 = new BindableEnumSetting<FruitEnum, TextBox>(txtEnumFollower, "enum1", FruitEnum.Banana);
                setting3.BindingEnabled = chkBindingEnabled.Checked;
                txtEnumValue.TextChanged += new EventHandler(txtEnumValue_TextChanged);

                setting4 = new BindableEnumSettingComboBox<FruitEnum>(cmbEnum2, "enum2", FruitEnum.Banana);
                setting4.BindingEnabled = chkBindingEnabled.Checked;
                setting4.ValueChanged += new EventHandler<ValueChangedEventArgs<FruitEnum>>(setting4_ValueChanged);

                setting5 = (BindableEnumSettingComboBox<FruitEnum>)setting4.DeepCopy();
                setting5.Name = "enum3";
                setting5.Unbind(cmbEnum2);
                setting5.Bind(cmbEnum3);
                setting5.SavedValue = setting5.Value = FruitEnum.Orange;
                setting5.BindingEnabled = chkBindingEnabled.Checked;
                setting5.ValueChanged += new EventHandler<ValueChangedEventArgs<FruitEnum>>(setting5_ValueChanged);
                txtEnum3Follower.Text = setting5.Value.ToString();

                /*setting5 = new BindableEnumSettingComboBox<FruitEnum>(cmbEnum3, "enum3", FruitEnum.Orange);
                setting5.BindingEnabled = chkBindingEnabled.Checked;
                setting5.ValueChanged += new EventHandler<ValueChangedEventArgs<FruitEnum>>(setting5_ValueChanged);
                txtEnum3Follower.Text = setting5.Value.ToString();*/

                setting7 = new BindableStringArraySetting(cmbStringArray, "stringArray", new string[] { "one", "two", "three", "Douglas Adams" });
                setting7.BindingEnabled = chkBindingEnabled.Checked;
                setting7.ValueChanged += new EventHandler<ValueChangedEventArgs<string>>(setting7_ValueChanged);
                txtStringArrayFollower.Text = setting7.Value;

                try
                {
                    setting6 = new BindableEnumSettingRadioButtons<FruitEnum>(radOrange, "enum4", FruitEnum.Orange);
                    setting6.Bind(radApple, FruitEnum.Apple);
                    setting6.Bind(radBanana, FruitEnum.Banana);
                    setting6.BindingEnabled = chkBindingEnabled.Checked;
                    setting6.ValueChanged += new EventHandler<ValueChangedEventArgs<FruitEnum>>(setting6_ValueChanged);
                    txtEnum4Follower.Text = setting6.Value.ToString();

                    HybridSetting tmp = new HybridSetting("radio enabler test");
                    tmp.AddEnableSetting(new BindableBoolRadioSetting(radBanana, "enabled", radBanana.Checked));
                    tmp.Add(new BindableSettingValue<string, TextBox>(txtEnum4Follower, "follower", txtEnum4Follower.Text));
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }

                hybrid = new HybridSetting("hybrid X");
                hybrid.AddEnableSetting(new BindableBoolSetting(chkEnableX, "enabled", true));
                hybrid.Add(new BindableSettingValue<string, TextBox>(txtHybridX, "data", "apple"));
                ((SettingValue<string>)hybrid["data"]).ValueChanged += new EventHandler<ValueChangedEventArgs<string>>(SettingsTestForm_ValueChanging);
                ((SettingValue<bool>)hybrid["enabled"]).ValueChanged += new EventHandler<ValueChangedEventArgs<bool>>(SettingsTestForm_ValueChanging1);

                grp = new SettingsGroup("s grp");
                grp.Add(setting);
                grp.Add(setting2);
                grp.Add(setting3);
                grp.Add(setting4);
                grp.Add(setting5);
                grp.Add(setting6);
                grp.Add(setting7);
                grp.Add(hybrid);

                savedSettings = new UserSettings(grp.Name);
                if (savedSettings.Settings != null)
                {
                    grp.LoadSettingsFrom(savedSettings.Settings);
                }
            }
            catch (Exception ex)
            {
                logger.ErrorException("Error setting up settingstestform", ex);
                throw;
            }
        }

        void setting7_ValueChanged(object sender, ValueChangedEventArgs<string> e)
        {
            txtStringArrayFollower.Text = e.Value;
        }

        void SettingsTestForm_ValueChanging(object sender, ValueChangedEventArgs<string> e)
        {
            txtHybridFollower.Text = hybrid.Fetch<string>("data") + hybrid.Fetch<bool>("enabled").ToString();            
        }

        void SettingsTestForm_ValueChanging1(object sender, ValueChangedEventArgs<bool> e)
        {
            txtHybridFollower.Text = hybrid.Fetch<string>("data") + hybrid.Fetch<bool>("enabled").ToString();
        }

        void setting6_ValueChanged(object sender, ValueChangedEventArgs<SettingsTestForm.FruitEnum> e)
        {
            txtEnum4Follower.Text = e.Value.ToString();
        }

        void setting5_ValueChanged(object sender, ValueChangedEventArgs<SettingsTestForm.FruitEnum> e)
        {
            txtEnum3Follower.Text = e.Value.ToString();
        }

        void setting4_ValueChanged(object sender, ValueChangedEventArgs<SettingsTestForm.FruitEnum> e)
        {
            txtEnum2Follower.Text = e.Value.ToString();
        }

        void txtEnumValue_TextChanged(object sender, EventArgs e)
        {
            setting3.Value = setting3.StringToEnumHandler(txtEnumValue.Text);
        }

        void txtDisplay_TextChanged(object sender, EventArgs e)
        {
            if (setting.Value.ToString() != txtDisplay.Text)
            {
                bool tmp;
                if (Boolean.TryParse(txtDisplay.Text, out tmp))
                    setting.Value = tmp;
            }
        }

        void setting_ValueChanged(object sender, ValueChangedEventArgs<bool> e)
        {
            if (txtDisplay.Text != e.Value.ToString())
                txtDisplay.Text = e.Value.ToString();
        }

        int i = 0;
        private void button1_Click(object sender, EventArgs e)
        {
            if (i == 0)
                setting7.Values = new string[] {"apple",  "three", "Stephen Hawking", "Test" };
            else
                setting7.Values = new string[] { };
            i = 1 - i;
        }
    }
}
