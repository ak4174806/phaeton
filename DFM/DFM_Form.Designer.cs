﻿namespace DFM
{
    partial class DFM_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.SplitContainer outerContainer;
            System.Windows.Forms.SplitContainer topContainer;
            System.Windows.Forms.Label lblTargetThickness;
            System.Windows.Forms.Label lblStripCentre;
            System.Windows.Forms.Label lblStripWidth;
            System.Windows.Forms.Label lblScanComment;
            System.Windows.Forms.Label lblHeading;
            System.Windows.Forms.PictureBox picLogo;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DFM_Form));
            this.zgcScanDisplay = new ZedGraph.ZedGraphControl();
            this.lblTargetThicknessUnit = new System.Windows.Forms.Label();
            this.lblStripCentreUnit = new System.Windows.Forms.Label();
            this.lblStripWidthUnit = new System.Windows.Forms.Label();
            this.numTargetThickness = new System.Windows.Forms.NumericUpDown();
            this.numStripCentre = new System.Windows.Forms.NumericUpDown();
            this.numStripWidth = new System.Windows.Forms.NumericUpDown();
            this.btnQuickScan = new System.Windows.Forms.Button();
            this.btnGalilMonitor = new System.Windows.Forms.Button();
            this.txtPaintingDisplay = new System.Windows.Forms.TextBox();
            this.chkRequirePainting = new System.Windows.Forms.CheckBox();
            this.chkRepeatReadings = new System.Windows.Forms.CheckBox();
            this.txtScanComment = new System.Windows.Forms.TextBox();
            this.grpStripEdges = new System.Windows.Forms.GroupBox();
            this.txtRightEdge = new System.Windows.Forms.TextBox();
            this.txtLeftEdge = new System.Windows.Forms.TextBox();
            this.btnClearReadings = new System.Windows.Forms.Button();
            this.btnClearEdges = new System.Windows.Forms.Button();
            this.btnPastResults = new System.Windows.Forms.Button();
            this.btnConnect = new System.Windows.Forms.Button();
            this.btnStop = new System.Windows.Forms.Button();
            this.btnSettings = new System.Windows.Forms.Button();
            this.btnRunWithoutEdgeScan = new System.Windows.Forms.Button();
            this.btnRunWithEdgeScan = new System.Windows.Forms.Button();
            this.tblScanDisplays = new System.Windows.Forms.TableLayoutPanel();
            this.dfmScanDisplayControlLeft = new DFM.DFMScanDisplayControl();
            this.dfmScanDisplayControlMiddle = new DFM.DFMScanDisplayControl();
            this.dfmScanDisplayControlRight = new DFM.DFMScanDisplayControl();
            outerContainer = new System.Windows.Forms.SplitContainer();
            topContainer = new System.Windows.Forms.SplitContainer();
            lblTargetThickness = new System.Windows.Forms.Label();
            lblStripCentre = new System.Windows.Forms.Label();
            lblStripWidth = new System.Windows.Forms.Label();
            lblScanComment = new System.Windows.Forms.Label();
            lblHeading = new System.Windows.Forms.Label();
            picLogo = new System.Windows.Forms.PictureBox();
            outerContainer.Panel1.SuspendLayout();
            outerContainer.Panel2.SuspendLayout();
            outerContainer.SuspendLayout();
            topContainer.Panel1.SuspendLayout();
            topContainer.Panel2.SuspendLayout();
            topContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numTargetThickness)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numStripCentre)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numStripWidth)).BeginInit();
            this.grpStripEdges.SuspendLayout();
            this.tblScanDisplays.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(picLogo)).BeginInit();
            this.SuspendLayout();
            // 
            // outerContainer
            // 
            outerContainer.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            outerContainer.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            outerContainer.Location = new System.Drawing.Point(0, 78);
            outerContainer.Name = "outerContainer";
            outerContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // outerContainer.Panel1
            // 
            outerContainer.Panel1.Controls.Add(topContainer);
            // 
            // outerContainer.Panel2
            // 
            outerContainer.Panel2.Controls.Add(this.tblScanDisplays);
            outerContainer.Size = new System.Drawing.Size(883, 624);
            outerContainer.SplitterDistance = 242;
            outerContainer.TabIndex = 2;
            outerContainer.TabStop = false;
            // 
            // topContainer
            // 
            topContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            topContainer.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            topContainer.Location = new System.Drawing.Point(0, 0);
            topContainer.Name = "topContainer";
            // 
            // topContainer.Panel1
            // 
            topContainer.Panel1.Controls.Add(this.zgcScanDisplay);
            // 
            // topContainer.Panel2
            // 
            topContainer.Panel2.Controls.Add(this.lblTargetThicknessUnit);
            topContainer.Panel2.Controls.Add(this.lblStripCentreUnit);
            topContainer.Panel2.Controls.Add(this.lblStripWidthUnit);
            topContainer.Panel2.Controls.Add(this.numTargetThickness);
            topContainer.Panel2.Controls.Add(this.numStripCentre);
            topContainer.Panel2.Controls.Add(lblTargetThickness);
            topContainer.Panel2.Controls.Add(this.numStripWidth);
            topContainer.Panel2.Controls.Add(lblStripCentre);
            topContainer.Panel2.Controls.Add(lblStripWidth);
            topContainer.Panel2.Controls.Add(this.btnQuickScan);
            topContainer.Panel2.Controls.Add(this.btnGalilMonitor);
            topContainer.Panel2.Controls.Add(this.txtPaintingDisplay);
            topContainer.Panel2.Controls.Add(this.chkRequirePainting);
            topContainer.Panel2.Controls.Add(this.chkRepeatReadings);
            topContainer.Panel2.Controls.Add(this.txtScanComment);
            topContainer.Panel2.Controls.Add(lblScanComment);
            topContainer.Panel2.Controls.Add(this.grpStripEdges);
            topContainer.Panel2.Controls.Add(this.btnClearReadings);
            topContainer.Panel2.Controls.Add(this.btnClearEdges);
            topContainer.Panel2.Controls.Add(this.btnPastResults);
            topContainer.Panel2.Controls.Add(this.btnConnect);
            topContainer.Panel2.Controls.Add(this.btnStop);
            topContainer.Panel2.Controls.Add(this.btnSettings);
            topContainer.Panel2.Controls.Add(this.btnRunWithoutEdgeScan);
            topContainer.Panel2.Controls.Add(this.btnRunWithEdgeScan);
            topContainer.Size = new System.Drawing.Size(883, 242);
            topContainer.SplitterDistance = 395;
            topContainer.TabIndex = 0;
            // 
            // zgcScanDisplay
            // 
            this.zgcScanDisplay.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.zgcScanDisplay.Dock = System.Windows.Forms.DockStyle.Fill;
            this.zgcScanDisplay.Location = new System.Drawing.Point(0, 0);
            this.zgcScanDisplay.Name = "zgcScanDisplay";
            this.zgcScanDisplay.ScrollGrace = 0D;
            this.zgcScanDisplay.ScrollMaxX = 0D;
            this.zgcScanDisplay.ScrollMaxY = 0D;
            this.zgcScanDisplay.ScrollMaxY2 = 0D;
            this.zgcScanDisplay.ScrollMinX = 0D;
            this.zgcScanDisplay.ScrollMinY = 0D;
            this.zgcScanDisplay.ScrollMinY2 = 0D;
            this.zgcScanDisplay.Size = new System.Drawing.Size(395, 242);
            this.zgcScanDisplay.TabIndex = 6;
            // 
            // lblTargetThicknessUnit
            // 
            this.lblTargetThicknessUnit.AutoSize = true;
            this.lblTargetThicknessUnit.Location = new System.Drawing.Point(445, 217);
            this.lblTargetThicknessUnit.Name = "lblTargetThicknessUnit";
            this.lblTargetThicknessUnit.Size = new System.Drawing.Size(38, 13);
            this.lblTargetThicknessUnit.TabIndex = 19;
            this.lblTargetThicknessUnit.Text = "micron";
            // 
            // lblStripCentreUnit
            // 
            this.lblStripCentreUnit.AutoSize = true;
            this.lblStripCentreUnit.Location = new System.Drawing.Point(279, 217);
            this.lblStripCentreUnit.Name = "lblStripCentreUnit";
            this.lblStripCentreUnit.Size = new System.Drawing.Size(23, 13);
            this.lblStripCentreUnit.TabIndex = 19;
            this.lblStripCentreUnit.Text = "mm";
            this.lblStripCentreUnit.Visible = false;
            // 
            // lblStripWidthUnit
            // 
            this.lblStripWidthUnit.AutoSize = true;
            this.lblStripWidthUnit.Location = new System.Drawing.Point(134, 216);
            this.lblStripWidthUnit.Name = "lblStripWidthUnit";
            this.lblStripWidthUnit.Size = new System.Drawing.Size(23, 13);
            this.lblStripWidthUnit.TabIndex = 19;
            this.lblStripWidthUnit.Text = "mm";
            // 
            // numTargetThickness
            // 
            this.numTargetThickness.DecimalPlaces = 2;
            this.numTargetThickness.Location = new System.Drawing.Point(397, 214);
            this.numTargetThickness.Maximum = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
            this.numTargetThickness.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.numTargetThickness.Name = "numTargetThickness";
            this.numTargetThickness.Size = new System.Drawing.Size(48, 20);
            this.numTargetThickness.TabIndex = 18;
            this.numTargetThickness.Value = new decimal(new int[] {
            20,
            0,
            0,
            0});
            // 
            // numStripCentre
            // 
            this.numStripCentre.Location = new System.Drawing.Point(226, 214);
            this.numStripCentre.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.numStripCentre.Name = "numStripCentre";
            this.numStripCentre.Size = new System.Drawing.Size(47, 20);
            this.numStripCentre.TabIndex = 18;
            this.numStripCentre.Value = new decimal(new int[] {
            31,
            0,
            0,
            0});
            this.numStripCentre.Visible = false;
            // 
            // lblTargetThickness
            // 
            lblTargetThickness.AutoSize = true;
            lblTargetThickness.Location = new System.Drawing.Point(305, 217);
            lblTargetThickness.Name = "lblTargetThickness";
            lblTargetThickness.Size = new System.Drawing.Size(90, 13);
            lblTargetThickness.TabIndex = 17;
            lblTargetThickness.Text = "Target Thickness";
            // 
            // numStripWidth
            // 
            this.numStripWidth.DecimalPlaces = 1;
            this.numStripWidth.Location = new System.Drawing.Point(73, 214);
            this.numStripWidth.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.numStripWidth.Name = "numStripWidth";
            this.numStripWidth.Size = new System.Drawing.Size(55, 20);
            this.numStripWidth.TabIndex = 18;
            // 
            // lblStripCentre
            // 
            lblStripCentre.AutoSize = true;
            lblStripCentre.Location = new System.Drawing.Point(162, 217);
            lblStripCentre.Name = "lblStripCentre";
            lblStripCentre.Size = new System.Drawing.Size(62, 13);
            lblStripCentre.TabIndex = 17;
            lblStripCentre.Text = "Strip Centre";
            lblStripCentre.Visible = false;
            // 
            // lblStripWidth
            // 
            lblStripWidth.AutoSize = true;
            lblStripWidth.Location = new System.Drawing.Point(9, 216);
            lblStripWidth.Name = "lblStripWidth";
            lblStripWidth.Size = new System.Drawing.Size(59, 13);
            lblStripWidth.TabIndex = 17;
            lblStripWidth.Text = "Strip Width";
            // 
            // btnQuickScan
            // 
            this.btnQuickScan.Location = new System.Drawing.Point(324, 184);
            this.btnQuickScan.Name = "btnQuickScan";
            this.btnQuickScan.Size = new System.Drawing.Size(75, 23);
            this.btnQuickScan.TabIndex = 16;
            this.btnQuickScan.Text = "Quick Scan";
            this.btnQuickScan.UseVisualStyleBackColor = true;
            // 
            // btnGalilMonitor
            // 
            this.btnGalilMonitor.Location = new System.Drawing.Point(403, 184);
            this.btnGalilMonitor.Name = "btnGalilMonitor";
            this.btnGalilMonitor.Size = new System.Drawing.Size(75, 23);
            this.btnGalilMonitor.TabIndex = 16;
            this.btnGalilMonitor.Text = "Galil Monitor";
            this.btnGalilMonitor.UseVisualStyleBackColor = true;
            // 
            // txtPaintingDisplay
            // 
            this.txtPaintingDisplay.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPaintingDisplay.Location = new System.Drawing.Point(149, 184);
            this.txtPaintingDisplay.Name = "txtPaintingDisplay";
            this.txtPaintingDisplay.ReadOnly = true;
            this.txtPaintingDisplay.Size = new System.Drawing.Size(132, 22);
            this.txtPaintingDisplay.TabIndex = 15;
            // 
            // chkRequirePainting
            // 
            this.chkRequirePainting.AutoSize = true;
            this.chkRequirePainting.Location = new System.Drawing.Point(12, 187);
            this.chkRequirePainting.Name = "chkRequirePainting";
            this.chkRequirePainting.Size = new System.Drawing.Size(131, 17);
            this.chkRequirePainting.TabIndex = 14;
            this.chkRequirePainting.Text = "Require Painting Input";
            this.chkRequirePainting.UseVisualStyleBackColor = true;
            // 
            // chkRepeatReadings
            // 
            this.chkRepeatReadings.AutoSize = true;
            this.chkRepeatReadings.Location = new System.Drawing.Point(324, 91);
            this.chkRepeatReadings.Name = "chkRepeatReadings";
            this.chkRepeatReadings.Size = new System.Drawing.Size(148, 17);
            this.chkRepeatReadings.TabIndex = 13;
            this.chkRepeatReadings.Text = "Repeat every 60 seconds";
            this.chkRepeatReadings.UseVisualStyleBackColor = true;
            // 
            // txtScanComment
            // 
            this.txtScanComment.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtScanComment.Location = new System.Drawing.Point(137, 5);
            this.txtScanComment.Name = "txtScanComment";
            this.txtScanComment.Size = new System.Drawing.Size(135, 26);
            this.txtScanComment.TabIndex = 12;
            // 
            // lblScanComment
            // 
            lblScanComment.AutoSize = true;
            lblScanComment.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            lblScanComment.Location = new System.Drawing.Point(8, 8);
            lblScanComment.Name = "lblScanComment";
            lblScanComment.Size = new System.Drawing.Size(123, 20);
            lblScanComment.TabIndex = 11;
            lblScanComment.Text = "Scan Comment:";
            // 
            // grpStripEdges
            // 
            this.grpStripEdges.Controls.Add(this.txtRightEdge);
            this.grpStripEdges.Controls.Add(this.txtLeftEdge);
            this.grpStripEdges.Location = new System.Drawing.Point(278, 6);
            this.grpStripEdges.Name = "grpStripEdges";
            this.grpStripEdges.Size = new System.Drawing.Size(200, 80);
            this.grpStripEdges.TabIndex = 10;
            this.grpStripEdges.TabStop = false;
            this.grpStripEdges.Text = "Strip Edges";
            // 
            // txtRightEdge
            // 
            this.txtRightEdge.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.txtRightEdge.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRightEdge.Location = new System.Drawing.Point(103, 20);
            this.txtRightEdge.Name = "txtRightEdge";
            this.txtRightEdge.ReadOnly = true;
            this.txtRightEdge.Size = new System.Drawing.Size(91, 49);
            this.txtRightEdge.TabIndex = 12;
            this.txtRightEdge.Text = "-";
            this.txtRightEdge.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtLeftEdge
            // 
            this.txtLeftEdge.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.txtLeftEdge.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLeftEdge.Location = new System.Drawing.Point(6, 20);
            this.txtLeftEdge.Name = "txtLeftEdge";
            this.txtLeftEdge.ReadOnly = true;
            this.txtLeftEdge.Size = new System.Drawing.Size(91, 49);
            this.txtLeftEdge.TabIndex = 11;
            this.txtLeftEdge.Text = "-";
            this.txtLeftEdge.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // btnClearReadings
            // 
            this.btnClearReadings.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClearReadings.Location = new System.Drawing.Point(137, 37);
            this.btnClearReadings.Name = "btnClearReadings";
            this.btnClearReadings.Size = new System.Drawing.Size(136, 31);
            this.btnClearReadings.TabIndex = 7;
            this.btnClearReadings.Text = "Clear Readings";
            this.btnClearReadings.UseVisualStyleBackColor = true;
            // 
            // btnClearEdges
            // 
            this.btnClearEdges.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClearEdges.Location = new System.Drawing.Point(138, 74);
            this.btnClearEdges.Name = "btnClearEdges";
            this.btnClearEdges.Size = new System.Drawing.Size(135, 31);
            this.btnClearEdges.TabIndex = 7;
            this.btnClearEdges.Text = "Clear Edges";
            this.btnClearEdges.UseVisualStyleBackColor = true;
            // 
            // btnPastResults
            // 
            this.btnPastResults.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPastResults.Location = new System.Drawing.Point(12, 37);
            this.btnPastResults.Name = "btnPastResults";
            this.btnPastResults.Size = new System.Drawing.Size(116, 31);
            this.btnPastResults.TabIndex = 7;
            this.btnPastResults.Text = "Database";
            this.btnPastResults.UseVisualStyleBackColor = true;
            // 
            // btnConnect
            // 
            this.btnConnect.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConnect.Location = new System.Drawing.Point(12, 74);
            this.btnConnect.Name = "btnConnect";
            this.btnConnect.Size = new System.Drawing.Size(116, 31);
            this.btnConnect.TabIndex = 7;
            this.btnConnect.Text = "Connect";
            this.btnConnect.UseVisualStyleBackColor = true;
            // 
            // btnStop
            // 
            this.btnStop.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStop.Location = new System.Drawing.Point(12, 148);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(116, 31);
            this.btnStop.TabIndex = 7;
            this.btnStop.Text = "Stop";
            this.btnStop.UseVisualStyleBackColor = true;
            // 
            // btnSettings
            // 
            this.btnSettings.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSettings.Location = new System.Drawing.Point(12, 111);
            this.btnSettings.Name = "btnSettings";
            this.btnSettings.Size = new System.Drawing.Size(116, 31);
            this.btnSettings.TabIndex = 7;
            this.btnSettings.Text = "Settings";
            this.btnSettings.UseVisualStyleBackColor = true;
            // 
            // btnRunWithoutEdgeScan
            // 
            this.btnRunWithoutEdgeScan.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRunWithoutEdgeScan.Location = new System.Drawing.Point(138, 111);
            this.btnRunWithoutEdgeScan.Name = "btnRunWithoutEdgeScan";
            this.btnRunWithoutEdgeScan.Size = new System.Drawing.Size(164, 68);
            this.btnRunWithoutEdgeScan.TabIndex = 9;
            this.btnRunWithoutEdgeScan.Text = "Take Readings Without Edge Scan";
            this.btnRunWithoutEdgeScan.UseVisualStyleBackColor = true;
            // 
            // btnRunWithEdgeScan
            // 
            this.btnRunWithEdgeScan.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRunWithEdgeScan.Location = new System.Drawing.Point(309, 111);
            this.btnRunWithEdgeScan.Name = "btnRunWithEdgeScan";
            this.btnRunWithEdgeScan.Size = new System.Drawing.Size(164, 68);
            this.btnRunWithEdgeScan.TabIndex = 8;
            this.btnRunWithEdgeScan.Text = "Take Readings With Edge Scan";
            this.btnRunWithEdgeScan.UseVisualStyleBackColor = true;
            // 
            // tblScanDisplays
            // 
            this.tblScanDisplays.ColumnCount = 7;
            this.tblScanDisplays.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tblScanDisplays.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tblScanDisplays.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tblScanDisplays.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.tblScanDisplays.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tblScanDisplays.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tblScanDisplays.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 21F));
            this.tblScanDisplays.Controls.Add(this.dfmScanDisplayControlLeft, 1, 0);
            this.tblScanDisplays.Controls.Add(this.dfmScanDisplayControlMiddle, 3, 0);
            this.tblScanDisplays.Controls.Add(this.dfmScanDisplayControlRight, 5, 0);
            this.tblScanDisplays.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tblScanDisplays.Location = new System.Drawing.Point(0, 0);
            this.tblScanDisplays.Name = "tblScanDisplays";
            this.tblScanDisplays.RowCount = 2;
            this.tblScanDisplays.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tblScanDisplays.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.tblScanDisplays.Size = new System.Drawing.Size(883, 378);
            this.tblScanDisplays.TabIndex = 0;
            // 
            // lblHeading
            // 
            lblHeading.AutoSize = true;
            lblHeading.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            lblHeading.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(83)))), ((int)(((byte)(84)))));
            lblHeading.Location = new System.Drawing.Point(93, 30);
            lblHeading.Name = "lblHeading";
            lblHeading.Size = new System.Drawing.Size(747, 25);
            lblHeading.TabIndex = 11;
            lblHeading.Text = "Wolf Innovation DFM1 Dynamic Paint Thickness Measurement System";
            // 
            // picLogo
            // 
            picLogo.Image = global::DFM.Properties.Resources.logo_notext;
            picLogo.InitialImage = null;
            picLogo.Location = new System.Drawing.Point(12, 12);
            picLogo.Name = "picLogo";
            picLogo.Size = new System.Drawing.Size(63, 60);
            picLogo.TabIndex = 12;
            picLogo.TabStop = false;
            // 
            // dfmScanDisplayControlLeft
            // 
            this.dfmScanDisplayControlLeft.BoundaryFiltering = false;
            this.dfmScanDisplayControlLeft.BoundaryFilteringThreshold = 0F;
            this.dfmScanDisplayControlLeft.BoundaryHigh = -1F;
            this.dfmScanDisplayControlLeft.BoundaryLow = -1F;
            this.dfmScanDisplayControlLeft.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dfmScanDisplayControlLeft.ErrorCaptionColour = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.dfmScanDisplayControlLeft.FontCaptionColour = System.Drawing.Color.Black;
            this.dfmScanDisplayControlLeft.GraphUnits = "microns";
            this.dfmScanDisplayControlLeft.Heading = "AISLE";
            this.dfmScanDisplayControlLeft.IsEmpty = true;
            this.dfmScanDisplayControlLeft.Location = new System.Drawing.Point(23, 3);
            this.dfmScanDisplayControlLeft.MainCaption = "No current scan";
            this.dfmScanDisplayControlLeft.MainText = "";
            this.dfmScanDisplayControlLeft.Name = "dfmScanDisplayControlLeft";
            this.dfmScanDisplayControlLeft.NumberFormat = "{0:0.0}";
            this.dfmScanDisplayControlLeft.ScaleFactor = null;
            this.dfmScanDisplayControlLeft.ScanAveragingNumber = 3;
            this.dfmScanDisplayControlLeft.Size = new System.Drawing.Size(254, 357);
            this.dfmScanDisplayControlLeft.SubCaption1 = "";
            this.dfmScanDisplayControlLeft.SubCaption2 = "";
            this.dfmScanDisplayControlLeft.SubCaption3 = "";
            this.dfmScanDisplayControlLeft.SubText1 = "";
            this.dfmScanDisplayControlLeft.SubText2 = "";
            this.dfmScanDisplayControlLeft.SubText3 = "";
            this.dfmScanDisplayControlLeft.TabIndex = 0;
            // 
            // dfmScanDisplayControlMiddle
            // 
            this.dfmScanDisplayControlMiddle.BoundaryFiltering = false;
            this.dfmScanDisplayControlMiddle.BoundaryFilteringThreshold = 0F;
            this.dfmScanDisplayControlMiddle.BoundaryHigh = -1F;
            this.dfmScanDisplayControlMiddle.BoundaryLow = -1F;
            this.dfmScanDisplayControlMiddle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dfmScanDisplayControlMiddle.ErrorCaptionColour = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.dfmScanDisplayControlMiddle.FontCaptionColour = System.Drawing.Color.Black;
            this.dfmScanDisplayControlMiddle.GraphUnits = "microns";
            this.dfmScanDisplayControlMiddle.Heading = "MIDDLE";
            this.dfmScanDisplayControlMiddle.IsEmpty = true;
            this.dfmScanDisplayControlMiddle.Location = new System.Drawing.Point(313, 3);
            this.dfmScanDisplayControlMiddle.MainCaption = "No current scan";
            this.dfmScanDisplayControlMiddle.MainText = "";
            this.dfmScanDisplayControlMiddle.Name = "dfmScanDisplayControlMiddle";
            this.dfmScanDisplayControlMiddle.NumberFormat = "{0:0.0}";
            this.dfmScanDisplayControlMiddle.ScaleFactor = null;
            this.dfmScanDisplayControlMiddle.ScanAveragingNumber = 3;
            this.dfmScanDisplayControlMiddle.Size = new System.Drawing.Size(254, 357);
            this.dfmScanDisplayControlMiddle.SubCaption1 = "";
            this.dfmScanDisplayControlMiddle.SubCaption2 = "";
            this.dfmScanDisplayControlMiddle.SubCaption3 = "";
            this.dfmScanDisplayControlMiddle.SubText1 = "";
            this.dfmScanDisplayControlMiddle.SubText2 = "";
            this.dfmScanDisplayControlMiddle.SubText3 = "";
            this.dfmScanDisplayControlMiddle.TabIndex = 1;
            // 
            // dfmScanDisplayControlRight
            // 
            this.dfmScanDisplayControlRight.BoundaryFiltering = false;
            this.dfmScanDisplayControlRight.BoundaryFilteringThreshold = 0F;
            this.dfmScanDisplayControlRight.BoundaryHigh = -1F;
            this.dfmScanDisplayControlRight.BoundaryLow = -1F;
            this.dfmScanDisplayControlRight.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dfmScanDisplayControlRight.ErrorCaptionColour = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.dfmScanDisplayControlRight.FontCaptionColour = System.Drawing.Color.Black;
            this.dfmScanDisplayControlRight.GraphUnits = "microns";
            this.dfmScanDisplayControlRight.Heading = "WALL";
            this.dfmScanDisplayControlRight.IsEmpty = true;
            this.dfmScanDisplayControlRight.Location = new System.Drawing.Point(603, 3);
            this.dfmScanDisplayControlRight.MainCaption = "No current scan";
            this.dfmScanDisplayControlRight.MainText = "";
            this.dfmScanDisplayControlRight.Name = "dfmScanDisplayControlRight";
            this.dfmScanDisplayControlRight.NumberFormat = "{0:0.0}";
            this.dfmScanDisplayControlRight.ScaleFactor = null;
            this.dfmScanDisplayControlRight.ScanAveragingNumber = 3;
            this.dfmScanDisplayControlRight.Size = new System.Drawing.Size(254, 357);
            this.dfmScanDisplayControlRight.SubCaption1 = "";
            this.dfmScanDisplayControlRight.SubCaption2 = "";
            this.dfmScanDisplayControlRight.SubCaption3 = "";
            this.dfmScanDisplayControlRight.SubText1 = "";
            this.dfmScanDisplayControlRight.SubText2 = "";
            this.dfmScanDisplayControlRight.SubText3 = "";
            this.dfmScanDisplayControlRight.TabIndex = 2;
            // 
            // DFM_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(884, 704);
            this.Controls.Add(picLogo);
            this.Controls.Add(lblHeading);
            this.Controls.Add(outerContainer);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(826, 100);
            this.Name = "DFM_Form";
            this.Text = "Wolf Innovation DFM1 Dynamic Paint Thickness Measurement System";
            outerContainer.Panel1.ResumeLayout(false);
            outerContainer.Panel2.ResumeLayout(false);
            outerContainer.ResumeLayout(false);
            topContainer.Panel1.ResumeLayout(false);
            topContainer.Panel2.ResumeLayout(false);
            topContainer.Panel2.PerformLayout();
            topContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.numTargetThickness)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numStripCentre)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numStripWidth)).EndInit();
            this.grpStripEdges.ResumeLayout(false);
            this.grpStripEdges.PerformLayout();
            this.tblScanDisplays.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(picLogo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private ZedGraph.ZedGraphControl zgcScanDisplay;
        private System.Windows.Forms.TextBox txtRightEdge;
        private System.Windows.Forms.TextBox txtLeftEdge;
        private System.Windows.Forms.Button btnConnect;
        private System.Windows.Forms.Button btnRunWithoutEdgeScan;
        private System.Windows.Forms.Button btnRunWithEdgeScan;
        private System.Windows.Forms.Button btnSettings;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.Button btnClearEdges;
        private System.Windows.Forms.TableLayoutPanel tblScanDisplays;
        private DFMScanDisplayControl dfmScanDisplayControlLeft;
        private DFMScanDisplayControl dfmScanDisplayControlMiddle;
        private DFMScanDisplayControl dfmScanDisplayControlRight;
        private System.Windows.Forms.GroupBox grpStripEdges;
        private System.Windows.Forms.TextBox txtScanComment;
        private System.Windows.Forms.Button btnPastResults;
        private System.Windows.Forms.Button btnClearReadings;
        private System.Windows.Forms.CheckBox chkRepeatReadings;
        private System.Windows.Forms.TextBox txtPaintingDisplay;
        private System.Windows.Forms.CheckBox chkRequirePainting;
        private System.Windows.Forms.Button btnGalilMonitor;
        private System.Windows.Forms.Button btnQuickScan;
        private System.Windows.Forms.Label lblStripCentreUnit;
        private System.Windows.Forms.Label lblStripWidthUnit;
        private System.Windows.Forms.NumericUpDown numStripCentre;
        private System.Windows.Forms.NumericUpDown numStripWidth;
        private System.Windows.Forms.Label lblTargetThicknessUnit;
        private System.Windows.Forms.NumericUpDown numTargetThickness;


    }
}