﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace DFM
{
    public partial class AlgorithmSelect : Form
    {
        public AlgorithmSelect()
        {
            InitializeComponent();
            this.FormClosing += new FormClosingEventHandler(AlgoSelect_closing);
        }

        void AlgoSelect_closing(object sender, FormClosingEventArgs e)
        {
            this.Hide();
            e.Cancel = true;
        }


        private void AlgorithmSelect_Load(object sender, EventArgs e)
        {

        }

        private void numYminAvg_ValueChanged(object sender, EventArgs e)
        {

        }
    }
}
