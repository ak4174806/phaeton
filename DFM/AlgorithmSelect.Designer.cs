﻿namespace DFM
{
    partial class AlgorithmSelect
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AlgorithmSelect));
            this.grpbxAlgo1 = new System.Windows.Forms.GroupBox();
            this.grpbxAlgo2 = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.nmHeight = new System.Windows.Forms.NumericUpDown();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.nmPointsBwAvgAlgo2 = new System.Windows.Forms.NumericUpDown();
            this.nmPoints4AvgAlgo2 = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.nmPercentage = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.nmDiffBwPits = new System.Windows.Forms.NumericUpDown();
            this.grpbxAlgo3 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.nmAlgo3AvgPoint = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.nmThresholdValue = new System.Windows.Forms.NumericUpDown();
            this.num2 = new System.Windows.Forms.Label();
            this.nmPointsBwAvg = new System.Windows.Forms.NumericUpDown();
            this.nmPoints4Avg = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.rdbtnAlgo1 = new System.Windows.Forms.RadioButton();
            this.rdbtnAlgo2 = new System.Windows.Forms.RadioButton();
            this.rdbtnAlgo3 = new System.Windows.Forms.RadioButton();
            this.btnRefreshReading = new System.Windows.Forms.Button();
            this.rdbtnAlgo4 = new System.Windows.Forms.RadioButton();
            this.grpbxAlgo4 = new System.Windows.Forms.GroupBox();
            this.label9 = new System.Windows.Forms.Label();
            this.nmHeightAlgo4 = new System.Windows.Forms.NumericUpDown();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label10 = new System.Windows.Forms.Label();
            this.nmPointsBwAvgAlgo4 = new System.Windows.Forms.NumericUpDown();
            this.nmPoints4AvgAlgo4 = new System.Windows.Forms.NumericUpDown();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.nmPercentageAlgo4 = new System.Windows.Forms.NumericUpDown();
            this.label13 = new System.Windows.Forms.Label();
            this.nmDiffBwPitsAlgo4 = new System.Windows.Forms.NumericUpDown();
            this.label14 = new System.Windows.Forms.Label();
            this.nmFactor = new System.Windows.Forms.NumericUpDown();
            this.Range = new System.Windows.Forms.Label();
            this.nmRange = new System.Windows.Forms.NumericUpDown();
            this.grpbxAlgo2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nmHeight)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nmPointsBwAvgAlgo2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmPoints4AvgAlgo2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmPercentage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmDiffBwPits)).BeginInit();
            this.grpbxAlgo3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nmAlgo3AvgPoint)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmThresholdValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmPointsBwAvg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmPoints4Avg)).BeginInit();
            this.grpbxAlgo4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nmHeightAlgo4)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nmPointsBwAvgAlgo4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmPoints4AvgAlgo4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmPercentageAlgo4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmDiffBwPitsAlgo4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmFactor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmRange)).BeginInit();
            this.SuspendLayout();
            // 
            // grpbxAlgo1
            // 
            this.grpbxAlgo1.Location = new System.Drawing.Point(12, 103);
            this.grpbxAlgo1.Name = "grpbxAlgo1";
            this.grpbxAlgo1.Size = new System.Drawing.Size(170, 276);
            this.grpbxAlgo1.TabIndex = 0;
            this.grpbxAlgo1.TabStop = false;
            this.grpbxAlgo1.Text = "Algorithm1";
            // 
            // grpbxAlgo2
            // 
            this.grpbxAlgo2.Controls.Add(this.label8);
            this.grpbxAlgo2.Controls.Add(this.nmHeight);
            this.grpbxAlgo2.Controls.Add(this.groupBox1);
            this.grpbxAlgo2.Controls.Add(this.label5);
            this.grpbxAlgo2.Controls.Add(this.nmPercentage);
            this.grpbxAlgo2.Controls.Add(this.label4);
            this.grpbxAlgo2.Controls.Add(this.nmDiffBwPits);
            this.grpbxAlgo2.Location = new System.Drawing.Point(188, 103);
            this.grpbxAlgo2.Name = "grpbxAlgo2";
            this.grpbxAlgo2.Size = new System.Drawing.Size(274, 276);
            this.grpbxAlgo2.TabIndex = 1;
            this.grpbxAlgo2.TabStop = false;
            this.grpbxAlgo2.Text = "Algorithm2";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(25, 184);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(38, 13);
            this.label8.TabIndex = 14;
            this.label8.Text = "Height";
            // 
            // nmHeight
            // 
            this.nmHeight.Location = new System.Drawing.Point(175, 181);
            this.nmHeight.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.nmHeight.Name = "nmHeight";
            this.nmHeight.Size = new System.Drawing.Size(76, 20);
            this.nmHeight.TabIndex = 13;
            this.nmHeight.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.nmPointsBwAvgAlgo2);
            this.groupBox1.Controls.Add(this.nmPoints4AvgAlgo2);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Location = new System.Drawing.Point(10, 24);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(258, 91);
            this.groupBox1.TabIndex = 12;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "For D Column";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(18, 53);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(115, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "No. of points b/w avgs";
            // 
            // nmPointsBwAvgAlgo2
            // 
            this.nmPointsBwAvgAlgo2.Location = new System.Drawing.Point(169, 53);
            this.nmPointsBwAvgAlgo2.Name = "nmPointsBwAvgAlgo2";
            this.nmPointsBwAvgAlgo2.Size = new System.Drawing.Size(70, 20);
            this.nmPointsBwAvgAlgo2.TabIndex = 8;
            this.nmPointsBwAvgAlgo2.Value = new decimal(new int[] {
            47,
            0,
            0,
            0});
            // 
            // nmPoints4AvgAlgo2
            // 
            this.nmPoints4AvgAlgo2.Location = new System.Drawing.Point(171, 22);
            this.nmPoints4AvgAlgo2.Name = "nmPoints4AvgAlgo2";
            this.nmPoints4AvgAlgo2.Size = new System.Drawing.Size(66, 20);
            this.nmPoints4AvgAlgo2.TabIndex = 7;
            this.nmPoints4AvgAlgo2.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(18, 26);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(133, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "No. of Points for averaging";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(24, 153);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(62, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "Percentage";
            // 
            // nmPercentage
            // 
            this.nmPercentage.Location = new System.Drawing.Point(174, 150);
            this.nmPercentage.Name = "nmPercentage";
            this.nmPercentage.Size = new System.Drawing.Size(76, 20);
            this.nmPercentage.TabIndex = 10;
            this.nmPercentage.Value = new decimal(new int[] {
            20,
            0,
            0,
            0});
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(26, 123);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(91, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Distance b/w Pits";
            // 
            // nmDiffBwPits
            // 
            this.nmDiffBwPits.Location = new System.Drawing.Point(175, 121);
            this.nmDiffBwPits.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.nmDiffBwPits.Name = "nmDiffBwPits";
            this.nmDiffBwPits.Size = new System.Drawing.Size(76, 20);
            this.nmDiffBwPits.TabIndex = 8;
            this.nmDiffBwPits.Value = new decimal(new int[] {
            200,
            0,
            0,
            0});
            // 
            // grpbxAlgo3
            // 
            this.grpbxAlgo3.Controls.Add(this.label6);
            this.grpbxAlgo3.Controls.Add(this.nmAlgo3AvgPoint);
            this.grpbxAlgo3.Controls.Add(this.label7);
            this.grpbxAlgo3.Controls.Add(this.nmThresholdValue);
            this.grpbxAlgo3.Controls.Add(this.num2);
            this.grpbxAlgo3.Controls.Add(this.nmPointsBwAvg);
            this.grpbxAlgo3.Controls.Add(this.nmPoints4Avg);
            this.grpbxAlgo3.Controls.Add(this.label1);
            this.grpbxAlgo3.Location = new System.Drawing.Point(469, 103);
            this.grpbxAlgo3.Name = "grpbxAlgo3";
            this.grpbxAlgo3.Size = new System.Drawing.Size(228, 276);
            this.grpbxAlgo3.TabIndex = 2;
            this.grpbxAlgo3.TabStop = false;
            this.grpbxAlgo3.Text = "Algorithm3";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(7, 154);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(55, 13);
            this.label6.TabIndex = 7;
            this.label6.Text = "AvgPoints";
            // 
            // nmAlgo3AvgPoint
            // 
            this.nmAlgo3AvgPoint.Location = new System.Drawing.Point(146, 147);
            this.nmAlgo3AvgPoint.Name = "nmAlgo3AvgPoint";
            this.nmAlgo3AvgPoint.Size = new System.Drawing.Size(76, 20);
            this.nmAlgo3AvgPoint.TabIndex = 6;
            this.nmAlgo3AvgPoint.Value = new decimal(new int[] {
            20,
            0,
            0,
            0});
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(7, 114);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(84, 13);
            this.label7.TabIndex = 5;
            this.label7.Text = "Threshold Value";
            // 
            // nmThresholdValue
            // 
            this.nmThresholdValue.DecimalPlaces = 5;
            this.nmThresholdValue.Location = new System.Drawing.Point(146, 107);
            this.nmThresholdValue.Name = "nmThresholdValue";
            this.nmThresholdValue.Size = new System.Drawing.Size(76, 20);
            this.nmThresholdValue.TabIndex = 4;
            this.nmThresholdValue.Value = new decimal(new int[] {
            15,
            0,
            0,
            262144});
            // 
            // num2
            // 
            this.num2.AutoSize = true;
            this.num2.Location = new System.Drawing.Point(7, 66);
            this.num2.Name = "num2";
            this.num2.Size = new System.Drawing.Size(115, 13);
            this.num2.TabIndex = 3;
            this.num2.Text = "No. of points b/w avgs";
            // 
            // nmPointsBwAvg
            // 
            this.nmPointsBwAvg.Location = new System.Drawing.Point(146, 64);
            this.nmPointsBwAvg.Name = "nmPointsBwAvg";
            this.nmPointsBwAvg.Size = new System.Drawing.Size(76, 20);
            this.nmPointsBwAvg.TabIndex = 2;
            this.nmPointsBwAvg.Value = new decimal(new int[] {
            47,
            0,
            0,
            0});
            // 
            // nmPoints4Avg
            // 
            this.nmPoints4Avg.Location = new System.Drawing.Point(146, 25);
            this.nmPoints4Avg.Name = "nmPoints4Avg";
            this.nmPoints4Avg.Size = new System.Drawing.Size(76, 20);
            this.nmPoints4Avg.TabIndex = 1;
            this.nmPoints4Avg.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(133, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "No. of Points for averaging";
            // 
            // rdbtnAlgo1
            // 
            this.rdbtnAlgo1.AutoSize = true;
            this.rdbtnAlgo1.Checked = true;
            this.rdbtnAlgo1.Location = new System.Drawing.Point(64, 68);
            this.rdbtnAlgo1.Name = "rdbtnAlgo1";
            this.rdbtnAlgo1.Size = new System.Drawing.Size(74, 17);
            this.rdbtnAlgo1.TabIndex = 3;
            this.rdbtnAlgo1.TabStop = true;
            this.rdbtnAlgo1.Text = "Algorithm1";
            this.rdbtnAlgo1.UseVisualStyleBackColor = true;
            // 
            // rdbtnAlgo2
            // 
            this.rdbtnAlgo2.AutoSize = true;
            this.rdbtnAlgo2.Location = new System.Drawing.Point(273, 68);
            this.rdbtnAlgo2.Name = "rdbtnAlgo2";
            this.rdbtnAlgo2.Size = new System.Drawing.Size(74, 17);
            this.rdbtnAlgo2.TabIndex = 4;
            this.rdbtnAlgo2.Text = "Algorithm2";
            this.rdbtnAlgo2.UseVisualStyleBackColor = true;
            // 
            // rdbtnAlgo3
            // 
            this.rdbtnAlgo3.AutoSize = true;
            this.rdbtnAlgo3.Location = new System.Drawing.Point(534, 68);
            this.rdbtnAlgo3.Name = "rdbtnAlgo3";
            this.rdbtnAlgo3.Size = new System.Drawing.Size(74, 17);
            this.rdbtnAlgo3.TabIndex = 5;
            this.rdbtnAlgo3.Text = "Algorithm3";
            this.rdbtnAlgo3.UseVisualStyleBackColor = true;
            // 
            // btnRefreshReading
            // 
            this.btnRefreshReading.Enabled = false;
            this.btnRefreshReading.Location = new System.Drawing.Point(12, 9);
            this.btnRefreshReading.Name = "btnRefreshReading";
            this.btnRefreshReading.Size = new System.Drawing.Size(77, 32);
            this.btnRefreshReading.TabIndex = 38;
            this.btnRefreshReading.Text = "Refresh";
            this.btnRefreshReading.UseVisualStyleBackColor = true;
            // 
            // rdbtnAlgo4
            // 
            this.rdbtnAlgo4.AutoSize = true;
            this.rdbtnAlgo4.Location = new System.Drawing.Point(797, 67);
            this.rdbtnAlgo4.Name = "rdbtnAlgo4";
            this.rdbtnAlgo4.Size = new System.Drawing.Size(74, 17);
            this.rdbtnAlgo4.TabIndex = 40;
            this.rdbtnAlgo4.Text = "Algorithm4";
            this.rdbtnAlgo4.UseVisualStyleBackColor = true;
            // 
            // grpbxAlgo4
            // 
            this.grpbxAlgo4.Controls.Add(this.label14);
            this.grpbxAlgo4.Controls.Add(this.nmFactor);
            this.grpbxAlgo4.Controls.Add(this.Range);
            this.grpbxAlgo4.Controls.Add(this.nmRange);
            this.grpbxAlgo4.Controls.Add(this.label9);
            this.grpbxAlgo4.Controls.Add(this.nmHeightAlgo4);
            this.grpbxAlgo4.Controls.Add(this.groupBox3);
            this.grpbxAlgo4.Controls.Add(this.label12);
            this.grpbxAlgo4.Controls.Add(this.nmPercentageAlgo4);
            this.grpbxAlgo4.Controls.Add(this.label13);
            this.grpbxAlgo4.Controls.Add(this.nmDiffBwPitsAlgo4);
            this.grpbxAlgo4.Location = new System.Drawing.Point(705, 102);
            this.grpbxAlgo4.Name = "grpbxAlgo4";
            this.grpbxAlgo4.Size = new System.Drawing.Size(274, 277);
            this.grpbxAlgo4.TabIndex = 39;
            this.grpbxAlgo4.TabStop = false;
            this.grpbxAlgo4.Text = "Algorithm4";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(25, 184);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(38, 13);
            this.label9.TabIndex = 14;
            this.label9.Text = "Height";
            // 
            // nmHeightAlgo4
            // 
            this.nmHeightAlgo4.Location = new System.Drawing.Point(175, 181);
            this.nmHeightAlgo4.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.nmHeightAlgo4.Name = "nmHeightAlgo4";
            this.nmHeightAlgo4.Size = new System.Drawing.Size(76, 20);
            this.nmHeightAlgo4.TabIndex = 13;
            this.nmHeightAlgo4.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Controls.Add(this.nmPointsBwAvgAlgo4);
            this.groupBox3.Controls.Add(this.nmPoints4AvgAlgo4);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Location = new System.Drawing.Point(10, 24);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(258, 91);
            this.groupBox3.TabIndex = 12;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "For D Column";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(18, 53);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(115, 13);
            this.label10.TabIndex = 9;
            this.label10.Text = "No. of points b/w avgs";
            // 
            // nmPointsBwAvgAlgo4
            // 
            this.nmPointsBwAvgAlgo4.Location = new System.Drawing.Point(169, 53);
            this.nmPointsBwAvgAlgo4.Name = "nmPointsBwAvgAlgo4";
            this.nmPointsBwAvgAlgo4.Size = new System.Drawing.Size(70, 20);
            this.nmPointsBwAvgAlgo4.TabIndex = 8;
            this.nmPointsBwAvgAlgo4.Value = new decimal(new int[] {
            47,
            0,
            0,
            0});
            // 
            // nmPoints4AvgAlgo4
            // 
            this.nmPoints4AvgAlgo4.Location = new System.Drawing.Point(171, 22);
            this.nmPoints4AvgAlgo4.Name = "nmPoints4AvgAlgo4";
            this.nmPoints4AvgAlgo4.Size = new System.Drawing.Size(66, 20);
            this.nmPoints4AvgAlgo4.TabIndex = 7;
            this.nmPoints4AvgAlgo4.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(18, 26);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(133, 13);
            this.label11.TabIndex = 6;
            this.label11.Text = "No. of Points for averaging";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(24, 153);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(62, 13);
            this.label12.TabIndex = 11;
            this.label12.Text = "Percentage";
            // 
            // nmPercentageAlgo4
            // 
            this.nmPercentageAlgo4.Location = new System.Drawing.Point(174, 150);
            this.nmPercentageAlgo4.Name = "nmPercentageAlgo4";
            this.nmPercentageAlgo4.Size = new System.Drawing.Size(76, 20);
            this.nmPercentageAlgo4.TabIndex = 10;
            this.nmPercentageAlgo4.Value = new decimal(new int[] {
            20,
            0,
            0,
            0});
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(26, 123);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(91, 13);
            this.label13.TabIndex = 9;
            this.label13.Text = "Distance b/w Pits";
            // 
            // nmDiffBwPitsAlgo4
            // 
            this.nmDiffBwPitsAlgo4.Location = new System.Drawing.Point(175, 121);
            this.nmDiffBwPitsAlgo4.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.nmDiffBwPitsAlgo4.Name = "nmDiffBwPitsAlgo4";
            this.nmDiffBwPitsAlgo4.Size = new System.Drawing.Size(76, 20);
            this.nmDiffBwPitsAlgo4.TabIndex = 8;
            this.nmDiffBwPitsAlgo4.Value = new decimal(new int[] {
            200,
            0,
            0,
            0});
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(26, 244);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(37, 13);
            this.label14.TabIndex = 18;
            this.label14.Text = "Factor";
            // 
            // nmFactor
            // 
            this.nmFactor.DecimalPlaces = 4;
            this.nmFactor.Location = new System.Drawing.Point(176, 241);
            this.nmFactor.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.nmFactor.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            196608});
            this.nmFactor.Name = "nmFactor";
            this.nmFactor.Size = new System.Drawing.Size(76, 20);
            this.nmFactor.TabIndex = 17;
            this.nmFactor.Value = new decimal(new int[] {
            1,
            0,
            0,
            196608});
            // 
            // Range
            // 
            this.Range.AutoSize = true;
            this.Range.Location = new System.Drawing.Point(25, 213);
            this.Range.Name = "Range";
            this.Range.Size = new System.Drawing.Size(39, 13);
            this.Range.TabIndex = 16;
            this.Range.Text = "Range";
            // 
            // nmRange
            // 
            this.nmRange.Location = new System.Drawing.Point(175, 210);
            this.nmRange.Name = "nmRange";
            this.nmRange.Size = new System.Drawing.Size(76, 20);
            this.nmRange.TabIndex = 15;
            this.nmRange.Value = new decimal(new int[] {
            50,
            0,
            0,
            0});
            // 
            // AlgorithmSelect
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1012, 399);
            this.Controls.Add(this.rdbtnAlgo4);
            this.Controls.Add(this.grpbxAlgo4);
            this.Controls.Add(this.btnRefreshReading);
            this.Controls.Add(this.rdbtnAlgo3);
            this.Controls.Add(this.rdbtnAlgo2);
            this.Controls.Add(this.rdbtnAlgo1);
            this.Controls.Add(this.grpbxAlgo3);
            this.Controls.Add(this.grpbxAlgo2);
            this.Controls.Add(this.grpbxAlgo1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "AlgorithmSelect";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "AlgorithmSelect";
            this.Load += new System.EventHandler(this.AlgorithmSelect_Load);
            this.grpbxAlgo2.ResumeLayout(false);
            this.grpbxAlgo2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nmHeight)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nmPointsBwAvgAlgo2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmPoints4AvgAlgo2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmPercentage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmDiffBwPits)).EndInit();
            this.grpbxAlgo3.ResumeLayout(false);
            this.grpbxAlgo3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nmAlgo3AvgPoint)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmThresholdValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmPointsBwAvg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmPoints4Avg)).EndInit();
            this.grpbxAlgo4.ResumeLayout(false);
            this.grpbxAlgo4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nmHeightAlgo4)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nmPointsBwAvgAlgo4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmPoints4AvgAlgo4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmPercentageAlgo4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmDiffBwPitsAlgo4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmFactor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmRange)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.GroupBox grpbxAlgo1;
        public System.Windows.Forms.GroupBox grpbxAlgo2;
        public System.Windows.Forms.GroupBox grpbxAlgo3;
        public System.Windows.Forms.RadioButton rdbtnAlgo1;
        public System.Windows.Forms.RadioButton rdbtnAlgo2;
        public System.Windows.Forms.RadioButton rdbtnAlgo3;
        private System.Windows.Forms.Label label7;
        public System.Windows.Forms.NumericUpDown nmThresholdValue;
        private System.Windows.Forms.Label num2;
        public System.Windows.Forms.NumericUpDown nmPointsBwAvg;
        public System.Windows.Forms.NumericUpDown nmPoints4Avg;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.Button btnRefreshReading;
        private System.Windows.Forms.Label label6;
        public System.Windows.Forms.NumericUpDown nmAlgo3AvgPoint;
        private System.Windows.Forms.Label label5;
        public System.Windows.Forms.NumericUpDown nmPercentage;
        private System.Windows.Forms.Label label4;
        public System.Windows.Forms.NumericUpDown nmDiffBwPits;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label2;
        public System.Windows.Forms.NumericUpDown nmPointsBwAvgAlgo2;
        public System.Windows.Forms.NumericUpDown nmPoints4AvgAlgo2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label8;
        public System.Windows.Forms.NumericUpDown nmHeight;
        public System.Windows.Forms.RadioButton rdbtnAlgo4;
        public System.Windows.Forms.GroupBox grpbxAlgo4;
        private System.Windows.Forms.Label label9;
        public System.Windows.Forms.NumericUpDown nmHeightAlgo4;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label10;
        public System.Windows.Forms.NumericUpDown nmPointsBwAvgAlgo4;
        public System.Windows.Forms.NumericUpDown nmPoints4AvgAlgo4;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        public System.Windows.Forms.NumericUpDown nmPercentageAlgo4;
        private System.Windows.Forms.Label label13;
        public System.Windows.Forms.NumericUpDown nmDiffBwPitsAlgo4;
        private System.Windows.Forms.Label label14;
        public System.Windows.Forms.NumericUpDown nmFactor;
        private System.Windows.Forms.Label Range;
        public System.Windows.Forms.NumericUpDown nmRange;
    }
}