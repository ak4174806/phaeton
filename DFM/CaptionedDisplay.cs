﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace DFM
{
    [DefaultProperty("Text")]
    public partial class CaptionedDisplay : UserControl
    {
        public CaptionedDisplay()
        {
            InitializeComponent();            
        }

        [Browsable(true)]
        [Category("Appearance")]
        public override string Text
        {
            get
            {
                return lblValue.Text;
            }
            set
            {
                Settings.Bindable.ThreadSafeControlUpdates.UpdateText(lblValue, value);
                //lblValue.Text = value;
            }
        }

        [Browsable(true)]
        [Category("Appearance")]
        public string Caption
        {
            get
            {
                return lblCaption.Text;
            }
            set
            {
                Settings.Bindable.ThreadSafeControlUpdates.UpdateText(lblCaption, value);
                //lblCaption.Text = value;
            }
        }

        [Browsable(true)]
        [Category("Appearance")]
        public float TextFontSize
        {
            get
            {
                return lblValue.Font.Size;
            }
            set
            {
                lblValue.Font = new System.Drawing.Font(lblValue.Font.FontFamily, value);
            }
        }

        [Browsable(true)]
        [Category("Appearance")]
        public float CaptionFontSize
        {
            get
            {
                return lblCaption.Font.Size;
            }
            set
            {
                Point tmp = new Point(lblCaption.Location.X, lblCaption.Location.Y + lblCaption.Size.Height);
                Size tmp2 = lblCaption.Size;
                lblCaption.Font = new System.Drawing.Font(lblCaption.Font.FontFamily, value);
                lblCaption.AutoSize = true;
                lblCaption.AutoSize = false;
                lblCaption.Location = new Point(tmp.X, tmp.Y - lblCaption.Size.Height);
                lblCaption.Size = new Size(tmp2.Width, lblCaption.Size.Height);
            }
        }
        
        private Color fontColour;
        [Browsable(true)]
        [Category("Appearance")]
        public Color FontColour
        {
            get { return fontColour; }
            set
            {
                fontColour = value;
                updateColours();
            }
        }

        private void updateColours()
        {
            lblValue.ForeColor = FontColour;
        }
    }
}
