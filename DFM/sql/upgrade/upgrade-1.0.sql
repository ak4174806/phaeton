-- add unit to all pit depths
UPDATE [scan_results] SET pit_depth = pit_depth || ' micron';

-- update version number
UPDATE keystore SET [value]='1.1' WHERE [key]='database_version';