DROP TABLE IF EXISTS scan_results;
DROP TABLE IF EXISTS keystore; 

CREATE TABLE scan_results 
(
	result_id INTEGER PRIMARY KEY, -- unique identifier of scan
	date INTEGER NOT NULL, 
	pit_depth TEXT NOT NULL, 
	num_pits INTEGER NOT NULL,
	scan_position INTEGER NOT NULL,
	scan_path TEXT NOT NULL, 
	scan_id INTEGER NOT NULL, -- scan id as used in filename of scan
	comment TEXT
);

CREATE TABLE keystore
(
	key TEXT PRIMARY KEY,
	value TEXT
);

INSERT INTO keystore VALUES 
(
	'database_version', '1.0'
);