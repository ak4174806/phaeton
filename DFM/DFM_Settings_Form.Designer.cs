﻿namespace DFM
{
    partial class DFM_Settings_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.Label lblSensorBaudrate;
            System.Windows.Forms.Label lblSensorType;
            System.Windows.Forms.Label lblSensorModel;
            System.Windows.Forms.Label lblLaserBaudrate;
            System.Windows.Forms.Label lblLaserPort;
            System.Windows.Forms.Label lblMotorAddress;
            System.Windows.Forms.Label label6;
            System.Windows.Forms.Label lblDAQStop;
            System.Windows.Forms.Label label7;
            System.Windows.Forms.Label label12;
            System.Windows.Forms.Label lblRepeatInterval;
            System.Windows.Forms.Label label9;
            System.Windows.Forms.Label lblLaserStart;
            System.Windows.Forms.Label label8;
            System.Windows.Forms.Label lblGatingFreq;
            System.Windows.Forms.Label lblDutyCycle;
            System.Windows.Forms.Label label13;
            System.Windows.Forms.Label lblMotorRightPos;
            System.Windows.Forms.Label lblMotorMiddlePos;
            System.Windows.Forms.Label label20;
            System.Windows.Forms.Label lblTruncateFirst;
            System.Windows.Forms.Label label22;
            System.Windows.Forms.Label lblTruncateLast;
            System.Windows.Forms.Label lblMaxPeakWidth;
            System.Windows.Forms.Label lblPeakHysteresis;
            System.Windows.Forms.Label lblPeakThreshold;
            System.Windows.Forms.Label lblPitPoints;
            System.Windows.Forms.Label lblSubstrateRoughness;
            System.Windows.Forms.Label lblCompressionFactor;
            System.Windows.Forms.Label lblSmoothingWidth;
            System.Windows.Forms.Label lblMinPitWidth;
            System.Windows.Forms.Label lblThresholdParam;
            System.Windows.Forms.Label lblFilenamePattern;
            System.Windows.Forms.Label lblDataLocation;
            System.Windows.Forms.TabPage tpScanTiming;
            System.Windows.Forms.Label label3;
            System.Windows.Forms.Label lblDAQStart;
            System.Windows.Forms.TabPage tpMotorDetails;
            System.Windows.Forms.Label lblMotorPositionLabels;
            System.Windows.Forms.Label lblWhere;
            System.Windows.Forms.Label lblMotorLeftPos;
            System.Windows.Forms.Label label10;
            System.Windows.Forms.Label lblScanAveNumber;
            System.Windows.Forms.Label lblUnits;
            System.Windows.Forms.Label lblFilterErrorsFiller;
            System.Windows.Forms.Label lblPaintingDelay;
            System.Windows.Forms.Label label2;
            System.Windows.Forms.Label lblLaserHandshake;
            this.chkRepeatScan = new System.Windows.Forms.CheckBox();
            this.numRepeatScanInterval = new System.Windows.Forms.NumericUpDown();
            this.chkHomeAfterScans = new System.Windows.Forms.CheckBox();
            this.chkRepeat = new System.Windows.Forms.CheckBox();
            this.numDAQStart = new System.Windows.Forms.NumericUpDown();
            this.numLaserStart = new System.Windows.Forms.NumericUpDown();
            this.numDAQStop = new System.Windows.Forms.NumericUpDown();
            this.numRepeatCount = new System.Windows.Forms.NumericUpDown();
            this.numRepeatInterval = new System.Windows.Forms.NumericUpDown();
            this.txtMotorRightPositionLabel = new System.Windows.Forms.TextBox();
            this.txtMotorMiddlePositionLabel = new System.Windows.Forms.TextBox();
            this.txtMotorLeftPositionLabel = new System.Windows.Forms.TextBox();
            this.numMotorLeftPos = new System.Windows.Forms.NumericUpDown();
            this.lblMotorUnit1 = new System.Windows.Forms.Label();
            this.numMotorMiddlePos = new System.Windows.Forms.NumericUpDown();
            this.lblMotorUnit2 = new System.Windows.Forms.Label();
            this.chkCentreMiddle = new System.Windows.Forms.CheckBox();
            this.chkSkipMotorLeftPos = new System.Windows.Forms.CheckBox();
            this.chkSkipMotorMiddlePos = new System.Windows.Forms.CheckBox();
            this.numMotorRightPos = new System.Windows.Forms.NumericUpDown();
            this.lblMotorUnit3 = new System.Windows.Forms.Label();
            this.chkSkipMotorRightPos = new System.Windows.Forms.CheckBox();
            this.lblSensorPort = new System.Windows.Forms.Label();
            this.lblBoundaryLines = new System.Windows.Forms.Label();
            this.controlButtonsContainer = new System.Windows.Forms.SplitContainer();
            this.tabSettings = new System.Windows.Forms.TabControl();
            this.tpConnection = new System.Windows.Forms.TabPage();
            this.btnRefreshCOMPorts = new System.Windows.Forms.Button();
            this.btnConnect = new System.Windows.Forms.Button();
            this.tabConnection = new System.Windows.Forms.TabControl();
            this.tpSensorConnection = new System.Windows.Forms.TabPage();
            this.txtSensorAddress = new System.Windows.Forms.TextBox();
            this.cmbSensorBaudrate = new System.Windows.Forms.ComboBox();
            this.cmbSensorPort = new System.Windows.Forms.ComboBox();
            this.cmbSensorType = new System.Windows.Forms.ComboBox();
            this.cmbSensorModel = new System.Windows.Forms.ComboBox();
            this.chkConnectSensor = new System.Windows.Forms.CheckBox();
            this.tpLaserConnection = new System.Windows.Forms.TabPage();
            this.cmbLaserBaudrate = new System.Windows.Forms.ComboBox();
            this.cmbLaserHandshake = new System.Windows.Forms.ComboBox();
            this.cmbLaserPort = new System.Windows.Forms.ComboBox();
            this.chkConnectLaser = new System.Windows.Forms.CheckBox();
            this.tpMotorConnection = new System.Windows.Forms.TabPage();
            this.cmbMotorAddress = new System.Windows.Forms.ComboBox();
            this.chkConnectMotor = new System.Windows.Forms.CheckBox();
            this.tpScanParameters = new System.Windows.Forms.TabPage();
            this.tabScanParameters = new System.Windows.Forms.TabControl();
            this.tpLaserDetails = new System.Windows.Forms.TabPage();
            this.chkDisplayPulseCountErrorMessages = new System.Windows.Forms.CheckBox();
            this.chkFilterPulseCountErrors = new System.Windows.Forms.CheckBox();
            this.chkValidatePulseCount = new System.Windows.Forms.CheckBox();
            this.radPulseCount = new System.Windows.Forms.RadioButton();
            this.numPulseCount = new System.Windows.Forms.NumericUpDown();
            this.radRunTime = new System.Windows.Forms.RadioButton();
            this.numRunTime = new System.Windows.Forms.NumericUpDown();
            this.numGatingFreq = new System.Windows.Forms.NumericUpDown();
            this.numDutyCycle = new System.Windows.Forms.NumericUpDown();
            this.tpMiscScan = new System.Windows.Forms.TabPage();
            this.numPaintingDelay = new System.Windows.Forms.NumericUpDown();
            this.chkRequirePainting = new System.Windows.Forms.CheckBox();
            this.tpDataAnalysis = new System.Windows.Forms.TabPage();
            this.tabDataAnalysis = new System.Windows.Forms.TabControl();
            this.tpDataProcessing = new System.Windows.Forms.TabPage();
            this.numTruncateFirst = new System.Windows.Forms.NumericUpDown();
            this.numTruncateLast = new System.Windows.Forms.NumericUpDown();
            this.chkFilterPeaks = new System.Windows.Forms.CheckBox();
            this.numPeakThreshold = new System.Windows.Forms.NumericUpDown();
            this.numPeakHysteresis = new System.Windows.Forms.NumericUpDown();
            this.numMaxPeakWidth = new System.Windows.Forms.NumericUpDown();
            this.tpAnalysisParameters = new System.Windows.Forms.TabPage();
            this.numSubstrateRoughness = new System.Windows.Forms.NumericUpDown();
            this.numThresholdParam = new System.Windows.Forms.NumericUpDown();
            this.numSmoothingWidth = new System.Windows.Forms.NumericUpDown();
            this.numMinPitWidth = new System.Windows.Forms.NumericUpDown();
            this.numCompressionFactor = new System.Windows.Forms.NumericUpDown();
            this.numPitPoints = new System.Windows.Forms.NumericUpDown();
            this.tpDataLocation = new System.Windows.Forms.TabPage();
            this.numFilterErrorsThreshold = new System.Windows.Forms.NumericUpDown();
            this.chkFilterErrors = new System.Windows.Forms.CheckBox();
            this.cmbUnits = new System.Windows.Forms.ComboBox();
            this.numScanAveNumber = new System.Windows.Forms.NumericUpDown();
            this.numThicknessRange = new System.Windows.Forms.NumericUpDown();
            this.numTargetThickness = new System.Windows.Forms.NumericUpDown();
            this.txtDataLocation = new System.Windows.Forms.TextBox();
            this.btnChooseDataLocation = new System.Windows.Forms.Button();
            this.txtFilenamePattern = new System.Windows.Forms.TextBox();
            this.tpMisc = new System.Windows.Forms.TabPage();
            this.chkEnableQuickScan = new System.Windows.Forms.CheckBox();
            this.btnChangePassword = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.lblHeading = new System.Windows.Forms.Label();
            this.outerContainer = new System.Windows.Forms.SplitContainer();
            this.folderBrowser = new System.Windows.Forms.FolderBrowserDialog();
            lblSensorBaudrate = new System.Windows.Forms.Label();
            lblSensorType = new System.Windows.Forms.Label();
            lblSensorModel = new System.Windows.Forms.Label();
            lblLaserBaudrate = new System.Windows.Forms.Label();
            lblLaserPort = new System.Windows.Forms.Label();
            lblMotorAddress = new System.Windows.Forms.Label();
            label6 = new System.Windows.Forms.Label();
            lblDAQStop = new System.Windows.Forms.Label();
            label7 = new System.Windows.Forms.Label();
            label12 = new System.Windows.Forms.Label();
            lblRepeatInterval = new System.Windows.Forms.Label();
            label9 = new System.Windows.Forms.Label();
            lblLaserStart = new System.Windows.Forms.Label();
            label8 = new System.Windows.Forms.Label();
            lblGatingFreq = new System.Windows.Forms.Label();
            lblDutyCycle = new System.Windows.Forms.Label();
            label13 = new System.Windows.Forms.Label();
            lblMotorRightPos = new System.Windows.Forms.Label();
            lblMotorMiddlePos = new System.Windows.Forms.Label();
            label20 = new System.Windows.Forms.Label();
            lblTruncateFirst = new System.Windows.Forms.Label();
            label22 = new System.Windows.Forms.Label();
            lblTruncateLast = new System.Windows.Forms.Label();
            lblMaxPeakWidth = new System.Windows.Forms.Label();
            lblPeakHysteresis = new System.Windows.Forms.Label();
            lblPeakThreshold = new System.Windows.Forms.Label();
            lblPitPoints = new System.Windows.Forms.Label();
            lblSubstrateRoughness = new System.Windows.Forms.Label();
            lblCompressionFactor = new System.Windows.Forms.Label();
            lblSmoothingWidth = new System.Windows.Forms.Label();
            lblMinPitWidth = new System.Windows.Forms.Label();
            lblThresholdParam = new System.Windows.Forms.Label();
            lblFilenamePattern = new System.Windows.Forms.Label();
            lblDataLocation = new System.Windows.Forms.Label();
            tpScanTiming = new System.Windows.Forms.TabPage();
            label3 = new System.Windows.Forms.Label();
            lblDAQStart = new System.Windows.Forms.Label();
            tpMotorDetails = new System.Windows.Forms.TabPage();
            lblMotorPositionLabels = new System.Windows.Forms.Label();
            lblWhere = new System.Windows.Forms.Label();
            lblMotorLeftPos = new System.Windows.Forms.Label();
            label10 = new System.Windows.Forms.Label();
            lblScanAveNumber = new System.Windows.Forms.Label();
            lblUnits = new System.Windows.Forms.Label();
            lblFilterErrorsFiller = new System.Windows.Forms.Label();
            lblPaintingDelay = new System.Windows.Forms.Label();
            label2 = new System.Windows.Forms.Label();
            lblLaserHandshake = new System.Windows.Forms.Label();
            tpScanTiming.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numRepeatScanInterval)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numDAQStart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numLaserStart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numDAQStop)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numRepeatCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numRepeatInterval)).BeginInit();
            tpMotorDetails.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numMotorLeftPos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numMotorMiddlePos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numMotorRightPos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.controlButtonsContainer)).BeginInit();
            this.controlButtonsContainer.Panel1.SuspendLayout();
            this.controlButtonsContainer.Panel2.SuspendLayout();
            this.controlButtonsContainer.SuspendLayout();
            this.tabSettings.SuspendLayout();
            this.tpConnection.SuspendLayout();
            this.tabConnection.SuspendLayout();
            this.tpSensorConnection.SuspendLayout();
            this.tpLaserConnection.SuspendLayout();
            this.tpMotorConnection.SuspendLayout();
            this.tpScanParameters.SuspendLayout();
            this.tabScanParameters.SuspendLayout();
            this.tpLaserDetails.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numPulseCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numRunTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numGatingFreq)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numDutyCycle)).BeginInit();
            this.tpMiscScan.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numPaintingDelay)).BeginInit();
            this.tpDataAnalysis.SuspendLayout();
            this.tabDataAnalysis.SuspendLayout();
            this.tpDataProcessing.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numTruncateFirst)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numTruncateLast)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPeakThreshold)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPeakHysteresis)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numMaxPeakWidth)).BeginInit();
            this.tpAnalysisParameters.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numSubstrateRoughness)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numThresholdParam)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSmoothingWidth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numMinPitWidth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numCompressionFactor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPitPoints)).BeginInit();
            this.tpDataLocation.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numFilterErrorsThreshold)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numScanAveNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numThicknessRange)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numTargetThickness)).BeginInit();
            this.tpMisc.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.outerContainer)).BeginInit();
            this.outerContainer.Panel1.SuspendLayout();
            this.outerContainer.Panel2.SuspendLayout();
            this.outerContainer.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblSensorBaudrate
            // 
            lblSensorBaudrate.AutoSize = true;
            lblSensorBaudrate.Location = new System.Drawing.Point(168, 69);
            lblSensorBaudrate.Name = "lblSensorBaudrate";
            lblSensorBaudrate.Size = new System.Drawing.Size(53, 13);
            lblSensorBaudrate.TabIndex = 43;
            lblSensorBaudrate.Text = "Baudrate:";
            // 
            // lblSensorType
            // 
            lblSensorType.AutoSize = true;
            lblSensorType.Location = new System.Drawing.Point(168, 42);
            lblSensorType.Name = "lblSensorType";
            lblSensorType.Size = new System.Drawing.Size(34, 13);
            lblSensorType.TabIndex = 36;
            lblSensorType.Text = "Type:";
            // 
            // lblSensorModel
            // 
            lblSensorModel.AutoSize = true;
            lblSensorModel.Location = new System.Drawing.Point(9, 42);
            lblSensorModel.Name = "lblSensorModel";
            lblSensorModel.Size = new System.Drawing.Size(39, 13);
            lblSensorModel.TabIndex = 37;
            lblSensorModel.Text = "Model:";
            // 
            // lblLaserBaudrate
            // 
            lblLaserBaudrate.AutoSize = true;
            lblLaserBaudrate.Location = new System.Drawing.Point(16, 70);
            lblLaserBaudrate.Name = "lblLaserBaudrate";
            lblLaserBaudrate.Size = new System.Drawing.Size(53, 13);
            lblLaserBaudrate.TabIndex = 47;
            lblLaserBaudrate.Text = "Baudrate:";
            // 
            // lblLaserPort
            // 
            lblLaserPort.AutoSize = true;
            lblLaserPort.Location = new System.Drawing.Point(15, 44);
            lblLaserPort.Name = "lblLaserPort";
            lblLaserPort.Size = new System.Drawing.Size(29, 13);
            lblLaserPort.TabIndex = 44;
            lblLaserPort.Text = "Port:";
            // 
            // lblMotorAddress
            // 
            lblMotorAddress.AutoSize = true;
            lblMotorAddress.Location = new System.Drawing.Point(15, 44);
            lblMotorAddress.Name = "lblMotorAddress";
            lblMotorAddress.Size = new System.Drawing.Size(48, 13);
            lblMotorAddress.TabIndex = 48;
            lblMotorAddress.Text = "Address:";
            // 
            // label6
            // 
            label6.AutoSize = true;
            label6.Location = new System.Drawing.Point(215, 17);
            label6.Name = "label6";
            label6.Size = new System.Drawing.Size(20, 13);
            label6.TabIndex = 34;
            label6.Text = "ms";
            // 
            // lblDAQStop
            // 
            lblDAQStop.AutoSize = true;
            lblDAQStop.Location = new System.Drawing.Point(17, 68);
            lblDAQStop.Name = "lblDAQStop";
            lblDAQStop.Size = new System.Drawing.Size(82, 13);
            lblDAQStop.TabIndex = 38;
            lblDAQStop.Text = "Stop DAQ after:";
            // 
            // label7
            // 
            label7.AutoSize = true;
            label7.Location = new System.Drawing.Point(215, 44);
            label7.Name = "label7";
            label7.Size = new System.Drawing.Size(20, 13);
            label7.TabIndex = 37;
            label7.Text = "ms";
            // 
            // label12
            // 
            label12.AutoSize = true;
            label12.Location = new System.Drawing.Point(293, 98);
            label12.Name = "label12";
            label12.Size = new System.Drawing.Size(12, 13);
            label12.TabIndex = 40;
            label12.Text = "s";
            // 
            // lblRepeatInterval
            // 
            lblRepeatInterval.AutoSize = true;
            lblRepeatInterval.Location = new System.Drawing.Point(172, 98);
            lblRepeatInterval.Name = "lblRepeatInterval";
            lblRepeatInterval.Size = new System.Drawing.Size(60, 13);
            lblRepeatInterval.TabIndex = 40;
            lblRepeatInterval.Text = "times every";
            // 
            // label9
            // 
            label9.AutoSize = true;
            label9.Location = new System.Drawing.Point(215, 69);
            label9.Name = "label9";
            label9.Size = new System.Drawing.Size(20, 13);
            label9.TabIndex = 40;
            label9.Text = "ms";
            // 
            // lblLaserStart
            // 
            lblLaserStart.AutoSize = true;
            lblLaserStart.Location = new System.Drawing.Point(17, 43);
            lblLaserStart.Name = "lblLaserStart";
            lblLaserStart.Size = new System.Drawing.Size(81, 13);
            lblLaserStart.TabIndex = 35;
            lblLaserStart.Text = "Start laser after:";
            // 
            // label8
            // 
            label8.AutoSize = true;
            label8.Location = new System.Drawing.Point(210, 105);
            label8.Name = "label8";
            label8.Size = new System.Drawing.Size(15, 13);
            label8.TabIndex = 31;
            label8.Text = "%";
            // 
            // lblGatingFreq
            // 
            lblGatingFreq.AutoSize = true;
            lblGatingFreq.Location = new System.Drawing.Point(19, 75);
            lblGatingFreq.Name = "lblGatingFreq";
            lblGatingFreq.Size = new System.Drawing.Size(94, 13);
            lblGatingFreq.TabIndex = 26;
            lblGatingFreq.Text = "Gating Frequency:";
            // 
            // lblDutyCycle
            // 
            lblDutyCycle.AutoSize = true;
            lblDutyCycle.Location = new System.Drawing.Point(19, 104);
            lblDutyCycle.Name = "lblDutyCycle";
            lblDutyCycle.Size = new System.Drawing.Size(61, 13);
            lblDutyCycle.TabIndex = 29;
            lblDutyCycle.Text = "Duty Cycle:";
            // 
            // label13
            // 
            label13.AutoSize = true;
            label13.Location = new System.Drawing.Point(210, 77);
            label13.Name = "label13";
            label13.Size = new System.Drawing.Size(20, 13);
            label13.TabIndex = 28;
            label13.Text = "Hz";
            // 
            // lblMotorRightPos
            // 
            lblMotorRightPos.AutoSize = true;
            lblMotorRightPos.Location = new System.Drawing.Point(13, 99);
            lblMotorRightPos.Name = "lblMotorRightPos";
            lblMotorRightPos.Size = new System.Drawing.Size(72, 13);
            lblMotorRightPos.TabIndex = 47;
            lblMotorRightPos.Text = "Right Position";
            // 
            // lblMotorMiddlePos
            // 
            lblMotorMiddlePos.AutoSize = true;
            lblMotorMiddlePos.Location = new System.Drawing.Point(13, 54);
            lblMotorMiddlePos.Name = "lblMotorMiddlePos";
            lblMotorMiddlePos.Size = new System.Drawing.Size(78, 13);
            lblMotorMiddlePos.TabIndex = 44;
            lblMotorMiddlePos.Text = "Middle Position";
            // 
            // label20
            // 
            label20.AutoSize = true;
            label20.Location = new System.Drawing.Point(161, 15);
            label20.Name = "label20";
            label20.Size = new System.Drawing.Size(51, 13);
            label20.TabIndex = 61;
            label20.Text = "% of data";
            // 
            // lblTruncateFirst
            // 
            lblTruncateFirst.AutoSize = true;
            lblTruncateFirst.Location = new System.Drawing.Point(21, 16);
            lblTruncateFirst.Name = "lblTruncateFirst";
            lblTruncateFirst.Size = new System.Drawing.Size(72, 13);
            lblTruncateFirst.TabIndex = 59;
            lblTruncateFirst.Text = "Truncate first:";
            // 
            // label22
            // 
            label22.AutoSize = true;
            label22.Location = new System.Drawing.Point(161, 35);
            label22.Name = "label22";
            label22.Size = new System.Drawing.Size(51, 13);
            label22.TabIndex = 64;
            label22.Text = "% of data";
            // 
            // lblTruncateLast
            // 
            lblTruncateLast.AutoSize = true;
            lblTruncateLast.Location = new System.Drawing.Point(20, 35);
            lblTruncateLast.Name = "lblTruncateLast";
            lblTruncateLast.Size = new System.Drawing.Size(69, 13);
            lblTruncateLast.TabIndex = 62;
            lblTruncateLast.Text = "Truncate last";
            // 
            // lblMaxPeakWidth
            // 
            lblMaxPeakWidth.AutoSize = true;
            lblMaxPeakWidth.Location = new System.Drawing.Point(19, 126);
            lblMaxPeakWidth.Name = "lblMaxPeakWidth";
            lblMaxPeakWidth.Size = new System.Drawing.Size(86, 13);
            lblMaxPeakWidth.TabIndex = 52;
            lblMaxPeakWidth.Text = "Max Peak Width";
            // 
            // lblPeakHysteresis
            // 
            lblPeakHysteresis.AutoSize = true;
            lblPeakHysteresis.Location = new System.Drawing.Point(19, 105);
            lblPeakHysteresis.Name = "lblPeakHysteresis";
            lblPeakHysteresis.Size = new System.Drawing.Size(128, 13);
            lblPeakHysteresis.TabIndex = 53;
            lblPeakHysteresis.Text = "Peak Hysteresis (microns)";
            // 
            // lblPeakThreshold
            // 
            lblPeakThreshold.AutoSize = true;
            lblPeakThreshold.Location = new System.Drawing.Point(19, 86);
            lblPeakThreshold.Name = "lblPeakThreshold";
            lblPeakThreshold.Size = new System.Drawing.Size(127, 13);
            lblPeakThreshold.TabIndex = 54;
            lblPeakThreshold.Text = "Peak Threshold (microns)";
            // 
            // lblPitPoints
            // 
            lblPitPoints.AutoSize = true;
            lblPitPoints.Location = new System.Drawing.Point(21, 134);
            lblPitPoints.Name = "lblPitPoints";
            lblPitPoints.Size = new System.Drawing.Size(92, 13);
            lblPitPoints.TabIndex = 70;
            lblPitPoints.Text = "Points in each Pit:";
            // 
            // lblSubstrateRoughness
            // 
            lblSubstrateRoughness.AutoSize = true;
            lblSubstrateRoughness.Location = new System.Drawing.Point(21, 15);
            lblSubstrateRoughness.Name = "lblSubstrateRoughness";
            lblSubstrateRoughness.Size = new System.Drawing.Size(112, 13);
            lblSubstrateRoughness.TabIndex = 68;
            lblSubstrateRoughness.Text = "Substrate Roughness:";
            // 
            // lblCompressionFactor
            // 
            lblCompressionFactor.AutoSize = true;
            lblCompressionFactor.Location = new System.Drawing.Point(21, 107);
            lblCompressionFactor.Name = "lblCompressionFactor";
            lblCompressionFactor.Size = new System.Drawing.Size(103, 13);
            lblCompressionFactor.TabIndex = 61;
            lblCompressionFactor.Text = "Compression Factor:";
            // 
            // lblSmoothingWidth
            // 
            lblSmoothingWidth.AutoSize = true;
            lblSmoothingWidth.Location = new System.Drawing.Point(21, 58);
            lblSmoothingWidth.Name = "lblSmoothingWidth";
            lblSmoothingWidth.Size = new System.Drawing.Size(91, 13);
            lblSmoothingWidth.TabIndex = 60;
            lblSmoothingWidth.Text = "Smoothing Width:";
            // 
            // lblMinPitWidth
            // 
            lblMinPitWidth.AutoSize = true;
            lblMinPitWidth.Location = new System.Drawing.Point(21, 82);
            lblMinPitWidth.Name = "lblMinPitWidth";
            lblMinPitWidth.Size = new System.Drawing.Size(73, 13);
            lblMinPitWidth.TabIndex = 63;
            lblMinPitWidth.Text = "Min Pit Width:";
            // 
            // lblThresholdParam
            // 
            lblThresholdParam.AutoSize = true;
            lblThresholdParam.Location = new System.Drawing.Point(21, 35);
            lblThresholdParam.Name = "lblThresholdParam";
            lblThresholdParam.Size = new System.Drawing.Size(108, 13);
            lblThresholdParam.TabIndex = 62;
            lblThresholdParam.Text = "Threshold Parameter:";
            // 
            // lblFilenamePattern
            // 
            lblFilenamePattern.AutoSize = true;
            lblFilenamePattern.Location = new System.Drawing.Point(18, 60);
            lblFilenamePattern.Name = "lblFilenamePattern";
            lblFilenamePattern.Size = new System.Drawing.Size(89, 13);
            lblFilenamePattern.TabIndex = 38;
            lblFilenamePattern.Text = "Filename Pattern:";
            // 
            // lblDataLocation
            // 
            lblDataLocation.AutoSize = true;
            lblDataLocation.Location = new System.Drawing.Point(18, 23);
            lblDataLocation.Name = "lblDataLocation";
            lblDataLocation.Size = new System.Drawing.Size(77, 13);
            lblDataLocation.TabIndex = 35;
            lblDataLocation.Text = "Data Location:";
            // 
            // tpScanTiming
            // 
            tpScanTiming.Controls.Add(this.chkRepeatScan);
            tpScanTiming.Controls.Add(this.numRepeatScanInterval);
            tpScanTiming.Controls.Add(label3);
            tpScanTiming.Controls.Add(this.chkHomeAfterScans);
            tpScanTiming.Controls.Add(this.chkRepeat);
            tpScanTiming.Controls.Add(lblDAQStart);
            tpScanTiming.Controls.Add(this.numDAQStart);
            tpScanTiming.Controls.Add(label6);
            tpScanTiming.Controls.Add(lblLaserStart);
            tpScanTiming.Controls.Add(this.numLaserStart);
            tpScanTiming.Controls.Add(label7);
            tpScanTiming.Controls.Add(lblDAQStop);
            tpScanTiming.Controls.Add(this.numDAQStop);
            tpScanTiming.Controls.Add(label9);
            tpScanTiming.Controls.Add(this.numRepeatCount);
            tpScanTiming.Controls.Add(lblRepeatInterval);
            tpScanTiming.Controls.Add(this.numRepeatInterval);
            tpScanTiming.Controls.Add(label12);
            tpScanTiming.Location = new System.Drawing.Point(4, 22);
            tpScanTiming.Name = "tpScanTiming";
            tpScanTiming.Padding = new System.Windows.Forms.Padding(3);
            tpScanTiming.Size = new System.Drawing.Size(316, 173);
            tpScanTiming.TabIndex = 0;
            tpScanTiming.Text = "Scan Timing";
            tpScanTiming.UseVisualStyleBackColor = true;
            // 
            // chkRepeatScan
            // 
            this.chkRepeatScan.AutoSize = true;
            this.chkRepeatScan.Location = new System.Drawing.Point(20, 146);
            this.chkRepeatScan.Name = "chkRepeatScan";
            this.chkRepeatScan.Size = new System.Drawing.Size(116, 17);
            this.chkRepeatScan.TabIndex = 50;
            this.chkRepeatScan.Text = "Repeat scan every";
            this.chkRepeatScan.UseVisualStyleBackColor = true;
            // 
            // numRepeatScanInterval
            // 
            this.numRepeatScanInterval.Location = new System.Drawing.Point(142, 145);
            this.numRepeatScanInterval.Maximum = new decimal(new int[] {
            200000000,
            0,
            0,
            0});
            this.numRepeatScanInterval.Name = "numRepeatScanInterval";
            this.numRepeatScanInterval.Size = new System.Drawing.Size(49, 20);
            this.numRepeatScanInterval.TabIndex = 48;
            this.numRepeatScanInterval.Value = new decimal(new int[] {
            60,
            0,
            0,
            0});
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Location = new System.Drawing.Point(197, 150);
            label3.Name = "label3";
            label3.Size = new System.Drawing.Size(12, 13);
            label3.TabIndex = 49;
            label3.Text = "s";
            // 
            // chkHomeAfterScans
            // 
            this.chkHomeAfterScans.AutoSize = true;
            this.chkHomeAfterScans.Location = new System.Drawing.Point(20, 123);
            this.chkHomeAfterScans.Name = "chkHomeAfterScans";
            this.chkHomeAfterScans.Size = new System.Drawing.Size(109, 17);
            this.chkHomeAfterScans.TabIndex = 46;
            this.chkHomeAfterScans.Text = "Home after scans";
            this.chkHomeAfterScans.UseVisualStyleBackColor = true;
            // 
            // chkRepeat
            // 
            this.chkRepeat.AutoSize = true;
            this.chkRepeat.Location = new System.Drawing.Point(20, 97);
            this.chkRepeat.Name = "chkRepeat";
            this.chkRepeat.Size = new System.Drawing.Size(104, 17);
            this.chkRepeat.TabIndex = 45;
            this.chkRepeat.Text = "Repeat readings";
            this.chkRepeat.UseVisualStyleBackColor = true;
            // 
            // lblDAQStart
            // 
            lblDAQStart.AutoSize = true;
            lblDAQStart.Location = new System.Drawing.Point(17, 16);
            lblDAQStart.Name = "lblDAQStart";
            lblDAQStart.Size = new System.Drawing.Size(82, 13);
            lblDAQStart.TabIndex = 32;
            lblDAQStart.Text = "Start DAQ after:";
            // 
            // numDAQStart
            // 
            this.numDAQStart.Location = new System.Drawing.Point(128, 13);
            this.numDAQStart.Maximum = new decimal(new int[] {
            200000000,
            0,
            0,
            0});
            this.numDAQStart.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numDAQStart.Name = "numDAQStart";
            this.numDAQStart.Size = new System.Drawing.Size(84, 20);
            this.numDAQStart.TabIndex = 33;
            this.numDAQStart.Value = new decimal(new int[] {
            180,
            0,
            0,
            0});
            // 
            // numLaserStart
            // 
            this.numLaserStart.Location = new System.Drawing.Point(128, 40);
            this.numLaserStart.Maximum = new decimal(new int[] {
            200000000,
            0,
            0,
            0});
            this.numLaserStart.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numLaserStart.Name = "numLaserStart";
            this.numLaserStart.Size = new System.Drawing.Size(84, 20);
            this.numLaserStart.TabIndex = 36;
            this.numLaserStart.Value = new decimal(new int[] {
            182,
            0,
            0,
            0});
            // 
            // numDAQStop
            // 
            this.numDAQStop.Location = new System.Drawing.Point(128, 65);
            this.numDAQStop.Maximum = new decimal(new int[] {
            200000000,
            0,
            0,
            0});
            this.numDAQStop.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numDAQStop.Name = "numDAQStop";
            this.numDAQStop.Size = new System.Drawing.Size(84, 20);
            this.numDAQStop.TabIndex = 39;
            this.numDAQStop.Value = new decimal(new int[] {
            220,
            0,
            0,
            0});
            // 
            // numRepeatCount
            // 
            this.numRepeatCount.Location = new System.Drawing.Point(127, 94);
            this.numRepeatCount.Maximum = new decimal(new int[] {
            1215752192,
            23,
            0,
            0});
            this.numRepeatCount.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numRepeatCount.Name = "numRepeatCount";
            this.numRepeatCount.Size = new System.Drawing.Size(40, 20);
            this.numRepeatCount.TabIndex = 42;
            this.numRepeatCount.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            // 
            // numRepeatInterval
            // 
            this.numRepeatInterval.DecimalPlaces = 1;
            this.numRepeatInterval.Location = new System.Drawing.Point(238, 96);
            this.numRepeatInterval.Maximum = new decimal(new int[] {
            200000000,
            0,
            0,
            0});
            this.numRepeatInterval.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numRepeatInterval.Name = "numRepeatInterval";
            this.numRepeatInterval.Size = new System.Drawing.Size(53, 20);
            this.numRepeatInterval.TabIndex = 44;
            this.numRepeatInterval.Value = new decimal(new int[] {
            12,
            0,
            0,
            65536});
            // 
            // tpMotorDetails
            // 
            tpMotorDetails.Controls.Add(this.txtMotorRightPositionLabel);
            tpMotorDetails.Controls.Add(this.txtMotorMiddlePositionLabel);
            tpMotorDetails.Controls.Add(this.txtMotorLeftPositionLabel);
            tpMotorDetails.Controls.Add(lblMotorPositionLabels);
            tpMotorDetails.Controls.Add(lblWhere);
            tpMotorDetails.Controls.Add(lblMotorLeftPos);
            tpMotorDetails.Controls.Add(this.numMotorLeftPos);
            tpMotorDetails.Controls.Add(this.lblMotorUnit1);
            tpMotorDetails.Controls.Add(lblMotorMiddlePos);
            tpMotorDetails.Controls.Add(this.numMotorMiddlePos);
            tpMotorDetails.Controls.Add(this.lblMotorUnit2);
            tpMotorDetails.Controls.Add(this.chkCentreMiddle);
            tpMotorDetails.Controls.Add(this.chkSkipMotorLeftPos);
            tpMotorDetails.Controls.Add(this.chkSkipMotorMiddlePos);
            tpMotorDetails.Controls.Add(lblMotorRightPos);
            tpMotorDetails.Controls.Add(this.numMotorRightPos);
            tpMotorDetails.Controls.Add(this.lblMotorUnit3);
            tpMotorDetails.Controls.Add(this.chkSkipMotorRightPos);
            tpMotorDetails.Location = new System.Drawing.Point(4, 22);
            tpMotorDetails.Name = "tpMotorDetails";
            tpMotorDetails.Size = new System.Drawing.Size(316, 173);
            tpMotorDetails.TabIndex = 2;
            tpMotorDetails.Text = "Motor Details";
            tpMotorDetails.UseVisualStyleBackColor = true;
            // 
            // txtMotorRightPositionLabel
            // 
            this.txtMotorRightPositionLabel.Location = new System.Drawing.Point(241, 99);
            this.txtMotorRightPositionLabel.Name = "txtMotorRightPositionLabel";
            this.txtMotorRightPositionLabel.Size = new System.Drawing.Size(65, 20);
            this.txtMotorRightPositionLabel.TabIndex = 52;
            this.txtMotorRightPositionLabel.Text = "RIGHT";
            // 
            // txtMotorMiddlePositionLabel
            // 
            this.txtMotorMiddlePositionLabel.Location = new System.Drawing.Point(241, 54);
            this.txtMotorMiddlePositionLabel.Name = "txtMotorMiddlePositionLabel";
            this.txtMotorMiddlePositionLabel.Size = new System.Drawing.Size(65, 20);
            this.txtMotorMiddlePositionLabel.TabIndex = 52;
            this.txtMotorMiddlePositionLabel.Text = "MIDDLE";
            // 
            // txtMotorLeftPositionLabel
            // 
            this.txtMotorLeftPositionLabel.Location = new System.Drawing.Point(241, 29);
            this.txtMotorLeftPositionLabel.Name = "txtMotorLeftPositionLabel";
            this.txtMotorLeftPositionLabel.Size = new System.Drawing.Size(65, 20);
            this.txtMotorLeftPositionLabel.TabIndex = 52;
            this.txtMotorLeftPositionLabel.Text = "LEFT";
            // 
            // lblMotorPositionLabels
            // 
            lblMotorPositionLabels.AutoSize = true;
            lblMotorPositionLabels.Location = new System.Drawing.Point(244, 10);
            lblMotorPositionLabels.Name = "lblMotorPositionLabels";
            lblMotorPositionLabels.Size = new System.Drawing.Size(60, 13);
            lblMotorPositionLabels.TabIndex = 51;
            lblMotorPositionLabels.Text = "Description";
            // 
            // lblWhere
            // 
            lblWhere.Location = new System.Drawing.Point(13, 127);
            lblWhere.Name = "lblWhere";
            lblWhere.Size = new System.Drawing.Size(272, 32);
            lblWhere.TabIndex = 1;
            lblWhere.Text = "Positive numbers are distance from left edge, negative numbers from right edge";
            // 
            // lblMotorLeftPos
            // 
            lblMotorLeftPos.AutoSize = true;
            lblMotorLeftPos.Location = new System.Drawing.Point(13, 29);
            lblMotorLeftPos.Name = "lblMotorLeftPos";
            lblMotorLeftPos.Size = new System.Drawing.Size(68, 13);
            lblMotorLeftPos.TabIndex = 41;
            lblMotorLeftPos.Text = "Left Position:";
            // 
            // numMotorLeftPos
            // 
            this.numMotorLeftPos.Location = new System.Drawing.Point(97, 27);
            this.numMotorLeftPos.Maximum = new decimal(new int[] {
            200000000,
            0,
            0,
            0});
            this.numMotorLeftPos.Minimum = new decimal(new int[] {
            200000000,
            0,
            0,
            -2147483648});
            this.numMotorLeftPos.Name = "numMotorLeftPos";
            this.numMotorLeftPos.Size = new System.Drawing.Size(50, 20);
            this.numMotorLeftPos.TabIndex = 42;
            this.numMotorLeftPos.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // lblMotorUnit1
            // 
            this.lblMotorUnit1.AutoSize = true;
            this.lblMotorUnit1.Location = new System.Drawing.Point(152, 31);
            this.lblMotorUnit1.Name = "lblMotorUnit1";
            this.lblMotorUnit1.Size = new System.Drawing.Size(23, 13);
            this.lblMotorUnit1.TabIndex = 43;
            this.lblMotorUnit1.Text = "mm";
            // 
            // numMotorMiddlePos
            // 
            this.numMotorMiddlePos.Location = new System.Drawing.Point(97, 54);
            this.numMotorMiddlePos.Maximum = new decimal(new int[] {
            200000000,
            0,
            0,
            0});
            this.numMotorMiddlePos.Minimum = new decimal(new int[] {
            200000000,
            0,
            0,
            -2147483648});
            this.numMotorMiddlePos.Name = "numMotorMiddlePos";
            this.numMotorMiddlePos.Size = new System.Drawing.Size(50, 20);
            this.numMotorMiddlePos.TabIndex = 45;
            this.numMotorMiddlePos.Value = new decimal(new int[] {
            200,
            0,
            0,
            0});
            // 
            // lblMotorUnit2
            // 
            this.lblMotorUnit2.AutoSize = true;
            this.lblMotorUnit2.Location = new System.Drawing.Point(152, 58);
            this.lblMotorUnit2.Name = "lblMotorUnit2";
            this.lblMotorUnit2.Size = new System.Drawing.Size(23, 13);
            this.lblMotorUnit2.TabIndex = 46;
            this.lblMotorUnit2.Text = "mm";
            // 
            // chkCentreMiddle
            // 
            this.chkCentreMiddle.AutoSize = true;
            this.chkCentreMiddle.Location = new System.Drawing.Point(181, 78);
            this.chkCentreMiddle.Name = "chkCentreMiddle";
            this.chkCentreMiddle.Size = new System.Drawing.Size(56, 17);
            this.chkCentreMiddle.TabIndex = 50;
            this.chkCentreMiddle.Text = "centre";
            this.chkCentreMiddle.UseVisualStyleBackColor = true;
            // 
            // chkSkipMotorLeftPos
            // 
            this.chkSkipMotorLeftPos.AutoSize = true;
            this.chkSkipMotorLeftPos.Location = new System.Drawing.Point(181, 29);
            this.chkSkipMotorLeftPos.Name = "chkSkipMotorLeftPos";
            this.chkSkipMotorLeftPos.Size = new System.Drawing.Size(45, 17);
            this.chkSkipMotorLeftPos.TabIndex = 50;
            this.chkSkipMotorLeftPos.Text = "skip";
            this.chkSkipMotorLeftPos.UseVisualStyleBackColor = true;
            // 
            // chkSkipMotorMiddlePos
            // 
            this.chkSkipMotorMiddlePos.AutoSize = true;
            this.chkSkipMotorMiddlePos.Location = new System.Drawing.Point(181, 56);
            this.chkSkipMotorMiddlePos.Name = "chkSkipMotorMiddlePos";
            this.chkSkipMotorMiddlePos.Size = new System.Drawing.Size(45, 17);
            this.chkSkipMotorMiddlePos.TabIndex = 50;
            this.chkSkipMotorMiddlePos.Text = "skip";
            this.chkSkipMotorMiddlePos.UseVisualStyleBackColor = true;
            // 
            // numMotorRightPos
            // 
            this.numMotorRightPos.Location = new System.Drawing.Point(97, 99);
            this.numMotorRightPos.Maximum = new decimal(new int[] {
            200000000,
            0,
            0,
            0});
            this.numMotorRightPos.Minimum = new decimal(new int[] {
            200000000,
            0,
            0,
            -2147483648});
            this.numMotorRightPos.Name = "numMotorRightPos";
            this.numMotorRightPos.Size = new System.Drawing.Size(50, 20);
            this.numMotorRightPos.TabIndex = 48;
            this.numMotorRightPos.Value = new decimal(new int[] {
            10,
            0,
            0,
            -2147483648});
            // 
            // lblMotorUnit3
            // 
            this.lblMotorUnit3.AutoSize = true;
            this.lblMotorUnit3.Location = new System.Drawing.Point(152, 103);
            this.lblMotorUnit3.Name = "lblMotorUnit3";
            this.lblMotorUnit3.Size = new System.Drawing.Size(23, 13);
            this.lblMotorUnit3.TabIndex = 49;
            this.lblMotorUnit3.Text = "mm";
            // 
            // chkSkipMotorRightPos
            // 
            this.chkSkipMotorRightPos.AutoSize = true;
            this.chkSkipMotorRightPos.Location = new System.Drawing.Point(181, 102);
            this.chkSkipMotorRightPos.Name = "chkSkipMotorRightPos";
            this.chkSkipMotorRightPos.Size = new System.Drawing.Size(45, 17);
            this.chkSkipMotorRightPos.TabIndex = 50;
            this.chkSkipMotorRightPos.Text = "skip";
            this.chkSkipMotorRightPos.UseVisualStyleBackColor = true;
            // 
            // label10
            // 
            label10.AutoSize = true;
            label10.Location = new System.Drawing.Point(234, 142);
            label10.Name = "label10";
            label10.Size = new System.Drawing.Size(21, 13);
            label10.TabIndex = 55;
            label10.Text = "+/-";
            // 
            // lblScanAveNumber
            // 
            lblScanAveNumber.AutoSize = true;
            lblScanAveNumber.Location = new System.Drawing.Point(18, 86);
            lblScanAveNumber.Name = "lblScanAveNumber";
            lblScanAveNumber.Size = new System.Drawing.Size(137, 13);
            lblScanAveNumber.TabIndex = 70;
            lblScanAveNumber.Text = "Scan Display Ave. Number:";
            // 
            // lblUnits
            // 
            lblUnits.AutoSize = true;
            lblUnits.Location = new System.Drawing.Point(18, 113);
            lblUnits.Name = "lblUnits";
            lblUnits.Size = new System.Drawing.Size(34, 13);
            lblUnits.TabIndex = 72;
            lblUnits.Text = "Units:";
            // 
            // lblFilterErrorsFiller
            // 
            lblFilterErrorsFiller.AutoSize = true;
            lblFilterErrorsFiller.Location = new System.Drawing.Point(151, 170);
            lblFilterErrorsFiller.Name = "lblFilterErrorsFiller";
            lblFilterErrorsFiller.Size = new System.Drawing.Size(123, 13);
            lblFilterErrorsFiller.TabIndex = 76;
            lblFilterErrorsFiller.Text = "% outside boundary lines";
            // 
            // lblPaintingDelay
            // 
            lblPaintingDelay.AutoSize = true;
            lblPaintingDelay.Location = new System.Drawing.Point(16, 46);
            lblPaintingDelay.Name = "lblPaintingDelay";
            lblPaintingDelay.Size = new System.Drawing.Size(135, 13);
            lblPaintingDelay.TabIndex = 16;
            lblPaintingDelay.Text = "Delay after starting painting";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new System.Drawing.Point(227, 46);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(12, 13);
            label2.TabIndex = 39;
            label2.Text = "s";
            // 
            // lblLaserHandshake
            // 
            lblLaserHandshake.AutoSize = true;
            lblLaserHandshake.Location = new System.Drawing.Point(157, 44);
            lblLaserHandshake.Name = "lblLaserHandshake";
            lblLaserHandshake.Size = new System.Drawing.Size(65, 13);
            lblLaserHandshake.TabIndex = 44;
            lblLaserHandshake.Text = "Handshake:";
            // 
            // lblSensorPort
            // 
            this.lblSensorPort.AutoSize = true;
            this.lblSensorPort.Location = new System.Drawing.Point(9, 69);
            this.lblSensorPort.Name = "lblSensorPort";
            this.lblSensorPort.Size = new System.Drawing.Size(29, 13);
            this.lblSensorPort.TabIndex = 40;
            this.lblSensorPort.Text = "Port:";
            // 
            // lblBoundaryLines
            // 
            this.lblBoundaryLines.AutoSize = true;
            this.lblBoundaryLines.Location = new System.Drawing.Point(19, 142);
            this.lblBoundaryLines.Name = "lblBoundaryLines";
            this.lblBoundaryLines.Size = new System.Drawing.Size(135, 13);
            this.lblBoundaryLines.TabIndex = 52;
            this.lblBoundaryLines.Text = "Target Thickness (microns)";
            // 
            // controlButtonsContainer
            // 
            this.controlButtonsContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.controlButtonsContainer.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.controlButtonsContainer.Location = new System.Drawing.Point(0, 0);
            this.controlButtonsContainer.MinimumSize = new System.Drawing.Size(212, 203);
            this.controlButtonsContainer.Name = "controlButtonsContainer";
            this.controlButtonsContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // controlButtonsContainer.Panel1
            // 
            this.controlButtonsContainer.Panel1.Controls.Add(this.tabSettings);
            this.controlButtonsContainer.Panel1.Padding = new System.Windows.Forms.Padding(15, 8, 15, 8);
            this.controlButtonsContainer.Panel1MinSize = 0;
            // 
            // controlButtonsContainer.Panel2
            // 
            this.controlButtonsContainer.Panel2.Controls.Add(this.btnSave);
            this.controlButtonsContainer.Panel2.Controls.Add(this.btnOK);
            this.controlButtonsContainer.Panel2.Controls.Add(this.btnCancel);
            this.controlButtonsContainer.Panel2MinSize = 0;
            this.controlButtonsContainer.Size = new System.Drawing.Size(403, 308);
            this.controlButtonsContainer.SplitterDistance = 261;
            this.controlButtonsContainer.TabIndex = 2;
            this.controlButtonsContainer.TabStop = false;
            // 
            // tabSettings
            // 
            this.tabSettings.Controls.Add(this.tpConnection);
            this.tabSettings.Controls.Add(this.tpScanParameters);
            this.tabSettings.Controls.Add(this.tpDataAnalysis);
            this.tabSettings.Controls.Add(this.tpDataLocation);
            this.tabSettings.Controls.Add(this.tpMisc);
            this.tabSettings.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabSettings.Location = new System.Drawing.Point(15, 8);
            this.tabSettings.Name = "tabSettings";
            this.tabSettings.SelectedIndex = 0;
            this.tabSettings.Size = new System.Drawing.Size(373, 245);
            this.tabSettings.TabIndex = 3;
            // 
            // tpConnection
            // 
            this.tpConnection.Controls.Add(this.btnRefreshCOMPorts);
            this.tpConnection.Controls.Add(this.btnConnect);
            this.tpConnection.Controls.Add(this.tabConnection);
            this.tpConnection.Location = new System.Drawing.Point(4, 22);
            this.tpConnection.Name = "tpConnection";
            this.tpConnection.Size = new System.Drawing.Size(365, 219);
            this.tpConnection.TabIndex = 0;
            this.tpConnection.Text = "Connection";
            this.tpConnection.UseVisualStyleBackColor = true;
            // 
            // btnRefreshCOMPorts
            // 
            this.btnRefreshCOMPorts.Location = new System.Drawing.Point(42, 159);
            this.btnRefreshCOMPorts.Name = "btnRefreshCOMPorts";
            this.btnRefreshCOMPorts.Size = new System.Drawing.Size(115, 23);
            this.btnRefreshCOMPorts.TabIndex = 30;
            this.btnRefreshCOMPorts.Text = "Refresh COM Ports";
            this.btnRefreshCOMPorts.UseVisualStyleBackColor = true;
            // 
            // btnConnect
            // 
            this.btnConnect.Location = new System.Drawing.Point(179, 159);
            this.btnConnect.Name = "btnConnect";
            this.btnConnect.Size = new System.Drawing.Size(115, 23);
            this.btnConnect.TabIndex = 31;
            this.btnConnect.Text = "Connect";
            this.btnConnect.UseVisualStyleBackColor = true;
            // 
            // tabConnection
            // 
            this.tabConnection.Controls.Add(this.tpSensorConnection);
            this.tabConnection.Controls.Add(this.tpLaserConnection);
            this.tabConnection.Controls.Add(this.tpMotorConnection);
            this.tabConnection.Location = new System.Drawing.Point(16, 18);
            this.tabConnection.Name = "tabConnection";
            this.tabConnection.SelectedIndex = 0;
            this.tabConnection.Size = new System.Drawing.Size(314, 129);
            this.tabConnection.TabIndex = 29;
            // 
            // tpSensorConnection
            // 
            this.tpSensorConnection.Controls.Add(this.txtSensorAddress);
            this.tpSensorConnection.Controls.Add(lblSensorBaudrate);
            this.tpSensorConnection.Controls.Add(this.cmbSensorBaudrate);
            this.tpSensorConnection.Controls.Add(this.lblSensorPort);
            this.tpSensorConnection.Controls.Add(this.cmbSensorPort);
            this.tpSensorConnection.Controls.Add(lblSensorType);
            this.tpSensorConnection.Controls.Add(this.cmbSensorType);
            this.tpSensorConnection.Controls.Add(lblSensorModel);
            this.tpSensorConnection.Controls.Add(this.cmbSensorModel);
            this.tpSensorConnection.Controls.Add(this.chkConnectSensor);
            this.tpSensorConnection.Location = new System.Drawing.Point(4, 22);
            this.tpSensorConnection.Name = "tpSensorConnection";
            this.tpSensorConnection.Padding = new System.Windows.Forms.Padding(3);
            this.tpSensorConnection.Size = new System.Drawing.Size(306, 103);
            this.tpSensorConnection.TabIndex = 0;
            this.tpSensorConnection.Text = "Sensor";
            this.tpSensorConnection.UseVisualStyleBackColor = true;
            // 
            // txtSensorAddress
            // 
            this.txtSensorAddress.Location = new System.Drawing.Point(53, 66);
            this.txtSensorAddress.Name = "txtSensorAddress";
            this.txtSensorAddress.Size = new System.Drawing.Size(108, 20);
            this.txtSensorAddress.TabIndex = 44;
            // 
            // cmbSensorBaudrate
            // 
            this.cmbSensorBaudrate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSensorBaudrate.FormattingEnabled = true;
            this.cmbSensorBaudrate.Location = new System.Drawing.Point(223, 66);
            this.cmbSensorBaudrate.Name = "cmbSensorBaudrate";
            this.cmbSensorBaudrate.Size = new System.Drawing.Size(69, 21);
            this.cmbSensorBaudrate.TabIndex = 41;
            // 
            // cmbSensorPort
            // 
            this.cmbSensorPort.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSensorPort.FormattingEnabled = true;
            this.cmbSensorPort.Location = new System.Drawing.Point(53, 66);
            this.cmbSensorPort.Name = "cmbSensorPort";
            this.cmbSensorPort.Size = new System.Drawing.Size(109, 21);
            this.cmbSensorPort.TabIndex = 42;
            // 
            // cmbSensorType
            // 
            this.cmbSensorType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSensorType.FormattingEnabled = true;
            this.cmbSensorType.Location = new System.Drawing.Point(223, 39);
            this.cmbSensorType.Name = "cmbSensorType";
            this.cmbSensorType.Size = new System.Drawing.Size(69, 21);
            this.cmbSensorType.TabIndex = 39;
            // 
            // cmbSensorModel
            // 
            this.cmbSensorModel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSensorModel.FormattingEnabled = true;
            this.cmbSensorModel.Location = new System.Drawing.Point(53, 39);
            this.cmbSensorModel.Name = "cmbSensorModel";
            this.cmbSensorModel.Size = new System.Drawing.Size(109, 21);
            this.cmbSensorModel.TabIndex = 38;
            // 
            // chkConnectSensor
            // 
            this.chkConnectSensor.AutoSize = true;
            this.chkConnectSensor.Checked = true;
            this.chkConnectSensor.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkConnectSensor.Location = new System.Drawing.Point(19, 13);
            this.chkConnectSensor.Name = "chkConnectSensor";
            this.chkConnectSensor.Size = new System.Drawing.Size(112, 17);
            this.chkConnectSensor.TabIndex = 35;
            this.chkConnectSensor.Text = "Connect to sensor";
            this.chkConnectSensor.UseVisualStyleBackColor = true;
            // 
            // tpLaserConnection
            // 
            this.tpLaserConnection.Controls.Add(lblLaserBaudrate);
            this.tpLaserConnection.Controls.Add(this.cmbLaserBaudrate);
            this.tpLaserConnection.Controls.Add(lblLaserHandshake);
            this.tpLaserConnection.Controls.Add(lblLaserPort);
            this.tpLaserConnection.Controls.Add(this.cmbLaserHandshake);
            this.tpLaserConnection.Controls.Add(this.cmbLaserPort);
            this.tpLaserConnection.Controls.Add(this.chkConnectLaser);
            this.tpLaserConnection.Location = new System.Drawing.Point(4, 22);
            this.tpLaserConnection.Name = "tpLaserConnection";
            this.tpLaserConnection.Padding = new System.Windows.Forms.Padding(3);
            this.tpLaserConnection.Size = new System.Drawing.Size(306, 103);
            this.tpLaserConnection.TabIndex = 1;
            this.tpLaserConnection.Text = "Laser";
            this.tpLaserConnection.UseVisualStyleBackColor = true;
            // 
            // cmbLaserBaudrate
            // 
            this.cmbLaserBaudrate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbLaserBaudrate.FormattingEnabled = true;
            this.cmbLaserBaudrate.Location = new System.Drawing.Point(80, 67);
            this.cmbLaserBaudrate.Name = "cmbLaserBaudrate";
            this.cmbLaserBaudrate.Size = new System.Drawing.Size(69, 21);
            this.cmbLaserBaudrate.TabIndex = 45;
            // 
            // cmbLaserHandshake
            // 
            this.cmbLaserHandshake.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbLaserHandshake.FormattingEnabled = true;
            this.cmbLaserHandshake.Location = new System.Drawing.Point(224, 41);
            this.cmbLaserHandshake.Name = "cmbLaserHandshake";
            this.cmbLaserHandshake.Size = new System.Drawing.Size(76, 21);
            this.cmbLaserHandshake.TabIndex = 46;
            // 
            // cmbLaserPort
            // 
            this.cmbLaserPort.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbLaserPort.FormattingEnabled = true;
            this.cmbLaserPort.Location = new System.Drawing.Point(80, 41);
            this.cmbLaserPort.Name = "cmbLaserPort";
            this.cmbLaserPort.Size = new System.Drawing.Size(69, 21);
            this.cmbLaserPort.TabIndex = 46;
            // 
            // chkConnectLaser
            // 
            this.chkConnectLaser.AutoSize = true;
            this.chkConnectLaser.Checked = true;
            this.chkConnectLaser.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkConnectLaser.Location = new System.Drawing.Point(19, 13);
            this.chkConnectLaser.Name = "chkConnectLaser";
            this.chkConnectLaser.Size = new System.Drawing.Size(103, 17);
            this.chkConnectLaser.TabIndex = 29;
            this.chkConnectLaser.Text = "Connect to laser";
            this.chkConnectLaser.UseVisualStyleBackColor = true;
            // 
            // tpMotorConnection
            // 
            this.tpMotorConnection.Controls.Add(lblMotorAddress);
            this.tpMotorConnection.Controls.Add(this.cmbMotorAddress);
            this.tpMotorConnection.Controls.Add(this.chkConnectMotor);
            this.tpMotorConnection.Location = new System.Drawing.Point(4, 22);
            this.tpMotorConnection.Name = "tpMotorConnection";
            this.tpMotorConnection.Size = new System.Drawing.Size(306, 103);
            this.tpMotorConnection.TabIndex = 2;
            this.tpMotorConnection.Text = "Motor";
            this.tpMotorConnection.UseVisualStyleBackColor = true;
            // 
            // cmbMotorAddress
            // 
            this.cmbMotorAddress.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbMotorAddress.FormattingEnabled = true;
            this.cmbMotorAddress.Location = new System.Drawing.Point(82, 41);
            this.cmbMotorAddress.Name = "cmbMotorAddress";
            this.cmbMotorAddress.Size = new System.Drawing.Size(110, 21);
            this.cmbMotorAddress.TabIndex = 49;
            // 
            // chkConnectMotor
            // 
            this.chkConnectMotor.AutoSize = true;
            this.chkConnectMotor.Checked = true;
            this.chkConnectMotor.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkConnectMotor.Location = new System.Drawing.Point(19, 13);
            this.chkConnectMotor.Name = "chkConnectMotor";
            this.chkConnectMotor.Size = new System.Drawing.Size(107, 17);
            this.chkConnectMotor.TabIndex = 47;
            this.chkConnectMotor.Text = "Connect to motor";
            this.chkConnectMotor.UseVisualStyleBackColor = true;
            // 
            // tpScanParameters
            // 
            this.tpScanParameters.Controls.Add(this.tabScanParameters);
            this.tpScanParameters.Location = new System.Drawing.Point(4, 22);
            this.tpScanParameters.Name = "tpScanParameters";
            this.tpScanParameters.Size = new System.Drawing.Size(365, 219);
            this.tpScanParameters.TabIndex = 1;
            this.tpScanParameters.Text = "Scan Parameters";
            this.tpScanParameters.UseVisualStyleBackColor = true;
            // 
            // tabScanParameters
            // 
            this.tabScanParameters.Controls.Add(tpScanTiming);
            this.tabScanParameters.Controls.Add(this.tpLaserDetails);
            this.tabScanParameters.Controls.Add(tpMotorDetails);
            this.tabScanParameters.Controls.Add(this.tpMiscScan);
            this.tabScanParameters.Location = new System.Drawing.Point(11, 8);
            this.tabScanParameters.Name = "tabScanParameters";
            this.tabScanParameters.SelectedIndex = 0;
            this.tabScanParameters.Size = new System.Drawing.Size(324, 199);
            this.tabScanParameters.TabIndex = 2;
            // 
            // tpLaserDetails
            // 
            this.tpLaserDetails.Controls.Add(this.chkDisplayPulseCountErrorMessages);
            this.tpLaserDetails.Controls.Add(this.chkFilterPulseCountErrors);
            this.tpLaserDetails.Controls.Add(this.chkValidatePulseCount);
            this.tpLaserDetails.Controls.Add(this.radPulseCount);
            this.tpLaserDetails.Controls.Add(this.numPulseCount);
            this.tpLaserDetails.Controls.Add(this.radRunTime);
            this.tpLaserDetails.Controls.Add(this.numRunTime);
            this.tpLaserDetails.Controls.Add(lblGatingFreq);
            this.tpLaserDetails.Controls.Add(this.numGatingFreq);
            this.tpLaserDetails.Controls.Add(label13);
            this.tpLaserDetails.Controls.Add(lblDutyCycle);
            this.tpLaserDetails.Controls.Add(this.numDutyCycle);
            this.tpLaserDetails.Controls.Add(label8);
            this.tpLaserDetails.Location = new System.Drawing.Point(4, 22);
            this.tpLaserDetails.Name = "tpLaserDetails";
            this.tpLaserDetails.Padding = new System.Windows.Forms.Padding(3);
            this.tpLaserDetails.Size = new System.Drawing.Size(316, 173);
            this.tpLaserDetails.TabIndex = 1;
            this.tpLaserDetails.Text = "Laser Details";
            this.tpLaserDetails.UseVisualStyleBackColor = true;
            // 
            // chkDisplayPulseCountErrorMessages
            // 
            this.chkDisplayPulseCountErrorMessages.AutoSize = true;
            this.chkDisplayPulseCountErrorMessages.Checked = true;
            this.chkDisplayPulseCountErrorMessages.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkDisplayPulseCountErrorMessages.Location = new System.Drawing.Point(150, 150);
            this.chkDisplayPulseCountErrorMessages.Name = "chkDisplayPulseCountErrorMessages";
            this.chkDisplayPulseCountErrorMessages.Size = new System.Drawing.Size(129, 17);
            this.chkDisplayPulseCountErrorMessages.TabIndex = 36;
            this.chkDisplayPulseCountErrorMessages.Text = "Display error message";
            this.chkDisplayPulseCountErrorMessages.UseVisualStyleBackColor = true;
            // 
            // chkFilterPulseCountErrors
            // 
            this.chkFilterPulseCountErrors.AutoSize = true;
            this.chkFilterPulseCountErrors.Checked = true;
            this.chkFilterPulseCountErrors.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkFilterPulseCountErrors.Location = new System.Drawing.Point(50, 150);
            this.chkFilterPulseCountErrors.Name = "chkFilterPulseCountErrors";
            this.chkFilterPulseCountErrors.Size = new System.Drawing.Size(77, 17);
            this.chkFilterPulseCountErrors.TabIndex = 35;
            this.chkFilterPulseCountErrors.Text = "Filter errors";
            this.chkFilterPulseCountErrors.UseVisualStyleBackColor = true;
            // 
            // chkValidatePulseCount
            // 
            this.chkValidatePulseCount.AutoSize = true;
            this.chkValidatePulseCount.Checked = true;
            this.chkValidatePulseCount.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkValidatePulseCount.Location = new System.Drawing.Point(22, 129);
            this.chkValidatePulseCount.Name = "chkValidatePulseCount";
            this.chkValidatePulseCount.Size = new System.Drawing.Size(122, 17);
            this.chkValidatePulseCount.TabIndex = 34;
            this.chkValidatePulseCount.Text = "Validate pulse count";
            this.chkValidatePulseCount.UseVisualStyleBackColor = true;
            // 
            // radPulseCount
            // 
            this.radPulseCount.AutoSize = true;
            this.radPulseCount.Checked = true;
            this.radPulseCount.Location = new System.Drawing.Point(22, 17);
            this.radPulseCount.Name = "radPulseCount";
            this.radPulseCount.Size = new System.Drawing.Size(82, 17);
            this.radPulseCount.TabIndex = 33;
            this.radPulseCount.TabStop = true;
            this.radPulseCount.Text = "Pulse Count";
            this.radPulseCount.UseVisualStyleBackColor = true;
            // 
            // numPulseCount
            // 
            this.numPulseCount.Location = new System.Drawing.Point(126, 17);
            this.numPulseCount.Maximum = new decimal(new int[] {
            99999,
            0,
            0,
            0});
            this.numPulseCount.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numPulseCount.Name = "numPulseCount";
            this.numPulseCount.Size = new System.Drawing.Size(81, 20);
            this.numPulseCount.TabIndex = 25;
            this.numPulseCount.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // radRunTime
            // 
            this.radRunTime.AutoSize = true;
            this.radRunTime.Location = new System.Drawing.Point(22, 43);
            this.radRunTime.Name = "radRunTime";
            this.radRunTime.Size = new System.Drawing.Size(93, 17);
            this.radRunTime.TabIndex = 32;
            this.radRunTime.Text = "Run Time (ms)";
            this.radRunTime.UseVisualStyleBackColor = true;
            // 
            // numRunTime
            // 
            this.numRunTime.Location = new System.Drawing.Point(126, 43);
            this.numRunTime.Maximum = new decimal(new int[] {
            32766,
            0,
            0,
            0});
            this.numRunTime.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numRunTime.Name = "numRunTime";
            this.numRunTime.Size = new System.Drawing.Size(81, 20);
            this.numRunTime.TabIndex = 24;
            this.numRunTime.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // numGatingFreq
            // 
            this.numGatingFreq.Location = new System.Drawing.Point(127, 73);
            this.numGatingFreq.Maximum = new decimal(new int[] {
            20000,
            0,
            0,
            0});
            this.numGatingFreq.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numGatingFreq.Name = "numGatingFreq";
            this.numGatingFreq.Size = new System.Drawing.Size(80, 20);
            this.numGatingFreq.TabIndex = 27;
            this.numGatingFreq.Value = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            // 
            // numDutyCycle
            // 
            this.numDutyCycle.DecimalPlaces = 1;
            this.numDutyCycle.Location = new System.Drawing.Point(127, 101);
            this.numDutyCycle.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numDutyCycle.Name = "numDutyCycle";
            this.numDutyCycle.Size = new System.Drawing.Size(80, 20);
            this.numDutyCycle.TabIndex = 30;
            this.numDutyCycle.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // tpMiscScan
            // 
            this.tpMiscScan.Controls.Add(this.numPaintingDelay);
            this.tpMiscScan.Controls.Add(label2);
            this.tpMiscScan.Controls.Add(lblPaintingDelay);
            this.tpMiscScan.Controls.Add(this.chkRequirePainting);
            this.tpMiscScan.Location = new System.Drawing.Point(4, 22);
            this.tpMiscScan.Name = "tpMiscScan";
            this.tpMiscScan.Padding = new System.Windows.Forms.Padding(3);
            this.tpMiscScan.Size = new System.Drawing.Size(316, 173);
            this.tpMiscScan.TabIndex = 3;
            this.tpMiscScan.Text = "Misc.";
            this.tpMiscScan.UseVisualStyleBackColor = true;
            // 
            // numPaintingDelay
            // 
            this.numPaintingDelay.Location = new System.Drawing.Point(157, 42);
            this.numPaintingDelay.Maximum = new decimal(new int[] {
            200000000,
            0,
            0,
            0});
            this.numPaintingDelay.Name = "numPaintingDelay";
            this.numPaintingDelay.Size = new System.Drawing.Size(65, 20);
            this.numPaintingDelay.TabIndex = 38;
            this.numPaintingDelay.Value = new decimal(new int[] {
            182,
            0,
            0,
            0});
            // 
            // chkRequirePainting
            // 
            this.chkRequirePainting.AutoSize = true;
            this.chkRequirePainting.Location = new System.Drawing.Point(21, 17);
            this.chkRequirePainting.Name = "chkRequirePainting";
            this.chkRequirePainting.Size = new System.Drawing.Size(131, 17);
            this.chkRequirePainting.TabIndex = 15;
            this.chkRequirePainting.Text = "Require Painting Input";
            this.chkRequirePainting.UseVisualStyleBackColor = true;
            // 
            // tpDataAnalysis
            // 
            this.tpDataAnalysis.Controls.Add(this.tabDataAnalysis);
            this.tpDataAnalysis.Location = new System.Drawing.Point(4, 22);
            this.tpDataAnalysis.Name = "tpDataAnalysis";
            this.tpDataAnalysis.Size = new System.Drawing.Size(365, 219);
            this.tpDataAnalysis.TabIndex = 2;
            this.tpDataAnalysis.Text = "Data Analysis";
            this.tpDataAnalysis.UseVisualStyleBackColor = true;
            // 
            // tabDataAnalysis
            // 
            this.tabDataAnalysis.Controls.Add(this.tpDataProcessing);
            this.tabDataAnalysis.Controls.Add(this.tpAnalysisParameters);
            this.tabDataAnalysis.Location = new System.Drawing.Point(16, 16);
            this.tabDataAnalysis.Name = "tabDataAnalysis";
            this.tabDataAnalysis.SelectedIndex = 0;
            this.tabDataAnalysis.Size = new System.Drawing.Size(314, 187);
            this.tabDataAnalysis.TabIndex = 3;
            // 
            // tpDataProcessing
            // 
            this.tpDataProcessing.Controls.Add(lblTruncateFirst);
            this.tpDataProcessing.Controls.Add(this.numTruncateFirst);
            this.tpDataProcessing.Controls.Add(label20);
            this.tpDataProcessing.Controls.Add(lblTruncateLast);
            this.tpDataProcessing.Controls.Add(this.numTruncateLast);
            this.tpDataProcessing.Controls.Add(label22);
            this.tpDataProcessing.Controls.Add(this.chkFilterPeaks);
            this.tpDataProcessing.Controls.Add(lblPeakThreshold);
            this.tpDataProcessing.Controls.Add(this.numPeakThreshold);
            this.tpDataProcessing.Controls.Add(lblPeakHysteresis);
            this.tpDataProcessing.Controls.Add(this.numPeakHysteresis);
            this.tpDataProcessing.Controls.Add(lblMaxPeakWidth);
            this.tpDataProcessing.Controls.Add(this.numMaxPeakWidth);
            this.tpDataProcessing.Location = new System.Drawing.Point(4, 22);
            this.tpDataProcessing.Name = "tpDataProcessing";
            this.tpDataProcessing.Padding = new System.Windows.Forms.Padding(3);
            this.tpDataProcessing.Size = new System.Drawing.Size(306, 161);
            this.tpDataProcessing.TabIndex = 0;
            this.tpDataProcessing.Text = "Data Processing";
            this.tpDataProcessing.UseVisualStyleBackColor = true;
            // 
            // numTruncateFirst
            // 
            this.numTruncateFirst.Location = new System.Drawing.Point(101, 11);
            this.numTruncateFirst.Name = "numTruncateFirst";
            this.numTruncateFirst.Size = new System.Drawing.Size(58, 20);
            this.numTruncateFirst.TabIndex = 60;
            this.numTruncateFirst.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // numTruncateLast
            // 
            this.numTruncateLast.Location = new System.Drawing.Point(101, 31);
            this.numTruncateLast.Name = "numTruncateLast";
            this.numTruncateLast.Size = new System.Drawing.Size(58, 20);
            this.numTruncateLast.TabIndex = 63;
            this.numTruncateLast.Value = new decimal(new int[] {
            20,
            0,
            0,
            0});
            // 
            // chkFilterPeaks
            // 
            this.chkFilterPeaks.AutoSize = true;
            this.chkFilterPeaks.Checked = true;
            this.chkFilterPeaks.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkFilterPeaks.Location = new System.Drawing.Point(22, 66);
            this.chkFilterPeaks.Name = "chkFilterPeaks";
            this.chkFilterPeaks.Size = new System.Drawing.Size(81, 17);
            this.chkFilterPeaks.TabIndex = 58;
            this.chkFilterPeaks.Text = "Filter Peaks";
            this.chkFilterPeaks.UseVisualStyleBackColor = true;
            // 
            // numPeakThreshold
            // 
            this.numPeakThreshold.DecimalPlaces = 1;
            this.numPeakThreshold.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numPeakThreshold.Location = new System.Drawing.Point(156, 79);
            this.numPeakThreshold.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.numPeakThreshold.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numPeakThreshold.Name = "numPeakThreshold";
            this.numPeakThreshold.Size = new System.Drawing.Size(59, 20);
            this.numPeakThreshold.TabIndex = 57;
            this.numPeakThreshold.Value = new decimal(new int[] {
            40,
            0,
            0,
            0});
            // 
            // numPeakHysteresis
            // 
            this.numPeakHysteresis.DecimalPlaces = 1;
            this.numPeakHysteresis.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numPeakHysteresis.Location = new System.Drawing.Point(156, 101);
            this.numPeakHysteresis.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.numPeakHysteresis.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numPeakHysteresis.Name = "numPeakHysteresis";
            this.numPeakHysteresis.Size = new System.Drawing.Size(59, 20);
            this.numPeakHysteresis.TabIndex = 55;
            this.numPeakHysteresis.Value = new decimal(new int[] {
            20,
            0,
            0,
            0});
            // 
            // numMaxPeakWidth
            // 
            this.numMaxPeakWidth.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numMaxPeakWidth.Location = new System.Drawing.Point(156, 122);
            this.numMaxPeakWidth.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.numMaxPeakWidth.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numMaxPeakWidth.Name = "numMaxPeakWidth";
            this.numMaxPeakWidth.Size = new System.Drawing.Size(59, 20);
            this.numMaxPeakWidth.TabIndex = 56;
            this.numMaxPeakWidth.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // tpAnalysisParameters
            // 
            this.tpAnalysisParameters.Controls.Add(lblSubstrateRoughness);
            this.tpAnalysisParameters.Controls.Add(this.numSubstrateRoughness);
            this.tpAnalysisParameters.Controls.Add(lblThresholdParam);
            this.tpAnalysisParameters.Controls.Add(this.numThresholdParam);
            this.tpAnalysisParameters.Controls.Add(lblSmoothingWidth);
            this.tpAnalysisParameters.Controls.Add(this.numSmoothingWidth);
            this.tpAnalysisParameters.Controls.Add(lblMinPitWidth);
            this.tpAnalysisParameters.Controls.Add(this.numMinPitWidth);
            this.tpAnalysisParameters.Controls.Add(lblCompressionFactor);
            this.tpAnalysisParameters.Controls.Add(this.numCompressionFactor);
            this.tpAnalysisParameters.Controls.Add(lblPitPoints);
            this.tpAnalysisParameters.Controls.Add(this.numPitPoints);
            this.tpAnalysisParameters.Location = new System.Drawing.Point(4, 22);
            this.tpAnalysisParameters.Name = "tpAnalysisParameters";
            this.tpAnalysisParameters.Padding = new System.Windows.Forms.Padding(3);
            this.tpAnalysisParameters.Size = new System.Drawing.Size(306, 161);
            this.tpAnalysisParameters.TabIndex = 1;
            this.tpAnalysisParameters.Text = "Analysis Parameters";
            this.tpAnalysisParameters.UseVisualStyleBackColor = true;
            // 
            // numSubstrateRoughness
            // 
            this.numSubstrateRoughness.DecimalPlaces = 1;
            this.numSubstrateRoughness.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numSubstrateRoughness.Location = new System.Drawing.Point(153, 11);
            this.numSubstrateRoughness.Maximum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numSubstrateRoughness.Minimum = new decimal(new int[] {
            5,
            0,
            0,
            -2147483648});
            this.numSubstrateRoughness.Name = "numSubstrateRoughness";
            this.numSubstrateRoughness.Size = new System.Drawing.Size(67, 20);
            this.numSubstrateRoughness.TabIndex = 69;
            // 
            // numThresholdParam
            // 
            this.numThresholdParam.DecimalPlaces = 3;
            this.numThresholdParam.Increment = new decimal(new int[] {
            5,
            0,
            0,
            131072});
            this.numThresholdParam.Location = new System.Drawing.Point(154, 33);
            this.numThresholdParam.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numThresholdParam.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            196608});
            this.numThresholdParam.Name = "numThresholdParam";
            this.numThresholdParam.Size = new System.Drawing.Size(67, 20);
            this.numThresholdParam.TabIndex = 65;
            this.numThresholdParam.Value = new decimal(new int[] {
            65,
            0,
            0,
            131072});
            // 
            // numSmoothingWidth
            // 
            this.numSmoothingWidth.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numSmoothingWidth.Location = new System.Drawing.Point(153, 56);
            this.numSmoothingWidth.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numSmoothingWidth.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numSmoothingWidth.Name = "numSmoothingWidth";
            this.numSmoothingWidth.Size = new System.Drawing.Size(67, 20);
            this.numSmoothingWidth.TabIndex = 67;
            this.numSmoothingWidth.Value = new decimal(new int[] {
            65,
            0,
            0,
            0});
            // 
            // numMinPitWidth
            // 
            this.numMinPitWidth.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numMinPitWidth.Location = new System.Drawing.Point(153, 80);
            this.numMinPitWidth.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numMinPitWidth.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numMinPitWidth.Name = "numMinPitWidth";
            this.numMinPitWidth.Size = new System.Drawing.Size(67, 20);
            this.numMinPitWidth.TabIndex = 64;
            this.numMinPitWidth.Value = new decimal(new int[] {
            15,
            0,
            0,
            0});
            // 
            // numCompressionFactor
            // 
            this.numCompressionFactor.DecimalPlaces = 3;
            this.numCompressionFactor.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.numCompressionFactor.Location = new System.Drawing.Point(153, 105);
            this.numCompressionFactor.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numCompressionFactor.Name = "numCompressionFactor";
            this.numCompressionFactor.Size = new System.Drawing.Size(67, 20);
            this.numCompressionFactor.TabIndex = 66;
            this.numCompressionFactor.Value = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            // 
            // numPitPoints
            // 
            this.numPitPoints.Location = new System.Drawing.Point(153, 132);
            this.numPitPoints.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.numPitPoints.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numPitPoints.Name = "numPitPoints";
            this.numPitPoints.Size = new System.Drawing.Size(67, 20);
            this.numPitPoints.TabIndex = 71;
            this.numPitPoints.Value = new decimal(new int[] {
            4,
            0,
            0,
            0});
            // 
            // tpDataLocation
            // 
            this.tpDataLocation.Controls.Add(lblFilterErrorsFiller);
            this.tpDataLocation.Controls.Add(this.numFilterErrorsThreshold);
            this.tpDataLocation.Controls.Add(this.chkFilterErrors);
            this.tpDataLocation.Controls.Add(lblUnits);
            this.tpDataLocation.Controls.Add(this.cmbUnits);
            this.tpDataLocation.Controls.Add(lblScanAveNumber);
            this.tpDataLocation.Controls.Add(this.numScanAveNumber);
            this.tpDataLocation.Controls.Add(label10);
            this.tpDataLocation.Controls.Add(this.numThicknessRange);
            this.tpDataLocation.Controls.Add(this.lblBoundaryLines);
            this.tpDataLocation.Controls.Add(this.numTargetThickness);
            this.tpDataLocation.Controls.Add(lblDataLocation);
            this.tpDataLocation.Controls.Add(this.txtDataLocation);
            this.tpDataLocation.Controls.Add(this.btnChooseDataLocation);
            this.tpDataLocation.Controls.Add(lblFilenamePattern);
            this.tpDataLocation.Controls.Add(this.txtFilenamePattern);
            this.tpDataLocation.Location = new System.Drawing.Point(4, 22);
            this.tpDataLocation.Name = "tpDataLocation";
            this.tpDataLocation.Size = new System.Drawing.Size(365, 219);
            this.tpDataLocation.TabIndex = 3;
            this.tpDataLocation.Text = "Data Location";
            this.tpDataLocation.UseVisualStyleBackColor = true;
            // 
            // numFilterErrorsThreshold
            // 
            this.numFilterErrorsThreshold.Location = new System.Drawing.Point(104, 166);
            this.numFilterErrorsThreshold.Name = "numFilterErrorsThreshold";
            this.numFilterErrorsThreshold.Size = new System.Drawing.Size(45, 20);
            this.numFilterErrorsThreshold.TabIndex = 75;
            // 
            // chkFilterErrors
            // 
            this.chkFilterErrors.AutoSize = true;
            this.chkFilterErrors.Location = new System.Drawing.Point(21, 167);
            this.chkFilterErrors.Name = "chkFilterErrors";
            this.chkFilterErrors.Size = new System.Drawing.Size(77, 17);
            this.chkFilterErrors.TabIndex = 74;
            this.chkFilterErrors.Text = "Filter errors";
            this.chkFilterErrors.UseVisualStyleBackColor = true;
            // 
            // cmbUnits
            // 
            this.cmbUnits.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbUnits.FormattingEnabled = true;
            this.cmbUnits.Location = new System.Drawing.Point(114, 110);
            this.cmbUnits.Name = "cmbUnits";
            this.cmbUnits.Size = new System.Drawing.Size(109, 21);
            this.cmbUnits.TabIndex = 73;
            // 
            // numScanAveNumber
            // 
            this.numScanAveNumber.Location = new System.Drawing.Point(164, 84);
            this.numScanAveNumber.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numScanAveNumber.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numScanAveNumber.Name = "numScanAveNumber";
            this.numScanAveNumber.Size = new System.Drawing.Size(67, 20);
            this.numScanAveNumber.TabIndex = 71;
            this.numScanAveNumber.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
            // 
            // numThicknessRange
            // 
            this.numThicknessRange.DecimalPlaces = 2;
            this.numThicknessRange.Location = new System.Drawing.Point(258, 139);
            this.numThicknessRange.Maximum = new decimal(new int[] {
            100000000,
            0,
            0,
            0});
            this.numThicknessRange.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.numThicknessRange.Name = "numThicknessRange";
            this.numThicknessRange.Size = new System.Drawing.Size(68, 20);
            this.numThicknessRange.TabIndex = 54;
            this.numThicknessRange.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // numTargetThickness
            // 
            this.numTargetThickness.DecimalPlaces = 2;
            this.numTargetThickness.Location = new System.Drawing.Point(159, 139);
            this.numTargetThickness.Maximum = new decimal(new int[] {
            100000000,
            0,
            0,
            0});
            this.numTargetThickness.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.numTargetThickness.Name = "numTargetThickness";
            this.numTargetThickness.Size = new System.Drawing.Size(68, 20);
            this.numTargetThickness.TabIndex = 53;
            this.numTargetThickness.Value = new decimal(new int[] {
            20,
            0,
            0,
            0});
            // 
            // txtDataLocation
            // 
            this.txtDataLocation.Location = new System.Drawing.Point(114, 20);
            this.txtDataLocation.Name = "txtDataLocation";
            this.txtDataLocation.ReadOnly = true;
            this.txtDataLocation.Size = new System.Drawing.Size(117, 20);
            this.txtDataLocation.TabIndex = 36;
            this.txtDataLocation.Text = "c:\\";
            // 
            // btnChooseDataLocation
            // 
            this.btnChooseDataLocation.Location = new System.Drawing.Point(238, 17);
            this.btnChooseDataLocation.Name = "btnChooseDataLocation";
            this.btnChooseDataLocation.Size = new System.Drawing.Size(57, 23);
            this.btnChooseDataLocation.TabIndex = 37;
            this.btnChooseDataLocation.Text = "Choose";
            this.btnChooseDataLocation.UseVisualStyleBackColor = true;
            // 
            // txtFilenamePattern
            // 
            this.txtFilenamePattern.Location = new System.Drawing.Point(114, 57);
            this.txtFilenamePattern.Name = "txtFilenamePattern";
            this.txtFilenamePattern.Size = new System.Drawing.Size(117, 20);
            this.txtFilenamePattern.TabIndex = 39;
            this.txtFilenamePattern.Text = "scan {nnn}.csv";
            // 
            // tpMisc
            // 
            this.tpMisc.Controls.Add(this.chkEnableQuickScan);
            this.tpMisc.Controls.Add(this.btnChangePassword);
            this.tpMisc.Location = new System.Drawing.Point(4, 22);
            this.tpMisc.Name = "tpMisc";
            this.tpMisc.Padding = new System.Windows.Forms.Padding(3);
            this.tpMisc.Size = new System.Drawing.Size(365, 219);
            this.tpMisc.TabIndex = 4;
            this.tpMisc.Text = "Misc";
            this.tpMisc.UseVisualStyleBackColor = true;
            // 
            // chkEnableQuickScan
            // 
            this.chkEnableQuickScan.AutoSize = true;
            this.chkEnableQuickScan.Checked = true;
            this.chkEnableQuickScan.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkEnableQuickScan.Location = new System.Drawing.Point(25, 59);
            this.chkEnableQuickScan.Name = "chkEnableQuickScan";
            this.chkEnableQuickScan.Size = new System.Drawing.Size(118, 17);
            this.chkEnableQuickScan.TabIndex = 1;
            this.chkEnableQuickScan.Text = "Enable Quick Scan";
            this.chkEnableQuickScan.UseVisualStyleBackColor = true;
            // 
            // btnChangePassword
            // 
            this.btnChangePassword.Location = new System.Drawing.Point(25, 19);
            this.btnChangePassword.Name = "btnChangePassword";
            this.btnChangePassword.Size = new System.Drawing.Size(158, 23);
            this.btnChangePassword.TabIndex = 0;
            this.btnChangePassword.Text = "Change Settings Password";
            this.btnChangePassword.UseVisualStyleBackColor = true;
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSave.Location = new System.Drawing.Point(12, 7);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(57, 27);
            this.btnSave.TabIndex = 23;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOK.Location = new System.Drawing.Point(271, 7);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(57, 27);
            this.btnOK.TabIndex = 24;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(334, 7);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(57, 27);
            this.btnCancel.TabIndex = 25;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // lblHeading
            // 
            this.lblHeading.AutoSize = true;
            this.lblHeading.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHeading.Location = new System.Drawing.Point(12, 9);
            this.lblHeading.Name = "lblHeading";
            this.lblHeading.Size = new System.Drawing.Size(142, 25);
            this.lblHeading.TabIndex = 1;
            this.lblHeading.Text = "DFM Settings";
            // 
            // outerContainer
            // 
            this.outerContainer.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.outerContainer.IsSplitterFixed = true;
            this.outerContainer.Location = new System.Drawing.Point(0, 0);
            this.outerContainer.MinimumSize = new System.Drawing.Size(211, 248);
            this.outerContainer.Name = "outerContainer";
            this.outerContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // outerContainer.Panel1
            // 
            this.outerContainer.Panel1.Controls.Add(this.lblHeading);
            // 
            // outerContainer.Panel2
            // 
            this.outerContainer.Panel2.Controls.Add(this.controlButtonsContainer);
            this.outerContainer.Size = new System.Drawing.Size(403, 353);
            this.outerContainer.SplitterDistance = 41;
            this.outerContainer.TabIndex = 0;
            this.outerContainer.TabStop = false;
            // 
            // DFM_Settings_Form
            // 
            this.AcceptButton = this.btnOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(519, 463);
            this.Controls.Add(this.outerContainer);
            this.MaximizeBox = false;
            this.Name = "DFM_Settings_Form";
            this.Text = "DFM Settings";
            tpScanTiming.ResumeLayout(false);
            tpScanTiming.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numRepeatScanInterval)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numDAQStart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numLaserStart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numDAQStop)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numRepeatCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numRepeatInterval)).EndInit();
            tpMotorDetails.ResumeLayout(false);
            tpMotorDetails.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numMotorLeftPos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numMotorMiddlePos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numMotorRightPos)).EndInit();
            this.controlButtonsContainer.Panel1.ResumeLayout(false);
            this.controlButtonsContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.controlButtonsContainer)).EndInit();
            this.controlButtonsContainer.ResumeLayout(false);
            this.tabSettings.ResumeLayout(false);
            this.tpConnection.ResumeLayout(false);
            this.tabConnection.ResumeLayout(false);
            this.tpSensorConnection.ResumeLayout(false);
            this.tpSensorConnection.PerformLayout();
            this.tpLaserConnection.ResumeLayout(false);
            this.tpLaserConnection.PerformLayout();
            this.tpMotorConnection.ResumeLayout(false);
            this.tpMotorConnection.PerformLayout();
            this.tpScanParameters.ResumeLayout(false);
            this.tabScanParameters.ResumeLayout(false);
            this.tpLaserDetails.ResumeLayout(false);
            this.tpLaserDetails.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numPulseCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numRunTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numGatingFreq)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numDutyCycle)).EndInit();
            this.tpMiscScan.ResumeLayout(false);
            this.tpMiscScan.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numPaintingDelay)).EndInit();
            this.tpDataAnalysis.ResumeLayout(false);
            this.tabDataAnalysis.ResumeLayout(false);
            this.tpDataProcessing.ResumeLayout(false);
            this.tpDataProcessing.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numTruncateFirst)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numTruncateLast)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPeakThreshold)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPeakHysteresis)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numMaxPeakWidth)).EndInit();
            this.tpAnalysisParameters.ResumeLayout(false);
            this.tpAnalysisParameters.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numSubstrateRoughness)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numThresholdParam)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSmoothingWidth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numMinPitWidth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numCompressionFactor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPitPoints)).EndInit();
            this.tpDataLocation.ResumeLayout(false);
            this.tpDataLocation.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numFilterErrorsThreshold)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numScanAveNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numThicknessRange)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numTargetThickness)).EndInit();
            this.tpMisc.ResumeLayout(false);
            this.tpMisc.PerformLayout();
            this.outerContainer.Panel1.ResumeLayout(false);
            this.outerContainer.Panel1.PerformLayout();
            this.outerContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.outerContainer)).EndInit();
            this.outerContainer.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        protected System.Windows.Forms.SplitContainer controlButtonsContainer;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label lblHeading;
        protected System.Windows.Forms.SplitContainer outerContainer;
        protected System.Windows.Forms.TabControl tabSettings;
        private System.Windows.Forms.TabPage tpConnection;
        private System.Windows.Forms.TabPage tpScanParameters;
        private System.Windows.Forms.TabPage tpDataAnalysis;
        private System.Windows.Forms.TabPage tpDataLocation;
        private System.Windows.Forms.Button btnRefreshCOMPorts;
        private System.Windows.Forms.Button btnConnect;
        private System.Windows.Forms.TabControl tabConnection;
        private System.Windows.Forms.TabPage tpSensorConnection;
        private System.Windows.Forms.ComboBox cmbSensorBaudrate;
        private System.Windows.Forms.ComboBox cmbSensorPort;
        private System.Windows.Forms.ComboBox cmbSensorType;
        private System.Windows.Forms.ComboBox cmbSensorModel;
        private System.Windows.Forms.CheckBox chkConnectSensor;
        private System.Windows.Forms.TabPage tpLaserConnection;
        private System.Windows.Forms.ComboBox cmbLaserPort;
        private System.Windows.Forms.ComboBox cmbLaserBaudrate;
        private System.Windows.Forms.CheckBox chkConnectLaser;
        private System.Windows.Forms.TabPage tpMotorConnection;
        private System.Windows.Forms.ComboBox cmbMotorAddress;
        private System.Windows.Forms.CheckBox chkConnectMotor;
        private System.Windows.Forms.TabControl tabScanParameters;
        private System.Windows.Forms.NumericUpDown numRepeatInterval;
        private System.Windows.Forms.NumericUpDown numRepeatCount;
        private System.Windows.Forms.NumericUpDown numDAQStop;
        private System.Windows.Forms.NumericUpDown numDAQStart;
        private System.Windows.Forms.NumericUpDown numLaserStart;
        private System.Windows.Forms.TabPage tpLaserDetails;
        private System.Windows.Forms.RadioButton radRunTime;
        private System.Windows.Forms.RadioButton radPulseCount;
        private System.Windows.Forms.NumericUpDown numRunTime;
        private System.Windows.Forms.NumericUpDown numPulseCount;
        private System.Windows.Forms.NumericUpDown numDutyCycle;
        private System.Windows.Forms.NumericUpDown numGatingFreq;
        private System.Windows.Forms.CheckBox chkSkipMotorMiddlePos;
        private System.Windows.Forms.NumericUpDown numMotorRightPos;
        private System.Windows.Forms.NumericUpDown numMotorLeftPos;
        private System.Windows.Forms.NumericUpDown numMotorMiddlePos;
        private System.Windows.Forms.TabControl tabDataAnalysis;
        private System.Windows.Forms.TabPage tpDataProcessing;
        private System.Windows.Forms.NumericUpDown numTruncateFirst;
        private System.Windows.Forms.NumericUpDown numTruncateLast;
        private System.Windows.Forms.NumericUpDown numMaxPeakWidth;
        private System.Windows.Forms.NumericUpDown numPeakHysteresis;
        private System.Windows.Forms.NumericUpDown numPeakThreshold;
        private System.Windows.Forms.CheckBox chkFilterPeaks;
        private System.Windows.Forms.TabPage tpAnalysisParameters;
        private System.Windows.Forms.NumericUpDown numPitPoints;
        private System.Windows.Forms.NumericUpDown numSubstrateRoughness;
        private System.Windows.Forms.NumericUpDown numCompressionFactor;
        private System.Windows.Forms.NumericUpDown numSmoothingWidth;
        private System.Windows.Forms.NumericUpDown numMinPitWidth;
        private System.Windows.Forms.NumericUpDown numThresholdParam;
        private System.Windows.Forms.TextBox txtFilenamePattern;
        private System.Windows.Forms.Button btnChooseDataLocation;
        private System.Windows.Forms.CheckBox chkSkipMotorRightPos;
        private System.Windows.Forms.FolderBrowserDialog folderBrowser;
        private System.Windows.Forms.CheckBox chkRepeat;
        private System.Windows.Forms.NumericUpDown numThicknessRange;
        private System.Windows.Forms.NumericUpDown numTargetThickness;
        private System.Windows.Forms.NumericUpDown numScanAveNumber;
        private System.Windows.Forms.ComboBox cmbUnits;
        private System.Windows.Forms.Label lblBoundaryLines;
        private System.Windows.Forms.CheckBox chkCentreMiddle;
        private System.Windows.Forms.Label lblMotorUnit1;
        private System.Windows.Forms.Label lblMotorUnit2;
        private System.Windows.Forms.Label lblMotorUnit3;
        private System.Windows.Forms.NumericUpDown numFilterErrorsThreshold;
        private System.Windows.Forms.CheckBox chkFilterErrors;
        private System.Windows.Forms.CheckBox chkHomeAfterScans;
        private System.Windows.Forms.CheckBox chkSkipMotorLeftPos;
        private System.Windows.Forms.TextBox txtMotorRightPositionLabel;
        private System.Windows.Forms.TextBox txtMotorMiddlePositionLabel;
        private System.Windows.Forms.TextBox txtMotorLeftPositionLabel;
        private System.Windows.Forms.TabPage tpMiscScan;
        private System.Windows.Forms.NumericUpDown numPaintingDelay;
        private System.Windows.Forms.CheckBox chkRequirePainting;
        private System.Windows.Forms.CheckBox chkRepeatScan;
        private System.Windows.Forms.NumericUpDown numRepeatScanInterval;
        private System.Windows.Forms.TabPage tpMisc;
        private System.Windows.Forms.Button btnChangePassword;
        private System.Windows.Forms.CheckBox chkDisplayPulseCountErrorMessages;
        private System.Windows.Forms.CheckBox chkFilterPulseCountErrors;
        private System.Windows.Forms.CheckBox chkValidatePulseCount;
        private System.Windows.Forms.Label lblSensorPort;
        private System.Windows.Forms.TextBox txtSensorAddress;
        private System.Windows.Forms.CheckBox chkEnableQuickScan;
        private System.Windows.Forms.ComboBox cmbLaserHandshake;
        public System.Windows.Forms.TextBox txtDataLocation;


    }
}