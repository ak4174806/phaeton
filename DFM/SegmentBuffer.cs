﻿using System;
using System.Collections.Generic;
using System.Text;

using Analysis;
using PaintAnalysis.Common;

namespace DFM
{
    public class SegmentBuffer : List<DFMAnalysis>
    {
        public SegmentBuffer(int maxSize)
            : base(maxSize)
        {
            MaxSize = maxSize;
        }

        public bool IsFull
        {
            get
            {
                return this.Count >= MaxSize;
            }            
        }

        public int MaxSize
        {
            get;
            set;
        }

        public void GetJoinedData(DFMAnalysis analysis)
        {
            List<double> data_x = new List<double>();
            List<double> data_y = new List<double>();

            for (int i = 0; i < Count; i++)
            {
                data_x.AddRange(this[i].Data.Data_X);
                data_y.AddRange(this[i].Data.Data_Y);
            }

            // convert y-values to mm for use in SetData
            analysis.SetData(data_x.ToArray(), Array.ConvertAll<double, double>(data_y.ToArray(), UnitConverter.GetDistanceConverter("micron", "mm")));
        }
    }
}
