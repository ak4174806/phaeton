﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
namespace DFM
{
    public partial class showPitsValue : Form
    {
        public showPitsValue()
        {
            InitializeComponent();
            this.FormClosing += new FormClosingEventHandler(closeform_pitsValue);
        }

        void closeform_pitsValue(object sender, FormClosingEventArgs e)
        {

            this.Hide();
            e.Cancel = true;
        }
       
        //alok 07-Nov-16 code to export data into csv file
         /*
        public void exportCSV_Click(object sender, EventArgs e)
        {   //generate csv only if there is any data into the datatable
            //no csv is generated if there is no rows in it
            if (this.pitsValue.Rows.Count > 1)
            {
                DFM_Simple_Settings_Form settinfrm = new DFM_Simple_Settings_Form();
                string DelfilePath = settinfrm.txtDataLocation.Text + "\\" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + "PitsValue.csv";
                var sb = new StringBuilder();

                var headers = this.pitsValue.Columns.Cast<DataGridViewColumn>();
                sb.AppendLine(string.Join(",", headers.Select(column => "\"" + column.HeaderText + "\"").ToArray()));

                foreach (DataGridViewRow row in this.pitsValue.Rows)
                {
                    var cells = row.Cells.Cast<DataGridViewCell>();
                    sb.AppendLine(string.Join(",", cells.Select(cell => "\"" + cell.Value + "\"").ToArray()));
                }

                File.WriteAllText(DelfilePath, sb.ToString());

                
                var scan_id = " ";
                int sId=0;
                long[] scanID=new long[]{};
                string[] date=new string[]{};
                long[] pitValues=new long[]{};
                //writing the data column wise into csv
                foreach (DataGridViewRow row in this.pitsValue.Rows)
                {
                    if ((string)row.Cells["scan_id"].Value != null)
                    {
                        //if scan id is equal keep inserting the data rows, if not then add new column and add data
                        if (scan_id != (string)row.Cells["scan_id"].Value)
                        {
                            scan_id = (string)row.Cells["scan_id"].Value;
                            scanID[sId]=(long)row.Cells["scan_id"].Value;
                            date[sId++]=(string)row.Cells["date"].Value;
                        }

                }
            }
            }
        }*/

        //alok 07-Nov-16 code to export data into csv file
        public void exportCSV_Click(object sender, EventArgs e)
        {
            export_data_csv();
            
        }
        public void  export_data_csv(){
            //generate csv only if there is any data into the datatable
            //no csv is generated if there is no rows in it
            if (this.pitsValue.Rows.Count > 1)
            {
                DFM_Simple_Settings_Form settinfrm = new DFM_Simple_Settings_Form();
                string DelfilePath = settinfrm.txtDataLocation.Text + "\\" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + "PitsValue.csv";
                var sb = new StringBuilder();

                var headers = this.pitsValue.Columns.Cast<DataGridViewColumn>();
                sb.AppendLine(string.Join(",", headers.Select(column => "\"" + column.HeaderText + "\"").ToArray()));

                foreach (DataGridViewRow row in this.pitsValue.Rows)
                {
                    var cells = row.Cells.Cast<DataGridViewCell>();
                    sb.AppendLine(string.Join(",", cells.Select(cell => "\"" + cell.Value + "\"").ToArray()));
                }

                File.WriteAllText(DelfilePath, sb.ToString());

            }
        }

       
        
        
    }
}
