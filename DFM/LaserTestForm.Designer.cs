﻿namespace DFM
{
    partial class LaserTestForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.GroupBox grpConnect;
            System.Windows.Forms.Label lblCommand;
            this.cmbCOM = new System.Windows.Forms.ComboBox();
            this.btnRun = new System.Windows.Forms.Button();
            this.btnConnect = new System.Windows.Forms.Button();
            this.lblCOM = new System.Windows.Forms.Label();
            this.btnRefreshCOM = new System.Windows.Forms.Button();
            this.txtCommand = new System.Windows.Forms.TextBox();
            grpConnect = new System.Windows.Forms.GroupBox();
            lblCommand = new System.Windows.Forms.Label();
            grpConnect.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpConnect
            // 
            grpConnect.Controls.Add(this.txtCommand);
            grpConnect.Controls.Add(this.cmbCOM);
            grpConnect.Controls.Add(this.btnRun);
            grpConnect.Controls.Add(this.btnConnect);
            grpConnect.Controls.Add(lblCommand);
            grpConnect.Controls.Add(this.lblCOM);
            grpConnect.Controls.Add(this.btnRefreshCOM);
            grpConnect.Location = new System.Drawing.Point(12, 12);
            grpConnect.Name = "grpConnect";
            grpConnect.Size = new System.Drawing.Size(263, 146);
            grpConnect.TabIndex = 23;
            grpConnect.TabStop = false;
            grpConnect.Text = "Connect to laser";
            // 
            // cmbCOM
            // 
            this.cmbCOM.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCOM.FormattingEnabled = true;
            this.cmbCOM.Location = new System.Drawing.Point(109, 26);
            this.cmbCOM.Name = "cmbCOM";
            this.cmbCOM.Size = new System.Drawing.Size(75, 21);
            this.cmbCOM.TabIndex = 19;
            // 
            // btnRun
            // 
            this.btnRun.Location = new System.Drawing.Point(194, 95);
            this.btnRun.Name = "btnRun";
            this.btnRun.Size = new System.Drawing.Size(63, 23);
            this.btnRun.TabIndex = 21;
            this.btnRun.Text = "Run";
            this.btnRun.UseVisualStyleBackColor = true;
            // 
            // btnConnect
            // 
            this.btnConnect.Location = new System.Drawing.Point(108, 54);
            this.btnConnect.Name = "btnConnect";
            this.btnConnect.Size = new System.Drawing.Size(75, 23);
            this.btnConnect.TabIndex = 21;
            this.btnConnect.Text = "Connect";
            this.btnConnect.UseVisualStyleBackColor = true;
            // 
            // lblCOM
            // 
            this.lblCOM.AutoSize = true;
            this.lblCOM.Location = new System.Drawing.Point(19, 31);
            this.lblCOM.Name = "lblCOM";
            this.lblCOM.Size = new System.Drawing.Size(56, 13);
            this.lblCOM.TabIndex = 18;
            this.lblCOM.Text = "COM Port:";
            // 
            // btnRefreshCOM
            // 
            this.btnRefreshCOM.Location = new System.Drawing.Point(194, 26);
            this.btnRefreshCOM.Name = "btnRefreshCOM";
            this.btnRefreshCOM.Size = new System.Drawing.Size(63, 23);
            this.btnRefreshCOM.TabIndex = 20;
            this.btnRefreshCOM.Text = "Refresh";
            this.btnRefreshCOM.UseVisualStyleBackColor = true;
            // 
            // lblCommand
            // 
            lblCommand.AutoSize = true;
            lblCommand.Location = new System.Drawing.Point(19, 100);
            lblCommand.Name = "lblCommand";
            lblCommand.Size = new System.Drawing.Size(57, 13);
            lblCommand.TabIndex = 18;
            lblCommand.Text = "Command:";
            // 
            // txtCommand
            // 
            this.txtCommand.Location = new System.Drawing.Point(108, 97);
            this.txtCommand.Name = "txtCommand";
            this.txtCommand.Size = new System.Drawing.Size(75, 20);
            this.txtCommand.TabIndex = 22;
            // 
            // LaserTestForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(292, 171);
            this.Controls.Add(grpConnect);
            this.Name = "LaserTestForm";
            this.Text = "Test Laser";
            grpConnect.ResumeLayout(false);
            grpConnect.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox cmbCOM;
        private System.Windows.Forms.Button btnRun;
        private System.Windows.Forms.Button btnConnect;
        private System.Windows.Forms.Label lblCOM;
        private System.Windows.Forms.Button btnRefreshCOM;
        private System.Windows.Forms.TextBox txtCommand;

    }
}