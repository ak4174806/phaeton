﻿using System;
using System.Collections.Generic;
using System.Text;

using System.Xml.Serialization;

namespace DFM.Settings
{
    public interface ISettingsGroup : ISetting
    {
        BaseSetting FetchSetting(string[] option);
        BaseSetting FetchSetting(string option);
        
        bool TryFetchSetting(string[] option, out BaseSetting value);
        bool TryFetchSetting(string option, out BaseSetting value);

        T Fetch<T>(string[] option);
        T Fetch<T>(string option); 
        
        bool TryFetch<T>(string[] option, out T value);
        bool TryFetch<T>(string option, out T value);

        ValidationResult[] ValidationErrors { get; }

        // subclass: SettingsGroup, HybridSettings, SettingsProfile
    }
}