﻿using System;
using System.Collections.Generic;
using System.Text;

using PaintAnalysis.Common;
using System.Xml.Serialization;
using NLog;

namespace DFM.Settings
{
    public class SettingsGroup : BaseSetting, ISettingsGroup
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();       
        
        #region Data Members

        private List<BaseSetting> settings;

        #endregion

        #region Constructors

        static SettingsGroup()
        {
            BaseSetting.SettingTypes.Add(typeof(SettingsGroup));
        }

        protected SettingsGroup()
            : base()
        {
            settings = new List<BaseSetting>();
            IsEnabled = IsEnabled;
            IsValidationEnabled = IsValidationEnabled;
        }

        public SettingsGroup(string name)
            : base(name)
        {
            settings = new List<BaseSetting>();
            IsEnabled = IsEnabled;
            IsValidationEnabled = IsValidationEnabled;
        }

        #endregion

        #region ISettingsGroup Members

        #region Fetching

        public T Fetch<T>(string[] option)
        {
            T value;
            bool result = TryFetch<T>(option, out value);
            if (result)
                return value;
            else
            {
                logger.Error("Fetching invalid option or setting type: " + String.Join("/", option));
                throw new ArgumentException("Invalid option or setting type", "option");
            }
        }
        public T Fetch<T>(string option)
        {
            return Fetch<T>(option.Split('/'));
        }

        public bool TryFetch<T>(string[] option, out T value)
        {
            value = default(T);
            
            BaseSetting setting;            
            bool result = TryFetchSetting(option, out setting);
            if (result)
            {
                SettingValue<T> settingValue = setting as SettingValue<T>;
                if (settingValue != null)
                {
                    value = settingValue.Value;
                    return true;
                }
                else
                {
                    logger.Info("Tried to fetch wrong type for a setting: " + String.Join(" ", option) + ". Type was: " + typeof(T).ToString());
                    return false;
                }
            }
            return false;
        }
        public bool TryFetch<T>(string option, out T value)
        {
            return TryFetch<T>(option.Split('/'), out value);
        }
        
        public BaseSetting FetchSetting(string[] option)
        {
            BaseSetting value;
            bool result = TryFetchSetting(option, out value);
            if (result)
                return value;
            else
            {
                logger.Error("Fetching invalid option or setting type: " + String.Join("/", option));                
                throw new ArgumentException("Invalid option or setting type", "option");
            }
        }
        public BaseSetting FetchSetting(string option)
        {
            return FetchSetting(option.Split('/'));
        }

        public bool TryFetchSetting(string[] option, out BaseSetting value)
        {
            SettingsGroup grp = this;
            value = null;

            for (int i = 0; i < option.Length - 1; i++)
            {
                if (grp.ContainsKey(option[i]))
                {
                    grp = grp[option[i]] as SettingsGroup;
                    if (grp == null) return false;
                }
                else
                {
                    logger.Info("Tried to fetch invalid setting (could not find key '" + option[i] + "'): " + String.Join("/", option));
                    return false;
                }
            }

            if (grp.ContainsKey(option[option.Length - 1]))
            {
                BaseSetting setting = grp[option[option.Length - 1]] as BaseSetting;
                if (setting != null)
                {
                    value = setting;
                    return true;
                }
            }

            logger.Info("Tried to fetch invalid setting (could not find key '" + option[option.Length - 1] + "'): " + String.Join("/", option));                   
            return false;
        }
        public bool TryFetchSetting(string option, out BaseSetting value)
        {
            return TryFetchSetting(option.Split('/'), out value);
        }

        #endregion

        #region Validation

        public ValidationResult[] ValidationErrors
        {
            get
            {
                List<ValidationResult> errors = new List<ValidationResult>();
                if (IsValidationEnabled)
                {
                    foreach (BaseSetting setting in Settings)
                    {
                        if (!setting.IsValid)
                        {
                            if (setting is ISettingValue)
                            {
                                ISettingValue csetting = (ISettingValue)setting;
                                errors.Add(csetting.LastValidationErrorMessage);
                            }
                            else if (setting is ISettingsGroup)
                            {
                                ISettingsGroup csetting = (ISettingsGroup)setting;
                                errors.AddRange(csetting.ValidationErrors);
                            }
                            else
                            {
                                logger.Error("Could not identify setting in SettingsGroup.Settings as either ISettingValue or ISettingsGroup");
                            }
                        }
                    }
                }
                return errors.ToArray();
            }
        }

        [XmlIgnore()]
        public override bool IsValid
        {
            get
            {
                if (!IsValidationEnabled) return true;

                foreach (BaseSetting setting in Settings)
                {
                    if (!setting.IsValid) 
                        return false;
                }
                return true;
            }
        }

        #endregion

        void value_ModifiedChanged(object sender, EventArgs e)
        {
            OnModifiedChanged(new EventArgs());
        }        

        #endregion
        
        #region ISetting Members

        public override void Revert()
        {
            foreach (BaseSetting setting in settings)
            {
                setting.Revert();
            }
        }

        public override void Save()
        {
            foreach (BaseSetting setting in settings)
            {
                setting.Save();
            }
        }

        public override void LoadSettingsFrom(BaseSetting other)
        {
            base.LoadSettingsFrom(other);

            SettingsGroup group = other as SettingsGroup;
            BaseSetting setting;
            bool result;
            if (group != null)
            {
                for (int i = 0; i < Count; i++)
                {
                    result = group.TryFetchSetting(this[i].Name, out setting);
                    if (result)
                    {
                        this[i].LoadSettingsFrom(setting);
                    }
                }
            }
        }

        [XmlIgnore()]
        public override bool IsModified
        {
            get
            {
                foreach (BaseSetting setting in Settings)
                {
                    if (setting.IsModified) 
                        return true;
                }
                return false;
            }            
        }

        [XmlIgnore()]
        public override bool IsEnabled
        {
            get { return base.IsEnabled; }
            set
            {
                base.IsEnabled = value;
                
                // was cancelled
                if (base.IsEnabled != value) return;

                if (Settings == null) return;
                
                // cache enableds so we can cancel event if one of them fails
                for (int i = 0; i < Settings.Count; i++)
                {
                    Settings[i].IsEnabled = value;
                }
            }
        }

        [XmlIgnore()]
        public override bool IsValidationEnabled
        {
            get { return base.IsValidationEnabled; }
            set
            {
                base.IsValidationEnabled = value;

                // was cancelled
                if (base.IsValidationEnabled != value) return;

                if (Settings == null) return;

                // cache enableds so we can cancel event if one of them fails
                for (int i = 0; i < Settings.Count; i++)
                {
                    Settings[i].IsValidationEnabled = value;
                }
            }
        }


        #endregion
                
        #region Cloning
        public override BaseSetting DeepCopy()
        {
            SettingsGroup clone = (SettingsGroup)base.DeepCopy();

            foreach (BaseSetting item in settings)
            {
                clone.Add(item.DeepCopy());
            }

            return clone;
        }

        protected override BaseSetting GetNew()
        {
            return new SettingsGroup();
        }
        #endregion

        #region XmlSerializable Data Members

        [XmlArray("SettingsList")]
        [XmlArrayItem("Setting")]
        public List<BaseSetting> Settings
        {
            get
            {
                return settings;
            }
            set
            {
                settings = value;
            }
        }

        #endregion

        #region Dictionary Overrides

        /// <summary>
        /// Adds the specified setting to the group, using the setting name as a key.
        /// </summary>
        /// <param name="setting">The value of the setting to add. Can not be null</param>
        public virtual void Add(BaseSetting setting)
        {
            if (setting == null)
                throw new ArgumentNullException("setting", "Setting cannot be null");

            Add(setting.Name, setting);
        }

        public virtual void Add(string key, BaseSetting value)
        {
            if (ContainsKey(key))
                throw new ArgumentException("Key not unique");

            settings.Add(value);
            value.ModifiedChanged += new EventHandler(value_ModifiedChanged);
        }

        public bool ContainsKey(string key)
        {
            foreach (BaseSetting setting in settings)
                if (setting.Name == key)
                    return true;

            return false;
         }

        #endregion               
        
        #region Dictionary Decorators

        [XmlIgnore()]
        public ICollection<string> Keys
        {
            get
            {
                List<string> keys = new List<string>();
                foreach (BaseSetting setting in settings)
                    keys.Add(setting.Name);
                
                return keys;
            }
        }

        public bool Remove(string key)
        {
            for (int i = 0; i < Count; i++)
                if (settings[i].Name == key)
                {
                    settings.RemoveAt(i);
                    return true;
                }

            return false;
        }

        public bool TryGetValue(string key, out BaseSetting value)
        {
            foreach (BaseSetting setting in settings)
                if (setting.Name == key)
                {
                    value = setting;
                    return true;
                }

            value = null;
            return false;
        }

        [XmlIgnore()]
        public ICollection<BaseSetting> Values
        {
            get { return settings; }
        }

        [XmlIgnore()]
        public BaseSetting this[string key]
        {
            get
            {
                foreach (BaseSetting setting in settings)
                    if (setting.Name == key)
                    {
                        return setting;
                    }

                throw new KeyNotFoundException("Could not find key: " + key);
            }
            set 
            {
                for (int i = 0; i < Count; i++)
                    if (settings[i].Name == key)
                    {
                        settings[i] = value;
                        return;
                    }

                Add(value);
            }
        }

        [XmlIgnore()]
        public BaseSetting this[int key]
        {
            get
            {
                return settings[key];
            }
            set
            {
                settings[key] = value;
            }
        }

        public virtual void Add(KeyValuePair<string, BaseSetting> item)
        {
            Add(item.Key, item.Value);
        }

        [XmlIgnore()]
        public int Count
        {
            get { return settings.Count; }
        }

        public void Clear()
        {
            settings.Clear();
        }

        public bool Remove(KeyValuePair<string, BaseSetting> item)
        {
            if (item.Key != item.Value.Name) return false;

            for (int i = 0; i < Count; i++)
                if (item.Value.Equals(settings[i]))
                {
                    settings.RemoveAt(i);
                    return true;
                }
            return false;
        }

        public bool Contains(KeyValuePair<string, BaseSetting> item)
        {
            foreach (BaseSetting setting in settings)
                if (setting.Name == item.Key)
                {
                    if (item.Value.Equals(setting))
                        return true;
                    else
                        return false;
                }
	
            return false;
        }

        public void CopyTo(KeyValuePair<string, BaseSetting>[] array, int arrayIndex)
        {
            if (array == null)
                throw new ArgumentNullException("array cannot be null");
            if (arrayIndex < 0)
                throw new ArgumentOutOfRangeException("arrayIndex can't be negative");
            if (arrayIndex + Count > array.Length)
                throw new ArgumentException("Not enough space in array");

            foreach (KeyValuePair<string, BaseSetting> item in this)
            {
                array[arrayIndex++] = item;
            }
        }
                
        public IEnumerator<KeyValuePair<string, BaseSetting>> GetEnumerator()
        {
            foreach (BaseSetting setting in settings)
            {
                yield return new KeyValuePair<string, BaseSetting>(setting.Name, setting);
            }
        }

        #endregion        
    }
}
