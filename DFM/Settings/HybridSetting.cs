﻿using System;
using System.Collections.Generic;
using System.Text;

using PaintAnalysis.Common;
using System.Xml.Serialization;
using DFM.Settings.Bindable;

namespace DFM.Settings
{
    public class HybridSetting : SettingsGroup
    {
        #region Data Members

        protected SettingValue<bool> enableSetting;
        protected bool changeValidationEnabled;

        #endregion

        #region Constructors

        static HybridSetting()
        {
            BaseSetting.SettingTypes.Add(typeof(HybridSetting));
        }

        protected HybridSetting()
            : base()
        { }

        public HybridSetting(string name)
            : base(name)
        { }

        #endregion
        
        #region Enable Settings
                
        /// <summary>
        /// Adds a bindable bool setting with the behaviour that it toggles all other settings' enabled status in this group, if they are bindable
        /// </summary>
        /// <param name="enableSetting"></param>
        public void AddEnableSetting(SettingValue<bool> enableSetting, bool changeValidationEnabled=true)
        {
            if (this.enableSetting != null)
                throw new ArgumentException("Can only have one enable setting at a time");

            this.enableSetting = enableSetting;
            this.changeValidationEnabled = changeValidationEnabled;

            Add(enableSetting);
            enableSetting.ValueChanged += new EventHandler<ValueChangedEventArgs<bool>>(enableSetting_ValueChanged);
            enableSetting_ValueChanged(enableSetting, new ValueChangedEventArgs<bool>(enableSetting.Value, enableSetting));            
        }

        void enableSetting_ValueChanged(object sender, ValueChangedEventArgs<bool> e)
        {
            if (Name == "form enabling")
            {
                int a = 1;
                a += 1;
                bool x;
                a = Settings.Count;
                if (Settings.Count > 1) x = Settings[1].IsValidationEnabled;
            }

            if (enableSetting != null)
            {
                foreach (BaseSetting setting in Settings)
                {
                    if (!setting.Equals(enableSetting))
                    {                        
                        setting.IsEnabled = e.Value;
                        if (changeValidationEnabled) setting.IsValidationEnabled = e.Value;
                    }
                }
            }
        }

        /// <summary>
        /// Fix it so that hybrid settings can be disabled by parent hybrids, but not enabled unless enabler is set
        /// </summary>
        [XmlIgnore()]
        public override bool IsEnabled
        {
            get { return base.IsEnabled; }
            set
            {
                // if trying to enable
                if (value)
                {
                    // if enabler says this hybrid should be enabled, go ahead
                    if (enableSetting == null || enableSetting.Value)
                        base.IsEnabled = value;
                    else if (value)
                    {
                        // otherwise, just enabled the enabler
                        enableSetting.IsEnabled = true;
                    }
                }
                else
                {
                    // if disabling, go ahead
                    base.IsEnabled = value;
                }
            }
        }

        [XmlIgnore()]
        public override bool IsValidationEnabled
        {
            get { return base.IsValidationEnabled; }
            set
            {
                // if trying to enable
                if (value)
                {
                    // if enabler says this hybrid should be enabled, go ahead
                    if (enableSetting == null || enableSetting.Value || !changeValidationEnabled)
                        base.IsValidationEnabled = value;
                }
                else
                {
                    // if disabling, go ahead
                    base.IsValidationEnabled = value;
                }
            }
        }

        public override void Add(BaseSetting setting)
        {
            base.Add(setting);
            addEnabledChangingHandlers(setting);
            if (enableSetting != null) enableSetting_ValueChanged(enableSetting, new ValueChangedEventArgs<bool>(enableSetting.Value, enableSetting));
        }

        public override void Add(KeyValuePair<string, BaseSetting> item)
        {
            base.Add(item);
            addEnabledChangingHandlers(item.Value);
            if (enableSetting != null) enableSetting_ValueChanged(enableSetting, new ValueChangedEventArgs<bool>(enableSetting.Value, enableSetting));
        }

        public override void Add(string key, BaseSetting value)
        {
            base.Add(key, value);
            addEnabledChangingHandlers(value);
            if (enableSetting != null) enableSetting_ValueChanged(enableSetting, new ValueChangedEventArgs<bool>(enableSetting.Value, enableSetting));            
        }

        private void addEnabledChangingHandlers(BaseSetting setting)
        {
            if (setting is SettingsGroup)
            {
                SettingsGroup tmp = (SettingsGroup)setting;
                foreach (BaseSetting s in tmp.Settings)
                    addEnabledChangingHandlers(s);
            }
            else
            {
                setting.EnabledChanging += new EventHandler<EnabledChangingEventArgs>(setting_EnabledChanging);
                setting.ValidationEnabledChanging += new EventHandler<EnabledChangingEventArgs>(setting_ValidationEnabledChanging);
            }
        }
        
        void setting_EnabledChanging(object sender, EnabledChangingEventArgs e)
        {
            if (e.InheritEnabled && e.Enabled && (enableSetting != null && sender != enableSetting && !enableSetting.Value)) e.Cancel = true;
        }

        void setting_ValidationEnabledChanging(object sender, EnabledChangingEventArgs e)
        {
            if (e.InheritEnabled && e.Enabled && (enableSetting != null && sender != enableSetting && !enableSetting.Value && changeValidationEnabled)) e.Cancel = true;
        }

        #endregion        

        #region Cloning
        public override BaseSetting DeepCopy()
        {
            HybridSetting clone = (HybridSetting)base.DeepCopy();
            clone.changeValidationEnabled = changeValidationEnabled;

            if (enableSetting != null)
                clone.AddEnableSetting(enableSetting);

            return clone;
        }

        protected override BaseSetting GetNew()
        {
            return new HybridSetting();
        }
        #endregion
    }
}
