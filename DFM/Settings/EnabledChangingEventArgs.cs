﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DFM.Settings
{
    public class EnabledChangingEventArgs : EventArgs
    {
        private bool enabled;
        public bool Enabled
        {
            get { return enabled; }
            set { enabled = value; }
        }

        private bool cancel;
        public bool Cancel
        {
            get { return cancel; }
            set { cancel = value; }
        }

        public bool InheritEnabled
        {
            get;
            set;
        }

        public EnabledChangingEventArgs(bool enabled, bool inheritEnabled)
        {
            Enabled = enabled;
            InheritEnabled = inheritEnabled;
        }
    }
}
