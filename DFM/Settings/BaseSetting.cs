﻿using System;
using System.Collections.Generic;
using System.Text;

using System.Xml.Serialization;

namespace DFM.Settings
{
    public class BaseSetting : ISetting
    {
        /// <summary>
        /// A list of the base classes of this that have been created, updated in static constructors
        /// </summary>
        public static List<Type> SettingTypes = null;

        #region Constructors

        static BaseSetting()
        {
            if (SettingTypes == null)
                SettingTypes = new List<Type>();

            SettingTypes.Add(typeof(BaseSetting));
        }

        protected BaseSetting()
        {
            InheritEnabled = true;
        }
        public BaseSetting(string name)
        {
            Name = name;
            IsEnabled = true;
            IsValidationEnabled = true;
            InheritEnabled = true;
        }

        #endregion

        #region ISetting Members

        private string name;
        private bool isEnabled;
        private bool inheritEnabled;
        private bool isValidationEnabled;

        /// <summary>
        /// An identifier for this setting
        /// </summary>
        [XmlElement("Name")]
        public virtual string Name
        {
            get { return name; }
            set { name = value; }
        }

        public virtual void Revert()
        { }

        public virtual void Save()
        { }

        public virtual void LoadSettingsFrom(BaseSetting other)
        { }

        [XmlIgnore()]
        public virtual bool IsModified
        {
            get { return false; }
        }

        [XmlIgnore()]
        public virtual bool IsValid
        {
            get { return true; }
        }

        [XmlIgnore()]
        public virtual bool IsEnabled
        {
            get { return isEnabled; }
            set
            {
                EnabledChangingEventArgs e = new EnabledChangingEventArgs(value, InheritEnabled);
                OnEnabledChanging(e);
                if (e.Cancel) return;

                isEnabled = value;
                OnEnabledChanged(new EventArgs());
            }
        }

        [XmlIgnore()]
        public virtual bool InheritEnabled
        {
            get { 
                return inheritEnabled; }
            set { inheritEnabled = value; }
        }

        [XmlIgnore()]
        public virtual bool IsValidationEnabled
        {
            get { return isValidationEnabled; }
            set
            {
                EnabledChangingEventArgs e = new EnabledChangingEventArgs(value, InheritEnabled);
                OnValidationEnabledChanging(e);
                if (e.Cancel) return;

                isValidationEnabled = value;
                OnValidationEnabledChanged(new EventArgs());
            }
        }

        #endregion

        #region Events

        public virtual event EventHandler ModifiedChanged;
        public event EventHandler EnabledChanged;
        public event EventHandler<EnabledChangingEventArgs> EnabledChanging;
        public event EventHandler ValidationEnabledChanged;
        public event EventHandler<EnabledChangingEventArgs> ValidationEnabledChanging;

        protected void OnModifiedChanged(EventArgs e)
        {
            if (ModifiedChanged != null)
                ModifiedChanged(this, e);
        }        

        protected void OnEnabledChanged(EventArgs e)
        {
            if (EnabledChanged != null)
                EnabledChanged(this, e);
        }

        protected void OnEnabledChanging(EnabledChangingEventArgs e)
        {
            if (EnabledChanging != null)
                EnabledChanging(this, e);
        }

        protected void OnValidationEnabledChanged(EventArgs e)
        {
            if (ValidationEnabledChanged != null)
                ValidationEnabledChanged(this, e);
        }

        protected void OnValidationEnabledChanging(EnabledChangingEventArgs e)
        {
            if (ValidationEnabledChanging != null)
                ValidationEnabledChanging(this, e);
        }

        #endregion

        #region Cloning

        public virtual BaseSetting DeepCopy()
        {
            BaseSetting clone = GetNew();
            clone.Name = Name;
            clone.IsEnabled = IsEnabled;
            clone.IsValidationEnabled = IsValidationEnabled;
            return clone;
        }
        
        protected virtual BaseSetting GetNew()
        {
            return new BaseSetting();
        }

        #endregion

        /* To implement:
         * 1) static constructor that adds its type to SettingTypes (if more properties)
         * 2) protected constructor with no arguments
         * 3) Xml Attributes for any properties
         * 4) Implement Revert and Save and LoadSettingsFrom
         * 5) Override GetNew and DeepCopy
         * 6) Public constructor that calls the main constructor here
         */
    }
}
