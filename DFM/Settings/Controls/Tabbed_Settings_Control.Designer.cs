﻿namespace DFM.Settings.Controls
{
    partial class Tabbed_Settings_Control
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabSettings = new System.Windows.Forms.TabControl();
            this.outerContainer.Panel2.SuspendLayout();
            this.outerContainer.SuspendLayout();
            this.controlButtonsContainer.Panel1.SuspendLayout();
            this.controlButtonsContainer.SuspendLayout();
            this.SuspendLayout();
            // 
            // outerContainer
            // 
            this.outerContainer.Size = new System.Drawing.Size(392, 359);
            // 
            // controlButtonsContainer
            // 
            // 
            // controlButtonsContainer.Panel1
            // 
            this.controlButtonsContainer.Panel1.Controls.Add(this.tabSettings);
            this.controlButtonsContainer.Size = new System.Drawing.Size(392, 314);
            this.controlButtonsContainer.SplitterDistance = 267;
            // 
            // tabSettings
            // 
            this.tabSettings.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tabSettings.Location = new System.Drawing.Point(12, 3);
            this.tabSettings.Name = "tabSettings";
            this.tabSettings.SelectedIndex = 0;
            this.tabSettings.Size = new System.Drawing.Size(368, 261);
            this.tabSettings.TabIndex = 2;
            // 
            // Tabbed_Settings_Control
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.FormTitle = "Tabbed Settings";
            this.Name = "Tabbed_Settings_Control";
            this.Size = new System.Drawing.Size(392, 359);
            this.outerContainer.Panel2.ResumeLayout(false);
            this.outerContainer.ResumeLayout(false);
            this.controlButtonsContainer.Panel1.ResumeLayout(false);
            this.controlButtonsContainer.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.TabControl tabSettings;


    }
}
