﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

using System.ComponentModel.Design;

namespace DFM.Settings.Controls
{
    [Designer("System.Windows.Forms.Design.ParentControlDesigner, System.Design", typeof(IDesigner))]
    public partial class Base_Settings_Control : UserControl
    {
        public Base_Settings_Control()
        {
            InitializeComponent();

            FormTitle = "Settings";
            btnSave.Click += new EventHandler(btnSave_Click);
            btnCancel.Click += new EventHandler(btnCancel_Click);
            btnOK.Click += new EventHandler(btnOK_Click);
        }

        void btnOK_Click(object sender, EventArgs e)
        {
            OK();
        }

        void btnCancel_Click(object sender, EventArgs e)
        {
            Cancel();
        }

        void btnSave_Click(object sender, EventArgs e)
        {
            Save();
        }

        [Description("The title of the form"),
        Category("Appearance"),
        DefaultValue("Settings"),
        Browsable(true)]
        public string FormTitle
        {
            get { return lblHeading.Text; }
            set
            {
                lblHeading.Text = Text = value;
            }
        }

        protected virtual void OK()
        { }

        protected virtual void Save()
        { }

        protected virtual void Cancel()
        { }
    }
}
