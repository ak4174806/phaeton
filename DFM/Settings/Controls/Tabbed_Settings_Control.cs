﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

using System.ComponentModel.Design;

namespace DFM.Settings.Controls
{
    [Designer("System.Windows.Forms.Design.ParentControlDesigner, System.Design", typeof(IDesigner))]
    public partial class Tabbed_Settings_Control : Base_Settings_Control
    {
        public Tabbed_Settings_Control()
        {
            InitializeComponent();
        }

        [Description("The TabPages in the TabControl"),
        Category("Behaviour"),
        DefaultValue(typeof(TabControl.TabPageCollection), "null"),
        Browsable(true)]
        public TabControl.TabPageCollection TabPages
        {
            get { return tabSettings.TabPages; }
        }
    }
}
