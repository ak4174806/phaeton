﻿using System;
using System.Collections.Generic;
using System.Text;

using System.Xml.Serialization;

namespace DFM.Settings
{
    public interface ISetting
    {
        string Name { get; }
        void Revert();
        void Save();

        BaseSetting DeepCopy();
        void LoadSettingsFrom(BaseSetting other);

        bool IsModified { get; }
        bool IsValid { get; }
        bool IsEnabled { get; set; }
        bool IsValidationEnabled { get; set; }
        bool InheritEnabled { get; set; }

        event EventHandler ModifiedChanged;

        event EventHandler EnabledChanged;
        event EventHandler<EnabledChangingEventArgs> EnabledChanging;

        event EventHandler ValidationEnabledChanged;
        event EventHandler<EnabledChangingEventArgs> ValidationEnabledChanging;
    }
}
