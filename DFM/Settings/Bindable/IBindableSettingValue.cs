﻿using System;
using System.Collections.Generic;
using System.Text;

using System.Windows.Forms;

namespace DFM.Settings.Bindable
{
    public interface IBindableSettingValue<Tdata, Tcontrol> : ISettingValue<Tdata>, IBaseBindableSettingValue
    {
        // need to temporarily ignore events to prevent loops?
        void Bind(Tcontrol control);

        void Unbind(Tcontrol control);

        bool TryUnbind(Tcontrol control);
    }
}
