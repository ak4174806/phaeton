﻿using System;
using System.Collections.Generic;
using System.Text;

using System.Windows.Forms;

namespace DFM.Settings.Bindable
{
    public class BindableScaledSetting : BindableFloatSetting
    {
        #region Constructors
        static BindableScaledSetting()
        {
            BaseSetting.SettingTypes.Add(typeof(BindableScaledSetting));
        }

        protected BindableScaledSetting()
            : base()
        {
            // deliberately don't set scale to 1 - that way 0 can be used when no scale has been set
            Scale = 0.0;
        }

        /// <summary>
        /// Creates a bound setting with the current and saved values set to the default.
        /// </summary>
        /// <param name="control">The control associated with this setting</param>
        /// <param name="name">A unique name for the setting, used internally for debugging</param>
        /// <param name="defaultValue">The default value for the setting to initialise. Useful for return to default</param>
        public BindableScaledSetting(NumericUpDown control, string name, float defaultValue)
            : base(control, name, defaultValue)
        {
            // deliberately don't set scale to 1 - that way 0 can be used when no scale has been set
            Scale = 0.0;
        }

        /// <summary>
        /// Creates a bound setting with the saved value set to value.
        /// </summary>
        /// <param name="control">The control associated with this setting</param>
        /// <param name="name">A unique name for the setting, used internally for debugging</param>
        /// <param name="defaultValue">The default value for the setting. Useful for return to default</param>
        /// <param name="value">The current value</param>
        public BindableScaledSetting(NumericUpDown control, string name, float defaultValue, float value)
            : base(control, name, defaultValue, value)
        { 
            // deliberately don't set scale to 1 - that way 0 can be used when no scale has been set
            Scale = 0.0;
        }

        /// <summary>
        /// Creates a bound setting
        /// </summary>
        /// <param name="control">The control associated with this setting</param>
        /// <param name="name">A unique name for the setting, used internally for debugging</param>
        /// <param name="defaultValue">The default value for the setting. Useful for return to default</param>
        /// <param name="value">The current value</param>
        /// <param name="savedValue">The saved value</param>
        public BindableScaledSetting(NumericUpDown control, string name, float defaultValue, float value, float savedValue)
            : base(control, name, defaultValue, value, savedValue)
        {
            // deliberately don't set scale to 1 - that way 0 can be used when no scale has been set
            Scale = 0.0;
        }

        /// <summary>
        /// Creates a setting with the current and saved values set to the default. Setting is bindable
        /// </summary>
        /// <param name="control">The control associated with this setting</param>
        /// <param name="name">A unique name for the setting, used internally for debugging</param>
        /// <param name="defaultValue">The default value for the setting to initialise. Useful for return to default</param>
        public BindableScaledSetting(string name, float defaultValue)
            : base(name, defaultValue)
        {
            // deliberately don't set scale to 1 - that way 0 can be used when no scale has been set
            Scale = 0.0;
        }

        /// <summary>
        /// Creates a setting with the saved value set to value. Setting is bindable
        /// </summary>
        /// <param name="control">The control associated with this setting</param>
        /// <param name="name">A unique name for the setting, used internally for debugging</param>
        /// <param name="defaultValue">The default value for the setting. Useful for return to default</param>
        /// <param name="value">The current value</param>
        public BindableScaledSetting(string name, float defaultValue, float value)
            : base(name, defaultValue, value)
        {
            // deliberately don't set scale to 1 - that way 0 can be used when no scale has been set
            Scale = 0.0;
        }

        /// <summary>
        /// Creates a setting. Setting is bindable
        /// </summary>
        /// <param name="control">The control associated with this setting</param>
        /// <param name="name">A unique name for the setting, used internally for debugging</param>
        /// <param name="defaultValue">The default value for the setting. Useful for return to default</param>
        /// <param name="value">The current value</param>
        /// <param name="savedValue">The saved value</param>
        public BindableScaledSetting(string name, float defaultValue, float value, float savedValue)
            : base(name, defaultValue, value, savedValue)
        {
            // deliberately don't set scale to 1 - that way 0 can be used when no scale has been set
            Scale = 0.0;
        }

        #endregion

        #region Properties

        private double scale;
        public double Scale
        {
            get { return scale; }
            set
            {
                if (Math.Abs(scale) > 1e-10)
                {
                    Value = (float)(Value / scale * value);
                    SavedValue = (float)(SavedValue / scale * value);
                    DefaultValue = (float)(DefaultValue / scale * value); 
                }
                scale = value;
            }
        }

        private int precision = -1;
        public int Precision
        {
            get { return precision; }
            set
            {
                precision = value;
                if (value > 0)
                {
                    foreach (NumericUpDown control in ControlList)
                    {
                        control.DecimalPlaces = value;
                    }
                }
            }
        }

        private decimal increment = -1;
        public decimal Increment
        {
            get { return increment; }
            set
            {
                increment = value;
                if (value > 0)
                {
                    foreach (NumericUpDown control in ControlList)
                    {
                        control.Increment = value;
                    }
                }
            }
        }

        #endregion

        #region Cloning
        protected override BaseSetting GetNew()
        {
            return new BindableScaledSetting();
        }

        /// <summary>
        /// Returns a copy of the object, bound to the same controls
        /// </summary>
        /// <returns></returns>
        public override BaseSetting DeepCopy()
        {
            BindableScaledSetting clone = (BindableScaledSetting)DeepCopy();
            clone.Precision = Precision;
            clone.Scale = Scale;
            return clone;
        }

        #endregion
    }
}
