﻿using System;
using System.Collections.Generic;
using System.Text;

using System.Windows.Forms;
using System.Xml.Serialization;

namespace DFM.Settings.Bindable
{
    public class BindableEnumSettingRadioButtons<TEnum> : BindableSettingValue<TEnum, RadioButton>
        where TEnum : struct, IComparable, IFormattable, IConvertible
    {
        #region Static Constructor

        static BindableEnumSettingRadioButtons()
        {
            if (!typeof(TEnum).IsEnum)
            {
                throw new ArgumentException("TEnum must be an enumerated type");
            }

            BaseSetting.SettingTypes.Add(typeof(BindableEnumSettingRadioButtons<TEnum>));
        }

        #endregion

        #region Constructors

        protected BindableEnumSettingRadioButtons()
            : base()
        {
            enumMappingList = new List<TEnum>(); 
        }

        /// <summary>
        /// Creates a bound setting with the current and saved values set to the default.
        /// </summary>
        /// <param name="control">The control associated with this setting</param>
        /// <param name="name">A unique name for the setting, used internally for debugging</param>
        /// <param name="defaultValue">The default value for the setting to initialise. Useful for return to default</param>
        public BindableEnumSettingRadioButtons(RadioButton control, string name, TEnum defaultValue)
            : base(name, defaultValue)
        {
            enumMappingList = new List<TEnum>(); 
            Bind(control, defaultValue);
        }

        /// <summary>
        /// Creates a setting. Setting is bindable
        /// </summary>
        /// <param name="control">The control associated with this setting</param>
        /// <param name="name">A unique name for the setting, used internally for debugging</param>
        /// <param name="defaultValue">The default value for the setting. Useful for return to default</param>
        /// <param name="value">The current value</param>
        /// <param name="savedValue">The saved value</param>
        public BindableEnumSettingRadioButtons(string name)
            : base(name, default(TEnum))
        {
            enumMappingList = new List<TEnum>(); 
        }

        #endregion
                       
        #region Extra Members
        
        private List<TEnum> enumMappingList;
        [XmlIgnore()]
        protected List<TEnum> EnumMappingList
        {
            get { return enumMappingList; }
        }

        #endregion
        
        #region BindableSettingValue Members
                
        protected override void init()
        {
            ValueChanged += new EventHandler<ValueChangedEventArgs<TEnum>>(BindableEnumSettingRadioButtons_ValueChanged);
        }

        protected override void doBind(RadioButton control)
        {
            // find last value added to mapping list
            TEnum curValue = DefaultValue;
            if (EnumMappingList.Count > 0) curValue = EnumMappingList[EnumMappingList.Count - 1];

            control.Checked = Value.Equals(curValue);
            control.CheckedChanged += new EventHandler(control_CheckedChanged);
        }

        protected override void doUnbind(RadioButton control)
        {   
            control.CheckedChanged -= new EventHandler(control_CheckedChanged);
        }

        #region Overridden from BindableSettingValue

        /// <summary>
        /// Creates a 2-way binding between the setting and the control
        /// </summary>
        /// <param name="control"></param>
        public virtual void Bind(RadioButton control, TEnum value)
        {
            if (EnumMappingList.Contains(value))
                throw new ArgumentException("value is not unique", "value");

            // why needed here: because this setting relies heavily on order and would behave erratically otherwise
            if (ControlList.Contains(control))
                throw new ArgumentException("control is not unique", "control");

            EnumMappingList.Add(value);
            base.Bind(control);
        }

        /// <summary>
        /// Do not use. Throws NotImplementedException
        /// </summary>
        /// <param name="control"></param>
        public override void Bind(RadioButton control)
        {
            throw new NotImplementedException();
        }

        public override bool TryUnbind(RadioButton control)
        {
            RadioButton other;
            for (int i = 0; i < ControlList.Count; i++)
            {
                other = ControlList[i];
                if (control == other)
                {
                    if (base.TryUnbind(control) && i < EnumMappingList.Count)
                    {
                        EnumMappingList.RemoveAt(i);
                        return true;
                    }
                    return false;
                }
            }
            return false;
        }   

        #endregion

        #endregion

        #region Binding Events

        void control_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton control = sender as RadioButton;
            if (BindingEnabled && control != null && control.Checked)
            {
                for (int i = 0; i < ControlList.Count && i < EnumMappingList.Count; i++)
                {
                    if (control.Equals(ControlList[i]))
                    {
                        Value = EnumMappingList[i];
                        break;
                    }
                }
            }
        }

        void BindableEnumSettingRadioButtons_ValueChanged(object sender, ValueChangedEventArgs<TEnum> e)
        {
            if (!BindingEnabled) return;

            for (int i = 0; i < ControlList.Count && i < EnumMappingList.Count; i++)
            {
                if (e.Value.Equals(EnumMappingList[i]))
                {
                    RadioButton control = ControlList[i];
                    if (control != null) ThreadSafeControlUpdates.UpdateRadioButton(control, true);
                    break;
                }
            }
        }

        #endregion

        #region Cloning
        protected override BaseSetting GetNew()
        {
            return new BindableEnumSettingRadioButtons<TEnum>();
        }

        /// <summary>
        /// Returns a copy of the object, bound to the same controls
        /// </summary>
        /// <returns></returns>
        public override BaseSetting DeepCopy()
        {
            BindableEnumSettingRadioButtons<TEnum> clone = (BindableEnumSettingRadioButtons<TEnum>)base.DeepCopy(true);
            
            for (int i = 0; i < ControlList.Count && i < EnumMappingList.Count; i++)
                clone.Bind(ControlList[i], EnumMappingList[i]);

            return clone;
        }

        #endregion
    }
}
