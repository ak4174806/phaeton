﻿using System;
using System.Collections.Generic;
using System.Text;

using System.Windows.Forms;
using System.Xml.Serialization;

namespace DFM.Settings.Bindable
{
    public class BindableSettingValue<Tdata, Tcontrol> : SettingValue<Tdata>, IBindableSettingValue<Tdata, Tcontrol>
        where Tcontrol : Control
    {
        #region Data Members

        private List<Tcontrol> controlList;
        private bool bindingEnabled;

        #endregion

        #region Constructors
        
        static BindableSettingValue()
        {
            BaseSetting.SettingTypes.Add(typeof(BindableSettingValue<Tdata, Tcontrol>));
        }
        
        // for serialising and cloning
        protected BindableSettingValue()
            : base()
        {
            controlList = new List<Tcontrol>();
            bindingEnabled = true;
        }

        /// <summary>
        /// Creates a bound setting with the current and saved values set to the default.
        /// </summary>
        /// <param name="control">The control associated with this setting</param>
        /// <param name="name">A unique name for the setting, used internally for debugging</param>
        /// <param name="defaultValue">The default value for the setting to initialise. Useful for return to default</param>
        public BindableSettingValue(Tcontrol control, string name, Tdata defaultValue)
            : this(control, name, defaultValue, defaultValue, defaultValue)
        { }

        /// <summary>
        /// Creates a bound setting with the saved value set to value.
        /// </summary>
        /// <param name="control">The control associated with this setting</param>
        /// <param name="name">A unique name for the setting, used internally for debugging</param>
        /// <param name="defaultValue">The default value for the setting. Useful for return to default</param>
        /// <param name="value">The current value</param>
        public BindableSettingValue(Tcontrol control, string name, Tdata defaultValue, Tdata value)
            : this(control, name, defaultValue, value, value)
        { }

        /// <summary>
        /// Creates a bound setting
        /// </summary>
        /// <param name="control">The control associated with this setting</param>
        /// <param name="name">A unique name for the setting, used internally for debugging</param>
        /// <param name="defaultValue">The default value for the setting. Useful for return to default</param>
        /// <param name="value">The current value</param>
        /// <param name="savedValue">The saved value</param>
        public BindableSettingValue(Tcontrol control, string name, Tdata defaultValue, Tdata value, Tdata savedValue)
            : this(name, defaultValue, value, savedValue)
        {
            Bind(control);
        }

        /// <summary>
        /// Creates a setting with the current and saved values set to the default. Setting is bindable
        /// </summary>
        /// <param name="control">The control associated with this setting</param>
        /// <param name="name">A unique name for the setting, used internally for debugging</param>
        /// <param name="defaultValue">The default value for the setting to initialise. Useful for return to default</param>
        public BindableSettingValue(string name, Tdata defaultValue)
            : this(name, defaultValue, defaultValue, defaultValue)
        { }

        /// <summary>
        /// Creates a setting with the saved value set to value. Setting is bindable
        /// </summary>
        /// <param name="control">The control associated with this setting</param>
        /// <param name="name">A unique name for the setting, used internally for debugging</param>
        /// <param name="defaultValue">The default value for the setting. Useful for return to default</param>
        /// <param name="value">The current value</param>
        public BindableSettingValue(string name, Tdata defaultValue, Tdata value)
            : this(name, defaultValue, value, value)
        { }

        /// <summary>
        /// Creates a setting. Setting is bindable
        /// </summary>
        /// <param name="control">The control associated with this setting</param>
        /// <param name="name">A unique name for the setting, used internally for debugging</param>
        /// <param name="defaultValue">The default value for the setting. Useful for return to default</param>
        /// <param name="value">The current value</param>
        /// <param name="savedValue">The saved value</param>
        public BindableSettingValue(string name, Tdata defaultValue, Tdata value, Tdata savedValue)
            : base(name, defaultValue, value, savedValue)
        {
            controlList = new List<Tcontrol>();
            bindingEnabled = true; 
        }

        ~BindableSettingValue()
        {
            if (ControlList == null) return;
            int lastCount = ControlList.Count + 1;
            while (ControlList.Count > 0 && ControlList.Count != lastCount)
            {
                lastCount = ControlList.Count;
                TryUnbind(ControlList[0]);
            }
        }

        #endregion

        #region IBindableSettingValue Members

        private bool inited = false;
        protected virtual void init()
        {
            ValueChanged += new EventHandler<ValueChangedEventArgs<Tdata>>(BindableSettingValue_ValueChanged);
        }

        protected virtual void doBind(Tcontrol control)
        {
            control.Text = Value.ToString();
            control.TextChanged += new EventHandler(control_TextChanged);
        }
        
        protected virtual void doUnbind(Tcontrol control)
        {
            control.TextChanged -= new EventHandler(control_TextChanged);
        }
                
        #region Events

        void control_TextChanged(object sender, EventArgs e)
        {            
            Tcontrol control = sender as Tcontrol;
            if (BindingEnabled && control != null)
            {
                if (Value.ToString() != control.Text)
                    Value = (Tdata)Convert.ChangeType(control.Text, typeof(Tdata));
            }
        }

        void BindableSettingValue_ValueChanged(object sender, ValueChangedEventArgs<Tdata> e)
        {
            foreach (Tcontrol control in ControlList)
            {
                if (BindingEnabled && control != null)
                {
                    ThreadSafeControlUpdates.UpdateText<Tdata>(control, e.Value);
                }
            }
        }

        #endregion

        #region No Need to Replace

        /// <summary>
        /// Creates a 2-way binding between the setting and the control
        /// </summary>
        /// <param name="control"></param>
        public virtual void Bind(Tcontrol control)
        {
            if (!inited)
            {
                init();
                inited = true;
            }
            doBind(control);
            ControlList.Add(control);
        }

        /// <summary>
        /// Removes a binding between control and this. Exception if no binding exists
        /// </summary>
        /// <param name="control"></param>
        public virtual void Unbind(Tcontrol control)
        {
            if (!TryUnbind(control))
                throw new ArgumentException("Not currently bound to specified control", "control");
        }

        /// <summary>
        /// Removes a binding between control and this, if a binding exists
        /// </summary>
        /// <param name="control"></param>
        /// <returns></returns>
        public virtual bool TryUnbind(Tcontrol control)
        {
            Tcontrol other;
            for (int i = 0; i < ControlList.Count; i++)
            {
                other = ControlList[i];
                if (control == other)
                {
                    try
                    {
                        doUnbind(control);
                        ControlList.RemoveAt(i);
                        return true;
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                }
            }
            return false;
        }        
        
        /// <summary>
        /// Whether binding is enabled
        /// </summary>
        [XmlIgnore()]
        public bool BindingEnabled
        {
            get { return bindingEnabled; }
            set { bindingEnabled = value; }
        }

        /// <summary>
        /// The controls this setting is bound to
        /// </summary>
        [XmlIgnore()]
        protected List<Tcontrol> ControlList
        {
            get { return controlList; }
        }

        [XmlIgnore()]
        public override bool IsEnabled
        {
            get { return base.IsEnabled; }
            set
            {
                base.IsEnabled = value;

                // cancelled change
                if (base.IsEnabled != value) return;

                if (ControlList == null) return;

                foreach (Tcontrol control in ControlList)
                {
                    setControlEnabled(control, value);
                }
            }
        }

        #region Cross-thread stuff
        private delegate void SetEnabledCallback(Tcontrol control, bool enabled);

        private void setControlEnabled(Tcontrol control, bool enabled)
        {
            if (control.InvokeRequired)
            {
                control.Invoke(new SetEnabledCallback(setControlEnabled), new object[] { control, enabled });
            }
            else
            {
                control.Enabled = enabled;
            }
        }
        #endregion
        #endregion

        #endregion

        #region Cloning

        /// <summary>
        /// Returns a copy of the object, bound to the same controls
        /// </summary>
        /// <returns></returns>
        public override BaseSetting DeepCopy()
        {
            return DeepCopy(false);
        }

        protected override BaseSetting GetNew()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Used so base classes can override DeepCopy without breaking chain of inheritance
        /// </summary>
        /// <param name="skipBinding"></param>
        /// <returns></returns>
        protected virtual BaseSetting DeepCopy(bool skipBinding)
        {
            BindableSettingValue<Tdata, Tcontrol> clone = (BindableSettingValue<Tdata, Tcontrol>)base.DeepCopy();
            clone.BindingEnabled = BindingEnabled;

            if (!skipBinding)
            {
                foreach (Tcontrol control in ControlList)
                    clone.Bind(control);
            }
            return clone;
        }

        #endregion

        /*
         * Need to implement GetNew, doBind, doUnbind, init, listen to BindingEnabled and copy all constructors. Also handle ValueChangeCancelled
         */
    }
}
