﻿using System;
using System.Collections.Generic;
using System.Text;

using System.Windows.Forms;

namespace DFM.Settings.Bindable
{
    public class BindableEnumSettingComboBox<TEnum> : BindableEnumSetting<TEnum, ComboBox>
        where TEnum : struct, IComparable, IFormattable, IConvertible
    {
        #region Constructors

        static BindableEnumSettingComboBox()
        {
            BaseSetting.SettingTypes.Add(typeof(BindableEnumSettingComboBox<TEnum>));
        }
        
        protected BindableEnumSettingComboBox()
            : base()
        { }

        /// <summary>
        /// Creates a bound setting with the current and saved values set to the default.
        /// </summary>
        /// <param name="control">The control associated with this setting</param>
        /// <param name="name">A unique name for the setting, used internally for debugging</param>
        /// <param name="defaultValue">The default value for the setting to initialise. Useful for return to default</param>
        public BindableEnumSettingComboBox(ComboBox control, string name, TEnum defaultValue)
            : base(control, name, defaultValue)
        { }

        /// <summary>
        /// Creates a bound setting with the saved value set to value.
        /// </summary>
        /// <param name="control">The control associated with this setting</param>
        /// <param name="name">A unique name for the setting, used internally for debugging</param>
        /// <param name="defaultValue">The default value for the setting. Useful for return to default</param>
        /// <param name="value">The current value</param>
        public BindableEnumSettingComboBox(ComboBox control, string name, TEnum defaultValue, TEnum value)
            : base(control, name, defaultValue, value)
        { }

        /// <summary>
        /// Creates a bound setting
        /// </summary>
        /// <param name="control">The control associated with this setting</param>
        /// <param name="name">A unique name for the setting, used internally for debugging</param>
        /// <param name="defaultValue">The default value for the setting. Useful for return to default</param>
        /// <param name="value">The current value</param>
        /// <param name="savedValue">The saved value</param>
        public BindableEnumSettingComboBox(ComboBox control, string name, TEnum defaultValue, TEnum value, TEnum savedValue)
            : base(control, name, defaultValue, value, savedValue)
        { }

        /// <summary>
        /// Creates a setting with the current and saved values set to the default. Setting is bindable
        /// </summary>
        /// <param name="control">The control associated with this setting</param>
        /// <param name="name">A unique name for the setting, used internally for debugging</param>
        /// <param name="defaultValue">The default value for the setting to initialise. Useful for return to default</param>
        public BindableEnumSettingComboBox(string name, TEnum defaultValue)
            : base(name, defaultValue)
        { }

        /// <summary>
        /// Creates a setting with the saved value set to value. Setting is bindable
        /// </summary>
        /// <param name="control">The control associated with this setting</param>
        /// <param name="name">A unique name for the setting, used internally for debugging</param>
        /// <param name="defaultValue">The default value for the setting. Useful for return to default</param>
        /// <param name="value">The current value</param>
        public BindableEnumSettingComboBox(string name, TEnum defaultValue, TEnum value)
            : base(name, defaultValue, value)
        { }

        /// <summary>
        /// Creates a setting. Setting is bindable
        /// </summary>
        /// <param name="control">The control associated with this setting</param>
        /// <param name="name">A unique name for the setting, used internally for debugging</param>
        /// <param name="defaultValue">The default value for the setting. Useful for return to default</param>
        /// <param name="value">The current value</param>
        /// <param name="savedValue">The saved value</param>
        public BindableEnumSettingComboBox(string name, TEnum defaultValue, TEnum value, TEnum savedValue)
            : base(name, defaultValue, value, savedValue)
        { }

        #endregion
                
        #region BindableSettingValue Members
                
        protected override void init()
        {
            ValueChanged += new EventHandler<ValueChangedEventArgs<TEnum>>(BindableEnumSettingComboBox_ValueChanged);
        }

        protected override void doBind(ComboBox control)
        {
            control.Items.Clear();
            foreach (TEnum s in Enum.GetValues(typeof(TEnum)))
            {
                control.Items.Add(EnumToStringHandler(s));
            }
            control.SelectedItem = EnumToStringHandler(Value);

            control.TextChanged += new EventHandler(control_TextChanged);
            control.SelectedIndexChanged += new EventHandler(control_SelectedIndexChanged);
        }

        protected override void doUnbind(ComboBox control)
        {
            control.TextChanged -= new EventHandler(control_TextChanged);
            control.SelectedIndexChanged -= new EventHandler(control_SelectedIndexChanged);
        }

        #endregion

        #region Binding Events

        void control_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox control = sender as ComboBox;
            if (BindingEnabled && control != null)
            {
                if (control.SelectedIndex > -1 && EnumToStringHandler(Value) != control.SelectedItem.ToString())
                    Value = StringToEnumHandler(control.Text);
            }
        }

        void control_TextChanged(object sender, EventArgs e)
        {
            ComboBox control = sender as ComboBox;
            if (BindingEnabled && control != null)
            {
                if (control.SelectedIndex == -1 && IsValidEnumHandler(control.Text))
                    Value = StringToEnumHandler(control.Text);
            }
        }

        void BindableEnumSettingComboBox_ValueChanged(object sender, ValueChangedEventArgs<TEnum> e)
        {
            foreach (ComboBox control in ControlList)
            {
                if (BindingEnabled && control != null)
                {
                    ThreadSafeControlUpdates.UpdateComboBox(control, EnumToStringHandler(e.Value));
                }
            }
        }

        #endregion

        #region Cloning
        protected override BaseSetting GetNew()
        {
            return new BindableEnumSettingComboBox<TEnum>();
        }
        #endregion

    }
}
