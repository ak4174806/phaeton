﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DFM.Settings.Bindable
{
    public interface IBaseBindableSettingValue : ISettingValue
    {
        bool BindingEnabled { get; set; }
    }
}
