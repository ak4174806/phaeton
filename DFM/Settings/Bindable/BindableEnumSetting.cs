﻿using System;
using System.Collections.Generic;
using System.Text;

using System.Windows.Forms;
using System.Xml.Serialization;

namespace DFM.Settings.Bindable
{
    public class BindableEnumSetting<TEnum, TControl> : BindableSettingValue<TEnum, TControl>
        where TEnum : struct, IComparable, IFormattable, IConvertible
        where TControl : Control
    {
        #region Static Constructor

        static BindableEnumSetting()
        {
            if (!typeof(TEnum).IsEnum)
            {
                throw new ArgumentException("TEnum must be an enumerated type");
            }

            BaseSetting.SettingTypes.Add(typeof(BindableEnumSetting<TEnum, TControl>));
        }

        #endregion

        #region Constructors

        protected BindableEnumSetting()
            : base()
        { }

        /// <summary>
        /// Creates a bound setting with the current and saved values set to the default.
        /// </summary>
        /// <param name="control">The control associated with this setting</param>
        /// <param name="name">A unique name for the setting, used internally for debugging</param>
        /// <param name="defaultValue">The default value for the setting to initialise. Useful for return to default</param>
        public BindableEnumSetting(TControl control, string name, TEnum defaultValue)
            : base(control, name, defaultValue)
        { }

        /// <summary>
        /// Creates a bound setting with the saved value set to value.
        /// </summary>
        /// <param name="control">The control associated with this setting</param>
        /// <param name="name">A unique name for the setting, used internally for debugging</param>
        /// <param name="defaultValue">The default value for the setting. Useful for return to default</param>
        /// <param name="value">The current value</param>
        public BindableEnumSetting(TControl control, string name, TEnum defaultValue, TEnum value)
            : base(control, name, defaultValue, value)
        { }

        /// <summary>
        /// Creates a bound setting
        /// </summary>
        /// <param name="control">The control associated with this setting</param>
        /// <param name="name">A unique name for the setting, used internally for debugging</param>
        /// <param name="defaultValue">The default value for the setting. Useful for return to default</param>
        /// <param name="value">The current value</param>
        /// <param name="savedValue">The saved value</param>
        public BindableEnumSetting(TControl control, string name, TEnum defaultValue, TEnum value, TEnum savedValue)
            : base(control, name, defaultValue, value, savedValue)
        { }

        /// <summary>
        /// Creates a setting with the current and saved values set to the default. Setting is bindable
        /// </summary>
        /// <param name="control">The control associated with this setting</param>
        /// <param name="name">A unique name for the setting, used internally for debugging</param>
        /// <param name="defaultValue">The default value for the setting to initialise. Useful for return to default</param>
        public BindableEnumSetting(string name, TEnum defaultValue)
            : base(name, defaultValue)
        { }

        /// <summary>
        /// Creates a setting with the saved value set to value. Setting is bindable
        /// </summary>
        /// <param name="control">The control associated with this setting</param>
        /// <param name="name">A unique name for the setting, used internally for debugging</param>
        /// <param name="defaultValue">The default value for the setting. Useful for return to default</param>
        /// <param name="value">The current value</param>
        public BindableEnumSetting(string name, TEnum defaultValue, TEnum value)
            : base(name, defaultValue, value)
        { }

        /// <summary>
        /// Creates a setting. Setting is bindable
        /// </summary>
        /// <param name="control">The control associated with this setting</param>
        /// <param name="name">A unique name for the setting, used internally for debugging</param>
        /// <param name="defaultValue">The default value for the setting. Useful for return to default</param>
        /// <param name="value">The current value</param>
        /// <param name="savedValue">The saved value</param>
        public BindableEnumSetting(string name, TEnum defaultValue, TEnum value, TEnum savedValue)
            : base(name, defaultValue, value, savedValue)
        { }

        #endregion

        #region Enum <-> String Conversion

        public delegate string EnumToStringDelegate(TEnum value);
        public delegate TEnum StringToEnumDelegate(string value);
        public delegate bool IsValidEnumDelegate(string value);

        private EnumToStringDelegate enumToStringHandler;
        [XmlIgnore()]
        public EnumToStringDelegate EnumToStringHandler
        {
            get
            {
                if (enumToStringHandler == null)
                    return new EnumToStringDelegate(enumToString);
                return enumToStringHandler;
            }
            set { enumToStringHandler = value; }
        }

        private StringToEnumDelegate stringToEnumHandler;
        [XmlIgnore()]
        public StringToEnumDelegate StringToEnumHandler
        {
            get
            {
                if (stringToEnumHandler == null)
                    return new StringToEnumDelegate(stringToEnum);
                return stringToEnumHandler;
            }
            set { stringToEnumHandler = value; }
        }

        private IsValidEnumDelegate isValidEnumHandler;
        [XmlIgnore()]
        public IsValidEnumDelegate IsValidEnumHandler
        {
            get
            {
                if (isValidEnumHandler == null)
                    return new IsValidEnumDelegate(isValidEnum);
                return isValidEnumHandler;
            }
            set { isValidEnumHandler = value; }
        }

        protected virtual string enumToString(TEnum value)
        {
            return value.ToString();
        }

        protected virtual TEnum stringToEnum(string value)
        {
            try
            {
                return (TEnum)Enum.Parse(typeof(TEnum), value, true);
            }
            catch (Exception)
            {
                return DefaultValue;
            }
        }

        protected virtual bool isValidEnum(string value)
        {
            try
            {
                TEnum tmp = (TEnum)Enum.Parse(typeof(TEnum), value, true);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        #endregion

        #region BindableSettingValue Members
                
        protected override void init()
        {
            ValueChanged += new EventHandler<ValueChangedEventArgs<TEnum>>(BindableEnumSetting_ValueChanged);
        }

        protected override void doBind(TControl control)
        {
            control.Text = EnumToStringHandler(Value);
            control.TextChanged += new EventHandler(control_TextChanged);
        }

        protected override void doUnbind(TControl control)
        {
            control.TextChanged -= new EventHandler(control_TextChanged);
        }

        #endregion

        #region Binding Events

        void control_TextChanged(object sender, EventArgs e)
        {
            TControl control = sender as TControl;
            if (BindingEnabled && control != null)
            {
                if (EnumToStringHandler(Value) != control.Text)
                    Value = StringToEnumHandler(control.Text);                
            }
        }

        void BindableEnumSetting_ValueChanged(object sender, ValueChangedEventArgs<TEnum> e)
        {
            foreach (TControl control in ControlList)
            {
                if (BindingEnabled && control != null && control.Text != EnumToStringHandler(e.Value))
                {
                    ThreadSafeControlUpdates.UpdateText<string>(control, EnumToStringHandler(e.Value));
                }
            }
        }

        #endregion

        #region Cloning
        protected override BaseSetting GetNew()
        {
            return new BindableEnumSetting<TEnum, TControl>();
        }
        #endregion
    }
}
