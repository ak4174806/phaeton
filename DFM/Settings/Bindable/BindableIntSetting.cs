﻿using System;
using System.Collections.Generic;
using System.Text;

using System.Windows.Forms;

namespace DFM.Settings.Bindable
{
    public class BindableIntSetting : BindableSettingValue<int, NumericUpDown>
    {
        #region Constructors
        static BindableIntSetting()
        {
            BaseSetting.SettingTypes.Add(typeof(BindableIntSetting));
        }
        
        protected BindableIntSetting()
            : base()
        { }

        /// <summary>
        /// Creates a bound setting with the current and saved values set to the default.
        /// </summary>
        /// <param name="control">The control associated with this setting</param>
        /// <param name="name">A unique name for the setting, used internally for debugging</param>
        /// <param name="defaultValue">The default value for the setting to initialise. Useful for return to default</param>
        public BindableIntSetting(NumericUpDown control, string name, int defaultValue)
            : base(control, name, defaultValue)
        { }

        /// <summary>
        /// Creates a bound setting with the saved value set to value.
        /// </summary>
        /// <param name="control">The control associated with this setting</param>
        /// <param name="name">A unique name for the setting, used internally for debugging</param>
        /// <param name="defaultValue">The default value for the setting. Useful for return to default</param>
        /// <param name="value">The current value</param>
        public BindableIntSetting(NumericUpDown control, string name, int defaultValue, int value)
            : base(control, name, defaultValue, value)
        { }

        /// <summary>
        /// Creates a bound setting
        /// </summary>
        /// <param name="control">The control associated with this setting</param>
        /// <param name="name">A unique name for the setting, used internally for debugging</param>
        /// <param name="defaultValue">The default value for the setting. Useful for return to default</param>
        /// <param name="value">The current value</param>
        /// <param name="savedValue">The saved value</param>
        public BindableIntSetting(NumericUpDown control, string name, int defaultValue, int value, int savedValue)
            : base(control, name, defaultValue, value, savedValue)
        { }

        /// <summary>
        /// Creates a setting with the current and saved values set to the default. Setting is bindable
        /// </summary>
        /// <param name="control">The control associated with this setting</param>
        /// <param name="name">A unique name for the setting, used internally for debugging</param>
        /// <param name="defaultValue">The default value for the setting to initialise. Useful for return to default</param>
        public BindableIntSetting(string name, int defaultValue)
            : base(name, defaultValue)
        { }

        /// <summary>
        /// Creates a setting with the saved value set to value. Setting is bindable
        /// </summary>
        /// <param name="control">The control associated with this setting</param>
        /// <param name="name">A unique name for the setting, used internally for debugging</param>
        /// <param name="defaultValue">The default value for the setting. Useful for return to default</param>
        /// <param name="value">The current value</param>
        public BindableIntSetting(string name, int defaultValue, int value)
            : base(name, defaultValue, value)
        { }

        /// <summary>
        /// Creates a setting. Setting is bindable
        /// </summary>
        /// <param name="control">The control associated with this setting</param>
        /// <param name="name">A unique name for the setting, used internally for debugging</param>
        /// <param name="defaultValue">The default value for the setting. Useful for return to default</param>
        /// <param name="value">The current value</param>
        /// <param name="savedValue">The saved value</param>
        public BindableIntSetting(string name, int defaultValue, int value, int savedValue)
            : base(name, defaultValue, value, savedValue)
        { }

        #endregion

        #region BindableSettingValue Members

        protected override void init()
        {
            ValueChanged += new EventHandler<ValueChangedEventArgs<int>>(BindableIntSetting_ValueChanged);
        }

        protected override void doBind(NumericUpDown control)
        {
            control.Value = Convert.ToDecimal(Value);
            control.ValueChanged += new EventHandler(control_ValueChanged);
        }

        protected override void doUnbind(NumericUpDown control)
        {
            control.ValueChanged -= new EventHandler(control_ValueChanged);
        }

        #endregion

        #region Binding Events

        void control_ValueChanged(object sender, EventArgs e)
        {
            NumericUpDown control = sender as NumericUpDown;
            if (BindingEnabled && control != null)
            {
                if (Value != Convert.ToInt32(control.Value))
                    Value = Convert.ToInt32(control.Value);
            }
        }

        void BindableIntSetting_ValueChanged(object sender, ValueChangedEventArgs<int> e)
        {
            foreach (NumericUpDown control in ControlList)
            {
                if (BindingEnabled && control != null)
                {
                    ThreadSafeControlUpdates.UpdateValue<decimal>(control, Math.Min(control.Maximum, Math.Max(Convert.ToDecimal(e.Value), control.Minimum)));
                }
            }
        }

        #endregion

        #region Cloning
        protected override BaseSetting GetNew()
        {
            return new BindableIntSetting();
        }
        #endregion
    }
}
