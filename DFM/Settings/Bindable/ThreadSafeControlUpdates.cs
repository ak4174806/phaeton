﻿using System;
using System.Collections.Generic;
using System.Text;

using System.Windows.Forms;

namespace DFM.Settings.Bindable
{
    public class ThreadSafeControlUpdates
    {
        public delegate void ControlEventHandlerCallback<Tcontrol, Tdata>(Tcontrol sender, Tdata value);
        
        public static void UpdateText<Tdata>(Control control, Tdata value)
        {
            if (control.InvokeRequired)
            {
                control.Invoke(new ControlEventHandlerCallback<Control, Tdata>(UpdateText<Tdata>), new object[] { control, value });
            }
            else
            {
                if (control.Text != value.ToString())
                    control.Text = value.ToString();
            }
        }

        public static void UpdateCheckBox(CheckBox control, bool value)
        {
            if (control.InvokeRequired)
            {
                control.Invoke(new ControlEventHandlerCallback<CheckBox, bool>(UpdateCheckBox), new object[] { control, value });
            }
            else
            {
                if (control.Checked != value)
                    control.Checked = value;
            }
        }

        public static void UpdateRadioButton(RadioButton control, bool value)
        {
            if (control.InvokeRequired)
            {
                control.Invoke(new ControlEventHandlerCallback<RadioButton, bool>(UpdateRadioButton), new object[] { control, value });
            }
            else
            {
                if (control.Checked != value)
                    control.Checked = value;
            }
        }

        public static void UpdateValue<Tdata>(NumericUpDown control, Tdata value)
        {
            if (control.InvokeRequired)
            {
                control.Invoke(new ControlEventHandlerCallback<NumericUpDown, Tdata>(UpdateValue<Tdata>), new object[] { control, value });
            }
            else
            {
                if (Math.Abs(Convert.ToSingle(value) - Convert.ToSingle(control.Value)) > Single.Epsilon)
                    control.Value = Convert.ToDecimal(value);
            }
        }

        public static void UpdateComboBox(ComboBox control, string value)
        {
            if (control.InvokeRequired)
            {
                control.Invoke(new ControlEventHandlerCallback<ComboBox, string>(UpdateComboBox), new object[] { control, value });
            }
            else
            {
                if (control.SelectedIndex > -1 && control.SelectedItem.ToString() != value)
                    control.SelectedItem = value;
                else if (control.SelectedIndex == -1 && control.Text != value)
                    control.SelectedItem = value;
            }
        }

        public static void UpdateEnabled(Control control, bool enabled)
        {
            if (control.InvokeRequired)
            {
                control.Invoke(new ControlEventHandlerCallback<Control, bool>(UpdateEnabled), new object[] { control, enabled });
            }
            else
            {
                control.Enabled = enabled;
            }
        }

        public static void UpdateVisible(Control control, bool visible)
        {
            if (control.InvokeRequired)
            {
                control.Invoke(new ControlEventHandlerCallback<Control, bool>(UpdateVisible), new object[] { control, visible });
            }
            else
            {
                control.Visible = visible;
            }
        }
    }
}
