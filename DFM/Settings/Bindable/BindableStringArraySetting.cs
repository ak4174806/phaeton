﻿using System;
using System.Collections.Generic;
using System.Text;

using System.Windows.Forms;
using System.Xml.Serialization;
using NLog;

namespace DFM.Settings.Bindable
{
    public class BindableStringArraySetting : BindableSettingValue<string, ComboBox>
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        #region Data Members

        private string[] values;
        private bool allowOtherValues;
        private ValidateHandler validateHandler;

        #endregion

        #region Extra Stuff

        [XmlIgnore()]
        public string[] Values
        {
            get
            {
                if (values == null) values = new string[0];
                return values;
            }
            set
            {
                values = value;

                if (!Validate(DefaultValue)) DefaultValue = values[0];
                if (!Validate(Value)) Value = values[0];
                if (!Validate(SavedValue)) SavedValue = values[0];

                foreach (ComboBox control in ControlList)
                    updateItems(control);                
            }
        }

        [XmlIgnore()]
        public bool AllowOtherValues
        {
            get { return allowOtherValues; }
            set { allowOtherValues = value; }
        }

        public delegate bool ValidateHandler(string value);

        [XmlIgnore()]
        public ValidateHandler Validate
        {
            get
            {
                if (AllowOtherValues)
                    return new ValidateHandler(delegate(string value) { return true; });
                if (validateHandler == null) return new ValidateHandler(doValidate);
                return validateHandler;
            }
            set
            {
                validateHandler = value;
            }
        }

        protected bool doValidate(string value)
        {
            if (Values.Length == 0) return true;
            foreach (string v in Values)
            {
                if (v == value) return true;
            }
            return false;
        }

        #region Overrides

        [XmlIgnore()]
        public override string Value
        {
            get
            {
                if (!Validate(base.Value))
                {
                    base.Value = DefaultValue;
                    return DefaultValue;
                }
                return base.Value;
            }
            set
            {
                if (Validate(value))
                    base.Value = value;
            }
        }

        [XmlElement("SavedValue")]
        public override string SavedValue
        {
            get
            {
                if (!Validate(base.SavedValue))
                {
                    base.SavedValue = DefaultValue;
                    return DefaultValue;
                }
                return base.SavedValue;
            }
            set
            {
                if (Validate(value))
                    base.SavedValue = value;
            }
        }

        [XmlIgnore()]
        public override string DefaultValue
        {
            get
            {
                if (!Validate(base.DefaultValue) && Values.Length > 0)
                {
                    base.DefaultValue = Values[0];
                    return Values[0];
                }
                return base.DefaultValue;
            }
            protected set
            {
                if (Validate(value))
                    base.DefaultValue = value;
                else if (Values.Length > 0)
                    base.DefaultValue = Values[0];
            }
        }

        #endregion

        #endregion

        #region Constructors

        static BindableStringArraySetting()
        {
            BaseSetting.SettingTypes.Add(typeof(BindableStringArraySetting));
        }

        protected BindableStringArraySetting()
            : base()
        {
            Values = new string[0];
        }

        public BindableStringArraySetting(ComboBox control, string name, string[] values)
            : base(control, name, null)
        {
            Values = values;
            if (values.Length > 0)
                DefaultValue = Value = SavedValue = values[0];
        }

        public BindableStringArraySetting(ComboBox control, string name, string[] values, string defaultValue)
            : base(control, name, defaultValue)
        {
            Values = values;
        }


        public BindableStringArraySetting(ComboBox control, string name, string[] values, string defaultValue, string value)
            : base(control, name, defaultValue, value)
        {
            Values = values;
        }

        public BindableStringArraySetting(ComboBox control, string name, string[] values, string defaultValue, string value, string savedValue)
            : base(control, name, defaultValue, value, savedValue)
        {
            Values = values;
        }

        public BindableStringArraySetting(string name, string[] values)
            : base(name, null)
        {
            Values = values;
            if (values.Length > 0)
                DefaultValue = Value = SavedValue = values[0];
        }
        
        public BindableStringArraySetting(string name, string[] values, string defaultValue)
            : base(name, defaultValue)
        {
            Values = values;
        }

        public BindableStringArraySetting(string name, string[] values, string defaultValue, string value)
            : base(name, defaultValue, value)
        {
            Values = values;
        }

        public BindableStringArraySetting(string name, string[] values, string defaultValue, string value, string savedValue)
            : base(name, defaultValue, value, savedValue)
        {
            Values = values;
        }

        #endregion

        #region BindableSettingValue Members

        protected override void init()
        {
            ValueChanged += new EventHandler<ValueChangedEventArgs<string>>(BindableStringArraySetting_ValueChanged);
        }

        protected virtual void updateItems(ComboBox control)
        {
            control.Items.Clear();
            control.Items.AddRange(Values);
            if (control.Items.Count > 0)
            {
                if (Validate(Value))
                    control.SelectedItem = Value;
                else
                {
                    Value = DefaultValue;
                    control.SelectedItem = Value;
                }
            }
        }

        protected override void doBind(ComboBox control)
        {
            if ((control.DropDownStyle != ComboBoxStyle.DropDownList && !AllowOtherValues))
            {
                logger.Error("Control passed to BindableStringArraySetting was not a dropdownlist when disallowing other values");
            }                    

            updateItems(control);
            control.SelectedIndexChanged += new EventHandler(control_SelectedIndexChanged);
            control.TextChanged += new EventHandler(control_TextChanged);
        }

        protected override void doUnbind(ComboBox control)
        {
            control.SelectedIndexChanged -= new EventHandler(control_SelectedIndexChanged);
            control.TextChanged -= new EventHandler(control_TextChanged);
        }

        #endregion

        #region Binding Events

        void control_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox control = sender as ComboBox;
            if (BindingEnabled && control != null )
            {
                if (control.SelectedIndex > -1 && Value != control.SelectedItem.ToString() && Validate(control.SelectedItem.ToString()))
                    Value = control.SelectedItem.ToString();
            }
        }

        void control_TextChanged(object sender, EventArgs e)
        {
            ComboBox control = sender as ComboBox;
            if (BindingEnabled && control != null )
            {
                if (control.SelectedIndex == -1 && Value != control.Text && Validate(control.Text))
                    Value = control.Text;
            }
        }

        void BindableStringArraySetting_ValueChanged(object sender, ValueChangedEventArgs<string> e)
        {
            if (!BindingEnabled) return;

            foreach (ComboBox control in ControlList)
            {
                if (control != null)
                {
                    ThreadSafeControlUpdates.UpdateComboBox(control, e.Value);
                }
            }
        }

        #endregion

        #region Cloning
        protected override BaseSetting GetNew()
        {
            return new BindableStringArraySetting();
        }

        public override BaseSetting DeepCopy()
        {
            BindableStringArraySetting clone = (BindableStringArraySetting)base.DeepCopy();
            clone.Values = Values;
            clone.Validate = Validate;
            clone.AllowOtherValues = AllowOtherValues;
            return clone;
        }
        #endregion
    }
}
