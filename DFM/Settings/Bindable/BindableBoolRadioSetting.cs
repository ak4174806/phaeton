﻿using System;
using System.Collections.Generic;
using System.Text;

using System.Windows.Forms;

namespace DFM.Settings.Bindable
{
    public class BindableBoolRadioSetting : BindableSettingValue<bool, RadioButton>
    {
        #region Constructors
        static BindableBoolRadioSetting()
        {
            BaseSetting.SettingTypes.Add(typeof(BindableBoolRadioSetting));
        }

        protected BindableBoolRadioSetting()
            : base()
        { }

        /// <summary>
        /// Creates a bound setting with the current and saved values set to the default.
        /// </summary>
        /// <param name="control">The control associated with this setting</param>
        /// <param name="name">A unique name for the setting, used internally for debugging</param>
        /// <param name="defaultValue">The default value for the setting to initialise. Useful for return to default</param>
        public BindableBoolRadioSetting(RadioButton control, string name, bool defaultValue)
            : base(control, name, defaultValue)
        { }

        /// <summary>
        /// Creates a bound setting with the saved value set to value.
        /// </summary>
        /// <param name="control">The control associated with this setting</param>
        /// <param name="name">A unique name for the setting, used internally for debugging</param>
        /// <param name="defaultValue">The default value for the setting. Useful for return to default</param>
        /// <param name="value">The current value</param>
        public BindableBoolRadioSetting(RadioButton control, string name, bool defaultValue, bool value)
            : base(control, name, defaultValue, value)
        { }

        /// <summary>
        /// Creates a bound setting
        /// </summary>
        /// <param name="control">The control associated with this setting</param>
        /// <param name="name">A unique name for the setting, used internally for debugging</param>
        /// <param name="defaultValue">The default value for the setting. Useful for return to default</param>
        /// <param name="value">The current value</param>
        /// <param name="savedValue">The saved value</param>
        public BindableBoolRadioSetting(RadioButton control, string name, bool defaultValue, bool value, bool savedValue)
            : base(control, name, defaultValue, value, savedValue)
        { }

        /// <summary>
        /// Creates a setting with the current and saved values set to the default. Setting is bindable
        /// </summary>
        /// <param name="control">The control associated with this setting</param>
        /// <param name="name">A unique name for the setting, used internally for debugging</param>
        /// <param name="defaultValue">The default value for the setting to initialise. Useful for return to default</param>
        public BindableBoolRadioSetting(string name, bool defaultValue)
            : base(name, defaultValue)
        { }

        /// <summary>
        /// Creates a setting with the saved value set to value. Setting is bindable
        /// </summary>
        /// <param name="control">The control associated with this setting</param>
        /// <param name="name">A unique name for the setting, used internally for debugging</param>
        /// <param name="defaultValue">The default value for the setting. Useful for return to default</param>
        /// <param name="value">The current value</param>
        public BindableBoolRadioSetting(string name, bool defaultValue, bool value)
            : base(name, defaultValue, value)
        { }

        /// <summary>
        /// Creates a setting. Setting is bindable
        /// </summary>
        /// <param name="control">The control associated with this setting</param>
        /// <param name="name">A unique name for the setting, used internally for debugging</param>
        /// <param name="defaultValue">The default value for the setting. Useful for return to default</param>
        /// <param name="value">The current value</param>
        /// <param name="savedValue">The saved value</param>
        public BindableBoolRadioSetting(string name, bool defaultValue, bool value, bool savedValue)
            : base(name, defaultValue, value, savedValue)
        { }

        #endregion

        #region BindableSettingValue Members

        protected override void init()
        {
            ValueChanged += new EventHandler<ValueChangedEventArgs<bool>>(BindableBoolRadioSetting_ValueChanged);
        }

        protected override void doBind(RadioButton control)
        {
            control.Checked = Value;
            control.CheckedChanged += new EventHandler(control_CheckedChanged);
        }

        protected override void doUnbind(RadioButton control)
        {
            control.CheckedChanged -= new EventHandler(control_CheckedChanged);
        }

        #endregion

        #region Binding Events

        void control_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton control = sender as RadioButton;
            if (BindingEnabled && control != null)
            {
                if (Value != control.Checked)
                    Value = control.Checked;
            }
        }

        void BindableBoolRadioSetting_ValueChanged(object sender, ValueChangedEventArgs<bool> e)
        {
            foreach (RadioButton control in ControlList)
            {
                if (BindingEnabled && control != null)
                {
                    ThreadSafeControlUpdates.UpdateRadioButton(control, e.Value);
                }
            }
        }

        #endregion

        #region Cloning
        protected override BaseSetting GetNew()
        {
            return new BindableBoolRadioSetting();
        }
        #endregion
    }
}
