﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DFM.Settings
{
    public interface ISettingValue<T> : ISettingValue
    {
        T Value { get; set; }
        T SavedValue { get; set; }
        T DefaultValue { get; }

        event EventHandler<ValueChangingEventArgs<T>> ValueChanging;
        event EventHandler<ValueChangedEventArgs<T>> ValueChanged;

        void AddValidator(SettingValidator<T> validator);
        event EventHandler<ValidationErrorEventArgs<T>> ValidationError;        
        ValidationResult<T> LastValidationError { get; }
        bool ValidateOnValueChangeOnly { get; set; }
    }

    public interface ISettingValue : ISetting
    {
        ValidationResult LastValidationErrorMessage { get; }
    }
    
    public delegate void SettingValidator<T>(ISettingValue<T> setting, ValidationResult<T> result);
}
