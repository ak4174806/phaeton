﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DFM.Settings
{
    public class ValueChangingEventArgs<T> : EventArgs
    {
        private T value;
        private ISettingValue<T> setting;
        private bool cancel;
        
        /// <summary>
        /// The new value to which the setting is changing
        /// </summary>
        public T Value
        {
            get { return value; }
            set { this.value = value; }
        }

        /// <summary>
        /// The setting object (contains old value)
        /// </summary>
        public ISettingValue<T> Setting
        {
            get { return setting; }
            set { setting = value; }
        }

        /// <summary>
        /// Whether to prevent the change
        /// </summary>
        public bool Cancel
        {
            get { return cancel; }
            set { cancel = value; }
        }

        public ValueChangingEventArgs(T value, ISettingValue<T> setting)
        {
            Value = value;
            Setting = setting;
            Cancel = false;
        }
    }
}
