﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DFM.Settings
{
    public class ValidationErrorEventArgs<T> : EventArgs
    {
        private ValidationResult<T> result;
        private ISettingValue<T> setting;

        /// <summary>
        /// The validation error details
        /// </summary>
        public ValidationResult<T> Result
        {
            get { return result; }
            set { result = value; }
        }

        /// <summary>
        /// The setting object which threw the error
        /// </summary>
        public ISettingValue<T> Setting
        {
            get { return setting; }
            set { setting = value; }
        }

        public ValidationErrorEventArgs(ISettingValue<T> setting, ValidationResult<T> result)
        {
            Result = result;
            Setting = setting;
        }
    }
}
