﻿namespace DFM.Settings.Forms
{
    partial class Base_Settings_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.controlButtonsContainer = new System.Windows.Forms.SplitContainer();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.lblHeading = new System.Windows.Forms.Label();
            this.outerContainer = new System.Windows.Forms.SplitContainer();
            this.controlButtonsContainer.Panel2.SuspendLayout();
            this.controlButtonsContainer.SuspendLayout();
            this.outerContainer.Panel1.SuspendLayout();
            this.outerContainer.Panel2.SuspendLayout();
            this.outerContainer.SuspendLayout();
            this.SuspendLayout();
            // 
            // controlButtonsContainer
            // 
            this.controlButtonsContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.controlButtonsContainer.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.controlButtonsContainer.Location = new System.Drawing.Point(0, 0);
            this.controlButtonsContainer.MinimumSize = new System.Drawing.Size(212, 203);
            this.controlButtonsContainer.Name = "controlButtonsContainer";
            this.controlButtonsContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.controlButtonsContainer.Panel1MinSize = 0;
            // 
            // controlButtonsContainer.Panel2
            // 
            this.controlButtonsContainer.Panel2.Controls.Add(this.btnSave);
            this.controlButtonsContainer.Panel2.Controls.Add(this.btnOK);
            this.controlButtonsContainer.Panel2.Controls.Add(this.btnCancel);
            this.controlButtonsContainer.Panel2MinSize = 0;
            this.controlButtonsContainer.Size = new System.Drawing.Size(286, 271);
            this.controlButtonsContainer.SplitterDistance = 224;
            this.controlButtonsContainer.TabIndex = 2;
            this.controlButtonsContainer.TabStop = false;
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSave.Location = new System.Drawing.Point(12, 7);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(57, 27);
            this.btnSave.TabIndex = 23;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOK.Location = new System.Drawing.Point(154, 7);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(57, 27);
            this.btnOK.TabIndex = 24;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(217, 7);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(57, 27);
            this.btnCancel.TabIndex = 25;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // lblHeading
            // 
            this.lblHeading.AutoSize = true;
            this.lblHeading.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHeading.Location = new System.Drawing.Point(12, 9);
            this.lblHeading.Name = "lblHeading";
            this.lblHeading.Size = new System.Drawing.Size(142, 25);
            this.lblHeading.TabIndex = 1;
            this.lblHeading.Text = "DFM Settings";
            // 
            // outerContainer
            // 
            this.outerContainer.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.outerContainer.IsSplitterFixed = true;
            this.outerContainer.Location = new System.Drawing.Point(0, 0);
            this.outerContainer.MinimumSize = new System.Drawing.Size(211, 248);
            this.outerContainer.Name = "outerContainer";
            this.outerContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // outerContainer.Panel1
            // 
            this.outerContainer.Panel1.Controls.Add(this.lblHeading);
            // 
            // outerContainer.Panel2
            // 
            this.outerContainer.Panel2.Controls.Add(this.controlButtonsContainer);
            this.outerContainer.Size = new System.Drawing.Size(286, 316);
            this.outerContainer.SplitterDistance = 41;
            this.outerContainer.TabIndex = 0;
            this.outerContainer.TabStop = false;
            // 
            // Base_Settings_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(519, 463);
            this.Controls.Add(this.outerContainer);
            this.Name = "Base_Settings_Form";
            this.Text = "DFM Settings";
            this.controlButtonsContainer.Panel2.ResumeLayout(false);
            this.controlButtonsContainer.ResumeLayout(false);
            this.outerContainer.Panel1.ResumeLayout(false);
            this.outerContainer.Panel1.PerformLayout();
            this.outerContainer.Panel2.ResumeLayout(false);
            this.outerContainer.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        protected System.Windows.Forms.SplitContainer controlButtonsContainer;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label lblHeading;
        protected System.Windows.Forms.SplitContainer outerContainer;


    }
}