﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace DFM.Settings.Forms
{
    public partial class Base_Settings_Form : Form
    {
        public Base_Settings_Form()
        {
            InitializeComponent();

            FormTitle = "Settings";
            btnSave.Click += new EventHandler(btnSave_Click);
            btnCancel.Click += new EventHandler(btnCancel_Click);
            btnOK.Click += new EventHandler(btnOK_Click);
        }

        void btnOK_Click(object sender, EventArgs e)
        {
            OK();
        }

        void btnCancel_Click(object sender, EventArgs e)
        {
            Cancel();
        }

        void btnSave_Click(object sender, EventArgs e)
        {
            Save();
        }

        [Description("The title to display for the form"),
        Category("Appearance"),
        DefaultValue("Settings"),
        Browsable(true)]
        public string FormTitle
        {
            get { return lblHeading.Text; }
            set
            {
                lblHeading.Text = Text = value;
            }
        }

        protected virtual void OK()
        { }

        protected virtual void Save()
        { }

        protected virtual void Cancel()
        { }
    }
}
