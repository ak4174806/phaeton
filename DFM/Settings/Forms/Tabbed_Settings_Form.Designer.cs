﻿namespace DFM.Settings.Forms
{
    partial class Tabbed_Settings_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabSettings = new System.Windows.Forms.TabControl();
            this.controlButtonsContainer.Panel1.SuspendLayout();
            this.controlButtonsContainer.SuspendLayout();
            this.outerContainer.Panel2.SuspendLayout();
            this.outerContainer.SuspendLayout();
            this.SuspendLayout();
            // 
            // controlButtonsContainer
            // 
            // 
            // controlButtonsContainer.Panel1
            // 
            this.controlButtonsContainer.Panel1.Controls.Add(this.tabSettings);
            this.controlButtonsContainer.Panel1.Padding = new System.Windows.Forms.Padding(15, 10, 15, 10);
            // 
            // outerContainer
            // 
            // 
            // tabSettings
            // 
            this.tabSettings.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabSettings.Location = new System.Drawing.Point(15, 10);
            this.tabSettings.Name = "tabSettings";
            this.tabSettings.SelectedIndex = 0;
            this.tabSettings.Size = new System.Drawing.Size(256, 204);
            this.tabSettings.TabIndex = 2;
            // 
            // Tabbed_Settings_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(289, 319);
            this.FormTitle = "Tabbed Settings Form";
            this.Name = "Tabbed_Settings_Form";
            this.Text = "Tabbed Settings Form";
            this.controlButtonsContainer.Panel1.ResumeLayout(false);
            this.controlButtonsContainer.ResumeLayout(false);
            this.outerContainer.Panel2.ResumeLayout(false);
            this.outerContainer.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        protected System.Windows.Forms.TabControl tabSettings;



    }
}