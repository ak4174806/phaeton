﻿using System;
using System.Collections.Generic;
using System.Text;

using System.Xml.Serialization;

namespace DFM.Settings
{
    public class SettingValue<T> : BaseSetting, ISettingValue<T>
    {
        #region Data Members
        
        private T value;
        private T savedValue;
        private T defaultValue;
        
        private bool isValid;
        private List<SettingValidator<T>> validators;
        private ValidationResult<T> lastValidationError;
        private bool validateOnValueChangeOnly = false;
        
        #endregion

        #region Constructors
        static SettingValue()
        {
            BaseSetting.SettingTypes.Add(typeof(SettingValue<T>));
        }

        protected SettingValue()
            : base()
        {
            isValid = false;

            ValidationResult<T> tmp = new ValidationResult<T>(default(T));
            tmp.IsError = true;
            tmp.ErrorMessage = "Value not set";
            OnValidationError(new ValidationErrorEventArgs<T>(this, tmp));

            validators = new List<SettingValidator<T>>();
            ValueChanging += new EventHandler<ValueChangingEventArgs<T>>(SettingValue_ValidateValue);
        }

        /// <summary>
        /// Creates a setting with the current and saved values set to the default.
        /// </summary>
        /// <param name="name">A unique name for the setting, used internally for debugging</param>
        /// <param name="defaultValue">The default value for the setting to initialise. Useful for return to default</param>
        public SettingValue(string name, T defaultValue)
            : this(name, defaultValue, defaultValue, defaultValue)
        { }

        /// <summary>
        /// Creates a setting with the saved value set to value.
        /// </summary>
        /// <param name="name">A unique name for the setting, used internally for debugging</param>
        /// <param name="defaultValue">The default value for the setting. Useful for return to default</param>
        /// <param name="value">The current value</param>
        public SettingValue(string name, T defaultValue, T value)
            : this(name, defaultValue, value, value)
        { }

        /// <summary>
        /// Creates a setting
        /// </summary>
        /// <param name="name">A unique name for the setting, used internally for debugging</param>
        /// <param name="defaultValue">The default value for the setting. Useful for return to default</param>
        /// <param name="value">The current value</param>
        /// <param name="savedValue">The saved value</param>
        public SettingValue(string name, T defaultValue, T value, T savedValue)
            : base(name)
        {
            isValid = true;
            DefaultValue = defaultValue;
            Value = value;
            SavedValue = savedValue;

            validators = new List<SettingValidator<T>>();
            ValueChanging += new EventHandler<ValueChangingEventArgs<T>>(SettingValue_ValidateValue);
        }

        #endregion

        #region ISettingValue Members

        #region Data
        
        /// <summary>
        /// The current value of the setting
        /// </summary>
        [XmlIgnore()]
        public virtual T Value
        {
            get { return value; }
            set
            {
                if (value != null && !value.Equals(this.value))
                {
                    // reset validation errors
                    isValid = true;
                    LastValidationError = null;

                    ValueChangingEventArgs<T> e = new ValueChangingEventArgs<T>(value, this);
                    OnValueChanging(e);                    
                    if (e.Cancel)
                        return;

                    // use updated value from ValueChanging event
                    this.value = e.Value;
                    OnValueChanged(new ValueChangedEventArgs<T>(e.Value, this));

                    if ((this.value.Equals(SavedValue)) ^ IsModified)
                    {
                        OnModifiedChanged(new EventArgs());
                    }
                }
            }
        }

        /// <summary>
        /// The saved value of the setting
        /// </summary>
        [XmlElement("SavedValue")]
        public virtual T SavedValue
        {
            get { return savedValue; }
            set { savedValue = value; }
        }

        /// <summary>
        /// The default value for this setting
        /// </summary>
        [XmlIgnore()]
        public virtual T DefaultValue
        {
            get { return defaultValue; }
            protected set { defaultValue = value; }
        }

        /// <summary>
        /// Whether Value and SavedValue differ
        /// </summary>
        [XmlIgnore()]
        public override bool IsModified
        {
            get { return (Value != null && SavedValue != null && !Value.Equals(SavedValue)); }
        }

        #endregion

        #region Value Changing Events

        /// <summary>
        /// Raised before Value changes. Allows users to intercept any changes
        /// </summary>
        public event EventHandler<ValueChangingEventArgs<T>> ValueChanging;

        /// <summary>
        /// Raised after Value changes
        /// </summary>
        public event EventHandler<ValueChangedEventArgs<T>> ValueChanged;

        protected virtual void OnValueChanging(ValueChangingEventArgs<T> e)
        {
            if (ValueChanging != null)
            {
                ValueChanging(this, e);
            }
        }

        protected virtual void OnValueChanged(ValueChangedEventArgs<T> e)
        {
            if (ValueChanged != null)
            {
                ValueChanged(this, e);
            }
        }

        #endregion

        #region Validation

        /// <summary>
        /// Adds a validator
        /// </summary>
        /// <param name="validator"></param>
        public void AddValidator(SettingValidator<T> validator)
        {
            validators.Add(validator);
        }

        /// <summary>
        /// Raised when an error occurs in validating Value
        /// </summary>
        public event EventHandler<ValidationErrorEventArgs<T>> ValidationError;

        protected void OnValidationError(ValidationErrorEventArgs<T> e)
        {
            isValid = false;
            LastValidationError = e.Result;
            
            if (ValidationError != null)
            {
                ValidationError(this, e);
            }
        }
        
        public void OnValidationError(string errorMessage)
        {
            ValidationResult<T> result = new ValidationResult<T>(Value);
            result.IsError = true;
            result.ErrorMessage = errorMessage;
            OnValidationError(new ValidationErrorEventArgs<T>(this, result));
        }

        public void ClearValidationError()
        {
            isValid = true;
            LastValidationError = null;
        }

        private void SettingValue_ValidateValue(object sender, ValueChangingEventArgs<T> e)
        {
            if (!IsValidationEnabled) return;

            ValidationResult<T> result = new ValidationResult<T>(e.Value);
            foreach (SettingValidator<T> validator in validators)
            {
                validator(this, result);
            }

            e.Value = result.Value;

            if (result.IsError)
            {
                //e.Cancel = true;
                OnValidationError(new ValidationErrorEventArgs<T>(this, result));
            }
        }              

        /// <summary>
        /// Whether the control is currently valid
        /// </summary>
        [XmlIgnore()]
        public override bool IsValid
        {
            get
            {
                if (!IsValidationEnabled) return true;
                if (ValidateOnValueChangeOnly) return isValid;
                else 
                {
                    ValidationResult<T> result = new ValidationResult<T>(Value);
                    foreach (SettingValidator<T> validator in validators)
                    {
                        validator(this, result);
                    }

                    if (result.IsError)
                        OnValidationError(new ValidationErrorEventArgs<T>(this, result));

                    // Value = result.Value;
                    return !result.IsError;
                }
            }
        }
        
        /// <summary>
        /// Whether the control should validate only when the value changes, or every time IsValid is accessed
        /// </summary>
        [XmlIgnore()]
        public virtual bool ValidateOnValueChangeOnly
        {
            get { return validateOnValueChangeOnly; }
            set { validateOnValueChangeOnly = value; }
        }

        /// <summary>
        /// The current validation error details or null
        /// </summary>
        [XmlIgnore()]
        public ValidationResult<T> LastValidationError
        {
            get
            {
                if (IsValid) return null;
                return lastValidationError;
            }
            protected set { lastValidationError = value; }
        }
        
        [XmlIgnore()]
        public ValidationResult LastValidationErrorMessage
        {
            get
            {
                if (LastValidationError == null) return null;
                return LastValidationError.ToValidationResult();
            }
        }        

        #endregion
        
        #endregion

        #region ISetting Members
        /// <summary>
        /// Restores the setting value to the previously saved value
        /// </summary>
        public override void Revert()
        {
            Value = SavedValue;
        }

        /// <summary>
        /// Saves the current value
        /// </summary>
        public override void Save()
        {
            SavedValue = Value;
        }

        /// <summary>
        /// Updates this setting with the value of other. Used to merge deserialised
        /// </summary>
        /// <param name="other"></param>
        public override void LoadSettingsFrom(BaseSetting other)
        {
            base.LoadSettingsFrom(other);

            SettingValue<T> setting = other as SettingValue<T>;
            if (setting != null)
            {
                if (other.Name == "enabled")
                {
                    int a = 1;
                    a += 1;
                }

                SavedValue = Value = setting.SavedValue;
            }
        }

        #endregion

        #region Cloning

        /// <summary>
        /// Clones this object
        /// </summary>
        /// <returns></returns>
        public override BaseSetting DeepCopy()
        {
            SettingValue<T> clone = (SettingValue<T>)base.DeepCopy();
            clone.Value = Value;
            clone.SavedValue = SavedValue;
            clone.DefaultValue = DefaultValue;
            clone.ValidateOnValueChangeOnly = ValidateOnValueChangeOnly;

            foreach (SettingValidator<T> validator in validators)
                clone.AddValidator(validator);

            return clone;
        }

        protected override BaseSetting GetNew()
        {
            return new SettingValue<T>();
        }

        #endregion
    }
}
