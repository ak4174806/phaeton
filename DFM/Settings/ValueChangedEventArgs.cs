﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DFM.Settings
{
    public class ValueChangedEventArgs<T> : EventArgs
    {
        private T value;
        private ISettingValue<T> setting;

        /// <summary>
        /// The new value of the setting
        /// </summary>
        public T Value
        {
            get { return value; }
            set { this.value = value; }
        }

        /// <summary>
        /// The setting object (contains new value)
        /// </summary>
        public ISettingValue<T> Setting
        {
            get { return setting; }
            set { setting = value; }
        }

        public ValueChangedEventArgs(T value, ISettingValue<T> setting)
        {
            Value = value;
            Setting = setting;
        }
    }
}
