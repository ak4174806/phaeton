﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DFM.Settings
{
    public class ValidationResult<T>
    {
        private bool isError = false;
        private string errorMessage = "";
        private T value;

        /// <summary>
        /// Whether an error was found
        /// </summary>
        public bool IsError
        {
            get { return isError; }
            set { isError = value; }
        }

        /// <summary>
        /// The appropriate error message
        /// </summary>
        public string ErrorMessage
        {
            get
            {
                if (!IsError) throw new ApplicationException("Tried to access validation error message when no validation error occurred.");
                return errorMessage;
            }
            set { errorMessage = value; }
        }

        /// <summary>
        /// The processed value
        /// </summary>
        public T Value
        {
            get { return value; }
            set { this.value = value; }
        }

        public ValidationResult(T value)
        {
            IsError = false;
            ErrorMessage = "";
            Value = value;
        }

        public ValidationResult ToValidationResult()
        {
            if (IsError) return new ValidationResult(ErrorMessage);
            else return new ValidationResult();
        }
    }

    public class ValidationResult
    {
        private bool isError;
        private string errorMessage;

        /// <summary>
        /// Whether an error was found
        /// </summary>
        public bool IsError
        {
            get { return isError; }
            set { isError = value; }
        }

        /// <summary>
        /// The appropriate error message
        /// </summary>
        public string ErrorMessage
        {
            get
            {
                if (!IsError) throw new ApplicationException("Tried to access validation error message when no validation error occurred.");
                return errorMessage;
            }
            set { errorMessage = value; }
        }

        public ValidationResult()
        {
            IsError = false;
            ErrorMessage = "";
        }

        public ValidationResult(string errorMessage)
        {
            IsError = true;
            ErrorMessage = errorMessage;
        }
    }
}
