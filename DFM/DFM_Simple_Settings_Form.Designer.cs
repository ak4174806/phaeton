﻿namespace DFM
{
    partial class DFM_Simple_Settings_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.Label lblFilenamePattern;
            System.Windows.Forms.Label lblDataLocation;
            System.Windows.Forms.Label lblScanAveNumber;
            System.Windows.Forms.Label lblMaxPeakWidth;
            System.Windows.Forms.Label lblPeakHysteresis;
            System.Windows.Forms.Label lblPeakThreshold;
            System.Windows.Forms.Label label22;
            System.Windows.Forms.Label lblTruncateLast;
            System.Windows.Forms.Label label20;
            System.Windows.Forms.Label lblTruncateFirst;
            System.Windows.Forms.Label label8;
            System.Windows.Forms.Label lblDutyCycle;
            System.Windows.Forms.Label label13;
            System.Windows.Forms.Label lblGatingFreq;
            System.Windows.Forms.TabPage tpScanTiming;
            System.Windows.Forms.Label label7;
            System.Windows.Forms.Label label21;
            System.Windows.Forms.Label label24;
            System.Windows.Forms.Label label26;
            System.Windows.Forms.Label lblLaserStart;
            System.Windows.Forms.Label lblDAQStart;
            System.Windows.Forms.Label label6;
            System.Windows.Forms.Label label9;
            System.Windows.Forms.Label lblDAQStop;
            System.Windows.Forms.Label lblRepeatInterval;
            System.Windows.Forms.Label label12;
            System.Windows.Forms.Label label3;
            System.Windows.Forms.Label label2;
            System.Windows.Forms.Label label1;
            System.Windows.Forms.Label lblLaserPort;
            System.Windows.Forms.Label lblLaserBaudrate;
            System.Windows.Forms.Label lblSensorModel;
            System.Windows.Forms.Label lblSensorType;
            System.Windows.Forms.Label lblSensorBaudrate;
            System.Windows.Forms.Label lblLaserHandshake;
            System.Windows.Forms.Label lblUnits;
            System.Windows.Forms.Label label4;
            System.Windows.Forms.Label label25;
            this.rdBtnAutoDetect = new System.Windows.Forms.RadioButton();
            this.numLaserStart = new System.Windows.Forms.NumericUpDown();
            this.grpBxAuto = new System.Windows.Forms.GroupBox();
            this.nmEdgePoints = new System.Windows.Forms.NumericUpDown();
            this.nmHigherRange = new System.Windows.Forms.NumericUpDown();
            this.nmLowerRange = new System.Windows.Forms.NumericUpDown();
            this.rdBtnTimer = new System.Windows.Forms.RadioButton();
            this.grpBxTimer = new System.Windows.Forms.GroupBox();
            this.numDAQStart = new System.Windows.Forms.NumericUpDown();
            this.numDAQStop = new System.Windows.Forms.NumericUpDown();
            this.txtMotorCommand = new System.Windows.Forms.TextBox();
            this.chkRepeat = new System.Windows.Forms.CheckBox();
            this.numRepeatCount = new System.Windows.Forms.NumericUpDown();
            this.numRepeatInterval = new System.Windows.Forms.NumericUpDown();
            this.numMoveMotorAfter = new System.Windows.Forms.NumericUpDown();
            this.lblSensorPort = new System.Windows.Forms.Label();
            this.controlButtonsContainer = new System.Windows.Forms.SplitContainer();
            this.tabSettings = new System.Windows.Forms.TabControl();
            this.tpConnection = new System.Windows.Forms.TabPage();
            this.btnRefreshCOMPorts = new System.Windows.Forms.Button();
            this.btnConnect = new System.Windows.Forms.Button();
            this.tabConnection = new System.Windows.Forms.TabControl();
            this.tpSensorConnection = new System.Windows.Forms.TabPage();
            this.txtSensorAddress = new System.Windows.Forms.TextBox();
            this.cmbSensorBaudrate = new System.Windows.Forms.ComboBox();
            this.cmbSensorPort = new System.Windows.Forms.ComboBox();
            this.cmbSensorType = new System.Windows.Forms.ComboBox();
            this.cmbSensorModel = new System.Windows.Forms.ComboBox();
            this.chkConnectSensor = new System.Windows.Forms.CheckBox();
            this.tpLaserConnection = new System.Windows.Forms.TabPage();
            this.cmbLaserHandshake = new System.Windows.Forms.ComboBox();
            this.cmbLaserBaudrate = new System.Windows.Forms.ComboBox();
            this.cmbLaserPort = new System.Windows.Forms.ComboBox();
            this.chkConnectLaser = new System.Windows.Forms.CheckBox();
            this.tpNewLaserConnection = new System.Windows.Forms.TabPage();
            this.grpBxLaser = new System.Windows.Forms.GroupBox();
            this.numT = new System.Windows.Forms.NumericUpDown();
            this.numN = new System.Windows.Forms.NumericUpDown();
            this.label30 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.numB = new System.Windows.Forms.NumericUpDown();
            this.numD = new System.Windows.Forms.NumericUpDown();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.grpBxMotion = new System.Windows.Forms.GroupBox();
            this.numV = new System.Windows.Forms.NumericUpDown();
            this.label29 = new System.Windows.Forms.Label();
            this.numE = new System.Windows.Forms.NumericUpDown();
            this.label27 = new System.Windows.Forms.Label();
            this.numS = new System.Windows.Forms.NumericUpDown();
            this.label23 = new System.Windows.Forms.Label();
            this.cmbNewLaserPort = new System.Windows.Forms.ComboBox();
            this.chkConnectNewlaser = new System.Windows.Forms.CheckBox();
            this.tpScanParameters = new System.Windows.Forms.TabPage();
            this.tabScanParameters = new System.Windows.Forms.TabControl();
            this.tpLaserDetails = new System.Windows.Forms.TabPage();
            this.radPulseCount = new System.Windows.Forms.RadioButton();
            this.numPulseCount = new System.Windows.Forms.NumericUpDown();
            this.radRunTime = new System.Windows.Forms.RadioButton();
            this.numRunTime = new System.Windows.Forms.NumericUpDown();
            this.numGatingFreq = new System.Windows.Forms.NumericUpDown();
            this.numDutyCycle = new System.Windows.Forms.NumericUpDown();
            this.tpAlgorithms = new System.Windows.Forms.TabPage();
            this.lblIncorrectPwd = new System.Windows.Forms.Label();
            this.ScanDataGrid = new System.Windows.Forms.DataGridView();
            this.label11 = new System.Windows.Forms.Label();
            this.txtPwd = new System.Windows.Forms.TextBox();
            this.btnUnlock = new System.Windows.Forms.Button();
            this.btnAddPaint = new System.Windows.Forms.Button();
            this.tpDataAnalysis = new System.Windows.Forms.TabPage();
            this.tabDataAnalysis = new System.Windows.Forms.TabControl();
            this.tpDataProcessing = new System.Windows.Forms.TabPage();
            this.numTruncateFirst = new System.Windows.Forms.NumericUpDown();
            this.numTruncateLast = new System.Windows.Forms.NumericUpDown();
            this.chkFilterPeaks = new System.Windows.Forms.CheckBox();
            this.numPeakThreshold = new System.Windows.Forms.NumericUpDown();
            this.numPeakHysteresis = new System.Windows.Forms.NumericUpDown();
            this.numMaxPeakWidth = new System.Windows.Forms.NumericUpDown();
            this.tpDataLocation = new System.Windows.Forms.TabPage();
            this.chkMultipleSegmentScan = new System.Windows.Forms.CheckBox();
            this.cmbUnits = new System.Windows.Forms.ComboBox();
            this.numMaxSegments = new System.Windows.Forms.NumericUpDown();
            this.numScanAveNumber = new System.Windows.Forms.NumericUpDown();
            this.txtDataLocation = new System.Windows.Forms.TextBox();
            this.txtFilenamePattern = new System.Windows.Forms.TextBox();
            this.btnChooseDataLocation = new System.Windows.Forms.Button();
            this.tpMisc = new System.Windows.Forms.TabPage();
            this.grpBxConversionX = new System.Windows.Forms.GroupBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.nmSensorFrequency = new System.Windows.Forms.NumericUpDown();
            this.nmLineSpeed = new System.Windows.Forms.NumericUpDown();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.chkAppyLeftMiddleRight = new System.Windows.Forms.CheckBox();
            this.grpPits = new System.Windows.Forms.GroupBox();
            this.nmIgnoreEdge = new System.Windows.Forms.NumericUpDown();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.chkFilter = new System.Windows.Forms.CheckBox();
            this.grpFilteration = new System.Windows.Forms.GroupBox();
            this.nmDiffFilter = new System.Windows.Forms.NumericUpDown();
            this.label10 = new System.Windows.Forms.Label();
            this.nmLagTime = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.btnChangePassword = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.numA = new System.Windows.Forms.NumericUpDown();
            this.label28 = new System.Windows.Forms.Label();
            this.lblHeading = new System.Windows.Forms.Label();
            this.outerContainer = new System.Windows.Forms.SplitContainer();
            this.folderBrowser = new System.Windows.Forms.FolderBrowserDialog();
            this.tmrFade = new System.Timers.Timer();
            lblFilenamePattern = new System.Windows.Forms.Label();
            lblDataLocation = new System.Windows.Forms.Label();
            lblScanAveNumber = new System.Windows.Forms.Label();
            lblMaxPeakWidth = new System.Windows.Forms.Label();
            lblPeakHysteresis = new System.Windows.Forms.Label();
            lblPeakThreshold = new System.Windows.Forms.Label();
            label22 = new System.Windows.Forms.Label();
            lblTruncateLast = new System.Windows.Forms.Label();
            label20 = new System.Windows.Forms.Label();
            lblTruncateFirst = new System.Windows.Forms.Label();
            label8 = new System.Windows.Forms.Label();
            lblDutyCycle = new System.Windows.Forms.Label();
            label13 = new System.Windows.Forms.Label();
            lblGatingFreq = new System.Windows.Forms.Label();
            tpScanTiming = new System.Windows.Forms.TabPage();
            label7 = new System.Windows.Forms.Label();
            label21 = new System.Windows.Forms.Label();
            label24 = new System.Windows.Forms.Label();
            label26 = new System.Windows.Forms.Label();
            lblLaserStart = new System.Windows.Forms.Label();
            lblDAQStart = new System.Windows.Forms.Label();
            label6 = new System.Windows.Forms.Label();
            label9 = new System.Windows.Forms.Label();
            lblDAQStop = new System.Windows.Forms.Label();
            lblRepeatInterval = new System.Windows.Forms.Label();
            label12 = new System.Windows.Forms.Label();
            label3 = new System.Windows.Forms.Label();
            label2 = new System.Windows.Forms.Label();
            label1 = new System.Windows.Forms.Label();
            lblLaserPort = new System.Windows.Forms.Label();
            lblLaserBaudrate = new System.Windows.Forms.Label();
            lblSensorModel = new System.Windows.Forms.Label();
            lblSensorType = new System.Windows.Forms.Label();
            lblSensorBaudrate = new System.Windows.Forms.Label();
            lblLaserHandshake = new System.Windows.Forms.Label();
            lblUnits = new System.Windows.Forms.Label();
            label4 = new System.Windows.Forms.Label();
            label25 = new System.Windows.Forms.Label();
            tpScanTiming.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numLaserStart)).BeginInit();
            this.grpBxAuto.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nmEdgePoints)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmHigherRange)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmLowerRange)).BeginInit();
            this.grpBxTimer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numDAQStart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numDAQStop)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numRepeatCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numRepeatInterval)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numMoveMotorAfter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.controlButtonsContainer)).BeginInit();
            this.controlButtonsContainer.Panel1.SuspendLayout();
            this.controlButtonsContainer.Panel2.SuspendLayout();
            this.controlButtonsContainer.SuspendLayout();
            this.tabSettings.SuspendLayout();
            this.tpConnection.SuspendLayout();
            this.tabConnection.SuspendLayout();
            this.tpSensorConnection.SuspendLayout();
            this.tpLaserConnection.SuspendLayout();
            this.tpNewLaserConnection.SuspendLayout();
            this.grpBxLaser.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numD)).BeginInit();
            this.grpBxMotion.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numV)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numE)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numS)).BeginInit();
            this.tpScanParameters.SuspendLayout();
            this.tabScanParameters.SuspendLayout();
            this.tpLaserDetails.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numPulseCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numRunTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numGatingFreq)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numDutyCycle)).BeginInit();
            this.tpAlgorithms.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ScanDataGrid)).BeginInit();
            this.tpDataAnalysis.SuspendLayout();
            this.tabDataAnalysis.SuspendLayout();
            this.tpDataProcessing.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numTruncateFirst)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numTruncateLast)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPeakThreshold)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPeakHysteresis)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numMaxPeakWidth)).BeginInit();
            this.tpDataLocation.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numMaxSegments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numScanAveNumber)).BeginInit();
            this.tpMisc.SuspendLayout();
            this.grpBxConversionX.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nmSensorFrequency)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmLineSpeed)).BeginInit();
            this.grpPits.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nmIgnoreEdge)).BeginInit();
            this.grpFilteration.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nmDiffFilter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmLagTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.outerContainer)).BeginInit();
            this.outerContainer.Panel1.SuspendLayout();
            this.outerContainer.Panel2.SuspendLayout();
            this.outerContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tmrFade)).BeginInit();
            this.SuspendLayout();
            // 
            // lblFilenamePattern
            // 
            lblFilenamePattern.AutoSize = true;
            lblFilenamePattern.Location = new System.Drawing.Point(18, 60);
            lblFilenamePattern.Name = "lblFilenamePattern";
            lblFilenamePattern.Size = new System.Drawing.Size(89, 13);
            lblFilenamePattern.TabIndex = 38;
            lblFilenamePattern.Text = "Filename Pattern:";
            // 
            // lblDataLocation
            // 
            lblDataLocation.AutoSize = true;
            lblDataLocation.Location = new System.Drawing.Point(18, 23);
            lblDataLocation.Name = "lblDataLocation";
            lblDataLocation.Size = new System.Drawing.Size(77, 13);
            lblDataLocation.TabIndex = 35;
            lblDataLocation.Text = "Data Location:";
            // 
            // lblScanAveNumber
            // 
            lblScanAveNumber.AutoSize = true;
            lblScanAveNumber.Location = new System.Drawing.Point(18, 86);
            lblScanAveNumber.Name = "lblScanAveNumber";
            lblScanAveNumber.Size = new System.Drawing.Size(137, 13);
            lblScanAveNumber.TabIndex = 70;
            lblScanAveNumber.Text = "Scan Display Ave. Number:";
            // 
            // lblMaxPeakWidth
            // 
            lblMaxPeakWidth.AutoSize = true;
            lblMaxPeakWidth.Location = new System.Drawing.Point(19, 126);
            lblMaxPeakWidth.Name = "lblMaxPeakWidth";
            lblMaxPeakWidth.Size = new System.Drawing.Size(86, 13);
            lblMaxPeakWidth.TabIndex = 52;
            lblMaxPeakWidth.Text = "Max Peak Width";
            // 
            // lblPeakHysteresis
            // 
            lblPeakHysteresis.AutoSize = true;
            lblPeakHysteresis.Location = new System.Drawing.Point(19, 105);
            lblPeakHysteresis.Name = "lblPeakHysteresis";
            lblPeakHysteresis.Size = new System.Drawing.Size(128, 13);
            lblPeakHysteresis.TabIndex = 53;
            lblPeakHysteresis.Text = "Peak Hysteresis (microns)";
            // 
            // lblPeakThreshold
            // 
            lblPeakThreshold.AutoSize = true;
            lblPeakThreshold.Location = new System.Drawing.Point(19, 86);
            lblPeakThreshold.Name = "lblPeakThreshold";
            lblPeakThreshold.Size = new System.Drawing.Size(127, 13);
            lblPeakThreshold.TabIndex = 54;
            lblPeakThreshold.Text = "Peak Threshold (microns)";
            // 
            // label22
            // 
            label22.AutoSize = true;
            label22.Location = new System.Drawing.Point(161, 35);
            label22.Name = "label22";
            label22.Size = new System.Drawing.Size(51, 13);
            label22.TabIndex = 64;
            label22.Text = "% of data";
            // 
            // lblTruncateLast
            // 
            lblTruncateLast.AutoSize = true;
            lblTruncateLast.Location = new System.Drawing.Point(20, 35);
            lblTruncateLast.Name = "lblTruncateLast";
            lblTruncateLast.Size = new System.Drawing.Size(69, 13);
            lblTruncateLast.TabIndex = 62;
            lblTruncateLast.Text = "Truncate last";
            // 
            // label20
            // 
            label20.AutoSize = true;
            label20.Location = new System.Drawing.Point(161, 15);
            label20.Name = "label20";
            label20.Size = new System.Drawing.Size(51, 13);
            label20.TabIndex = 61;
            label20.Text = "% of data";
            // 
            // lblTruncateFirst
            // 
            lblTruncateFirst.AutoSize = true;
            lblTruncateFirst.Location = new System.Drawing.Point(21, 16);
            lblTruncateFirst.Name = "lblTruncateFirst";
            lblTruncateFirst.Size = new System.Drawing.Size(72, 13);
            lblTruncateFirst.TabIndex = 59;
            lblTruncateFirst.Text = "Truncate first:";
            // 
            // label8
            // 
            label8.AutoSize = true;
            label8.Location = new System.Drawing.Point(210, 105);
            label8.Name = "label8";
            label8.Size = new System.Drawing.Size(15, 13);
            label8.TabIndex = 31;
            label8.Text = "%";
            // 
            // lblDutyCycle
            // 
            lblDutyCycle.AutoSize = true;
            lblDutyCycle.Location = new System.Drawing.Point(19, 104);
            lblDutyCycle.Name = "lblDutyCycle";
            lblDutyCycle.Size = new System.Drawing.Size(61, 13);
            lblDutyCycle.TabIndex = 29;
            lblDutyCycle.Text = "Duty Cycle:";
            // 
            // label13
            // 
            label13.AutoSize = true;
            label13.Location = new System.Drawing.Point(210, 77);
            label13.Name = "label13";
            label13.Size = new System.Drawing.Size(20, 13);
            label13.TabIndex = 28;
            label13.Text = "Hz";
            // 
            // lblGatingFreq
            // 
            lblGatingFreq.AutoSize = true;
            lblGatingFreq.Location = new System.Drawing.Point(19, 75);
            lblGatingFreq.Name = "lblGatingFreq";
            lblGatingFreq.Size = new System.Drawing.Size(94, 13);
            lblGatingFreq.TabIndex = 26;
            lblGatingFreq.Text = "Gating Frequency:";
            // 
            // tpScanTiming
            // 
            tpScanTiming.Controls.Add(this.rdBtnAutoDetect);
            tpScanTiming.Controls.Add(label7);
            tpScanTiming.Controls.Add(this.numLaserStart);
            tpScanTiming.Controls.Add(this.grpBxAuto);
            tpScanTiming.Controls.Add(lblLaserStart);
            tpScanTiming.Controls.Add(this.rdBtnTimer);
            tpScanTiming.Controls.Add(this.grpBxTimer);
            tpScanTiming.Controls.Add(this.txtMotorCommand);
            tpScanTiming.Controls.Add(this.chkRepeat);
            tpScanTiming.Controls.Add(this.numRepeatCount);
            tpScanTiming.Controls.Add(lblRepeatInterval);
            tpScanTiming.Controls.Add(this.numRepeatInterval);
            tpScanTiming.Controls.Add(label12);
            tpScanTiming.Controls.Add(label3);
            tpScanTiming.Controls.Add(label2);
            tpScanTiming.Controls.Add(this.numMoveMotorAfter);
            tpScanTiming.Controls.Add(label1);
            tpScanTiming.Location = new System.Drawing.Point(4, 22);
            tpScanTiming.Name = "tpScanTiming";
            tpScanTiming.Padding = new System.Windows.Forms.Padding(3);
            tpScanTiming.Size = new System.Drawing.Size(422, 182);
            tpScanTiming.TabIndex = 0;
            tpScanTiming.Text = "Scan Mode";
            tpScanTiming.UseVisualStyleBackColor = true;
            tpScanTiming.Click += new System.EventHandler(this.tpScanTiming_Click);
            // 
            // rdBtnAutoDetect
            // 
            this.rdBtnAutoDetect.AutoSize = true;
            this.rdBtnAutoDetect.Location = new System.Drawing.Point(277, 3);
            this.rdBtnAutoDetect.Name = "rdBtnAutoDetect";
            this.rdBtnAutoDetect.Size = new System.Drawing.Size(82, 17);
            this.rdBtnAutoDetect.TabIndex = 54;
            this.rdBtnAutoDetect.TabStop = true;
            this.rdBtnAutoDetect.Text = "Auto Detect";
            this.rdBtnAutoDetect.UseVisualStyleBackColor = true;
            this.rdBtnAutoDetect.CheckedChanged += new System.EventHandler(this.rdBtnAutoDetect_CheckedChanged);
            // 
            // label7
            // 
            label7.AutoSize = true;
            label7.Location = new System.Drawing.Point(181, 83);
            label7.Name = "label7";
            label7.Size = new System.Drawing.Size(20, 13);
            label7.TabIndex = 37;
            label7.Text = "ms";
            // 
            // numLaserStart
            // 
            this.numLaserStart.Location = new System.Drawing.Point(93, 80);
            this.numLaserStart.Maximum = new decimal(new int[] {
            200000000,
            0,
            0,
            0});
            this.numLaserStart.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numLaserStart.Name = "numLaserStart";
            this.numLaserStart.Size = new System.Drawing.Size(84, 20);
            this.numLaserStart.TabIndex = 36;
            this.numLaserStart.Value = new decimal(new int[] {
            182,
            0,
            0,
            0});
            // 
            // grpBxAuto
            // 
            this.grpBxAuto.Controls.Add(label21);
            this.grpBxAuto.Controls.Add(this.nmEdgePoints);
            this.grpBxAuto.Controls.Add(label24);
            this.grpBxAuto.Controls.Add(this.nmHigherRange);
            this.grpBxAuto.Controls.Add(label26);
            this.grpBxAuto.Controls.Add(this.nmLowerRange);
            this.grpBxAuto.Location = new System.Drawing.Point(211, 19);
            this.grpBxAuto.Name = "grpBxAuto";
            this.grpBxAuto.Size = new System.Drawing.Size(205, 84);
            this.grpBxAuto.TabIndex = 53;
            this.grpBxAuto.TabStop = false;
            // 
            // label21
            // 
            label21.AutoSize = true;
            label21.Location = new System.Drawing.Point(17, 12);
            label21.Name = "label21";
            label21.Size = new System.Drawing.Size(74, 13);
            label21.TabIndex = 32;
            label21.Text = "Lower Range:";
            // 
            // nmEdgePoints
            // 
            this.nmEdgePoints.Location = new System.Drawing.Point(108, 59);
            this.nmEdgePoints.Maximum = new decimal(new int[] {
            200000000,
            0,
            0,
            0});
            this.nmEdgePoints.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nmEdgePoints.Name = "nmEdgePoints";
            this.nmEdgePoints.Size = new System.Drawing.Size(84, 20);
            this.nmEdgePoints.TabIndex = 39;
            this.nmEdgePoints.Value = new decimal(new int[] {
            300,
            0,
            0,
            0});
            // 
            // label24
            // 
            label24.AutoSize = true;
            label24.Location = new System.Drawing.Point(17, 61);
            label24.Name = "label24";
            label24.Size = new System.Drawing.Size(92, 13);
            label24.TabIndex = 38;
            label24.Text = "Edge Pts Ignored:";
            // 
            // nmHigherRange
            // 
            this.nmHigherRange.DecimalPlaces = 3;
            this.nmHigherRange.Location = new System.Drawing.Point(108, 34);
            this.nmHigherRange.Maximum = new decimal(new int[] {
            200000000,
            0,
            0,
            0});
            this.nmHigherRange.Name = "nmHigherRange";
            this.nmHigherRange.Size = new System.Drawing.Size(84, 20);
            this.nmHigherRange.TabIndex = 36;
            this.nmHigherRange.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
            // 
            // label26
            // 
            label26.AutoSize = true;
            label26.Location = new System.Drawing.Point(17, 36);
            label26.Name = "label26";
            label26.Size = new System.Drawing.Size(76, 13);
            label26.TabIndex = 35;
            label26.Text = "Higher Range:";
            // 
            // nmLowerRange
            // 
            this.nmLowerRange.DecimalPlaces = 3;
            this.nmLowerRange.Location = new System.Drawing.Point(108, 9);
            this.nmLowerRange.Maximum = new decimal(new int[] {
            200000000,
            0,
            0,
            0});
            this.nmLowerRange.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            196608});
            this.nmLowerRange.Name = "nmLowerRange";
            this.nmLowerRange.Size = new System.Drawing.Size(84, 20);
            this.nmLowerRange.TabIndex = 33;
            this.nmLowerRange.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // lblLaserStart
            // 
            lblLaserStart.AutoSize = true;
            lblLaserStart.Location = new System.Drawing.Point(6, 82);
            lblLaserStart.Name = "lblLaserStart";
            lblLaserStart.Size = new System.Drawing.Size(81, 13);
            lblLaserStart.TabIndex = 35;
            lblLaserStart.Text = "Start laser after:";
            // 
            // rdBtnTimer
            // 
            this.rdBtnTimer.AutoSize = true;
            this.rdBtnTimer.Location = new System.Drawing.Point(65, 3);
            this.rdBtnTimer.Name = "rdBtnTimer";
            this.rdBtnTimer.Size = new System.Drawing.Size(81, 17);
            this.rdBtnTimer.TabIndex = 53;
            this.rdBtnTimer.TabStop = true;
            this.rdBtnTimer.Text = "Timer Mode";
            this.rdBtnTimer.UseVisualStyleBackColor = true;
            this.rdBtnTimer.CheckedChanged += new System.EventHandler(this.rdBtnTimer_CheckedChanged);
            // 
            // grpBxTimer
            // 
            this.grpBxTimer.Controls.Add(lblDAQStart);
            this.grpBxTimer.Controls.Add(label6);
            this.grpBxTimer.Controls.Add(label9);
            this.grpBxTimer.Controls.Add(this.numDAQStart);
            this.grpBxTimer.Controls.Add(this.numDAQStop);
            this.grpBxTimer.Controls.Add(lblDAQStop);
            this.grpBxTimer.Location = new System.Drawing.Point(6, 18);
            this.grpBxTimer.Name = "grpBxTimer";
            this.grpBxTimer.Size = new System.Drawing.Size(199, 58);
            this.grpBxTimer.TabIndex = 52;
            this.grpBxTimer.TabStop = false;
            // 
            // lblDAQStart
            // 
            lblDAQStart.AutoSize = true;
            lblDAQStart.Location = new System.Drawing.Point(0, 12);
            lblDAQStart.Name = "lblDAQStart";
            lblDAQStart.Size = new System.Drawing.Size(82, 13);
            lblDAQStart.TabIndex = 32;
            lblDAQStart.Text = "Start DAQ after:";
            lblDAQStart.Click += new System.EventHandler(this.lblDAQStart_Click);
            // 
            // label6
            // 
            label6.AutoSize = true;
            label6.Location = new System.Drawing.Point(175, 13);
            label6.Name = "label6";
            label6.Size = new System.Drawing.Size(20, 13);
            label6.TabIndex = 34;
            label6.Text = "ms";
            label6.Click += new System.EventHandler(this.label6_Click);
            // 
            // label9
            // 
            label9.AutoSize = true;
            label9.Location = new System.Drawing.Point(175, 38);
            label9.Name = "label9";
            label9.Size = new System.Drawing.Size(20, 13);
            label9.TabIndex = 40;
            label9.Text = "ms";
            // 
            // numDAQStart
            // 
            this.numDAQStart.Location = new System.Drawing.Point(87, 9);
            this.numDAQStart.Maximum = new decimal(new int[] {
            200000000,
            0,
            0,
            0});
            this.numDAQStart.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numDAQStart.Name = "numDAQStart";
            this.numDAQStart.Size = new System.Drawing.Size(84, 20);
            this.numDAQStart.TabIndex = 33;
            this.numDAQStart.Value = new decimal(new int[] {
            180,
            0,
            0,
            0});
            this.numDAQStart.ValueChanged += new System.EventHandler(this.numDAQStart_ValueChanged);
            // 
            // numDAQStop
            // 
            this.numDAQStop.Location = new System.Drawing.Point(87, 32);
            this.numDAQStop.Maximum = new decimal(new int[] {
            200000000,
            0,
            0,
            0});
            this.numDAQStop.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numDAQStop.Name = "numDAQStop";
            this.numDAQStop.Size = new System.Drawing.Size(84, 20);
            this.numDAQStop.TabIndex = 39;
            this.numDAQStop.Value = new decimal(new int[] {
            220,
            0,
            0,
            0});
            // 
            // lblDAQStop
            // 
            lblDAQStop.AutoSize = true;
            lblDAQStop.Location = new System.Drawing.Point(0, 34);
            lblDAQStop.Name = "lblDAQStop";
            lblDAQStop.Size = new System.Drawing.Size(82, 13);
            lblDAQStop.TabIndex = 38;
            lblDAQStop.Text = "Stop DAQ after:";
            // 
            // txtMotorCommand
            // 
            this.txtMotorCommand.Location = new System.Drawing.Point(154, 156);
            this.txtMotorCommand.Name = "txtMotorCommand";
            this.txtMotorCommand.Size = new System.Drawing.Size(85, 20);
            this.txtMotorCommand.TabIndex = 51;
            this.txtMotorCommand.Text = "M";
            // 
            // chkRepeat
            // 
            this.chkRepeat.AutoSize = true;
            this.chkRepeat.Location = new System.Drawing.Point(20, 110);
            this.chkRepeat.Name = "chkRepeat";
            this.chkRepeat.Size = new System.Drawing.Size(104, 17);
            this.chkRepeat.TabIndex = 50;
            this.chkRepeat.Text = "Repeat readings";
            this.chkRepeat.UseVisualStyleBackColor = true;
            // 
            // numRepeatCount
            // 
            this.numRepeatCount.Location = new System.Drawing.Point(127, 107);
            this.numRepeatCount.Maximum = new decimal(new int[] {
            1215752192,
            23,
            0,
            0});
            this.numRepeatCount.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numRepeatCount.Name = "numRepeatCount";
            this.numRepeatCount.Size = new System.Drawing.Size(40, 20);
            this.numRepeatCount.TabIndex = 48;
            this.numRepeatCount.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            // 
            // lblRepeatInterval
            // 
            lblRepeatInterval.AutoSize = true;
            lblRepeatInterval.Location = new System.Drawing.Point(172, 111);
            lblRepeatInterval.Name = "lblRepeatInterval";
            lblRepeatInterval.Size = new System.Drawing.Size(60, 13);
            lblRepeatInterval.TabIndex = 47;
            lblRepeatInterval.Text = "times every";
            // 
            // numRepeatInterval
            // 
            this.numRepeatInterval.DecimalPlaces = 1;
            this.numRepeatInterval.Location = new System.Drawing.Point(238, 109);
            this.numRepeatInterval.Maximum = new decimal(new int[] {
            200000000,
            0,
            0,
            0});
            this.numRepeatInterval.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numRepeatInterval.Name = "numRepeatInterval";
            this.numRepeatInterval.Size = new System.Drawing.Size(53, 20);
            this.numRepeatInterval.TabIndex = 49;
            this.numRepeatInterval.Value = new decimal(new int[] {
            12,
            0,
            0,
            65536});
            // 
            // label12
            // 
            label12.AutoSize = true;
            label12.Location = new System.Drawing.Point(293, 111);
            label12.Name = "label12";
            label12.Size = new System.Drawing.Size(12, 13);
            label12.TabIndex = 46;
            label12.Text = "s";
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Location = new System.Drawing.Point(16, 159);
            label3.Name = "label3";
            label3.Size = new System.Drawing.Size(136, 13);
            label3.TabIndex = 38;
            label3.Text = "Motor/Controller Command:";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new System.Drawing.Point(16, 135);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(87, 13);
            label2.TabIndex = 38;
            label2.Text = "Move motor after";
            // 
            // numMoveMotorAfter
            // 
            this.numMoveMotorAfter.Location = new System.Drawing.Point(154, 132);
            this.numMoveMotorAfter.Maximum = new decimal(new int[] {
            200000000,
            0,
            0,
            0});
            this.numMoveMotorAfter.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numMoveMotorAfter.Name = "numMoveMotorAfter";
            this.numMoveMotorAfter.Size = new System.Drawing.Size(84, 20);
            this.numMoveMotorAfter.TabIndex = 39;
            this.numMoveMotorAfter.Value = new decimal(new int[] {
            220,
            0,
            0,
            0});
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new System.Drawing.Point(241, 130);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(20, 13);
            label1.TabIndex = 40;
            label1.Text = "ms";
            // 
            // lblLaserPort
            // 
            lblLaserPort.AutoSize = true;
            lblLaserPort.Location = new System.Drawing.Point(15, 44);
            lblLaserPort.Name = "lblLaserPort";
            lblLaserPort.Size = new System.Drawing.Size(29, 13);
            lblLaserPort.TabIndex = 44;
            lblLaserPort.Text = "Port:";
            // 
            // lblLaserBaudrate
            // 
            lblLaserBaudrate.AutoSize = true;
            lblLaserBaudrate.Location = new System.Drawing.Point(16, 82);
            lblLaserBaudrate.Name = "lblLaserBaudrate";
            lblLaserBaudrate.Size = new System.Drawing.Size(53, 13);
            lblLaserBaudrate.TabIndex = 47;
            lblLaserBaudrate.Text = "Baudrate:";
            // 
            // lblSensorModel
            // 
            lblSensorModel.AutoSize = true;
            lblSensorModel.Location = new System.Drawing.Point(9, 42);
            lblSensorModel.Name = "lblSensorModel";
            lblSensorModel.Size = new System.Drawing.Size(39, 13);
            lblSensorModel.TabIndex = 37;
            lblSensorModel.Text = "Model:";
            // 
            // lblSensorType
            // 
            lblSensorType.AutoSize = true;
            lblSensorType.Location = new System.Drawing.Point(168, 42);
            lblSensorType.Name = "lblSensorType";
            lblSensorType.Size = new System.Drawing.Size(34, 13);
            lblSensorType.TabIndex = 36;
            lblSensorType.Text = "Type:";
            // 
            // lblSensorBaudrate
            // 
            lblSensorBaudrate.AutoSize = true;
            lblSensorBaudrate.Location = new System.Drawing.Point(168, 69);
            lblSensorBaudrate.Name = "lblSensorBaudrate";
            lblSensorBaudrate.Size = new System.Drawing.Size(53, 13);
            lblSensorBaudrate.TabIndex = 43;
            lblSensorBaudrate.Text = "Baudrate:";
            // 
            // lblLaserHandshake
            // 
            lblLaserHandshake.AutoSize = true;
            lblLaserHandshake.Location = new System.Drawing.Point(186, 44);
            lblLaserHandshake.Name = "lblLaserHandshake";
            lblLaserHandshake.Size = new System.Drawing.Size(65, 13);
            lblLaserHandshake.TabIndex = 48;
            lblLaserHandshake.Text = "Handshake:";
            // 
            // lblUnits
            // 
            lblUnits.AutoSize = true;
            lblUnits.Location = new System.Drawing.Point(18, 113);
            lblUnits.Name = "lblUnits";
            lblUnits.Size = new System.Drawing.Size(34, 13);
            lblUnits.TabIndex = 74;
            lblUnits.Text = "Units:";
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Location = new System.Drawing.Point(18, 170);
            label4.Name = "label4";
            label4.Size = new System.Drawing.Size(90, 13);
            label4.TabIndex = 70;
            label4.Text = "Max # Segments:";
            // 
            // label25
            // 
            label25.AutoSize = true;
            label25.Location = new System.Drawing.Point(137, 7);
            label25.Name = "label25";
            label25.Size = new System.Drawing.Size(29, 13);
            label25.TabIndex = 49;
            label25.Text = "Port:";
            // 
            // lblSensorPort
            // 
            this.lblSensorPort.AutoSize = true;
            this.lblSensorPort.Location = new System.Drawing.Point(9, 69);
            this.lblSensorPort.Name = "lblSensorPort";
            this.lblSensorPort.Size = new System.Drawing.Size(29, 13);
            this.lblSensorPort.TabIndex = 40;
            this.lblSensorPort.Text = "Port:";
            // 
            // controlButtonsContainer
            // 
            this.controlButtonsContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.controlButtonsContainer.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.controlButtonsContainer.Location = new System.Drawing.Point(0, 0);
            this.controlButtonsContainer.MinimumSize = new System.Drawing.Size(212, 203);
            this.controlButtonsContainer.Name = "controlButtonsContainer";
            this.controlButtonsContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // controlButtonsContainer.Panel1
            // 
            this.controlButtonsContainer.Panel1.Controls.Add(this.tabSettings);
            this.controlButtonsContainer.Panel1.Padding = new System.Windows.Forms.Padding(15, 8, 15, 8);
            this.controlButtonsContainer.Panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.controlButtonsContainer_Panel1_Paint);
            this.controlButtonsContainer.Panel1MinSize = 0;
            // 
            // controlButtonsContainer.Panel2
            // 
            this.controlButtonsContainer.Panel2.Controls.Add(this.btnSave);
            this.controlButtonsContainer.Panel2.Controls.Add(this.btnOK);
            this.controlButtonsContainer.Panel2.Controls.Add(this.btnCancel);
            this.controlButtonsContainer.Panel2MinSize = 0;
            this.controlButtonsContainer.Size = new System.Drawing.Size(474, 308);
            this.controlButtonsContainer.SplitterDistance = 261;
            this.controlButtonsContainer.TabIndex = 2;
            this.controlButtonsContainer.TabStop = false;
            // 
            // tabSettings
            // 
            this.tabSettings.Controls.Add(this.tpConnection);
            this.tabSettings.Controls.Add(this.tpScanParameters);
            this.tabSettings.Controls.Add(this.tpAlgorithms);
            this.tabSettings.Controls.Add(this.tpDataAnalysis);
            this.tabSettings.Controls.Add(this.tpDataLocation);
            this.tabSettings.Controls.Add(this.tpMisc);
            this.tabSettings.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabSettings.Location = new System.Drawing.Point(15, 8);
            this.tabSettings.Name = "tabSettings";
            this.tabSettings.SelectedIndex = 0;
            this.tabSettings.Size = new System.Drawing.Size(444, 245);
            this.tabSettings.TabIndex = 3;
            // 
            // tpConnection
            // 
            this.tpConnection.Controls.Add(this.btnRefreshCOMPorts);
            this.tpConnection.Controls.Add(this.btnConnect);
            this.tpConnection.Controls.Add(this.tabConnection);
            this.tpConnection.Location = new System.Drawing.Point(4, 22);
            this.tpConnection.Name = "tpConnection";
            this.tpConnection.Size = new System.Drawing.Size(436, 219);
            this.tpConnection.TabIndex = 0;
            this.tpConnection.Text = "Connection";
            this.tpConnection.UseVisualStyleBackColor = true;
            // 
            // btnRefreshCOMPorts
            // 
            this.btnRefreshCOMPorts.Location = new System.Drawing.Point(54, 189);
            this.btnRefreshCOMPorts.Name = "btnRefreshCOMPorts";
            this.btnRefreshCOMPorts.Size = new System.Drawing.Size(115, 23);
            this.btnRefreshCOMPorts.TabIndex = 30;
            this.btnRefreshCOMPorts.Text = "Refresh COM Ports";
            this.btnRefreshCOMPorts.UseVisualStyleBackColor = true;
            // 
            // btnConnect
            // 
            this.btnConnect.Location = new System.Drawing.Point(191, 189);
            this.btnConnect.Name = "btnConnect";
            this.btnConnect.Size = new System.Drawing.Size(115, 23);
            this.btnConnect.TabIndex = 31;
            this.btnConnect.Text = "Connect";
            this.btnConnect.UseVisualStyleBackColor = true;
            // 
            // tabConnection
            // 
            this.tabConnection.Controls.Add(this.tpSensorConnection);
            this.tabConnection.Controls.Add(this.tpLaserConnection);
            this.tabConnection.Controls.Add(this.tpNewLaserConnection);
            this.tabConnection.Location = new System.Drawing.Point(16, 14);
            this.tabConnection.Name = "tabConnection";
            this.tabConnection.SelectedIndex = 0;
            this.tabConnection.Size = new System.Drawing.Size(406, 169);
            this.tabConnection.TabIndex = 29;
            // 
            // tpSensorConnection
            // 
            this.tpSensorConnection.Controls.Add(this.txtSensorAddress);
            this.tpSensorConnection.Controls.Add(lblSensorBaudrate);
            this.tpSensorConnection.Controls.Add(this.cmbSensorBaudrate);
            this.tpSensorConnection.Controls.Add(this.lblSensorPort);
            this.tpSensorConnection.Controls.Add(this.cmbSensorPort);
            this.tpSensorConnection.Controls.Add(lblSensorType);
            this.tpSensorConnection.Controls.Add(this.cmbSensorType);
            this.tpSensorConnection.Controls.Add(lblSensorModel);
            this.tpSensorConnection.Controls.Add(this.cmbSensorModel);
            this.tpSensorConnection.Controls.Add(this.chkConnectSensor);
            this.tpSensorConnection.Location = new System.Drawing.Point(4, 22);
            this.tpSensorConnection.Name = "tpSensorConnection";
            this.tpSensorConnection.Padding = new System.Windows.Forms.Padding(3);
            this.tpSensorConnection.Size = new System.Drawing.Size(398, 143);
            this.tpSensorConnection.TabIndex = 0;
            this.tpSensorConnection.Text = "Sensor";
            this.tpSensorConnection.UseVisualStyleBackColor = true;
            // 
            // txtSensorAddress
            // 
            this.txtSensorAddress.Location = new System.Drawing.Point(54, 67);
            this.txtSensorAddress.Name = "txtSensorAddress";
            this.txtSensorAddress.Size = new System.Drawing.Size(108, 20);
            this.txtSensorAddress.TabIndex = 45;
            // 
            // cmbSensorBaudrate
            // 
            this.cmbSensorBaudrate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSensorBaudrate.FormattingEnabled = true;
            this.cmbSensorBaudrate.Location = new System.Drawing.Point(223, 66);
            this.cmbSensorBaudrate.Name = "cmbSensorBaudrate";
            this.cmbSensorBaudrate.Size = new System.Drawing.Size(69, 21);
            this.cmbSensorBaudrate.TabIndex = 41;
            // 
            // cmbSensorPort
            // 
            this.cmbSensorPort.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSensorPort.FormattingEnabled = true;
            this.cmbSensorPort.Location = new System.Drawing.Point(53, 66);
            this.cmbSensorPort.Name = "cmbSensorPort";
            this.cmbSensorPort.Size = new System.Drawing.Size(109, 21);
            this.cmbSensorPort.TabIndex = 42;
            // 
            // cmbSensorType
            // 
            this.cmbSensorType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSensorType.FormattingEnabled = true;
            this.cmbSensorType.Location = new System.Drawing.Point(223, 39);
            this.cmbSensorType.Name = "cmbSensorType";
            this.cmbSensorType.Size = new System.Drawing.Size(69, 21);
            this.cmbSensorType.TabIndex = 39;
            // 
            // cmbSensorModel
            // 
            this.cmbSensorModel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSensorModel.FormattingEnabled = true;
            this.cmbSensorModel.Location = new System.Drawing.Point(53, 39);
            this.cmbSensorModel.Name = "cmbSensorModel";
            this.cmbSensorModel.Size = new System.Drawing.Size(109, 21);
            this.cmbSensorModel.TabIndex = 38;
            // 
            // chkConnectSensor
            // 
            this.chkConnectSensor.AutoSize = true;
            this.chkConnectSensor.Checked = true;
            this.chkConnectSensor.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkConnectSensor.Location = new System.Drawing.Point(19, 13);
            this.chkConnectSensor.Name = "chkConnectSensor";
            this.chkConnectSensor.Size = new System.Drawing.Size(112, 17);
            this.chkConnectSensor.TabIndex = 35;
            this.chkConnectSensor.Text = "Connect to sensor";
            this.chkConnectSensor.UseVisualStyleBackColor = true;
            // 
            // tpLaserConnection
            // 
            this.tpLaserConnection.Controls.Add(lblLaserHandshake);
            this.tpLaserConnection.Controls.Add(this.cmbLaserHandshake);
            this.tpLaserConnection.Controls.Add(lblLaserBaudrate);
            this.tpLaserConnection.Controls.Add(this.cmbLaserBaudrate);
            this.tpLaserConnection.Controls.Add(lblLaserPort);
            this.tpLaserConnection.Controls.Add(this.cmbLaserPort);
            this.tpLaserConnection.Controls.Add(this.chkConnectLaser);
            this.tpLaserConnection.Location = new System.Drawing.Point(4, 22);
            this.tpLaserConnection.Name = "tpLaserConnection";
            this.tpLaserConnection.Padding = new System.Windows.Forms.Padding(3);
            this.tpLaserConnection.Size = new System.Drawing.Size(398, 143);
            this.tpLaserConnection.TabIndex = 1;
            this.tpLaserConnection.Text = "Laser";
            this.tpLaserConnection.UseVisualStyleBackColor = true;
            // 
            // cmbLaserHandshake
            // 
            this.cmbLaserHandshake.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbLaserHandshake.FormattingEnabled = true;
            this.cmbLaserHandshake.Location = new System.Drawing.Point(257, 41);
            this.cmbLaserHandshake.Name = "cmbLaserHandshake";
            this.cmbLaserHandshake.Size = new System.Drawing.Size(76, 21);
            this.cmbLaserHandshake.TabIndex = 49;
            // 
            // cmbLaserBaudrate
            // 
            this.cmbLaserBaudrate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbLaserBaudrate.FormattingEnabled = true;
            this.cmbLaserBaudrate.Location = new System.Drawing.Point(83, 79);
            this.cmbLaserBaudrate.Name = "cmbLaserBaudrate";
            this.cmbLaserBaudrate.Size = new System.Drawing.Size(85, 21);
            this.cmbLaserBaudrate.TabIndex = 45;
            // 
            // cmbLaserPort
            // 
            this.cmbLaserPort.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbLaserPort.FormattingEnabled = true;
            this.cmbLaserPort.Location = new System.Drawing.Point(83, 41);
            this.cmbLaserPort.Name = "cmbLaserPort";
            this.cmbLaserPort.Size = new System.Drawing.Size(85, 21);
            this.cmbLaserPort.TabIndex = 46;
            // 
            // chkConnectLaser
            // 
            this.chkConnectLaser.AutoSize = true;
            this.chkConnectLaser.Checked = true;
            this.chkConnectLaser.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkConnectLaser.Location = new System.Drawing.Point(19, 13);
            this.chkConnectLaser.Name = "chkConnectLaser";
            this.chkConnectLaser.Size = new System.Drawing.Size(103, 17);
            this.chkConnectLaser.TabIndex = 29;
            this.chkConnectLaser.Text = "Connect to laser";
            this.chkConnectLaser.UseVisualStyleBackColor = true;
            // 
            // tpNewLaserConnection
            // 
            this.tpNewLaserConnection.Controls.Add(this.grpBxLaser);
            this.tpNewLaserConnection.Controls.Add(this.grpBxMotion);
            this.tpNewLaserConnection.Controls.Add(label25);
            this.tpNewLaserConnection.Controls.Add(this.cmbNewLaserPort);
            this.tpNewLaserConnection.Controls.Add(this.chkConnectNewlaser);
            this.tpNewLaserConnection.Location = new System.Drawing.Point(4, 22);
            this.tpNewLaserConnection.Name = "tpNewLaserConnection";
            this.tpNewLaserConnection.Size = new System.Drawing.Size(398, 143);
            this.tpNewLaserConnection.TabIndex = 2;
            this.tpNewLaserConnection.Text = "New Laser";
            this.tpNewLaserConnection.UseVisualStyleBackColor = true;
            // 
            // grpBxLaser
            // 
            this.grpBxLaser.Controls.Add(this.numT);
            this.grpBxLaser.Controls.Add(this.numN);
            this.grpBxLaser.Controls.Add(this.label30);
            this.grpBxLaser.Controls.Add(this.label33);
            this.grpBxLaser.Controls.Add(this.numB);
            this.grpBxLaser.Controls.Add(this.numD);
            this.grpBxLaser.Controls.Add(this.label31);
            this.grpBxLaser.Controls.Add(this.label32);
            this.grpBxLaser.Location = new System.Drawing.Point(173, 34);
            this.grpBxLaser.Name = "grpBxLaser";
            this.grpBxLaser.Size = new System.Drawing.Size(180, 106);
            this.grpBxLaser.TabIndex = 53;
            this.grpBxLaser.TabStop = false;
            this.grpBxLaser.Text = "Laser Parameters";
            // 
            // numT
            // 
            this.numT.Location = new System.Drawing.Point(80, 84);
            this.numT.Name = "numT";
            this.numT.Size = new System.Drawing.Size(72, 20);
            this.numT.TabIndex = 14;
            // 
            // numN
            // 
            this.numN.Location = new System.Drawing.Point(81, 37);
            this.numN.Maximum = new decimal(new int[] {
            160000,
            0,
            0,
            0});
            this.numN.Name = "numN";
            this.numN.Size = new System.Drawing.Size(72, 20);
            this.numN.TabIndex = 10;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(34, 84);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(14, 13);
            this.label30.TabIndex = 13;
            this.label30.Text = "T";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(35, 15);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(15, 13);
            this.label33.TabIndex = 8;
            this.label33.Text = "D";
            // 
            // numB
            // 
            this.numB.DecimalPlaces = 1;
            this.numB.Location = new System.Drawing.Point(80, 60);
            this.numB.Name = "numB";
            this.numB.Size = new System.Drawing.Size(72, 20);
            this.numB.TabIndex = 12;
            // 
            // numD
            // 
            this.numD.Location = new System.Drawing.Point(81, 15);
            this.numD.Maximum = new decimal(new int[] {
            95,
            0,
            0,
            0});
            this.numD.Minimum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numD.Name = "numD";
            this.numD.Size = new System.Drawing.Size(72, 20);
            this.numD.TabIndex = 7;
            this.numD.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(34, 60);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(14, 13);
            this.label31.TabIndex = 11;
            this.label31.Text = "B";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(35, 37);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(15, 13);
            this.label32.TabIndex = 9;
            this.label32.Text = "N";
            // 
            // grpBxMotion
            // 
            this.grpBxMotion.Controls.Add(this.numV);
            this.grpBxMotion.Controls.Add(this.label29);
            this.grpBxMotion.Controls.Add(this.numE);
            this.grpBxMotion.Controls.Add(this.label27);
            this.grpBxMotion.Controls.Add(this.numS);
            this.grpBxMotion.Controls.Add(this.label23);
            this.grpBxMotion.Location = new System.Drawing.Point(3, 34);
            this.grpBxMotion.Name = "grpBxMotion";
            this.grpBxMotion.Size = new System.Drawing.Size(164, 106);
            this.grpBxMotion.TabIndex = 52;
            this.grpBxMotion.TabStop = false;
            this.grpBxMotion.Text = "Motion Parameters";
            // 
            // numV
            // 
            this.numV.Location = new System.Drawing.Point(61, 77);
            this.numV.Maximum = new decimal(new int[] {
            200,
            0,
            0,
            0});
            this.numV.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numV.Name = "numV";
            this.numV.Size = new System.Drawing.Size(72, 20);
            this.numV.TabIndex = 6;
            this.numV.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(15, 77);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(14, 13);
            this.label29.TabIndex = 5;
            this.label29.Text = "V";
            // 
            // numE
            // 
            this.numE.Location = new System.Drawing.Point(62, 46);
            this.numE.Maximum = new decimal(new int[] {
            160000,
            0,
            0,
            0});
            this.numE.Name = "numE";
            this.numE.Size = new System.Drawing.Size(72, 20);
            this.numE.TabIndex = 2;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(16, 46);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(14, 13);
            this.label27.TabIndex = 1;
            this.label27.Text = "E";
            // 
            // numS
            // 
            this.numS.Location = new System.Drawing.Point(62, 16);
            this.numS.Maximum = new decimal(new int[] {
            160000,
            0,
            0,
            0});
            this.numS.Name = "numS";
            this.numS.Size = new System.Drawing.Size(72, 20);
            this.numS.TabIndex = 0;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(16, 16);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(14, 13);
            this.label23.TabIndex = 0;
            this.label23.Text = "S";
            // 
            // cmbNewLaserPort
            // 
            this.cmbNewLaserPort.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbNewLaserPort.FormattingEnabled = true;
            this.cmbNewLaserPort.Items.AddRange(new object[] {
            "COM1",
            "COM2",
            "COM3",
            "COM4"});
            this.cmbNewLaserPort.Location = new System.Drawing.Point(166, 3);
            this.cmbNewLaserPort.Name = "cmbNewLaserPort";
            this.cmbNewLaserPort.Size = new System.Drawing.Size(74, 21);
            this.cmbNewLaserPort.TabIndex = 51;
            // 
            // chkConnectNewlaser
            // 
            this.chkConnectNewlaser.AutoSize = true;
            this.chkConnectNewlaser.Checked = true;
            this.chkConnectNewlaser.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkConnectNewlaser.Location = new System.Drawing.Point(8, 7);
            this.chkConnectNewlaser.Name = "chkConnectNewlaser";
            this.chkConnectNewlaser.Size = new System.Drawing.Size(128, 17);
            this.chkConnectNewlaser.TabIndex = 48;
            this.chkConnectNewlaser.Text = "Connect to New laser";
            this.chkConnectNewlaser.UseVisualStyleBackColor = true;
            // 
            // tpScanParameters
            // 
            this.tpScanParameters.Controls.Add(this.tabScanParameters);
            this.tpScanParameters.Location = new System.Drawing.Point(4, 22);
            this.tpScanParameters.Name = "tpScanParameters";
            this.tpScanParameters.Size = new System.Drawing.Size(436, 219);
            this.tpScanParameters.TabIndex = 1;
            this.tpScanParameters.Text = "Scan Parameters";
            this.tpScanParameters.UseVisualStyleBackColor = true;
            // 
            // tabScanParameters
            // 
            this.tabScanParameters.Controls.Add(tpScanTiming);
            this.tabScanParameters.Controls.Add(this.tpLaserDetails);
            this.tabScanParameters.Location = new System.Drawing.Point(3, 8);
            this.tabScanParameters.Name = "tabScanParameters";
            this.tabScanParameters.SelectedIndex = 0;
            this.tabScanParameters.Size = new System.Drawing.Size(430, 208);
            this.tabScanParameters.TabIndex = 2;
            // 
            // tpLaserDetails
            // 
            this.tpLaserDetails.Controls.Add(this.radPulseCount);
            this.tpLaserDetails.Controls.Add(this.numPulseCount);
            this.tpLaserDetails.Controls.Add(this.radRunTime);
            this.tpLaserDetails.Controls.Add(this.numRunTime);
            this.tpLaserDetails.Controls.Add(lblGatingFreq);
            this.tpLaserDetails.Controls.Add(this.numGatingFreq);
            this.tpLaserDetails.Controls.Add(label13);
            this.tpLaserDetails.Controls.Add(lblDutyCycle);
            this.tpLaserDetails.Controls.Add(this.numDutyCycle);
            this.tpLaserDetails.Controls.Add(label8);
            this.tpLaserDetails.Location = new System.Drawing.Point(4, 22);
            this.tpLaserDetails.Name = "tpLaserDetails";
            this.tpLaserDetails.Padding = new System.Windows.Forms.Padding(3);
            this.tpLaserDetails.Size = new System.Drawing.Size(422, 182);
            this.tpLaserDetails.TabIndex = 1;
            this.tpLaserDetails.Text = "Laser Details";
            this.tpLaserDetails.UseVisualStyleBackColor = true;
            // 
            // radPulseCount
            // 
            this.radPulseCount.AutoSize = true;
            this.radPulseCount.Checked = true;
            this.radPulseCount.Location = new System.Drawing.Point(22, 17);
            this.radPulseCount.Name = "radPulseCount";
            this.radPulseCount.Size = new System.Drawing.Size(82, 17);
            this.radPulseCount.TabIndex = 33;
            this.radPulseCount.TabStop = true;
            this.radPulseCount.Text = "Pulse Count";
            this.radPulseCount.UseVisualStyleBackColor = true;
            // 
            // numPulseCount
            // 
            this.numPulseCount.Location = new System.Drawing.Point(126, 17);
            this.numPulseCount.Maximum = new decimal(new int[] {
            99999,
            0,
            0,
            0});
            this.numPulseCount.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numPulseCount.Name = "numPulseCount";
            this.numPulseCount.Size = new System.Drawing.Size(81, 20);
            this.numPulseCount.TabIndex = 25;
            this.numPulseCount.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // radRunTime
            // 
            this.radRunTime.AutoSize = true;
            this.radRunTime.Location = new System.Drawing.Point(22, 43);
            this.radRunTime.Name = "radRunTime";
            this.radRunTime.Size = new System.Drawing.Size(93, 17);
            this.radRunTime.TabIndex = 32;
            this.radRunTime.Text = "Run Time (ms)";
            this.radRunTime.UseVisualStyleBackColor = true;
            // 
            // numRunTime
            // 
            this.numRunTime.Location = new System.Drawing.Point(126, 43);
            this.numRunTime.Maximum = new decimal(new int[] {
            32766,
            0,
            0,
            0});
            this.numRunTime.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numRunTime.Name = "numRunTime";
            this.numRunTime.Size = new System.Drawing.Size(81, 20);
            this.numRunTime.TabIndex = 24;
            this.numRunTime.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // numGatingFreq
            // 
            this.numGatingFreq.Location = new System.Drawing.Point(127, 73);
            this.numGatingFreq.Maximum = new decimal(new int[] {
            20000,
            0,
            0,
            0});
            this.numGatingFreq.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numGatingFreq.Name = "numGatingFreq";
            this.numGatingFreq.Size = new System.Drawing.Size(80, 20);
            this.numGatingFreq.TabIndex = 27;
            this.numGatingFreq.Value = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            // 
            // numDutyCycle
            // 
            this.numDutyCycle.DecimalPlaces = 1;
            this.numDutyCycle.Location = new System.Drawing.Point(127, 101);
            this.numDutyCycle.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numDutyCycle.Name = "numDutyCycle";
            this.numDutyCycle.Size = new System.Drawing.Size(80, 20);
            this.numDutyCycle.TabIndex = 30;
            this.numDutyCycle.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // tpAlgorithms
            // 
            this.tpAlgorithms.Controls.Add(this.lblIncorrectPwd);
            this.tpAlgorithms.Controls.Add(this.ScanDataGrid);
            this.tpAlgorithms.Controls.Add(this.label11);
            this.tpAlgorithms.Controls.Add(this.txtPwd);
            this.tpAlgorithms.Controls.Add(this.btnUnlock);
            this.tpAlgorithms.Controls.Add(this.btnAddPaint);
            this.tpAlgorithms.Location = new System.Drawing.Point(4, 22);
            this.tpAlgorithms.Name = "tpAlgorithms";
            this.tpAlgorithms.Size = new System.Drawing.Size(436, 219);
            this.tpAlgorithms.TabIndex = 6;
            this.tpAlgorithms.Text = "Algorithms";
            this.tpAlgorithms.UseVisualStyleBackColor = true;
            this.tpAlgorithms.Enter += new System.EventHandler(this.tpAlgorithms_Enter);
            // 
            // lblIncorrectPwd
            // 
            this.lblIncorrectPwd.AutoSize = true;
            this.lblIncorrectPwd.ForeColor = System.Drawing.Color.Red;
            this.lblIncorrectPwd.Location = new System.Drawing.Point(253, 166);
            this.lblIncorrectPwd.Name = "lblIncorrectPwd";
            this.lblIncorrectPwd.Size = new System.Drawing.Size(98, 13);
            this.lblIncorrectPwd.TabIndex = 5;
            this.lblIncorrectPwd.Text = "Incorrect Password";
            this.lblIncorrectPwd.Visible = false;
            // 
            // ScanDataGrid
            // 
            this.ScanDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ScanDataGrid.Location = new System.Drawing.Point(-4, 0);
            this.ScanDataGrid.Name = "ScanDataGrid";
            this.ScanDataGrid.Size = new System.Drawing.Size(437, 159);
            this.ScanDataGrid.TabIndex = 4;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(168, 190);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(81, 13);
            this.label11.TabIndex = 3;
            this.label11.Text = "Enter Password";
            // 
            // txtPwd
            // 
            this.txtPwd.Location = new System.Drawing.Point(252, 186);
            this.txtPwd.Name = "txtPwd";
            this.txtPwd.Size = new System.Drawing.Size(100, 20);
            this.txtPwd.TabIndex = 2;
            this.txtPwd.UseSystemPasswordChar = true;
            // 
            // btnUnlock
            // 
            this.btnUnlock.Location = new System.Drawing.Point(364, 185);
            this.btnUnlock.Name = "btnUnlock";
            this.btnUnlock.Size = new System.Drawing.Size(69, 23);
            this.btnUnlock.TabIndex = 1;
            this.btnUnlock.Text = "Unlock";
            this.btnUnlock.UseVisualStyleBackColor = true;
            this.btnUnlock.Click += new System.EventHandler(this.btnUnlock_Click);
            // 
            // btnAddPaint
            // 
            this.btnAddPaint.Location = new System.Drawing.Point(3, 190);
            this.btnAddPaint.Name = "btnAddPaint";
            this.btnAddPaint.Size = new System.Drawing.Size(75, 23);
            this.btnAddPaint.TabIndex = 0;
            this.btnAddPaint.Text = "Add Paint";
            this.btnAddPaint.UseVisualStyleBackColor = true;
            this.btnAddPaint.Click += new System.EventHandler(this.button1_Click);
            // 
            // tpDataAnalysis
            // 
            this.tpDataAnalysis.Controls.Add(this.tabDataAnalysis);
            this.tpDataAnalysis.Location = new System.Drawing.Point(4, 22);
            this.tpDataAnalysis.Name = "tpDataAnalysis";
            this.tpDataAnalysis.Size = new System.Drawing.Size(436, 219);
            this.tpDataAnalysis.TabIndex = 2;
            this.tpDataAnalysis.Text = "Data Analysis";
            this.tpDataAnalysis.UseVisualStyleBackColor = true;
            // 
            // tabDataAnalysis
            // 
            this.tabDataAnalysis.Controls.Add(this.tpDataProcessing);
            this.tabDataAnalysis.Location = new System.Drawing.Point(16, 16);
            this.tabDataAnalysis.Name = "tabDataAnalysis";
            this.tabDataAnalysis.SelectedIndex = 0;
            this.tabDataAnalysis.Size = new System.Drawing.Size(314, 187);
            this.tabDataAnalysis.TabIndex = 3;
            // 
            // tpDataProcessing
            // 
            this.tpDataProcessing.Controls.Add(lblTruncateFirst);
            this.tpDataProcessing.Controls.Add(this.numTruncateFirst);
            this.tpDataProcessing.Controls.Add(label20);
            this.tpDataProcessing.Controls.Add(lblTruncateLast);
            this.tpDataProcessing.Controls.Add(this.numTruncateLast);
            this.tpDataProcessing.Controls.Add(label22);
            this.tpDataProcessing.Controls.Add(this.chkFilterPeaks);
            this.tpDataProcessing.Controls.Add(lblPeakThreshold);
            this.tpDataProcessing.Controls.Add(this.numPeakThreshold);
            this.tpDataProcessing.Controls.Add(lblPeakHysteresis);
            this.tpDataProcessing.Controls.Add(this.numPeakHysteresis);
            this.tpDataProcessing.Controls.Add(lblMaxPeakWidth);
            this.tpDataProcessing.Controls.Add(this.numMaxPeakWidth);
            this.tpDataProcessing.Location = new System.Drawing.Point(4, 22);
            this.tpDataProcessing.Name = "tpDataProcessing";
            this.tpDataProcessing.Padding = new System.Windows.Forms.Padding(3);
            this.tpDataProcessing.Size = new System.Drawing.Size(306, 161);
            this.tpDataProcessing.TabIndex = 0;
            this.tpDataProcessing.Text = "Data Processing";
            this.tpDataProcessing.UseVisualStyleBackColor = true;
            // 
            // numTruncateFirst
            // 
            this.numTruncateFirst.Location = new System.Drawing.Point(101, 11);
            this.numTruncateFirst.Name = "numTruncateFirst";
            this.numTruncateFirst.Size = new System.Drawing.Size(58, 20);
            this.numTruncateFirst.TabIndex = 60;
            this.numTruncateFirst.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // numTruncateLast
            // 
            this.numTruncateLast.Location = new System.Drawing.Point(101, 31);
            this.numTruncateLast.Name = "numTruncateLast";
            this.numTruncateLast.Size = new System.Drawing.Size(58, 20);
            this.numTruncateLast.TabIndex = 63;
            this.numTruncateLast.Value = new decimal(new int[] {
            20,
            0,
            0,
            0});
            // 
            // chkFilterPeaks
            // 
            this.chkFilterPeaks.AutoSize = true;
            this.chkFilterPeaks.Checked = true;
            this.chkFilterPeaks.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkFilterPeaks.Location = new System.Drawing.Point(22, 66);
            this.chkFilterPeaks.Name = "chkFilterPeaks";
            this.chkFilterPeaks.Size = new System.Drawing.Size(81, 17);
            this.chkFilterPeaks.TabIndex = 58;
            this.chkFilterPeaks.Text = "Filter Peaks";
            this.chkFilterPeaks.UseVisualStyleBackColor = true;
            // 
            // numPeakThreshold
            // 
            this.numPeakThreshold.DecimalPlaces = 1;
            this.numPeakThreshold.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numPeakThreshold.Location = new System.Drawing.Point(156, 79);
            this.numPeakThreshold.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.numPeakThreshold.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numPeakThreshold.Name = "numPeakThreshold";
            this.numPeakThreshold.Size = new System.Drawing.Size(59, 20);
            this.numPeakThreshold.TabIndex = 57;
            this.numPeakThreshold.Value = new decimal(new int[] {
            40,
            0,
            0,
            0});
            // 
            // numPeakHysteresis
            // 
            this.numPeakHysteresis.DecimalPlaces = 1;
            this.numPeakHysteresis.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numPeakHysteresis.Location = new System.Drawing.Point(156, 101);
            this.numPeakHysteresis.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.numPeakHysteresis.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numPeakHysteresis.Name = "numPeakHysteresis";
            this.numPeakHysteresis.Size = new System.Drawing.Size(59, 20);
            this.numPeakHysteresis.TabIndex = 55;
            this.numPeakHysteresis.Value = new decimal(new int[] {
            20,
            0,
            0,
            0});
            // 
            // numMaxPeakWidth
            // 
            this.numMaxPeakWidth.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numMaxPeakWidth.Location = new System.Drawing.Point(156, 122);
            this.numMaxPeakWidth.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.numMaxPeakWidth.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numMaxPeakWidth.Name = "numMaxPeakWidth";
            this.numMaxPeakWidth.Size = new System.Drawing.Size(59, 20);
            this.numMaxPeakWidth.TabIndex = 56;
            this.numMaxPeakWidth.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // tpDataLocation
            // 
            this.tpDataLocation.Controls.Add(this.chkMultipleSegmentScan);
            this.tpDataLocation.Controls.Add(lblUnits);
            this.tpDataLocation.Controls.Add(this.cmbUnits);
            this.tpDataLocation.Controls.Add(label4);
            this.tpDataLocation.Controls.Add(lblScanAveNumber);
            this.tpDataLocation.Controls.Add(this.numMaxSegments);
            this.tpDataLocation.Controls.Add(this.numScanAveNumber);
            this.tpDataLocation.Controls.Add(lblDataLocation);
            this.tpDataLocation.Controls.Add(this.txtDataLocation);
            this.tpDataLocation.Controls.Add(this.txtFilenamePattern);
            this.tpDataLocation.Controls.Add(this.btnChooseDataLocation);
            this.tpDataLocation.Controls.Add(lblFilenamePattern);
            this.tpDataLocation.Location = new System.Drawing.Point(4, 22);
            this.tpDataLocation.Name = "tpDataLocation";
            this.tpDataLocation.Size = new System.Drawing.Size(436, 219);
            this.tpDataLocation.TabIndex = 3;
            this.tpDataLocation.Text = "Data Location";
            this.tpDataLocation.UseVisualStyleBackColor = true;
            // 
            // chkMultipleSegmentScan
            // 
            this.chkMultipleSegmentScan.AutoSize = true;
            this.chkMultipleSegmentScan.Location = new System.Drawing.Point(20, 146);
            this.chkMultipleSegmentScan.Name = "chkMultipleSegmentScan";
            this.chkMultipleSegmentScan.Size = new System.Drawing.Size(135, 17);
            this.chkMultipleSegmentScan.TabIndex = 76;
            this.chkMultipleSegmentScan.Text = "Multiple Segment Scan";
            this.chkMultipleSegmentScan.UseVisualStyleBackColor = true;
            // 
            // cmbUnits
            // 
            this.cmbUnits.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbUnits.FormattingEnabled = true;
            this.cmbUnits.Location = new System.Drawing.Point(114, 110);
            this.cmbUnits.Name = "cmbUnits";
            this.cmbUnits.Size = new System.Drawing.Size(109, 21);
            this.cmbUnits.TabIndex = 75;
            // 
            // numMaxSegments
            // 
            this.numMaxSegments.Location = new System.Drawing.Point(114, 168);
            this.numMaxSegments.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numMaxSegments.Name = "numMaxSegments";
            this.numMaxSegments.Size = new System.Drawing.Size(67, 20);
            this.numMaxSegments.TabIndex = 71;
            this.numMaxSegments.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
            // 
            // numScanAveNumber
            // 
            this.numScanAveNumber.Location = new System.Drawing.Point(164, 84);
            this.numScanAveNumber.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numScanAveNumber.Name = "numScanAveNumber";
            this.numScanAveNumber.Size = new System.Drawing.Size(67, 20);
            this.numScanAveNumber.TabIndex = 71;
            this.numScanAveNumber.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
            // 
            // txtDataLocation
            // 
            this.txtDataLocation.Location = new System.Drawing.Point(114, 20);
            this.txtDataLocation.Name = "txtDataLocation";
            this.txtDataLocation.ReadOnly = true;
            this.txtDataLocation.Size = new System.Drawing.Size(117, 20);
            this.txtDataLocation.TabIndex = 36;
            this.txtDataLocation.Text = "c:\\";
            // 
            // txtFilenamePattern
            // 
            this.txtFilenamePattern.Location = new System.Drawing.Point(114, 57);
            this.txtFilenamePattern.Name = "txtFilenamePattern";
            this.txtFilenamePattern.Size = new System.Drawing.Size(117, 20);
            this.txtFilenamePattern.TabIndex = 39;
            this.txtFilenamePattern.Text = "scan {nnn}.csv";
            // 
            // btnChooseDataLocation
            // 
            this.btnChooseDataLocation.Location = new System.Drawing.Point(238, 17);
            this.btnChooseDataLocation.Name = "btnChooseDataLocation";
            this.btnChooseDataLocation.Size = new System.Drawing.Size(57, 23);
            this.btnChooseDataLocation.TabIndex = 37;
            this.btnChooseDataLocation.Text = "Choose";
            this.btnChooseDataLocation.UseVisualStyleBackColor = true;
            // 
            // tpMisc
            // 
            this.tpMisc.Controls.Add(this.grpBxConversionX);
            this.tpMisc.Controls.Add(this.chkAppyLeftMiddleRight);
            this.tpMisc.Controls.Add(this.grpPits);
            this.tpMisc.Controls.Add(this.chkFilter);
            this.tpMisc.Controls.Add(this.grpFilteration);
            this.tpMisc.Controls.Add(this.nmLagTime);
            this.tpMisc.Controls.Add(this.label5);
            this.tpMisc.Controls.Add(this.btnChangePassword);
            this.tpMisc.Location = new System.Drawing.Point(4, 22);
            this.tpMisc.Name = "tpMisc";
            this.tpMisc.Padding = new System.Windows.Forms.Padding(3);
            this.tpMisc.Size = new System.Drawing.Size(436, 219);
            this.tpMisc.TabIndex = 5;
            this.tpMisc.Text = "Misc";
            this.tpMisc.UseVisualStyleBackColor = true;
            this.tpMisc.Click += new System.EventHandler(this.tpMisc_Click);
            // 
            // grpBxConversionX
            // 
            this.grpBxConversionX.Controls.Add(this.label19);
            this.grpBxConversionX.Controls.Add(this.label18);
            this.grpBxConversionX.Controls.Add(this.nmSensorFrequency);
            this.grpBxConversionX.Controls.Add(this.nmLineSpeed);
            this.grpBxConversionX.Controls.Add(this.label17);
            this.grpBxConversionX.Controls.Add(this.label16);
            this.grpBxConversionX.Location = new System.Drawing.Point(208, 106);
            this.grpBxConversionX.Name = "grpBxConversionX";
            this.grpBxConversionX.Size = new System.Drawing.Size(225, 95);
            this.grpBxConversionX.TabIndex = 7;
            this.grpBxConversionX.TabStop = false;
            this.grpBxConversionX.Text = "Conversion of xScale";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(188, 62);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(26, 13);
            this.label19.TabIndex = 4;
            this.label19.Text = "(Hz)";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(185, 26);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(42, 13);
            this.label18.TabIndex = 3;
            this.label18.Text = "(mm/m)";
            // 
            // nmSensorFrequency
            // 
            this.nmSensorFrequency.Location = new System.Drawing.Point(104, 59);
            this.nmSensorFrequency.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.nmSensorFrequency.Name = "nmSensorFrequency";
            this.nmSensorFrequency.Size = new System.Drawing.Size(81, 20);
            this.nmSensorFrequency.TabIndex = 3;
            this.nmSensorFrequency.Value = new decimal(new int[] {
            20000,
            0,
            0,
            0});
            // 
            // nmLineSpeed
            // 
            this.nmLineSpeed.Location = new System.Drawing.Point(104, 21);
            this.nmLineSpeed.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.nmLineSpeed.Name = "nmLineSpeed";
            this.nmLineSpeed.Size = new System.Drawing.Size(81, 20);
            this.nmLineSpeed.TabIndex = 2;
            this.nmLineSpeed.Value = new decimal(new int[] {
            63,
            0,
            0,
            0});
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(4, 59);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(93, 13);
            this.label17.TabIndex = 1;
            this.label17.Text = "Sensor Frequency";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(6, 21);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(61, 13);
            this.label16.TabIndex = 0;
            this.label16.Text = "Line Speed";
            // 
            // chkAppyLeftMiddleRight
            // 
            this.chkAppyLeftMiddleRight.AutoSize = true;
            this.chkAppyLeftMiddleRight.Location = new System.Drawing.Point(215, 17);
            this.chkAppyLeftMiddleRight.Name = "chkAppyLeftMiddleRight";
            this.chkAppyLeftMiddleRight.Size = new System.Drawing.Size(198, 17);
            this.chkAppyLeftMiddleRight.TabIndex = 6;
            this.chkAppyLeftMiddleRight.Text = "Apply Left, Middle and Right settings";
            this.chkAppyLeftMiddleRight.UseVisualStyleBackColor = true;
            this.chkAppyLeftMiddleRight.CheckedChanged += new System.EventHandler(this.chkAppyLeftMiddleRight_CheckedChanged);
            // 
            // grpPits
            // 
            this.grpPits.Controls.Add(this.nmIgnoreEdge);
            this.grpPits.Controls.Add(this.label15);
            this.grpPits.Controls.Add(this.label14);
            this.grpPits.Location = new System.Drawing.Point(208, 40);
            this.grpPits.Name = "grpPits";
            this.grpPits.Size = new System.Drawing.Size(222, 60);
            this.grpPits.TabIndex = 5;
            this.grpPits.TabStop = false;
            this.grpPits.Text = "Left Middle Right";
            // 
            // nmIgnoreEdge
            // 
            this.nmIgnoreEdge.Location = new System.Drawing.Point(121, 27);
            this.nmIgnoreEdge.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.nmIgnoreEdge.Name = "nmIgnoreEdge";
            this.nmIgnoreEdge.Size = new System.Drawing.Size(66, 20);
            this.nmIgnoreEdge.TabIndex = 8;
            this.nmIgnoreEdge.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.nmIgnoreEdge.ValueChanged += new System.EventHandler(this.nmIgnoreEdge_ValueChanged);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(187, 32);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(29, 13);
            this.label15.TabIndex = 2;
            this.label15.Text = "(mm)";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(1, 29);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(114, 13);
            this.label14.TabIndex = 0;
            this.label14.Text = "Ignore from each edge";
            // 
            // chkFilter
            // 
            this.chkFilter.AutoSize = true;
            this.chkFilter.Location = new System.Drawing.Point(9, 83);
            this.chkFilter.Name = "chkFilter";
            this.chkFilter.Size = new System.Drawing.Size(77, 17);
            this.chkFilter.TabIndex = 4;
            this.chkFilter.Text = "Apply Filter";
            this.chkFilter.UseVisualStyleBackColor = true;
            // 
            // grpFilteration
            // 
            this.grpFilteration.Controls.Add(this.nmDiffFilter);
            this.grpFilteration.Controls.Add(this.label10);
            this.grpFilteration.Location = new System.Drawing.Point(9, 113);
            this.grpFilteration.Name = "grpFilteration";
            this.grpFilteration.Size = new System.Drawing.Size(178, 88);
            this.grpFilteration.TabIndex = 3;
            this.grpFilteration.TabStop = false;
            this.grpFilteration.Text = "Filteration";
            this.grpFilteration.Enter += new System.EventHandler(this.grpFilteration_Enter);
            // 
            // nmDiffFilter
            // 
            this.nmDiffFilter.DecimalPlaces = 5;
            this.nmDiffFilter.Increment = new decimal(new int[] {
            1,
            0,
            0,
            262144});
            this.nmDiffFilter.Location = new System.Drawing.Point(78, 30);
            this.nmDiffFilter.Name = "nmDiffFilter";
            this.nmDiffFilter.Size = new System.Drawing.Size(80, 20);
            this.nmDiffFilter.TabIndex = 4;
            this.nmDiffFilter.Value = new decimal(new int[] {
            1,
            0,
            0,
            196608});
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 32);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(56, 13);
            this.label10.TabIndex = 4;
            this.label10.Text = "Difference";
            // 
            // nmLagTime
            // 
            this.nmLagTime.Location = new System.Drawing.Point(84, 48);
            this.nmLagTime.Name = "nmLagTime";
            this.nmLagTime.Size = new System.Drawing.Size(80, 20);
            this.nmLagTime.TabIndex = 2;
            this.nmLagTime.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 52);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(51, 13);
            this.label5.TabIndex = 1;
            this.label5.Text = "Lag Time";
            // 
            // btnChangePassword
            // 
            this.btnChangePassword.Location = new System.Drawing.Point(7, 11);
            this.btnChangePassword.Name = "btnChangePassword";
            this.btnChangePassword.Size = new System.Drawing.Size(158, 23);
            this.btnChangePassword.TabIndex = 0;
            this.btnChangePassword.Text = "Change Settings Password";
            this.btnChangePassword.UseVisualStyleBackColor = true;
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSave.Location = new System.Drawing.Point(12, 7);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(57, 27);
            this.btnSave.TabIndex = 23;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click_1);
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOK.Location = new System.Drawing.Point(342, 7);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(57, 27);
            this.btnOK.TabIndex = 24;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(405, 7);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(57, 27);
            this.btnCancel.TabIndex = 25;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // numA
            // 
            this.numA.Location = new System.Drawing.Point(104, 412);
            this.numA.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numA.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numA.Name = "numA";
            this.numA.Size = new System.Drawing.Size(72, 20);
            this.numA.TabIndex = 4;
            this.numA.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numA.Visible = false;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(58, 412);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(14, 13);
            this.label28.TabIndex = 3;
            this.label28.Text = "A";
            this.label28.Visible = false;
            // 
            // lblHeading
            // 
            this.lblHeading.AutoSize = true;
            this.lblHeading.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHeading.Location = new System.Drawing.Point(12, 9);
            this.lblHeading.Name = "lblHeading";
            this.lblHeading.Size = new System.Drawing.Size(176, 25);
            this.lblHeading.TabIndex = 1;
            this.lblHeading.Text = "Phaeton Settings";
            // 
            // outerContainer
            // 
            this.outerContainer.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.outerContainer.IsSplitterFixed = true;
            this.outerContainer.Location = new System.Drawing.Point(0, 0);
            this.outerContainer.MinimumSize = new System.Drawing.Size(211, 248);
            this.outerContainer.Name = "outerContainer";
            this.outerContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // outerContainer.Panel1
            // 
            this.outerContainer.Panel1.Controls.Add(this.lblHeading);
            // 
            // outerContainer.Panel2
            // 
            this.outerContainer.Panel2.Controls.Add(this.controlButtonsContainer);
            this.outerContainer.Size = new System.Drawing.Size(474, 353);
            this.outerContainer.SplitterDistance = 41;
            this.outerContainer.TabIndex = 0;
            this.outerContainer.TabStop = false;
            // 
            // tmrFade
            // 
            this.tmrFade.Interval = 20D;
            this.tmrFade.SynchronizingObject = this;
            this.tmrFade.Elapsed += new System.Timers.ElapsedEventHandler(this.tmrFade_Elapsed);
            // 
            // DFM_Simple_Settings_Form
            // 
            this.AcceptButton = this.btnOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(542, 463);
            this.Controls.Add(this.outerContainer);
            this.Controls.Add(this.numA);
            this.Controls.Add(this.label28);
            this.MaximizeBox = false;
            this.Name = "DFM_Simple_Settings_Form";
            this.Text = "Phaeton Settings";
            this.Load += new System.EventHandler(this.DFM_Simple_Settings_Form_Load);
            tpScanTiming.ResumeLayout(false);
            tpScanTiming.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numLaserStart)).EndInit();
            this.grpBxAuto.ResumeLayout(false);
            this.grpBxAuto.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nmEdgePoints)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmHigherRange)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmLowerRange)).EndInit();
            this.grpBxTimer.ResumeLayout(false);
            this.grpBxTimer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numDAQStart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numDAQStop)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numRepeatCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numRepeatInterval)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numMoveMotorAfter)).EndInit();
            this.controlButtonsContainer.Panel1.ResumeLayout(false);
            this.controlButtonsContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.controlButtonsContainer)).EndInit();
            this.controlButtonsContainer.ResumeLayout(false);
            this.tabSettings.ResumeLayout(false);
            this.tpConnection.ResumeLayout(false);
            this.tabConnection.ResumeLayout(false);
            this.tpSensorConnection.ResumeLayout(false);
            this.tpSensorConnection.PerformLayout();
            this.tpLaserConnection.ResumeLayout(false);
            this.tpLaserConnection.PerformLayout();
            this.tpNewLaserConnection.ResumeLayout(false);
            this.tpNewLaserConnection.PerformLayout();
            this.grpBxLaser.ResumeLayout(false);
            this.grpBxLaser.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numD)).EndInit();
            this.grpBxMotion.ResumeLayout(false);
            this.grpBxMotion.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numV)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numE)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numS)).EndInit();
            this.tpScanParameters.ResumeLayout(false);
            this.tabScanParameters.ResumeLayout(false);
            this.tpLaserDetails.ResumeLayout(false);
            this.tpLaserDetails.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numPulseCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numRunTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numGatingFreq)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numDutyCycle)).EndInit();
            this.tpAlgorithms.ResumeLayout(false);
            this.tpAlgorithms.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ScanDataGrid)).EndInit();
            this.tpDataAnalysis.ResumeLayout(false);
            this.tabDataAnalysis.ResumeLayout(false);
            this.tpDataProcessing.ResumeLayout(false);
            this.tpDataProcessing.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numTruncateFirst)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numTruncateLast)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPeakThreshold)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPeakHysteresis)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numMaxPeakWidth)).EndInit();
            this.tpDataLocation.ResumeLayout(false);
            this.tpDataLocation.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numMaxSegments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numScanAveNumber)).EndInit();
            this.tpMisc.ResumeLayout(false);
            this.tpMisc.PerformLayout();
            this.grpBxConversionX.ResumeLayout(false);
            this.grpBxConversionX.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nmSensorFrequency)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmLineSpeed)).EndInit();
            this.grpPits.ResumeLayout(false);
            this.grpPits.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nmIgnoreEdge)).EndInit();
            this.grpFilteration.ResumeLayout(false);
            this.grpFilteration.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nmDiffFilter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmLagTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numA)).EndInit();
            this.outerContainer.Panel1.ResumeLayout(false);
            this.outerContainer.Panel1.PerformLayout();
            this.outerContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.outerContainer)).EndInit();
            this.outerContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tmrFade)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        protected System.Windows.Forms.SplitContainer controlButtonsContainer;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label lblHeading;
        protected System.Windows.Forms.SplitContainer outerContainer;
        private System.Windows.Forms.FolderBrowserDialog folderBrowser;
        protected System.Windows.Forms.TabControl tabSettings;
        private System.Windows.Forms.TabPage tpConnection;
        private System.Windows.Forms.Button btnRefreshCOMPorts;
        private System.Windows.Forms.Button btnConnect;
        private System.Windows.Forms.TabControl tabConnection;
        private System.Windows.Forms.TabPage tpSensorConnection;
        private System.Windows.Forms.ComboBox cmbSensorBaudrate;
        private System.Windows.Forms.ComboBox cmbSensorPort;
        private System.Windows.Forms.ComboBox cmbSensorType;
        private System.Windows.Forms.ComboBox cmbSensorModel;
        private System.Windows.Forms.CheckBox chkConnectSensor;
        private System.Windows.Forms.TabPage tpLaserConnection;
        private System.Windows.Forms.ComboBox cmbLaserBaudrate;
        private System.Windows.Forms.ComboBox cmbLaserPort;
        private System.Windows.Forms.CheckBox chkConnectLaser;
        private System.Windows.Forms.TabPage tpScanParameters;
        private System.Windows.Forms.TabControl tabScanParameters;
        private System.Windows.Forms.NumericUpDown numDAQStart;
        private System.Windows.Forms.NumericUpDown numLaserStart;
        private System.Windows.Forms.NumericUpDown numDAQStop;
        private System.Windows.Forms.TabPage tpLaserDetails;
        private System.Windows.Forms.RadioButton radPulseCount;
        private System.Windows.Forms.NumericUpDown numPulseCount;
        private System.Windows.Forms.RadioButton radRunTime;
        private System.Windows.Forms.NumericUpDown numRunTime;
        private System.Windows.Forms.NumericUpDown numGatingFreq;
        private System.Windows.Forms.NumericUpDown numDutyCycle;
        private System.Windows.Forms.TabPage tpDataAnalysis;
        private System.Windows.Forms.TabControl tabDataAnalysis;
        private System.Windows.Forms.TabPage tpDataProcessing;
        private System.Windows.Forms.NumericUpDown numTruncateFirst;
        private System.Windows.Forms.NumericUpDown numTruncateLast;
        private System.Windows.Forms.CheckBox chkFilterPeaks;
        private System.Windows.Forms.NumericUpDown numPeakThreshold;
        private System.Windows.Forms.NumericUpDown numPeakHysteresis;
        private System.Windows.Forms.NumericUpDown numMaxPeakWidth;
        private System.Windows.Forms.TabPage tpDataLocation;
        private System.Windows.Forms.NumericUpDown numScanAveNumber;
        private System.Windows.Forms.TextBox txtFilenamePattern;
        private System.Windows.Forms.Button btnChooseDataLocation;
        private System.Windows.Forms.CheckBox chkRepeat;
        private System.Windows.Forms.NumericUpDown numRepeatCount;
        private System.Windows.Forms.NumericUpDown numRepeatInterval;
        private System.Windows.Forms.TabPage tpMisc;
        private System.Windows.Forms.Button btnChangePassword;
        private System.Windows.Forms.TextBox txtSensorAddress;
        private System.Windows.Forms.ComboBox cmbLaserHandshake;
        private System.Windows.Forms.Label lblSensorPort;
        private System.Windows.Forms.NumericUpDown numMoveMotorAfter;
        private System.Windows.Forms.TextBox txtMotorCommand;
        private System.Windows.Forms.ComboBox cmbUnits;
        private System.Windows.Forms.CheckBox chkMultipleSegmentScan;
        private System.Windows.Forms.NumericUpDown numMaxSegments;
        private System.Windows.Forms.NumericUpDown nmLagTime;
        private System.Windows.Forms.Label label5;
        public System.Windows.Forms.GroupBox grpFilteration;
        private System.Windows.Forms.Label label10;
        public System.Windows.Forms.CheckBox chkFilter;
        private System.Windows.Forms.TabPage tpAlgorithms;
        private System.Windows.Forms.Button btnAddPaint;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtPwd;
        private System.Windows.Forms.Button btnUnlock;
        public System.Windows.Forms.DataGridView ScanDataGrid;
        protected internal System.Timers.Timer tmrFade;
        private System.Windows.Forms.Label lblIncorrectPwd;
        private System.Windows.Forms.CheckBox chkAppyLeftMiddleRight;
        private System.Windows.Forms.GroupBox grpPits;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.GroupBox grpBxConversionX;
        private System.Windows.Forms.NumericUpDown nmSensorFrequency;
        private System.Windows.Forms.NumericUpDown nmLineSpeed;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.GroupBox grpBxTimer;
        private System.Windows.Forms.RadioButton rdBtnAutoDetect;
        private System.Windows.Forms.GroupBox grpBxAuto;
        private System.Windows.Forms.NumericUpDown nmEdgePoints;
        private System.Windows.Forms.NumericUpDown nmHigherRange;
        private System.Windows.Forms.NumericUpDown nmLowerRange;
        private System.Windows.Forms.RadioButton rdBtnTimer;
        public System.Windows.Forms.NumericUpDown nmDiffFilter;
        private System.Windows.Forms.NumericUpDown nmIgnoreEdge;
        private System.Windows.Forms.TabPage tpNewLaserConnection;
        private System.Windows.Forms.GroupBox grpBxLaser;
        private System.Windows.Forms.NumericUpDown numT;
        private System.Windows.Forms.NumericUpDown numN;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.NumericUpDown numB;
        private System.Windows.Forms.NumericUpDown numD;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.GroupBox grpBxMotion;
        private System.Windows.Forms.NumericUpDown numV;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.NumericUpDown numA;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.NumericUpDown numE;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.NumericUpDown numS;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.ComboBox cmbNewLaserPort;
        private System.Windows.Forms.CheckBox chkConnectNewlaser;
        public System.Windows.Forms.TextBox txtDataLocation;


    }
}