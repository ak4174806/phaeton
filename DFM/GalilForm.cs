﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace DFM
{
    public partial class GalilForm : BaseGalilForm
    {
        public GalilForm()
        {
            InitializeComponent();

            this.VisibleChanged += new EventHandler(delegate(object sender, EventArgs e)
            {
                if (this.Visible == true)
                {
                    refreshEdges();
                }
            });

            this.FormClosing += new FormClosingEventHandler(delegate(object sender, FormClosingEventArgs e)
            {
                e.Cancel = true;
                this.Hide();
            });

            m.FinishedSetup += new Motor.MotorTaskHandler(m_FinishedSetup);
        }

        void m_FinishedSetup(object sender, Motor.MotorTaskEventArgs e)
        {
            if (e.Success)
            {
                chkSkipEdgeScan.Enabled = true;
            }            
        }

        public override bool Connect(string address)
        {
            chkSkipEdgeScan.Enabled = false;
            bool result = base.Connect(address);
            if (result && m.IsSetup)
            {
                chkSkipEdgeScan.Enabled = true;
            }

            return result;
        }

        protected override bool refreshEdges()
        {
            bool result = base.refreshEdges();
            if (result && m.IsConnected && m.IsSetup)
            {
                chkSkipEdgeScan.Enabled = true;
            }
            else
            {
                chkSkipEdgeScan.Enabled = false;
                chkSkipEdgeScan.Checked = false;
            }

            return result;
        }

        public bool SkipSetup
        {
            get { return chkSkipEdgeScan.Enabled && chkSkipEdgeScan.Checked; }
        }
    }
}
