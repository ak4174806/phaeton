﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using System.IO;

using ZedGraph;
using Analysis;
using PaintAnalysis.Common;
using SensorTester;

namespace DFM
{
    public partial class ScanBrowserForm : BaseScanBrowserForm
    {
        protected GraphPane pane;
        DFMAnalysis analysis;

        public ScanBrowserForm()
        {
            InitializeComponent();
            this.FormClosing += new FormClosingEventHandler(ScanBrowserFrm_closing);
        }

        void ScanBrowserFrm_closing(object sender, FormClosingEventArgs e)
        {
            this.Hide();
            e.Cancel = true;
        }


        public delegate void DisplayedScanChangedHandler(string filename);
        public event DisplayedScanChangedHandler DisplayedScanChanged;

        protected void OnDisplayedScanChanged(string filename)
        {
            if (DisplayedScanChanged != null)
                DisplayedScanChanged(filename);
        }

        protected override bool DisplayScan(string path, string label)
        {
            if (!File.Exists(path))
            {
                return false;
            }

            OnDisplayedScanChanged(path);
            return true;
        }

        private void ScanBrowserForm_Load(object sender, EventArgs e)
        {

        }

        private void grpChooseScan_Enter(object sender, EventArgs e)
        {

        }

        private void splitContainer1_Panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnRefreshFiles_Click(object sender, EventArgs e)
        {

        }

        private void splitContainer1_Panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void grpChooseScan_Enter_1(object sender, EventArgs e)
        {

        }

        private void tmrFade_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            this.Opacity += 0.10;
            if (this.Opacity >= .95)
            {
                this.Opacity = 1;
                tmrFade.Enabled = false;
            }
        }
    }
}
