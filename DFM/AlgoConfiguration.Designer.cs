﻿namespace DFM
{
    partial class AlgoConfiguration
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.lblHeading = new System.Windows.Forms.Label();
            this.cmbAlgo = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtPaint = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.nmNoPoints4Avg = new System.Windows.Forms.NumericUpDown();
            this.nmNoPointsBwAvgs = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.nmDistanceBwPits = new System.Windows.Forms.NumericUpDown();
            this.lblDistanceBwPits = new System.Windows.Forms.Label();
            this.grpBxD = new System.Windows.Forms.GroupBox();
            this.NoPointsforYmaxAvg = new System.Windows.Forms.NumericUpDown();
            this.label19 = new System.Windows.Forms.Label();
            this.nmPercentage = new System.Windows.Forms.NumericUpDown();
            this.lblPercentage = new System.Windows.Forms.Label();
            this.nmThresholdValue = new System.Windows.Forms.NumericUpDown();
            this.lblThresholdValue = new System.Windows.Forms.Label();
            this.nmAvgPoints = new System.Windows.Forms.NumericUpDown();
            this.lblAvgPoints = new System.Windows.Forms.Label();
            this.nmHeight = new System.Windows.Forms.NumericUpDown();
            this.label9 = new System.Windows.Forms.Label();
            this.rdForPit1 = new System.Windows.Forms.RadioButton();
            this.rdForAllPits = new System.Windows.Forms.RadioButton();
            this.nmRange = new System.Windows.Forms.NumericUpDown();
            this.lblRange = new System.Windows.Forms.Label();
            this.nmFactor = new System.Windows.Forms.NumericUpDown();
            this.lblFactor = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.lblInvalidType = new System.Windows.Forms.Label();
            this.nmPointsInEachPit = new System.Windows.Forms.NumericUpDown();
            this.lblPointsInEachPit = new System.Windows.Forms.Label();
            this.nmCompressionFactor = new System.Windows.Forms.NumericUpDown();
            this.lblCompressionFactor = new System.Windows.Forms.Label();
            this.nmMinPitWidth = new System.Windows.Forms.NumericUpDown();
            this.lblMinPitWidth = new System.Windows.Forms.Label();
            this.nmSmoothingWidth = new System.Windows.Forms.NumericUpDown();
            this.lblSmoothingWidth = new System.Windows.Forms.Label();
            this.nmSubstrateRoghness = new System.Windows.Forms.NumericUpDown();
            this.lblSubstrateRoughness = new System.Windows.Forms.Label();
            this.chkHeightOptional = new System.Windows.Forms.CheckBox();
            this.grpBoxHeightOption = new System.Windows.Forms.GroupBox();
            this.nmAvgNum = new System.Windows.Forms.NumericUpDown();
            this.lblAveragingNum = new System.Windows.Forms.Label();
            this.nmOffsetValue = new System.Windows.Forms.NumericUpDown();
            this.lblOffsetValue = new System.Windows.Forms.Label();
            this.tmrFade = new System.Timers.Timer();
            this.grpbxSurfaceRoughness = new System.Windows.Forms.GroupBox();
            this.nmNoCountBackPts = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.nmNoPoints4Avg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmNoPointsBwAvgs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmDistanceBwPits)).BeginInit();
            this.grpBxD.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NoPointsforYmaxAvg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmPercentage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmThresholdValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmAvgPoints)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmHeight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmRange)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmFactor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmPointsInEachPit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmCompressionFactor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmMinPitWidth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmSmoothingWidth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmSubstrateRoghness)).BeginInit();
            this.grpBoxHeightOption.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nmAvgNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmOffsetValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tmrFade)).BeginInit();
            this.grpbxSurfaceRoughness.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nmNoCountBackPts)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(25, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Select Algorithm";
            // 
            // lblHeading
            // 
            this.lblHeading.AutoSize = true;
            this.lblHeading.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHeading.Location = new System.Drawing.Point(12, 9);
            this.lblHeading.Name = "lblHeading";
            this.lblHeading.Size = new System.Drawing.Size(236, 25);
            this.lblHeading.TabIndex = 3;
            this.lblHeading.Text = "Algorithm Configuration";
            // 
            // cmbAlgo
            // 
            this.cmbAlgo.FormattingEnabled = true;
            this.cmbAlgo.Items.AddRange(new object[] {
            "Algorithm1",
            "Algorithm2",
            "Algorithm3",
            "Plastisol Algorithm",
            "Surface Roughness"});
            this.cmbAlgo.Location = new System.Drawing.Point(189, 44);
            this.cmbAlgo.Name = "cmbAlgo";
            this.cmbAlgo.Size = new System.Drawing.Size(121, 21);
            this.cmbAlgo.TabIndex = 4;
            this.cmbAlgo.SelectedIndexChanged += new System.EventHandler(this.cmbAlgo_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(25, 74);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Paint Type";
            // 
            // txtPaint
            // 
            this.txtPaint.Location = new System.Drawing.Point(189, 74);
            this.txtPaint.Name = "txtPaint";
            this.txtPaint.Size = new System.Drawing.Size(120, 20);
            this.txtPaint.TabIndex = 6;
            this.txtPaint.TextChanged += new System.EventHandler(this.txtPaint_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(5, 47);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(134, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "No. of Points for Averaging";
            // 
            // nmNoPoints4Avg
            // 
            this.nmNoPoints4Avg.Location = new System.Drawing.Point(163, 45);
            this.nmNoPoints4Avg.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.nmNoPoints4Avg.Name = "nmNoPoints4Avg";
            this.nmNoPoints4Avg.Size = new System.Drawing.Size(121, 20);
            this.nmNoPoints4Avg.TabIndex = 8;
            this.nmNoPoints4Avg.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // nmNoPointsBwAvgs
            // 
            this.nmNoPointsBwAvgs.Location = new System.Drawing.Point(163, 71);
            this.nmNoPointsBwAvgs.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.nmNoPointsBwAvgs.Name = "nmNoPointsBwAvgs";
            this.nmNoPointsBwAvgs.Size = new System.Drawing.Size(121, 20);
            this.nmNoPointsBwAvgs.TabIndex = 10;
            this.nmNoPointsBwAvgs.Value = new decimal(new int[] {
            47,
            0,
            0,
            0});
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(5, 73);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(117, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "No. of Points b/w Avgs";
            // 
            // nmDistanceBwPits
            // 
            this.nmDistanceBwPits.Location = new System.Drawing.Point(189, 227);
            this.nmDistanceBwPits.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.nmDistanceBwPits.Name = "nmDistanceBwPits";
            this.nmDistanceBwPits.Size = new System.Drawing.Size(121, 20);
            this.nmDistanceBwPits.TabIndex = 12;
            this.nmDistanceBwPits.Value = new decimal(new int[] {
            200,
            0,
            0,
            0});
            // 
            // lblDistanceBwPits
            // 
            this.lblDistanceBwPits.AutoSize = true;
            this.lblDistanceBwPits.Location = new System.Drawing.Point(28, 227);
            this.lblDistanceBwPits.Name = "lblDistanceBwPits";
            this.lblDistanceBwPits.Size = new System.Drawing.Size(91, 13);
            this.lblDistanceBwPits.TabIndex = 11;
            this.lblDistanceBwPits.Text = "Distance b/w Pits";
            // 
            // grpBxD
            // 
            this.grpBxD.Controls.Add(this.nmNoCountBackPts);
            this.grpBxD.Controls.Add(this.label5);
            this.grpBxD.Controls.Add(this.NoPointsforYmaxAvg);
            this.grpBxD.Controls.Add(this.label19);
            this.grpBxD.Controls.Add(this.nmNoPoints4Avg);
            this.grpBxD.Controls.Add(this.nmNoPointsBwAvgs);
            this.grpBxD.Controls.Add(this.label3);
            this.grpBxD.Controls.Add(this.label4);
            this.grpBxD.Location = new System.Drawing.Point(25, 99);
            this.grpBxD.Name = "grpBxD";
            this.grpBxD.Size = new System.Drawing.Size(362, 122);
            this.grpBxD.TabIndex = 13;
            this.grpBxD.TabStop = false;
            this.grpBxD.Text = "For Column D";
            this.grpBxD.Enter += new System.EventHandler(this.grpBxD_Enter);
            // 
            // NoPointsforYmaxAvg
            // 
            this.NoPointsforYmaxAvg.Location = new System.Drawing.Point(163, 18);
            this.NoPointsforYmaxAvg.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.NoPointsforYmaxAvg.Name = "NoPointsforYmaxAvg";
            this.NoPointsforYmaxAvg.Size = new System.Drawing.Size(121, 20);
            this.NoPointsforYmaxAvg.TabIndex = 12;
            this.NoPointsforYmaxAvg.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(5, 21);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(137, 13);
            this.label19.TabIndex = 11;
            this.label19.Text = "No. of Points For Ymax Avg";
            this.label19.Click += new System.EventHandler(this.label19_Click);
            // 
            // nmPercentage
            // 
            this.nmPercentage.Location = new System.Drawing.Point(188, 254);
            this.nmPercentage.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.nmPercentage.Name = "nmPercentage";
            this.nmPercentage.Size = new System.Drawing.Size(122, 20);
            this.nmPercentage.TabIndex = 15;
            this.nmPercentage.Value = new decimal(new int[] {
            20,
            0,
            0,
            0});
            // 
            // lblPercentage
            // 
            this.lblPercentage.AutoSize = true;
            this.lblPercentage.Location = new System.Drawing.Point(28, 254);
            this.lblPercentage.Name = "lblPercentage";
            this.lblPercentage.Size = new System.Drawing.Size(62, 13);
            this.lblPercentage.TabIndex = 14;
            this.lblPercentage.Text = "Percentage";
            // 
            // nmThresholdValue
            // 
            this.nmThresholdValue.DecimalPlaces = 5;
            this.nmThresholdValue.Location = new System.Drawing.Point(188, 282);
            this.nmThresholdValue.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.nmThresholdValue.Name = "nmThresholdValue";
            this.nmThresholdValue.Size = new System.Drawing.Size(121, 20);
            this.nmThresholdValue.TabIndex = 17;
            this.nmThresholdValue.Value = new decimal(new int[] {
            15,
            0,
            0,
            262144});
            // 
            // lblThresholdValue
            // 
            this.lblThresholdValue.AutoSize = true;
            this.lblThresholdValue.Location = new System.Drawing.Point(27, 282);
            this.lblThresholdValue.Name = "lblThresholdValue";
            this.lblThresholdValue.Size = new System.Drawing.Size(84, 13);
            this.lblThresholdValue.TabIndex = 16;
            this.lblThresholdValue.Text = "Threshold Value";
            // 
            // nmAvgPoints
            // 
            this.nmAvgPoints.Location = new System.Drawing.Point(188, 308);
            this.nmAvgPoints.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.nmAvgPoints.Name = "nmAvgPoints";
            this.nmAvgPoints.Size = new System.Drawing.Size(122, 20);
            this.nmAvgPoints.TabIndex = 19;
            this.nmAvgPoints.Value = new decimal(new int[] {
            20,
            0,
            0,
            0});
            // 
            // lblAvgPoints
            // 
            this.lblAvgPoints.AutoSize = true;
            this.lblAvgPoints.Location = new System.Drawing.Point(28, 308);
            this.lblAvgPoints.Name = "lblAvgPoints";
            this.lblAvgPoints.Size = new System.Drawing.Size(79, 13);
            this.lblAvgPoints.TabIndex = 18;
            this.lblAvgPoints.Text = "Average Points";
            this.lblAvgPoints.Click += new System.EventHandler(this.label8_Click);
            // 
            // nmHeight
            // 
            this.nmHeight.DecimalPlaces = 4;
            this.nmHeight.Location = new System.Drawing.Point(164, 15);
            this.nmHeight.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.nmHeight.Name = "nmHeight";
            this.nmHeight.Size = new System.Drawing.Size(101, 20);
            this.nmHeight.TabIndex = 21;
            this.nmHeight.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(20, 22);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(38, 13);
            this.label9.TabIndex = 20;
            this.label9.Text = "Height";
            // 
            // rdForPit1
            // 
            this.rdForPit1.AutoSize = true;
            this.rdForPit1.Location = new System.Drawing.Point(277, 12);
            this.rdForPit1.Name = "rdForPit1";
            this.rdForPit1.Size = new System.Drawing.Size(85, 17);
            this.rdForPit1.TabIndex = 22;
            this.rdForPit1.Text = "Only For Pit1";
            this.rdForPit1.UseVisualStyleBackColor = true;
            // 
            // rdForAllPits
            // 
            this.rdForAllPits.AutoSize = true;
            this.rdForAllPits.Checked = true;
            this.rdForAllPits.Location = new System.Drawing.Point(277, 29);
            this.rdForAllPits.Name = "rdForAllPits";
            this.rdForAllPits.Size = new System.Drawing.Size(74, 17);
            this.rdForAllPits.TabIndex = 23;
            this.rdForAllPits.TabStop = true;
            this.rdForAllPits.Text = "For All Pits";
            this.rdForAllPits.UseVisualStyleBackColor = true;
            // 
            // nmRange
            // 
            this.nmRange.Location = new System.Drawing.Point(188, 393);
            this.nmRange.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.nmRange.Name = "nmRange";
            this.nmRange.Size = new System.Drawing.Size(122, 20);
            this.nmRange.TabIndex = 25;
            this.nmRange.Value = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.nmRange.ValueChanged += new System.EventHandler(this.nmRange_ValueChanged);
            // 
            // lblRange
            // 
            this.lblRange.AutoSize = true;
            this.lblRange.Location = new System.Drawing.Point(31, 395);
            this.lblRange.Name = "lblRange";
            this.lblRange.Size = new System.Drawing.Size(39, 13);
            this.lblRange.TabIndex = 24;
            this.lblRange.Text = "Range";
            // 
            // nmFactor
            // 
            this.nmFactor.DecimalPlaces = 4;
            this.nmFactor.Location = new System.Drawing.Point(189, 421);
            this.nmFactor.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.nmFactor.Name = "nmFactor";
            this.nmFactor.Size = new System.Drawing.Size(121, 20);
            this.nmFactor.TabIndex = 27;
            this.nmFactor.Value = new decimal(new int[] {
            1,
            0,
            0,
            196608});
            // 
            // lblFactor
            // 
            this.lblFactor.AutoSize = true;
            this.lblFactor.Location = new System.Drawing.Point(31, 421);
            this.lblFactor.Name = "lblFactor";
            this.lblFactor.Size = new System.Drawing.Size(37, 13);
            this.lblFactor.TabIndex = 26;
            this.lblFactor.Text = "Factor";
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(25, 692);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 28;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(312, 692);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 29;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // lblInvalidType
            // 
            this.lblInvalidType.AutoSize = true;
            this.lblInvalidType.Location = new System.Drawing.Point(315, 81);
            this.lblInvalidType.Name = "lblInvalidType";
            this.lblInvalidType.Size = new System.Drawing.Size(92, 13);
            this.lblInvalidType.TabIndex = 30;
            this.lblInvalidType.Text = "Invalid Paint Type";
            this.lblInvalidType.Visible = false;
            // 
            // nmPointsInEachPit
            // 
            this.nmPointsInEachPit.Location = new System.Drawing.Point(188, 564);
            this.nmPointsInEachPit.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.nmPointsInEachPit.Name = "nmPointsInEachPit";
            this.nmPointsInEachPit.Size = new System.Drawing.Size(121, 20);
            this.nmPointsInEachPit.TabIndex = 40;
            this.nmPointsInEachPit.Value = new decimal(new int[] {
            4,
            0,
            0,
            0});
            // 
            // lblPointsInEachPit
            // 
            this.lblPointsInEachPit.AutoSize = true;
            this.lblPointsInEachPit.Location = new System.Drawing.Point(31, 564);
            this.lblPointsInEachPit.Name = "lblPointsInEachPit";
            this.lblPointsInEachPit.Size = new System.Drawing.Size(90, 13);
            this.lblPointsInEachPit.TabIndex = 39;
            this.lblPointsInEachPit.Text = "Points in Each Pit";
            // 
            // nmCompressionFactor
            // 
            this.nmCompressionFactor.DecimalPlaces = 5;
            this.nmCompressionFactor.Location = new System.Drawing.Point(188, 535);
            this.nmCompressionFactor.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.nmCompressionFactor.Name = "nmCompressionFactor";
            this.nmCompressionFactor.Size = new System.Drawing.Size(121, 20);
            this.nmCompressionFactor.TabIndex = 38;
            this.nmCompressionFactor.Value = new decimal(new int[] {
            100,
            0,
            0,
            196608});
            // 
            // lblCompressionFactor
            // 
            this.lblCompressionFactor.AutoSize = true;
            this.lblCompressionFactor.Location = new System.Drawing.Point(31, 535);
            this.lblCompressionFactor.Name = "lblCompressionFactor";
            this.lblCompressionFactor.Size = new System.Drawing.Size(100, 13);
            this.lblCompressionFactor.TabIndex = 37;
            this.lblCompressionFactor.Text = "Compression Factor";
            // 
            // nmMinPitWidth
            // 
            this.nmMinPitWidth.Location = new System.Drawing.Point(188, 505);
            this.nmMinPitWidth.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.nmMinPitWidth.Name = "nmMinPitWidth";
            this.nmMinPitWidth.Size = new System.Drawing.Size(121, 20);
            this.nmMinPitWidth.TabIndex = 36;
            this.nmMinPitWidth.Value = new decimal(new int[] {
            15,
            0,
            0,
            0});
            // 
            // lblMinPitWidth
            // 
            this.lblMinPitWidth.AutoSize = true;
            this.lblMinPitWidth.Location = new System.Drawing.Point(31, 505);
            this.lblMinPitWidth.Name = "lblMinPitWidth";
            this.lblMinPitWidth.Size = new System.Drawing.Size(70, 13);
            this.lblMinPitWidth.TabIndex = 35;
            this.lblMinPitWidth.Text = "Min Pit Width";
            // 
            // nmSmoothingWidth
            // 
            this.nmSmoothingWidth.Location = new System.Drawing.Point(189, 477);
            this.nmSmoothingWidth.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.nmSmoothingWidth.Name = "nmSmoothingWidth";
            this.nmSmoothingWidth.Size = new System.Drawing.Size(121, 20);
            this.nmSmoothingWidth.TabIndex = 34;
            this.nmSmoothingWidth.Value = new decimal(new int[] {
            65,
            0,
            0,
            0});
            this.nmSmoothingWidth.ValueChanged += new System.EventHandler(this.numericUpDown4_ValueChanged);
            // 
            // lblSmoothingWidth
            // 
            this.lblSmoothingWidth.AutoSize = true;
            this.lblSmoothingWidth.Location = new System.Drawing.Point(31, 479);
            this.lblSmoothingWidth.Name = "lblSmoothingWidth";
            this.lblSmoothingWidth.Size = new System.Drawing.Size(88, 13);
            this.lblSmoothingWidth.TabIndex = 33;
            this.lblSmoothingWidth.Text = "Smoothing Width";
            // 
            // nmSubstrateRoghness
            // 
            this.nmSubstrateRoghness.DecimalPlaces = 4;
            this.nmSubstrateRoghness.Location = new System.Drawing.Point(188, 449);
            this.nmSubstrateRoghness.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.nmSubstrateRoghness.Name = "nmSubstrateRoghness";
            this.nmSubstrateRoghness.Size = new System.Drawing.Size(122, 20);
            this.nmSubstrateRoghness.TabIndex = 32;
            // 
            // lblSubstrateRoughness
            // 
            this.lblSubstrateRoughness.AutoSize = true;
            this.lblSubstrateRoughness.Location = new System.Drawing.Point(31, 449);
            this.lblSubstrateRoughness.Name = "lblSubstrateRoughness";
            this.lblSubstrateRoughness.Size = new System.Drawing.Size(109, 13);
            this.lblSubstrateRoughness.TabIndex = 31;
            this.lblSubstrateRoughness.Text = "Substrate Roughness";
            // 
            // chkHeightOptional
            // 
            this.chkHeightOptional.AutoSize = true;
            this.chkHeightOptional.Location = new System.Drawing.Point(6, 351);
            this.chkHeightOptional.Name = "chkHeightOptional";
            this.chkHeightOptional.Size = new System.Drawing.Size(15, 14);
            this.chkHeightOptional.TabIndex = 41;
            this.chkHeightOptional.UseVisualStyleBackColor = true;
            this.chkHeightOptional.CheckedChanged += new System.EventHandler(this.chkHeightOptional_CheckedChanged);
            // 
            // grpBoxHeightOption
            // 
            this.grpBoxHeightOption.Controls.Add(this.nmHeight);
            this.grpBoxHeightOption.Controls.Add(this.label9);
            this.grpBoxHeightOption.Controls.Add(this.rdForPit1);
            this.grpBoxHeightOption.Controls.Add(this.rdForAllPits);
            this.grpBoxHeightOption.Location = new System.Drawing.Point(25, 333);
            this.grpBoxHeightOption.Name = "grpBoxHeightOption";
            this.grpBoxHeightOption.Size = new System.Drawing.Size(362, 52);
            this.grpBoxHeightOption.TabIndex = 42;
            this.grpBoxHeightOption.TabStop = false;
            this.grpBoxHeightOption.Text = "Optional";
            // 
            // nmAvgNum
            // 
            this.nmAvgNum.Location = new System.Drawing.Point(163, 53);
            this.nmAvgNum.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.nmAvgNum.Name = "nmAvgNum";
            this.nmAvgNum.Size = new System.Drawing.Size(121, 20);
            this.nmAvgNum.TabIndex = 46;
            this.nmAvgNum.Value = new decimal(new int[] {
            20,
            0,
            0,
            0});
            // 
            // lblAveragingNum
            // 
            this.lblAveragingNum.AutoSize = true;
            this.lblAveragingNum.Location = new System.Drawing.Point(6, 53);
            this.lblAveragingNum.Name = "lblAveragingNum";
            this.lblAveragingNum.Size = new System.Drawing.Size(95, 13);
            this.lblAveragingNum.TabIndex = 45;
            this.lblAveragingNum.Text = "Averaging Number";
            // 
            // nmOffsetValue
            // 
            this.nmOffsetValue.DecimalPlaces = 5;
            this.nmOffsetValue.Location = new System.Drawing.Point(163, 21);
            this.nmOffsetValue.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.nmOffsetValue.Name = "nmOffsetValue";
            this.nmOffsetValue.Size = new System.Drawing.Size(121, 20);
            this.nmOffsetValue.TabIndex = 44;
            this.nmOffsetValue.Value = new decimal(new int[] {
            5,
            0,
            0,
            196608});
            // 
            // lblOffsetValue
            // 
            this.lblOffsetValue.AutoSize = true;
            this.lblOffsetValue.Location = new System.Drawing.Point(6, 23);
            this.lblOffsetValue.Name = "lblOffsetValue";
            this.lblOffsetValue.Size = new System.Drawing.Size(65, 13);
            this.lblOffsetValue.TabIndex = 43;
            this.lblOffsetValue.Text = "Offset Value";
            // 
            // tmrFade
            // 
            this.tmrFade.Interval = 20D;
            this.tmrFade.SynchronizingObject = this;
            this.tmrFade.Elapsed += new System.Timers.ElapsedEventHandler(this.tmrFade_Elapsed);
            // 
            // grpbxSurfaceRoughness
            // 
            this.grpbxSurfaceRoughness.Controls.Add(this.lblOffsetValue);
            this.grpbxSurfaceRoughness.Controls.Add(this.lblAveragingNum);
            this.grpbxSurfaceRoughness.Controls.Add(this.nmAvgNum);
            this.grpbxSurfaceRoughness.Controls.Add(this.nmOffsetValue);
            this.grpbxSurfaceRoughness.Location = new System.Drawing.Point(25, 594);
            this.grpbxSurfaceRoughness.Name = "grpbxSurfaceRoughness";
            this.grpbxSurfaceRoughness.Size = new System.Drawing.Size(362, 85);
            this.grpbxSurfaceRoughness.TabIndex = 47;
            this.grpbxSurfaceRoughness.TabStop = false;
            this.grpbxSurfaceRoughness.Text = "For Surface Roughness";
            // 
            // nmNoCountBackPts
            // 
            this.nmNoCountBackPts.Location = new System.Drawing.Point(164, 97);
            this.nmNoCountBackPts.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.nmNoCountBackPts.Name = "nmNoCountBackPts";
            this.nmNoCountBackPts.Size = new System.Drawing.Size(121, 20);
            this.nmNoCountBackPts.TabIndex = 14;
            this.nmNoCountBackPts.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 99);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(127, 13);
            this.label5.TabIndex = 13;
            this.label5.Text = "No. of Count Back Points";
            // 
            // AlgoConfiguration
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(439, 721);
            this.Controls.Add(this.grpbxSurfaceRoughness);
            this.Controls.Add(this.grpBoxHeightOption);
            this.Controls.Add(this.chkHeightOptional);
            this.Controls.Add(this.nmPointsInEachPit);
            this.Controls.Add(this.lblPointsInEachPit);
            this.Controls.Add(this.nmCompressionFactor);
            this.Controls.Add(this.lblCompressionFactor);
            this.Controls.Add(this.nmMinPitWidth);
            this.Controls.Add(this.lblMinPitWidth);
            this.Controls.Add(this.nmSmoothingWidth);
            this.Controls.Add(this.lblSmoothingWidth);
            this.Controls.Add(this.nmSubstrateRoghness);
            this.Controls.Add(this.lblSubstrateRoughness);
            this.Controls.Add(this.lblInvalidType);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.nmFactor);
            this.Controls.Add(this.lblFactor);
            this.Controls.Add(this.nmRange);
            this.Controls.Add(this.lblRange);
            this.Controls.Add(this.nmAvgPoints);
            this.Controls.Add(this.lblAvgPoints);
            this.Controls.Add(this.nmThresholdValue);
            this.Controls.Add(this.lblThresholdValue);
            this.Controls.Add(this.nmPercentage);
            this.Controls.Add(this.lblPercentage);
            this.Controls.Add(this.grpBxD);
            this.Controls.Add(this.nmDistanceBwPits);
            this.Controls.Add(this.lblDistanceBwPits);
            this.Controls.Add(this.txtPaint);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cmbAlgo);
            this.Controls.Add(this.lblHeading);
            this.Controls.Add(this.label1);
            this.Name = "AlgoConfiguration";
            this.Text = "Algorithm Configuration";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.AlgoConfiguration_Load);
            ((System.ComponentModel.ISupportInitialize)(this.nmNoPoints4Avg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmNoPointsBwAvgs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmDistanceBwPits)).EndInit();
            this.grpBxD.ResumeLayout(false);
            this.grpBxD.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NoPointsforYmaxAvg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmPercentage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmThresholdValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmAvgPoints)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmHeight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmRange)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmFactor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmPointsInEachPit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmCompressionFactor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmMinPitWidth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmSmoothingWidth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmSubstrateRoghness)).EndInit();
            this.grpBoxHeightOption.ResumeLayout(false);
            this.grpBoxHeightOption.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nmAvgNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmOffsetValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tmrFade)).EndInit();
            this.grpbxSurfaceRoughness.ResumeLayout(false);
            this.grpbxSurfaceRoughness.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nmNoCountBackPts)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.Label label1;
        public System.Windows.Forms.Label lblHeading;
        public System.Windows.Forms.ComboBox cmbAlgo;
        public System.Windows.Forms.Label label2;
        public System.Windows.Forms.TextBox txtPaint;
        public System.Windows.Forms.Label label3;
        public System.Windows.Forms.NumericUpDown nmNoPoints4Avg;
        public System.Windows.Forms.NumericUpDown nmNoPointsBwAvgs;
        public System.Windows.Forms.Label label4;
        public System.Windows.Forms.NumericUpDown nmDistanceBwPits;
        public System.Windows.Forms.Label lblDistanceBwPits;
        public System.Windows.Forms.GroupBox grpBxD;
        public System.Windows.Forms.NumericUpDown nmPercentage;
        public System.Windows.Forms.Label lblPercentage;
        public System.Windows.Forms.NumericUpDown nmThresholdValue;
        public System.Windows.Forms.Label lblThresholdValue;
        public System.Windows.Forms.NumericUpDown nmAvgPoints;
        public System.Windows.Forms.Label lblAvgPoints;
        public System.Windows.Forms.NumericUpDown nmHeight;
        public System.Windows.Forms.Label label9;
        public System.Windows.Forms.RadioButton rdForPit1;
        public System.Windows.Forms.RadioButton rdForAllPits;
        public System.Windows.Forms.NumericUpDown nmRange;
        public System.Windows.Forms.Label lblRange;
        public System.Windows.Forms.NumericUpDown nmFactor;
        public System.Windows.Forms.Label lblFactor;
        public System.Windows.Forms.Button btnSave;
        public System.Windows.Forms.Button btnClose;
        public System.Windows.Forms.Label lblInvalidType;
        public System.Windows.Forms.NumericUpDown nmPointsInEachPit;
        public System.Windows.Forms.Label lblPointsInEachPit;
        public System.Windows.Forms.NumericUpDown nmCompressionFactor;
        public System.Windows.Forms.Label lblCompressionFactor;
        public System.Windows.Forms.NumericUpDown nmMinPitWidth;
        public System.Windows.Forms.Label lblMinPitWidth;
        public System.Windows.Forms.NumericUpDown nmSmoothingWidth;
        public System.Windows.Forms.Label lblSmoothingWidth;
        public System.Windows.Forms.NumericUpDown nmSubstrateRoghness;
        public System.Windows.Forms.Label lblSubstrateRoughness;
        public System.Windows.Forms.CheckBox chkHeightOptional;
        public System.Windows.Forms.GroupBox grpBoxHeightOption;
        public System.Windows.Forms.NumericUpDown nmAvgNum;
        public System.Windows.Forms.Label lblAveragingNum;
        public System.Windows.Forms.NumericUpDown nmOffsetValue;
        public System.Windows.Forms.Label lblOffsetValue;
        protected internal System.Timers.Timer tmrFade;
        public System.Windows.Forms.NumericUpDown NoPointsforYmaxAvg;
        public System.Windows.Forms.Label label19;
        private System.Windows.Forms.GroupBox grpbxSurfaceRoughness;
        public System.Windows.Forms.NumericUpDown nmNoCountBackPts;
        public System.Windows.Forms.Label label5;
    }
}