﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DFM.Motor
{
    public interface IPollMotor
    {
        event PollMotorStatusChangedHandler StatusChanged;
        event PollMotorHomedChangedHandler HomedChanged;
        event PollMotorFaultChangedHandler FaultChanged;
        event PollMotorErrorHandler PollMotorError;
        event PollMotorDataChangedHandler PollDataChanged;

        StatusWord Status { get; }        
        HomedWord Homed { get; }
        FaultWord Fault { get; }
        int TargetA { get; }
        int LeftEdge { get; }
        int RightEdge { get; }
        bool IsError { get; }
        Exception Error { get; }
        bool RunPoll { get; set; }

        void ClearError();
    }

    public delegate void PollMotorStatusChangedHandler(object sender, StatusChangedEventArgs e);
    public delegate void PollMotorHomedChangedHandler(object sender, HomedChangedEventArgs e);
    public delegate void PollMotorFaultChangedHandler(object sender, FaultChangedEventArgs e);
    public delegate void PollMotorErrorHandler(object sender, MotorErrorEventArgs e);        
    public delegate void PollMotorDataChangedHandler(object sender, DataChangedEventArgs e);        
}
