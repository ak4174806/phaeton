﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DFM.Motor
{
    public class MessageEventArgs
    {
        private string message;
        public string Message
        {
            get { return message; }
            set { message = value; }
        }

        public MessageEventArgs(string message)
        {
            this.message = message;
        }
    }
}
