﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DFM.Motor
{
    public class StatusChangedEventArgs : EventArgs
    {
        private StatusWord status;
        public StatusWord Status
        {
            get { return status; }
            set { status = value; }
        }

        public StatusChangedEventArgs(StatusWord status)
        {
            this.status = status;
        }
    }

    public class HomedChangedEventArgs : EventArgs
    {
        private HomedWord homed;
        public HomedWord Homed
        {
            get { return homed; }
            set { homed = value; }
        }

        public HomedChangedEventArgs(HomedWord homed)
        {
            this.homed = homed;
        }
    }

    public class FaultChangedEventArgs : EventArgs
    {
        private FaultWord fault;
        public FaultWord Fault
        {
            get { return fault; }
            set { fault = value; }
        }

        public FaultChangedEventArgs(FaultWord fault)
        {
            this.fault = fault;
        }
    }

    public class DataChangedEventArgs : EventArgs
    {
        public int LeftEdge
        {
            get;
            set;
        }

        public int RightEdge
        {
            get;
            set;
        }

        public int TargetA
        {
            get;
            set;
        }

        public DataChangedEventArgs(int leftEdge, int rightEdge, int targetA)
        {
            this.LeftEdge = leftEdge;
            this.RightEdge = rightEdge;
            this.TargetA = targetA;
        }
    }
}
