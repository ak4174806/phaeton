﻿using System;
using System.Collections.Generic;
using System.Text;

using NLog;

namespace DFM.Motor
{
    public class MockMotorControl : IMotorControl
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        private int invalidEdgeFlag = -10000;

        public MockMotorControl(int leftEdge, int rightEdge, bool delays=true)
        {
            storedLeftEdge = leftEdge;
            storedRightEdge = rightEdge;
            programDownloaded = false;
            this.delays = delays;

            Address = "";
            IsConnected = IsSetup = IsInitialised = false;
            LeftEdge = RightEdge = invalidEdgeFlag;

            PollState = new MockPollMotor();
        }

        private int storedLeftEdge
        {
            get;
            set;
        }
        private int storedRightEdge
        {
            get;
            set;
        }
        private bool programDownloaded
        {
            get;
            set;
        }
        private bool delays
        {
            get;
            set;
        }

        public event MotorTaskHandler FinishedMove;
        public event MotorTaskHandler FinishedSetup;
        public event MotorTaskHandler FinishedSetupWithoutEdgeScan;
        public event MotorSetupStateHandler SetupStateChanged;
        public event MotorMessageHandler Message;

        protected virtual void OnFinishedMove(MotorTaskEventArgs e)
        {
            if (FinishedMove != null)
            {
                // Invokes the delegates.
                FinishedMove(this, e);
            }
        }
        protected virtual void OnFinishedSetup(MotorTaskEventArgs e)
        {
            if (FinishedSetup != null)
            {
                // Invokes the delegates.
                FinishedSetup(this, e);
            }
        }
        protected virtual void OnFinishedSetupWithoutEdgeScan(MotorTaskEventArgs e)
        {
            if (FinishedSetupWithoutEdgeScan != null)
            {
                // Invokes the delegates.
                FinishedSetupWithoutEdgeScan(this, e);
            }
        }
        protected virtual void OnSetupStateChanged(SetupStateEventArgs e)
        {
            if (SetupStateChanged != null)
            {
                // Invokes the delegates.
                SetupStateChanged(this, e);
            }
        }
        protected virtual void OnMessage(MessageEventArgs e)
        {
            if (Message != null)
            {
                // Invokes the delegates.
                Message(this, e);
            }
        }

        public string Address
        {
            get;
            protected set;
        }
        public bool IsInitialised
        {
            get;
            protected set;
        }        
        public bool IsSetup
        {
            get;
            protected set;
        }
        public bool IsConnected
        {
            get;
            protected set;
        }

        private int leftEdge;
        public int LeftEdge
        {
            get
            {
                if (!IsConnected || !IsSetup || leftEdge == invalidEdgeFlag) return invalidEdgeFlag;
                return leftEdge;
            }
            protected set
            {
                leftEdge = value;
            }
        }

        private int rightEdge;
        public int RightEdge
        {
            get
            {
                if (!IsConnected || !IsSetup || rightEdge == invalidEdgeFlag) return invalidEdgeFlag;
                return rightEdge;
            }
            protected set
            {
                rightEdge = value;
            }
        }

        public bool EdgesFound
        {
            get
            {
                return LeftEdge != invalidEdgeFlag && RightEdge != invalidEdgeFlag;
            }            
        }

        public void ClearEdges()
        {
            LeftEdge = RightEdge = invalidEdgeFlag;
        }

        public IPollMotor PollState
        {
            get;
            protected set;
        }

        public bool ReadyToMove
        {
            get { return IsConnected && IsSetup; }
        }

        public bool Connect(string address, bool silent)
        {
            return Connect(address);
        }
        public bool Connect(string address)
        {
            if (delays) System.Threading.Thread.Sleep(100);
            IsConnected = true;
            Address = address;
            return true;
        }
        public void Disconnect()
        {
            if (delays) System.Threading.Thread.Sleep(20);
            IsConnected = false;
            Address = "";
        }
        public bool TestConnection()
        {
            if (!IsConnected) return false;
            if (delays) System.Threading.Thread.Sleep(50);
            return true;
        }
        public bool TestProgram()
        {
            if (!IsConnected) return false;
            if (delays) System.Threading.Thread.Sleep(50);
            return programDownloaded;
        }
        public bool DownloadProgram(string program)
        {
            if (!IsConnected) return false;
            if (delays) System.Threading.Thread.Sleep(50);
            programDownloaded = true;
            return true;
        }
        public bool DownloadProgramFile(string programFile)
        {
            return System.IO.File.Exists(programFile) && DownloadProgram(programFile);
        }
        public void Setup()
        {
            try
            {
                if (!(IsConnected && TestProgram())) throw new ApplicationException("Need to connect and download program first");

                if (delays) System.Threading.Thread.Sleep(50);

                LeftEdge = storedLeftEdge;
                RightEdge = storedRightEdge;
                IsSetup = true;

                if (!IsInitialised)
                {
                    OnSetupStateChanged(new SetupStateEventArgs(MotorSetupState.Initialising));
                    OnSetupStateChanged(new SetupStateEventArgs(MotorSetupState.Homing));
                    IsInitialised = true;
                    logger.Trace("Initialising motor");
                }
                else logger.Trace("Skipping homing");
                OnSetupStateChanged(new SetupStateEventArgs(MotorSetupState.EdgeFinding));
                OnSetupStateChanged(new SetupStateEventArgs(MotorSetupState.Done));

                // call it async
                System.Windows.Forms.Timer timer = new System.Windows.Forms.Timer();
                timer.Tick += new EventHandler(delegate(object sender, EventArgs e)
                {
                    timer.Stop();
                    OnFinishedSetup(new MotorTaskEventArgs(true));
                });
                timer.Interval = 1000;
                timer.Start();
            }
            catch (Exception ex)
            {
                logger.ErrorException(ex.Message, ex);
                throw;
            }
        }
        public void SetupWithoutEdgeScan(int leftEdge, int rightEdge)
        {
            try
            {
                if (!(IsConnected && TestProgram())) throw new ApplicationException("Need to connect and download program first");

                LeftEdge = leftEdge;
                RightEdge = rightEdge;
                IsSetup = true;

                if (delays) System.Threading.Thread.Sleep(50);

                if (!IsInitialised)
                {
                    OnSetupStateChanged(new SetupStateEventArgs(MotorSetupState.Initialising));
                    OnSetupStateChanged(new SetupStateEventArgs(MotorSetupState.Homing));
                    IsInitialised = true;
                    logger.Trace("Initialising motor");
                }
                else logger.Trace("Skipping homing");
                
                // call it async
                System.Windows.Forms.Timer timer = new System.Windows.Forms.Timer();
                timer.Tick += new EventHandler(delegate(object sender, EventArgs e)
                {
                    timer.Stop();
                    OnFinishedSetupWithoutEdgeScan(new MotorTaskEventArgs(true));
                });
                timer.Interval = 200;
                timer.Start();
            }
            catch (Exception ex)
            {
                logger.ErrorException(ex.Message, ex);
                throw;
            }
        }
        public void MoveTo(int position)
        {
            try
            {
                if (!ReadyToMove) throw new ApplicationException("Need to set up first");
                if (LeftEdge == invalidEdgeFlag || RightEdge == invalidEdgeFlag) throw new ApplicationException("Need to find edges first");
                if (!(LeftEdge < position && position < RightEdge) && position != 0) throw new ApplicationException("Need to move to spot within strip");
                if (delays) System.Threading.Thread.Sleep(50);

                if (PollState is MockPollMotor)
                {
                    ((MockPollMotor)PollState).TargetA = position;
                }

                // call it async
                System.Windows.Forms.Timer timer = new System.Windows.Forms.Timer();
                timer.Tick += new EventHandler(delegate(object sender, EventArgs e)
                {
                    timer.Stop();
                    OnFinishedMove(new MotorTaskEventArgs(true));
                });
                timer.Interval = 1000;
                timer.Start();
            }
            catch (Exception ex)
            {
                logger.ErrorException(ex.Message, ex);
                throw;
            }
        }
        public bool StopProcesses()
        {
            return true;
        }
    }
}
