﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DFM.Motor
{
    public class MotorTaskEventArgs : EventArgs
    {
        private bool success = false;
        private Exception error = null;

        public MotorTaskEventArgs(bool success, Exception error)
            : this(success)
        {
            this.error = error;
        }

        public MotorTaskEventArgs(bool success)
        {
            this.success = success;
        }

        public bool Success
        {
            get { return success; }
        }

        public Exception Error
        {
            get { return error; }
        }
    }
}
