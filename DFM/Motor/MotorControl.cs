﻿using System;
using System.Collections.Generic;
using System.Text;

using Galil;
using System.Runtime.InteropServices;
using System.IO;
using System.ComponentModel;
using System.Threading;
using NLog;

namespace DFM.Motor
{
    public class MotorControl : IMotorControl
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        private int invalidEdgeFlag = -10000;

        public static string[] GetAddresses()
        {
            Galil.Galil g = new Galil.Galil();
            object[] add = g.addresses() as object[];
            string[] addresses = new string[add.Length];
            for (int i = 0; i < add.Length; i++)
                addresses[i] = (string)add[i];
            
            return addresses;
        }

        protected Galil.Galil g;
        protected IPollMotor pollState;
        protected BackgroundWorker setupWorker;
        protected BackgroundWorker moveWorker;
        
        public MotorControl()
        {
            address = "";
            IsInitialised = false;
            IsSetup = false;
            isConnected = false;
            edgeLeft = invalidEdgeFlag;
            edgeRight = invalidEdgeFlag;
            clearEdges = false;

            g = new Galil.Galil();
            pollState = new PollMotor(g);
            setupWorker = new BackgroundWorker();
            moveWorker = new BackgroundWorker();

            // forward messages from the Galil
            g.onMessage += new Events_onMessageEventHandler(delegate(string s)
            {
                OnMessage(new MessageEventArgs(s));
            });

            setupWorker.DoWork += new DoWorkEventHandler(setupWorker_DoWork);
            setupWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(setupWorker_RunWorkerCompleted);
            setupWorker.ProgressChanged += new ProgressChangedEventHandler(setupWorker_ProgressChanged);
            setupWorker.WorkerReportsProgress = true;
            setupWorker.WorkerSupportsCancellation = true;

            moveWorker.DoWork += new DoWorkEventHandler(moveWorker_DoWork);
            moveWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(moveWorker_RunWorkerCompleted);
            moveWorker.WorkerReportsProgress = false;
            moveWorker.WorkerSupportsCancellation = true;
        }
        ~MotorControl()
        {
            stopWorkers();
        }
        
        public event MotorTaskHandler FinishedMove;
        public event MotorTaskHandler FinishedSetup;
        public event MotorTaskHandler FinishedSetupWithoutEdgeScan;
        public event MotorSetupStateHandler SetupStateChanged;
        public event MotorMessageHandler Message;
        
        protected virtual void OnFinishedMove(MotorTaskEventArgs e)
        {
            if (FinishedMove != null)
            {
                // Invokes the delegates.
                FinishedMove(this, e);
            }
        }
        protected virtual void OnFinishedSetup(MotorTaskEventArgs e)
        {
            if (FinishedSetup != null)
            {
                // Invokes the delegates.
                FinishedSetup(this, e);
            }
        }
        protected virtual void OnFinishedSetupWithoutEdgeScan(MotorTaskEventArgs e)
        {
            if (FinishedSetupWithoutEdgeScan != null)
            {
                // Invokes the delegates.
                FinishedSetupWithoutEdgeScan(this, e);
            }
        }
        protected virtual void OnSetupStateChanged(SetupStateEventArgs e)
        {
            if (SetupStateChanged != null)
            {
                // Invokes the delegates.
                SetupStateChanged(this, e);
            }
        }
        protected virtual void OnMessage(MessageEventArgs e)
        {
            if (Message != null)
            {
                // Invokes the delegates.
                Message(this, e);
            }
        }
        
        protected string address;
        public string Address
        {
            get { return address; }
        }

        protected bool isInitialised;
        public bool IsInitialised
        {
            get { return isInitialised; }
            private set
            {
                isInitialised = value;
                if (!isInitialised) IsSetup = false;
            }
        }

        protected bool isSetup;
        public bool IsSetup
        {
            get { return isSetup; }
            private set { isSetup = value; }
        }

        protected bool isConnected;
        public bool IsConnected
        {
            get { return isConnected; }
        }

        protected int edgeLeft;
        public int LeftEdge
        {
            get
            {
                if (IsConnected && IsSetup && PollState.Status.EdgesFound)
                {
                    logger.Trace("Requested LeftEdge, saved is {0}", edgeLeft);
                    if (edgeLeft != invalidEdgeFlag) return edgeLeft;

                    logger.Trace("Requested LeftEdge, need to ask it", edgeLeft);
                    string response = "";
                    try
                    {
                        response = g.command("EDGE_LT=");
                        edgeLeft = Convert.ToInt32(Convert.ToDecimal(response));
                        if (edgeLeft == 0)
                        {
                            edgeLeft = invalidEdgeFlag;
                            logger.Trace("Requested LeftEdge, failed as Galil couldn't get it (had 0 saved)");
                            throw new ApplicationException("Left edge was not found by Galil. Needs to be setup again.");
                        }
                        logger.Trace("Requested LeftEdge, worked, {0}", edgeLeft);
                        return edgeLeft;
                    }
                    catch (COMException ex)
                    {
                        logger.Trace("Requested LeftEdge, failed as communication error");
                        throw new ApplicationException("Could not read left edge: " + ex.Message, ex);
                    }
                    catch (FormatException ex)
                    {
                        logger.Trace("Requested LeftEdge, failed as format error");
                        throw new ApplicationException("Response from left edge was not a valid integer: " + response, ex);
                    }
                }
                else
                {
                    logger.Trace("Requested LeftEdge, denied because not connected, setup or EdgesFound, {0}, {1}, {2}");
                    return invalidEdgeFlag;
                }
            }
        }

        protected int edgeRight;
        public int RightEdge
        {
            get
            {
                // todo: removed "&&!clearEdges"
                if (IsConnected && IsSetup && PollState.Status.EdgesFound)
                {
                    if (edgeRight != invalidEdgeFlag) return edgeRight;

                    string response = "";
                    try
                    {
                        response = g.command("EDGE_RT=");
                        edgeRight = Convert.ToInt32(Convert.ToDecimal(response));
                        if (edgeRight == 0)
                        {
                            edgeRight = invalidEdgeFlag;
                            throw new ApplicationException("Right edge was not found by Galil. Needs to be setup again.");
                        }
                        return edgeRight;
                    }
                    catch (COMException ex)
                    {
                        throw new ApplicationException("Could not read right edge: " + ex.Message, ex);
                    }
                    catch (FormatException ex)
                    {
                        throw new ApplicationException("Response from right edge was not a valid integer: " + response, ex);
                    }
                }
                else
                    return invalidEdgeFlag;
            }
        }

        private bool clearEdges;
        public bool EdgesFound
        {
            get
            {
                if (IsConnected && PollState.Status.EdgesFound)
                {
                    if (edgeLeft != invalidEdgeFlag && edgeRight != invalidEdgeFlag) return true;

                    string response;
                    try
                    {
                        response = g.command("EDGE_LT=");
                        edgeLeft = Convert.ToInt32(Convert.ToDecimal(response));
                        if (edgeLeft == 0)
                        {
                            logger.Error("Edges not found as EDGE_LT=" + edgeLeft.ToString());
                            logger.Trace("Actually: " + g.command("EDGE_LT="));
                            edgeLeft = invalidEdgeFlag;
                            return false;
                        }

                        response = g.command("EDGE_RT=");
                        edgeRight = Convert.ToInt32(Convert.ToDecimal(response));
                        if (edgeRight == 0)
                        {
                            logger.Error("Edges not found as EDGE_RT=" + edgeRight.ToString());
                            logger.Trace("Actually: " + g.command("EDGE_RT="));
                            edgeRight = invalidEdgeFlag;
                            return false;
                        }
                    }
                    catch (Exception ex)
                    {
                        logger.ErrorException("Failed EdgesFound", ex);
                        return false; 
                    }

                    return true;
                }

                return false;
            }
        }

        public void ClearEdges()
        {
            edgeLeft = edgeRight = invalidEdgeFlag;
            clearEdges = true;
        }

        public IPollMotor PollState
        {
            get
            {
                return pollState;
            }
        }

        public bool Connect(string address, bool silent)
        {
            isConnected = false;

            // let galil validate address

            address = address.Trim();
            if (address == "")
            { 
                // allow it, Connections dialog will appear
            }
            if (address.StartsWith("COM") && address.Length < 6)
            {
                // then it is a COM port without an IP address
                address = address + " 115200";
            }

            try
            {
                // connect
                g.address = address;
                
                // start status word poll
                PollState.RunPoll = true;
            }
            catch (COMException ex)
            {                
                if (silent) return false;

                throw new ApplicationException("Could not connect to Galil on address: " + address + ". Error is: " + ex.Message, ex);
            }

            this.address = address;
            isConnected = true;
            return true;
        }

        public bool Connect(string address)
        {
            return Connect(address, false);
        }

        public void Disconnect()
        {
            PollState.RunPoll = false;
            isConnected = false;
            address = "";
        }

        public bool TestConnection()
        {
            if (!IsConnected)
            {
                throw new ApplicationException("TestConnection called before connecting to Galil");
            }

            try
            {
                string response = g.command("MG _BN");
                return true;
            }
            catch (COMException)
            {
                return false;
            }
        }

        // runs a test routine to check if a program has been downloaded
        public bool TestProgram()
        {
            if (!IsConnected)
            {
                throw new ApplicationException("TestConnection called before connecting to Galil");
            }

            string response;
            try
            {
                // TEST_VAR is a variable
                // PRGTEST should set it to 1
                response = g.command("TEST_VAR=0");
                response = g.command("XQ#PRGTEST");
                response = g.command("TEST_VAR=");

                return Convert.ToInt32(Convert.ToDecimal(response)) == 1;
            }
            catch (COMException)
            {
                return false;
            }
        }

        public bool DownloadProgram(string program)
        {
            if (!IsConnected)
            {
                throw new ApplicationException("DownloadProgram called before connecting to Galil");
            }
            
            try
            {
                g.programDownload(program);
                return true;
            }
            catch (COMException ex)
            {
                throw new ApplicationException("Could not download program: " + program + " to Galil. Error was: " + ex.Message, ex);
            }
        }

        public bool DownloadProgramFile(string programFile)
        {
            if (!IsConnected)
            {
                throw new ApplicationException("DownloadProgramFile called before connecting to Galil");
            }

            if (!File.Exists(programFile))
            {
                throw new ApplicationException("Could not find file: " + programFile + " to download to Galil");
            }

            try
            {
                g.programDownloadFile(programFile);
                return true;
            }
            catch (COMException ex)
            {
                throw new ApplicationException("Could not download program: " + programFile + " to Galil. Error was: " + ex.Message, ex);
            }
        }

        protected void stopWorkers()
        {
            foreach (BackgroundWorker worker in new BackgroundWorker[] { setupWorker, moveWorker })
            {
                if (worker != null && worker.IsBusy)
                {
                    worker.CancelAsync();

                    // try kill it, quadratic wait times
                    for (int i = 1; i <= 5; i++)
                    {
                        if (worker.IsBusy) Thread.Sleep(5 * i * i);
                        else break;
                    }                    
                }
            }
        }

        public void Setup()
        {
            logger.Trace("Set up with edge scan");
            edgeLeft = invalidEdgeFlag;
            edgeRight = invalidEdgeFlag;
            IsSetup = false;
            
            if (!IsConnected)
            {
                throw new ApplicationException("Setup called before connecting to Galil");
            }

            if (!TestProgram())
            {
                throw new ApplicationException("Program could not be found on Galil");
            }

            if (setupWorker.IsBusy)
            {
                throw new ApplicationException("Setup already running");
            }

            setupWorker.RunWorkerAsync(new QuickScanParameters());
        }
        
        public void SetupWithoutEdgeScan(int leftEdge, int rightEdge)
        {
            logger.Trace("Set up without edge scan");
            edgeLeft = invalidEdgeFlag;
            edgeRight = invalidEdgeFlag;
            IsSetup = false;

            if (!IsConnected)
            {
                throw new ApplicationException("Setup called before connecting to Galil");
            }

            if (!TestProgram())
            {
                throw new ApplicationException("Program could not be found on Galil");
            }

            if (setupWorker.IsBusy)
            {
                throw new ApplicationException("Setup already running");
            }

            setupWorker.RunWorkerAsync(new QuickScanParameters(leftEdge, rightEdge));
        }

        void setupWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            SetupScanResult result = new SetupScanResult(false, SetupScanResult.SetupTypes.WithEdgeScan);
            if (e.Result != null) result = (SetupScanResult)e.Result;
            if (e.Cancelled || e.Error != null) result.Success = false;            

            IsSetup = result.Success && EdgesFound;

            MotorTaskEventArgs e2 = new MotorTaskEventArgs(result.Success, e.Error);
            
            // verify edges not the same
            if (IsSetup && LeftEdge == RightEdge)
            {
                e2 = new MotorTaskEventArgs(false, new ApplicationException("Same edge position found for both edges (" + LeftEdge + ", " + RightEdge + "), edge scan was discarded"));
                IsSetup = false;
            }            

            if (result.SetupType == SetupScanResult.SetupTypes.WithEdgeScan)
                OnFinishedSetup(e2);
            else
                OnFinishedSetupWithoutEdgeScan(e2);
        }

        void setupWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if (e.ProgressPercentage == 0)
            {
                MotorSetupState state = (MotorSetupState)e.UserState;
                SetupStateEventArgs e2 = new SetupStateEventArgs(state);
                OnSetupStateChanged(e2);

                // todo: fix this for both
            }
        }

        void setupWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = sender as BackgroundWorker;
            string response;
            
            QuickScanParameters skipEdgeScan = new QuickScanParameters();
            if (e.Argument != null) skipEdgeScan = (QuickScanParameters)e.Argument;
            logger.Trace("Quick scan param are " + skipEdgeScan);

            e.Result = new SetupScanResult(false, SetupScanResult.SetupTypes.WithEdgeScan);
            if (skipEdgeScan.SkipEdgeScan) e.Result = new SetupScanResult(false, SetupScanResult.SetupTypes.WithoutEdgeScan);
            
            if (!IsInitialised)
            {
                logger.Trace("Initialising for the first time");

                // run auto
                worker.ReportProgress(0, MotorSetupState.Initialising);
                try
                {
                    response = g.command("XQ#AUTO");
                    Thread.Sleep(50);

                    if (!PollState.Status.IsReset) Thread.Sleep(50);
                    if (!PollState.Status.IsReset)
                    {
                        worker.ReportProgress(0, MotorSetupState.Error);
                        throw new ApplicationException("Unable to run #AUTO successfully");
                    }

                    if (worker.CancellationPending)
                    {
                        worker.ReportProgress(0, MotorSetupState.Cancelled);
                        e.Cancel = true;
                        return;
                    }
                }
                catch (COMException ex)
                {
                    worker.ReportProgress(0, MotorSetupState.Error);
                    throw new ApplicationException("Error initialising motor in #AUTO: " + ex.Message, ex);
                }

                // home
                worker.ReportProgress(0, MotorSetupState.Homing);
                try
                {
                    response = g.command("XQ#HOME");
                    Thread.Sleep(250);

                    // block until move is done
                    while (PollState.Status.Homing || PollState.Status.Busy)
                    {
                        if (worker.CancellationPending)
                        {
                            worker.ReportProgress(0, MotorSetupState.Cancelled);
                            e.Cancel = true;
                            return;
                        }

                        Thread.Sleep(50);
                    }

                    // time for flags to be set
                    Thread.Sleep(20);

                    if (PollState.Status.Fault)
                    {
                        worker.ReportProgress(0, MotorSetupState.Error);
                        throw new ApplicationException("Home failed: " + PollState.Fault.Message);
                    }

                    if (!PollState.Status.Homed)
                    {
                        worker.ReportProgress(0, MotorSetupState.Error);
                        throw new ApplicationException("Home failed without any indicated fault");
                    }

                    if (worker.CancellationPending)
                    {
                        worker.ReportProgress(0, MotorSetupState.Cancelled);
                        e.Cancel = true;
                        return;
                    }
                }
                catch (COMException ex)
                {
                    worker.ReportProgress(0, MotorSetupState.Error);
                    throw new ApplicationException("Error homing motor in #HOME: " + ex.Message, ex);
                }

                // set initialised flag
                IsInitialised = true;
                logger.Trace("Finished homing");
            }
            else
            {
                if (PollState.Status.Fault)
                {
                    response = g.command("XQ#FLT_CLR");
                    Thread.Sleep(250);

                    // block until scan is done
                    while (PollState.Status.Fault || PollState.Fault.State > 0)
                    {
                        if (worker.CancellationPending)
                        {
                            worker.ReportProgress(0, MotorSetupState.Cancelled);
                            e.Cancel = true;
                            return;
                        }

                        Thread.Sleep(50);
                    }
                }

                logger.Trace("Skipping homing");
            }

            if (skipEdgeScan.SkipEdgeScan)
            {
                logger.Trace("Skipping edge scan");
                logger.Trace("Setting edges to {0} and {1}", skipEdgeScan.TargetLeftEdge, skipEdgeScan.TargetRightEdge);

                // set edges to Galil
                response = g.command(String.Format("EDGE_LT={0}", skipEdgeScan.TargetLeftEdge));
                Thread.Sleep(50);

                response = g.command(String.Format("EDGE_RT={0}", skipEdgeScan.TargetRightEdge));
                Thread.Sleep(50);

                // set status bits
                response = g.command(String.Format("S_WORD1={0}", PollState.Status.State | 16)); // set edge found bit
                Thread.Sleep(50);
            }
            else
            {
                logger.Trace("Running edge scan");

                // edge scan
                worker.ReportProgress(0, MotorSetupState.EdgeFinding);
                try
                {
                    response = g.command("XQ#EDGESCN");
                    Thread.Sleep(250);

                    // block until scan is done
                    while (PollState.Status.Busy)
                    {
                        if (worker.CancellationPending)
                        {
                            worker.ReportProgress(0, MotorSetupState.Cancelled);
                            e.Cancel = true;
                            return;
                        }

                        Thread.Sleep(50);
                    }

                    // time for flags to be set
                    Thread.Sleep(20);

                    if (PollState.Status.Fault && (PollState.Fault.LeftEdgeNotFound || PollState.Fault.RightEdgeNotFound))
                    {
                        worker.ReportProgress(0, MotorSetupState.Error);
                        throw new ApplicationException("Could not find the strip edges");
                    }

                    if (PollState.Status.Fault)
                    {
                        worker.ReportProgress(0, MotorSetupState.Error);
                        throw new ApplicationException("Error finding strip edges: " + PollState.Fault.Message);
                    }

                    if (!PollState.Status.EdgesFound)
                    {
                        worker.ReportProgress(0, MotorSetupState.Error);
                        throw new ApplicationException("Could not find edges, no indication of fault");
                    }

                    if (worker.CancellationPending)
                    {
                        worker.ReportProgress(0, MotorSetupState.Cancelled);
                        e.Cancel = true;
                        return;
                    }
                }
                catch (COMException ex)
                {
                    worker.ReportProgress(0, MotorSetupState.Error);
                    throw new ApplicationException("Error scanning for edges in #EDGESCN: " + ex.Message, ex);
                }
            }

            logger.Trace("Verifying edges and status");

            // check status
            if (!PollState.Status.ReadyToMove)
            {
                worker.ReportProgress(0, MotorSetupState.Error);
                if (PollState.Status.Fault)
                {
                    throw new ApplicationException("A fault occurred when setting up: " + PollState.Fault.Message);
                }
                else
                {
                    throw new ApplicationException("State is wrong at the end of setup");
                }
            }

            if (!EdgesFound)
            {
                try
                {
                    int l = LeftEdge;
                    int r = RightEdge;
                }
                catch (Exception ex)
                {
                    worker.ReportProgress(0, MotorSetupState.Error);
                    throw new ApplicationException("No edges found, even though the Galil reports that it found edges successfully: " + ex.Message, ex);
                }

                // therefore l == invalidEdgeFlag or r == invalidEdgeFlag
                throw new ApplicationException("Edges weren't found");
            }
            
            logger.Trace("Success");
            worker.ReportProgress(0, MotorSetupState.Done);
            ((SetupScanResult)e.Result).Success = true;
        }
        
        public bool ReadyToMove
        {
            get { return IsConnected && IsSetup && PollState.Status.ReadyToMove && !moveWorker.IsBusy; }
        }

        public void MoveTo(int position)
        {
            if (!IsConnected || !IsSetup)
            {
                throw new ApplicationException("MoveTo called before connecting to Galil and setting it up");
            }

            if (PollState.Status.Fault)
            {
                throw new ApplicationException("MoveTo called while Galil had an unresolved fault: " + PollState.Fault.Message);
            }

            if (!ReadyToMove)
            {
                throw new ApplicationException("MoveTo called before Galil was ready");
            }
            
            int left = LeftEdge;
            int right = RightEdge;

            if (left == invalidEdgeFlag || right == invalidEdgeFlag)
            {
                throw new ApplicationException("MoveTo called before connecting to Galil and setting it up");
            }

            // note: strictly less than as don't want to scan right at edge
            if (!(left < position && position < right))
            {
                // exception for homing motor
                if (position != 0)
                    throw new ApplicationException("Position to move too lies outside strip");
            }

            moveWorker.RunWorkerAsync(position);
        }

        void moveWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            bool success;
            if (e.Cancelled || e.Error != null) success = false;
            else success = (bool)e.Result;

            MotorTaskEventArgs e2 = new MotorTaskEventArgs(success, e.Error);
            OnFinishedMove(e2);
        }

        void moveWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = sender as BackgroundWorker;
            int position = (int)e.Argument;

            string response;
            e.Result = false;

            // set position
            try
            {
                response = g.command("TARGET_A=" + position.ToString());
                Thread.Sleep(50);

                if (worker.CancellationPending)
                {
                    e.Cancel = true;
                    return;
                }

                response = g.command("XQ#MOVE_A");
                Thread.Sleep(200);

                if (worker.CancellationPending)
                {
                    e.Cancel = true;
                    return;
                }

                Thread.Sleep(200);

                // block until move is done
                while (PollState.Status.Busy)
                {
                    if (worker.CancellationPending)
                    {
                        e.Cancel = true;
                        return;
                    }

                    Thread.Sleep(50);
                }

                // check for success - check for faults and then make sure ready to move again
                if (PollState.Status.Fault)
                {
                    throw new ApplicationException("Unable to move motor: " + PollState.Fault.Message);
                }

                if (!PollState.Status.ReadyToMove)
                {
                    throw new ApplicationException("Some issue occurred with motor, it needs to be set up again");
                }

                if (worker.CancellationPending)
                {
                    e.Cancel = true;
                    return;
                }

                e.Result = true;
            }
            catch (COMException ex)
            {
                throw new ApplicationException("Error sending commands to motor: " + ex.Message, ex);
            }
        }
        
        private bool killWorker(BackgroundWorker worker)
        {
            worker.CancelAsync();
            int i = 1;
            while (worker.IsBusy && i <= 8)
            {
                Thread.Sleep(50 * i * i);
                i += 1;
            }
            return !worker.IsBusy;
        }

        public bool StopProcesses()
        {
            try
            {
                bool a1 = killWorker(moveWorker);
                bool a2 = killWorker(setupWorker);
                if (!a1) logger.Error("Could not kill moveWorker");
                if (!a2) logger.Error("Could not kill setupWorker");
                return true;
            }
            catch (Exception ex)
            {
                logger.ErrorException("Error stopping workers: " + ex.Message, ex);
                return false;
            }
        }
    }

    public class QuickScanParameters
    {
        public bool SkipEdgeScan
        {
            get;
            set;
        }

        public int TargetLeftEdge
        {
            get;
            set;
        }

        public int TargetRightEdge
        {
            get;
            set;
        }

        public QuickScanParameters()
        {
            SkipEdgeScan = false;
            TargetLeftEdge = TargetRightEdge = 0;
        }

        public QuickScanParameters(int targetLeftEdge, int targetRightEdge)
        {
            SkipEdgeScan = true;
            TargetLeftEdge = targetLeftEdge;
            TargetRightEdge = targetRightEdge;
        }

        public override string ToString()
        {
            return String.Format("SkipEdgeScan = {0}, TargetLeftEdge = {1}, TargetRightEdge = {2}", SkipEdgeScan, TargetLeftEdge, TargetRightEdge);
        }
    }

    public class SetupScanResult
    {
        public bool Success
        {
            get;
            set;
        }

        public enum SetupTypes
        {
            WithEdgeScan,
            WithoutEdgeScan
        }

        public SetupTypes SetupType
        {
            get;
            set;
        }

        public SetupScanResult(bool success, SetupTypes setupType)
        {
            Success = success;
            SetupType = setupType;
        }

        public override string ToString()
        {
            return String.Format("Success = {0}, SetupType = {1}", Success, SetupType);
        }
    }
}
