﻿using System;
using System.Collections.Generic;
using System.Text;

using Galil;
using System.Runtime.InteropServices;
using System.IO;
using System.ComponentModel;
using System.Threading;


namespace DFM.Motor
{
    public interface IMotorControl
    {
        event MotorTaskHandler FinishedMove;
        event MotorTaskHandler FinishedSetup;
        event MotorTaskHandler FinishedSetupWithoutEdgeScan;
        event MotorSetupStateHandler SetupStateChanged;
        event MotorMessageHandler Message;

        string Address { get; }
        bool IsInitialised { get; }
        bool IsSetup { get; }
        bool IsConnected { get; }
        int LeftEdge { get; }
        int RightEdge { get; }
        bool EdgesFound { get; }
        IPollMotor PollState { get; }
        bool ReadyToMove { get; }

        bool Connect(string address, bool silent);
        bool Connect(string address);
        void Disconnect();
        bool TestConnection();
        bool TestProgram();
        bool DownloadProgram(string program);
        bool DownloadProgramFile(string programFile);
        void Setup();
        void MoveTo(int position);
        void ClearEdges();
        bool StopProcesses();

        void SetupWithoutEdgeScan(int leftEdge, int rightEdge);
    }

    public delegate void MotorTaskHandler(object sender, MotorTaskEventArgs e);
    public delegate void MotorSetupStateHandler(object sender, SetupStateEventArgs e);
    public delegate void MotorMessageHandler(object sender, MessageEventArgs e);

    public enum MotorSetupState
    {
        Cancelled,
        Error,
        Initialising,
        Homing,
        EdgeFinding,
        Done
    }
}
