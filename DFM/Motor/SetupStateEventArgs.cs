﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DFM.Motor
{
    public class SetupStateEventArgs
    {
        private MotorSetupState setupState;

        public SetupStateEventArgs(MotorSetupState setupState)
        {
            this.setupState = setupState;
        }

        public MotorSetupState SetupState
        {
            get { return setupState; }
        }
    }
}
