﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DFM.Motor
{
    public class MotorErrorEventArgs : EventArgs
    {
        private Exception error;
        public Exception Error
        {
            get { return error; }
            set { error = value; }
        }

        public MotorErrorEventArgs(Exception error)
        {
            this.error = error;
        }
    }
}
