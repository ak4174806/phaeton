﻿namespace DFM.Results
{
    partial class DFMResultsDisplayForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.resultsGrid = new System.Windows.Forms.DataGridView();
            this.resultIdColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dateColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pitDepthColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.numPitsColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.scanPositionColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.scanPathColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.scanIdColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.commentColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.deleteColumn = new System.Windows.Forms.DataGridViewButtonColumn();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.btnExport = new System.Windows.Forms.Button();
            this.saveExportFile = new System.Windows.Forms.SaveFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.resultsGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // resultsGrid
            // 
            this.resultsGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.resultsGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.resultsGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.resultIdColumn,
            this.dateColumn,
            this.pitDepthColumn,
            this.numPitsColumn,
            this.scanPositionColumn,
            this.scanPathColumn,
            this.scanIdColumn,
            this.commentColumn,
            this.deleteColumn});
            this.resultsGrid.Location = new System.Drawing.Point(0, 0);
            this.resultsGrid.Name = "resultsGrid";
            this.resultsGrid.Size = new System.Drawing.Size(780, 130);
            this.resultsGrid.TabIndex = 0;
            // 
            // resultIdColumn
            // 
            this.resultIdColumn.DataPropertyName = "result_id";
            this.resultIdColumn.HeaderText = "Result ID";
            this.resultIdColumn.Name = "resultIdColumn";
            this.resultIdColumn.ReadOnly = true;
            this.resultIdColumn.Visible = false;
            // 
            // dateColumn
            // 
            this.dateColumn.DataPropertyName = "date";
            this.dateColumn.HeaderText = "Date";
            this.dateColumn.Name = "dateColumn";
            this.dateColumn.Width = 55;
            // 
            // pitDepthColumn
            // 
            this.pitDepthColumn.DataPropertyName = "pit_depth";
            this.pitDepthColumn.HeaderText = "Pit Depth";
            this.pitDepthColumn.Name = "pitDepthColumn";
            this.pitDepthColumn.Width = 76;
            // 
            // numPitsColumn
            // 
            this.numPitsColumn.DataPropertyName = "num_pits";
            this.numPitsColumn.HeaderText = "Num Pits";
            this.numPitsColumn.Name = "numPitsColumn";
            this.numPitsColumn.Width = 74;
            // 
            // scanPositionColumn
            // 
            this.scanPositionColumn.DataPropertyName = "scan_position";
            this.scanPositionColumn.HeaderText = "Scan Position";
            this.scanPositionColumn.Name = "scanPositionColumn";
            this.scanPositionColumn.Width = 97;
            // 
            // scanPathColumn
            // 
            this.scanPathColumn.DataPropertyName = "scan_path";
            this.scanPathColumn.HeaderText = "Scan Path";
            this.scanPathColumn.Name = "scanPathColumn";
            this.scanPathColumn.Width = 82;
            // 
            // scanIdColumn
            // 
            this.scanIdColumn.DataPropertyName = "scan_id";
            this.scanIdColumn.HeaderText = "Scan ID";
            this.scanIdColumn.Name = "scanIdColumn";
            this.scanIdColumn.Width = 71;
            // 
            // commentColumn
            // 
            this.commentColumn.DataPropertyName = "comment";
            this.commentColumn.HeaderText = "Comment";
            this.commentColumn.Name = "commentColumn";
            this.commentColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.commentColumn.Width = 76;
            // 
            // deleteColumn
            // 
            this.deleteColumn.HeaderText = "Delete Result";
            this.deleteColumn.Name = "deleteColumn";
            this.deleteColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.deleteColumn.Text = "Delete";
            this.deleteColumn.UseColumnTextForButtonValue = true;
            this.deleteColumn.Width = 96;
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Location = new System.Drawing.Point(636, 139);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(133, 23);
            this.btnCancel.TabIndex = 4;
            this.btnCancel.Text = "Cancel Changes";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Location = new System.Drawing.Point(501, 138);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(129, 23);
            this.btnSave.TabIndex = 3;
            this.btnSave.Text = "Save Changes";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnRefresh
            // 
            this.btnRefresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnRefresh.Location = new System.Drawing.Point(12, 139);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(129, 23);
            this.btnRefresh.TabIndex = 3;
            this.btnRefresh.Text = "Refresh";
            this.btnRefresh.UseVisualStyleBackColor = true;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // btnExport
            // 
            this.btnExport.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnExport.Location = new System.Drawing.Point(163, 139);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(108, 23);
            this.btnExport.TabIndex = 5;
            this.btnExport.Text = "Export Database";
            this.btnExport.UseVisualStyleBackColor = true;
            // 
            // saveExportFile
            // 
            this.saveExportFile.DefaultExt = "csv";
            this.saveExportFile.FileName = "dfm_results.csv";
            this.saveExportFile.Filter = "CSV Files|*.csv|All files|*.*";
            // 
            // DFMResultsDisplayForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(781, 171);
            this.Controls.Add(this.btnExport);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnRefresh);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.resultsGrid);
            this.Name = "DFMResultsDisplayForm";
            this.Text = "DFM Results";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.resultsGrid)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView resultsGrid;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.DataGridViewTextBoxColumn resultIdColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dateColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn pitDepthColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn numPitsColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn scanPositionColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn scanPathColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn scanIdColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn commentColumn;
        private System.Windows.Forms.DataGridViewButtonColumn deleteColumn;
        private System.Windows.Forms.Button btnExport;
        private System.Windows.Forms.SaveFileDialog saveExportFile;


    }
}