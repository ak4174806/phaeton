﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using System.IO;
using PaintAnalysis.Common;
using PaintAnalysis.Common.Results;
using NLog;

namespace DFM.Results
{
    public partial class DFMResultsDisplayForm : Form
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        DFMResults results;
        private string[] scanPositions;

        private string pitDepthUnit;
        public string PitDepthUnit
        {
            get { return pitDepthUnit; }
            set
            {
                if (!String.IsNullOrEmpty(pitDepthUnit))
                {
                    // rescale results
                    for (int i = 0; i < resultsGrid.Rows.Count; i++)
                    {
                        if (resultsGrid.Rows[i].Cells["pitDepthColumn"].Value != null)
                        {
                            PitDepth tmp = new PitDepth(resultsGrid.Rows[i].Cells["pitDepthColumn"].Value.ToString(), pitDepthUnit);
                            if (tmp != null)
                            {
                                tmp.ChangeUnit(value);
                                resultsGrid.Rows[i].Cells["pitDepthColumn"].Value = tmp;
                            }
                        }
                    }

                    if (resultsGrid.Rows.Count > 1 && resultsGrid.Rows[0].Cells["pitDepthColumn"].Value != null)
                        if (!handleSave()) 
                            MessageBox.Show("There was an error saving the changes in units");
                }

                pitDepthUnit = value;
                resultsGrid.Columns["pitDepthColumn"].HeaderText = "Pit Depth (" + value + "s)";
                FormState = FormStates.Saved; // to fix above line
            }
        }

        public DFMResultsDisplayForm(DFMResults results)
        {
            InitializeComponent();

            this.results = results;
            scanPositions = new string[] { "left", "middle", "right" };;

            // fetch scale factor
            PitDepthUnit = "micron";
            
            // We manually added the columns, do not auto add any more..
            resultsGrid.AutoGenerateColumns = false;

            resultsGrid.CellFormatting += new DataGridViewCellFormattingEventHandler(resultsGrid_CellFormatting);
            resultsGrid.CellParsing += new DataGridViewCellParsingEventHandler(resultsGrid_CellParsing);
            resultsGrid.DataError += new DataGridViewDataErrorEventHandler(resultsGrid_DataError);

            resultsGrid.CellClick += new DataGridViewCellEventHandler(resultsGrid_CellClick);
            resultsGrid.CellValueChanged += new DataGridViewCellEventHandler(resultsGrid_CellValueChanged);
            resultsGrid.UserAddedRow += new DataGridViewRowEventHandler(resultsGrid_UserAddedRow);
            resultsGrid.UserDeletedRow += new DataGridViewRowEventHandler(resultsGrid_UserDeletedRow);

            resultsGrid.CellValidating += new DataGridViewCellValidatingEventHandler(resultsGrid_CellValidating);

            btnExport.Click += new EventHandler(btnExport_Click);

            FormClosing += new FormClosingEventHandler(DFMResultsDisplayForm_FormClosing);

            // see http://social.msdn.microsoft.com/forums/en-US/winformsdatacontrols/thread/dc2e02c1-0ef7-4721-a075-75e1b37ba1a9/
            //resultsGrid.EditingControlShowing += new DataGridViewEditingControlShowingEventHandler(resultsGrid_EditingControlShowing);
            // alternative: http://social.msdn.microsoft.com/forums/en-US/winformsdatacontrols/thread/d7347bfd-3392-4c82-8ca7-b60e1515b2e3/ at end with faux column
        }
                
        #region Form State

        public enum FormStates
        {
            Saved,
            Unsaved
        }

        private FormStates formState;
        public FormStates FormState
        {
            get { return formState; }
            private set
            {
                formState = value;
                bool is_saved = formState == FormStates.Saved;
                btnCancel.Enabled = !is_saved;
                btnSave.Enabled = !is_saved;
                btnRefresh.Enabled = is_saved;
            }
        }

        #endregion

        void DFMResultsDisplayForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing || e.CloseReason == CloseReason.None)
            {
                e.Cancel = true;
                if (FormState == FormStates.Unsaved)
                {
                    if (MessageBox.Show("Do you want to discard your changes to the database?", "Confirm", MessageBoxButtons.YesNo) == DialogResult.No) 
                        return;
                    handleCancel();
                }
                this.Hide();
            }
        }        

        void resultsGrid_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            resultsGrid.Rows[e.RowIndex].ErrorText = "";

            // don't validate a new row until it is finished
            if (resultsGrid.Rows[e.RowIndex].IsNewRow) return;

            DateTime date;
            int num_pits;
            int scanId;
            if (e.ColumnIndex == 1 && !DateTime.TryParse(e.FormattedValue.ToString(), out date))
            {
                // date
                e.Cancel = true;
                resultsGrid.Rows[e.RowIndex].ErrorText = "Please enter a valid date.";
            }
            else if (e.ColumnIndex == 2)
            {
                try
                {
                    new PitDepth(e.FormattedValue.ToString());
                }
                catch (Exception)
                {
                    // pit depth
                    e.Cancel = true;
                    resultsGrid.Rows[e.RowIndex].ErrorText = "Please enter a valid pit depth.";
                }
            }
            else if (e.ColumnIndex == 3 && (!Int32.TryParse(e.FormattedValue.ToString(), out num_pits) || num_pits < 0))
            {
                // num pits
                e.Cancel = true;
                resultsGrid.Rows[e.RowIndex].ErrorText = "Please enter a valid number of pits.";
            }
            else if (e.ColumnIndex == 4)
            {
                int pos;
                if (!(Int32.TryParse(e.FormattedValue.ToString(), out pos) && pos >= 0 && pos <= 2))
                {
                    e.Cancel = true;
                    resultsGrid.Rows[e.RowIndex].ErrorText = "Please enter a valid scan position (0, 1 or 2).";
                }


                /*// scan position
                bool valid = false;
                foreach (string position in scanPositions)
                {
                    if (position == e.FormattedValue.ToString())
                    {
                        valid = true;
                        break;
                    }
                }
                if (!valid)
                {
                    e.Cancel = true;
                    resultsGrid.Rows[e.RowIndex].ErrorText = "Please enter a valid position: " + String.Join(", ", scanPositions);
                }*/
            }
            else if (e.ColumnIndex == 5 && !Directory.Exists(e.FormattedValue.ToString()))
            {
                // scan path
                e.Cancel = true;
                resultsGrid.Rows[e.RowIndex].ErrorText = "Please enter a directory that exists.";
            }
            else if (e.ColumnIndex == 6 && !Int32.TryParse(e.FormattedValue.ToString(), out scanId))
            {
                // scan id
                e.Cancel = true;
                resultsGrid.Rows[e.RowIndex].ErrorText = "Please enter a valid scan id.";
            }
        }

        void results_ErrorEncountered(object sender, ErrorEncounteredEventArgs e)
        {
            MessageBox.Show(e.Description, "Error", MessageBoxButtons.OK);
        }

        void resultsGrid_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            handleDataGridChanged();
        }
        void resultsGrid_UserDeletedRow(object sender, DataGridViewRowEventArgs e)
        {
            handleDataGridChanged();
        }
        void resultsGrid_UserAddedRow(object sender, DataGridViewRowEventArgs e)
        {
            handleDataGridChanged();
        }
        private void handleDataGridChanged()
        {
            FormState = FormStates.Unsaved;
        }


        void resultsGrid_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == -1 || e.ColumnIndex >= resultsGrid.Columns.Count) return;

            if (e.RowIndex < 0) return;

            // look for delete button click
            if (e.ColumnIndex == resultsGrid.Columns["deleteColumn"].Index)
            {
                resultsGrid.Rows.RemoveAt(e.RowIndex);
                resultsGrid_CellValueChanged(sender, e);
            }
        }
        
        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {
                results.ResultsDatabase.ErrorEncountered += new EventHandler<ErrorEncounteredEventArgs>(results_ErrorEncountered);
                RefreshDisplay();
                FormState = FormStates.Saved;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        void resultsGrid_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            if (e.ColumnIndex == 1)
                MessageBox.Show("Please enter a valid date");
            else if (e.ColumnIndex == 2)
                MessageBox.Show("Please enter a valid pit depth");
            else if (e.ColumnIndex == 3)
                MessageBox.Show("Please enter a valid number of pits");
            else if (e.ColumnIndex == 4)
               MessageBox.Show("Please enter a valid scan position");
            else if (e.ColumnIndex == 5)
                MessageBox.Show("Please enter a valid scan directory");
            else if (e.ColumnIndex == 6)
                MessageBox.Show("Please enter a valid scan ID");
            else if (e.ColumnIndex >= 7 && e.ColumnIndex <= 8)
            {
                // validated elsewhere
                return;
            }
            else
                MessageBox.Show("Please enter valid data");
            
            e.Cancel = true;
        }

        private void resultsGrid_CellParsing(object sender, DataGridViewCellParsingEventArgs e)
        {
            if (e.ColumnIndex == -1 || e.ColumnIndex >= resultsGrid.Columns.Count || e.RowIndex == -1) return;
            
            // looking for date column
            if (resultsGrid.Columns[e.ColumnIndex].Name == "dateColumn")
            {
                if (e != null && e.Value != null)
                {
                    try
                    {
                        e.Value = DateTime.Parse(e.Value.ToString()).ToBinary();
                        e.ParsingApplied = true;
                    }
                    catch (Exception)
                    {
                        e.ParsingApplied = false;
                    }
                }
            }
            else if (resultsGrid.Columns[e.ColumnIndex].Name == "pitDepthColumn")
            {
                if (e != null && e.Value != null)
                {
                    try
                    {
                        // read it as a pit depth, use pit depth unit by default
                        e.Value = new PitDepth(e.Value.ToString(), PitDepthUnit).ToString();
                        e.ParsingApplied = true;
                    }
                    catch (Exception)
                    {
                        e.ParsingApplied = false;
                    }
                }
            }
            /*else if (resultsGrid.Columns[e.ColumnIndex].Name == "scanPositionColumn")
            {
                if (e != null && e.Value != null)
                {
                    try
                    {
                        bool valid = false;
                        int value = 0;
                        foreach (string position in scanPositions)
                        {
                            if (position == e.Value.ToString())
                            {
                                valid = true;
                                break;
                            }
                            value += 1;
                        }
                        if (valid)
                        {
                            e.Value = value;
                            e.ParsingApplied = true;   
                        }
                    }
                    catch (Exception)
                    {
                        e.ParsingApplied = false;
                    }
                }
            }        */
        }

        private void resultsGrid_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.ColumnIndex == -1 || e.ColumnIndex >= resultsGrid.Columns.Count) return;

            // looking for date column
            if (resultsGrid.Columns[e.ColumnIndex].Name == "dateColumn")
            {
                if (e!= null && e.Value != null && e.Value != e.CellStyle.DataSourceNullValue)
                {
                    try
                    {
                        e.Value = DateTime.FromBinary((long)e.Value).ToString("g");
                        e.FormattingApplied = true;                        
                    }
                    catch (FormatException)
                    {
                        e.FormattingApplied = false;
                    }
                }
            }
            else if (resultsGrid.Columns[e.ColumnIndex].Name == "pitDepthColumn")
            {
                if (e != null && e.Value != null && e.Value != e.CellStyle.DataSourceNullValue)
                {
                    try
                    {
                        // display just pit size
                        e.Value = new PitDepth(e.Value.ToString(), PitDepthUnit).PitSize.ToString("F3");
                        e.FormattingApplied = true;
                    }
                    catch (FormatException)
                    {
                        e.FormattingApplied = false;
                    }
                }
            }
            /*else if (resultsGrid.Columns[e.ColumnIndex].Name == "scanPositionColumn")
            {
                if (e != null && e.Value != null)
                {
                    try
                    {
                        int tmp;
                        bool success = Int32.TryParse(e.Value.ToString(), out tmp);
                        if (success && tmp <= scanPositions.Length && tmp >= 0)
                        {
                            
                            e.Value = scanPositions[tmp];
                            e.FormattingApplied = true;
                        }
                    }
                    catch (Exception)
                    {
                        e.FormattingApplied = false;
                    }
                }
            }*/
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (handleSave()) MessageBox.Show("Changes saved successfully");
            else MessageBox.Show("Unable to save changes to database");
        }
        private bool handleSave()
        {
            bool saved = results.SaveChanges();

            if (saved) FormState = FormStates.Saved;
            return saved;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            handleCancel();
        }
        private void handleCancel()
        {
            results.CancelChanges();
            FormState = FormStates.Saved;
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            RefreshDisplay();
        }

        public void RefreshDisplay()
        {            
            resultsGrid.DataSource = new DataView(results.LoadData());
        }

        void btnExport_Click(object sender, EventArgs e)
        {
            saveExportFile.ShowDialog();
            if (saveExportFile.FileName != "")
            {
                string filename = saveExportFile.FileName;

                // open file
                StreamWriter sw = null;
                try
                {
                    sw = new StreamWriter(filename, false);
                    sw.WriteLine(String.Join(",", new string[] { "Date", pitDepthColumn.HeaderText, "Num Pits", "Scan Position", "Scan Path", "Scan ID", "Comment" }));

                    foreach (DataGridViewRow row in resultsGrid.Rows)
                    {
                        for (int i = 0; i < 7; i++)
                        {
                            // skip id column
                            sw.Write(row.Cells[i + 1].FormattedValue);
                            if (i < 6) sw.Write(",");
                        }
                        sw.WriteLine();
                    }

                    sw.Flush();
                    sw.Close();
                }
                catch (Exception ex)
                {
                    logger.ErrorException("Could not write to '" + filename + "': " + ex.Message, ex);
                    throw new ApplicationException("Could not write to '" + filename + "': " + ex.Message);
                }
                finally
                {
                    if (sw != null)
                        sw.Close();
                }
            }
        }
    }
}
