﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DFM.Results
{
    public class DFMResult
    {
        public int ResultId
        {
            get;
            set;
        }

        public DateTime Date
        {
            get;
            set;
        }

        public PitDepth PitDepth
        {
            get;
            set;
        }

        public int NumPits
        {
            get;
            set;
        }

        public int ScanPosition
        {
            get;
            set;
        }

        public string ScanPath
        {
            get;
            set;
        }

        public int ScanId
        {
            get;
            set;
        }

        public string Comment
        {
            get;
            set;
        }
    }
}
