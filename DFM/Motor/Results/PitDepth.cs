﻿using System;
using System.Collections.Generic;
using System.Text;

using PaintAnalysis.Common;
using NLog;

namespace DFM.Results
{
    public class PitDepth
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public static string DefaultUnit = "micron";

        private double pitSize;
        public double PitSize
        {
            get { return pitSize; }
            set { pitSize = value; }
        }

        private string unit;
        public string Unit
        {
            get { return unit; }
            private set { unit = value; }
        }

        public PitDepth(double pitSize, string unit)
        {
            PitSize = pitSize;
            Unit = unit;
        }

        public PitDepth(string quantity)
        {
            if (!FromString(quantity)) throw new ArgumentException("Invalid quantity passed: " + quantity);
        }

        public PitDepth(string quantity, string defaultUnit)
        {
            if (!FromString(quantity, defaultUnit)) throw new ArgumentException("Invalid quantity passed: " + quantity);
            ChangeUnit(defaultUnit);
        }

        public override string ToString()
        {
            return PitSize.ToString() + " " + Unit;
        }

        public string ToString(int precision)
        {
            return PitSize.ToString("F" + precision.ToString()) + " " + Unit;
        }

        public bool FromString(string quantity)
        {
            return FromString(quantity, PitDepth.DefaultUnit);
        }

        public bool FromString(string quantity, string defaultUnit)
        {
            try
            {
                string[] tmp = quantity.Split(' ');
                PitSize = Convert.ToDouble(tmp[0]);

                if (tmp.Length == 1) Unit = defaultUnit;
                else Unit = tmp[1];
                return true;
            }
            catch (InvalidCastException ex)
            {
                logger.Warn("PitDepth.FromString failed to parse " + quantity + ". " + ex.Message);
                return false;
            }
        }

        public bool ChangeUnit(string destUnit)
        {
            try
            {
                double tmp = UnitConverter.ConvertDistance(Unit, destUnit, PitSize);
                PitSize = tmp;
                Unit = destUnit;
                return true;
            }
            catch (Exception ex)
            {
                logger.Warn("PitDepth.ChangeUnit failed to convert " + ToString() + " to " + destUnit + ". " + ex.Message);
                return false;
            }
        }
    }
}
