﻿using System;
using System.Collections.Generic;
using System.Text;

using System.Data;
using System.Data.SQLite;
using System.IO;
using PaintAnalysis.Common.Results;

namespace DFM.Results
{
    public class DFMResults
    {
        public event EventHandler<ErrorEncounteredEventArgs> ErrorEncountered;
        protected void OnErrorEncountered(ErrorEncounteredEventArgs e)
        {
            if (ErrorEncountered != null)
                ErrorEncountered(this, e);
        }
                
        private SQLiteDataAdapter resultsDataAdapter;
        public DataTable ResultsTable { get; private set; }
        public ResultsDB ResultsDatabase { get; private set; }

        public DFMResults(ResultsDB ResultsDb)
        {
            ResultsDatabase = ResultsDb;
            ResultsTable = ResultsDatabase.CombinedDataSet.Tables.Add("scan_results");

            // forward errors from resultsdb
            ResultsDatabase.ErrorEncountered += new EventHandler<ErrorEncounteredEventArgs>(delegate(object sender, ErrorEncounteredEventArgs e)
            {
                OnErrorEncountered(e);
            });            
        }

        public bool SaveChanges()
        {
            try
            {
                resultsDataAdapter.Update(ResultsTable);
                return true;
            }
            catch (Exception ex)
            {
                OnErrorEncountered(new ErrorEncounteredEventArgs(ex.Message));
                return false;
            }
        }

        public void CancelChanges()
        {
            ResultsTable.RejectChanges();
        }

        public bool AddResultIfNew(DFMResult result)
        {
            // check if it is new
            string query = "SELECT COUNT([result_id]) FROM [scan_results] WHERE [scan_path]='" + result.ScanPath + "' AND [scan_id]=" + result.ScanId;
            string res = ResultsDatabase.Database.ExecuteScalar(query);
            if (res != "")
            {
                // don't handle case when res == ""
                int iRes;
                bool success = Int32.TryParse(res, out iRes);
                if (success && iRes > 0)
                {
                    // this scan already exists here
                    return true;
                }
            }

            if (result.Date == null)
                result.Date = DateTime.Now;

            Dictionary<string, string> data = new Dictionary<string,string>();
            // don't set result_id
            data["date"] = result.Date.ToBinary().ToString();
            data["pit_depth"] = result.PitDepth.ToString();
            data["num_pits"] = result.NumPits.ToString();
            data["scan_position"] = result.ScanPosition.ToString();
            data["scan_path"] = result.ScanPath;
            data["scan_id"] = result.ScanId.ToString();
            data["comment"] = result.Comment;

            return ResultsDatabase.Database.Insert("scan_results", data);
        }
        
        public DataTable LoadData()
        {
            SQLiteConnection cnn = ResultsDatabase.Database.GetConnection();
            
            // command to get data
            SQLiteCommand command = cnn.CreateCommand();
            command.CommandText = "SELECT [result_id], [date], [pit_depth], [num_pits], [scan_position], [scan_path], [scan_id], [comment] FROM scan_results";

            // Create the data adapter using our select command
            resultsDataAdapter = new SQLiteDataAdapter(command);

            // The command builder will take care of our update, insert, deletion commands
            SQLiteCommandBuilder commandBuilder = new SQLiteCommandBuilder(resultsDataAdapter);

            // Create a dataset and fill it with our records
            ResultsTable.Clear();
            resultsDataAdapter.Fill(ResultsTable);

            return ResultsTable;
        }

        public double GetPitDepthScaleFactor()
        {
            string res = ResultsDatabase.Database.ExecuteScalar("SELECT [value] FROM [keystore] WHERE [key] = 'pit_depth_scale_factor';");
            if (res != "")
            {
                double output;
                if (Double.TryParse(res, out output)) return output;            
            }

            // otherwise, set default of 1.0
            SetPitDepthScaleFactor(1.0);
            return 1.0;            
        }

        private void SetPitDepthScaleFactor(double scaleFactor)
        {
            string query = "INSERT OR REPLACE INTO [keystore] ([key], [value]) VALUES ('pit_depth_scale_factor', '" + scaleFactor.ToString() + "');";
            int rows = ResultsDatabase.Database.ExecuteNonQuery(query);
        }
    }
}
