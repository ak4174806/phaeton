﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DFM.Motor
{
    public class MockPollMotor : IPollMotor
    {
        public MockPollMotor()
        {
            Status = new StatusWord();
            Homed = new HomedWord();
            Fault = new FaultWord();
            TargetA = -1;
            LeftEdge = RightEdge = -1;
            ClearError();
            RunPoll = false;
        }

        public event PollMotorStatusChangedHandler StatusChanged;
        public event PollMotorHomedChangedHandler HomedChanged;
        public event PollMotorFaultChangedHandler FaultChanged;
        public event PollMotorErrorHandler PollMotorError;
        public event PollMotorDataChangedHandler PollDataChanged;

        protected virtual void OnStatusChanged(StatusChangedEventArgs e)
        {
            if (StatusChanged != null)
            {
                // Invokes the delegates.
                StatusChanged(this, e);
            }
        }
        protected virtual void OnHomedChanged(HomedChangedEventArgs e)
        {
            if (HomedChanged != null)
            {
                // Invokes the delegates.
                HomedChanged(this, e);
            }
        }
        protected virtual void OnFaultChanged(FaultChangedEventArgs e)
        {
            if (FaultChanged != null)
            {
                // Invokes the delegates.
                FaultChanged(this, e);
            }
        }
        protected virtual void OnPollMotorError(MotorErrorEventArgs e)
        {
            if (PollMotorError != null)
            {
                // Invokes the delegates.
                PollMotorError(this, e);
            }
        }
        protected virtual void OnPollDataChanged(DataChangedEventArgs e)
        {
            if (PollDataChanged != null)
            {
                // Invokes the delegates.
                PollDataChanged(this, e);
            }
        }

        public StatusWord Status { get; protected set; }
        public HomedWord Homed { get; protected set; }
        public FaultWord Fault { get; protected set; }
        public int TargetA { get; set; }
        public int LeftEdge { get; set; }
        public int RightEdge { get; set; }
        public bool IsError { get; protected set; }
        public Exception Error { get; protected set; }
        public bool RunPoll { get; set; }
        
        public void ClearError()
        {
            IsError = false;
            Error = null;
        }

        public void raiseStatusChanged()
        {
            OnStatusChanged(new StatusChangedEventArgs(Status));
        }
    }
}
