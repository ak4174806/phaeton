﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DFM.Motor
{
    public class StatusWord
    {
        public bool Homing = false;
        public bool Homed = false;
        public bool Fault = false;
        public bool Busy = false;
        public bool EdgesFound = false;
        public bool HomeA = false;
        public bool EdgeRight = false;
        public bool Painting = false;

        public int State = 0;

        public bool ReadyToMove
        {
            get { return Homed && !Homing && !Fault && !Busy && EdgesFound; }
        }

        public bool IsReset
        {
            get { return !(Homing || Homing || Fault || Busy || EdgesFound || HomeA || EdgeRight); }
        }
    }

    public class HomedWord
    {
        public bool HomedA = false;
        public bool HomedB = false;

        public int State = 0;
    }

    public class FaultWord
    {
        public bool HomeAFailed = false;
        public bool HomeBFailed = false;
        public bool NotHomed = false;
        public bool Estop = false;
        public bool ChecksumFault = false;
        public bool AlreadyHoming = false;
        public bool LimitReached = false;
        public bool APositionalError = false;
        public bool BPositionalError = false;
        public bool ProgramError = false;
        public bool LeftEdgeNotFound = false;
        public bool RightEdgeNotFound = false;
        public bool NoEdgeScan = false;

        public int State = 0;

        public string Message
        {
            get { return ""; }
        }
    }
}
