﻿using System;
using System.Collections.Generic;
using System.Text;

using Galil;
using System.Threading;
using System.ComponentModel;
using System.Runtime.InteropServices;

namespace DFM.Motor
{
    public class PollMotor : IPollMotor
    {
        private BackgroundWorker poller;
        protected Galil.Galil g;

        #region Events
        public event PollMotorStatusChangedHandler StatusChanged;
        public event PollMotorHomedChangedHandler HomedChanged;
        public event PollMotorFaultChangedHandler FaultChanged;
        public event PollMotorDataChangedHandler PollDataChanged;
        public event PollMotorErrorHandler PollMotorError;

        protected virtual void OnStatusChanged(StatusChangedEventArgs e)
        {
            if (StatusChanged != null)
            {
                // Invokes the delegates.
                StatusChanged(this, e);
            }
        }
        protected virtual void OnHomedChanged(HomedChangedEventArgs e)
        {
            if (HomedChanged != null)
            {
                // Invokes the delegates.
                HomedChanged(this, e);
            }
        }
        protected virtual void OnFaultChanged(FaultChangedEventArgs e)
        {
            if (FaultChanged != null)
            {
                // Invokes the delegates.
                FaultChanged(this, e);
            }
        }

        protected virtual void OnPollMotorError(MotorErrorEventArgs e)
        {
            if (PollMotorError != null)
            {
                // Invokes the delegates.
                PollMotorError(this, e);
            }
        }

        protected virtual void OnPollDataChanged(DataChangedEventArgs e)
        {
            if (PollDataChanged != null)
            {
                // Invokes the delegates.
                PollDataChanged(this, e);
            }
        }
        #endregion

        protected StatusWord status;
        public StatusWord Status
        {
            get
            {
                return status;
            }
        }

        protected HomedWord homed;
        public HomedWord Homed
        {
            get
            {
                return homed;
            }
        }

        protected FaultWord fault;
        public FaultWord Fault
        {
            get
            {
                return fault;
            }
        }

        private int targetA;
        public int TargetA
        {
            get { return targetA; }
        }

        private int leftEdge;
        public int LeftEdge
        {
            get { return leftEdge; }
        }

        private int rightEdge;
        public int RightEdge
        {
            get { return rightEdge; }
        }

        protected bool isError;
        public bool IsError
        {
            get { return isError; }
        }

        protected Exception error;
        public Exception Error
        {
            get { return error; }
        }

        public PollMotor(Galil.Galil g)
        {
            status = new StatusWord();
            homed = new HomedWord();
            fault = new FaultWord();
            targetA = -1;
            leftEdge = rightEdge = -1;

            ClearError();

            this.g = g;

            // set up background worker
            poller = new BackgroundWorker();
            poller.WorkerReportsProgress = true;
            poller.WorkerSupportsCancellation = true;

            poller.ProgressChanged += new ProgressChangedEventHandler(poller_ProgressChanged);
            poller.DoWork += new DoWorkEventHandler(poller_DoWork);
        }

        ~PollMotor()
        {
            stopWorker();
        }

        public void ClearError()
        {
            isError = false;
            error = null;
        }

        protected void throwError(Exception ex)
        {
            isError = true;
            error = ex;

            MotorErrorEventArgs e = new MotorErrorEventArgs(ex);
            OnPollMotorError(e);
        }

        protected void stopWorker()
        {
            if (poller.IsBusy)
            {
                poller.CancelAsync();

                // try kill it, quadratic wait times
                for (int i = 1; i <= 5; i++)
                {
                    if (poller.IsBusy) Thread.Sleep(5 * i * i);
                    else break;
                }
            }
        }

        protected bool runPoll = false;
        public bool RunPoll
        {
            get { return runPoll; }
            set
            {
                runPoll = value;

                if (runPoll && !poller.IsBusy)
                    poller.RunWorkerAsync();
                else if (!runPoll && poller.IsBusy)
                    stopWorker();
            }
        }

        protected enum WordChanged
        {
            Status,
            Homed,
            Fault,
            DataWord
        }

        void poller_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = sender as BackgroundWorker;
            int istatus = 0, ihomed = 0, ifault = 0;
            string response = "";

            // find initial values of state flags
            try
            {
                response = g.command("S_WORD1=");
                istatus = Convert.ToInt32(Convert.ToDecimal(response));

                response = g.command("H_WORD1=");
                ihomed = Convert.ToInt32(Convert.ToDecimal(response));

                response = g.command("F_WORD1=");
                ifault = Convert.ToInt32(Convert.ToDecimal(response));                
            }
            catch (FormatException ex)
            {
                worker.ReportProgress(0, new ApplicationException("Could not convert state flags '" + response + "' on Galil to integers: " + ex.Message, ex));
            }
            catch (COMException ex)
            {
                worker.ReportProgress(0, new ApplicationException("Could not read state flags from Galil: " + ex.Message, ex));
            }
            catch (Exception ex)
            {
                worker.ReportProgress(0, new ApplicationException("Could not read initial values of status flags: " + ex.Message, ex));
            }

            bool firstRun = true;
            int nstatus = 0, nhomed = 0, nfault = 0, ntargetA = 0, nLeftEdge = 0, nRightEdge = 0;
            while (true)
            {
                if (worker.CancellationPending) break;
                Thread.Sleep(20);

                try
                {
                    response = g.command("S_WORD1=");
                    nstatus = Convert.ToInt32(Convert.ToDecimal(response));
                }
                catch (Exception ex)
                {
                    worker.ReportProgress(0, new ApplicationException("Error polling status word" + ex.Message, ex));
                    continue;
                }

                if (nstatus != istatus || firstRun)
                {
                    StatusWord nword = new StatusWord();
                    nword.Homing = (nstatus & 1) > 0;
                    nword.Homed = (nstatus & 2) > 0;
                    nword.Fault = (nstatus & 4) > 0;
                    nword.Busy = (nstatus & 8) > 0;
                    nword.EdgesFound = (nstatus & 16) > 0;
                    nword.HomeA = (nstatus & 32) > 0;
                    nword.EdgeRight = (nstatus & 64) > 0;
                    nword.Painting = (nstatus & 128) > 0;

                    nword.State = nstatus;
                    istatus = nstatus;
                    lock (status)
                    {
                        status = nword;
                    }

                    worker.ReportProgress(-1, WordChanged.Status);
                }

                try
                {
                    response = g.command("H_WORD1=");
                    nhomed = Convert.ToInt32(Convert.ToDecimal(response));
                }
                catch (Exception ex)
                {
                    worker.ReportProgress(0, new ApplicationException("Error polling homed word" + ex.Message, ex));
                    continue;
                }

                if (nhomed != ihomed || firstRun)
                {
                    HomedWord nword = new HomedWord();
                    nword.HomedA = (nhomed & 1) > 0;
                    nword.HomedB = (nhomed & 2) > 0;

                    nword.State = nhomed;
                    ihomed = nhomed;
                    lock (homed)
                    {
                        homed = nword;
                    }

                    worker.ReportProgress(-1, WordChanged.Homed);
                }

                try
                {
                    response = g.command("F_WORD1=");
                    nfault = Convert.ToInt32(Convert.ToDecimal(response));
                }
                catch (Exception ex)
                {
                    worker.ReportProgress(0, new ApplicationException("Error polling fault word" + ex.Message, ex));
                    continue;
                }

                if (nfault != ifault || firstRun)
                {
                    FaultWord nword = new FaultWord();
                    nword.HomeAFailed = (nfault & 1) > 0;
                    nword.HomeBFailed = (nfault & 2) > 0;
                    nword.NotHomed = (nfault & 4) > 0;
                    nword.Estop = (nfault & 8) > 0;
                    nword.ChecksumFault = (nfault & 16) > 0;
                    nword.AlreadyHoming = (nfault & 32) > 0;
                    nword.LimitReached = (nfault & 64) > 0;
                    nword.APositionalError = (nfault & 128) > 0;
                    nword.BPositionalError = (nfault & 256) > 0;
                    nword.ProgramError = (nfault & 512) > 0;
                    nword.LeftEdgeNotFound = (nfault & 1024) > 0;
                    nword.NoEdgeScan = (nfault & 2048) > 0;
                    nword.RightEdgeNotFound = (nfault & 4096) > 0;

                    nword.State = nfault;
                    ifault = nfault;
                    lock (fault)
                    {
                        fault = nword;
                    }

                    worker.ReportProgress(-1, WordChanged.Fault);
                }

                try
                {
                    // no event raised if this changes
                    response = g.command("TARGET_A=");
                    ntargetA = Convert.ToInt32(Convert.ToDecimal(response));

                    // no event raised if this changes
                    response = g.command("EDGE_LT=");
                    nLeftEdge = Convert.ToInt32(Convert.ToDecimal(response));

                    // no event raised if this changes
                    response = g.command("EDGE_RT=");
                    nRightEdge = Convert.ToInt32(Convert.ToDecimal(response));
                }
                catch (Exception ex)
                {
                    worker.ReportProgress(0, new ApplicationException("Error polling data words: " + ex.Message, ex));
                    continue;
                }
                bool dataWordChanged = false;
                if (targetA != ntargetA || firstRun)
                {
                    targetA = ntargetA;
                    dataWordChanged = true;
                }
                if (leftEdge != nLeftEdge || firstRun)
                {
                    leftEdge = nLeftEdge;
                    dataWordChanged = true;
                }
                if (rightEdge != nRightEdge || firstRun)
                {
                    rightEdge = nRightEdge;
                    dataWordChanged = true;
                }

                if (dataWordChanged) worker.ReportProgress(-1, WordChanged.DataWord);

                firstRun = false;
            }
        }

        void poller_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if (e.ProgressPercentage == -1)
            {
                // indicates a state word changed
                WordChanged change = (WordChanged)e.UserState;

                if (change == WordChanged.Status)
                    OnStatusChanged(new StatusChangedEventArgs(Status));
                else if (change == WordChanged.Homed)
                    OnHomedChanged(new HomedChangedEventArgs(Homed));
                else if (change == WordChanged.Fault)
                    OnFaultChanged(new FaultChangedEventArgs(Fault));
                else if (change == WordChanged.DataWord)
                    OnPollDataChanged(new DataChangedEventArgs(LeftEdge, RightEdge, TargetA));
            }
            else if (e.ProgressPercentage == 0)
            {
                // indicates an error
                Exception ex = (Exception)e.UserState;
                throwError(ex);
            }
        }
    }
}
