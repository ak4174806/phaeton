﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SQLite;

namespace DFM
{
    public partial class AlgoConfiguration : Form
    {
        long Flag;
        
        public AlgoConfiguration()
        {
            InitializeComponent();
            cmbAlgo.SelectedIndex = 0;
            Flag = 0;
        }
        
        public AlgoConfiguration(long edit)
        {
            InitializeComponent();
            Flag = edit;
            //cmbAlgo.SelectedIndex = 0;
          // this.FormClosing += new FormClosingEventHandler(AlgoConfig_closing);
        }

       /* void AlgoConfig_closing(object sender, FormClosingEventArgs e)
        {
            //ScanDataGrid.DataSource = null;
            DFM_Simple_Settings_Form dfmSetting = new DFM_Simple_Settings_Form();
            
            dfmSetting.LoadData();

            //MessageBox.Show("closed");
        }
      */  

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void cmbAlgo_SelectedIndexChanged(object sender, EventArgs e)
        {
            //for algorithm1
            if (cmbAlgo.SelectedIndex == 0)
            {
                grpBxD.Enabled = false;
                lblDistanceBwPits.Enabled = false;
                nmDistanceBwPits.Enabled = false;
                lblPercentage.Enabled = false;
                nmPercentage.Enabled = false;
                lblThresholdValue.Enabled = false;
                nmThresholdValue.Enabled = true;
                lblAvgPoints.Enabled = false;
                nmAvgPoints.Enabled = false;
                //nmHeight.Enabled = false;
                chkHeightOptional.Enabled = false;
                grpBoxHeightOption.Enabled = false;
                //rdForAllPits.Enabled = false;
                //rdForPit1.Enabled = false;
                lblRange.Enabled = false;
                nmRange.Enabled = false;
                lblFactor.Enabled = false;
                nmFactor.Enabled = false;
                lblSmoothingWidth.Enabled = true;
                nmSmoothingWidth.Enabled = true;
                lblMinPitWidth.Enabled = true;
                nmMinPitWidth.Enabled = true;
                lblCompressionFactor.Enabled = true;
                nmCompressionFactor.Enabled = true;
                lblPointsInEachPit.Enabled = true;
                nmPointsInEachPit.Enabled = true;
                grpbxSurfaceRoughness.Enabled = false;
                //lblOffsetValue.Enabled = false;
                //nmOffsetValue.Enabled = false;
                //lblAveragingNum.Enabled = false;
                //nmAvgNum.Enabled = false;
                lblSubstrateRoughness.Enabled = true;
                nmSubstrateRoghness.Enabled = true;
            
            }
                //for algorithm 2
            else if (cmbAlgo.SelectedIndex == 1)
            {
                grpBxD.Enabled = true;
                lblDistanceBwPits.Enabled = true;
                nmDistanceBwPits.Enabled = true;
                lblPercentage.Enabled = true;
                nmPercentage.Enabled = true;
                lblThresholdValue.Enabled = false;
                nmThresholdValue.Enabled = false;
                lblAvgPoints.Enabled = false;
                nmAvgPoints.Enabled = false;
               // nmHeight.Enabled = true;
                chkHeightOptional.Enabled = true;
                if (chkHeightOptional.Checked == true)
                {
                    grpBoxHeightOption.Enabled = true;
                }
                else
                {
                    grpBoxHeightOption.Enabled = false;
                }
                // rdForAllPits.Enabled = true;
               // rdForPit1.Enabled = true;
                lblRange.Enabled = false;
                nmRange.Enabled = false;
                lblFactor.Enabled = false;
                nmFactor.Enabled = false;
                lblSmoothingWidth.Enabled = false;
                nmSmoothingWidth.Enabled = false;
                lblMinPitWidth.Enabled = false;
                nmMinPitWidth.Enabled = false;
                lblCompressionFactor.Enabled = false;
                nmCompressionFactor.Enabled = false;
                lblPointsInEachPit.Enabled = false;
                nmPointsInEachPit.Enabled = false;
                grpbxSurfaceRoughness.Enabled = false;
                //lblOffsetValue.Enabled = false;
                //nmOffsetValue.Enabled = false;
                //lblAveragingNum.Enabled = false;
                //nmAvgNum.Enabled = false;
                lblSubstrateRoughness.Enabled = true;
                nmSubstrateRoghness.Enabled = true;
            

            }
                //for algorithm 3
            else if (cmbAlgo.SelectedIndex == 2)
            {
                grpBxD.Enabled = true;
                lblDistanceBwPits.Enabled = false;
                nmDistanceBwPits.Enabled = false;
                lblPercentage.Enabled = false;
                nmPercentage.Enabled = false;
                lblThresholdValue.Enabled = true;
                nmThresholdValue.Enabled = true;
                lblAvgPoints.Enabled = true;
                nmAvgPoints.Enabled = true;
                //nmHeight.Enabled = false;
                chkHeightOptional.Enabled = false;
                grpBoxHeightOption.Enabled = false;
                grpBoxHeightOption.Enabled = false;
                //rdForAllPits.Enabled = false;
                //rdForPit1.Enabled = false;
                lblRange.Enabled = false;
                nmRange.Enabled = false;
                lblFactor.Enabled = false;
                nmFactor.Enabled = false;
                lblSmoothingWidth.Enabled = false;
                nmSmoothingWidth.Enabled = false;
                lblMinPitWidth.Enabled = false;
                nmMinPitWidth.Enabled = false;
                lblCompressionFactor.Enabled = false;
                nmCompressionFactor.Enabled = false;
                lblPointsInEachPit.Enabled = false;
                nmPointsInEachPit.Enabled = false;
                grpbxSurfaceRoughness.Enabled = false;
                //lblOffsetValue.Enabled = false;
                //nmOffsetValue.Enabled = false;
                //lblAveragingNum.Enabled = false;
                //nmAvgNum.Enabled = false;
                lblSubstrateRoughness.Enabled = true;
                nmSubstrateRoghness.Enabled = true;
            
            }
            //for algorithm 4
            else if (cmbAlgo.SelectedIndex == 3)
            {
                grpBxD.Enabled = true;
                lblDistanceBwPits.Enabled = true;
                nmDistanceBwPits.Enabled = true;
                lblPercentage.Enabled = true;
                nmPercentage.Enabled = true;
                lblThresholdValue.Enabled = false;
                nmThresholdValue.Enabled = false;
                lblAvgPoints.Enabled = false;
                nmAvgPoints.Enabled = false;
                //nmHeight.Enabled = true;
                chkHeightOptional.Enabled = true;
                if (chkHeightOptional.Checked == true)
                {
                    grpBoxHeightOption.Enabled = true;
                }
                else
                {
                    grpBoxHeightOption.Enabled = false;
                }
               // rdForAllPits.Enabled = true;
               // rdForPit1.Enabled = true;
                lblRange.Enabled = true;
                nmRange.Enabled = true;
                lblFactor.Enabled = true;
                nmFactor.Enabled = true;
                lblSmoothingWidth.Enabled = false;
                nmSmoothingWidth.Enabled = false;
                lblMinPitWidth.Enabled = false;
                nmMinPitWidth.Enabled = false;
                lblCompressionFactor.Enabled = false;
                nmCompressionFactor.Enabled = false;
                lblPointsInEachPit.Enabled = false;
                nmPointsInEachPit.Enabled = false;
                grpbxSurfaceRoughness.Enabled = false;
                //lblOffsetValue.Enabled = false;
                //nmOffsetValue.Enabled = false;
                //lblAveragingNum.Enabled = false;
                //nmAvgNum.Enabled = false;
                lblSubstrateRoughness.Enabled = true;
                nmSubstrateRoghness.Enabled = true;
            

            }
                //for surface Roughness
            else if (cmbAlgo.SelectedIndex == 4)
            {
                grpBxD.Enabled = false;
                lblDistanceBwPits.Enabled = false;
                nmDistanceBwPits.Enabled = false;
                lblPercentage.Enabled = false;
                nmPercentage.Enabled = false;
                lblThresholdValue.Enabled = false;
                nmThresholdValue.Enabled = false;
                lblAvgPoints.Enabled = false;
                nmAvgPoints.Enabled = false;
                //nmHeight.Enabled = false;
                chkHeightOptional.Enabled = false;
                grpBoxHeightOption.Enabled = false;
                lblSubstrateRoughness.Enabled = false;
                nmSubstrateRoghness.Enabled = false;
                //rdForAllPits.Enabled = false;
                //rdForPit1.Enabled = false;
                lblRange.Enabled = false;
                nmRange.Enabled = false;
                lblFactor.Enabled = false;
                nmFactor.Enabled = false;
                lblSmoothingWidth.Enabled = false;
                nmSmoothingWidth.Enabled = false;
                lblMinPitWidth.Enabled = false;
                nmMinPitWidth.Enabled = false;
                lblCompressionFactor.Enabled = false;
                nmCompressionFactor.Enabled = false;
                lblPointsInEachPit.Enabled = false;
                nmPointsInEachPit.Enabled = false;
                grpbxSurfaceRoughness.Enabled = true;
                //lblOffsetValue.Enabled = true;
                //nmOffsetValue.Enabled = true;
                //lblAveragingNum.Enabled = true;
                //nmAvgNum.Enabled = true;


            }
        }

        
        private void btnSave_Click(object sender, EventArgs e)
        {
           // MessageBox.Show(Flag.ToString());

            // Warning message if PaintType is Blank
            if (txtPaint.Text == "")
            {
                // MessageBox.Show("Enter Paint Type");
                lblInvalidType.Visible = true;
                lblInvalidType.Text = "Enter Paint type!";
                lblInvalidType.ForeColor = Color.Red;
                return;

            }



            //set variable if HeightOption is enable or not
            string HeightOption = "All";
            if (rdForAllPits.Checked == true)
            {
                HeightOption = "All";
            }
            else if (rdForPit1.Checked == true)
            {
                HeightOption = "First";
            }


            string HeightEnable = "No";
            if (chkHeightOptional.Checked == true)
            {
                HeightEnable = "Yes";
            }
            else if (chkHeightOptional.Checked == false)
            {
                HeightEnable = "No";
            }


            if (Flag == 0)
            {

                // Same PaintName not allowed
                try
                {

                    SQLiteConnection con = new SQLiteConnection("Data Source=ScanDataBase.s3db");
                    con.Open();
                    SQLiteCommand com = new SQLiteCommand();
                    com.Connection = con;
                    // int f = 50;
                    // get all pit values of scan
                    com.CommandText = "select PaintName from tbl_paint_type where PaintName='" + txtPaint.Text + "'";
                    com.ExecuteNonQuery();
                    SQLiteDataReader dr = com.ExecuteReader();

                    if (dr.HasRows)
                    {
                        lblInvalidType.Visible = true;
                        lblInvalidType.Text = "Already Exist!";
                        lblInvalidType.ForeColor = Color.Red;
                        dr.Close();
                        con.Close();
                        return;
                    }
                    else
                    {
                        dr.Close();
                        con.Close();
                    }
                }
                catch (SQLiteException ex)
                {
                    MessageBox.Show(ex.Message);
                }
         


                    // Add paint Type into database
                    try
                    {
                        SQLiteConnection con = new SQLiteConnection("Data Source=ScanDataBase.s3db");
                        con.Open();
                        SQLiteCommand com = new SQLiteCommand();
                        com.Connection = con;
                        // MessageBox.Show(System.DateTime.Now.ToString()); 
                        com.CommandText = "insert into tbl_paint_type(PaintName,Algorithm,NoPointAvg,NoPointBwAvg,DistanceBwPits,Percentage,ThresholdValue,AveragePoints,Range,Factor,HeightOnly4First,Height,SubstrateRoughness,SmoothingWidth,MinPitWidth,CompressionFactor,PointsInEachPit,HeightEnable,OffsetValue,AveragingNum,NoPointsForYmaxAvg,NoCountBackPts) values('" + txtPaint.Text + "','" + cmbAlgo.SelectedItem.ToString() + "','" + nmNoPoints4Avg.Value + "','" + nmNoPointsBwAvgs.Value + "','" + nmDistanceBwPits.Value + "','" + nmPercentage.Value + "','" + nmThresholdValue.Value + "','" + nmAvgPoints.Value + "','" + nmRange.Value + "','" + nmFactor.Value + "','" + HeightOption.ToString() + "','" + nmHeight.Value + "','" + nmSubstrateRoghness.Value + "','" + nmSmoothingWidth.Value + "','" + nmMinPitWidth.Value + "','" + nmCompressionFactor.Value + "','" + nmPointsInEachPit.Value + "','" + HeightEnable.ToString() + "','" + nmOffsetValue.Value + "','" + nmAvgNum.Value + "','" + nmNoCountBackPts.Value + "','" + NoPointsforYmaxAvg.Value + "')";
                        com.ExecuteNonQuery();
                        con.Close();
                    }
                    catch (SQLiteException ex)
                    {
                        MessageBox.Show(ex.Message);
                    }

            }
            else
            {

                // Same PaintName not allowed
                try
                {

                    SQLiteConnection con = new SQLiteConnection("Data Source=ScanDataBase.s3db");
                    con.Open();
                    SQLiteCommand com = new SQLiteCommand();
                    com.Connection = con;
                    // int f = 50;
                    // get all pit values of scan
                    com.CommandText = "select PaintName from tbl_paint_type where PaintName='" + txtPaint.Text + "' and id not in ("+Flag+")";
                    com.ExecuteNonQuery();
                    SQLiteDataReader dr = com.ExecuteReader();

                    if (dr.HasRows)
                    {
                        lblInvalidType.Visible = true;
                        lblInvalidType.Text = "Already Exist!";
                        lblInvalidType.ForeColor = Color.Red;
                        dr.Close();
                        con.Close();
                        return;
                    }
                    else 
                    {
                        dr.Close();
                        con.Close();
                    }
                    
                }
                catch (SQLiteException ex)
                {
                    MessageBox.Show(ex.Message);
                }
         


               
                //update paint Type into database
                try
                {
                    SQLiteConnection con = new SQLiteConnection("Data Source=ScanDataBase.s3db");
                    con.Open();
                    SQLiteCommand com = new SQLiteCommand();
                    com.Connection = con;
                    // MessageBox.Show(System.DateTime.Now.ToString()); 
                    com.CommandText = "update tbl_paint_type set PaintName='" + txtPaint.Text + "',Algorithm='" + cmbAlgo.SelectedItem + "',NoPointAvg=" + nmNoPoints4Avg.Value + ",NoPointBwAvg=" + nmNoPointsBwAvgs.Value + ",DistanceBwPits=" + nmDistanceBwPits.Value + ",Percentage=" + nmPercentage.Value + ",ThresholdValue='" + nmThresholdValue.Value + "',AveragePoints=" + nmAvgPoints.Value + ",Range=" + nmRange.Value + ",Factor='" + nmFactor.Value + "',HeightOnly4First='" + HeightOption.ToString() + "',Height=" + nmHeight.Value + ",SubstrateRoughness='" + nmSubstrateRoghness.Value + "',SmoothingWidth=" + nmSmoothingWidth.Value + ",MinPitWidth=" + nmMinPitWidth.Value + ",CompressionFactor='" + nmCompressionFactor.Value + "',PointsInEachPit=" + nmPointsInEachPit.Value + ",HeightEnable='" + HeightEnable.ToString() + "',OffsetValue='" + nmOffsetValue.Value + "',AveragingNum=" + nmAvgNum.Value + ",NoPointsForYmaxAvg=" + NoPointsforYmaxAvg.Value + ",NoCountBackPts=" + nmNoCountBackPts.Value + " where id=" + Flag;
                    com.ExecuteNonQuery();
                    con.Close();
                }
                catch (SQLiteException ex)
                {
                    MessageBox.Show(ex.Message);
                }

            }


                this.Close();
               // DFM_Simple_Settings_Form dfmSetting = new DFM_Simple_Settings_Form();
               // dfmSetting.ScanDataGrid.DataSource = null;
               // dfmSetting.ScanDataGrid.Rows.Clear();
                //dfmSetting.ScanDataGrid.Refresh();

                //dfmSetting.ScanDataGrid.Rows.Clear();
            //dfmSetting.LoadData();
             // dfmSetting.ScanDataGrid.DataSource = new DataView(dfmSetting.LoadData());
                //dfmSetting.ScanDataGrid.DataSource = null;
                //ParentForm.Refresh();
            

               // dfmSetting.PaintTypeLoad();

           

            
       
        }
            
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void AlgoConfiguration_Load(object sender, EventArgs e)
        {

        }

        private void nmRange_ValueChanged(object sender, EventArgs e)
        {

        }

        private void grpBxD_Enter(object sender, EventArgs e)
        {

        }

        private void numericUpDown4_ValueChanged(object sender, EventArgs e)
        {

        }

        private void txtPaint_TextChanged(object sender, EventArgs e)
        {
            lblInvalidType.Visible = false;
        }

        private void chkHeightOptional_CheckedChanged(object sender, EventArgs e)
        {
            if (chkHeightOptional.Checked == true)
            {
                grpBoxHeightOption.Enabled = true;
            }
            else if (chkHeightOptional.Checked == false)
            {
                grpBoxHeightOption.Enabled = false;
            }

        }

        private void tmrFade_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            this.Opacity += 0.10;
            if (this.Opacity >= .95)
            {
                this.Opacity = 1;
                tmrFade.Enabled = false;
            }
        }

        private void label19_Click(object sender, EventArgs e)
        {

        }
    }
}
