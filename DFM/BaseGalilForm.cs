﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using DFM.Motor;
using System.IO;
using System.Threading;

namespace DFM
{
    public partial class BaseGalilForm : Form
    {
        public BaseGalilForm()
        {
            InitializeComponent();

            m = new MotorControl();
            m.SetupStateChanged += new MotorSetupStateHandler(m_SetupStateChanged);
            m.FinishedSetup += new MotorTaskHandler(m_FinishedSetup);
            m.FinishedMove += new MotorTaskHandler(m_FinishedMove);
            m.Message += new MotorMessageHandler(m_Message);
        
            m.PollState.PollMotorError += new PollMotorErrorHandler(PollState_PollMotorError);
            m.PollState.StatusChanged += new PollMotorStatusChangedHandler(PollState_StatusChanged);
            m.PollState.HomedChanged += new PollMotorHomedChangedHandler(PollState_HomedChanged);
            m.PollState.FaultChanged += new PollMotorFaultChangedHandler(PollState_FaultChanged);

            this.Shown += new EventHandler(delegate(object sender, EventArgs e)
            {
                refreshAddresses();
                refreshEdges();
            });
        }        

        void PollState_FaultChanged(object sender, FaultChangedEventArgs e)
        {
            textBox3.AppendText(Environment.NewLine + "Fault changed: " + e.Fault.State.ToString());
        }

        void PollState_StatusChanged(object sender, StatusChangedEventArgs e)
        {
            textBox3.AppendText(Environment.NewLine + "Status changed: " + e.Status.State.ToString());
        }

        void PollState_HomedChanged(object sender, HomedChangedEventArgs e)
        {
            textBox3.AppendText(Environment.NewLine + "Homed changed: " + e.Homed.State.ToString());
        }

        void PollState_PollMotorError(object sender, MotorErrorEventArgs e)
        {
            textBox3.AppendText(Environment.NewLine + "Polling Error: " + e.Error.ToString());
            if (m.PollState.RunPoll)
            {
                m.PollState.RunPoll = false;
                m.Disconnect();
            }
        }

        public MotorControl m;

        protected bool firstRun = true;

        public virtual bool Connect(string address)
        {
            try
            {
                print("Connecting ...");
                m.Connect(address);
                print("Connected");

                print("Testing connection ...");
                bool result = m.TestConnection();
                print("Result = " + result.ToString());
            }
            catch (Exception ex)
            {
                print("Could not open: " + ex.Message);
                return false;
            }

            print("");

            try
            {
                print("Testing program exists ...");
                bool result = m.TestProgram();

                if (result && firstRun) print("It does! Downloading again anyway ...");
                else if (result) print("It does!");
                else print("It doesn't! Downloading ...");

                if (!result || firstRun)
                {
                    string fname = Path.Combine(Path.GetDirectoryName(Application.ExecutablePath), "galil program.dmc");
                    if (!File.Exists(fname))
                    {
                        print("Could not find file: " + fname);
                        return false;
                    }
                    result = m.DownloadProgramFile(fname);
                    if (result) print("Downloaded " + fname);
                    else print("Download failed ...");

                    print("Testing program exists ...");
                    result = m.TestProgram();

                    if (result) print("It does!");
                    else
                    {
                        print("It doesn't! Huh?");
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                print("Error testing program: " + ex.Message);
                return false;
            }

            firstRun = false;
            print("");
            return true;
        }

        public bool Connect()
        {
            if (cmbAddress.Items.Count == 0)
                refreshAddresses();

            if (cmbAddress.SelectedIndex > -1)
                return Connect(cmbAddress.SelectedItem.ToString());
            else
            {
                print("Please select an address to try and connect to");
                return false;
            }
        }

        public bool IsConnected
        {
            get
            {
                if (firstRun) return false;

                try
                {
                    print("Testing connection ...");
                    bool result = m.TestConnection();
                    print("Result = " + result.ToString());
                    return true;
                }
                catch (Exception ex)
                {
                    print("Could not open: " + ex.Message);
                    return false;
                }
            }
        }

        void print(string s)
        {
            textBox1.AppendText(Environment.NewLine + s);
        }

        void m_Message(object sender, MessageEventArgs e)
        {
            textBox2.AppendText(Environment.NewLine + e.Message);
        }

        public virtual void Disconnect()
        {
            print("Disconnecting ...");
            try
            {
                m.Disconnect();
            }
            catch { }
            print("Disconnected");
        }

        public virtual void Setup()
        {            
            try
            {
                print("Setting up ...");
                m.Setup();
            }
            catch (Exception ex)
            {
                print("Could not setup: " + ex.Message);
                return;
            }
        }

        void m_FinishedSetup(object sender, MotorTaskEventArgs e)
        {
            if (!e.Success)
            {
                print("Setup failed " + e.Error.Message);
                return;
            }

            refreshEdges();

            print("Success - set up finished");
            print("Left edge = " + m.LeftEdge.ToString());
            print("Right edge = " + m.RightEdge.ToString());
            print("Ready to move = " + m.ReadyToMove);
            print(" ");
        }

        void m_SetupStateChanged(object sender, SetupStateEventArgs e)
        {
            print("Setup state: " + e.SetupState.ToString());
        }
        
        public void MoveTo(int pos)
        {
            try
            {
                print("Trying to move to: " + pos.ToString());
                m.MoveTo(pos);            
            }
            catch (Exception ex)
            {
                print("Could not move: " + ex.Message);
                return;
            }
        }

        public void MoveTo()
        {
            MoveTo(Convert.ToInt32(numScanPos.Value));
        }

        void m_FinishedMove(object sender, MotorTaskEventArgs e)
        {
            if (!e.Success)
            {
                print("Move failed: " + e.Error.Message);
                return;
            }
            
            print("Success - move finished");
            print("Ready to move = " + m.ReadyToMove);
            print(" ");
        }
        
        private void btnRefresh_Click(object sender, EventArgs e)
        {
            refreshAddresses();
        }

        protected void refreshAddresses()
        {
            cmbAddress.Items.Clear();
            string[] a = MotorControl.GetAddresses();
            cmbAddress.Items.AddRange(a);
                        
            // select IP address by default
            if (cmbAddress.Items.Count > 0)
                cmbAddress.SelectedIndex = 0;

            for (int i = 0; i < cmbAddress.Items.Count; i++)
            {
                if (!cmbAddress.Items[i].ToString().StartsWith("COM"))
                {
                    cmbAddress.SelectedIndex = i;
                    break;
                }
            }
        }

        protected virtual bool refreshEdges()
        {
            txtLeftEdge.Text = "";
            txtRightEdge.Text = "";

            int leftEdge = 0, rightEdge = 0;
            try
            {
                leftEdge = m.LeftEdge;
                rightEdge = m.RightEdge;
            }
            catch (ApplicationException ex)
            {
                print("Unable to read edges in refreshEdges: " + ex.Message);
                return false;
            }
            catch (Exception ex)
            {
                print("Unknown error reading edges in refreshEdges: " + ex.Message);
                return false;
            }

            if (leftEdge > 0 && rightEdge > 0)
            {
                txtLeftEdge.Text = leftEdge.ToString();
                txtRightEdge.Text = rightEdge.ToString();
                numScanPos.Enabled = true;
                numScanPos.ReadOnly = false;
                numScanPos.Minimum = leftEdge + 1;
                numScanPos.Maximum = rightEdge - 1;
                numScanPos.Value = leftEdge + 5;
            }
            else
            {
                numScanPos.Enabled = false;
                numScanPos.ReadOnly = true;
                numScanPos.Minimum = 0;
                numScanPos.Value = 0;
            }

            return true;
        }
    }
}
