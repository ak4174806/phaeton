﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using DFM.Motor;
using System.IO;
using System.Threading;

namespace DFM
{
    public partial class GalilMonitorForm : Form
    {
        IMotorControl m;

        public GalilMonitorForm(IMotorControl m, bool debug=false)
        {
            InitializeComponent();
            this.m = m;
            
            m.SetupStateChanged += new MotorSetupStateHandler(m_SetupStateChanged);
            m.FinishedSetup += new MotorTaskHandler(m_FinishedSetup);
            m.FinishedMove += new MotorTaskHandler(m_FinishedMove);
            m.Message += new MotorMessageHandler(m_Message);
        
            m.PollState.PollMotorError += new PollMotorErrorHandler(PollState_PollMotorError);
            m.PollState.StatusChanged += new PollMotorStatusChangedHandler(PollState_StatusChanged);
            m.PollState.HomedChanged += new PollMotorHomedChangedHandler(PollState_HomedChanged);
            m.PollState.FaultChanged += new PollMotorFaultChangedHandler(PollState_FaultChanged);
            m.PollState.PollDataChanged += new PollMotorDataChangedHandler(PollState_PollDataChanged);

            this.Shown += new EventHandler(delegate(object sender, EventArgs e)
            {
                refreshEdges();
            });

            this.FormClosing += new FormClosingEventHandler(delegate(object sender, FormClosingEventArgs e)
            {
                if (e.CloseReason == CloseReason.UserClosing || e.CloseReason == CloseReason.None)
                {
                    e.Cancel = true;
                    this.Hide();
                }
            });

            textBox3.AppendText("Status: " + m.PollState.Status.State.ToString());

            if (!debug) btnTogglePainting.Visible = false;
            btnTogglePainting.Click += new EventHandler(btnTogglePainting_Click);
        }

                void btnTogglePainting_Click(object sender, EventArgs e)
        {
            if (m.PollState is MockPollMotor)
            {
                m.PollState.Status.Painting = !m.PollState.Status.Painting;
                ((MockPollMotor)m.PollState).raiseStatusChanged();
            }
        }        

        void PollState_FaultChanged(object sender, FaultChangedEventArgs e)
        {
            textBox3.AppendText(Environment.NewLine + "Fault changed: " + e.Fault.State.ToString());
        }

        void PollState_StatusChanged(object sender, StatusChangedEventArgs e)
        {
            textBox3.AppendText(Environment.NewLine + "Status changed: " + e.Status.State.ToString());
        }

        void PollState_HomedChanged(object sender, HomedChangedEventArgs e)
        {
            textBox3.AppendText(Environment.NewLine + "Homed changed: " + e.Homed.State.ToString());
        }

        void PollState_PollMotorError(object sender, MotorErrorEventArgs e)
        {
            textBox3.AppendText(Environment.NewLine + "Polling Error: " + e.Error.ToString());
            if (m.PollState.RunPoll)
            {
                m.PollState.RunPoll = false;
                m.Disconnect();
            }
        }

        void PollState_PollDataChanged(object sender, DataChangedEventArgs e)
        {
            textBox3.AppendText(Environment.NewLine + "Target A: " + e.TargetA.ToString());
            textBox3.AppendText(Environment.NewLine + "EDGE_LT: " + e.LeftEdge.ToString());
            textBox3.AppendText(Environment.NewLine + "EDGE_RT: " + e.RightEdge.ToString());
        }


        protected bool firstRun = true;

        public bool IsConnected
        {
            get
            {
                if (firstRun) return false;

                try
                {
                    print("Testing connection ...");
                    bool result = m.TestConnection();
                    print("Result = " + result.ToString());
                    return true;
                }
                catch (Exception ex)
                {
                    print("Could not open: " + ex.Message);
                    return false;
                }
            }
        }

        void print(string s)
        {
            textBox1.AppendText(Environment.NewLine + s);
        }

        void m_Message(object sender, MessageEventArgs e)
        {
            textBox2.AppendText(Environment.NewLine + e.Message);
        }

        public virtual void Disconnect()
        {
            print("Disconnecting ...");
            try
            {
                m.Disconnect();
            }
            catch { }
            print("Disconnected");
        }

        public virtual void Setup()
        {            
            try
            {
                print("Setting up ...");
                m.Setup();
            }
            catch (Exception ex)
            {
                print("Could not setup: " + ex.Message);
                return;
            }
        }

        void m_FinishedSetup(object sender, MotorTaskEventArgs e)
        {
            if (!e.Success)
            {
                if (e.Error != null)
                    print("Setup failed " + e.Error.Message);
                else
                    print("Setup cancelled");
                return;
            }

            refreshEdges();

            print("Success - set up finished");
            print("Left edge = " + m.LeftEdge.ToString());
            print("Right edge = " + m.RightEdge.ToString());
            print("Ready to move = " + m.ReadyToMove);
            print(" ");
        }

        void m_SetupStateChanged(object sender, SetupStateEventArgs e)
        {
            print("Setup state: " + e.SetupState.ToString());
        }

        public void MoveTo(int pos)
        {
            // doesn't get called as m.MoveTo called directly, so don't know target motor position
            try
            {
                print("Trying to move to: " + pos.ToString());
                m.MoveTo(pos);            
            }
            catch (Exception ex)
            {
                print("Could not move: " + ex.Message);
                return;
            }
        }

        void m_FinishedMove(object sender, MotorTaskEventArgs e)
        {
            if (!e.Success)
            {
                if (e.Error != null) print("Move failed: " + e.Error.Message);
                else print("Move cancelled");
                return;
            }
            
            print("Success - move finished to " + m.PollState.TargetA);
            print("Ready to move = " + m.ReadyToMove);
            print(" ");
        }
        

        protected virtual bool refreshEdges()
        {
            txtLeftEdge.Text = "";
            txtRightEdge.Text = "";

            int leftEdge = 0, rightEdge = 0;
            try
            {
                leftEdge = m.LeftEdge;
                rightEdge = m.RightEdge;
            }
            catch (ApplicationException ex)
            {
                print("Unable to read edges in refreshEdges: " + ex.Message);
                return false;
            }
            catch (Exception ex)
            {
                print("Unknown error reading edges in refreshEdges: " + ex.Message);
                return false;
            }

            if (leftEdge > 0 && rightEdge > 0)
            {
                txtLeftEdge.Text = leftEdge.ToString();
                txtRightEdge.Text = rightEdge.ToString();
            }

            return true;
        }
    }
}
