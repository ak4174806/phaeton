﻿namespace DFM
{
    partial class test
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabbed_Settings_Control1 = new DFM.Settings.Controls.Tabbed_Settings_Control();
            this.SuspendLayout();
            // 
            // tabbed_Settings_Control1
            // 
            this.tabbed_Settings_Control1.FormTitle = "Tabbed Settings";
            this.tabbed_Settings_Control1.Location = new System.Drawing.Point(0, 0);
            this.tabbed_Settings_Control1.MinimumSize = new System.Drawing.Size(217, 254);
            this.tabbed_Settings_Control1.Name = "tabbed_Settings_Control1";
            this.tabbed_Settings_Control1.Size = new System.Drawing.Size(299, 275);
            this.tabbed_Settings_Control1.TabIndex = 0;
            // 
            // test
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(633, 539);
            this.Controls.Add(this.tabbed_Settings_Control1);
            this.Name = "test";
            this.Text = "test";
            this.ResumeLayout(false);

        }

        #endregion

        private Settings.Controls.Tabbed_Settings_Control tabbed_Settings_Control1;

    }
}