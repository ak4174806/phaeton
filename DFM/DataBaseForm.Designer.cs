﻿namespace DFM
{
    partial class DataBaseForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DataBaseForm));
            this.ScanDataGrid = new System.Windows.Forms.DataGridView();
            this.txtFolderDb = new System.Windows.Forms.TextBox();
            this.btnbrowseDB = new System.Windows.Forms.Button();
            this.btnExportDB = new System.Windows.Forms.Button();
            this.folderBrowserDb = new System.Windows.Forms.FolderBrowserDialog();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnApply = new System.Windows.Forms.Button();
            this.cmbStitchAvg = new System.Windows.Forms.ComboBox();
            this.chkSelectAll = new System.Windows.Forms.CheckBox();
            this.btnDeleteAll = new System.Windows.Forms.Button();
            this.tmrFade = new System.Timers.Timer();
            ((System.ComponentModel.ISupportInitialize)(this.ScanDataGrid)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tmrFade)).BeginInit();
            this.SuspendLayout();
            // 
            // ScanDataGrid
            // 
            this.ScanDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ScanDataGrid.Location = new System.Drawing.Point(12, 46);
            this.ScanDataGrid.Name = "ScanDataGrid";
            this.ScanDataGrid.Size = new System.Drawing.Size(1226, 453);
            this.ScanDataGrid.TabIndex = 0;
            // 
            // txtFolderDb
            // 
            this.txtFolderDb.Location = new System.Drawing.Point(12, 602);
            this.txtFolderDb.Name = "txtFolderDb";
            this.txtFolderDb.Size = new System.Drawing.Size(159, 20);
            this.txtFolderDb.TabIndex = 1;
            this.txtFolderDb.Text = "c:\\";
            // 
            // btnbrowseDB
            // 
            this.btnbrowseDB.Location = new System.Drawing.Point(200, 602);
            this.btnbrowseDB.Name = "btnbrowseDB";
            this.btnbrowseDB.Size = new System.Drawing.Size(99, 31);
            this.btnbrowseDB.TabIndex = 2;
            this.btnbrowseDB.Text = "Choose";
            this.btnbrowseDB.UseVisualStyleBackColor = true;
            // 
            // btnExportDB
            // 
            this.btnExportDB.Location = new System.Drawing.Point(908, 602);
            this.btnExportDB.Name = "btnExportDB";
            this.btnExportDB.Size = new System.Drawing.Size(107, 44);
            this.btnExportDB.TabIndex = 3;
            this.btnExportDB.Text = "Export";
            this.btnExportDB.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnApply);
            this.groupBox1.Controls.Add(this.cmbStitchAvg);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(440, 505);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(283, 159);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Stitching and Averaging";
            // 
            // btnApply
            // 
            this.btnApply.Location = new System.Drawing.Point(39, 86);
            this.btnApply.Name = "btnApply";
            this.btnApply.Size = new System.Drawing.Size(166, 39);
            this.btnApply.TabIndex = 1;
            this.btnApply.Text = "Apply";
            this.btnApply.UseVisualStyleBackColor = true;
            // 
            // cmbStitchAvg
            // 
            this.cmbStitchAvg.FormattingEnabled = true;
            this.cmbStitchAvg.Items.AddRange(new object[] {
            "Stitching",
            "Averaging"});
            this.cmbStitchAvg.Location = new System.Drawing.Point(39, 41);
            this.cmbStitchAvg.Name = "cmbStitchAvg";
            this.cmbStitchAvg.Size = new System.Drawing.Size(166, 28);
            this.cmbStitchAvg.TabIndex = 0;
            // 
            // chkSelectAll
            // 
            this.chkSelectAll.AutoSize = true;
            this.chkSelectAll.Location = new System.Drawing.Point(855, 23);
            this.chkSelectAll.Name = "chkSelectAll";
            this.chkSelectAll.Size = new System.Drawing.Size(70, 17);
            this.chkSelectAll.TabIndex = 5;
            this.chkSelectAll.Text = "Select All";
            this.chkSelectAll.UseVisualStyleBackColor = true;
            // 
            // btnDeleteAll
            // 
            this.btnDeleteAll.Location = new System.Drawing.Point(748, 12);
            this.btnDeleteAll.Name = "btnDeleteAll";
            this.btnDeleteAll.Size = new System.Drawing.Size(87, 28);
            this.btnDeleteAll.TabIndex = 2;
            this.btnDeleteAll.Text = "Delete";
            this.btnDeleteAll.UseVisualStyleBackColor = true;
            // 
            // tmrFade
            // 
            this.tmrFade.Interval = 20D;
            this.tmrFade.SynchronizingObject = this;
            this.tmrFade.Elapsed += new System.Timers.ElapsedEventHandler(this.tmrFade_Elapsed);
            // 
            // DataBaseForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1250, 676);
            this.Controls.Add(this.btnDeleteAll);
            this.Controls.Add(this.chkSelectAll);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnExportDB);
            this.Controls.Add(this.btnbrowseDB);
            this.Controls.Add(this.txtFolderDb);
            this.Controls.Add(this.ScanDataGrid);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "DataBaseForm";
            this.Text = "Phaeton : Database";
            this.Load += new System.EventHandler(this.DataBaseForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ScanDataGrid)).EndInit();
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tmrFade)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.DataGridView ScanDataGrid;
        public System.Windows.Forms.TextBox txtFolderDb;
        public System.Windows.Forms.Button btnbrowseDB;
        public System.Windows.Forms.Button btnExportDB;
        public System.Windows.Forms.FolderBrowserDialog folderBrowserDb;
        public System.Windows.Forms.GroupBox groupBox1;
        public System.Windows.Forms.Button btnApply;
        public System.Windows.Forms.ComboBox cmbStitchAvg;
        public System.Windows.Forms.CheckBox chkSelectAll;
        public System.Windows.Forms.Button btnDeleteAll;
        protected internal System.Timers.Timer tmrFade;
    }
}