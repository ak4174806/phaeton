﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SQLite;

using DFM.Settings;
using DFM.Settings.Bindable;
using System.Xml.Serialization;
using System.IO;
using SensorControl;
using SensorControl.Sensors;
using DFM.Motor;
using NLog;
using System.IO.Ports;
using DFM.Laser;
using PaintAnalysis.Common;

namespace DFM
{
    public partial class DFM_Simple_Settings_Form : Form
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        #region Data Members

        private SettingsGroup settings;
        private UserSettings savedSettings;

        private ISensor[] sensors;
        private ILaser laser;
        private ILaser newLaser;

        private Form openingForm;
        #endregion


        DataTable dataTable = new DataTable();
      //  AlgoConfiguration algoConfig = new AlgoConfiguration();

        public DFM_Simple_Settings_Form()
        {
            InitializeComponent();
            
            btnSave.Click += new EventHandler(btnSave_Click);
            btnCancel.Click += new EventHandler(btnCancel_Click);
            btnOK.Click += new EventHandler(btnOK_Click);
            chkFilter.CheckedChanged += new EventHandler(chkFilter_checked);
            
            
            // Laser and New laser connect event
            chkConnectLaser.CheckedChanged += new EventHandler(chkConnectLaser_checked);
            chkConnectNewlaser.CheckedChanged += new EventHandler(chkConnectNewLaser_checked);
                        
            FormClosing += new FormClosingEventHandler(DFM_Settings_Form_FormClosing);

            string debugFile = Path.Combine(Path.GetDirectoryName(Application.ExecutablePath), "debug.txt");
            IsDebugging = File.Exists(debugFile);
            if (!IsDebugging)
            {
                sensors = new ISensor[3];
                sensors[0] = new ILD2200_MESensor();
                sensors[1] = new ILD1700_MESensor();
                sensors[2] = new ILD2300_MESensor();
                laser = new PLCLaser();
                newLaser = new NewPLCLaser();
            }
            else
            {
                logger.Trace("Debugging");
                sensors = new ISensor[5];
                sensors[0] = new ILD2200_MESensor();
                sensors[1] = new ILD1700_MESensor();
                sensors[2] = new ILD2300_MESensor();
                sensors[3] = new MockSensor();
                sensors[4] = new MockSensorFileScans(File.ReadAllText(debugFile));
                laser = new MockLaser();
                
            }
            
            FormTitle = "Phaeton Settings";
            Settings = new SettingsGroup("DFM Settings 2");
            init();
            Settings.ModifiedChanged += new EventHandler(Settings_ModifiedChanged);
            btnSave.Click += new EventHandler(Settings_ModifiedChanged);
            btnOK.Click += new EventHandler(Settings_ModifiedChanged);
            btnCancel.Click += new EventHandler(Settings_ModifiedChanged);


            // initailize leftmiddleright checkbox
            if (chkAppyLeftMiddleRight.Checked)
            {
                grpPits.Enabled = true;

            }
            else
            {
                grpPits.Enabled = false;
            }

            // initailize filter checkbox
            if (chkFilter.Checked)
            {
                grpFilteration.Enabled = true;

            }
            else
            {
                grpFilteration.Enabled = false;
            }

           // algoConfig.btnSave.Click += new EventHandler(AlgobtnSave_Click);

           // dataTable.Columns.Add("Id");
           // dataTable.Columns.Add("PaintName");
           // dataTable.Columns.Add("Algorithm");
            //dataTable.Columns.Add("");
        }

        #region General Form Behaviour

        public Form OpeningForm
        {
            get { return openingForm; }
            set { openingForm = value; }
        }    

        public SettingsGroup Settings
        {
            get { return settings; }
            private set { settings = value; }
        }

        public string FormTitle
        {
            get { return lblHeading.Text; }
            set { lblHeading.Text = Text = value; }
        }
        
        private void message(string m)
        {
            MessageBox.Show(m);
        }

        void Settings_ModifiedChanged(object sender, EventArgs e)
        {
            Text = (Settings.IsModified ? "* " : "") + FormTitle;
        }

        void DFM_Settings_Form_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing || e.CloseReason == CloseReason.None)
            {                
                if (OpeningForm != null) e.Cancel = true;
                
                bool changed = Settings.IsModified;
                bool valid = Settings.IsValid;

                if (this.DialogResult == DialogResult.Cancel || this.DialogResult == System.Windows.Forms.DialogResult.None)
                {
                    if (!changed || MessageBox.Show("You will lose any changes. Continue?", "Cancelling options changes", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        // reset form first
                        if (changed) Settings.Revert();
                        if (OpeningForm != null) this.Hide();
                    }
                    else e.Cancel = true;
                }
                else if (this.DialogResult == DialogResult.OK)
                {
                    if (valid)
                    {
                        Settings.Save();
                        savedSettings.Settings = Settings;
                        savedSettings.Save();
                        if (OpeningForm != null) this.Hide();
                    }
                    else
                    {
                        e.Cancel = true;
                        message("Error: " + Settings.ValidationErrors[0].ErrorMessage);
                    }
                }
                else
                {
                    // close for some other reason - then treat it as a cancel
                    if (changed) Settings.Revert();
                    if (OpeningForm != null) this.Hide();
                }
            }
        }

        void btnOK_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        void btnSave_Click(object sender, EventArgs e)
        {
            Save();
        }

       /* void   AlgobtnSave_Click(object sender, EventArgs e)
        {

            if (algoConfig.txtPaint.Text == "")
            {
                // MessageBox.Show("Enter Paint Type");
               algoConfig.lblInvalidType.Visible = true;
               algoConfig.lblInvalidType.Text = "Enter Paint type!";
               algoConfig.lblInvalidType.ForeColor = Color.Red;
                return;

            }

            bool HeightOption = false;
            if (algoConfig.rdForAllPits.Checked == true)
            {
                HeightOption = false;
            }
            else if (algoConfig.rdForPit1.Checked == true)
            {
                HeightOption = true;
            }

            // Add paint Type into database
            try
            {
                SQLiteConnection con = new SQLiteConnection("Data Source=ScanDataBase.s3db");
                con.Open();
                SQLiteCommand com = new SQLiteCommand();
                com.Connection = con;
                // MessageBox.Show(System.DateTime.Now.ToString()); 
                com.CommandText = "insert into tbl_paint_type(PaintName,Algorithm,NoPointAvg,NoPointBwAvg,DistanceBwPits,Percentage,ThresholdValue,AveragePoints,Range,Factor,HeightOnly4First,Height,SubstrateRoughness,SmoothingWidth,MinPitWidth,CompressionFactor,PointsInEachPit) values('" + algoConfig.txtPaint.Text + "','" + algoConfig.cmbAlgo.SelectedItem.ToString() + "','" + algoConfig.nmNoPoints4Avg.Value + "','" + algoConfig.nmNoPointsBwAvgs.Value + "','" + algoConfig.nmDistanceBwPits.Value + "','" + algoConfig.nmPercentage.Value + "','" + algoConfig.nmThresholdValue.Value + "','" + algoConfig.nmAvgPoints.Value + "','" + algoConfig.nmRange.Value + "','" + algoConfig.nmFactor.Value + "','" + HeightOption + "','" + algoConfig.nmHeight.Value + "','" + algoConfig.nmSubstrateRoghness.Value + "','" + algoConfig.nmSmoothingWidth.Value + "','" + algoConfig.nmMinPitWidth.Value + "','" + algoConfig.nmCompressionFactor.Enabled + "','" + algoConfig.nmPointsInEachPit.Value + "')";
                com.ExecuteNonQuery();
                con.Close();
            }
            catch (SQLiteException ex)
            {
                MessageBox.Show(ex.Message);
            }

            //ScanDataGrid.DataSource = null;
            //LoadData();
            //ScanDataGrid.AllowUserToAddRows = false;
            //ScanDataGrid.DataSource = new DataView(LoadData());
           // DFM_Simple_Settings_Form dfmSetting = new DFM_Simple_Settings_Form();
            //dfmSetting.LoadData();
            //ScanDataGrid.DataSource = new DataView(LoadData());
            PaintTypeLoad();
            algoConfig.Hide();

           // this.Close();

           
        }
      */
       


#region connect laser and laser check event
       
        // if Laser checkbox hit checked then we check whether other one is checked or not if it is then restrict this one
        void chkConnectLaser_checked(object sender, EventArgs e)
        {
            if (chkConnectLaser.Checked == true)
            {
                if (chkConnectNewlaser.Checked == true)
                {
                    chkConnectLaser.Checked = false;
                    MessageBox.Show("Only one laser can be connected at a time");

                }
            }

        
        }

        // if NEwLaser checkbox hit checked then we check whether other one is checked or not if it is then restrict this one
        void chkConnectNewLaser_checked(object sender, EventArgs e)
        {
            if (chkConnectNewlaser.Checked == true)
            {
                if (chkConnectLaser.Checked == true)
                {
                    chkConnectNewlaser.Checked = false;
                    MessageBox.Show("Only one laser can be connected at a time");
                }
                else
                {
                    //enable all control inside New Laser Controller tab
                    cmbNewLaserPort.Enabled = true;
                    grpBxLaser.Enabled = true;
                    grpBxMotion.Enabled = true;
                }

            }
            else 
            {
                //disable all control inside New Laser Controller tab
                cmbNewLaserPort.Enabled = false;
                grpBxLaser.Enabled = false;
                grpBxMotion.Enabled = false;

            
            }

        }
        
        
#endregion 



        void chkFilter_checked(object sender, EventArgs e)
        {
            if (chkFilter.Checked)
            {
                grpFilteration.Enabled = true;
            }
            else
            {
                grpFilteration.Enabled = false;
            }
        }


        public void Save()
        {
            if (Settings.IsValid)
            {
                Settings.Save();
                savedSettings.Settings = Settings;
                savedSettings.Save();
            }
            else
            {
                message("Error: " + Settings.ValidationErrors[0].ErrorMessage);
            }
        }

        #endregion

        #region DFM Specific Behaviour

        public bool IsDebugging
        {
            get;
            protected set;
        }
                
        #region Initialisation

        private void init()
        {
            logger.Trace("Initing DFM settings form");
            FormTitle = "Phaeton Settings";

            logger.Trace("Adding DFM settings form events");
            addEvents();

            SensorAddressType = SensorAddressTypes.COM_Port;

            logger.Trace("Creating DFM settings objects");
            try
            {
                addSettings();
            }
            catch (Exception ex)
            {
                logger.ErrorException("Failed creating DFM settings objects", ex);
                message("Initialising settings objects failed");
                return;
            }

            logger.Trace("Starting to load saved settings");
            savedSettings = new UserSettings(Settings.Name);
            if (savedSettings.Settings != null)
            {
                logger.Trace("Found saved settings");
                Settings.LoadSettingsFrom(savedSettings.Settings);
            }
            else
            {
                logger.Trace("Found no saved settings");
            }

            addValidators();

            // connect button text
            ConnectionStateChanged += new EventHandler<ConnectionStateEventArgs>(delegate(object sender, ConnectionStateEventArgs e)
            {
                if (e.ConnectionState == ConnectionStates.Connected) DFM.Settings.Bindable.ThreadSafeControlUpdates.UpdateText(btnConnect, "Disconnect");
                else DFM.Settings.Bindable.ThreadSafeControlUpdates.UpdateText(btnConnect, "Connect");
            });
            
            // set saved settings
            Settings.Save();

            // add global enabler
            HybridSetting enabler = new HybridSetting("form enabling");
            isEnabled = new SettingValue<bool>("enabled", true);
            enabler.AddEnableSetting(isEnabled, false); // don't disable validation
            enabler.Add(Settings);

            // add units changer handler and trigger it once.   
            SettingValue<DataUnits> tmp = (SettingValue<DataUnits>)Settings.FetchSetting("data/units");
            tmp.ValueChanged += new EventHandler<ValueChangedEventArgs<DataUnits>>(handleDataUnitsChanged);
            handleDataUnitsChanged(cmbUnits, new ValueChangedEventArgs<DataUnits>(tmp.Value, tmp));
        }

        private void addSettings()
        {
            HybridSetting tmp;
            {
                // start connection
                SettingsGroup connection = new SettingsGroup("connection");

                // sensor connection
                HybridSetting sensorConnection = new HybridSetting("sensor");
                sensorConnection.AddEnableSetting(new BindableBoolSetting(chkConnectSensor, "enabled", chkConnectSensor.Checked));
                sensorConnection.Add(new BindableStringArraySetting(cmbSensorModel, "model", new string[] { }));
                sensorConnection.Add(new BindableStringArraySetting(cmbSensorType, "type", new string[] { }));
                sensorConnection.Add(new BindableStringArraySetting(cmbSensorPort, "port", new string[] { }));
                sensorConnection.Add(new BindableSettingValue<string, TextBox>(txtSensorAddress, "ip_address", ""));
                sensorConnection.Add(new BindableStringArraySetting(cmbSensorBaudrate, "baudrate", new string[] { }));

                // laser connection
                HybridSetting laserConnection = new HybridSetting("laser");
                laserConnection.AddEnableSetting(new BindableBoolSetting(chkConnectLaser, "enabled", chkConnectLaser.Checked));
                laserConnection.Add(new BindableStringArraySetting(cmbLaserPort, "port", new string[] { }));
                laserConnection.Add(new BindableStringArraySetting(cmbLaserBaudrate, "baudrate", new string[] { }));
                laserConnection.Add(new BindableStringArraySetting(cmbLaserHandshake, "handshake", new string[] { "RequestToSend", "None" }, "RequestToSend"));

                // New laser connection
                HybridSetting NewlaserConnection = new HybridSetting("new laser");
                NewlaserConnection.AddEnableSetting(new BindableBoolSetting(chkConnectNewlaser, "enabled",chkConnectNewlaser.Checked));
                NewlaserConnection.Add(new BindableStringArraySetting(cmbNewLaserPort, "port", new string[] { }));
                //NewlaserConnection.Add(new BindableStringArraySetting(cmbNewLaserBaudrate, "baudrate", new string[] { }));
                //laserConnection.Add(new BindableStringArraySetting(cmbLaserHandshake, "handshake", new string[] { "RequestToSend", "None" }, "RequestToSend"));
              
                //(for New laser Controller) motion parameters
                NewlaserConnection.Add(new BindableIntSetting(numS, "start position", Convert.ToInt32(numS.Value)));
                NewlaserConnection.Add(new BindableIntSetting(numE, "end position", Convert.ToInt32(numE.Value)));
                NewlaserConnection.Add(new BindableIntSetting(numA, "accelerate rate", Convert.ToInt32(numA.Value)));
                NewlaserConnection.Add(new BindableIntSetting(numV, "steady velocity", Convert.ToInt32(numV.Value)));

                //(for New laser Controller) laser parameters
                NewlaserConnection.Add(new BindableIntSetting(numD, "duty cycle", Convert.ToInt32(numD.Value)));
                NewlaserConnection.Add(new BindableIntSetting(numN, "num of cycle", Convert.ToInt32(numN.Value)));
                NewlaserConnection.Add(new BindableIntSetting(numB, "num of burst", Convert.ToInt32(numB.Value)));
                NewlaserConnection.Add(new BindableIntSetting(numT, "total pulses", Convert.ToInt32(numT.Value)));

                //Start command
                //NewlaserConnection.Add(new BindableSettingValue<string, TextBox>(txtBxStartCommand, "start command", ""));


                // end connection
                connection.Add(sensorConnection);
                connection.Add(laserConnection);
                connection.Add(NewlaserConnection);// add new laser controller
                Settings.Add(connection);

                // update all above
                updateSensorModel();
                updateSensorTypes();
                updateComPorts();
                updateBaudrates();

                // enabler by connection state
                tmp = new HybridSetting("tmp");
                ConnectionState = ConnectionStates.Disconnected; // default value
                SettingValue<bool> enabler = new SettingValue<bool>("enabled", ConnectionState == ConnectionStates.Disconnected);

                ConnectionStateChanged += new EventHandler<ConnectionStateEventArgs>(delegate(object sender, ConnectionStateEventArgs e)
                {
                    enabler.Value = e.ConnectionState == ConnectionStates.Disconnected;
                });

                tmp.AddEnableSetting(enabler);
                tmp.Add(connection);
            }

            {
                // start scan parameters
                SettingsGroup scanParameters = new SettingsGroup("scan");

                scanParameters.Add(new BindableSettingValue<string, TextBox>(txtMotorCommand, "motor command", txtMotorCommand.Text));

                // scan timing
                SettingsGroup scanTiming = new SettingsGroup("timing");
               // scanTiming.Add(new BindableBoolSetting(rdBtnTimer, "Timer Enabled", rdBtnTimer.Checked));
                scanTiming.Add(new BindableBoolRadioSetting(rdBtnTimer, "Timer Mode",Convert.ToBoolean(rdBtnTimer.Checked)));
                scanTiming.Add(new BindableIntSetting(numDAQStart, "daq start", Convert.ToInt32(numDAQStart.Value)));
                scanTiming.Add(new BindableIntSetting(numLaserStart, "laser start", Convert.ToInt32(numLaserStart.Value)));
                scanTiming.Add(new BindableIntSetting(numDAQStop, "daq stop", Convert.ToInt32(numDAQStop.Value)));
                scanTiming.Add(new BindableIntSetting(numMoveMotorAfter, "move motor", Convert.ToInt32(numMoveMotorAfter.Value)));

                scanTiming.Add(new BindableBoolRadioSetting(rdBtnAutoDetect, "Auto Detect", Convert.ToBoolean(rdBtnAutoDetect.Checked)));
                //scanTiming.Add(new BindableBoolSetting(chkPVS, "Like PVS", chkPVS.Checked));
                scanTiming.Add(new BindableSettingValue<double, NumericUpDown>(nmLowerRange, "Lower Range", Convert.ToDouble(nmLowerRange.Value)));
                scanTiming.Add(new BindableSettingValue<double, NumericUpDown>(nmHigherRange, "Higher Range", Convert.ToDouble(nmHigherRange.Value)));
                //scanTiming.Add(new BindableFloatSetting(nmLowerRange, "Lower Range", Convert.ToDouble(nmLowerRange.Value)));
                //scanTiming.Add(new BindableIntSetting(nmHigherRange, "Higher Range", Convert.ToInt32(nmHigherRange.Value)));
                scanTiming.Add(new BindableIntSetting(nmEdgePoints, "Edge Points", Convert.ToInt32(nmEdgePoints.Value)));
              //  scanTiming.Add(new BindableIntSetting(numMoveMotorAfter, "move motor", Convert.ToInt32(numMoveMotorAfter.Value)));
                
                HybridSetting repeat = new HybridSetting("repeat");
                repeat.AddEnableSetting(new BindableBoolSetting(chkRepeat, "enabled", chkRepeat.Checked));
                repeat.Add(new BindableIntSetting(numRepeatCount, "count", Convert.ToInt32(numRepeatCount.Value)));
                repeat.Add(new BindableFloatSetting(numRepeatInterval, "interval", Convert.ToSingle(numRepeatInterval.Value)));
                scanTiming.Add(repeat);

                // laser
                SettingsGroup scanLaser = new SettingsGroup("laser");
                BindableEnumSettingRadioButtons<LaserDurationSpecification> laserDuration = new BindableEnumSettingRadioButtons<LaserDurationSpecification>("duration specification");
                laserDuration.Bind(radPulseCount, LaserDurationSpecification.PulseCount); // default as first
                laserDuration.Bind(radRunTime, LaserDurationSpecification.RunTime);
                scanLaser.Add(laserDuration);

                scanLaser.Add(new BindableIntSetting(numPulseCount, "pulse count", Convert.ToInt32(numPulseCount.Value)));
                scanLaser.Add(new BindableIntSetting(numRunTime, "run time", Convert.ToInt32(numRunTime.Value)));
                scanLaser.Add(new BindableIntSetting(numGatingFreq, "gating frequency", Convert.ToInt32(numGatingFreq.Value)));
                scanLaser.Add(new BindableFloatSetting(numDutyCycle, "duty cycle", Convert.ToSingle(numDutyCycle.Value)));

                // laser duration enablers - tmps only
                tmp = new HybridSetting("tmp");
                tmp.AddEnableSetting(new BindableBoolRadioSetting(radRunTime, "enabled", radRunTime.Checked));
                tmp.Add(scanLaser.FetchSetting("run time"));

                tmp = new HybridSetting("tmp");
                tmp.AddEnableSetting(new BindableBoolRadioSetting(radPulseCount, "enabled", radPulseCount.Checked));
                tmp.Add(scanLaser.FetchSetting("pulse count"));

                // enablers: for timing controls, and other settings
                tmp = new HybridSetting("tmp");
                tmp.AddEnableSetting((SettingValue<bool>)Settings.FetchSetting("connection/sensor/enabled"));
                tmp.Add(scanTiming.FetchSetting("daq start"));
                tmp.Add(scanTiming.FetchSetting("daq stop"));

                tmp = new HybridSetting("tmp");
                tmp.AddEnableSetting((SettingValue<bool>)Settings.FetchSetting("connection/laser/enabled"));
                tmp.Add(scanTiming.FetchSetting("laser start"));
                tmp.Add(scanLaser);

                // end scan parameters
                scanParameters.Add(scanTiming);
                scanParameters.Add(scanLaser);
                Settings.Add(scanParameters);
            }

            {
                // start data analysis
                SettingsGroup dataAnalysis = new SettingsGroup("data");

                // data processing
                SettingsGroup dataProcessing = new SettingsGroup("processing");
                dataProcessing.Add(new BindableIntSetting(numTruncateFirst, "truncate first", Convert.ToInt32(numTruncateFirst.Value)));
                dataProcessing.Add(new BindableIntSetting(numTruncateLast, "truncate last", Convert.ToInt32(numTruncateLast.Value)));

                HybridSetting filtering = new HybridSetting("filtering");
                filtering.AddEnableSetting(new BindableBoolSetting(chkFilterPeaks, "enabled", chkFilterPeaks.Checked));
                filtering.Add(new BindableFloatSetting(numPeakThreshold, "peak threshold", Convert.ToSingle(numPeakThreshold.Value)));
                filtering.Add(new BindableFloatSetting(numPeakHysteresis, "peak hysteresis", Convert.ToSingle(numPeakHysteresis.Value)));
                filtering.Add(new BindableIntSetting(numMaxPeakWidth, "max peak width", Convert.ToInt32(numMaxPeakWidth.Value)));
                dataProcessing.Add(filtering);

                // analysis parameters
             /*   SettingsGroup analysis = new SettingsGroup("analysis");
                analysis.Add(new BindableFloatSetting(numSubstrateRoughness, "substrate roughness", Convert.ToSingle(numSubstrateRoughness.Value)));
                analysis.Add(new BindableFloatSetting(numThresholdParam, "threshold parameter", Convert.ToSingle(numThresholdParam.Value)));
                analysis.Add(new BindableIntSetting(numSmoothingWidth, "smoothing width", Convert.ToInt32(numSmoothingWidth.Value)));
                analysis.Add(new BindableIntSetting(numMinPitWidth, "min pit width", Convert.ToInt32(numMinPitWidth.Value)));
                analysis.Add(new BindableFloatSetting(numCompressionFactor, "compression factor", Convert.ToSingle(numCompressionFactor.Value)));
                analysis.Add(new BindableIntSetting(numPitPoints, "pit count", Convert.ToInt32(numPitPoints.Value)));
                */
                // data saving
                SettingsGroup saving = new SettingsGroup("saving");
                saving.Add(new BindableSettingValue<string, TextBox>(txtDataLocation, "location", txtDataLocation.Text));
                saving.Add(new BindableSettingValue<string, TextBox>(txtFilenamePattern, "filename pattern", txtFilenamePattern.Text));

                dataAnalysis.Add(new BindableIntSetting(numScanAveNumber, "scan averaging number", Convert.ToInt32(numScanAveNumber.Value)));
                dataAnalysis.Add(new BindableEnumSettingComboBox<DataUnits>(cmbUnits, "units", DataUnits.Metric));

                SettingsGroup segments = new SettingsGroup("segments");
                segments.Add(new BindableBoolSetting(chkMultipleSegmentScan, "multiple segments", false));
                segments.Add(new BindableIntSetting(numMaxSegments, "max segments", 3));
                dataAnalysis.Add(segments);

                // end data analysis
                dataAnalysis.Add(dataProcessing);
               // dataAnalysis.Add(analysis);
                dataAnalysis.Add(saving);
                Settings.Add(dataAnalysis);
            }

            {
               
                    // start misc
                    SettingsGroup misc = new SettingsGroup("misc");
                    misc.Add(new SettingValue<string>("password", ""));
                    misc.Add(new BindableIntSetting(nmLagTime, "lag time", Convert.ToInt32(nmLagTime.Value)));
                    misc.Add(new BindableSettingValue<double, NumericUpDown>(nmDiffFilter, "Diff filter", Convert.ToDouble(nmDiffFilter.Value)));
                    misc.Add(new BindableBoolSetting(chkFilter, "Filter Check", chkFilter.Checked));
                    misc.Add(new BindableBoolSetting(chkAppyLeftMiddleRight, "Apply Left Middle Right", chkAppyLeftMiddleRight.Checked));
                    misc.Add(new BindableIntSetting(nmIgnoreEdge, "Ignore Edge", Convert.ToInt32(nmIgnoreEdge.Value)));
                    misc.Add(new BindableIntSetting(nmLineSpeed, "Line Speed", Convert.ToInt32(nmLineSpeed.Value)));
                    misc.Add(new BindableIntSetting(nmSensorFrequency, "Sensor Frequency", Convert.ToInt32(nmSensorFrequency.Value)));
  
                // misc.Add(new BindableSettingValue<double, NumericUpDown>(nmIgnoreEdge, "Ignore Edge", Convert.ToDouble(nmIgnoreEdge.Value)));
                    //misc.Add(new BindableSettingValue<double, NumericUpDown>(nmLineSpeed, "Line Speed", Convert.ToDouble(nmLineSpeed.Value)));
                    //misc.Add(new BindableSettingValue<double, NumericUpDown>(nmSensorFrequency, "Sensor Frequency", Convert.ToDouble(nmSensorFrequency.Value)));

                    Settings.Add(misc);
               

            }
        }

        private void addEvents()
        {
            cmbSensorModel.SelectedIndexChanged += new EventHandler(cmbSensorModel_SelectedIndexChanged);
            btnRefreshCOMPorts.Click += new EventHandler(btnRefreshCOMPorts_Click);
            btnConnect.Click += new EventHandler(btnConnect_Click);

            btnChooseDataLocation.Click += new EventHandler(btnChooseDataLocation_Click);

            btnChangePassword.Click += new EventHandler(btnChangePassword_Click);
        }

        private void addValidators()
        {
            SettingValue<int> daqStart = (SettingValue<int>)Settings.FetchSetting("scan/timing/daq start");
            SettingValue<int> laserStart = (SettingValue<int>)Settings.FetchSetting("scan/timing/laser start");
            SettingValue<int> daqStop = (SettingValue<int>)Settings.FetchSetting("scan/timing/daq stop");
            SettingValue<float> repeatInterval = (SettingValue<float>)Settings.FetchSetting("scan/timing/repeat/interval"); // in seconds

            SettingValue<bool> sensorEnabled = (SettingValue<bool>)Settings.FetchSetting("connection/sensor/enabled");
            SettingValue<bool> laserEnabled = (SettingValue<bool>)Settings.FetchSetting("connection/laser/enabled");
            SettingValue<bool> NewlaserEnabled = (SettingValue<bool>)Settings.FetchSetting("connection/new laser/enabled");

            /*laserStart.ValidateOnValueChangeOnly = false;
            laserStart.AddValidator(new SettingValidator<int>(delegate(ISettingValue<int> settings, ValidationResult<int> e)
            {
                if ((e.Value > daqStop.Value || e.Value < daqStart.Value) && laserEnabled.Value && sensorEnabled.Value)
                {
                    e.IsError = true;
                    e.ErrorMessage = "Laser start time should be during DAQ cycle";
                }
            }));*/
            
            daqStop.ValidateOnValueChangeOnly = false;
            daqStop.AddValidator(new SettingValidator<int>(delegate(ISettingValue<int> settings, ValidationResult<int> e)
            {
                if (daqStart.Value >= e.Value && sensorEnabled.Value)
                {
                    e.IsError = true;
                    e.ErrorMessage = "DAQ stop time should be after DAQ start time";
                }
            }));

            repeatInterval.ValidateOnValueChangeOnly = false;
            repeatInterval.AddValidator(new SettingValidator<float>(delegate(ISettingValue<float> settings, ValidationResult<float> e)
            {
                if ((daqStop.Value >= 1000 * e.Value && sensorEnabled.Value) || (laserStart.Value >= e.Value * 1000 && laserEnabled.Value))
                {
                    e.IsError = true;
                    e.ErrorMessage = "The repeat time must be longer than the DAQ cycle and the laser cycle";
                }
            }));
            
            // validate data location
            SettingValue<string> dataFolder = (SettingValue<string>)Settings.FetchSetting("data/saving/location");
            SettingValue<string> filename = (SettingValue<string>)Settings.FetchSetting("data/saving/filename pattern");
            
            dataFolder.ValidateOnValueChangeOnly = false;
            dataFolder.AddValidator(new SettingValidator<string>(delegate(ISettingValue<string> setting, ValidationResult<string> e)
            {
                if (!Directory.Exists(e.Value))
                {
                    e.IsError = true;
                    e.ErrorMessage = "Could not find directory: " + setting.Value;
                }
            }));

            filename.AddValidator(new SettingValidator<string>(delegate(ISettingValue<string> setting, ValidationResult<string> e)
            {
                e.Value = e.Value.Trim();
                if (String.IsNullOrEmpty(e.Value))
                {
                    e.IsError = true;
                    e.ErrorMessage = "Enter a filename pattern";
                }
            }));            
        }

        private SettingValue<bool> isEnabled;
        public bool IsEnabled
        {
            get
            {
                if (isEnabled == null)
                {
                    logger.Warn("Tried to get DFM_Settings_Form.IsEnabled when isEnabled setting didn't exist");
                    return true;
                }
                return isEnabled.Value;
            }
            set
            {
                if (isEnabled == null)
                {
                    logger.Warn("Tried to set DFM_Settings_Form.IsEnabled when isEnabled setting didn't exist");
                    return;
                }
                isEnabled.Value = value;
            }            
        }

        #endregion

        #region Units Change

        private void handleDataUnitsChanged(object sender, ValueChangedEventArgs<DataUnits> e)
        {
            // nothing
        }
        #endregion

        #region Updating

        private void updateSensorModel()
        {
            string[] tmp = new string[sensors.Length];
            for (int i = 0; i < sensors.Length; i++)
            {
                tmp[i] = sensors[i].Properties.ModelName;
            }
            
            BindableStringArraySetting sensorModel = (BindableStringArraySetting)Settings.FetchSetting("connection/sensor/model");
            sensorModel.Values = tmp;
        }

        private void updateSensorTypes()
        {
            ISensor selected = Sensor;
            if (selected == null)
            {
                if (cmbSensorModel.Items.Count > 0) logger.Error("Could not work out what sensor was selected when updating sensor types");
                return;
            }

            // todo: can simplify
            string[] tmp = new string[selected.SensorTypes.Length];
            for (int i = 0; i < selected.SensorTypes.Length; i++)
            {
                tmp[i] = selected.SensorTypes[i];
            }

            BindableStringArraySetting sensorType = (BindableStringArraySetting)Settings.FetchSetting("connection/sensor/type");
            sensorType.Values = tmp;
        }

        private void updateComPorts()
        {
            string[] ports = SerialPort.GetPortNames();
            Array.Sort(ports);

            BindableStringArraySetting tmp = (BindableStringArraySetting)Settings.FetchSetting("connection/sensor/port");
            tmp.Values = ports;

            tmp = (BindableStringArraySetting)Settings.FetchSetting("connection/laser/port");
            tmp.Values = ports;
            tmp = (BindableStringArraySetting)Settings.FetchSetting("connection/new laser/port");
            tmp.Values = ports;
        }

        private void updateBaudrates()
        {
            // do sensor
            ISensor selected = Sensor;
            if (selected == null)
            {
                if (cmbSensorModel.Items.Count > 0) logger.Error("Could not work out what sensor was selected when updating baud rates");
            }
            else
            {
                string[] baudrates = new string[selected.BaudRates.Length + 1];
                baudrates[0] = "Autodetect";

                for (int i = 0; i < selected.BaudRates.Length; i++)
                {
                    baudrates[i + 1] = selected.BaudRates[i].ToString();
                }

                BindableStringArraySetting tmp = (BindableStringArraySetting)Settings.FetchSetting("connection/sensor/baudrate");
                tmp.Values = baudrates;
            }

            // do laser
            {
                string[] baudrates = new string[Laser.Baudrates.Length + 1];
                baudrates[0] = "Autodetect";

                for (int i = 0; i < Laser.Baudrates.Length; i++)
                {
                    baudrates[i + 1] = Laser.Baudrates[i].ToString();
                }

                BindableStringArraySetting tmp = (BindableStringArraySetting)Settings.FetchSetting("connection/laser/baudrate");
                tmp.Values = baudrates;
            }
            // do  New laser
           /* {
                string[] baudrates = new string[newLaser.Baudrates.Length + 1];
                baudrates[0] = "Autodetect";

                for (int i = 0; i < newLaser.Baudrates.Length; i++)
                {
                    baudrates[i + 1] = newLaser.Baudrates[i].ToString();
                }

                BindableStringArraySetting tmp = (BindableStringArraySetting)Settings.FetchSetting("connection/new laser/baudrate");
                tmp.Values = baudrates;
            }*/
        }
                
        private void updateSelectedAddressType()
        {
            ISensor selected = Sensor;
            if (selected != null)
            {
                SensorAddressType = selected.SensorAddressType;
            }
        }

        private SensorAddressTypes sensorAddressType;
        protected SensorAddressTypes SensorAddressType
        {
            get { return sensorAddressType; }
            set
            {
                sensorAddressType = value;
                if (sensorAddressType == SensorAddressTypes.IP_Address)
                {
                    txtSensorAddress.Visible = true;
                    cmbSensorPort.Visible = false;
                    lblSensorPort.Text = "IP Address";
                }
                else
                {
                    txtSensorAddress.Visible = false;
                    cmbSensorPort.Visible = true;
                    lblSensorPort.Text = "Port";
                }
            }
        }           

        #endregion

        #region Connection

        #region State Handling

        private ConnectionStates connectionState;
        public ConnectionStates ConnectionState
        {
            get { return connectionState; }
            set
            {
                connectionState = value;
                OnConnectionStateChanged(new ConnectionStateEventArgs(value));
            }
        }

        public class ConnectionStateEventArgs : EventArgs
        {
            private ConnectionStates connectionState;
            public ConnectionStates ConnectionState
            {
                get { return connectionState; }
                set { connectionState = value; }
            }

            public ConnectionStateEventArgs(ConnectionStates connectionState)
            {
                ConnectionState = connectionState;
            }
        }

        public event EventHandler<ConnectionStateEventArgs> ConnectionStateChanged;

        public enum ConnectionStates
        {
            Connected,
            Disconnected
        }
        
        protected void OnConnectionStateChanged(ConnectionStateEventArgs e)
        {
            if (ConnectionStateChanged != null)
            {
                ConnectionStateChanged(this, e);
            }
        }

        #endregion

        public void Connect()
        {
            string error;          
            if (ConnectionState == ConnectionStates.Connected)
            {
                bool success = TryDisconnect(out error);
                if (!success)
                {
                    logger.Error("Error disconnecting: " + error);
                    message("Error disconnecting: " + error);
                }
            }
            else
            {
                bool success = TryConnect(out error);
                if (!success)
                {
                    logger.Error("Error connecting: " + error);
                    message("Error connecting: " + error);
                }
            }
        }

        public bool TryConnect(out string error, bool fast=false)
        {
            
            error = "";
            DFM.Settings.Bindable.ThreadSafeControlUpdates.UpdateEnabled(btnConnect, false);

            SettingsGroup sensorSettings = (SettingsGroup)Settings.FetchSetting("connection/sensor");
            SettingsGroup laserSettings = (SettingsGroup)Settings.FetchSetting("connection/laser");
            SettingsGroup NewlaserSettings = (SettingsGroup)Settings.FetchSetting("connection/new laser");

           

            if (laserSettings.Fetch<bool>("enabled"))
            {
                Handshake handshake = Handshake.RequestToSend;
                if (laserSettings.Fetch<string>("handshake") == "None") handshake = Handshake.None;
                laser.HandshakeMethod = handshake;

                try
                {
                    logger.Trace("Connecting to laser");
                    if (laserSettings.Fetch<string>("baudrate") == "Autodetect")
                    {
                        logger.Trace("Autodetecting baudrate");
                        Laser.Connect(laserSettings.Fetch<string>("port"), fast);
                        ((SettingValue<string>)laserSettings.FetchSetting("baudrate")).Value = Laser.Baudrate.ToString();
                    }
                    else
                    {
                        int baudrate = Convert.ToInt32(laserSettings.Fetch<string>("baudrate"));
                        Laser.Connect(laserSettings.Fetch<string>("port"), baudrate);
                    }
                    logger.Trace("Connected to laser successfully");
                }
                catch (Exception ex)
                {
                    try { Laser.Disconnect(); }
                    catch { }

                    error = "Could not connect to laser: " + ex.Message;
                    logger.Info(error);

                    DFM.Settings.Bindable.ThreadSafeControlUpdates.UpdateEnabled(btnConnect, true);
                    return false;
                }
            }

            else if (NewlaserSettings.Fetch<bool>("enabled"))
            {
               // Handshake handshake = Handshake.RequestToSend;
                //if (laserSettings.Fetch<string>("handshake") == "None") handshake = Handshake.None;
                Handshake handshake = Handshake.None; // for New laser we assume No handshake method
                newLaser.HandshakeMethod = handshake;

                try
                {
                     logger.Trace("Connecting to New laser");
                   // if (laserSettings.Fetch<string>("baudrate") == "Autodetect")
                   // {
                        logger.Trace("Autodetecting baudrate");
                        //logger.Trace("Port:" + NewlaserSettings.Fetch<string>("port"));
                        //logger.Trace("Baudrate:" + NewLaser.Baudrate.ToString());
                        NewLaser.Connect(NewlaserSettings.Fetch<string>("port"), fast);
                        //((SettingValue<string>)NewlaserSettings.FetchSetting("baudrate")).Value = NewLaser.Baudrate.ToString();
                        //logger.Trace("Port:" + NewlaserSettings.Fetch<string>("port"));
                        //logger.Trace("Baudrate:" + NewLaser.Baudrate.ToString());
                   // }
                    //else
                   // {
                     //   int baudrate = Convert.ToInt32(laserSettings.Fetch<string>("baudrate"));
                     //   Laser.Connect(laserSettings.Fetch<string>("port"), baudrate);
                    //}
                    logger.Trace("Connected to New laser successfully");


                }
                catch (Exception ex)
                {
                    try { NewLaser.Disconnect(); }
                    catch { }

                    error = "Could not connect to New laser: " + ex.Message;
                    logger.Info(error);

                    DFM.Settings.Bindable.ThreadSafeControlUpdates.UpdateEnabled(btnConnect, true);
                    return false;
                }
            }
            


            if (sensorSettings.Fetch<bool>("enabled"))
            {
                try
                {
                    string port;
                    if (SensorAddressType == SensorAddressTypes.COM_Port)
                        port = sensorSettings.Fetch<string>("port");
                    else
                        port = sensorSettings.Fetch<string>("ip_address");

                    logger.Trace("Connecting to sensor");
                    if (sensorSettings.Fetch<string>("baudrate") == "Autodetect")
                    {
                        logger.Trace("Autodetecting baudrate");
                        Sensor.Connect(port);
                        ((SettingValue<string>)sensorSettings.FetchSetting("baudrate")).Value = Sensor.BaudRate.ToString();
                    }
                    else
                    {
                        int baudrate = Convert.ToInt32(sensorSettings.Fetch<string>("baudrate"));
                        Sensor.Connect(port, baudrate);
                    }
                    logger.Trace("Connected to sensor successfully");
                }
                catch (Exception ex)
                {
                    try
                    {
                        if (laserSettings.Fetch<bool>("enabled")) Laser.Disconnect();
                        if (NewlaserSettings.Fetch<bool>("enabled")) NewLaser.Disconnect();// Disconnect New Laser if it got connected---15-July-15
                        Sensor.Disconnect();
                    }
                    catch { }

                    error = "Could not connect to sensor: " + ex.Message;
                    logger.Info(error);

                    DFM.Settings.Bindable.ThreadSafeControlUpdates.UpdateEnabled(btnConnect, true);
                    return false;
                }

                Sensor.ReadingReference = ReadingReferences.SMR;
            }

            DFM.Settings.Bindable.ThreadSafeControlUpdates.UpdateEnabled(btnConnect, true);
            ConnectionState = ConnectionStates.Connected;
            return true;
        }

        public bool TryDisconnect(out string error)
        {
            error = "";

            try
            {
                if (Settings.Fetch<bool>("connection/sensor/enabled")) Sensor.Disconnect();
                if (Settings.Fetch<bool>("connection/laser/enabled")) Laser.Disconnect();
                if (Settings.Fetch<bool>("connection/new laser/enabled")) NewLaser.Disconnect();
            }
            catch (Exception ex)
            {
                error = ex.Message;
                return false;
            }

            ConnectionState = ConnectionStates.Disconnected;
            return true;
        }

        #endregion

        #region DFM Event Handlers

        void btnChooseDataLocation_Click(object sender, EventArgs e)
        {
            SettingValue<string> folder = (SettingValue<string>)Settings.FetchSetting("data/saving/location");
            
            if (Directory.Exists(folder.Value)) folderBrowser.SelectedPath = folder.Value;
            folderBrowser.ShowDialog();

            if (folderBrowser.SelectedPath != "")
            {
                folder.Value = folderBrowser.SelectedPath;
            }
        }

        void btnConnect_Click(object sender, EventArgs e)
        {
            Connect();
        }

        void cmbSensorModel_SelectedIndexChanged(object sender, EventArgs e)
        {
            updateSelectedAddressType();
            updateSensorTypes();
            updateBaudrates();
        }

        void btnRefreshCOMPorts_Click(object sender, EventArgs e)
        {
            updateComPorts();
        }

        void btnChangePassword_Click(object sender, EventArgs e)
        {
            PasswordChangeForm form = new PasswordChangeForm();
            form.ShowDialog();

            if (form.DialogResult == System.Windows.Forms.DialogResult.OK)
            {
                SettingValue<string> setting = (SettingValue<string>)Settings.FetchSetting("misc/password");
                setting.Value = form.Password;
            }
        }        

        #endregion

        #region Devices

        private delegate ISensor SensorCallback();

        public ISensor Sensor
        {
            get
            {
                if (cmbSensorModel.InvokeRequired)
                {
                    return (ISensor)cmbSensorModel.Invoke(new SensorCallback(delegate()
                    {
                        return Sensor;
                    }));
                }
                else
                {
                    if (cmbSensorModel.SelectedIndex == -1) return null;
                    return sensors[cmbSensorModel.SelectedIndex];
                }
            }
        }

        public ILaser Laser
        {
            get { return laser; }
        }

        public ILaser NewLaser
        {
            get { return newLaser; }
        }

        #endregion        

        private void tpMisc_Click(object sender, EventArgs e)
        {

        }

        #endregion

     
        private void DFM_Simple_Settings_Form_Load(object sender, EventArgs e)
        {
            PaintTypeLoad();
           
            
        }

        public void PaintTypeLoad()
        {
            try
            {
                // dbfrm.cmbStitchAvg.SelectedIndex = 0;
                ScanDataGrid.AllowUserToAddRows = false;
                ScanDataGrid.DataSource = new DataView(LoadData());

                // For Delete button
                DataGridViewButtonColumn btndel = new DataGridViewButtonColumn();

                btndel.HeaderText = "Delete";
                btndel.Name = "Delete";
                btndel.Text = "Delete";
                btndel.Width = 43;
                btndel.UseColumnTextForButtonValue = true;
                ScanDataGrid.Columns.Add(btndel);

                // For pitvalue button
                DataGridViewButtonColumn btnpit = new DataGridViewButtonColumn();
                btnpit.HeaderText = "Edit";
                btnpit.Name = "Edit";
                btnpit.Text = "Edit";
                btnpit.Width = 43;
                btnpit.UseColumnTextForButtonValue = true;
                ScanDataGrid.Columns.Add(btnpit);

                // Click event of button
                ScanDataGrid.CellContentClick += new DataGridViewCellEventHandler(GridBtnClick);

             // Algoconfig.FormClosing += new FormClosingEventHandler(AlgoConfig_closing);

            }
            catch (SQLiteException ex)
            {
                MessageBox.Show(ex.Message);
            }

          
        
        }
       
        private void GridBtnClick(object sender, DataGridViewCellEventArgs e)
        {


            //for delete button in datagridview cell
            if (e.RowIndex >= 0 && ((DataGridView)sender).Columns[e.ColumnIndex].HeaderText == "Delete")
            {
               // MessageBox.Show(e.ColumnIndex.ToString());
                //MessageBox.Show(((DataGridView)sender).Rows[e.RowIndex].Cells["id"].Value.ToString());

                //confirmation for delete scan
                if (MessageBox.Show("Do you want to delete?", "Confirm delete", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                   // MessageBox.Show(((DataGridView)sender).Rows[e.RowIndex].Cells["id"].Value.ToString());


                    long row1 = Convert.ToInt64(((DataGridView)sender).Rows[e.RowIndex].Cells["id"].Value.ToString());


                    try
                    {

                        SQLiteConnection con = new SQLiteConnection("Data Source=ScanDataBase.s3db");
                        con.Open();
                        SQLiteCommand com = new SQLiteCommand();
                        com.Connection = con;

                        // delete particular scan
                        com.CommandText = "delete from tbl_paint_type where id=" + row1;
                        com.ExecuteNonQuery();

                        con.Close();
                    }
                    catch (SQLiteException ex)
                    {
                        MessageBox.Show(ex.Message);
                    }

                    //Function to load data from the database
                    LoadData();


                }

            }
            
            //for Eidt button in datagridview
            else if (e.RowIndex >= 0 && ((DataGridView)sender).Columns[e.ColumnIndex].HeaderText == "Edit")
            {

                //fetches 
                long row2 = Convert.ToInt64(((DataGridView)sender).Rows[e.RowIndex].Cells["id"].Value.ToString());
                // PitDisplayForm PitDispForm = new PitDisplayForm();

               

                //fetches 
                try
                {
                    //PitDispForm.txtData.Text = "";
                    SQLiteConnection con = new SQLiteConnection("Data Source=ScanDataBase.s3db");
                    con.Open();
                    SQLiteCommand com = new SQLiteCommand();
                    com.Connection = con;

                    // get all pitvalues for particular scan
                    com.CommandText = "select * from tbl_paint_type where id=" + row2;
                    com.ExecuteNonQuery();
                    //PitDispForm.lblHeading.Text = "ScanId= " + row2.ToString();
                    //PitDispForm.Heading = "ScanId= " + row2.ToString();
                    SQLiteDataReader dr = com.ExecuteReader();


                    //display input in seprate form
                    while (dr.Read())
                    {
                         AlgoConfiguration algoConfig = new AlgoConfiguration(row2);
                         algoConfig.Opacity = 0;


                        algoConfig.cmbAlgo.SelectedItem = dr["Algorithm"].ToString();
                        algoConfig.txtPaint.Text = dr["PaintName"].ToString();
                        algoConfig.nmDistanceBwPits.Value=Convert.ToDecimal(dr["DistanceBwPits"].ToString());
                       // HeightOnly4First;
                        algoConfig.nmPercentage.Value=Convert.ToDecimal(dr["Percentage"].ToString());
                        algoConfig.nmThresholdValue.Value = Convert.ToDecimal(dr["ThresholdValue"].ToString());
                        algoConfig.nmAvgPoints.Value = Convert.ToDecimal(dr["AveragePoints"].ToString());
                        algoConfig.nmHeight.Value =Convert.ToDecimal(dr["Height"].ToString());
                       // rdForAllPits.Enabled = false;
                        //rdForPit1.Enabled = false;
                        algoConfig.nmRange.Value=Convert.ToDecimal(dr["Range"].ToString());
                        algoConfig.nmFactor.Value=Convert.ToDecimal(dr["Factor"].ToString());
                        algoConfig.nmSmoothingWidth.Value=Convert.ToDecimal(dr["SmoothingWidth"].ToString());
                        algoConfig.nmMinPitWidth.Value=Convert.ToDecimal(dr["MinPitWidth"].ToString());
                        algoConfig.nmCompressionFactor.Value = Convert.ToDecimal(dr["CompressionFactor"].ToString());
                        algoConfig.nmPointsInEachPit.Value=Convert.ToDecimal(dr["PointsInEachPit"].ToString());
                        algoConfig.nmSubstrateRoghness.Value=Convert.ToDecimal(dr["SubstrateRoughness"].ToString());
                        algoConfig.nmNoPoints4Avg.Value=Convert.ToDecimal(dr["NoPointAvg"].ToString());
                        algoConfig.nmNoPointsBwAvgs.Value=Convert.ToDecimal(dr["NoPointBwAvg"].ToString());
                        algoConfig.NoPointsforYmaxAvg.Value = Convert.ToDecimal(dr["NoPointsForYmaxAvg"].ToString());
                        algoConfig.nmNoCountBackPts.Value = Convert.ToDecimal(dr["NoCountBackPts"].ToString());

                        algoConfig.nmOffsetValue.Value = Convert.ToDecimal(dr["OffsetValue"].ToString());
                        algoConfig.nmAvgNum.Value = Convert.ToDecimal(dr["AveragingNum"].ToString());

                        if (dr["HeightOnly4First"].ToString()== "First")
                        {
                            algoConfig.rdForAllPits.Checked = false;
                            algoConfig.rdForPit1.Checked = true;
                        }
                        else if(dr["HeightOnly4First"].ToString() == "All")
                        {
                            algoConfig.rdForAllPits.Checked = true;
                            algoConfig.rdForPit1.Checked = false;
                        
                        }

                        //for optional height enable/disable
                        if (dr["HeightEnable"].ToString() == "Yes")
                        {
                            //algoConfig.rdForAllPits.Checked = false;
                           // algoConfig.rdForPit1.Checked = true;
                            algoConfig.grpBoxHeightOption.Enabled = true;
                            //algoConfig.chkHeightOptional.Enabled = true;
                            algoConfig.chkHeightOptional.Checked = true;
                        }
                        else if (dr["HeightEnable"].ToString() == "No")
                        {
                            //algoConfig.rdForAllPits.Checked = true;
                            //algoConfig.rdForPit1.Checked = false;
                            algoConfig.grpBoxHeightOption.Enabled = false;
                            //algoConfig.chkHeightOptional.Enabled = false;
                            algoConfig.chkHeightOptional.Checked = false;
                        }

                        //PitDispForm.txtData.Text += dr["pit_value"].ToString() + "\r\n";
                        algoConfig.Show();
                        algoConfig.tmrFade.Enabled = true;
                        algoConfig.FormClosing += new FormClosingEventHandler(AlgoConfig_closing);
                    }
                    dr.Close();

                    con.Close();
                }
                catch (SQLiteException ex)
                {
                    MessageBox.Show(ex.Message);
                }

                //Function to load data from the database
              //  LoadData();


               // PitDispForm.Show();

            }

        }


        public DataTable LoadData()
        {

            SQLiteConnection cnn = new SQLiteConnection("Data Source=ScanDataBase.s3db");

            // command to get data
            SQLiteCommand command = cnn.CreateCommand();
            command.CommandText = "SELECT id,PaintName,Algorithm FROM tbl_paint_type";

            // Create the data adapter using our select command
            SQLiteDataAdapter resultsDataAdapter = new SQLiteDataAdapter(command);

            // The command builder will take care of our update, insert, deletion commands
            SQLiteCommandBuilder commandBuilder = new SQLiteCommandBuilder(resultsDataAdapter);

            // Create a dataset and fill it with our records
            dataTable.Clear();
            resultsDataAdapter.Fill(dataTable);

            return dataTable;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //PasswordFrm pwdFrm = new PasswordFrm();
            //pwdFrm.Show();
            AlgoConfiguration algoConfig = new AlgoConfiguration();
            algoConfig.Opacity = 0;
            algoConfig.Show();
            algoConfig.tmrFade.Enabled = true;
            algoConfig.FormClosing += new FormClosingEventHandler(AlgoConfig_closing);
            //algoConfig.Show();
        }

        void AlgoConfig_closing(object sender, FormClosingEventArgs e)
        {
          //  ScanDataGrid.DataSource = null;
            dataTable.Clear();
            LoadData();
            
            //MessageBox.Show("closed");
        }

        

        private void btnSave_Click_1(object sender, EventArgs e)
        {

        }

        private void tpAlgorithms_Enter(object sender, EventArgs e)
        {
            btnAddPaint.Enabled = false;
            ScanDataGrid.Enabled = false;
            btnUnlock.Enabled = true;
            btnUnlock.Text = "Unlock";
            txtPwd.Text = "";

        }

        private void controlButtonsContainer_Panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnUnlock_Click(object sender, EventArgs e)
        {
            if (txtPwd.Text == "admin")
            {
                lblIncorrectPwd.Visible = false;
                btnAddPaint.Enabled = true;
                ScanDataGrid.Enabled = true;
                btnUnlock.Enabled = false;
                btnUnlock.Text = "Unlocked";
                txtPwd.Text = "";
            }
            else
            {
                lblIncorrectPwd.Visible = true;
                btnAddPaint.Enabled = false;
                ScanDataGrid.Enabled = false;
                btnUnlock.Enabled = true;
                btnUnlock.Text = "Unlock";
                txtPwd.Text = "";
            
            
            }
        }

        private void tmrFade_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            this.Opacity += 0.10;
            if (this.Opacity >= .95)
            {
                this.Opacity = 1;
                tmrFade.Enabled = false;
            }
        }

        private void grpFilteration_Enter(object sender, EventArgs e)
        {

        }

        private void chkAppyLeftMiddleRight_CheckedChanged(object sender, EventArgs e)
        {
            if (chkAppyLeftMiddleRight.Checked)
            {

                grpPits.Enabled = true;
            }
            else
            {
                grpPits.Enabled = false;
            }
        }

        private void tpScanTiming_Click(object sender, EventArgs e)
        {

        }

        private void numDAQStart_ValueChanged(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void lblDAQStart_Click(object sender, EventArgs e)
        {

        }

        private void rdBtnAutoDetect_CheckedChanged(object sender, EventArgs e)
        {
            if (rdBtnAutoDetect.Checked == true)
            {
                grpBxAuto.Enabled = true;
                grpBxTimer.Enabled = false;
            }
            else
            {
                grpBxTimer.Enabled = true;
                grpBxAuto.Enabled = false;
            }
        }

        private void rdBtnTimer_CheckedChanged(object sender, EventArgs e)
        {
            if (rdBtnTimer.Checked == true)
            {
                grpBxAuto.Enabled = false;
                grpBxTimer.Enabled = true;
            }
            else
            {
                grpBxTimer.Enabled = false;
                grpBxAuto.Enabled = true;
            }

        }

        private void nmIgnoreEdge_ValueChanged(object sender, EventArgs e)
        {
          //  MessageBox.Show("value changed");
        }

      
    }
}
