﻿namespace DFM
{
    partial class CaptionedDisplay
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.Panel pnlDisplay;
            this.lblValue = new System.Windows.Forms.Label();
            this.lblCaption = new System.Windows.Forms.Label();
            pnlDisplay = new System.Windows.Forms.Panel();
            pnlDisplay.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlDisplay
            // 
            pnlDisplay.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            pnlDisplay.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            pnlDisplay.Controls.Add(this.lblValue);
            pnlDisplay.Controls.Add(this.lblCaption);
            pnlDisplay.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlDisplay.Location = new System.Drawing.Point(0, 0);
            pnlDisplay.Name = "pnlDisplay";
            pnlDisplay.Size = new System.Drawing.Size(174, 119);
            pnlDisplay.TabIndex = 0;
            // 
            // lblValue
            // 
            this.lblValue.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblValue.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblValue.Location = new System.Drawing.Point(-1, -1);
            this.lblValue.Name = "lblValue";
            this.lblValue.Size = new System.Drawing.Size(174, 87);
            this.lblValue.TabIndex = 6;
            this.lblValue.Text = "28.3";
            this.lblValue.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblCaption
            // 
            this.lblCaption.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCaption.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCaption.Location = new System.Drawing.Point(3, 86);
            this.lblCaption.Name = "lblCaption";
            this.lblCaption.Size = new System.Drawing.Size(166, 24);
            this.lblCaption.TabIndex = 7;
            this.lblCaption.Text = "Average of 3";
            this.lblCaption.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // CaptionedDisplay
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(pnlDisplay);
            this.Name = "CaptionedDisplay";
            this.Size = new System.Drawing.Size(174, 119);
            pnlDisplay.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblCaption;
        private System.Windows.Forms.Label lblValue;
    }
}
