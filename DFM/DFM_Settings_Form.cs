﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using DFM.Settings;
using DFM.Settings.Bindable;
using System.Xml.Serialization;
using System.IO;
using SensorControl;
using SensorControl.Sensors;
using DFM.Motor;
using NLog;
using System.IO.Ports;
using DFM.Laser;
using PaintAnalysis.Common;

namespace DFM
{
    public partial class DFM_Settings_Form : Form
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        #region Data Members

        private SettingsGroup settings;
        private UserSettings savedSettings;

        private ISensor[] sensors;
        private ILaser laser;
        private IMotorControl motor;

        private Form openingForm;
        #endregion

        public DFM_Settings_Form()
        {
            InitializeComponent();
            
            btnSave.Click += new EventHandler(btnSave_Click);
            btnCancel.Click += new EventHandler(btnCancel_Click);
            btnOK.Click += new EventHandler(btnOK_Click);
                        
            FormClosing += new FormClosingEventHandler(DFM_Settings_Form_FormClosing);

            string debugFile = Path.Combine(Path.GetDirectoryName(Application.ExecutablePath), "debug.txt");
            IsDebugging = File.Exists(debugFile);
            if (!IsDebugging)
            {
                sensors = new ISensor[3];
                sensors[0] = new ILD2200_MESensor();
                sensors[1] = new ILD1700_MESensor();
                sensors[2] = new ILD2300_MESensor();
                laser = new PLCLaser();
                motor = new MotorControl();
            }
            else
            {
                logger.Trace("Debugging");
                sensors = new ISensor[5];
                sensors[0] = new ILD2200_MESensor();
                sensors[1] = new ILD1700_MESensor();
                sensors[2] = new ILD2300_MESensor();
                sensors[3] = new MockSensor();
                sensors[4] = new MockSensorFileScans(File.ReadAllText(debugFile));
                laser = new MockLaser();
                motor = new MockMotorControl(100, 600);
            }
            
            FormTitle = "DFM Settings";
            Settings = new SettingsGroup("DFM Settings");
            init();
            Settings.ModifiedChanged += new EventHandler(Settings_ModifiedChanged);
            btnSave.Click += new EventHandler(Settings_ModifiedChanged);
            btnOK.Click += new EventHandler(Settings_ModifiedChanged);
            btnCancel.Click += new EventHandler(Settings_ModifiedChanged);
        }

        #region General Form Behaviour

        public Form OpeningForm
        {
            get { return openingForm; }
            set { openingForm = value; }
        }    

        public SettingsGroup Settings
        {
            get { return settings; }
            private set { settings = value; }
        }

        public string FormTitle
        {
            get { return lblHeading.Text; }
            set { lblHeading.Text = Text = value; }
        }
        
        private void message(string m)
        {
            MessageBox.Show(m);
        }

        void Settings_ModifiedChanged(object sender, EventArgs e)
        {
            Text = (Settings.IsModified ? "* " : "") + FormTitle;
        }

        void DFM_Settings_Form_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing || e.CloseReason == CloseReason.None)
            {                
                if (OpeningForm != null) e.Cancel = true;
                
                bool changed = Settings.IsModified;
                bool valid = Settings.IsValid;

                if (this.DialogResult == DialogResult.Cancel || this.DialogResult == System.Windows.Forms.DialogResult.None)
                {
                    if (!changed || MessageBox.Show("You will lose any changes. Continue?", "Cancelling options changes", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        // reset form first
                        if (changed) Settings.Revert();
                        if (OpeningForm != null) this.Hide();
                    }
                    else e.Cancel = true;
                }
                else if (this.DialogResult == DialogResult.OK)
                {
                    if (valid)
                    {
                        Settings.Save();
                        savedSettings.Settings = Settings;
                        savedSettings.Save();
                        if (OpeningForm != null) this.Hide();
                    }
                    else
                    {
                        e.Cancel = true;
                        message("Error: " + Settings.ValidationErrors[0].ErrorMessage);
                    }
                }
                else
                {
                    // close for some other reason - then treat it as a cancel
                    if (changed) Settings.Revert();
                    if (OpeningForm != null) this.Hide();
                }
            }
        }

        void btnOK_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        void btnSave_Click(object sender, EventArgs e)
        {
            Save();
        }

        public void Save()
        {
            if (Settings.IsValid)
            {
                Settings.Save();
                savedSettings.Settings = Settings;
                savedSettings.Save();
            }
            else
            {
                message("Error: " + Settings.ValidationErrors[0].ErrorMessage);
            }
        }

        #endregion

        #region DFM Specific Behaviour
        
        public bool IsDebugging
        {
            get;
            protected set;
        }
        
        #region Initialisation

        private void init()
        {
            logger.Trace("Initing DFM settings form");
            FormTitle = "DFM Settings";

            logger.Trace("Adding DFM settings form events");
            addEvents();

            SensorAddressType = SensorAddressTypes.COM_Port;

            logger.Trace("Creating DFM settings objects");
            try
            {
                addSettings();
            }
            catch (Exception ex)
            {
                logger.ErrorException("Failed creating DFM settings objects", ex);
                message("Initialising settings objects failed");
                return;
            }

            logger.Trace("Starting to load saved settings");
            savedSettings = new UserSettings(Settings.Name);
            if (savedSettings.Settings != null)
            {
                logger.Trace("Found saved settings");
                Settings.LoadSettingsFrom(savedSettings.Settings);
            }
            else
            {
                logger.Trace("Found no saved settings");
            }

            addValidators();

            // connect button text
            ConnectionStateChanged += new EventHandler<ConnectionStateEventArgs>(delegate(object sender, ConnectionStateEventArgs e)
            {
                if (e.ConnectionState == ConnectionStates.Connected) DFM.Settings.Bindable.ThreadSafeControlUpdates.UpdateText(btnConnect, "Disconnect");
                else DFM.Settings.Bindable.ThreadSafeControlUpdates.UpdateText(btnConnect, "Connect");
            });
            
            // set saved settings
            Settings.Save();

            // add global enabler
            HybridSetting enabler = new HybridSetting("form enabling");
            isEnabled = new SettingValue<bool>("enabled", true);
            enabler.AddEnableSetting(isEnabled, false); // don't disable validation
            enabler.Add(Settings);

            // add units changer handler and trigger it once.   
            SettingValue<DataUnits> tmp = (SettingValue<DataUnits>)Settings.FetchSetting("data/units");
            tmp.ValueChanged += new EventHandler<ValueChangedEventArgs<DataUnits>>(handleDataUnitsChanged);
            handleDataUnitsChanged(cmbUnits, new ValueChangedEventArgs<DataUnits>(tmp.Value, tmp));
        }

        private void addSettings()
        {
            HybridSetting tmp;
            {
                // start connection
                SettingsGroup connection = new SettingsGroup("connection");

                // sensor connection
                HybridSetting sensorConnection = new HybridSetting("sensor");
                sensorConnection.AddEnableSetting(new BindableBoolSetting(chkConnectSensor, "enabled", chkConnectSensor.Checked));
                sensorConnection.Add(new BindableStringArraySetting(cmbSensorModel, "model", new string[] { }));
                sensorConnection.Add(new BindableStringArraySetting(cmbSensorType, "type", new string[] { }));
                sensorConnection.Add(new BindableStringArraySetting(cmbSensorPort, "port", new string[] { }));
                sensorConnection.Add(new BindableSettingValue<string, TextBox>(txtSensorAddress, "ip_address", ""));
                sensorConnection.Add(new BindableStringArraySetting(cmbSensorBaudrate, "baudrate", new string[] { }));

                // laser connection
                HybridSetting laserConnection = new HybridSetting("laser");
                laserConnection.AddEnableSetting(new BindableBoolSetting(chkConnectLaser, "enabled", chkConnectLaser.Checked));
                laserConnection.Add(new BindableStringArraySetting(cmbLaserPort, "port", new string[] { }));
                laserConnection.Add(new BindableStringArraySetting(cmbLaserBaudrate, "baudrate", new string[] { }));
                laserConnection.Add(new BindableStringArraySetting(cmbLaserHandshake, "handshake", new string[] { "RequestToSend", "None" }, "RequestToSend"));

                // motor connection
                HybridSetting motorConnection = new HybridSetting("motor");
                motorConnection.AddEnableSetting(new BindableBoolSetting(chkConnectMotor, "enabled", chkConnectMotor.Checked));
                motorConnection.Add(new BindableStringArraySetting(cmbMotorAddress, "address", new string[] { }));

                // end connection
                connection.Add(sensorConnection);
                connection.Add(laserConnection);
                connection.Add(motorConnection);
                Settings.Add(connection);

                // update all above
                updateSensorModel();
                updateSensorTypes();
                updateComPorts();
                updateBaudrates();
                updateAddresses();

                // enabler by connection state
                tmp = new HybridSetting("tmp");
                ConnectionState = ConnectionStates.Disconnected; // default value
                SettingValue<bool> enabler = new SettingValue<bool>("enabled", ConnectionState == ConnectionStates.Disconnected);

                ConnectionStateChanged += new EventHandler<ConnectionStateEventArgs>(delegate(object sender, ConnectionStateEventArgs e)
                {
                    enabler.Value = e.ConnectionState == ConnectionStates.Disconnected;
                });

                tmp.AddEnableSetting(enabler);
                tmp.Add(connection);
            }

            {
                // start scan parameters
                SettingsGroup scanParameters = new SettingsGroup("scan");

                // scan timing
                SettingsGroup scanTiming = new SettingsGroup("timing");
                scanTiming.Add(new BindableIntSetting(numDAQStart, "daq start", Convert.ToInt32(numDAQStart.Value)));
                scanTiming.Add(new BindableIntSetting(numLaserStart, "laser start", Convert.ToInt32(numLaserStart.Value)));
                scanTiming.Add(new BindableIntSetting(numDAQStop, "daq stop", Convert.ToInt32(numDAQStop.Value)));

                HybridSetting repeat = new HybridSetting("repeat");
                repeat.AddEnableSetting(new BindableBoolSetting(chkRepeat, "enabled", chkRepeat.Checked));
                repeat.Add(new BindableIntSetting(numRepeatCount, "count", Convert.ToInt32(numRepeatCount.Value)));
                repeat.Add(new BindableFloatSetting(numRepeatInterval, "interval", Convert.ToSingle(numRepeatInterval.Value)));
                scanTiming.Add(repeat);

                HybridSetting repeatScan = new HybridSetting("repeat scan");
                repeatScan.AddEnableSetting(new BindableBoolSetting(chkRepeatScan, "enabled", chkRepeatScan.Checked));
                repeatScan.Add(new BindableIntSetting(numRepeatScanInterval, "interval", Convert.ToInt32(numRepeatScanInterval.Value)));
                scanTiming.Add(repeatScan);

                scanTiming.Add(new BindableBoolSetting(chkHomeAfterScans, "home after", chkHomeAfterScans.Checked));

                // laser
                SettingsGroup scanLaser = new SettingsGroup("laser");
                BindableEnumSettingRadioButtons<LaserDurationSpecification> laserDuration = new BindableEnumSettingRadioButtons<LaserDurationSpecification>("duration specification");
                laserDuration.Bind(radPulseCount, LaserDurationSpecification.PulseCount); // default as first
                laserDuration.Bind(radRunTime, LaserDurationSpecification.RunTime);
                scanLaser.Add(laserDuration);

                scanLaser.Add(new BindableIntSetting(numPulseCount, "pulse count", Convert.ToInt32(numPulseCount.Value)));
                scanLaser.Add(new BindableIntSetting(numRunTime, "run time", Convert.ToInt32(numRunTime.Value)));
                scanLaser.Add(new BindableIntSetting(numGatingFreq, "gating frequency", Convert.ToInt32(numGatingFreq.Value)));
                scanLaser.Add(new BindableFloatSetting(numDutyCycle, "duty cycle", Convert.ToSingle(numDutyCycle.Value)));

                HybridSetting validate = new HybridSetting("validate pulse count");
                validate.AddEnableSetting(new BindableBoolSetting(chkValidatePulseCount, "enabled", chkValidatePulseCount.Checked));
                validate.Add(new BindableBoolSetting(chkFilterPulseCountErrors, "filter errors", chkFilterPulseCountErrors.Checked));
                validate.Add(new BindableBoolSetting(chkDisplayPulseCountErrorMessages, "display message", chkDisplayPulseCountErrorMessages.Checked));
                scanLaser.Add(validate);

                // laser duration enablers - tmps only
                tmp = new HybridSetting("tmp");
                tmp.AddEnableSetting(new BindableBoolRadioSetting(radRunTime, "enabled", radRunTime.Checked));
                tmp.Add(scanLaser.FetchSetting("run time"));

                tmp = new HybridSetting("tmp");
                tmp.AddEnableSetting(new BindableBoolRadioSetting(radPulseCount, "enabled", radPulseCount.Checked));
                tmp.Add(scanLaser.FetchSetting("pulse count"));


                // motor
                SettingsGroup scanMotor = new SettingsGroup("motor");

                HybridSetting leftPos = new HybridSetting("left pos");
                leftPos.AddEnableSetting(new BindableInvertedBoolSetting(chkSkipMotorLeftPos, "enabled", !chkSkipMotorLeftPos.Checked));
                leftPos.Add(new BindableIntSetting(numMotorLeftPos, "value", Convert.ToInt32(numMotorLeftPos.Value)));

                HybridSetting middlePos = new HybridSetting("middle pos");
                middlePos.AddEnableSetting(new BindableInvertedBoolSetting(chkSkipMotorMiddlePos, "enabled", !chkSkipMotorMiddlePos.Checked));

                HybridSetting middleValue = new HybridSetting("pos");
                middleValue.Add(new BindableIntSetting(numMotorMiddlePos, "value", Convert.ToInt32(numMotorMiddlePos.Value)));
                middleValue.AddEnableSetting(new BindableInvertedBoolSetting(chkCentreMiddle, "centred", !chkCentreMiddle.Checked)); // add motor centering
                middlePos.Add(middleValue);

                HybridSetting rightPos = new HybridSetting("right pos");
                rightPos.AddEnableSetting(new BindableInvertedBoolSetting(chkSkipMotorRightPos, "enabled", !chkSkipMotorRightPos.Checked));
                rightPos.Add(new BindableIntSetting(numMotorRightPos, "value", Convert.ToInt32(numMotorRightPos.Value)));

                SettingsGroup labels = new SettingsGroup("labels");
                labels.Add(new BindableSettingValue<string, TextBox>(txtMotorLeftPositionLabel, "left", txtMotorLeftPositionLabel.Text));
                labels.Add(new BindableSettingValue<string, TextBox>(txtMotorMiddlePositionLabel, "middle", txtMotorMiddlePositionLabel.Text));
                labels.Add(new BindableSettingValue<string, TextBox>(txtMotorRightPositionLabel, "right", txtMotorRightPositionLabel.Text));

                scanMotor.Add(leftPos);
                scanMotor.Add(middlePos);
                scanMotor.Add(rightPos);
                scanMotor.Add(labels);

                // misc
                HybridSetting painting = new HybridSetting("painting input");
                painting.AddEnableSetting(new BindableBoolSetting(chkRequirePainting, "required", chkRequirePainting.Checked));
                painting.Add(new BindableIntSetting(numPaintingDelay, "delay", Convert.ToInt32(numPaintingDelay.Value)));

                // enablers: for timing controls, and other settings
                tmp = new HybridSetting("tmp");
                tmp.AddEnableSetting((SettingValue<bool>)Settings.FetchSetting("connection/sensor/enabled"));
                tmp.Add(scanTiming.FetchSetting("daq start"));
                tmp.Add(scanTiming.FetchSetting("daq stop"));

                tmp = new HybridSetting("tmp");
                tmp.AddEnableSetting((SettingValue<bool>)Settings.FetchSetting("connection/laser/enabled"));
                tmp.Add(scanTiming.FetchSetting("laser start"));
                tmp.Add(scanLaser);

                tmp = new HybridSetting("tmp");
                tmp.AddEnableSetting((SettingValue<bool>)Settings.FetchSetting("connection/motor/enabled"));
                tmp.Add(scanMotor);

                // end scan parameters
                scanParameters.Add(scanTiming);
                scanParameters.Add(scanLaser);
                scanParameters.Add(scanMotor);
                scanParameters.Add(painting);
                Settings.Add(scanParameters);
            }

            {
                // start data analysis
                SettingsGroup dataAnalysis = new SettingsGroup("data");

                // data processing
                SettingsGroup dataProcessing = new SettingsGroup("processing");
                dataProcessing.Add(new BindableIntSetting(numTruncateFirst, "truncate first", Convert.ToInt32(numTruncateFirst.Value)));
                dataProcessing.Add(new BindableIntSetting(numTruncateLast, "truncate last", Convert.ToInt32(numTruncateLast.Value)));

                HybridSetting filtering = new HybridSetting("filtering");
                filtering.AddEnableSetting(new BindableBoolSetting(chkFilterPeaks, "enabled", chkFilterPeaks.Checked));
                filtering.Add(new BindableFloatSetting(numPeakThreshold, "peak threshold", Convert.ToSingle(numPeakThreshold.Value)));
                filtering.Add(new BindableFloatSetting(numPeakHysteresis, "peak hysteresis", Convert.ToSingle(numPeakHysteresis.Value)));
                filtering.Add(new BindableIntSetting(numMaxPeakWidth, "max peak width", Convert.ToInt32(numMaxPeakWidth.Value)));
                dataProcessing.Add(filtering);

                // analysis parameters
                SettingsGroup analysis = new SettingsGroup("analysis");
                analysis.Add(new BindableFloatSetting(numSubstrateRoughness, "substrate roughness", Convert.ToSingle(numSubstrateRoughness.Value)));
                analysis.Add(new BindableFloatSetting(numThresholdParam, "threshold parameter", Convert.ToSingle(numThresholdParam.Value)));
                analysis.Add(new BindableIntSetting(numSmoothingWidth, "smoothing width", Convert.ToInt32(numSmoothingWidth.Value)));
                analysis.Add(new BindableIntSetting(numMinPitWidth, "min pit width", Convert.ToInt32(numMinPitWidth.Value)));
                analysis.Add(new BindableFloatSetting(numCompressionFactor, "compression factor", Convert.ToSingle(numCompressionFactor.Value)));
                analysis.Add(new BindableIntSetting(numPitPoints, "pit count", Convert.ToInt32(numPitPoints.Value)));

                // data saving
                SettingsGroup saving = new SettingsGroup("saving");
                saving.Add(new BindableSettingValue<string, TextBox>(txtDataLocation, "location", txtDataLocation.Text));
                saving.Add(new BindableSettingValue<string, TextBox>(txtFilenamePattern, "filename pattern", txtFilenamePattern.Text));

                SettingsGroup boundaries = new SettingsGroup("boundaries");
                boundaries.Add(new BindableScaledSetting(numTargetThickness, "target", Convert.ToSingle(numTargetThickness.Value)));
                boundaries.Add(new BindableScaledSetting(numThicknessRange, "range", Convert.ToSingle(numThicknessRange.Value)));

                HybridSetting filterErrors = new HybridSetting("filtering");
                filterErrors.AddEnableSetting(new BindableBoolSetting(chkFilterErrors, "enabled", chkFilterErrors.Checked));
                filterErrors.Add(new BindableFloatSetting(numFilterErrorsThreshold, "threshold", Convert.ToSingle(numFilterErrorsThreshold.Value)));
                boundaries.Add(filterErrors);

                dataAnalysis.Add(new BindableIntSetting(numScanAveNumber, "scan averaging number", Convert.ToInt32(numScanAveNumber.Value)));
                dataAnalysis.Add(new BindableEnumSettingComboBox<DataUnits>(cmbUnits, "units", DataUnits.Metric));

                // end data analysis
                dataAnalysis.Add(dataProcessing);
                dataAnalysis.Add(analysis);
                dataAnalysis.Add(saving);
                dataAnalysis.Add(boundaries);
                Settings.Add(dataAnalysis);
            }

            {
                // start misc
                SettingsGroup misc = new SettingsGroup("misc");
                misc.Add(new SettingValue<string>("password", ""));
                misc.Add(new BindableBoolSetting(chkEnableQuickScan, "enable quick scan", chkEnableQuickScan.Checked));

                Settings.Add(misc);
            }
        }

        private void addEvents()
        {
            cmbSensorModel.SelectedIndexChanged += new EventHandler(cmbSensorModel_SelectedIndexChanged);
            btnRefreshCOMPorts.Click += new EventHandler(btnRefreshCOMPorts_Click);
            btnConnect.Click += new EventHandler(btnConnect_Click);

            btnChooseDataLocation.Click += new EventHandler(btnChooseDataLocation_Click);

            btnChangePassword.Click += new EventHandler(btnChangePassword_Click);
        }

        private void addValidators()
        {
            SettingValue<int> daqStart = (SettingValue<int>)Settings.FetchSetting("scan/timing/daq start");
            SettingValue<int> laserStart = (SettingValue<int>)Settings.FetchSetting("scan/timing/laser start");
            SettingValue<int> daqStop = (SettingValue<int>)Settings.FetchSetting("scan/timing/daq stop");
            SettingValue<float> repeatInterval = (SettingValue<float>)Settings.FetchSetting("scan/timing/repeat/interval"); // in seconds

            SettingValue<bool> sensorEnabled = (SettingValue<bool>)Settings.FetchSetting("connection/sensor/enabled");
            SettingValue<bool> laserEnabled = (SettingValue<bool>)Settings.FetchSetting("connection/laser/enabled");

            laserStart.ValidateOnValueChangeOnly = false;
            laserStart.AddValidator(new SettingValidator<int>(delegate(ISettingValue<int> settings, ValidationResult<int> e)
            {
                if ((e.Value > daqStop.Value || e.Value < daqStart.Value) && laserEnabled.Value && sensorEnabled.Value)
                {
                    e.IsError = true;
                    e.ErrorMessage = "Laser start time should be during DAQ cycle";
                }
            }));
            
            daqStop.ValidateOnValueChangeOnly = false;
            daqStop.AddValidator(new SettingValidator<int>(delegate(ISettingValue<int> settings, ValidationResult<int> e)
            {
                if (daqStart.Value >= e.Value && sensorEnabled.Value)
                {
                    e.IsError = true;
                    e.ErrorMessage = "DAQ stop time should be after DAQ start time";
                }
            }));

            repeatInterval.ValidateOnValueChangeOnly = false;
            repeatInterval.AddValidator(new SettingValidator<float>(delegate(ISettingValue<float> settings, ValidationResult<float> e)
            {
                if ((daqStop.Value >= 1000 * e.Value && sensorEnabled.Value) || (laserStart.Value >= e.Value * 1000 && laserEnabled.Value))
                {
                    e.IsError = true;
                    e.ErrorMessage = "The repeat time must be longer than the DAQ cycle and the laser cycle";
                }
            }));
            
            // validate data location
            SettingValue<string> dataFolder = (SettingValue<string>)Settings.FetchSetting("data/saving/location");
            SettingValue<string> filename = (SettingValue<string>)Settings.FetchSetting("data/saving/filename pattern");
            SettingValue<float> targetThickness = (SettingValue<float>)Settings.FetchSetting("data/boundaries/target");
            SettingValue<float> targetRange = (SettingValue<float>)Settings.FetchSetting("data/boundaries/range");

            dataFolder.ValidateOnValueChangeOnly = false;
            dataFolder.AddValidator(new SettingValidator<string>(delegate(ISettingValue<string> setting, ValidationResult<string> e)
            {
                if (!Directory.Exists(e.Value))
                {
                    e.IsError = true;
                    e.ErrorMessage = "Could not find directory: " + setting.Value;
                }
            }));

            filename.AddValidator(new SettingValidator<string>(delegate(ISettingValue<string> setting, ValidationResult<string> e)
            {
                e.Value = e.Value.Trim();
                if (String.IsNullOrEmpty(e.Value))
                {
                    e.IsError = true;
                    e.ErrorMessage = "Enter a filename pattern";
                }
            }));

            targetThickness.AddValidator(new SettingValidator<float>(delegate(ISettingValue<float> setting, ValidationResult<float> e)
            {
                if (e.Value < targetRange.Value)
                {
                    e.IsError = true;
                    e.ErrorMessage = "The target thickness should be greater than the target range";
                }
            }));

            targetRange.AddValidator(new SettingValidator<float>(delegate(ISettingValue<float> setting, ValidationResult<float> e)
            {
                if (e.Value > targetThickness.Value)
                {
                    e.IsError = true;
                    e.ErrorMessage = "The target thickness should be greater than the target range";
                }
            }));

            // validate motor positions
            SettingValue<int> leftPos = (SettingValue<int>)Settings.FetchSetting("scan/motor/left pos/value");
            SettingValue<int> middlePos = (SettingValue<int>)Settings.FetchSetting("scan/motor/middle pos/pos/value");
            SettingValue<int> rightPos = (SettingValue<int>)Settings.FetchSetting("scan/motor/right pos/value");
            
            leftPos.ValidateOnValueChangeOnly = false;
            leftPos.AddValidator(new SettingValidator<int>(delegate(ISettingValue<int> setting, ValidationResult<int> e)
            {
                if (Motor.EdgesFound && Motor.LeftEdge > -1 && Motor.RightEdge > -1)
                {
                    int tmp = ProcessMovePosition(e.Value);

                    if (tmp <= 0 || tmp >= Motor.RightEdge)
                    {
                        e.IsError = true;
                        e.ErrorMessage = "Left position has to be within the strip";
                    }
                }
            }));
            
            middlePos.ValidateOnValueChangeOnly = false;
            middlePos.AddValidator(new SettingValidator<int>(delegate(ISettingValue<int> setting, ValidationResult<int> e)
            {
                if (Motor.EdgesFound && Motor.LeftEdge > -1 && Motor.RightEdge > -1)
                {
                    int tmp = ProcessMovePosition(e.Value);

                    if (tmp <= 0 || tmp >= Motor.RightEdge)
                    {
                        e.IsError = true;
                        e.ErrorMessage = "Middle position has to be within the strip and to the right of the left position";
                    }
                }  
            }));
            
            rightPos.ValidateOnValueChangeOnly = false;
            rightPos.AddValidator(new SettingValidator<int>(delegate(ISettingValue<int> setting, ValidationResult<int> e)
            {
                if (Motor.EdgesFound && Motor.LeftEdge > -1 && Motor.RightEdge > -1)
                {
                    int tmp = ProcessMovePosition(e.Value);

                    if (tmp <= 0 || tmp >= Motor.RightEdge)
                    {
                        e.IsError = true;
                        e.ErrorMessage = "Right position has to be within the strip and to the right of the middle position";
                    }
                }
            }));



            // check at least one motor position enabled
            SettingValue<bool> leftPosE = (SettingValue<bool>)Settings.FetchSetting("scan/motor/left pos/enabled");
            SettingValue<bool> middlePosE = (SettingValue<bool>)Settings.FetchSetting("scan/motor/middle pos/enabled");
            SettingValue<bool> rightPosE = (SettingValue<bool>)Settings.FetchSetting("scan/motor/right pos/enabled");

            leftPosE.AddValidator(new SettingValidator<bool>(delegate(ISettingValue<bool> setting, ValidationResult<bool> e)
            {
                if (!(e.Value || middlePosE.Value || rightPosE.Value))
                {
                    e.IsError = true;
                    e.ErrorMessage = "Enable at least one motor position";
                }
            }));
            middlePosE.AddValidator(new SettingValidator<bool>(delegate(ISettingValue<bool> setting, ValidationResult<bool> e)
            {
                if (!(leftPosE.Value || e.Value || rightPosE.Value))
                {
                    e.IsError = true;
                    e.ErrorMessage = "Enable at least one motor position";
                }
            }));
            rightPosE.AddValidator(new SettingValidator<bool>(delegate(ISettingValue<bool> setting, ValidationResult<bool> e)
            {
                if (!(leftPosE.Value || middlePosE.Value || e.Value))
                {
                    e.IsError = true;
                    e.ErrorMessage = "Enable at least one motor position";
                }
            }));

            // auto-set centre position
            SettingValue<bool> middlePosCentred = (SettingValue<bool>)Settings.FetchSetting("scan/motor/middle pos/pos/centred");
            middlePosCentred.ValueChanged += new EventHandler<ValueChangedEventArgs<bool>>(delegate(object sender, ValueChangedEventArgs<bool> e)
            {
                if (Motor.EdgesFound && Motor.LeftEdge > -1 && Motor.RightEdge > -1)
                {
                    if (!e.Value) middlePos.Value = (-Motor.LeftEdge + Motor.RightEdge) / 2; // fix for positions as offsets now
                }
            });
            Motor.FinishedSetup += new MotorTaskHandler(delegate(object sender, MotorTaskEventArgs e)
            {
                if (Motor.EdgesFound && Motor.LeftEdge > -1 && Motor.RightEdge > -1)
                {
                    if (!middlePosCentred.Value) middlePos.Value = (-Motor.LeftEdge + Motor.RightEdge) / 2;
                    middlePos.Save(); // avoid setup failing because unsaved
                }
            });
        }

        public int ProcessMovePosition(int moveTo)
        {
            if (moveTo < 0) return moveTo + Motor.RightEdge;
            return moveTo + Motor.LeftEdge;
        }        

        private SettingValue<bool> isEnabled;
        public bool IsEnabled
        {
            get
            {
                if (isEnabled == null)
                {
                    logger.Warn("Tried to get DFM_Settings_Form.IsEnabled when isEnabled setting didn't exist");
                    return true;
                }
                return isEnabled.Value;
            }
            set
            {
                if (isEnabled == null)
                {
                    logger.Warn("Tried to set DFM_Settings_Form.IsEnabled when isEnabled setting didn't exist");
                    return;
                }
                isEnabled.Value = value;
            }            
        }

        #endregion

        #region Units Change
        
        private void handleDataUnitsChanged(object sender, ValueChangedEventArgs<DataUnits> e)
        {
            // microns vs mils for boundary lines
            BindableScaledSetting tmp1 = (BindableScaledSetting)Settings.FetchSetting("data/boundaries/target");
            BindableScaledSetting tmp2 = (BindableScaledSetting)Settings.FetchSetting("data/boundaries/range");
            tmp1.Precision = 5;
            tmp2.Precision = 5;
            if (e.Value == DataUnits.Metric)
            {
                tmp1.Scale = 1.0;
                tmp1.Precision = 2;
                tmp1.Increment = 1;
                tmp2.Scale = 1.0;
                tmp2.Precision = 2;
                tmp2.Increment = 1;
                lblBoundaryLines.Text = "Target Thickness (microns)";
            }
            else
            {
                double scale = UnitConverter.ConvertDistance("micron", "mil", 1.0);

                tmp1.Scale = scale;
                tmp1.Precision = 3;
                tmp1.Increment = (decimal)0.01;
                tmp2.Scale = scale;
                tmp2.Precision = 3;
                tmp2.Increment = (decimal)0.01;
                lblBoundaryLines.Text = "Target Thickness (mils)";
            }
            
            /*
            // motor positions
            BindableScaledSetting motor1 = (BindableScaledSetting)Settings.FetchSetting("scan/motor/left pos/value");
            BindableScaledSetting motor2 = (BindableScaledSetting)Settings.FetchSetting("scan/motor/middle pos/pos/value");
            BindableScaledSetting motor3 = (BindableScaledSetting)Settings.FetchSetting("scan/motor/right pos/value");
            if (e.Value == DataUnits.Metric)
            {
                foreach (BindableScaledSetting m in new BindableScaledSetting[] { motor1, motor2, motor3 })
                {
                    m.Precision = 5;
                    m.Scale = 1.0;
                    m.Precision = 0;
                }
                lblMotorUnit1.Text = lblMotorUnit2.Text = lblMotorUnit3.Text = "mm";
            }
            else
            {
                double scale = UnitConverter.ConvertDistance("mm", "inch", 1.0);
                foreach (BindableScaledSetting m in new BindableScaledSetting[] { motor1, motor2, motor3 })
                {
                    m.Precision = 5;
                    m.Scale = scale;
                    m.Precision = 1;
                }
                lblMotorUnit1.Text = lblMotorUnit2.Text = lblMotorUnit3.Text = "mm";
            }*/
        }
        #endregion

        #region Updating

        private void updateSensorModel()
        {
            string[] tmp = new string[sensors.Length];
            for (int i = 0; i < sensors.Length; i++)
            {
                tmp[i] = sensors[i].Properties.ModelName;
            }
            
            BindableStringArraySetting sensorModel = (BindableStringArraySetting)Settings.FetchSetting("connection/sensor/model");
            sensorModel.Values = tmp;
        }

        private void updateSensorTypes()
        {
            ISensor selected = Sensor;
            if (selected == null)
            {
                if (cmbSensorModel.Items.Count > 0) logger.Error("Could not work out what sensor was selected when updating sensor types");
                return;
            }

            // todo: can simplify
            string[] tmp = new string[selected.SensorTypes.Length];
            for (int i = 0; i < selected.SensorTypes.Length; i++)
            {
                tmp[i] = selected.SensorTypes[i];
            }

            BindableStringArraySetting sensorType = (BindableStringArraySetting)Settings.FetchSetting("connection/sensor/type");
            sensorType.Values = tmp;
        }

        private void updateComPorts()
        {
            string[] ports = SerialPort.GetPortNames();
            Array.Sort(ports);

            BindableStringArraySetting tmp = (BindableStringArraySetting)Settings.FetchSetting("connection/sensor/port");
            tmp.Values = ports;

            tmp = (BindableStringArraySetting)Settings.FetchSetting("connection/laser/port");
            tmp.Values = ports;
        }

        private void updateBaudrates()
        {
            // do sensor
            ISensor selected = Sensor;
            if (selected == null)
            {
                if (cmbSensorModel.Items.Count > 0) logger.Error("Could not work out what sensor was selected when updating baud rates");
            }
            else
            {
                string[] baudrates = new string[selected.BaudRates.Length + 1];
                baudrates[0] = "Autodetect";

                for (int i = 0; i < selected.BaudRates.Length; i++)
                {
                    baudrates[i + 1] = selected.BaudRates[i].ToString();
                }

                BindableStringArraySetting tmp = (BindableStringArraySetting)Settings.FetchSetting("connection/sensor/baudrate");
                tmp.Values = baudrates;
            }

            // do laser
            {
                string[] baudrates = new string[Laser.Baudrates.Length + 1];
                baudrates[0] = "Autodetect";

                for (int i = 0; i < Laser.Baudrates.Length; i++)
                {
                    baudrates[i + 1] = Laser.Baudrates[i].ToString();
                }

                BindableStringArraySetting tmp = (BindableStringArraySetting)Settings.FetchSetting("connection/laser/baudrate");
                tmp.Values = baudrates;
            }
        }

        private void updateAddresses()
        {
            string[] addresses = MotorControl.GetAddresses();

            BindableStringArraySetting tmp = (BindableStringArraySetting)Settings.FetchSetting("connection/motor/address");
            tmp.Values = addresses;

            // select IP addresses by default
            for (int i = 0; i < addresses.Length; i++)
            {
                if (!addresses[i].StartsWith("COM"))
                {
                    tmp.Value = addresses[i];
                    break;
                }
            }
        }

        private void updateSelectedAddressType()
        {
            ISensor selected = Sensor;
            if (selected != null)
            {
                SensorAddressType = selected.SensorAddressType;
            }
        }

        private SensorAddressTypes sensorAddressType;
        protected SensorAddressTypes SensorAddressType
        {
            get { return sensorAddressType; }
            set
            {
                sensorAddressType = value;
                if (sensorAddressType == SensorAddressTypes.IP_Address)
                {
                    txtSensorAddress.Visible = true;
                    cmbSensorPort.Visible = false;
                    lblSensorPort.Text = "IP Address";
                }
                else
                {
                    txtSensorAddress.Visible = false;
                    cmbSensorPort.Visible = true;
                    lblSensorPort.Text = "Port";
                }
            }
        }            
                
        #endregion

        #region Connection

        #region State Handling

        private ConnectionStates connectionState;
        public ConnectionStates ConnectionState
        {
            get { return connectionState; }
            set
            {
                connectionState = value;
                OnConnectionStateChanged(new ConnectionStateEventArgs(value));
            }
        }

        public class ConnectionStateEventArgs : EventArgs
        {
            private ConnectionStates connectionState;
            public ConnectionStates ConnectionState
            {
                get { return connectionState; }
                set { connectionState = value; }
            }

            public ConnectionStateEventArgs(ConnectionStates connectionState)
            {
                ConnectionState = connectionState;
            }
        }

        public event EventHandler<ConnectionStateEventArgs> ConnectionStateChanged;

        public enum ConnectionStates
        {
            Connected,
            Disconnected
        }
        
        protected void OnConnectionStateChanged(ConnectionStateEventArgs e)
        {
            if (ConnectionStateChanged != null)
            {
                ConnectionStateChanged(this, e);
            }
        }

        #endregion

        public void Connect()
        {
            string error;          
            if (ConnectionState == ConnectionStates.Connected)
            {
                bool success = TryDisconnect(out error);
                if (!success)
                {
                    logger.Error("Error disconnecting: " + error);
                    message("Error disconnecting: " + error);
                }
            }
            else
            {
                bool success = TryConnect(out error);
                if (!success)
                {
                    logger.Error("Error connecting: " + error);
                    message("Error connecting: " + error);
                }
            }
        }

        public bool TryConnect(out string error, bool fast=false)
        {
            error = "";
            DFM.Settings.Bindable.ThreadSafeControlUpdates.UpdateEnabled(btnConnect, false);

            SettingsGroup sensorSettings = (SettingsGroup)Settings.FetchSetting("connection/sensor");
            SettingsGroup laserSettings = (SettingsGroup)Settings.FetchSetting("connection/laser");
            SettingsGroup motorSettings = (SettingsGroup)Settings.FetchSetting("connection/motor");

            if (laserSettings.Fetch<bool>("enabled"))
            {
                Handshake handshake = Handshake.RequestToSend;
                if (laserSettings.Fetch<string>("handshake") == "None") handshake = Handshake.None;
                laser.HandshakeMethod = handshake;

                try
                {
                    logger.Trace("Connecting to laser");
                    if (laserSettings.Fetch<string>("baudrate") == "Autodetect")
                    {
                        logger.Trace("Autodetecting baudrate");
                        Laser.Connect(laserSettings.Fetch<string>("port"), fast);
                        ((SettingValue<string>)laserSettings.FetchSetting("baudrate")).Value = Laser.Baudrate.ToString();
                    }
                    else
                    {
                        int baudrate = Convert.ToInt32(laserSettings.Fetch<string>("baudrate"));
                        Laser.Connect(laserSettings.Fetch<string>("port"), baudrate);
                    }
                    logger.Trace("Connected to laser successfully");
                }
                catch (Exception ex)
                {
                    try { Laser.Disconnect(); }
                    catch { }

                    error = "Could not connect to laser: " + ex.Message;
                    logger.Info(error);

                    DFM.Settings.Bindable.ThreadSafeControlUpdates.UpdateEnabled(btnConnect, true);
                    return false;
                }
            }
            
            if (sensorSettings.Fetch<bool>("enabled"))
            {
                try
                {
                    string port;
                    if (SensorAddressType == SensorAddressTypes.COM_Port)
                        port = sensorSettings.Fetch<string>("port");
                    else
                        port = sensorSettings.Fetch<string>("ip_address");

                    logger.Trace("Connecting to sensor");
                    if (sensorSettings.Fetch<string>("baudrate") == "Autodetect")
                    {
                        logger.Trace("Autodetecting baudrate");
                        Sensor.Connect(port);
                        ((SettingValue<string>)sensorSettings.FetchSetting("baudrate")).Value = Sensor.BaudRate.ToString();
                    }
                    else
                    {
                        int baudrate = Convert.ToInt32(sensorSettings.Fetch<string>("baudrate"));
                        Sensor.Connect(port, baudrate);
                    }
                    logger.Trace("Connected to sensor successfully");
                }
                catch (Exception ex)
                {
                    try
                    {
                        if (laserSettings.Fetch<bool>("enabled")) Laser.Disconnect();
                        Sensor.Disconnect();
                    }
                    catch { }

                    error = "Could not connect to sensor: " + ex.Message;
                    logger.Info(error);

                    DFM.Settings.Bindable.ThreadSafeControlUpdates.UpdateEnabled(btnConnect, true);
                    return false;
                }

                Sensor.ReadingReference = ReadingReferences.SMR;
            }

            if (motorSettings.Fetch<bool>("enabled"))
            {
                try
                {
                    logger.Trace("Connecting to motor");
                    Motor.Connect(motorSettings.Fetch<string>("address"));

                    bool result = Motor.TestConnection();
                    if (!result) throw new ApplicationException("Could not connect to Galil");
                    logger.Trace("Connected to motor successfully");

                    logger.Trace("Downloading program");
                    string fname = Path.Combine(Path.GetDirectoryName(Application.ExecutablePath), "galil program.dmc");
                    if (!File.Exists(fname))
                    {
                        logger.Warn("Could not find program to download to Galil. Looking in: " + fname);
                        if (!Motor.TestProgram()) throw new ApplicationException("Could not find program on Galil or a file to download");
                    }

                    if (!Motor.DownloadProgramFile(fname)) throw new ApplicationException("Could not download program to Galil.");

                    logger.Trace("Downloaded program");

                    if (!Motor.TestProgram()) throw new ApplicationException("Downloaded program appears to be invalid: TestProgram failed");

                }
                catch (Exception ex)
                {
                    try
                    {
                        if (laserSettings.Fetch<bool>("enabled")) Laser.Disconnect();
                        if (sensorSettings.Fetch<bool>("enabled")) Sensor.Disconnect();
                        Motor.Disconnect();
                    }
                    catch { }

                    error = "Could not connect to motor: " + ex.Message;
                    logger.Info(error);

                    DFM.Settings.Bindable.ThreadSafeControlUpdates.UpdateEnabled(btnConnect, true);
                    return false;
                }
            }

            DFM.Settings.Bindable.ThreadSafeControlUpdates.UpdateEnabled(btnConnect, true);
            ConnectionState = ConnectionStates.Connected;
            return true;
        }

        public bool TryDisconnect(out string error)
        {
            error = "";

            try
            {
                if (Settings.Fetch<bool>("connection/sensor/enabled")) Sensor.Disconnect();
                if (Settings.Fetch<bool>("connection/laser/enabled")) Laser.Disconnect();
                if (Settings.Fetch<bool>("connection/motor/enabled")) Motor.Disconnect();
            }
            catch (Exception ex)
            {
                error = ex.Message;
                return false;
            }

            ConnectionState = ConnectionStates.Disconnected;
            return true;
        }

        #endregion

        #region DFM Event Handlers

        void btnChooseDataLocation_Click(object sender, EventArgs e)
        {
            SettingValue<string> folder = (SettingValue<string>)Settings.FetchSetting("data/saving/location");
            
            if (Directory.Exists(folder.Value)) folderBrowser.SelectedPath = folder.Value;
            folderBrowser.ShowDialog();

            if (folderBrowser.SelectedPath != "")
            {
                folder.Value = folderBrowser.SelectedPath;
            }
        }

        void btnConnect_Click(object sender, EventArgs e)
        {
            Connect();
        }

        void cmbSensorModel_SelectedIndexChanged(object sender, EventArgs e)
        {
            updateSelectedAddressType();
            updateSensorTypes();
            updateBaudrates();
        }

        void btnRefreshCOMPorts_Click(object sender, EventArgs e)
        {
            updateComPorts();
            updateAddresses();
        }

        void btnChangePassword_Click(object sender, EventArgs e)
        {
            PasswordChangeForm form = new PasswordChangeForm();
            form.ShowDialog();

            if (form.DialogResult == System.Windows.Forms.DialogResult.OK)
            {
                SettingValue<string> setting = (SettingValue<string>)Settings.FetchSetting("misc/password");
                setting.Value = form.Password;
            }
        }        

        #endregion

        #region Devices

        private delegate ISensor SensorCallback();

        public ISensor Sensor
        {
            get
            {
                if (cmbSensorModel.InvokeRequired)
                {
                    return (ISensor)cmbSensorModel.Invoke(new SensorCallback(delegate()
                    {
                        return Sensor;
                    }));
                }
                else
                {
                    if (cmbSensorModel.SelectedIndex == -1) return null;
                    return sensors[cmbSensorModel.SelectedIndex];
                }
            }
        }

        public ILaser Laser
        {
            get { return laser; }
        }

        public IMotorControl Motor
        {
            get { return motor; }
        }

        #endregion        

        #endregion
    }

    #region DFM Enums

    /// <summary>
    /// How to specify how many pits to create for the DFM
    /// </summary>
    public enum LaserDurationSpecification
    {
        PulseCount,
        RunTime
    }

    public enum DataUnits
    {
        Metric,
        Imperial
    }

    #endregion
}
