﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using NLog;
using DFM.Settings;
using System.Threading;
using SensorControl;
using SensorControl.Sensors;
using DFM.Scans;
using System.IO;
using Analysis;
using ZedGraph;
using PaintAnalysis.Common;
using DFM.Results;
using DFM.Settings.Bindable;
using System.Timers;
using SensorTester;
using System.Data.SQLite;


namespace DFM
{
    public partial class DFM_Form_simple : Form
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        
        #region Data Members

        // auxiliary forms
        DFM_Simple_Settings_Form settingsForm;
        ScanBrowserForm scanBrowserForm;
        
        // workers
        BackgroundWorker connectWorker;
        BackgroundWorker scanWorker;

        // helpers
        SegmentBuffer segmentBuffer;
        
        #endregion


         #region New Variables
                //my variables
                int flag = 0, lag = 0, CheckCount = 0,d1=0,FlagSelectAll=0;
                long[] scanId = new long[3];
                long Length, Inserted = 0, CurrentScanId, db = 0, readingCount;
                double[] B, C, D,Dnext, ymaxavg,yminavg, thickness,stitchThickness,dataFilter_x,dataFilter_y,Converted_x;
                long[] ymaxIndexavg, yminIndexavg, Algo3ThicknessIndex,DelId;
                long BlockSize, YmaxBlock, YminBlock, PointsToAvg, DiffBwAvgs, AvgPitPoint;
                long YmaxTmpLoc, minav, maxav, YminAvgPoint, YminPosFactor, YmaxAvgPoint,TotalSampleLength,Left1,Left2Mid1,Mid2Right1,Right2;
                double FinalThickness, DifferThreshold, Algo3Threshold, filterDiff, height, HeightValue,sensorFrequency,LineSpeed,IgnoreEdge;
                string FileNameRefresh = "",AlgoType="";
                bool filterCheck,LeftMidRightCheck,TimerCheck,AutoCheck;
                double[] resultReading;
                
                // for paint Type varaibles
                string PaintName,Algorithm;
                long NoPointAvg,NoPointBwAvg,DistanceBwPits,Percentage, AveragePoints,Range, PointsInEachPit,MinPitWidth, SmoothingWidth,AveragingNum,NoPointsForYmaxAvg;
                int NoCountBackPts;                   
                double ThresholValue=0.652,Factor,SubstrateRoughness=0.000,CompressionFactor,OffsetValue=0.0,RaAvg =0.0,RpAvg=0.0;
                bool HeightsOnly4First,HeightEnable;

                Thread laserThread = null;
                

         #endregion

                AlgorithmSelect algoSelect = new AlgorithmSelect();
                DataTable dataTable = new DataTable();
                DataBaseForm dbfrm = new DataBaseForm();
                PitDisplayForm PitDispForm = new PitDisplayForm();
                showPitsValue pitsVal = new showPitsValue();

                public void Screen()
                {
                    Application.Run(new SplashScreen());

                }

        public DFM_Form_simple()
        {
            Thread t = new Thread(new ThreadStart(Screen));
            t.Start();
            Thread.Sleep(5000);

            InitializeComponent();
            t.Abort();

            

            //code by shubhendu
           // btnBrowse.Enabled = true;
           // btnClear.Enabled = false;

            

            try
            {
                // line for readability
                logger.Trace("************************************************************************************************************************");

                // create helper stuff
                settingsForm = new DFM_Simple_Settings_Form();
                settingsForm.OpeningForm = this;

                scanBrowserForm = new ScanBrowserForm();

                RunningState = RunningStates.Stopped;
                LaserInited = false;
                NewLaserInited = false;
                
                connectWorker = new BackgroundWorker();
                connectWorker.WorkerReportsProgress = false;
                connectWorker.WorkerSupportsCancellation = true;
                connectWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(connectWorker_RunWorkerCompleted);
                connectWorker.DoWork += new DoWorkEventHandler(connectWorker_DoWork);

                scanWorker = new BackgroundWorker();
                scanWorker.WorkerReportsProgress = true;
                scanWorker.WorkerSupportsCancellation = true;
                scanWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(scanWorker_RunWorkerCompleted);
                scanWorker.ProgressChanged += new ProgressChangedEventHandler(scanWorker_ProgressChanged);
                scanWorker.DoWork += new DoWorkEventHandler(scanWorker_DoWork);

                segmentBuffer = new SegmentBuffer(1);

                algoSelect.Load += new EventHandler(AlgoSelect_Load);

                algoSelect.rdbtnAlgo1.CheckedChanged += new EventHandler(rdAlgo1_checked);
                algoSelect.rdbtnAlgo2.CheckedChanged += new EventHandler(rdAlgo2_checked);
                algoSelect.rdbtnAlgo3.CheckedChanged += new EventHandler(rdAlgo3_checked);
                algoSelect.rdbtnAlgo4.CheckedChanged += new EventHandler(rdAlgo4_checked);

                rdNormalBtn.CheckedChanged += new EventHandler(rdNormal_checked);
                rdStitchBtn.CheckedChanged += new EventHandler(rdStitch_checked);
                rdAverageBtn.CheckedChanged += new EventHandler(rdAverage_checked);

                algoSelect.btnRefreshReading.Click += new EventHandler(btnRefresh_Click);

                // click event for checkbox
                dbfrm.ScanDataGrid.CellContentClick += new DataGridViewCellEventHandler(GridCheck);
                dbfrm.Shown += new EventHandler(databaseFrm_Form_Load);
                dbfrm.btnExportDB.Click += new EventHandler(btnExprt_click);
                dbfrm.btnbrowseDB.Click += new EventHandler(btnChooseDb_Click);
                dbfrm.btnApply.Click += new EventHandler(btnApply_Click);
                dbfrm.chkSelectAll.CheckedChanged += new EventHandler(SelectAll_checked);
                dbfrm.btnDeleteAll.Click += new EventHandler(btnDeleteAll_Click);

                //for pit display form
                PitDispForm.btnExportDB.Click += new EventHandler(btnPitExprt_click);
                PitDispForm.btnbrowseDB.Click += new EventHandler(btnPitChooseDb_Click);

                // top graph
                GraphPane pane = zgcScanProfile.GraphPane;
                pane.Legend.IsVisible = false;
                // set axis labels
                pane.Title.Text = "Scan Profile";
                pane.XAxis.Title.Text = "Time (s)";
                pane.YAxis.Title.Text = "Distance (microns)";
                zgcScanProfile.Click += new EventHandler(delegate(object sender, EventArgs e)
                {
                    new GraphDisplay(zgcScanProfile, "Scan Profile");
                });

                // bottom graph
                pane = zgcPits.GraphPane;
                pane.Legend.IsVisible = false;
                // set axis labels
                pane.Title.Text = "Scan Pits";
                pane.XAxis.Title.Text = "Time (s)";
                pane.YAxis.Title.Text = "Pit Height (microns)";
                zgcPits.Click += new EventHandler(delegate(object sender, EventArgs e)
                {
                    new GraphDisplay(zgcPits, "Scan Pits");
                });

                // unit converter utilities
                micronConverter = UnitConverter.GetDistanceConverter("micron", "micron");

                // clear displays
                clearReadings();

                // setup enabling behaviour
                settingsForm.ConnectionStateChanged += new EventHandler<DFM_Simple_Settings_Form.ConnectionStateEventArgs>(delegate(object sender, DFM_Simple_Settings_Form.ConnectionStateEventArgs e)
                {
                    if (e.ConnectionState == DFM_Simple_Settings_Form.ConnectionStates.Connected) Settings.Bindable.ThreadSafeControlUpdates.UpdateText<string>(btnConnect, "Disconnect");
                    else Settings.Bindable.ThreadSafeControlUpdates.UpdateText<string>(btnConnect, "Connect");
                    
                    bool isConnected = e.ConnectionState == DFM_Simple_Settings_Form.ConnectionStates.Connected;
                    bool isRunning = (RunningState != RunningStates.Stopped);
                    Settings.Bindable.ThreadSafeControlUpdates.UpdateEnabled(btnRun, isConnected);           
                });

                DFMStateChanged += new EventHandler<DFMStateChangedEventArgs>(delegate(object sender, DFMStateChangedEventArgs e)
                {
                    handleStateUpdate();
                });
                OnDFMStateChanged(new DFMStateChangedEventArgs(RunningState, EdgeState, PaintingState));

                btnConnect.Click += new EventHandler(btnConnect_Click);
                btnSettings.Click += new EventHandler(btnSettings_Click);
                btnRun.Click += new EventHandler(btnRun_Click);
                btnClear.Click += new EventHandler(btnClear_Click);
                //btnPitValues.Click += new EventHandler(btnPitValues_Click);
                btnBrowse.Click += new EventHandler(btnBrowse_Click);
                scanBrowserForm.DisplayedScanChanged += new ScanBrowserForm.DisplayedScanChangedHandler(LoadScan);
                
                // change scan averaging number
                ((SettingValue<int>)settingsForm.Settings.FetchSetting("data/scan averaging number")).ValueChanged += new EventHandler<ValueChangedEventArgs<int>>(delegate(object sender, ValueChangedEventArgs<int> e)
                    {
                        // update graphs
                        if (lastAnalysis != null)
                        {
                            //Inserted = 1;
                            LoadScan(lastAnalysis);
                            
                           // InsertToDb();
                        }
                    });
                
                // re-init laser when settings change
                ((SettingsGroup)settingsForm.Settings.FetchSetting("scan/laser")).ModifiedChanged += new EventHandler(delegate(object sender, EventArgs e)
                    {
                        LaserInited = false;
                    });
                // re-init laser when settings change
               /* ((SettingsGroup)settingsForm.Settings.FetchSetting("scan/new laser")).ModifiedChanged += new EventHandler(delegate(object sender, EventArgs e)
                {
                    NewLaserInited = false;
                });*/

                // update max segments
                ((SettingValue<int>)settingsForm.Settings.FetchSetting("data/segments/max segments")).ValueChanged += new EventHandler<ValueChangedEventArgs<int>>(delegate(object sender, ValueChangedEventArgs<int> e)
                {
                    segmentBuffer.MaxSize = e.Value;
                });
                segmentBuffer.MaxSize = settingsForm.Settings.Fetch<int>("data/segments/max segments");

                // handle units changing
                SettingValue<DataUnits> tmp = (SettingValue<DataUnits>)settingsForm.Settings.FetchSetting("data/units");
                tmp.ValueChanged += new EventHandler<ValueChangedEventArgs<DataUnits>>(handleUnitsChanged);
                handleUnitsChanged(null, new ValueChangedEventArgs<DataUnits>(tmp.Value, tmp));

                // set it to always painting
                PaintingState = PaintingStates.Painting;

                // try connect
                btnConnect.Enabled = false;
                connectWorker.RunWorkerAsync(new ConnectBehaviour(true, true));

                // bind multiple segments checkbox - save if settings form is closed
               /* ((BindableBoolSetting)(settingsForm.Settings.FetchSetting("data/segments/multiple segments"))).Bind(chkMultipleSegmentScan);
                chkMultipleSegmentScan.CheckedChanged += new EventHandler(delegate(object sender, EventArgs e)
                {
                    if (settingsForm.Visible == false) settingsForm.Save();
                });
                */

                // misc settings
                FormClosing += new FormClosingEventHandler(delegate(object sender, FormClosingEventArgs e)
                {
                    Properties.Settings.Default.Save();

                    //alok
                    //calling the function to generate csv before closing a form
                    pitsVal.export_data_csv();
                });
            }
            catch (Exception ex)
            {
                logger.ErrorException(ex.Message, ex);
                message("Error in startup: " + ex.Message);
            }


            // Display available Paint Type in the List
            RefreshPaintList();
            cmbPaintType.SelectedIndex = 0;

            this.Opacity = 0;
            this.Show();
            this.tmrFade.Enabled = true;

            // aligning text of richtextbox to centere
            rtxtboxStatus.SelectionAlignment = HorizontalAlignment.Center;
            
        }

        private void handleStateUpdate()
        {
            bool isRunning = (RunningState != RunningStates.Stopped);
            bool isConnected = settingsForm.ConnectionState == DFM_Simple_Settings_Form.ConnectionStates.Connected;
           // bool empty = dispLeftStats.Text == "-";
            bool isSegmentBufferFull = segmentBuffer.Count == 0 || segmentBuffer.IsFull || !settingsForm.Settings.Fetch<bool>("data/segments/multiple segments");

           if (isRunning) Settings.Bindable.ThreadSafeControlUpdates.UpdateText<string>(btnRun, "Stop");
           else Settings.Bindable.ThreadSafeControlUpdates.UpdateText<string>(btnRun, "Measure");
           
            /*
            if (!isSegmentBufferFull && !empty)
            {
                Settings.Bindable.ThreadSafeControlUpdates.UpdateText<string>(btnClear, "Finish");
                Settings.Bindable.ThreadSafeControlUpdates.UpdateText<string>(lblSegmentScanStatus, String.Format("{0} / {1}", segmentBuffer.Count, settingsForm.Settings.Fetch<int>("data/segments/max segments")));
            }
            else
            {
                Settings.Bindable.ThreadSafeControlUpdates.UpdateText<string>(btnClear, "Clear");
                Settings.Bindable.ThreadSafeControlUpdates.UpdateText<string>(lblSegmentScanStatus, "");
            }
            */
            Settings.Bindable.ThreadSafeControlUpdates.UpdateEnabled(btnRun, isConnected);
            Settings.Bindable.ThreadSafeControlUpdates.UpdateEnabled(btnConnect, !isRunning && isSegmentBufferFull);
            //Settings.Bindable.ThreadSafeControlUpdates.UpdateEnabled(btnClear, !isRunning && !empty);
            //Settings.Bindable.ThreadSafeControlUpdates.UpdateEnabled(btnPitValues, !isRunning && !empty);
            settingsForm.IsEnabled = !isRunning && isSegmentBufferFull;
        }

        #region Misc

        private void message(string message)
        {
            MessageBox.Show(message);
        }

        #endregion

        #region Units

        private DataUnits CurrentUnits
        {
            get;
            set;
        }
        private Converter<double, double> micronConverter;

        void handleUnitsChanged(object sender, ValueChangedEventArgs<DataUnits> e)
        {
            // things to update:
            // top graph, bottom graph, boxes
            // not: pit values - done on creation only

            // change saved units
            DataUnits oldUnits = CurrentUnits;
            CurrentUnits = e.Value;

            if (CurrentUnits == DataUnits.Metric)
                micronConverter = UnitConverter.GetDistanceConverter("micron", "micron");
            else
                micronConverter = UnitConverter.GetDistanceConverter("micron", "mil");

            // change pit values
            //updatePitDisplays();

            // change 2 graphs            
            // top graph
            GraphPane scanPane = zgcScanProfile.GraphPane;
            GraphPane pitPane = zgcPits.GraphPane;

            if (CurrentUnits == DataUnits.Metric)
            {
                scanPane.YAxis.Title.Text = "Distance (microns)";
                pitPane.YAxis.Title.Text = "Pit Height (microns)";
                lblMode.Text = "Mode: Metric";
            }
            else
            {
                scanPane.YAxis.Title.Text = "Distance (mils)";
                pitPane.YAxis.Title.Text = "Pit Height (mils)";
                lblMode.Text = "Mode: Imperial";
            }
            
            // process any old data on it - know that there is at most 1 scan displayed

            // work out unit names to pass to converter
            string oldUnitName, curUnitName;
            oldUnitName = "micron";
            curUnitName = "micron";
            if (oldUnits == DataUnits.Imperial) oldUnitName = "mil";
            if (CurrentUnits == DataUnits.Imperial) curUnitName = "mil";

            if (scanPane.CurveList.Count > 0)
            {
                IPointList cur = scanPane.CurveList[0].Points;
                int n = cur.Count;
                double[] data_x = new double[n];
                double[] data_y = new double[n];
                for (int i = 0; i < n; i++)
                {
                    data_x[i] = cur[i].X;
                    data_y[i] = UnitConverter.ConvertDistance(oldUnitName, curUnitName, cur[i].Y);
                }

                scanPane.CurveList.Clear();
                PointPairList points = new PointPairList(data_x, data_y);
                scanPane.AddCurve("scan", points, Color.Blue, SymbolType.None);
                zgcScanProfile.AxisChange();
                zgcScanProfile.Invalidate();
            }

            if (pitPane.CurveList.Count > 0)
            {
                IPointList cur = pitPane.CurveList[0].Points;
                int n = cur.Count;
                double[] data_x = new double[n];
                double[] data_y = new double[n];
                for (int i = 0; i < n; i++)
                {
                    data_x[i] = cur[i].X;
                    data_y[i] = UnitConverter.ConvertDistance(oldUnitName, curUnitName, cur[i].Y);
                }

                pitPane.CurveList.Clear();
                PointPairList points = new PointPairList(data_x, data_y);
                pitPane.AddCurve("pits", points, Color.Purple);
                zgcPits.AxisChange();
                zgcPits.Invalidate();
            }
        }

        #endregion

        #region Button State Handling

        public event EventHandler<DFMStateChangedEventArgs> DFMStateChanged;

        private RunningStates runningState;
        public RunningStates RunningState
        {
            get { return runningState; }
            set
            {
                runningState = value;
                OnDFMStateChanged(new DFMStateChangedEventArgs(value, EdgeState, PaintingState));
            }
        }

        private EdgeStates edgeState;
        public EdgeStates EdgeState
        {
            get { return edgeState; }
            set
            {
                edgeState = value;                
                OnDFMStateChanged(new DFMStateChangedEventArgs(RunningState, value, PaintingState));
            }
        }

        private PaintingStates paintingState;
        public PaintingStates PaintingState
        {
            get { return paintingState; }
            set
            {
                paintingState = value;
                OnDFMStateChanged(new DFMStateChangedEventArgs(RunningState, EdgeState, value));
            }
        }

        protected void OnDFMStateChanged(DFMStateChangedEventArgs e)
        {
            if (DFMStateChanged != null)
            {
                DFMStateChanged(this, e);
            }
        }

        #endregion

        #region Events

        void btnRun_Click(object sender, EventArgs e)
        {
           /* bool response;
            response = settingsForm.NewLaser.Run("PD," + 10);
            if (!response)
            {
                message("Could not set duty cycle");
                logger.Error("Could not set duty cycle");
                RunningState = RunningStates.Stopped;
               // return false;
            }

            response = settingsForm.NewLaser.Run("PN," + 2);
            if (!response)
            {
                message("Could not set num of cycle");
                logger.Error("Could not set num of cycle");
                RunningState = RunningStates.Stopped;
               // return false;
            }
            response = settingsForm.NewLaser.Run("PB," + 10);
            if (!response)
            {
                message("Could not num of burst");
                logger.Error("Could not set num of burst");
                RunningState = RunningStates.Stopped;
               // return false;
            }*/
            
            // lag time
            lag = settingsForm.Settings.Fetch<int>("misc/lag time");
            
            lagTimeTimer.Interval = 1000;
            lagTimeTimer.Start();


            
        }

        private void lagTimeFinish_RunStart()
        {
            if (RunningState == RunningStates.Stopping)
            {
                message("Already stopping");
                return;
            }
            if (RunningState == RunningStates.Stopped)
                runScan();
            else
            {
                RunningState = RunningStates.Stopping;
                btnRun.Enabled = false; // todo
                try
                {
                    bool a1 = killWorker(scanWorker);
                    bool a2 = killWorker(connectWorker);
                    if (!a1) logger.Error("Could not kill scanworker1");
                    if (!a2) logger.Error("Could not kill connectWorker");
                }
                catch (Exception ex)
                {
                    logger.ErrorException("Error stopping scan: " + ex.Message, ex);
                }
                RunningState = RunningStates.Stopped;
            }
        
        }

        void btnBrowse_Click(object sender, EventArgs e)
        {
            scanBrowserForm.Opacity = 0;
            scanBrowserForm.Show();
            scanBrowserForm.tmrFade.Enabled = true;
            
           // scanBrowserForm.Show();
           // btnBrowse.Enabled = false;//code by shubhendu
        }

        

        

        bool killWorker(BackgroundWorker worker)
        {
            worker.CancelAsync();                
            int i = 1;
            while (worker.IsBusy && i <= 5)
            {
                Thread.Sleep(10 * i * i);
                i += 1;
            }
            return !worker.IsBusy;
        }

        void btnSettings_Click(object sender, EventArgs e)
        {
            settingsForm.Opacity = 0;
            //settingsForm.Show();
            
            
            settingsForm.FormClosing += new FormClosingEventHandler(settingsForm_closing);
            
            // check password
            string password = settingsForm.Settings.Fetch<string>("misc/password");

            bool valid = true;
            if (password != "")
            {
                valid = false;
                PasswordPromptForm pass = new PasswordPromptForm();
                pass.ShowDialog();
                if (pass.DialogResult == System.Windows.Forms.DialogResult.OK && pass.Password == password)
                    valid = true;
                else
                    message("Invalid password");
            }
            if (valid)
            {
                settingsForm.Show();
                settingsForm.WindowState = FormWindowState.Normal;
                settingsForm.Focus();
            }

            settingsForm.tmrFade.Enabled = true;
        }

        void settingsForm_closing(object sender, FormClosingEventArgs e)
        {
            //ScanDataGrid.DataSource = null;
            //LoadData();
           // MessageBox.Show("setting closed");
            
            // for avoiding dropdown change event
            Inserted = 0;
            int paintIndex = cmbPaintType.SelectedIndex;
            RefreshPaintList();
            cmbPaintType.SelectedIndex = paintIndex;
            Inserted = 1;
            //MessageBox.Show("closed");
        }

        void btnConnect_Click(object sender, EventArgs e)
        {
            if (!connectWorker.IsBusy)
            {
                btnConnect.Enabled = false;            
                connectWorker.RunWorkerAsync(new ConnectBehaviour(false, false));
            }
        }

        void btnClear_Click(object sender, EventArgs e)
        {
            // either Finish or Clear
            bool isMultiScanMode = settingsForm.Settings.Fetch<bool>("data/segments/multiple segments");
            if (!segmentBuffer.IsFull && isMultiScanMode && segmentBuffer.Count != 0)
                handleFinishMultipleSegmentScan();
            else
                clearReadings();
        }
        private void clearReadings()
        {
            // clear graphs
            zgcPits.GraphPane.CurveList.Clear();
            zgcPits.Invalidate();
            zgcScanProfile.GraphPane.CurveList.Clear();
            zgcScanProfile.Invalidate();

            txtPits.Text = "";
            //capDispMax.Text = "";

            // clear displays
            for (int i = 0; i < 3; i++)
            {
                pitAverages[i] = -1.0;
                pitStDevs[i] = -1.0;
                pitCounts[i] = -1;
            }
            updatePitDisplays();

            // clear last scan
            //lastAnalysis = null;

            btnClear.Enabled = false;
           // btnPitValues.Enabled = false;
        }

        void btnPitValues_Click(object sender, EventArgs e)
        {
            if (lastAnalysis == null)
            {
                message("No pits have been loaded");
                return;
            }
            // pass unit converted values
            new PitDisplayForm(Array.ConvertAll(lastAnalysis.Data.Peaks, micronConverter), "Pits");
        }

        #endregion
        
        #region Connect Worker

        void connectWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            ConnectBehaviour args;
            if (e.Argument != null) args = (ConnectBehaviour)e.Argument;
            else args = new ConnectBehaviour(false, false);

            string error;
            if (settingsForm.ConnectionState == DFM_Simple_Settings_Form.ConnectionStates.Connected)
            {
                if (!settingsForm.TryDisconnect(out error) && !args.Silent)
                    throw new ApplicationException(error);
            }
            else
            {
                if (!settingsForm.TryConnect(out error, args.Fast) && !args.Silent) 
                    throw new ApplicationException(error);
            }            
             
        }

        void connectWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Error != null) message(e.Error.Message);
            btnConnect.Enabled = true;
        }

        private struct ConnectBehaviour
        {
            private bool silent;
            public bool Silent
            {
                get { return silent; }
                set { silent = value; }
            }

            private bool fast;
            public bool Fast
            {
                get { return fast; }
                set { fast = value; }
            }

            public ConnectBehaviour(bool silent, bool fast)
            {
                this.silent = silent;
                this.fast = fast;
            }
        }

        #endregion
        
        #region Scans

        #region Scan Control
        private void runScan()
        {

            NewLaserInited = false;

            if (settingsForm.Settings.IsModified)
            {
                message("Save any settings changes before running a scan");
                RunningState = RunningStates.Stopped;
                return;
            }

            // validate
            if (!settingsForm.Settings.IsValid)
            {
                message(settingsForm.Settings.ValidationErrors[0].ErrorMessage);
                RunningState = RunningStates.Stopped;
                return;
            }
                        
            if (!(settingsForm.Settings.Fetch<bool>("connection/sensor/enabled") || settingsForm.Settings.Fetch<bool>("connection/laser/enabled")))
            {
                message("Enable at least one of the sensor or laser to take a scan");
                RunningState = RunningStates.Stopped;
                return;
            }

            logger.Trace("Preparing scan");
            RunningState = RunningStates.Preparing;
            
            // clear previous data
            clearReadings();
                        
            // set up laser - if first time or last was a quick scan, and if laser enabled
            logger.Trace("Preparing laser");
            logger.Trace("NewLaserInited: "+NewLaserInited);
            logger.Trace("New laser Enabled: "+settingsForm.Settings.Fetch<bool>("connection/new laser/enabled"));

            if (!LaserInited && settingsForm.Settings.Fetch<bool>("connection/laser/enabled"))
            {
                try
                {
                    if (!initLaser())
                    {
                        RunningState = RunningStates.Stopped;
                        return;
                    }
                    LaserInited = true;
                }
                catch (Exception ex)
                {
                    message("Could not set laser parameters: " + ex.Message);
                    logger.Error("Unhandled error in runWithoutEdgeScan when setting up laser: " + ex.Message);
                    RunningState = RunningStates.Stopped;
                    return;
                }
            }
           
            //because of new laser inroduced
            else if (!NewLaserInited && settingsForm.Settings.Fetch<bool>("connection/new laser/enabled"))
            {
                try
                {
                    if (!initNewLaser())
                    {
                        RunningState = RunningStates.Stopped;
                        return;
                    }
                    NewLaserInited = true;
                }
                catch (Exception ex)
                {
                    message("Could not set New laser parameters: " + ex.Message);
                    logger.Error("Unhandled error in runWithoutEdgeScan when setting up new laser: " + ex.Message);
                    RunningState = RunningStates.Stopped;
                    return;
                }
            }
            else { logger.Trace("Skipping both laser"); }

            // set up sensor to take readings
            logger.Trace("Preparing sensor");
            if (settingsForm.Settings.Fetch<bool>("connection/sensor/enabled"))
            {
                try
                {
                    // get frequency
                    double frequency = Convert.ToDouble(settingsForm.Sensor.Parameters["frequency"].Get());

                    settingsForm.Sensor.TimeIncrement = 1.0 / frequency;
                    // only reset time on fresh scan
                    if (!settingsForm.Settings.Fetch<bool>("data/segments/multiple segments") || segmentBuffer.Count == 0)
                        settingsForm.Sensor.CurrentTime = 0;

                    settingsForm.Sensor.EnsureReadingsInitialised();
                    settingsForm.Sensor.PortBuffer.Clear();
                }
                catch (Exception ex)
                {
                    message("Could not prepare sensor: " + ex.Message);
                    logger.Error("Unhandled error in runScan when setting up sensor: " + ex.Message);
                    RunningState = RunningStates.Stopped;
                    return;
                }
            }
            else { logger.Trace("Skipping sensor"); }
                        
            if (RunningState != RunningStates.Stopped && RunningState != RunningStates.Stopping)
            {
                // run scans
                scanWorker.RunWorkerAsync();
            }
            else logger.Trace("Not starting worker at end of runScan because running state is " + RunningState.ToString());
        }

        #region New laser variables

        private bool initNewLaser()
        {
          //  LaserDurationSpecification laserDuration = settingsForm.Settings.Fetch<LaserDurationSpecification>("scan/laser/duration specification");
            int startPosition = settingsForm.Settings.Fetch<int>("connection/new laser/start position");
            int endPosition = settingsForm.Settings.Fetch<int>("connection/new laser/end position");
            int accelerateRate = settingsForm.Settings.Fetch<int>("connection/new laser/accelerate rate");
            int steadyVelocity = settingsForm.Settings.Fetch<int>("connection/new laser/steady velocity");
            
            //variable to check response
             bool response;

            //variable to set the value of the prameter
            //start position range 1-160000  S
            //end position range 1-160000   E
            //acceleration/deceleration rate 1-10   A
            //steady state velocity 1-10   V

            //setting S
            string value = Math.Max(1, Math.Min(startPosition, 160000)).ToString().PadLeft(6, '0');
            logger.Trace("Setting start Position to: " + value);
            response = settingsForm.NewLaser.Run("PS,"+value);
            if (!response)
            {
                message("Could not set start position");
                logger.Error("Could not set start position");
                RunningState = RunningStates.Stopped;
                return false;
            }

            //setting E
            value = Math.Max(1, Math.Min(endPosition, 160000)).ToString().PadLeft(6, '0');
            logger.Trace("Setting end Position to: " + value);
            response = settingsForm.NewLaser.Run("PE,"+value);
            if (!response)
            {
                message("Could not set end position");
                logger.Error("Could not set end position");
                RunningState = RunningStates.Stopped;
                return false;
            }

            //setting A
            value = Math.Max(10, Math.Min(accelerateRate,200)).ToString().PadLeft(2, '0');
            logger.Trace("Setting accelerate rate to: " + value);
            response = settingsForm.NewLaser.Run("PA,"+value);
            if (!response)
            {
                message("Could not set accelerate rate");
                logger.Error("Could not set accelerate rate");
                RunningState = RunningStates.Stopped;
                return false;
            }

            //setting V
            value = Math.Max(10, Math.Min(steadyVelocity, 200)).ToString().PadLeft(2, '0');
            logger.Trace("Setting steady velocity to: " + value);
            response = settingsForm.NewLaser.Run("PV,"+value);
            if (!response)
            {
                message("Could not set steady velocity");
                logger.Error("Could not set steady velocity");
                RunningState = RunningStates.Stopped;
                return false;
            }

            //commands for laser output written in this function
            return initNewLaser2();
        }

        private bool initNewLaser2()
        {
            //  LaserDurationSpecification laserDuration = settingsForm.Settings.Fetch<LaserDurationSpecification>("scan/laser/duration specification");
            int dutyCycle = settingsForm.Settings.Fetch<int>("connection/new laser/duty cycle");
            int numOfCyle = settingsForm.Settings.Fetch<int>("connection/new laser/num of cycle");
            int numOfburst = settingsForm.Settings.Fetch<int>("connection/new laser/num of burst");
            int totalPulses = settingsForm.Settings.Fetch<int>("connection/new laser/total pulses");

            //variable to check response
            bool response;

            //variable to set the value of the prameter
            //duty cycle range 5-95  D
            //Number of cycles range 1-160000   N
            //Number of burst per laser cycle range 1-100   B
            //total pulses in a laser cycle 1-100   T

            //setting D
            string value = Math.Max(5, Math.Min(dutyCycle, 95)).ToString().PadLeft(2, '0');
            logger.Trace("Setting duty cycle to: " + value);
            response = settingsForm.NewLaser.Run("PD,"+value);
            if (!response)
            {
                message("Could not set duty cycle");
                logger.Error("Could not set duty cycle");
                RunningState = RunningStates.Stopped;
                return false;
            }

            //setting N
            value = Math.Max(1, Math.Min(numOfCyle, 160000)).ToString().PadLeft(6, '0');
            logger.Trace("Setting num of cycle to: " + value);
            response = settingsForm.NewLaser.Run("PN,"+value);
            if (!response)
            {
                message("Could not set num of cycle");
                logger.Error("Could not set num of cycle");
                RunningState = RunningStates.Stopped;
                return false;
            }

            //setting B
            value = Math.Max(1, Math.Min(numOfburst, 100)).ToString().PadLeft(3, '0');
            logger.Trace("Setting num of Burst to: " + value);
            response = settingsForm.NewLaser.Run("PB,"+value);
            if (!response)
            {
                message("Could not num of burst");
                logger.Error("Could not set num of burst");
                RunningState = RunningStates.Stopped;
                return false;
            }

            //setting T
            value = Math.Max(1, Math.Min(totalPulses, 100)).ToString().PadLeft(3, '0');
            logger.Trace("Setting total pulses to: " + value);
            response = settingsForm.NewLaser.Run("PT,"+value);
            if (!response)
            {
                message("Could not set total pulses");
                logger.Error("Could not set total pulses");
                RunningState = RunningStates.Stopped;
                return false;
            }

            

            return true;
        }

        #endregion


        private bool initLaser()
        {
            LaserDurationSpecification laserDuration = settingsForm.Settings.Fetch<LaserDurationSpecification>("scan/laser/duration specification");
            int pulseCount = settingsForm.Settings.Fetch<int>("scan/laser/pulse count");
            int runTime = settingsForm.Settings.Fetch<int>("scan/laser/run time");
            int gatingFrequency = settingsForm.Settings.Fetch<int>("scan/laser/gating frequency");
            float dutyCycle1 = settingsForm.Settings.Fetch<float>("scan/laser/duty cycle");
            int dutyCycle = (int)(Math.Round(10 * dutyCycle1));

            // run-time - set in ms, 1-32766
            // frequency - set in Hz, 1-20000
            // duty cycle - set in %, 1-1000 => 0.1%-100.0%

            // work out run time
            if (laserDuration == LaserDurationSpecification.PulseCount)
            {
                runTime = (int)Math.Round(1000.0 / gatingFrequency * pulseCount);
            }

            return initLaser2(runTime, gatingFrequency, dutyCycle);
        }

        private bool initLaser2(int runTime, int gatingFrequency, int dutyCycle)
        {
            bool response;

            string value = Math.Max(1, Math.Min(runTime, 32766)).ToString().PadLeft(5, '0');
            logger.Trace("Setting run time to: " + value);
            response = settingsForm.Laser.Run("R", value);
            if (!response)
            {
                message("Could not set run time/pulse count");
                logger.Error("Could not set run time/pulse count");
                RunningState = RunningStates.Stopped;
                return false;
            }

            value = Math.Max(1, Math.Min(gatingFrequency, 20000)).ToString().PadLeft(5, '0');
            logger.Trace("Setting gating frequency to: " + value);
            response = settingsForm.Laser.Run("F", value);
            if (!response)
            {
                message("Could not set gating frequency");
                logger.Error("Could not set gating frequency");
                RunningState = RunningStates.Stopped;
                return false;
            }

            value = Math.Max(1, Math.Min(dutyCycle, 1000)).ToString().PadLeft(4, '0');
            logger.Trace("Setting duty cycle to: " + value);
            response = settingsForm.Laser.Run("D", value);
            if (!response)
            {
                message("Could not set duty cycle");
                logger.Error("Could not set duty cycle");
                RunningState = RunningStates.Stopped;
                return false;
            }

            return true;
        }

        public int ExpectedPits
        {
            get
            {
                bool laserEnabled = settingsForm.Settings.Fetch<bool>("connection/laser/enabled");
                if (!laserEnabled) return -1;

                LaserDurationSpecification laserDuration = settingsForm.Settings.Fetch<LaserDurationSpecification>("scan/laser/duration specification");
                int pulseCount = settingsForm.Settings.Fetch<int>("scan/laser/pulse count");
                int runTime = settingsForm.Settings.Fetch<int>("scan/laser/run time");
                int gatingFrequency = settingsForm.Settings.Fetch<int>("scan/laser/gating frequency");
                float dutyCycle1 = settingsForm.Settings.Fetch<float>("scan/laser/duty cycle");
                int dutyCycle = (int)(Math.Round(10 * dutyCycle1));

                if (laserDuration == LaserDurationSpecification.PulseCount) return pulseCount;
                else
                {
                    return (int)Math.Round(runTime * gatingFrequency / 1000.0);
                }
            }
        }

        /// <summary>
        /// Whether the laser is inited
        /// </summary>
        public bool LaserInited
        {
            get;
            set;
        }

        public bool NewLaserInited
        {
            get;
            set;
        }


        #endregion

        #region Scanning Work

        #region Helpers

        void startDAQ()
        {
            logger.Trace("Start DAQ");
            settingsForm.Sensor.PortBuffer.Clear();
        }

        void stopDAQ()
        {
            logger.Trace("Stop DAQ");
            settingsForm.Sensor.PortBuffer.StopAutoUpdate();
        }

        bool startLaser()
        {
            bool response=false;
            if (settingsForm.Settings.Fetch<bool>("connection/laser/enabled"))
            {
                logger.Trace("Start laser");
                response= settingsForm.Laser.Run("G");
            }
            else if (settingsForm.Settings.Fetch<bool>("connection/new laser/enabled"))
            {
                //logger.Trace("Start New laser");
                //response = settingsForm.NewLaser.Run("S");
                return true;
            }

            //return response;
            return true;
        }

        bool startMotor()
        {
            bool response = false;
            string moveMotorCommand = settingsForm.Settings.Fetch<string>("scan/motor command");
            if (settingsForm.Settings.Fetch<bool>("connection/laser/enabled"))
            {
                logger.Trace("Start motor");
               
                if (moveMotorCommand == "")
                {
                    logger.Trace("Skipping motor as no command");
                    return true;
                }
                response=settingsForm.Laser.Run(moveMotorCommand);
            }
            else if (settingsForm.Settings.Fetch<bool>("connection/new laser/enabled"))
            {
                //logger.Trace("Start New laser controller");

                if (moveMotorCommand == "")
                {
                    logger.Trace("Skipping New laser controller as no command");
                    return true;
                }
                response=settingsForm.NewLaser.Run(moveMotorCommand);
            }

            //logger.Trace("response retuned");
            return response;

        }

        bool killLaser()
        {
            logger.Trace("Stop laser");
            return settingsForm.Laser.Run("K");
        }

        private bool sleep(int time, BackgroundWorker worker, DoWorkEventArgs e)
        {
            int count = time / 1000 + 1;
            for (int i = 0; i < count; i++)
            {
                if (i == count - 1) Thread.Sleep(time - (count - 1) * 1000);
                else Thread.Sleep(1000);

                if (worker.CancellationPending)
                {
                    e.Cancel = true;
                    return false;
                }
            }

            return true;
        }
                
        #endregion

        void scanWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = sender as BackgroundWorker;

            bool sensorEnabled = settingsForm.Settings.Fetch<bool>("connection/sensor/enabled");
            bool laserEnabled = settingsForm.Settings.Fetch<bool>("connection/laser/enabled");
            bool NewlaserEnabled = settingsForm.Settings.Fetch<bool>("connection/new laser/enabled");


            int daqStart = settingsForm.Settings.Fetch<int>("scan/timing/daq start");
            int laserStart = settingsForm.Settings.Fetch<int>("scan/timing/laser start");
            int daqStop = settingsForm.Settings.Fetch<int>("scan/timing/daq stop");
            bool scanRepeat = settingsForm.Settings.Fetch<bool>("scan/timing/repeat/enabled");
            int scanRepeatCount = settingsForm.Settings.Fetch<int>("scan/timing/repeat/count");
            int scanRepeatInterval = Convert.ToInt32(1000 * settingsForm.Settings.Fetch<float>("scan/timing/repeat/interval")); // convert from s to ms
            int moveMotor = settingsForm.Settings.Fetch<int>("scan/timing/move motor");

            if (worker.CancellationPending) { e.Cancel = true; return; }

            if (!scanRepeat) scanRepeatCount = 1;
            if (daqStop < daqStart) daqStop = daqStart + 50;

            settingsForm.Sensor.PortBuffer.Clear();

            string[] remaining = new string[] { "daq start", "laser", "motor", "daq stop" };
            int[] times = new int[] { daqStart, laserStart, moveMotor, daqStop };
            string[] order = new string[4];
            int[] delays = new int[4];

            for (int i = 0; i < 4; i++)
            {
                int min_j = -1;
                for (int j = 0; j < 4; j++)
                {
                    if (times[j] == -1) continue;
                    if (min_j == -1 || times[j] < times[min_j])
                        min_j = j;
                }
                order[i] = remaining[min_j];
                delays[i] = times[min_j];
                for (int j = 0; j < i; j++)
                    delays[i] -= delays[j];
                times[min_j] = -1;
            }

            int scanRepeatDelay = scanRepeatInterval;
            for (int j = 0; j < 4; j++)
                scanRepeatDelay -= delays[j];
            scanRepeatDelay = Math.Max(scanRepeatDelay, 50);

            if (!sensorEnabled && !laserEnabled && !NewlaserEnabled)
                throw new ApplicationException("Error, trying to take a scan with neither laser or sensor working");

            if (!sensorEnabled)
            {
                // laser must be enabled then
                for (int runCount = 1; runCount <= scanRepeatCount; runCount++)
                {
                    logger.Trace("Scan {0} of {0}, only laser", runCount, scanRepeatCount);

                    // start laser
                    if (!sleep(laserStart, worker, e)) return;
                    if (!startLaser()) throw new ApplicationException("Could not activate laser gating");

                    if (worker.CancellationPending) { e.Cancel = true; return; }
                    if (runCount + 1 < scanRepeatCount && !sleep(scanRepeatInterval - laserStart, worker, e)) return;
                    if (worker.CancellationPending) { e.Cancel = true; return; }
                    // note: don't report progress as no scans to load
                }

                // add motor bit here!

                logger.Trace("Finished scan worker do work");
            }
            else
            {
                for (int runCount = 1; runCount <= scanRepeatCount; runCount++)
                {
                    logger.Trace("Scan {0} of {0}, sensor and maybe laser", runCount, scanRepeatCount);

                    // start autoupdating
                    settingsForm.Sensor.PortBuffer.AutoUpdate(50);

                    TimerCheck = settingsForm.Settings.Fetch<bool>("scan/timing/Timer Mode");
                    AutoCheck = settingsForm.Settings.Fetch<bool>("scan/timing/Auto Detect");

                    ScanFile fileDetails=null;

                    // code for time mode or auto mode check 

                    // if timer mode is enable
                    if(TimerCheck==true)
                    {

                        logger.Trace("Timer Mode");
                           
                        for (int i = 0; i < 4; i++)
                            {
                                
                                if (delays[i] > 0 && !sleep(delays[i], worker, e)) return;
                                logger.Trace("after return,order "+i+" :"+order[i]);   
                            switch (order[i])
                                {
                                    case "daq start":
                                        startDAQ();
                                        break;
                                    case "laser":
                                        if (!startLaser()) { stopDAQ(); throw new ApplicationException("Could not activate laser gating"); }
                                        break;
                                    case "motor":
                                        //if (!startMotor()) { stopDAQ(); throw new ApplicationException("Could not activate motor/new laser controller"); }
                                        //Above line is replaced by below code   
                                        /************* THis ONE(used threading) 21-aug-15 ***************/
                                            if (laserEnabled || NewlaserEnabled)
                                            {
                                                /*if (laserStart != daqStart)
                                                {
                                                    if (!sleep(laserStart - daqStart, worker, e)) { stopDAQ(); return; }
                                                }*/
                                                //skip because in new laser Start and Stop time is not given
                                                laserThread = new Thread(() => { if (!startMotor()) { stopDAQ(); throw new ApplicationException("Could not activate motor/newlaserController"); } });
                                                laserThread.Start();

                                                //if (!startLaser()) { stopDAQ(); throw new ApplicationException("Could not activate the PLC"); }
                                                // delay = daqStop - laserStart;
                                            }
                                           break;
                                    case "daq stop":
                                        stopDAQ();
                                        break;
                                    default:
                                        stopDAQ();
                                        throw new ApplicationException("Could not work out remaining tasks");
                                }

                                if (worker.CancellationPending) { e.Cancel = true; stopDAQ(); return; }
                            }

                        logger.Trace("Sensor Reading Starting...");

                        // read data
                         SensorReading[] readings = settingsForm.Sensor.TakeReadings();
                        
                       //  ScanFile fileDetails;
                         if (!saveReadings(readings, out fileDetails))
                         {
                             throw new ApplicationException("Could not get any non-error readings");
                         }


                    }// if end of timer check
                    //condition if auto detect mode is enable
                    else if(AutoCheck==true)
                    {

                        logger.Trace("Auto Detection Mode");

                        logger.Trace("motor or new laser controller check");

                        /* if (!startMotor())
                        {
                            stopDAQ();
                            throw new ApplicationException("Could not activate motor/newlaserController");
                        } */  //THIS CODE is REPLACED by

                        /************* THis ONE(used threading) 21-aug-15 ***************/
                        if (laserEnabled || NewlaserEnabled)
                        {
                            /*if (laserStart != daqStart)
                            {
                                if (!sleep(laserStart - daqStart, worker, e)) { stopDAQ(); return; }
                            }*/
                            //skip because in new laser Start and Stop time is not given
                            laserThread = new Thread(() => { if (!startMotor()) { stopDAQ(); throw new ApplicationException("Could not activate motor/newlaserController"); } });
                            laserThread.Start();

                            //if (!startLaser()) { stopDAQ(); throw new ApplicationException("Could not activate the PLC"); }
                            // delay = daqStop - laserStart;
                        }


                          //new threshold concept
                          logger.Trace("Laser Check");
                         if (!startLaser()) 
                         {
                             stopDAQ(); 
                             throw new ApplicationException("Could not activate laser gating");
                         }
                                  

                                /****code for autosample *************************************/
                                //long lowerRange = settingsForm.Settings.Fetch<long>("scan/timing/Lower Range");
                                //long higherRange = settingsForm.Settings.Fetch<long>("scan/timing/Higher Range");
                        double lowerRange = settingsForm.Settings.Fetch<double>("scan/timing/Lower Range");
                        //double lowerRange= settingsForm.Settings.Fetch<double>("timing/Lower Range");
                        double higherRange = settingsForm.Settings.Fetch<double>("scan/timing/Higher Range");        
                        int EdgePoints = settingsForm.Settings.Fetch<int>("scan/timing/Edge Points");
                                
                                // varaible that helping to find start and end
                                long Threshold = EdgePoints, count, readingCount = 0;

                                // read data
                               // SensorReading[] readings;
                                // shortcut to sensor object
                                //PVSProfile settings = (PVSProfile)optionsDialog.ProfileControls.CurrentOptions;
                               /// ISensor sensor = optionsDialog.ProfileControls.Sensor;
                                //list for storing copy the readings from 'readings'
                                List<SensorReading> resultReading = new List<SensorReading>();
                                List<SensorReading> resultReadingIgnored = new List<SensorReading>();
                                // clear buffers
                               // sensor.PortBuffer.Clear();

                                // automatically buffer data temporarily to prevent filling serial ports buffer
                              //  sensor.PortBuffer.AutoUpdate(50);

                                   // logger.Trace("Like PVS");
                                
                         //take readings
                        //02-Jan-19
                        //Due to some issue with the readings in sensor or with sensor settings,it was not breaking like it was taking single readings 
                        //from some kind of buffer i guess
                        //To prevent that logic is implemented that break if some number of same points occured after some number of different points
                                SensorReading[] readings;
                                                        // loop for getting proper readings (for not to skip any of the reading)
                                                        for (int j = 0; j < 2; j++)
                                                        {
                                                            //to serach for new start
                                                            count = 0;
                                                            int breakpoint = 0, differentReadingBreakPoint = 0;
                                                            double repeatedReadings = 0, differentReadings = 0;
                                                            while (true)
                                                            {
                                                               

                                                                // cancel if necessary
                                                                if (worker.CancellationPending)
                                                                {
                                                                    // stop buffering
                                                                    settingsForm.Sensor.PortBuffer.StopAutoUpdate();
                                                                    e.Cancel = true;
                                                                    return;
                                                                }

                                                              
                                                                readings = settingsForm.Sensor.TakeReadings();
                                                                //double max=0, min=50;
                                                                //int no = 0,no1=0;
                                                                foreach (SensorReading r in readings)
                                                                {
                                                                    // first time start and store readings
                                                                    if (j == 0)
                                                                    {

                                                                        //find start
                                                                        if (count >= Threshold)
                                                                        {
                                                                            //ignore the repeated readings during the start of the data capture.
                                                                            //this should not create any problem at the start as we can safely remove repeated readings.
                                                                            if (differentReadings != r.Value)
                                                                            {
                                                                            // copy readings in seperate list
                                                                            // these are main readings which will be used for drawing graph later on
                                                                            resultReading.Add(r);
                                                                            //increment reading count
                                                                            readingCount++;
                                                                            
                                                                            differentReadingBreakPoint++;
                                                                               
                                                                            }
                                                                        }

                                                                        //ignore points till Threshold
                                                                        if (count < Threshold)
                                                                        {
                                                                            // skip points till threshold
                                                                            // if (!r.IsError)
                                                                            if ((lowerRange <= r.Value) && (r.Value <= higherRange))
                                                                            {
                                                                                 count++;
                                                                            }
                                                                            //restart if incorrect reading found
                                                                            else
                                                                            {
                                                                                // error resets search for start
                                                                                count = 0;
                                                                            }

                                                                        }

                                                                        
                                                                    }
                                                                    // for finding end
                                                                    // this j=1 logic
                                                                    else
                                                                    {
                                                                        //logger.Trace("repeat reading value: "+repeatedReadings+" r.value: "+r.Value);
                                                                        if (differentReadingBreakPoint > 100 && repeatedReadings == r.Value)
                                                                        {
                                                                            breakpoint++;
                                                                        }
                                                                       
                                                                        if (breakpoint > 100  || count >= Threshold)
                                                                        {
                                                                            break;
                                                                        }
                                                                        // ignore incorrect points
                                                                        //if (!r.IsError)
                                                                        if ((lowerRange <= r.Value) && (r.Value <= higherRange))
                                                                        {
                                                                            
                                                                            //only take the reading when they are different.
                                                                            if (differentReadings != r.Value)
                                                                            {
                                                                            resultReading.Add(r);
                                                                            count = 0;
                                                                            breakpoint = 0;
                                                                            
                                                                                differentReadingBreakPoint++;
                                                                            }
                                                                            else
                                                                            {
                                                                                resultReadingIgnored.Add(r);
                                                                               
                                                                            }

                                                                        }
                                                                        // store correct readings
                                                                        //if (!r.IsError)
                                                                        else
                                                                        {
                                                                            //logger.Trace("Value is out of range so increasing the count ,value:"+r.Value);
                                                                            count++;

                                                                        }
                                                                    }  //j=1
                                                                    repeatedReadings = r.Value;
                                                                    //if(j==1)
                                                                    //logger.Trace("Repeat reading at the bottom "+ repeatedReadings);
                                                                    differentReadings = r.Value;
                                                                }//end of foreach

                                                                //MessageBox.Show(min+" , "+max+" ,count= "+count+" threshold="+Threshold+" , j="+j );
                                                                // check if reached end/start of scan
                                                                if (breakpoint > 100 || count >= Threshold)
                                                                {
                                                                    logger.Trace("Total Readings considered :"+resultReading.ToArray().Length);
                                                                    logger.Trace("Total readings ignored :"+resultReadingIgnored.ToArray().Length);
                                                                    logger.Trace("Different reading breakpoint "+differentReadingBreakPoint);
                                                                    break;
                                                                }

                                                            
                                                            }//end of while

                                                        }// end of for j
                             

                                // stop buffering
                                settingsForm.Sensor.PortBuffer.StopAutoUpdate();
                                logger.Trace("Stopping auto update");
                                /**********by shubhendu****************************************************/

                              
                                if (!saveReadings(resultReading.ToArray(), out fileDetails))
                                {
                                    throw new ApplicationException("Could not get any non-error readings");
                                }

                    }

                    // read data
                   // SensorReading[] readings = settingsForm.Sensor.TakeReadings();

                   
                    // load scan file
                    worker.ReportProgress(Convert.ToInt32(100.0 * runCount / scanRepeatCount), new LastScanDetails(runCount, scanRepeatCount, fileDetails, ScanWorkerArgs.Positions.Left, false, ExpectedPits));

                    // repeat
                    if (worker.CancellationPending) { e.Cancel = true; return; }
                    if (runCount < scanRepeatCount)
                        if (!sleep(scanRepeatDelay, worker, e)) return;
                    if (worker.CancellationPending) { e.Cancel = true; return; }
                }
            }
            logger.Trace("Finished scan worker do work");
        }


        public SensorReading[] TakeMockReadings(int count)
        {
            SensorReading[] readings = new SensorReading[50];
            double currentTimeStamp = DateTime.Today.Millisecond;
            //first send first 5 records as -10 and then send 40 records as .5 and then send 2 records as -10.
            for (int i = 0; i < 50; i++)
            {
                if (i < 10)
                    readings[i] = new SensorReading(-10, currentTimeStamp);  //-10
                else if (i < 40)
                    readings[i] = new SensorReading(new Random().Next(1, 10) * 0.1);  //correct readings
                else
                    readings[i] = new SensorReading(-10);  //-10
            }
            return readings;

        }

        void scanWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            // load scans
            LastScanDetails details = (LastScanDetails)e.UserState;
            LoadScan(details);
        }

        void scanWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            logger.Trace("Scan worker completed");
            
            if (e.Error != null)
            {
                logger.ErrorException("Scan worker failed: " + e.Error.Message, e.Error);
                MessageBox.Show("No in-range data points available");
                //message("Error taking scan: " + e.Error.Message);
                segmentBuffer.Clear();
                RunningState = RunningStates.Stopped;
                return;
            }

            if (e.Cancelled)
            {
                logger.Warn("Scan worker was cancelled");
                segmentBuffer.Clear();
                RunningState = RunningStates.Stopped;
                return;
            }

            // if finished, overwrite last result with joined results
            bool multipleSegmentScans = settingsForm.Settings.Fetch<bool>("data/segments/multiple segments");
            if (multipleSegmentScans)
            {
                segmentBuffer.Add(lastAnalysis);
                if (segmentBuffer.IsFull)
                    handleFinishMultipleSegmentScan();
            }

            // this takes care of state changes
            RunningState = RunningStates.Stopped;    
            
            //enable stitching or averaging option
            if (Algorithm != "Surface Rougness")
            {
                grpStitchingAvging.Enabled = true;
            
            }

            // insert into database
            Inserted = 1;
            InsertToDb();

            //Stop laser thread and join it with the main thread ---- 21-Aug-15
            if (settingsForm.Settings.Fetch<bool>("connection/laser/enabled") || settingsForm.Settings.Fetch<bool>("connection/new laser/enabled"))
            {
                laserThread.Join();
            }

        }

        private void handleFinishMultipleSegmentScan()
        {
            if (segmentBuffer.Count > 0)
            {
                //Get settings from the database
                GetSettingsAsPerPaint();

                DFMAnalysis analysis = getEmptyAnalysis();
                segmentBuffer.GetJoinedData(analysis);
                //Inserted = 1;
                LoadScan(analysis);
                
                //InsertToDb();
                segmentBuffer.Clear();
            }
            handleStateUpdate();
        }

        #endregion

        #endregion

        #region Scan Analysis & Display

        private double[] pitAverages = new double[3] { -1, -1, -1 };
        private double[] pitStDevs = new double[3] { -1, -1, -1 };
        private int[] pitCounts = new int[3] { -1, -1, -1 };

        
        private void updatePitDisplays()
        {
            if (pitAverages[0] == -1)
            {
                // cleared
                foreach (CaptionedDisplay disp in new CaptionedDisplay[] { dispLeftStats, dispMiddleStats, dispRightStats })
                {
                    disp.Text = "-";
                    disp.Caption = "";
                }

                lblLeftStats.Text = "Left";
                lblMiddleStats.Text = "Middle";
                lblRightStats.Text = "Right";
            }
            else
            {
                string pitAverageFormat = "0.00";
                string pitStDevFormat = (CurrentUnits == DataUnits.Metric ? "0.00" : "0.000");

                // convert first
                dispLeftStats.Text = micronConverter(pitAverages[0]).ToString(pitAverageFormat);
                dispLeftStats.Caption = "sd = " + micronConverter(pitStDevs[0]).ToString(pitStDevFormat);
                lblLeftStats.Text = "Left " + pitCounts[0].ToString();

                dispMiddleStats.Text = micronConverter(pitAverages[1]).ToString(pitAverageFormat);
                dispMiddleStats.Caption = "sd = " + micronConverter(pitStDevs[1]).ToString(pitStDevFormat);
                lblMiddleStats.Text = "Middle " + pitCounts[1].ToString();

                dispRightStats.Text = micronConverter(pitAverages[2]).ToString(pitAverageFormat);
                dispRightStats.Caption = "sd = " + micronConverter(pitStDevs[2]).ToString(pitStDevFormat);
                lblRightStats.Text = "Right " + pitCounts[2].ToString();
            }
        }

        private void UpdateLeftMiddleRight()
        {
            // fill boxes
            //int numPitsLeft = analysis.Data.PeakCount / 3;
            int numPitsLeft = thickness.Length / 3;
            int numPitsRight = Math.Max(0, numPitsLeft);
            //int numPitsMiddle = Math.Max(0, analysis.Data.PeakCount - numPitsLeft - numPitsRight);
            int numPitsMiddle = Math.Max(0, thickness.Length - numPitsLeft - numPitsRight);
            double av1 = 0.0, av2 = 0.0, av3 = 0.0;
            double av12 = 0.0, av22 = 0.0, av32 = 0.0; // squared sums
            //for (int i = 0; i < analysis.Data.PeakCount; i++)
            for (int i = 0; i < thickness.Length; i++)
            {
                if (i < numPitsLeft)
                {
                    //av1 += analysis.Data.Peaks[i];
                    av1 += thickness[i];
                    //av12 += analysis.Data.Peaks[i] * analysis.Data.Peaks[i];
                    av12 += thickness[i] * thickness[i];
                }
                else if (i < numPitsLeft + numPitsMiddle)
                {
                    //av2 += analysis.Data.Peaks[i];
                    av2 += thickness[i];
                    //av22 += analysis.Data.Peaks[i] * analysis.Data.Peaks[i];
                    av22 += thickness[i] * thickness[i];
                }
                else
                {
                   // av3 += analysis.Data.Peaks[i];
                    av3 += thickness[i];
                    //av32 += analysis.Data.Peaks[i] * analysis.Data.Peaks[i];
                    av32 += thickness[i] * thickness[i];
                }
            }

            av1 /= numPitsLeft; av12 /= numPitsLeft;
            av2 /= numPitsMiddle; av22 /= numPitsMiddle;
            av3 /= numPitsRight; av32 /= numPitsRight;

            // save these values so units can be done properly
            pitAverages[0] = av1;
            pitAverages[1] = av2;
            pitAverages[2] = av3;
            pitStDevs[0] = Math.Sqrt(av12 - av1 * av1);
            pitStDevs[1] = Math.Sqrt(av22 - av2 * av2);
            pitStDevs[2] = Math.Sqrt(av32 - av3 * av3);
            pitCounts[0] = numPitsLeft;
            pitCounts[1] = numPitsMiddle;
            pitCounts[2] = numPitsRight;

        
        }


        // functions for OptionalLeftMiddleRight


        private void updatePitDisplaysOptional()
        {
            if (pitAverages[0] == -1)
            {
                // cleared
                foreach (CaptionedDisplay disp in new CaptionedDisplay[] { dispLeftStats, dispMiddleStats, dispRightStats })
                {
                    disp.Text = "-";
                    disp.Caption = "";
                }

                lblLeftStats.Text = "Left";
                lblMiddleStats.Text = "Middle";
                lblRightStats.Text = "Right";
            }
            else
            {
                string pitAverageFormat = "0.00";
                string pitStDevFormat = (CurrentUnits == DataUnits.Metric ? "0.00" : "0.000");

                // convert first
                dispLeftStats.Text = micronConverter(pitAverages[0]).ToString(pitAverageFormat);
                dispLeftStats.Caption = "sd = " + micronConverter(pitStDevs[0]).ToString(pitStDevFormat);
                lblLeftStats.Text = "Left " + pitCounts[0].ToString();

                dispMiddleStats.Text = micronConverter(pitAverages[1]).ToString(pitAverageFormat);
                dispMiddleStats.Caption = "sd = " + micronConverter(pitStDevs[1]).ToString(pitStDevFormat);
                lblMiddleStats.Text = "Middle " + pitCounts[1].ToString();

                dispRightStats.Text = micronConverter(pitAverages[2]).ToString(pitAverageFormat);
                dispRightStats.Caption = "sd = " + micronConverter(pitStDevs[2]).ToString(pitStDevFormat);
                lblRightStats.Text = "Right " + pitCounts[2].ToString();
            }
        }

        private void UpdateLeftMiddleRightOptional()
        {
            // fill boxes
            //int numPitsLeft = analysis.Data.PeakCount / 3;
            //double  IgnoreVal= settingsForm.Settings.Fetch<double>("misc/Ignore Edge");
            //long Diff = Convert.ToInt64(((TotalSampleLength-IgnoreVal)-(IgnoreVal+0))/3);

            //for()

            //for (long pitCount = 0; pitCount < thickness.Length; pitCount++)
            //{ 
                    
                    
           // }

            txtPits.Text = "";
            //capDispMax.Text = "";

            int numPitsLeft =0;
            int numPitsRight = 0;
            //int numPitsMiddle = Math.Max(0, analysis.Data.PeakCount - numPitsLeft - numPitsRight);
            int numPitsMiddle = 0;
            double av1 = 0.0, av2 = 0.0, av3 = 0.0;
            double av12 = 0.0, av22 = 0.0, av32 = 0.0; // squared sums
            string pitAverageFormat = "0.00";
            //for (int i = 0; i < analysis.Data.PeakCount; i++)
            for (int i = 0; i < thickness.Length; i++)
            {
                if ((Left1 <= Convert.ToInt64(Converted_x[i])) && (Convert.ToInt64(Converted_x[i]) <= Left2Mid1))
                {
                    //av1 += analysis.Data.Peaks[i];
                    av1 += thickness[i];
                    //av12 += analysis.Data.Peaks[i] * analysis.Data.Peaks[i];
                    av12 += thickness[i] * thickness[i];
                    numPitsLeft++;
                    txtPits.Text +=micronConverter(thickness[i]).ToString(pitAverageFormat) + "\r\n";
                }
                else if ((Left2Mid1 < Convert.ToInt64(Converted_x[i])) && (Convert.ToInt64(Converted_x[i]) <= Mid2Right1))
                {
                    //av2 += analysis.Data.Peaks[i];
                    av2 += thickness[i];
                    //av22 += analysis.Data.Peaks[i] * analysis.Data.Peaks[i];
                    av22 += thickness[i] * thickness[i];
                    numPitsMiddle++;
                    txtPits.Text += micronConverter(thickness[i]).ToString(pitAverageFormat) + "\r\n";
                }
                else if ((Mid2Right1 < Convert.ToInt64(Converted_x[i])) && (Convert.ToInt64(Converted_x[i]) <= Right2))
                {
                    // av3 += analysis.Data.Peaks[i];
                    av3 += thickness[i];
                    //av32 += analysis.Data.Peaks[i] * analysis.Data.Peaks[i];
                    av32 += thickness[i] * thickness[i];
                    numPitsRight++;
                    txtPits.Text += micronConverter(thickness[i]).ToString(pitAverageFormat) + "\r\n";
                }
            }

            av1 /= numPitsLeft; av12 /= numPitsLeft;
            av2 /= numPitsMiddle; av22 /= numPitsMiddle;
            av3 /= numPitsRight; av32 /= numPitsRight;

            lblTotalPit.Text = "Total No. of pits=" + (numPitsLeft + numPitsMiddle + numPitsRight);
            // save these values so units can be done properly
            pitAverages[0] = av1;
            pitAverages[1] = av2;
            pitAverages[2] = av3;
            pitStDevs[0] = Math.Sqrt(av12 - av1 * av1);
            pitStDevs[1] = Math.Sqrt(av22 - av2 * av2);
            pitStDevs[2] = Math.Sqrt(av32 - av3 * av3);
            pitCounts[0] = numPitsLeft;
            pitCounts[1] = numPitsMiddle;
            pitCounts[2] = numPitsRight;


        }


        private bool saveReadings(SensorReading[] readings, out ScanFile fileDetails)
        {
            // save readings
            // create x-values and y_values to write to file
            int rLen = readings.Length;
            double[] data_x, data_y;
            List<double> list_x = new List<double>();
            List<double> list_y = new List<double>();
            for (int j = 0; j < rLen; j++)
            {
                if (!readings[j].IsError)
                {
                    list_x.Add(readings[j].Time);
                    list_y.Add(readings[j].Value);
                }
            }

            data_x = list_x.ToArray();
            data_y = list_y.ToArray();

            // truncate data
            int truncateFirst = settingsForm.Settings.Fetch<int>("data/processing/truncate first");
            int truncateLast = settingsForm.Settings.Fetch<int>("data/processing/truncate last");

            rLen = data_x.Length;
            if (rLen > 0)
            {
                int start_index = 0;
                int end_index = rLen; // not included

                if (truncateFirst > 0) start_index = (int)Math.Floor(Convert.ToDouble(truncateFirst) / 100 * rLen);
                if (truncateLast > 0) end_index = rLen - (int)Math.Ceiling(Convert.ToDouble(truncateLast) / 100 * rLen);

                if (end_index > start_index)
                {
                    // todo: simplify with copyto
                    double[] data_x_tmp = new double[end_index - start_index];
                    double[] data_y_tmp = new double[end_index - start_index];

                    for (int i = start_index, j = 0; i < end_index; i++, j++)
                    {
                        data_x_tmp[j] = data_x[i];
                        data_y_tmp[j] = data_y[i];
                    }
                    data_x = data_x_tmp;
                    data_y = data_y_tmp;
                    rLen = end_index - start_index;
                }
                else
                {
                    data_x = new double[0];
                    data_y = new double[0];
                    rLen = 0;
                }
            }
                        
            if (rLen > 0)
            {
                string dataLocation = settingsForm.Settings.Fetch<string>("data/saving/location");
                string filenamePattern = settingsForm.Settings.Fetch<string>("data/saving/filename pattern");

                ScanManager scanManager = new ScanManager(dataLocation, filenamePattern);
                int nextScanId = scanManager.GetNextScanId();
                fileDetails = new ScanFile(dataLocation, filenamePattern, nextScanId);
                
                string filename = Path.Combine(dataLocation, fileDetails.Filename);
                
                // write data
                DataIO dataIO = new DataIO();
                dataIO.Mode = DataIO.Modes.Write;
                dataIO.Filename = Path.Combine(dataLocation, fileDetails.Filename);
                dataIO.WriteData(data_x, data_y);
                
                return true;
            }
            else
            {
                fileDetails = null;
                return false;
            }
        }

        protected void LoadScan(LastScanDetails details)
        {
            LoadScan(Path.Combine(details.FileDetails.Path, details.FileDetails.Filename));
        }

        protected DFMAnalysis getEmptyAnalysis()
        {
         
            DFMAnalysis analysis = new DFMAnalysis();
            analysis.Options = new DFMAnalysisOptions();
            //analysis.Options.SmoothingWidth = settingsForm.Settings.Fetch<int>("data/analysis/smoothing width");
            analysis.Options.SmoothingWidth = Convert.ToInt32(SmoothingWidth);
            //analysis.Options.MinPitWidth = settingsForm.Settings.Fetch<int>("data/analysis/min pit width");
            analysis.Options.MinPitWidth = Convert.ToInt32(MinPitWidth);
            //analysis.Options.PitPoints = settingsForm.Settings.Fetch<int>("data/analysis/pit count");
            analysis.Options.PitPoints = Convert.ToInt32(PointsInEachPit);
            //analysis.Options.ThresholdParameter = settingsForm.Settings.Fetch<float>("data/analysis/threshold parameter");
            analysis.Options.ThresholdParameter = ThresholValue;
            //analysis.Options.Compression = settingsForm.Settings.Fetch<float>("data/analysis/compression factor");
            analysis.Options.Compression = CompressionFactor;
            //analysis.Options.SubstrateRoughness = settingsForm.Settings.Fetch<float>("data/analysis/substrate roughness");
            analysis.Options.SubstrateRoughness =SubstrateRoughness;

            return analysis;
        }

        protected void LoadScan(DFMAnalysis analysis)
        {
            rtxtboxStatus.Text = "Scan in Progress..";
            //lblError.Visible = false;
            lblError.Visible = false;

            //GetSettingsAsPerPaint();
          
            try
            {
                // setting input parameters
               // analysis = getEmptyAnalysis();
                
                analysis.Analyse();
                //analysis = getEmptyAnalysis();

                // don't validate on number of expected pits
                
                //dataFilter_x
               
                // Function for filteration
               // Filteration(analysis.Data.Data_X, analysis.Data.Data_Y);

                // add info to top display with units converted
                GraphPane pane = zgcScanProfile.GraphPane;
                pane.CurveList.Clear();
                //PointPairList points = new PointPairList(analysis.Data.Data_X, Array.ConvertAll(analysis.Data.Normalised_Y, micronConverter));
                PointPairList points = new PointPairList(dataFilter_x, dataFilter_y);
                //PointPairList points = new PointPairList(analysis.Data.Data_X, analysis.Data.Data_Y);
                pane.AddCurve("scan", points, Color.Blue, SymbolType.None);
                zgcScanProfile.AxisChange();
                zgcScanProfile.Invalidate();

                
               

                //capture length of data
               Length = analysis.Data.Data_X.Length;
                //Length = dataFilter_x.Length;
                

                //if algorithm1 is selected
                //if (algoSelect.rdbtnAlgo1.Checked == true)
                if (Algorithm=="Algorithm1")
                {
                   // Length = analysis.Data.Data_X.Length;

                    AlgoType = "Algo1";
                    
                                // save last scan
                                lastAnalysis = analysis;
                                
                                //display list of pits and final thickness
                                double s3 = 0.0;
                                  
                            
                                //Reset Controls
                                 txtPits.Text = "";
                                 //capDispMax.Text = "";
                                 lblTotalPit.Text = "Total No. of pits=" + analysis.Data.PeakCount.ToString();

                                 string pitAverageFormat = "0.00";
                                // Display all pitsvalues
                                for (long n = 0; n < analysis.Data.PeakCount; n++)
                                {
                                   
                                    txtPits.Text +=micronConverter(analysis.Data.Peaks[n]).ToString(pitAverageFormat)+"\r\n";
                                    s3 = s3 + analysis.Data.Peaks[n];
                                }
                                
                                //Calculate finalThickness
                                FinalThickness = s3 / analysis.Data.PeakCount;
                               
                               //enalble Refresh button
                                algoSelect.btnRefreshReading.Enabled = true;
                               //Display finalThickness
                                //capDispMax.Text = Math.Round(FinalThickness,2).ToString();
                                
                                maxav = analysis.Data.PeakCount;
                                //analysis.Data.PeakIndex;
                
                                thickness = new double[maxav];
                                Converted_x = new double[maxav];
                                for (int i = 0; i < analysis.Data.PeakCount; i++)
                                {
                                   
                                        thickness[i]= analysis.Data.Peaks[i];
                                       // Converted_x[i] = analysis.Data.Data_X[i];
                                      Converted_x[i]= analysis.Data.PeakIndex[i];
                                       
                                }
                    
                            //update PitValue Graph            
                           DispPitGraph();
                }
                
                //for algo2
               // else if (algoSelect.rdbtnAlgo2.Checked == true)
                else if (Algorithm == "Algorithm2")
                {
                    Length = dataFilter_x.Length;

                    AlgoType = "Algo2";
                    
                    //call thickness meausrement function
                    //calculateThickness(dataFilter_x,dataFilter_y);
                    //calculateThickness(analysis.Data.Data_X,analysis.Data.Data_Y);
                   // Algo2(analysis.Data.Data_X, analysis.Data.Data_Y);
                    Algo2(dataFilter_x,dataFilter_y);

                    

                }
                
                //for algo3
               // else if (algoSelect.rdbtnAlgo3.Checked == true)
                else if (Algorithm == "Algorithm3")
                {
                    Length = dataFilter_x.Length;
                    AlgoType = "Algo3";
                   
                    //Function to apply Algorithm3
                    Algo3(dataFilter_x,dataFilter_y);
                    //Algo3(analysis.Data.Data_X,analysis.Data.Data_Y);

                }
                 //for algo4
                //else if (algoSelect.rdbtnAlgo4.Checked == true)
                else if (Algorithm == "Plastisol Algorithm")
                {
                    Length = dataFilter_x.Length;
                    AlgoType = "Plastisol";

                    //Function to apply Algorithm3
                    Algo4(dataFilter_x,dataFilter_y);
                    //Algo4(analysis.Data.Data_X, analysis.Data.Data_Y);

                }
                //for surface roughness algorithm
                else if (Algorithm == "Surface Roughness")
                {
                    Length = dataFilter_x.Length;
                    AlgoType = "Surface Roughness";

                    //Function to apply Algorithm3
                    SurfaceRoughness(dataFilter_x, dataFilter_y);
                    //Algo4(analysis.Data.Data_X, analysis.Data.Data_Y);

                }

                // Update Left Right and Middle only in case of other than Surface Roughness
                if (Algorithm != "Surface Roughness")
                {
                   // grpBoxOutput.Visible = true;
                   // grpBoxPitValues.Visible = true;
                  //  grpBoxSurfaceRoughness.Visible = false;
                   // zgcPits.Visible = true;
                   // lblPitValueHead.Visible = true;
                    lblPitValueHead.Visible = true;
                    grpBoxPitValues.Enabled = true;
                    zgcPits.Enabled = true;
                    lblMiddleStats.Visible = true;
                    dispMiddleStats.Visible = true;
                    grpStitchingAvging.Enabled = true;
                    RaAvg = 0.0;
                    RpAvg = 0.0;

                    //if ((LeftMidRightCheck == true) && (Algorithm !="Algorithm1"))
                    if ((LeftMidRightCheck == true))
                    {
                        UpdateLeftMiddleRightOptional();

                    }
                    else
                    {
                        // Calculate Left Middle and Right values
                        UpdateLeftMiddleRight();
                    }
                    // Display left,middle and right
                    updatePitDisplays();

                    

                    // For checking Stitching or averaging (if enable)
                    if (rdNormalBtn.Checked == false)
                    {
                        Check_stitch_avg();
                    }
                    else
                    {
                        rtxtboxStatus.Text = "Scan process Completed";
                    }
                }
                else
                {
                    //grpBoxOutput.Visible = false;
                    //grpBoxPitValues.Visible = false;
                    //grpBoxSurfaceRoughness.Visible = true;
                   // zgcPits.Visible = false;
                    //lblPitValueHead.Visible = false;
                    maxav = 0;
                    //thickness=new double[0];
                    pitAverages[0] = 0.00;
                    pitAverages[1] = 0.00;
                    pitAverages[2] = 0.00;
                    lblPitValueHead.Visible = false;
                    grpBoxPitValues.Enabled = false;
                    GraphPane pane1 = zgcPits.GraphPane;
                    pane1.CurveList.Clear();
                    txtPits.Text = "";
                    lblTotalPit.Text = "";
                    zgcPits.Enabled = false;
                    lblMiddleStats.Visible = false;
                    dispMiddleStats.Visible = false;
                    grpStitchingAvging.Enabled = false;
                    dispLeftStats.Caption = "";
                    dispRightStats.Caption = "";
                   rtxtboxStatus.Text = "Scan process Completed";
                
                }

            }
            catch (Exception ex)
            {
                logger.ErrorException(ex.Message, ex);
                throw;
            }

            btnCopyPit.Enabled = true;
            btnCopyPit.Text = "Copy";
        }
        
       

        DFMAnalysis lastAnalysis = null;
        protected void LoadScan(string filename)
        {
            FileNameRefresh = filename;
            try
            {
                // get settings from the database
                GetSettingsAsPerPaint();
                
                DFMAnalysis analysis= getEmptyAnalysis();
               // DFMAnalysis analysis;

                DataIO reader = new DataIO();
                reader.Mode = DataIO.Modes.Read;
                reader.Filename = filename;

                double[] data_x, data_y;
                reader.ReadData(out data_x, out data_y);

                // filter data
                if (settingsForm.Settings.Fetch<bool>("data/processing/filtering/enabled"))
                {
                    int rLen = data_y.Length;
                    double[] data_y_temp = new double[rLen];
                    for (int j = 0; j < rLen; j++)
                    {
                        data_y_temp[j] = data_y[j];
                    }

                   /* NoiseFilter.Filter(data_y_temp, out data_y,
                        settingsForm.Settings.Fetch<float>("data/processing/filtering/peak threshold") / 1000,
                        settingsForm.Settings.Fetch<float>("data/processing/filtering/peak hysteresis") / 1000,
                        analysis.Options.SmoothingWidth,
                        settingsForm.Settings.Fetch<int>("data/processing/filtering/max peak width"));*/

                    //Annalysis tab has been removed so change smmothing width to come from database
                    NoiseFilter.Filter(data_y_temp, out data_y,
                        settingsForm.Settings.Fetch<float>("data/processing/filtering/peak threshold") / 1000,
                        settingsForm.Settings.Fetch<float>("data/processing/filtering/peak hysteresis") / 1000,
                        Convert.ToInt32(SmoothingWidth),
                        settingsForm.Settings.Fetch<int>("data/processing/filtering/max peak width"));

                }


                /******************** New code For Filtration *******************/

               
                Filteration(data_x,data_y);
                
               
                /**************** By shubhendu *******************************/
                //Length = data_x.Length;
                 //analysis.SetData(data_x, data_y);
                analysis.SetData(dataFilter_x, dataFilter_y);

               Inserted = 1;

                LoadScan(analysis);

                //InsertToDb();

               /* if (Inserted == 0)
                {
                    Inserted=1;
                }
                else
                {
                    LoadScan(analysis);
                    
                }*/
                //for algo2
               /* if (algoSelect.rdbtnAlgo2.Checked == true)
                {


                    //code by shubhendu//
                    //call thickness meausrement function
                    
                    calculateThickness(data_x, data_y);
                    


                    //end//
                }
                    //for algo3
                else if (algoSelect.rdbtnAlgo3.Checked == true)
                {
                    //Length = data_x.Length;
                    Algo3(data_x, data_y);

                }
               */
                
            
               /* if (Inserted != 0)
                {
                    InsertToDb();
                }
                Inserted++;
                */
             
                
            }
            catch (Exception ex)
            {
                logger.ErrorException(ex.Message, ex);
                //listThickness.Items.Clear();
                txtPits.Text = "";
                //capDispMax.Text = "";
                //MessageBox.Show(ex.Message);
                MessageBox.Show("No pit found in this range with mentioned parameters.");
                //throw;
            }

           

        }

       

        //get all variables from the daatabase as per the paint selected
        public void GetSettingsAsPerPaint()
        {

            try
            {

                SQLiteConnection con = new SQLiteConnection("Data Source=ScanDataBase.s3db");
                con.Open();
                SQLiteCommand com = new SQLiteCommand();
                com.Connection = con;
               // int f = 50;
                // get all pit values of scan
                com.CommandText = "select * from tbl_paint_type where PaintName='"+cmbPaintType.SelectedItem+"'";
                com.ExecuteNonQuery();
                SQLiteDataReader dr = com.ExecuteReader();
                while (dr.Read())
                {
                    PaintName=dr["PaintName"].ToString();
                    Algorithm = dr["Algorithm"].ToString();
                    NoPointAvg =Convert.ToInt64(dr["NoPointAvg"].ToString());
                    NoPointBwAvg =Convert.ToInt64(dr["NoPointBwAvg"].ToString());
                    DistanceBwPits=Convert.ToInt64(dr["DistanceBwPits"].ToString());
                    Percentage = Convert.ToInt64(dr["Percentage"].ToString());
                    ThresholValue =Convert.ToDouble(dr["ThresholdValue"].ToString());
                    AveragePoints = Convert.ToInt64(dr["AveragePoints"].ToString());
                    Range=Convert.ToInt64(dr["Range"].ToString());
                    Factor =Convert.ToDouble(dr["Factor"].ToString());
                    //MessageBox.Show(dr["HeightOnly4First"].ToString());
                    if(dr["HeightOnly4First"].ToString()=="All")
                       {
                           HeightsOnly4First = false;
                       }
                    else if (dr["HeightOnly4First"].ToString() == "First")
                        {
                            HeightsOnly4First = true;
                        }
                    
                    if (dr["HeightEnable"].ToString() == "Yes")
                    {
                        HeightEnable =true ;
                    }
                    else if (dr["HeightEnable"].ToString() == "No")
                    {
                        HeightEnable = false;
                    }

                    HeightValue = Convert.ToDouble(dr["Height"]);
                    SubstrateRoughness = Convert.ToDouble(dr["SubstrateRoughness"].ToString());
                    SmoothingWidth = Convert.ToInt64(dr["SmoothingWidth"].ToString());
                    MinPitWidth = Convert.ToInt64(dr["MinPitWidth"].ToString());
                    CompressionFactor = Convert.ToDouble(dr["CompressionFactor"].ToString());
                    PointsInEachPit = Convert.ToInt64(dr["PointsInEachPit"].ToString());
                    OffsetValue = Convert.ToDouble(dr["OffsetValue"].ToString());
                    AveragingNum = Convert.ToInt64(dr["AveragingNum"].ToString());
                    NoPointsForYmaxAvg = Convert.ToInt64(dr["NoPointsForYmaxAvg"].ToString());
                    NoCountBackPts = Convert.ToInt32(dr["NoCountBackPts"].ToString());

                }
                dr.Close();
                con.Close();
            }
            catch (SQLiteException ex)
            {
                MessageBox.Show(ex.Message);
            }

        
        }


        //******shubhendu*** POC for thickness***///

        private void Filteration(double[] x,double[] y)
        {
            filterCheck = settingsForm.Settings.Fetch<bool>("misc/Filter Check");
            Length = x.Length;
           
            //get LineSpeed and Sensor Frequency for X-scale conversion
            LineSpeed =Convert.ToDouble(settingsForm.Settings.Fetch<int>("misc/Line Speed"));
            sensorFrequency =Convert.ToDouble(settingsForm.Settings.Fetch<int>("misc/Sensor Frequency"));

            double lineSpeedInMpS =Convert.ToInt32(LineSpeed)*1000/60;
            double lineBySensorFreq =Math.Round((lineSpeedInMpS / sensorFrequency),1);

            long fStart = 0, fEnd = Length,count=0;

            //if filteration is enable
            if (filterCheck == true)
            {
                // Filter Difference
                filterDiff = settingsForm.Settings.Fetch<double>("misc/Diff filter");
               // long f=1;
                
                       //for finding start point
                        for (long f1 = 1; f1 < Length; f1++)
                        {
                            if ((y[f1]- y[f1 - 1]) >= filterDiff)
                            {
                                fStart = f1;
                                break;
                            }
                        }
                        
                        //for finding End point
                        for (long f3 = fStart; f3 < Length; f3++)
                        {
                            if ((y[f3] - y[f3 + 1]) >= filterDiff)
                            {
                                fEnd = f3;
                                break;
                            }
                        }

                        dataFilter_y = new double[fEnd-fStart];
                        dataFilter_x = new double[fEnd-fStart];

                        double sum = 0.0;
                        //collect filter points
                        for (long f4 = fStart; f4 < fEnd; f4++)
                        {
                            //dataFilter_x[count] = x[count];
                            sum = sum + lineBySensorFreq;
                            dataFilter_x[count] = sum;
                            dataFilter_y[count] = y[f4];
                            count++;

                        }

                // total Sample Length is the last X-value
                TotalSampleLength = Convert.ToInt64(sum);
                
                Length = count;
            }
            else
            {
                dataFilter_y = new double[Length];
                dataFilter_x = new double[Length];

                double sum1 = 0.0;
                for (long f2 = 0; f2 < Length; f2++)
                {
                    //dataFilter_x[f2] = x[f2];
                    sum1=sum1+lineBySensorFreq;
                    dataFilter_x[f2]=sum1;
                    dataFilter_y[f2] = y[f2];
                }
            
                // total Sample Length is the last X-value
                TotalSampleLength = Convert.ToInt64(sum1);
            }
            string pitAverageFormat = "0.0";
            // show sample Length
            lblTotalSampleLen.Text ="Total Sample Length= "+ micronConverter(TotalSampleLength).ToString(pitAverageFormat);
            lblTotalSampleLen.Visible = true;

            // Define Range for Left Middle and Right
            LeftMidRightCheck = settingsForm.Settings.Fetch<bool>("misc/Apply Left Middle Right");
            IgnoreEdge =Convert.ToDouble(settingsForm.Settings.Fetch<int>("misc/Ignore Edge"));

            Left1 =Convert.ToInt64(IgnoreEdge);
            Right2 = Convert.ToInt64(TotalSampleLength) - Convert.ToInt64(IgnoreEdge);

            long diff = (Right2 - Left1) / 3;

            Left2Mid1 = Left1 + diff;
            Mid2Right1 = Left2Mid1 + diff;
            

        }

        private void Algo2(double[] x, double[] y)
        {

            // Varaible initialization
            D = new double[Length];
           // PointsToAvg = Convert.ToInt64(algoSelect.nmPoints4AvgAlgo2.Value);
            //DiffBwAvgs = Convert.ToInt64(algoSelect.nmPointsBwAvgAlgo2.Value);
            //Algo3Threshold = Convert.ToDouble(algoSelect.nmThresholdValue.Value);
            //AvgPitPoint = Convert.ToInt64(algoSelect.nmAlgo3AvgPoint.Value);

            //Calculate Difference array
            /*for (long diff = 0; diff < (Length - 3); diff++)
            {

                D[diff] = y[diff + 3] / 1000 - y[diff] / 1000;

            }*/

            Dnext = new double[Length];

            //Calculate D Column
            long diff1 = 0, k = 1;
            //k = (DiffBwAvgs + PointsToAvg) / 2;
            k = (NoPointBwAvg + NoPointAvg) / 2;
            //while (diff1 < (Length - (DiffBwAvgs + PointsToAvg)))
            while (diff1 < (Length - (NoPointBwAvg + NoPointAvg)))
            {

                double sum1 = 0.0, avg1 = 0.0, avg2 = 0.0, sum2 = 0.0,sum3=0.0,avg3=0.0;
                //for (long diff2 = diff1; diff2 < (diff1 + PointsToAvg); diff2++)
                for (long diff2 = diff1; diff2 < (diff1 + NoPointAvg); diff2++)
                {
                   //sum1 = sum1 + y[diff2] / 1000;
                    sum1 = sum1 + y[diff2] ;
                }

                //avg1 = sum1 / PointsToAvg;
                avg1 = sum1 / NoPointAvg;

                //for (long diff3 = (diff1 + DiffBwAvgs); diff3 < (diff1 + DiffBwAvgs + PointsToAvg); diff3++)
                for (long diff3 = (diff1 + NoPointBwAvg); diff3 < (diff1 + NoPointBwAvg + NoPointAvg); diff3++)
                {
                    //sum2 = sum2 + y[diff3] / 1000;
                    sum2 = sum2 + y[diff3];
                }

                //avg2 = sum2 / PointsToAvg;
                avg2 = sum2 / NoPointAvg;

                //Ymax Averaging --- 15-sep-15
                for (long diff4 = (k - NoPointsForYmaxAvg); diff4 <= (k + NoPointsForYmaxAvg); diff4++)
                {
                    //sum2 = sum2 + y[diff3] / 1000;
                    sum3 = sum3 + y[diff4];
                }

                //avg2 = sum2 / PointsToAvg;
                avg3 = sum3 / (2 * NoPointsForYmaxAvg + 1);


                //Apply formula
                Dnext[k] = 1000 * (avg3 - (avg1 + avg2) / 2);
                //Dnext[k] = 1000 * (y[k] - (avg1 + avg2) / 2);
                //Dnext[k] = 1000 * (y[k]- (avg1 + avg2) / 2);

                k++;
                diff1++;

            }


            //Find PitValues
            double FirstPit=0.0; //changed from Dnext[1] to 0.0 //updated on 24-sep-15
            long PitAt = 1;
            maxav = 0;

            //Array for getting thickness by Algo2
            double[] Algo2thickness = new double[Length];
            // array for optional Left middle and Right
            Converted_x=new double[Length];
  
            //for next pit iteration
            long Next = 1;
            long NextLeft = 1;
            long NextRight = DistanceBwPits+DistanceBwPits*Percentage/100; // updated on 24-sep-15
           
            //Find Next Pits
            while (Next < (k-DistanceBwPits))
            {
                
                //Max Point b/w NextLeft and NextRight should be NextPit
                double pit = Dnext[NextLeft];
                PitAt = NextLeft;
                for (long NextPoint = NextLeft; NextPoint <= NextRight; NextPoint++)
                {
                    if (pit < Dnext[NextPoint])
                    { 
                        pit=Dnext[NextPoint];
                        //Next = NextPoint;
                        PitAt = NextPoint - NoCountBackPts;
                    }
                
                }

                //D column formula has changed  ---- 15-Sep-15
                pit = pit - SubstrateRoughness;

                //ignore pits which have value less than height(threshold)
                    if ((HeightEnable==true))
                    {
                        //pass
                        if (HeightValue <= pit)
                        {
                            //use substrate roughness
                            Algo2thickness[maxav] = pit;
                            Converted_x[maxav] = x[PitAt];
                            Next = PitAt;
                            maxav++;
                        }
                        else
                        {
                            Next = NextRight+1;
                        }
                    }
                    else
                    {
                            //use substrate roughness
                           Algo2thickness[maxav] = pit;
                           Converted_x[maxav] = x[PitAt];
                           Next = PitAt;
                           maxav++;
                    }

                      Next = Next + DistanceBwPits;
                      NextLeft = Next - DistanceBwPits * Percentage / 100;
                      NextRight = Next + DistanceBwPits * Percentage / 100;

            }

           //Intialize PitValues array
            thickness = new double[maxav];
            for (long thickIndex = 0; thickIndex < maxav; thickIndex++)
            {
                
                    thickness[thickIndex] = Algo2thickness[thickIndex];
               
            }


                // Function to find finalThickness
                FindThickness(thickness);

            // Function to update PitValues Graph
            DispPitGraph();

        }



        private void Algo3(double[] x, double[] y)
        {

            // Varaible initialization
            D = new double[Length];
           // PointsToAvg=Convert.ToInt64(algoSelect.nmPoints4Avg.Value);
            //DiffBwAvgs=Convert.ToInt64(algoSelect.nmPointsBwAvg.Value);
            //Algo3Threshold=Convert.ToDouble(algoSelect.nmThresholdValue.Value);
           // AvgPitPoint=Convert.ToInt64(algoSelect.nmAlgo3AvgPoint.Value);

            //Calculate Difference array
            for (long diff = 0; diff < (Length - 3); diff++)
            {

                //D[diff] = y[diff + 3]/1000 - y[diff]/1000;
                D[diff] = y[diff + 3] - y[diff];

            }

            Dnext = new double[Length];

            //Calculate D Column
            long diff1 = 0,k=1;
            //k= (DiffBwAvgs + PointsToAvg)/2;
            k = (NoPointAvg + NoPointBwAvg) / 2;
            //while(diff1 < (Length-(DiffBwAvgs+PointsToAvg)))
            while (diff1 < (Length - (NoPointAvg + NoPointBwAvg)))
            {

                double sum1 = 0.0,avg1=0.0,avg2=0.0,sum2=0.0,sum3=0.0,avg3=0.0;
                //for (long diff2 = diff1; diff2 <(diff1 + PointsToAvg);diff2++)
                for (long diff2 = diff1; diff2 < (diff1 + NoPointAvg); diff2++)
                {
                   // sum1=sum1+y[diff2]/1000;
                    sum1 = sum1 + y[diff2];
                }

                //avg1 = sum1 / PointsToAvg;
                avg1 = sum1 / NoPointAvg;

                //for (long diff3 = (diff1+DiffBwAvgs); diff3 <(diff1 +DiffBwAvgs+PointsToAvg); diff3++)
                for (long diff3 = (diff1 + NoPointBwAvg); diff3 < (diff1 + NoPointBwAvg + NoPointAvg); diff3++)
                {
                    //sum2 = sum2 + y[diff3]/1000;
                    sum2 = sum2 + y[diff3];
                }

                //avg2 = sum2 / PointsToAvg;
                avg2 = sum2 / NoPointAvg;

                //Ymax Averaging --- 15-sep-15
                for (long diff4 = (k - NoPointsForYmaxAvg); diff4 <= (k + NoPointsForYmaxAvg); diff4++)
                {
                    //sum2 = sum2 + y[diff3] / 1000;
                    sum3 = sum3 + y[diff4];
                }

                //avg2 = sum2 / PointsToAvg;
                avg3 = sum3 / (2 * NoPointsForYmaxAvg + 1);


                //Apply formula
                Dnext[k] = 1000 * (avg3 - (avg1 + avg2) / 2);
                //Dnext[k]=1000*(y[k]/1000-(avg1+avg2)/2);
                //Dnext[k] = 1000 * (y[k] - (avg1 + avg2) / 2);

                k++;
                diff1++;

            }

           // long t = (DiffBwAvgs + PointsToAvg) / 2, t2 = 0,Apit=0;
            long t = (NoPointBwAvg + NoPointAvg) / 2, t2 = 0, Apit = 0;
            double[] Algo3thickness = new double[Length];
            Algo3ThicknessIndex = new long[Length];
            Converted_x=new double[Length];
            
            //while (t < (Length - (DiffBwAvgs + PointsToAvg)))
            while (t < (Length - (NoPointBwAvg + NoPointAvg)))
            {
            
               // DifferThreshold = Algo3Threshold;
                DifferThreshold = ThresholValue;
            
                long pointAlgo3= FindT(t,D);

                double Dmax = Dnext[pointAlgo3];

                //apply count back
                pointAlgo3 = pointAlgo3 - NoCountBackPts;

                //for (long t1 = pointAlgo3; t1 < (pointAlgo3 + AvgPitPoint); t1++)
                for (long t1 = pointAlgo3; t1 < (pointAlgo3 + AveragePoints); t1++)
                {
                    if (Dmax < Dnext[t1])
                    { 
                        Dmax=Dnext[t1];
                        Apit = t1;
                    }

                }

               //use substrate roughness
                Algo3thickness[t2] =Dmax-SubstrateRoughness;
                Algo3ThicknessIndex[t2] = Apit;
                Converted_x[t2] = x[Apit];
                t2++;
               // t = pointAlgo3 + AvgPitPoint+1;
                t = pointAlgo3 + AveragePoints + 1;
            
            }

            maxav = (t2-1);
            
            //copy to common thickness array
            thickness=new double[maxav];
            for (int e = 0; e < maxav; e++)
            {
                thickness[e] = Algo3thickness[e];
            
            }

             // Function to find finalThickness
             FindThickness(Algo3thickness);

            // display average thicknees of all pits
            //capDispMax.Text = Math.Round(FinalThickness,2).ToString();

            // Function to update PitValues Graph
            DispPitGraph();

        }




        private void Algo4BackUp(double[] x, double[] y)
        {

            // Varaible initialization
            D = new double[Length];
            // PointsToAvg = Convert.ToInt64(algoSelect.nmPoints4AvgAlgo2.Value);
            //DiffBwAvgs = Convert.ToInt64(algoSelect.nmPointsBwAvgAlgo2.Value);
            //Algo3Threshold = Convert.ToDouble(algoSelect.nmThresholdValue.Value);
            //AvgPitPoint = Convert.ToInt64(algoSelect.nmAlgo3AvgPoint.Value);

            //Calculate Difference array
            /*for (long diff = 0; diff < (Length - 3); diff++)
            {

                D[diff] = y[diff + 3] / 1000 - y[diff] / 1000;

            }*/

            Dnext = new double[Length];

            //Calculate D Column
            long diff1 = 0, k = 1;
            //k = (DiffBwAvgs + PointsToAvg) / 2;
            k = (NoPointBwAvg + NoPointAvg) / 2;
            //while (diff1 < (Length - (DiffBwAvgs + PointsToAvg)))
            while (diff1 < (Length - (NoPointBwAvg + NoPointAvg)))
            {

                double sum1 = 0.0, avg1 = 0.0, avg2 = 0.0, sum2 = 0.0, sum3 = 0.0, avg3 = 0.0;
                //for (long diff2 = diff1; diff2 < (diff1 + PointsToAvg); diff2++)
                for (long diff2 = diff1; diff2 < (diff1 + NoPointAvg); diff2++)
                {
                    //sum1 = sum1 + y[diff2] / 1000;
                    sum1 = sum1 + y[diff2];
                }

                //avg1 = sum1 / PointsToAvg;
                avg1 = sum1 / NoPointAvg;

                //for (long diff3 = (diff1 + DiffBwAvgs); diff3 < (diff1 + DiffBwAvgs + PointsToAvg); diff3++)
                for (long diff3 = (diff1 + NoPointBwAvg); diff3 < (diff1 + NoPointBwAvg + NoPointAvg); diff3++)
                {
                    //sum2 = sum2 + y[diff3] / 1000;
                    sum2 = sum2 + y[diff3];
                }

                //avg2 = sum2 / PointsToAvg;
                avg2 = sum2 / NoPointAvg;

                //Ymax Averaging --- 15-sep-15
                for (long diff4 = (k - NoPointsForYmaxAvg); diff4 <= (k + NoPointsForYmaxAvg); diff4++)
                {
                    //sum2 = sum2 + y[diff3] / 1000;
                    sum3 = sum3 + y[diff4];
                }

                //avg2 = sum2 / PointsToAvg;
                avg3 = sum3 / (2 * NoPointsForYmaxAvg + 1);


                //Apply formula
                Dnext[k] = 1000 * (avg3 - (avg1 + avg2) / 2);
                //Dnext[k] = 1000 * (y[k] - (avg1 + avg2) / 2);
                //Dnext[k] = 1000 * (y[k]- (avg1 + avg2) / 2);

                k++;
                diff1++;

            }

            /* long t = (DiffBwAvgs + PointsToAvg) / 2, t2 = 0, Apit = 0;
             double[] Algo3thickness = new double[Length];
             Algo3ThicknessIndex = new long[Length];

             while (t < (Length - (DiffBwAvgs + PointsToAvg)))
             {

                 DifferThreshold = Algo3Threshold;

                 long pointAlgo3 = FindT(t, D);

                 double Dmax = Dnext[pointAlgo3];
                 for (long t1 = pointAlgo3; t1 < (pointAlgo3 + AvgPitPoint); t1++)
                 {
                     if (Dmax < Dnext[t1])
                     {
                         Dmax = Dnext[t1];
                         Apit = t1;
                     }

                 }

                 Algo3thickness[t2] = Dmax;
                 Algo3ThicknessIndex[t2] = Apit;
                 t2++;
                 t = pointAlgo3 + AvgPitPoint + 1;

             }*/

            //input parameters of NewALgo2
            //DistanceBwPits = Convert.ToInt64(algoSelect.nmDiffBwPits.Value);
            //Percentage = Convert.ToInt64(algoSelect.nmPercentage.Value);
            //height = Convert.ToDouble(algoSelect.nmHeight.Value);

            //Find PitValues
            double FirstPit = Dnext[1];
            long FirstPitAt = 1;

            //Array for getting thickness by Algo2
            double[] Algo4thickness = new double[Length];
            long[] Algo4thicknessIndex = new long[Length];
            Converted_x = new double[Length];

            //Find FirstPit
            for (long pitIndex = 1; pitIndex < DistanceBwPits; pitIndex++)
            {

                if (FirstPit < Dnext[pitIndex])
                {
                    FirstPit = Dnext[pitIndex];
                    FirstPitAt = pitIndex;
                }

            }


            //apply susbstrat roughness ---- 15-Sep-15
            FirstPit = FirstPit - SubstrateRoughness;
            // apply count back  ---- 15-Sep-15
            FirstPitAt = FirstPitAt - NoCountBackPts;

            //commented becasue this averaging is done with Ymax.. Formula changed --- 15-Sep-15 (Shubh)
            /*double firstSum = 0.0;
            // average out first pit value
            for (long firstAvgindex = (FirstPitAt - NoPointsForYmaxAvg); firstAvgindex <= (FirstPitAt + NoPointsForYmaxAvg); firstAvgindex++)
            {
                firstSum = firstSum + Dnext[firstAvgindex];

            }
            //Applying substrate roughness in th algo
            FirstPit = firstSum / ((2 * NoPointsForYmaxAvg) + 1) - SubstrateRoughness;*/



            if ((HeightEnable == true))
            {
                if (HeightValue > FirstPit)
                {
                    Algo4thickness[0] = 0;
                    maxav = 0;
                    if (HeightsOnly4First == true)
                    {
                        lblError.Visible = true;
                        lblError.ForeColor = Color.Red;
                        lblError.Text = "First Pit did not Cross Threshold!";
                    }
                }
                else
                {
                    //use substrate roughness
                    Algo4thickness[0] = FirstPit;
                    Algo4thicknessIndex[0] = FirstPitAt;
                    Converted_x[0] = x[FirstPitAt];
                    maxav = 1;

                }
            }
            else
            {
                // use substrate roughness
                Algo4thickness[0] = FirstPit;
                Algo4thicknessIndex[0] = FirstPitAt;
                Converted_x[0] = x[FirstPitAt];
                maxav = 1;

            }

            //show warning message if First Pit crossed height/threshold
            /*if (height > FirstPit)
            {
                 //rtxtboxStatus.ForeColor = Color.Red;
                //rtxtboxStatus.Text = "First Pit not Crossed Threshold!";
                lblError.Visible = true;
                lblError.ForeColor = Color.Red;
                lblError.Text = "First Pit not Crossed Threshold!";
            
            }*/

            long Next = FirstPitAt;
            long NextLeft = FirstPitAt;
            long NextRight = FirstPitAt;




            //Find Next Pits
            while (Next < (k - DistanceBwPits))
            {
                Next = Next + DistanceBwPits;
                NextLeft = Next - DistanceBwPits * Percentage / 100;
                NextRight = Next + DistanceBwPits * Percentage / 100;

                //Max Point b/w NextLeft and NextRight should be NextPit
                double pit = Dnext[NextLeft];
                for (long NextPoint = NextLeft; NextPoint <= NextRight; NextPoint++)
                {
                    if (pit < Dnext[NextPoint])
                    {
                        pit = Dnext[NextPoint];
                        Next = NextPoint;
                    }

                }

                //apply count back
                Next = Next - NoCountBackPts;
                //D column formula has changed  ---- 15-Sep-15
                pit = pit - SubstrateRoughness;

                //commented becasue this averaging is done with Ymax.. Formula changed --- 15-Sep-15 (Shubh)
                /* double SumPit = 0.0;
                 // average out pit values
                 for (long AvgIndex = (Next - NoPointsForYmaxAvg); AvgIndex <= (Next + NoPointsForYmaxAvg); AvgIndex++)
                 {
                     SumPit = SumPit + Dnext[AvgIndex];

                 }

                 pit = SumPit / ((2 * NoPointsForYmaxAvg) + 1) - SubstrateRoughness;*/


                //ignore pits which have value greater than height
                if ((HeightEnable == true) && (HeightsOnly4First == false))
                {
                    if (HeightValue < pit)
                    {
                        Algo4thickness[maxav] = pit;
                        Algo4thicknessIndex[maxav] = Next;
                        Converted_x[maxav] = x[Next];
                        maxav++;
                    }
                }
                else
                {
                    Algo4thickness[maxav] = pit;
                    Algo4thicknessIndex[maxav] = Next;
                    Converted_x[maxav] = x[Next];
                    maxav++;
                }




            }


            // get Range value
            //long range = Convert.ToInt64(algoSelect.nmRange.Value);

            double sum = 0.0;

            // Calculate Range Average
            for (long RangeIndex = 0; RangeIndex < (maxav - 1); RangeIndex++)
            {

                // long left = Algo4thicknessIndex[RangeIndex] + range;
                // long right = Algo4thicknessIndex[RangeIndex+1] - range;
                // Algo4thicknessIndex[maxav] = Next;
                long left = Algo4thicknessIndex[RangeIndex] + Range;
                long right = Algo4thicknessIndex[RangeIndex + 1] - Range;

                // double max = Dnext[left];
                // double min = Dnext[right];
                double max = 0.0;
                double min = 0.0;

                // Find max and min values between the Range
                for (long maxIndex = left; maxIndex < right; maxIndex++)
                {

                    // Find max value
                    if (max < Dnext[maxIndex])
                    {

                        max = Dnext[maxIndex];
                    }

                    // Find min value
                    if (min > Dnext[maxIndex])
                    {
                        min = Dnext[maxIndex];

                    }
                }

                double diff = max - min;
                sum = sum + diff;

            }

            double avg = sum / (maxav - 1);

            // get multiplication factor
            //double factor = Convert.ToDouble(algoSelect.nmFactor.Value);



            //Intialize PitValues array
            thickness = new double[maxav];
            for (long thickIndex = 0; thickIndex < maxav; thickIndex++)
            {

                thickness[thickIndex] = Algo4thickness[thickIndex] + (avg * Factor);

            }


            // Function to find finalThickness
            FindThickness(thickness);

            // display average thicknees of all pits
            //capDispMax.Text = Math.Round(FinalThickness,2).ToString();

            // Function to update PitValues Graph
            DispPitGraph();

        }

        private void Algo4(double[] x, double[] y)
        {

            // Varaible initialization
            D = new double[Length];
            // PointsToAvg = Convert.ToInt64(algoSelect.nmPoints4AvgAlgo2.Value);
            //DiffBwAvgs = Convert.ToInt64(algoSelect.nmPointsBwAvgAlgo2.Value);
            //Algo3Threshold = Convert.ToDouble(algoSelect.nmThresholdValue.Value);
            //AvgPitPoint = Convert.ToInt64(algoSelect.nmAlgo3AvgPoint.Value);

            //Calculate Difference array
            /*for (long diff = 0; diff < (Length - 3); diff++)
            {

                D[diff] = y[diff + 3] / 1000 - y[diff] / 1000;

            }*/

            Dnext = new double[Length];

            //Calculate D Column
            long diff1 = 0, k = 1;
            //k = (DiffBwAvgs + PointsToAvg) / 2;
            k = (NoPointBwAvg + NoPointAvg) / 2;
            //while (diff1 < (Length - (DiffBwAvgs + PointsToAvg)))
            while (diff1 < (Length - (NoPointBwAvg + NoPointAvg)))
            {

                double sum1 = 0.0, avg1 = 0.0, avg2 = 0.0, sum2 = 0.0, sum3 = 0.0, avg3 = 0.0;
                //for (long diff2 = diff1; diff2 < (diff1 + PointsToAvg); diff2++)
                for (long diff2 = diff1; diff2 < (diff1 + NoPointAvg); diff2++)
                {
                    //sum1 = sum1 + y[diff2] / 1000;
                    sum1 = sum1 + y[diff2];
                }

                //avg1 = sum1 / PointsToAvg;
                avg1 = sum1 / NoPointAvg;

                //for (long diff3 = (diff1 + DiffBwAvgs); diff3 < (diff1 + DiffBwAvgs + PointsToAvg); diff3++)
                for (long diff3 = (diff1 + NoPointBwAvg); diff3 < (diff1 + NoPointBwAvg + NoPointAvg); diff3++)
                {
                    //sum2 = sum2 + y[diff3] / 1000;
                    sum2 = sum2 + y[diff3];
                }

                //avg2 = sum2 / PointsToAvg;
                avg2 = sum2 / NoPointAvg;

                //Ymax Averaging --- 15-sep-15
                for (long diff4 = (k - NoPointsForYmaxAvg); diff4 <= (k + NoPointsForYmaxAvg); diff4++)
                {
                    //sum2 = sum2 + y[diff3] / 1000;
                    sum3 = sum3 + y[diff4];
                }

                //avg2 = sum2 / PointsToAvg;
                avg3 = sum3 / (2 * NoPointsForYmaxAvg + 1);


                //Apply formula
                Dnext[k] = 1000 * (avg3 - (avg1 + avg2) / 2);
                //Dnext[k] = 1000 * (y[k] - (avg1 + avg2) / 2);
                //Dnext[k] = 1000 * (y[k]- (avg1 + avg2) / 2);

                k++;
                diff1++;

            }


            //Find PitValues
            double FirstPit = 0.0; //changed from Dnext[1] to 0.0 //updated on 24-sep-15
            long PitAt = 1;
            maxav = 0;

            //Array for getting thickness by Algo2
            double[] Algo4thickness = new double[Length];
            long[] Algo4thicknessIndex = new long[Length];
            // array for optional Left middle and Right
            Converted_x = new double[Length];

            //for next pit iteration
            long Next = 1;
            long NextLeft = 1;
            long NextRight = DistanceBwPits + DistanceBwPits * Percentage / 100; // updated on 24-sep-15

            //Find Next Pits
            while (Next < (k - DistanceBwPits))
            {

                //Max Point b/w NextLeft and NextRight should be NextPit
                double pit = Dnext[NextLeft];
                PitAt = NextLeft;
                for (long NextPoint = NextLeft; NextPoint <= NextRight; NextPoint++)
                {
                    if (pit < Dnext[NextPoint])
                    {
                        pit = Dnext[NextPoint];
                        //Next = NextPoint;
                        PitAt = NextPoint - NoCountBackPts;
                    }

                }

                //D column formula has changed  ---- 15-Sep-15
                pit = pit - SubstrateRoughness;

                //ignore pits which have value less than height(threshold)
                if ((HeightEnable == true))
                {
                    //pass
                    if (HeightValue <= pit)
                    {
                        //use substrate roughness
                        Algo4thickness[maxav] = pit;
                        Algo4thicknessIndex[maxav] = PitAt;
                        Converted_x[maxav] = x[PitAt];
                        Next = PitAt;
                        maxav++;
                    }
                    else
                    {
                        Next = NextRight + 1;
                    }
                }
                else
                {
                    //use substrate roughness
                    Algo4thickness[maxav] = pit;
                    Algo4thicknessIndex[maxav] = PitAt;
                    Converted_x[maxav] = x[PitAt];
                    Next = PitAt;
                    maxav++;
                }

                Next = Next + DistanceBwPits;
                NextLeft = Next - DistanceBwPits * Percentage / 100;
                NextRight = Next + DistanceBwPits * Percentage / 100;

            }
           

            // get Range value
            //long range = Convert.ToInt64(algoSelect.nmRange.Value);

            double sum = 0.0;

            // Calculate Range Average
            for (long RangeIndex = 0; RangeIndex < (maxav - 1); RangeIndex++)
            {

                // long left = Algo4thicknessIndex[RangeIndex] + range;
                // long right = Algo4thicknessIndex[RangeIndex+1] - range;
                // Algo4thicknessIndex[maxav] = Next;
                long left = Algo4thicknessIndex[RangeIndex] + Range;
                long right = Algo4thicknessIndex[RangeIndex + 1] - Range;

                // double max = Dnext[left];
                // double min = Dnext[right];
                double max = 0.0;
                double min = 0.0;

                // Find max and min values between the Range
                for (long maxIndex = left; maxIndex < right; maxIndex++)
                {

                    // Find max value
                    if (max < Dnext[maxIndex])
                    {

                        max = Dnext[maxIndex];
                    }

                    // Find min value
                    if (min > Dnext[maxIndex])
                    {
                        min = Dnext[maxIndex];

                    }
                }

                double diff = max - min;
                sum = sum + diff;

            }

            double avg = sum / (maxav - 1);

            // get multiplication factor
            //double factor = Convert.ToDouble(algoSelect.nmFactor.Value);



            //Intialize PitValues array
            thickness = new double[maxav];
            for (long thickIndex = 0; thickIndex < maxav; thickIndex++)
            {

                thickness[thickIndex] = Algo4thickness[thickIndex] + (avg * Factor);

            }


            // Function to find finalThickness
            FindThickness(thickness);

            // display average thicknees of all pits
            //capDispMax.Text = Math.Round(FinalThickness,2).ToString();

            // Function to update PitValues Graph
            DispPitGraph();

        }


       
        private void SurfaceRoughness(double[] x, double[] y)
        {
            
            double[] movingAvg=new double[Length];
            // Averaging Number as input parameter
            long AvgNum = AveragingNum;
            double runningSum = 0.0;


            // Calculate Moving Average
            //if averaging number is greater than Length of data
            if (Convert.ToInt64(AvgNum) >= (Length - 1))
            {
                for (long z = 0; z < (Length - 1); z++)
                {
                    movingAvg[Length] = y[z];
                    // Ny is the array with nonzero values
                    // txtOutput.Text += "moving avg:" + movAvg[z];
                }
            }
            //if averaging number is less than Length of data
            else
            {
                for (long k = 0; k < (Length - 1 - Convert.ToInt64(AvgNum)); k++)
                {

                    for (long m = k; m < (k + Convert.ToInt64(AvgNum)); m++)
                    {
                        runningSum = runningSum + y[m];


                    }

                    //movAvg[k] = runnigSum / 704;
                    movingAvg[k] = runningSum / AvgNum;
                    //txtOutput.Text += "moving avg:" + movAvg[k];
                    runningSum = 0.0;
                }
                Length = Length - Convert.ToInt64(AvgNum);  //set length of moving avg array//this will not allow to zero values in movavg array
            }
        
                        // Offset as input parameter
                        double offset = OffsetValue;
                        double[] offsetArray=new double[Length];

                        //calculate offset array
                        for (long offsetIndex = 0; offsetIndex < Length; offsetIndex++)
                        {
                            offsetArray[offsetIndex] = movingAvg[offsetIndex] - offset;

                        }

                     double[] diffArray = new double[Length];
                     double[] diffArrayAbs = new double[Length];
                     double[] diffArrayPositive = new double[Length];
                    
                    //calculate Difference array
                    for (long diffIndex = 0; diffIndex < Length; diffIndex++)
                    {
                        diffArray[diffIndex] = offsetArray[diffIndex] - y[diffIndex];
                        diffArrayAbs[diffIndex] =Math.Abs(offsetArray[diffIndex] - y[diffIndex]);

                        if ((offsetArray[diffIndex] - y[diffIndex]) > 0)
                        {
                            diffArrayPositive[diffIndex] = offsetArray[diffIndex] - y[diffIndex];
                        }
                        else 
                        {
                            diffArrayPositive[diffIndex] = 0;
                        }
                    }
            
                    double RAsum=0.0,RPsum=0.0;
                    for (long RaIndex = 0; RaIndex < Length; RaIndex++)
                    { 
                        RAsum=RAsum+diffArrayAbs[RaIndex];
                        RPsum = RPsum + diffArrayPositive[RaIndex];
                    }
                    
                     RaAvg = RAsum / Length;
                     RpAvg = (RPsum / Length)*1000;
                   
                   // lblRa.Text =Math.Round(RaAvg,4).ToString();
                   // lblRp.Text=Math.Round(RpAvg,4).ToString();
                    lblLeftStats.Text = "Value of Ra";
                    lblRightStats.Text = "Value of Rp";
                    dispLeftStats.Text = Math.Round(RaAvg, 4).ToString();
                    dispRightStats.Text = Math.Round(RpAvg, 4).ToString();

           // MessageBox.Show(RaAvg.ToString());
           // MessageBox.Show(RpAvg.ToString());


        }


        private void RefreshPaintList()
        {

            cmbPaintType.Items.Clear();
            try
            {

                SQLiteConnection con = new SQLiteConnection("Data Source=ScanDataBase.s3db");
                con.Open();
                SQLiteCommand com = new SQLiteCommand();
                com.Connection = con;

                com.CommandText = "select PaintName,id from tbl_paint_type";
                com.ExecuteNonQuery();
                SQLiteDataReader dr = com.ExecuteReader();
                while (dr.Read())
                {
                    
                    //cmbPaintType.Items.Add(new KeyValuePair<string,int>(dr["PaintName"].ToString(),Convert.ToInt32(dr["id"].ToString())));
                    cmbPaintType.Items.Add(dr["PaintName"].ToString());
                   //cmbPaintType.Items.Add(new ListViewItem(dr["PaintName"].ToString(), dr["id"].ToString()));
                   // cmbPaintType.SelectedIndex = 0;
                }
                dr.Close();
                con.Close();
            }
            catch (SQLiteException ex)
            {
                MessageBox.Show(ex.Message);
            }

          
        }

        private void DispPitGraph()
        {
            //get averaging number
            int averaging = settingsForm.Settings.Fetch<int>("data/scan averaging number");
        
            GraphPane pane1 = zgcPits.GraphPane;
            pane1.CurveList.Clear();
            PointPairList points1 = new PointPairList();
            double av = 0.0;
            
            //Skip dataPoints equal to averaging number
            for (int g = 0; g < maxav; g++)
            {
                // perform moving average
                av += thickness[g];
                 if (g + 1 < averaging) continue;
                 if (g - averaging >= 0) av -= thickness[g - averaging];
                 
                points1.Add(g + 1, micronConverter(av / averaging));
            }
            pane1.AddCurve("pits", points1, Color.Purple);
            zgcPits.Refresh();
            zgcPits.AxisChange();
            zgcPits.Invalidate();


        }

        void AlgoSelect_Load(object sender, EventArgs e)
        {
            algoSelect.grpbxAlgo1.Enabled = true;
            algoSelect.grpbxAlgo2.Enabled = false;
            algoSelect.grpbxAlgo3.Enabled = false;
            algoSelect.grpbxAlgo4.Enabled = false;
        
        }


        void rdAlgo1_checked(object sender, EventArgs e)
        {
            algoSelect.grpbxAlgo1.Enabled = true;
            algoSelect.grpbxAlgo2.Enabled = false;
            algoSelect.grpbxAlgo3.Enabled = false;
            algoSelect.grpbxAlgo4.Enabled = false;
        }

        void rdAlgo2_checked(object sender, EventArgs e)
        {
            algoSelect.grpbxAlgo1.Enabled = false;
            algoSelect.grpbxAlgo2.Enabled = true;
            algoSelect.grpbxAlgo3.Enabled = false;
            algoSelect.grpbxAlgo4.Enabled = false;
        }

        void rdAlgo3_checked(object sender, EventArgs e)
        {
            algoSelect.grpbxAlgo1.Enabled = false;
            algoSelect.grpbxAlgo2.Enabled = false;
            algoSelect.grpbxAlgo3.Enabled = true;
            algoSelect.grpbxAlgo4.Enabled = false;

        }

        void rdAlgo4_checked(object sender, EventArgs e)
        {
            algoSelect.grpbxAlgo1.Enabled = false;
            algoSelect.grpbxAlgo2.Enabled = false;
            algoSelect.grpbxAlgo3.Enabled = false;
            algoSelect.grpbxAlgo4.Enabled = true;

        }



       /* private void calculateThickness(double[] x, double[] y)
        {

            lblTotalPit.Text = "Total No. of pits=0";   
            
            //local variable declaration for calculation        
            //Length = x.Length;
                    B = x;
                    C = y;
                    D = new double[Length];
                    ymaxavg = new double[Length];
                    ymaxIndexavg = new long[Length];

                    // getting difference threshold from input parameter
                    DifferThreshold = Convert.ToDouble(algoSelect.numDifferThreshold.Value);
                    YminAvgPoint = (Convert.ToInt64(algoSelect.numYminAvg.Value))/2;
                    YminPosFactor = Convert.ToInt64(algoSelect.numYminPosFactor.Value);
                    YmaxAvgPoint = (Convert.ToInt64(algoSelect.numYmaxAvg.Value)) / 2;
                    
                    long YmaxTmp=1;
                    YmaxTmpLoc=1;
  
                    //Calculate Difference array
                    for (long diff = 0; diff < (Length - 4); diff++)
                    { 
                
                        D[diff]=C[diff+3]/1000-C[diff]/1000;
            
                    }

                    //Apply Algorithm
                    long i = 0,T=0;
                        maxav = 0;
                        minav = 0;

                        while (i < (Length-40))
                        {

                                // Function to find jump value greater than 0.015 
                                T=FindT(i,D);
                           
                            if(T<(Length-40))
                           {       
                                YmaxTmp = T;

                                // Find max value under 40 values after T
                                        for (long k = T; k <=(T + 40); k++)
                                        {
                                
                                            if (C[k] >C[YmaxTmp])
                                            {
                                                YmaxTmp = k;
                                            }

                                        }

                               
                         
                                    // averaging of values centered at ymaxtmp
                                    double ytmpsum = 0;
                                    for (long l = (YmaxTmp - YmaxAvgPoint); l <= (YmaxTmp + YmaxAvgPoint); l++)
                                    {

                                        ytmpsum = ytmpsum + C[l]/1000;

                                    }

                                    ymaxIndexavg[maxav] = YmaxTmp;
                                    ymaxavg[maxav] = ytmpsum / (2*YmaxAvgPoint+1);
                                    maxav++;
                                
                          }
                            
                            // increment for find next jump(T)
                            i=T+40;

                        }

                        // array declaration for calculating ymin
                        yminIndexavg = new long[maxav+1];
                        thickness = new double[maxav];
                        yminavg = new double[maxav+1];
                        
                        // Function to calculate ymin array    
                        CalculateYmin(C);

                        // calculate thickness array with help of ymaxavg array and yminavg array
                        double ymin1 = 0.0, ymin2 = 0.0, ymin = 0.0;
                            for (long n = 0; n < maxav; n++)
                            {
                                ymin1 = yminavg[n];
                                ymin2 = yminavg[n + 1];
                                ymin = (ymin1 + ymin2) / 2;
                                
                                thickness[n] = 1000*(ymaxavg[n] - ymin);
                               
                        
                            }
                            
                            // function to calculate average thickness of all pits with the help of thickness array
                            FindThickness(thickness);
                        // display average thicknees of all pits
                        //capDispMax.Text =Math.Round(FinalThickness,2).ToString();

                       
            
            // For loop to marking MAX and MIN values on graph
            for (long maxIndex = 0; maxIndex < maxav; maxIndex++)
                        {
                              PointPairList spl1 = new PointPairList
                              { 
                                   { ymaxIndexavg[maxIndex],y[ymaxIndexavg[maxIndex]]}
                                   };
                              
                            PointPairList spl2 = new PointPairList
                            { 
                                 { yminIndexavg[maxIndex],y[yminIndexavg[maxIndex]]}
                                 };

                            GraphPane pane = zgcScanProfile.GraphPane;

                            LineItem highLine = pane.AddCurve("", spl1, Color.Green);
                            LineItem highLine1 = pane.AddCurve("", spl2, Color.Red);
                            highLine.Symbol.IsVisible = true;

                            highLine.Symbol.Type = SymbolType.Circle;
                            highLine.Symbol.Fill.IsVisible = false;
                            highLine.Symbol.Border.Width = 1F;
                            highLine.Symbol.Size = 5F;
                            highLine.Symbol.IsVisible = true;

                            highLine1.Symbol.IsVisible = true;
                            highLine1.Symbol.Type = SymbolType.Circle;
                            highLine1.Symbol.Fill.IsVisible = false;
                            highLine1.Symbol.Border.Width = 1F;
                            highLine1.Symbol.Size = 5F;

                            zgcScanProfile.Refresh();
                            zgcScanProfile.Invalidate();

                        }
           
            // Function to update PitValue Graph
            DispPitGraph();           

            }
        */

            /*   private void CalculateYmin(double[] y)
                {
                   
                   // calculate yminavg array with help of ymaxavg array(mid value b/w two)
                   long i=0;
                   while (i < (maxav-1))
                   { 
                        yminIndexavg[i+1]=ymaxIndexavg[i]+(ymaxIndexavg[i+1]-ymaxIndexavg[i])/YminPosFactor;

                        double ymintmpsum = 0.0;
                        long mstart =0,t=0;
                        long mend = maxav-1;
                            
                            if ((yminIndexavg[i + 1]-YminAvgPoint) <= 0)
                            {
                                mstart = 0;
                            }
                            else
                            {
                                mstart = yminIndexavg[i + 1] - YminAvgPoint;
                            }
                            
                            if ((yminIndexavg[i + 1]+YminAvgPoint) >= ymaxIndexavg[maxav-1])
                            {
                                mend =yminIndexavg[maxav-1];
                            }
                            else
                            {
                                mend = yminIndexavg[i + 1]+YminAvgPoint;
                            }

                        for (long m =mstart; m <=mend; m++)
                        {
                            ymintmpsum = ymintmpsum + y[m]/1000;
                            t++;
                        }

                        yminavg[i + 1] = ymintmpsum/t;
                       i++;
                   }

                   long ytmp = 0;
                    
                    // calculate first ymin
                    ytmp = ymaxIndexavg[0] - (ymaxIndexavg[1] - ymaxIndexavg[0]) / YminPosFactor;


                        if (ytmp > 0)
                        {
                            yminIndexavg[0] = ytmp;
                        }
                        else 
                        {
                            yminIndexavg[0] = 0;
                        }

                        yminavg[0]=y[yminIndexavg[0]]/1000;


                        long ytmp1 =Length;
                    // calculate last ymin
                    ytmp1=ymaxIndexavg[maxav-1]+(ymaxIndexavg[maxav-1]-ymaxIndexavg[maxav-2])/YminPosFactor;

                    if (ytmp1 >= Length-1)
                    {
                        yminIndexavg[maxav] = Length-1;
                    }
                    else
                    {
                        yminIndexavg[maxav] = ytmp1;
                    }
                    yminavg[maxav] = y[yminIndexavg[maxav]]/1000;

               }
        */

        
        private long FindT(long i1,double[] d)
        {
            // Find jump value(index where difference is greater than Threshold)
            long i;
            for (i = i1; i <(Length-40); i++)
            {
                if ((d[i]) >= DifferThreshold)
                {
                    break;
                }
            }

            return i;
        }

        // Average of all thickness
        private void FindThickness(double[] t1)
        {
            double s3 = 0.0;
            //listThickness.Items.Clear();
            txtPits.Text = "";
            //capDispMax.Text = "";
            string pitAverageFormat = "0.00";
          //  string pitStDevFormat = (CurrentUnits == DataUnits.Metric ? "0.00" : "0.000");

            // convert first
          //  dispLeftStats.Text = micronConverter(pitAverages[0]).ToString(pitAverageFormat);
            lblTotalPit.Text = "Total No. of pits="+maxav.ToString();
            for (long n = 0; n<maxav; n++)
            {
                
                txtPits.Text +=micronConverter(t1[n]).ToString(pitAverageFormat) + "\r\n";
                s3 = s3 + t1[n];
            }
            FinalThickness = s3 / maxav;
            
            //enalble refresh button
            algoSelect.btnRefreshReading.Enabled = true;
        }
        
     


        //**********************************//

        #endregion


        #region database

        void btnDeleteAll_Click(object sender, EventArgs e)
        {
            DelId = new long[dbfrm.ScanDataGrid.Rows.Count];
            int NumDel = 0;
            for (int i = 0; i < dbfrm.ScanDataGrid.Rows.Count; i++)
            {
                if (Convert.ToBoolean(dbfrm.ScanDataGrid.Rows[i].Cells["Select"].Value) == true)
                {
                    DelId[NumDel] = Convert.ToInt64(dbfrm.ScanDataGrid.Rows[i].Cells["id"].Value);
                    NumDel++;
                }
            }

            if (NumDel == 0)
            { 
            MessageBox.Show("Please Select atleast one scan");
            return;

            }

            //confirmation for delete scan
            if (MessageBox.Show("Do you want to delete?", "Confirm delete Multiple", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {


                //long row1 = Convert.ToInt64(((DataGridView)sender).Rows[e.RowIndex].Cells[0].Value.ToString());


                try
                {

                    SQLiteConnection con = new SQLiteConnection("Data Source=ScanDataBase.s3db");
                    con.Open();
                    SQLiteCommand com = new SQLiteCommand();
                    com.Connection = con;

                    for (int j = 0; j < (NumDel); j++)
                    {
                        // delete particular scan
                        com.CommandText = "delete from tbl_scan where id=" + DelId[j];
                        com.ExecuteNonQuery();

                        // get all pit values of that scan
                        com.CommandText = "delete from tbl_scan_pits where scan_id=" + DelId[j];
                        com.ExecuteNonQuery();
                    }
                    con.Close();
                }
                catch (SQLiteException ex)
                {
                    MessageBox.Show(ex.Message);
                }

                //Function to load data from the database
                LoadData();


            }



        
        }

        void SelectAll_checked(object sender, EventArgs e)
        {

            
            //dbfrm.chkSelectAll.Checked = !dbfrm.chkSelectAll.Checked;
            if (dbfrm.chkSelectAll.Checked == true)
            {
                //Checked all checkbox
                for (int i = 0; i < dbfrm.ScanDataGrid.Rows.Count - 1; i++)
                {
                    dbfrm.ScanDataGrid.Rows[i].Cells["Select"].Value = true;
                   
                }
                CheckCount = dbfrm.ScanDataGrid.Rows.Count;
                FlagSelectAll = 0;
            }
            else if (dbfrm.chkSelectAll.Checked == false)
            {
                 //if (dbfrm.ScanDataGrid.c)
                //{
                //MessageBox.Show(e.ToString());
                    //unchecked all checkbox
                if (FlagSelectAll != 1)
                {
                    for (int i = 0; i < dbfrm.ScanDataGrid.Rows.Count - 1; i++)
                    {
                        dbfrm.ScanDataGrid.Rows[i].Cells["Select"].Value = false;
                    }
                    CheckCount = 0;
                }
                    //}
            }

        }

        void btnApply_Click(object sender, EventArgs e)
        {
            //CheckCount = 0;
            //MessageBox.Show("inApply: "+CheckCount.ToString());
            // click event for checkbox
           // dbfrm.ScanDataGrid.CellContentClick += new DataGridViewCellEventHandler(GridCheck);
            // if apply averaging or stitching only for 2 or 3 values
           


            if (CheckCount == 2 || CheckCount == 3)
            {
 
                d1=0;
                    for (int d = 0; d < (dbfrm.ScanDataGrid.RowCount - 1); d++)
                    { 
                        if (Convert.ToBoolean(dbfrm.ScanDataGrid.Rows[d].Cells["Select"].Value) == true)
                        {
                            if (dbfrm.ScanDataGrid.Rows[d].Cells[2].Value.ToString() == "Surface Roughness")
                            {
                                //dbfrm.ScanDataGrid.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = false;
                                dbfrm.chkSelectAll.Checked = false;
                                for (int i = 0; i < dbfrm.ScanDataGrid.Rows.Count - 1; i++)
                                {
                                    dbfrm.ScanDataGrid.Rows[i].Cells["Select"].Value = false;
                                }
                                CheckCount = 0;
                                MessageBox.Show("Surface Roughness Scan cannot be used for Stitching or Averaging!");
                                return;
                            }    
                            
                            
                            // store scan id into array 
                           scanId[d1] =Convert.ToInt32(dbfrm.ScanDataGrid.Rows[d].Cells["id"].Value);
                            d1++;
                        }
                  
                    }

                    
                    if (dbfrm.cmbStitchAvg.SelectedIndex == 0)
                    {
                        // Function for stitching
                        stitching();
                    }
                    else if (dbfrm.cmbStitchAvg.SelectedIndex == 1)
                    {
                        // Function for averaging
                        averaging();
                    }
            
           
            }
            else
            {

                MessageBox.Show("Choose 2 or 3 scans only!");
                return;
            }
            
        }

        void btnChooseDb_Click(object sender, EventArgs e)
        {
            dbfrm.folderBrowserDb.ShowDialog();

            if (dbfrm.folderBrowserDb.SelectedPath != "")
            {
                //settingfrm.txtDataFolder.Text = folderBrowser.SelectedPath;
                dbfrm.txtFolderDb.Text = dbfrm.folderBrowserDb.SelectedPath;
            }

            //output.WriteLine("Set data folder to " +settingfrm.txtDataFolder.Text, OutputManager.Level.SUCCESS);
        }

        // For Browse button on Pit DispForm
        void btnPitChooseDb_Click(object sender, EventArgs e)
        {
            PitDispForm.folderBrowserDb.ShowDialog();

            if (PitDispForm.folderBrowserDb.SelectedPath != "")
            {
                //settingfrm.txtDataFolder.Text = folderBrowser.SelectedPath;
                PitDispForm.txtFolderDb.Text = PitDispForm.folderBrowserDb.SelectedPath;
            }

            //output.WriteLine("Set data folder to " +settingfrm.txtDataFolder.Text, OutputManager.Level.SUCCESS);
        }

        // for pit DisplyaForm
        void btnPitExprt_click(object sender, EventArgs e)
        {
            StringBuilder sb = new StringBuilder();
            //string file = System.DateTime.Now + "_Export.csv";
            string filePath = @"" + PitDispForm.txtFolderDb.Text + "\\Export_" +PitDispForm.lblHeading.Text + ".csv";
            string delimiter = ",";

            string[][] output = new string[][]{   
                 };
            string[][] column = new string[][]{   
                 };


            column = new string[][]{  
	                            new string[]{"PitValues"
                                    }
	                            };
            int colen = column.GetLength(0);
            //MessageBox.Show(length.ToString());


            for (int index = 0; index < colen; index++)
            {

                sb.Append(string.Join(delimiter, column[index]));

                // sb.AppendLine();
                // MessageBox.Show(output[index].ToString());
            }
            //sb.AppendLine();

            //MessageBox.Show(dataTable.Rows.Count.ToString());
            //int row=Convert.ToInt32(dataTable.Rows.Count.ToString());
            //MessageBox.Show(row.ToString());
           // MessageBox.Show(PitDispForm.txtData.Lines.Length.ToString());
           // for (int i = 0; i <PitDispForm.txtData.Lines.Length; i++)
            //while(PitDispForm.txtData.Text!=null)
            //{
                output = new string[][]{  
	                           new string[]{
                                PitDispForm.txtData.Text
                                    }
	                            };
                // MessageBox.Show(dataTable.Rows[i]["Results"].ToString());
                sb.AppendLine();

                int length = output.GetLength(0);
                //MessageBox.Show(length.ToString());


                for (int index = 0; index < length; index++)
                {

                    sb.Append(string.Join(delimiter, output[index]));

                    // sb.AppendLine();
                    // MessageBox.Show(output[index].ToString());
                }

                //   sb.AppendLine(string.Join(delimiter, output[i]));

            //}
            // MessageBox.Show(sb.ToString());   
            File.WriteAllText(filePath, sb.ToString());
            MessageBox.Show("PitValues Exported At location:" + PitDispForm.txtFolderDb.Text);

        
        
        }

        void btnExprt_click(object sender, EventArgs e)
        {
            StringBuilder sb = new StringBuilder();
            //string file = System.DateTime.Now + "_Export.csv";
            string filePath = @"" + dbfrm.txtFolderDb.Text + "\\Export_" + DateTime.Now.ToString("dd-MM-yyyy_hh-mm-ss") + ".csv";
            string delimiter = ",";

            string[][] output = new string[][]{   
                 };
            string[][] column = new string[][]{   
                 };


            column = new string[][]{  
	                            new string[]{"id","date","comment","left","middle","right"
                                    }
	                            };
            int colen = column.GetLength(0);
            //MessageBox.Show(length.ToString());


            for (int index = 0; index < colen; index++)
            {

                sb.Append(string.Join(delimiter, column[index]));

                // sb.AppendLine();
                // MessageBox.Show(output[index].ToString());
            }
            //sb.AppendLine();

            //MessageBox.Show(dataTable.Rows.Count.ToString());
            //int row=Convert.ToInt32(dataTable.Rows.Count.ToString());
            //MessageBox.Show(row.ToString());

            for (int i = 0; i < Convert.ToInt32(dataTable.Rows.Count); i++)
            {
                output = new string[][]{  
	                           new string[]{
                                dbfrm.ScanDataGrid.Rows[i].Cells[0].Value.ToString(),
                                dbfrm.ScanDataGrid.Rows[i].Cells[1].Value.ToString(),
                                dbfrm.ScanDataGrid.Rows[i].Cells[2].Value.ToString(),
                                dbfrm.ScanDataGrid.Rows[i].Cells[3].Value.ToString(),
                                dbfrm.ScanDataGrid.Rows[i].Cells[4].Value.ToString(),
                                dbfrm.ScanDataGrid.Rows[i].Cells[5].Value.ToString()
                               
                                    }
	                            };
                // MessageBox.Show(dataTable.Rows[i]["Results"].ToString());
                sb.AppendLine();

                int length = output.GetLength(0);
                //MessageBox.Show(length.ToString());


                for (int index = 0; index < length; index++)
                {

                    sb.Append(string.Join(delimiter, output[index]));

                    // sb.AppendLine();
                    // MessageBox.Show(output[index].ToString());
                }

                //   sb.AppendLine(string.Join(delimiter, output[i]));

            }
            // MessageBox.Show(sb.ToString());   
            File.WriteAllText(filePath, sb.ToString());
            MessageBox.Show("Database Exported At location:" + dbfrm.txtFolderDb.Text);


        }



        void databaseFrm_Form_Load(object sender, EventArgs e)
        {


            try
            {
                dbfrm.cmbStitchAvg.SelectedIndex = 0;
                dbfrm.ScanDataGrid.AllowUserToAddRows = false;
                dbfrm.ScanDataGrid.DataSource = new DataView(LoadData());

                // For Delete button
                DataGridViewButtonColumn btndel = new DataGridViewButtonColumn();
               
                btndel.HeaderText = "Delete";
                btndel.Name = "Delete";
                btndel.Text = "Delete";
                btndel.UseColumnTextForButtonValue = true;
                dbfrm.ScanDataGrid.Columns.Add(btndel);

                // For pitvalue button
                DataGridViewButtonColumn btnpit = new DataGridViewButtonColumn();
                btnpit.HeaderText = "PitValues";
                btnpit.Name = "pitvalues";
                btnpit.Text = "PitValues";
                btnpit.UseColumnTextForButtonValue = true;
                dbfrm.ScanDataGrid.Columns.Add(btnpit);

                // Click event of button
                dbfrm.ScanDataGrid.CellContentClick += new DataGridViewCellEventHandler(GridBtnClick);

                // For Checkbox respective of each scan
                DataGridViewCheckBoxColumn chk = new DataGridViewCheckBoxColumn();
                chk.HeaderText ="Select";
                chk.Name ="Select";
                dbfrm.ScanDataGrid.Columns.Add(chk);
                
                //initialize all checkbox as not selected
                dbfrm.chkSelectAll.Checked = false;
                for (int i = 0; i < dbfrm.ScanDataGrid.Rows.Count - 1; i++)
                {
                    dbfrm.ScanDataGrid.Rows[i].Cells["Select"].Value = false;
                }
                CheckCount = 0;
                
            
            }
            catch (SQLiteException ex)
            {
                MessageBox.Show(ex.Message);
            }


        }


        #region stitch and average

        private void Check_stitch_avg()
        {
            // insert scan into tmp database
            InsertToTmpDb();
            //store Scan IDs
            scanId[d1] = CurrentScanId;
            d1++;

            // if Number Scans are completed then process them
            if (d1 == Convert.ToInt32(nmNoScans.Value))
            {
                    //for stitching
                    if (rdStitchBtn.Checked == true)
                    {
                        // Apply Stitching 
                        stitchingMain();

                    }
                    // for averaging
                    else if (rdAverageBtn.Checked == true)
                    {

                        // Apply Averaging
                        averagingMain();

                    }
                    
                    d1 = 0;
                    btnRun.Enabled = false;
                    rtxtboxStatus.Text = "All Scan Process completed!";
                    
                    // Delete Data from Tmp DATABASE
                    FlushTmpDb();
                    
                    // Display updated PitValues Graph
                    DispPitGraph();
                    
                    //Reset Controls
                    txtPits.Text = "";
                    lblTotalPit.Text = "Total No. of pits=" + thickness.Length.ToString();

                    //Display all pitsvalues
                    double s3 = 0.0;
                    for (long n = 0; n < thickness.Length; n++)
                    {

                        txtPits.Text += Math.Round(thickness[n], 2) + "\r\n";
                        s3 = s3 + thickness[n];
                    }
                    
                    // Update Left Middle Right
                    UpdateLeftMiddleRight();

                    //Display Left Middle Right
                    updatePitDisplays();
                                
            }
            else if (d1 == 1)
            {
                btnRun.Enabled = true;
                rtxtboxStatus.Text = "Scan1 completed! click 'Measure' for next";
            
            }
            else if (d1 == 2)
            {
                btnRun.Enabled = true;
                rtxtboxStatus.Text = "Scan2 completed! click 'Measure' for next";
            }
        
        }

        void rdNormal_checked(object sender, EventArgs e)
        {
            nmNoScans.Enabled = false;

        }
        void rdStitch_checked(object sender, EventArgs e)
        {
            nmNoScans.Enabled = true;

        }
        void rdAverage_checked(object sender, EventArgs e)
        {
            nmNoScans.Enabled = true;

        }

        private void FlushTmpDb()
        {

            try
            {

                SQLiteConnection con = new SQLiteConnection("Data Source=ScanDataBase.s3db");
                con.Open();
                SQLiteCommand com = new SQLiteCommand();
                com.Connection = con;

                // delete particular scan
                com.CommandText = "delete from tbl_tmp_scan";
                com.ExecuteNonQuery();

                // get all pit values of that scan
                com.CommandText = "delete from tbl_tmp_scan_pits";
                com.ExecuteNonQuery();
                con.Close();
            }
            catch (SQLiteException ex)
            {
                MessageBox.Show(ex.Message);
            }
        
        }

        private void stitchingMain()
        {
            // initialize varaibles
            double pitsum = 0.0;
            long pitsumCount = 0;
            //string stitchScan = "";
            stitchThickness = new double[10000];

            for (int s = 0; s < d1; s++)
            {
                //stitchScan += scanId[s] + ",";

                try
                {

                    SQLiteConnection con = new SQLiteConnection("Data Source=ScanDataBase.s3db");
                    con.Open();
                    SQLiteCommand com = new SQLiteCommand();
                    com.Connection = con;

                    // get all pit values of scan
                    com.CommandText = "select * from tbl_tmp_scan_pits where scan_id=" + Convert.ToInt64(scanId[s]);
                    com.ExecuteNonQuery();
                    SQLiteDataReader dr = com.ExecuteReader();
                    while (dr.Read())
                    {
                        stitchThickness[pitsumCount] = Convert.ToDouble(dr["pit_value"]);
                        pitsum += Convert.ToDouble(dr["pit_value"]);
                        pitsumCount++;
                    }
                    dr.Close();
                    con.Close();

                }
                catch (SQLiteException ex)
                {
                    MessageBox.Show(ex.Message);
                }



            }

            // get stitched thickness
            FinalThickness = Convert.ToDouble(pitsum / pitsumCount);
            thickness = new double[pitsumCount];
            //get stitched pits
            for (long st = 0; st < pitsumCount; st++)
            {
                thickness[st] = stitchThickness[st];
            }
            maxav = pitsumCount;
            AlgoType = d1.ToString()+" Stitch Scans";

            //insert new scan into database
            InsertToDb();
            // update data from database
            //LoadData();
            CheckCount = 0;
            // click event for checkbox
            //dbfrm.ScanDataGrid.CellContentClick += new DataGridViewCellEventHandler(GridCheck);
        }


        private void stitching()
        {
            // initialize varaibles
            double pitsum = 0.0;
            long pitsumCount = 0;
            string stitchScan = "";
            stitchThickness = new double[10000];
            
            for (int s = 0; s < d1; s++)
            {
                stitchScan += scanId[s]+"-";
                
                try
                {
                    
                    SQLiteConnection con = new SQLiteConnection("Data Source=ScanDataBase.s3db");
                    con.Open();
                    SQLiteCommand com = new SQLiteCommand();
                    com.Connection = con;

                    // get all pit values of scan
                    com.CommandText = "select * from tbl_scan_pits where scan_id="+Convert.ToInt64(scanId[s]);
                    com.ExecuteNonQuery();
                    SQLiteDataReader dr = com.ExecuteReader();
                    while (dr.Read())
                    {
                        stitchThickness[pitsumCount]=Convert.ToDouble(dr["pit_value"]);
                        pitsum +=Convert.ToDouble(dr["pit_value"]);
                        pitsumCount++;
                    }
                    dr.Close();
                    con.Close();

                }
                catch (SQLiteException ex)
                {
                    MessageBox.Show(ex.Message);
                }

                
            
            }

            // get stitched thickness
            FinalThickness =Convert.ToDouble(pitsum / pitsumCount);
            
            thickness=new double[pitsumCount];
            //get stitched pits
            for (long st = 0; st < pitsumCount; st++)
            {
                thickness[st] = stitchThickness[st];
            }
            maxav = pitsumCount;
            
            //update left right middle
            UpdateLeftMiddleRight();

            AlgoType = "Stitching of Scan: "+stitchScan;
            
            //insert new scan into database
            InsertToDb();
            // update data from database
            LoadData();
            CheckCount = 0;
            // click event for checkbox
            //dbfrm.ScanDataGrid.CellContentClick += new DataGridViewCellEventHandler(GridCheck);
        }

        private void averagingMain()
        {

            long pitsumCount = 0;
           // string stitchScan = "";
            long[] PitCountPerScan = new long[3];
            double[][] JaggedPits = new double[3][];

            //getting selected scan and their pits
            for (int s = 0; s < d1; s++)
            {
               // stitchScan += scanId[s] + ",";
                try
                {

                    SQLiteConnection con = new SQLiteConnection("Data Source=ScanDataBase.s3db");
                    con.Open();
                    SQLiteCommand com = new SQLiteCommand();
                    com.Connection = con;

                    // get all pit values of scan
                    com.CommandText = "select * from tbl_tmp_scan_pits where scan_id=" + Convert.ToInt64(scanId[s]);
                    com.ExecuteNonQuery();
                    SQLiteDataReader dr = com.ExecuteReader();
                    JaggedPits[s] = new double[10000];
                    while (dr.Read())
                    {

                        // get pitsValues per scan
                        JaggedPits[s][pitsumCount] = Convert.ToDouble(dr["pit_value"]);
                        pitsumCount++;
                    }
                    dr.Close();
                    con.Close();

                }
                catch (SQLiteException ex)
                {
                    MessageBox.Show(ex.Message);
                }

                PitCountPerScan[s] = pitsumCount;
                pitsumCount = 0;

            }

            // Find scan with minimum number of pits
            long minPitCountperScan = PitCountPerScan[0];
            for (long sc = 0; sc < d1; sc++)
            {
                if (minPitCountperScan > PitCountPerScan[sc])
                {
                    minPitCountperScan = PitCountPerScan[sc];
                }

            }


            // Find average of individual pit of different scans
            thickness = new double[minPitCountperScan];
            for (long sg = 0; sg < minPitCountperScan; sg++)
            {
                for (long sg1 = 0; sg1 < d1; sg1++)
                {
                    thickness[sg] += JaggedPits[sg1][sg];

                }
                thickness[sg] = thickness[sg] / d1;
            }


            //find finalThickness
            double thickSum = 0.0;
            for (long st = 0; st < minPitCountperScan; st++)
            {
                thickSum += thickness[st];
            }

            // calulate averaged thickness
            FinalThickness = Convert.ToDouble(thickSum / minPitCountperScan);
            maxav = minPitCountperScan;
            AlgoType = d1.ToString()+" Averaged Scans";

            // insert new averaged scan into DB
            InsertToDb();
            // update data from database
            //LoadData();
            CheckCount = 0;
            // click event for checkbox
            //dbfrm.ScanDataGrid.CellContentClick += new DataGridViewCellEventHandler(GridCheck);




        }

        
        private void averaging()
        {
            
            long pitsumCount = 0;
            string stitchScan = "";
            long[] PitCountPerScan = new long[3];
            double[][] JaggedPits = new double[3][];
            
            //getting selected scan and their pits
            for (int s = 0; s < d1; s++)
            {
                stitchScan += scanId[s] + "-";
                try
                {

                    SQLiteConnection con = new SQLiteConnection("Data Source=ScanDataBase.s3db");
                    con.Open();
                    SQLiteCommand com = new SQLiteCommand();
                    com.Connection = con;

                    // get all pit values of scan
                    com.CommandText = "select * from tbl_scan_pits where scan_id=" + Convert.ToInt64(scanId[s]);
                    com.ExecuteNonQuery();
                    SQLiteDataReader dr = com.ExecuteReader();
                    JaggedPits[s] = new double[10000]; 
                    while (dr.Read())
                    {
                       
                        // get pitsValues per scan
                        JaggedPits[s][pitsumCount] = Convert.ToDouble(dr["pit_value"]);
                        pitsumCount++;
                    }
                    dr.Close();
                    con.Close();

                }
                catch (SQLiteException ex)
                {
                    MessageBox.Show(ex.Message);
                }
                
                PitCountPerScan[s] = pitsumCount;
                pitsumCount = 0;

            }

            // Find scan with minimum number of pits
            long minPitCountperScan =PitCountPerScan[0];
            for (long sc = 0; sc < d1; sc++)
            {
                if (minPitCountperScan > PitCountPerScan[sc])
                { 
                    minPitCountperScan=PitCountPerScan[sc];
                }

            }


            // Find average of individual pit of different scans
            thickness=new double[minPitCountperScan];
            for (long sg = 0; sg < minPitCountperScan; sg++)
            {
                for (long sg1 = 0; sg1 < d1; sg1++)
                { 
                    thickness[sg] +=JaggedPits[sg1][sg];
                
                }
                thickness[sg] = thickness[sg] / d1;
            }

            
            //find finalThickness
            double thickSum = 0.0;
            for (long st = 0; st < minPitCountperScan; st++)
            {
                thickSum += thickness[st];
            }

            // calulate averaged thickness
            FinalThickness = Convert.ToDouble(thickSum/minPitCountperScan);
            maxav =minPitCountperScan;

            //update left middle and right values
            UpdateLeftMiddleRight();

            AlgoType = "Averaging of Scan: " + stitchScan;
            

            // insert new averaged scan into DB
            InsertToDb();
            // update data from database
            LoadData();
            CheckCount = 0;
            // click event for checkbox
           // dbfrm.ScanDataGrid.CellContentClick += new DataGridViewCellEventHandler(GridCheck);

           


        }

        #endregion


        private void GridCheck(object sender, DataGridViewCellEventArgs e)
        {
            //CheckCount = 0;
            //MessageBox.Show(CheckCount.ToString());

          

            //for checkbox in datagridview cell
            if (e.RowIndex >= 0 && e.RowIndex != (dbfrm.ScanDataGrid.RowCount - 1) && ((DataGridView)sender).Columns[e.ColumnIndex].HeaderText == "Select")
            {
                
                if (Convert.ToBoolean(dbfrm.ScanDataGrid.Rows[e.RowIndex].Cells[e.ColumnIndex].Value) == false)
                {
                    dbfrm.ScanDataGrid.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = true;
                    //MessageBox.Show(CheckCount.ToString());
                    CheckCount++;
                }
                else
                {
                    dbfrm.ScanDataGrid.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = false;
                    if(CheckCount!=0)
                            CheckCount--;
                }
              //  MessageBox.Show("inGridCheck: " + CheckCount.ToString());
            }
            
            //MessageBox.Show(CheckCount.ToString());
            if (CheckCount == dbfrm.ScanDataGrid.Rows.Count)
            {
                dbfrm.chkSelectAll.Checked = true;

                
            }
            else if(CheckCount != dbfrm.ScanDataGrid.Rows.Count)
            {
                FlagSelectAll = 1;
                dbfrm.chkSelectAll.Checked = false;
            }
            //MessageBox.Show(CheckCount.ToString());
        }

        private void GridBtnClick(object sender, DataGridViewCellEventArgs e)
        {


            //for delete button in datagridview cell
            if (e.RowIndex >= 0 && e.RowIndex != (dbfrm.ScanDataGrid.RowCount - 1) && ((DataGridView)sender).Columns[e.ColumnIndex].HeaderText == "Delete")
            {

                //confirmation for delete scan
                if (MessageBox.Show("Do you want to delete?", "Confirm delete", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {


                    long row1 = Convert.ToInt64(((DataGridView)sender).Rows[e.RowIndex].Cells[0].Value.ToString());


                    try
                    {
                      
                        SQLiteConnection con = new SQLiteConnection("Data Source=ScanDataBase.s3db");
                        con.Open();
                        SQLiteCommand com = new SQLiteCommand();
                        com.Connection = con;

                        // delete particular scan
                        com.CommandText = "delete from tbl_scan where id=" + row1;
                        com.ExecuteNonQuery();

                        // get all pit values of that scan
                        com.CommandText = "delete from tbl_scan_pits where scan_id=" + row1;
                        com.ExecuteNonQuery();
                        con.Close();
                    }
                    catch (SQLiteException ex)
                    {
                        MessageBox.Show(ex.Message);
                    }

                    //Function to load data from the database
                    LoadData();


                }

            }

            //for pitvaalue button in datagridview
            else if (e.RowIndex >= 0 && e.RowIndex != (dbfrm.ScanDataGrid.RowCount - 1) && ((DataGridView)sender).Columns[e.ColumnIndex].HeaderText == "PitValues")
            {

                if (((DataGridView)sender).Rows[e.RowIndex].Cells[2].Value.ToString() == "Surface Roughness")
                {

                    MessageBox.Show("No pit Found!");
                    return;
                
                }
                
                //fetches 
                long row2 = Convert.ToInt64(((DataGridView)sender).Rows[e.RowIndex].Cells[0].Value.ToString());
               // PitDisplayForm PitDispForm = new PitDisplayForm();

                //fetches 
                try
                {
                    PitDispForm.txtData.Text = "";
                    SQLiteConnection con = new SQLiteConnection("Data Source=ScanDataBase.s3db");
                    con.Open();
                    SQLiteCommand com = new SQLiteCommand();
                    com.Connection = con;

                    // get all pitvalues for particular scan
                    com.CommandText = "select * from tbl_scan_pits where scan_id=" + row2;
                    com.ExecuteNonQuery();
                    PitDispForm.lblHeading.Text = "ScanId= " + row2.ToString();
                    PitDispForm.Heading = "ScanId= " + row2.ToString();
                    SQLiteDataReader dr = com.ExecuteReader();
                    
                    //display pitValues in seprate form
                    while (dr.Read())
                    {
                        
                        PitDispForm.txtData.Text += dr["pit_value"].ToString() + "\r\n";
                        
                    }
                    dr.Close();

                    con.Close();
                }
                catch (SQLiteException ex)
                {
                    MessageBox.Show(ex.Message);
                }


                PitDispForm.Show();

            }

        }

        public DataTable LoadData()
        {

            SQLiteConnection cnn = new SQLiteConnection("Data Source=ScanDataBase.s3db");

            // command to get data
            SQLiteCommand command = cnn.CreateCommand();
            command.CommandText = "SELECT * FROM tbl_scan order by id desc";

            // Create the data adapter using our select command
            SQLiteDataAdapter resultsDataAdapter = new SQLiteDataAdapter(command);

            // The command builder will take care of our update, insert, deletion commands
            SQLiteCommandBuilder commandBuilder = new SQLiteCommandBuilder(resultsDataAdapter);

            // Create a dataset and fill it with our records
            dataTable.Clear();
            resultsDataAdapter.Fill(dataTable);

            return dataTable;
        }

        //Insert Scan into DataBase
        public void InsertToTmpDb()
        {

            try
            {

                SQLiteConnection con = new SQLiteConnection("Data Source=ScanDataBase.s3db");
                con.Open();
                SQLiteCommand com = new SQLiteCommand();
                com.Connection = con;
                // MessageBox.Show(System.DateTime.Now.ToString()); 
                com.CommandText = "insert into tbl_tmp_scan(date,algorithm,left,middle,right) values('" + System.DateTime.Now + "','" + AlgoType + "','" + Math.Round(pitAverages[0],2) + "','" + Math.Round(pitAverages[1],2) + "','" + Math.Round(pitAverages[2],2) + "')";

                com.ExecuteNonQuery();

                //fetch scan id
                com.CommandText = "select id from tbl_tmp_scan order by id desc limit 1";
                com.ExecuteNonQuery();
                SQLiteDataReader dr = com.ExecuteReader();
                while (dr.Read())
                {
                    CurrentScanId = Convert.ToInt64(dr["id"]);
                }
                dr.Close();


                // insert pits into database
                for (long i = 0; i < maxav; i++)
                {
                    com.CommandText = "insert into tbl_tmp_scan_pits(scan_id,pit_value) values('" + CurrentScanId + "','" + thickness[i] + "')";
                    com.ExecuteNonQuery();
                }

                con.Close();
            }
            catch (SQLiteException ex)
            {
                MessageBox.Show(ex.Message);
            }



        }
        int counter = 0;
        DateTime todays_date;
        //Insert Scan into DataBase
        public void InsertToDb()
        {

            try
            {

                SQLiteConnection con = new SQLiteConnection("Data Source=ScanDataBase.s3db");
                con.Open();
                SQLiteCommand com = new SQLiteCommand();
                com.Connection = con;
                // MessageBox.Show(System.DateTime.Now.ToString()); 
                com.CommandText = "insert into tbl_scan(date,algorithm,left,middle,right,ra,rp) values('" + System.DateTime.Now + "','" + AlgoType + "','" + Math.Round(pitAverages[0], 2) + "','" + Math.Round(pitAverages[1], 2) + "','" + Math.Round(pitAverages[2], 2) + "','" + Math.Round(RaAvg, 2) + "','" + Math.Round(RpAvg, 2) + "')";

                com.ExecuteNonQuery();

                //fetch scan id
                com.CommandText = "select id from tbl_scan order by id desc limit 1";
                com.ExecuteNonQuery();
                SQLiteDataReader dr = com.ExecuteReader();
                while (dr.Read())
                {
                    CurrentScanId = Convert.ToInt64(dr["id"]);
                }
                dr.Close();

                //MessageBox.Show(CurrentScanId.ToString());

                // insert pits into database
               // for (long i = 0; i < maxav; i++)
               // {
                    //com.CommandText = "insert into tbl_scan_pits(scan_id,pit_value) values('" + CurrentScanId + "','" + thickness[i] + "')";
                    //com.ExecuteNonQuery();
               // }

               
                //for the first time make the maximum rows required in the first column and hide it
                if (counter == 0)
                {
                    DataGridViewTextBoxColumn column = new DataGridViewTextBoxColumn();
                    column.Name = "id" + counter;
                    column.HeaderText = "";
                    column.DefaultCellStyle.WrapMode = DataGridViewTriState.True;

                    pitsVal.pitsValue.Columns.Add(column);
                    //hiding the empty column
                    pitsVal.pitsValue.Columns["id"+counter].Visible=false;

                    //making the maximum rows required
                    for (int i = 0; i < 50; i++)
                    {
                        int rowIndex = pitsVal.pitsValue.Rows.Add();

                        //Obtain a reference to the newly created DataGridViewRow 
                        var row = pitsVal.pitsValue.Rows[rowIndex];

                        row.Cells["id"+counter].Value = "";
                       // row.Cells["id"+counter].Visible = false;
                       // pitsVal.pitsValue.Rows.Add(row);
                        //pitsVal.pitsValue.Rows[i].Cells[0].Value="";

                    }
                    counter++;
                }
                //if next day arrived.,remove all the todays_date!=datetime.today counter==3
                if (todays_date!=DateTime.Today)
                {
                    //export all the old data into a csv before erasing the data.
                    pitsVal.export_data_csv();
                    todays_date = DateTime.Today;
                    //remove the previously present data into the datagrid view in the next day
                    for (int i = counter-1; i >0;i--)
                    {
                        pitsVal.pitsValue.Columns.RemoveAt(i);
                    }
                        counter=1;
                }
                    pitsVal.pitsValue.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
                    //create rows to store date,scanId,num_of_pits and pits value text in all column    
                    DataGridViewTextBoxColumn column1 = new DataGridViewTextBoxColumn();
                    column1.Name = "id" + counter;
                    column1.HeaderText = "";

                    column1.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
                    column1.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

                    pitsVal.pitsValue.Columns.Add(column1);
                    maxav = 11; //just to test.
                    //adding rows to store date,scan id,number of pits and pits values
                    pitsVal.pitsValue.Rows[0].Cells[counter].Value = System.DateTime.Now.ToString();
                    pitsVal.pitsValue.Rows[1].Cells[counter].Value = CurrentScanId;
                    pitsVal.pitsValue.Rows[2].Cells[counter].Value = "# of Pits=" + maxav;
                    pitsVal.pitsValue.Rows[3].Cells[counter].Value = "Pits Values";

                    //adding rows to store pits values
                    
                    for (int i = 0; i < maxav; i++)
                    {

                        // pitsVal.pitsValue.Rows[i+4].Cells[counter].Value = thickness[i];
                        pitsVal.pitsValue.Rows[i + 4].Cells[counter].Value = i;
                    }
                    //table.Rows.Add(row);

                    counter++;

                    con.Close();
               
            }
            catch (SQLiteException ex)
            {
                MessageBox.Show(ex.Message);
            }

            btnSave.Enabled = true;

        }

        
        
        #endregion

        private void outerContainer_Panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void capDispMax_Load(object sender, EventArgs e)
        {

        }
        
        //code by shubhendu
        private void btnClearGraph_Click(object sender, EventArgs e)
        {
            //zgcScanProfile.GraphPane.CurveList.Clear();
            //zgcScanProfile.Refresh();
            //btnBrowse.Enabled = true;

        }

        private void button1_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
            string file=openFileDialog1.FileName;
            //listThickness.Items.Clear();
            txtPits.Text = "";
            //capDispMax.Text = "";
            zgcScanProfile.GraphPane.CurveList.Clear();
            zgcScanProfile.Refresh();
            LoadScan(file);
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        
       

        private void btnRefreshReading_Click(object sender, EventArgs e)
        {
            //BaseScanBrowserForm baseScan = new BaseScanBrowserForm();

            LoadScan(FileNameRefresh);
        
        }

        private void btnAlgo_Click(object sender, EventArgs e)
        {
            algoSelect.Show();
        }
        
        void btnRefresh_Click(object sender, EventArgs e)
        {
            //Inserted = 0;
            LoadScan(FileNameRefresh);

        }

        private void DFM_Form_simple_Load(object sender, EventArgs e)
        {
            
        }

        private void btnDatabase_Click(object sender, EventArgs e)
        {
            //this.Opacity = 0;
            dbfrm.Opacity = 0;
            //dbfrm.Show();
            
            if (db == 0)
            {
                
                dbfrm.Show();
                db++;
            }
            else
            {
                //databasefrm.datagrid.DataSource = null;
                dbfrm.ScanDataGrid.DataSource = null;
                dbfrm.ScanDataGrid.Rows.Clear();
                dbfrm.ScanDataGrid.Columns.Clear();
                databaseFrm_Form_Load(sender, e);
                dbfrm.Show();
            }
            dbfrm.tmrFade.Enabled = true;
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void Functions_Enter(object sender, EventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }

        private void btnClear_Click_1(object sender, EventArgs e)
        {

        }

        private void btnConnect_Click_1(object sender, EventArgs e)
        {

        }

        private void lagTimeTimer_Tick(object sender, EventArgs e)
        {
            

            if (lag > 0)
            {
                //capDispMax.Text = lag.ToString();
                lag--;

            }
            else
            {
                //capDispMax.Text = "Starting...";
                lagTimeTimer.Stop();
                lagTimeFinish_RunStart();
            
            }

        }

        private void zgcScanProfile_Load(object sender, EventArgs e)
        {

        }

        private void btnCopyPit_Click_1(object sender, EventArgs e)
        {
            try
            {
                string s1 = "";
                foreach (object item in txtPits.Lines) s1 += item.ToString() + "\r\n";
                Clipboard.SetText(s1);
                btnCopyPit.Enabled = false;
                btnCopyPit.Text = "Copied";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);

            }
        }

        private void cmbPaintType_SelectedIndexChanged(object sender, EventArgs e)
        {
           // MessageBox.Show(FileNameRefresh.ToString());
            if (Inserted == 0)
            {
                Inserted = 0;
            }
            else if (Inserted == 1)
            {
                if (FileNameRefresh == "")
                {
                    MessageBox.Show("Please Select the Scan File first!");
                    return;
                }

                // load scan on change of paint Type.
                LoadScan(FileNameRefresh);
            }
        }

        private void tmrFade_Elapsed(object sender, ElapsedEventArgs e)
        {
            this.Opacity += 0.10;
            if (this.Opacity >= .95)
            {
                this.Opacity = 1;
                tmrFade.Enabled = false;
            }
        }

        private void lblMode_Click(object sender, EventArgs e)
        {

        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                SQLiteConnection con = new SQLiteConnection("Data Source=ScanDataBase.s3db");
                con.Open();
                SQLiteCommand com = new SQLiteCommand();
                com.Connection = con;
                // MessageBox.Show(System.DateTime.Now.ToString()); 
                com.CommandText = "update tbl_scan set comment='"+rtxtCommnt.Text+"' where id="+CurrentScanId+"";

                com.ExecuteNonQuery();

               

                con.Close();
            }
            catch (SQLiteException ex)
            {
                MessageBox.Show(ex.Message);
            }

            rtxtCommnt.Text = "";

            btnSave.Enabled = false;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            // Show the dialog and get result.
            DialogResult result = openFileDialog2.ShowDialog();

            if (result == DialogResult.OK) // Test result.
            {

                string file = openFileDialog2.FileName;
                textBox1.Text = file;
                //  richTextBox1.Text = "Plotting...";
                try
                {

                    string filePath = textBox1.Text;//getting file path


                    StreamReader streamReader = new StreamReader(File.OpenRead(filePath));
                    StringBuilder sb = new StringBuilder();

                    string[] totalData = new string[100];
                    totalData = streamReader.ReadLine().Split(',');

                    double[] x = new double[100000];
                    double[] y=new double[100000];
                    long i=0;
                    TimerCheck = settingsForm.Settings.Fetch<bool>("scan/timing/Timer Mode");
                    AutoCheck = settingsForm.Settings.Fetch<bool>("scan/timing/Auto Detect");

                    while (!streamReader.EndOfStream)//insert into datagrid source
                    {
                        totalData = streamReader.ReadLine().Split(',');
                        x[i] = Convert.ToDouble(totalData[0]);
                        y[i] = Convert.ToDouble(totalData[1]);
                        i++;

                    }
                        
                        /****code for autosample *************************************/
                        double lowerRange = settingsForm.Settings.Fetch<double>("scan/timing/Lower Range");
                        //double lowerRange= settingsForm.Settings.Fetch<double>("timing/Lower Range");
                        double higherRange = settingsForm.Settings.Fetch<double>("scan/timing/Higher Range");
                        int EdgePoints = settingsForm.Settings.Fetch<int>("scan/timing/Edge Points");
                        bool LikePVS = settingsForm.Settings.Fetch<bool>("scan/timing/Like PVS");

                        // varaible that helping to find start and end
                        long Threshold = EdgePoints, count;
                            readingCount = 0;

                        // read data
                        // SensorReading[] readings;

                        //list for storing copy the readings from 'readings'
                       // List<SensorReading> resultReading = new List<SensorReading>();
                     resultReading=new double[100000];

                     long k = 0, start = 0;
                     if (LikePVS == false)
                     {
                                     for (long j = 0; j <= i; j++)
                                     {
                                         if ((lowerRange <= y[j]) && (y[j] <= higherRange))
                                         {
                                             start = j;
                                             break;
                                         }

                                     }

                                     for (long startfound = start + EdgePoints; startfound < i; startfound++)
                                     {
                                         if ((lowerRange > y[startfound]) || (y[startfound] > higherRange))
                                         {
                                             break;
                                         }

                                         resultReading[readingCount] = y[startfound];

                                         readingCount++;

                                     }
                                     readingCount = readingCount - EdgePoints;

                     }
                     else if(LikePVS==true) 
                     {
                        // loop for getting proper readings (for not to skip any of the reading)
                       for (int j = 0; j < 2; j++)
                        {
                            //to serach for new start
                            count = 0;

                            //take readings
                           // SensorReading[] readings = settingsForm.Sensor.TakeReadings();


                            // errors indicate out of range
                           // foreach (SensorReading r in readings)
                            for (long readingIndex = 0; readingIndex < i;readingIndex++ )
                            {

                                // first time start and store readings
                                if (j == 0)
                                {
                                    //find start
                                    if (count >= Threshold)
                                    {
                                        // copy readings in seperate list
                                        //resultReading.Add(r);
                                        resultReading[k] = y[readingIndex];
                                        k++;
                                        //increment reading count
                                        readingCount++;
                                    }

                                    //ignore points till Threshold
                                    if (count < Threshold)
                                    {
                                        // skip points till threshold
                                        // if (!r.IsError)
                                        if ((lowerRange <= y[readingIndex]) && (y[readingIndex] <= higherRange))
                                        {
                                            count++;
                                        }
                                        //restart if incorrect reading found
                                        //if (r.IsError)
                                        else
                                        {
                                            // error resets search for start
                                            count = 0;
                                        }

                                    }
                                }
                                // for finding end
                                else
                                {
                                    // finding end
                                    if (count >= Threshold)
                                    {
                                        // found end
                                        break;
                                    }
                                    // ignore incorrect points
                                    //if (r.IsError)
                                    if ((lowerRange <= y[readingIndex]) && (y[readingIndex] <= higherRange))
                                    {
                                        //resultReading.Add(r);
                                        resultReading[k] = y[readingIndex];
                                        k++;
                                        count = 0;

                                    }
                                    // store correct readings
                                    //if (!r.IsError)
                                    else
                                    {
                                        count++;

                                    }

                                }

                            }//end of foreach

                        }// end of for j     
                }




                }
                catch (IOException ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }

            CreateCSV();
           
           
        }


        private void CreateCSV()
        {

            StringBuilder sb = new StringBuilder();
            string[][] delta = new string[][]{   
                                                        };//for delta
            string fileName = "_Testing.csv";
            string DelfilePath = @"c:\\" + fileName;
            string delimiter = ",";
            //log.Write("\n length of csv:" + len.ToString());
            for (long n = 0; n < (readingCount-1); n++)
            {
                delta = new string[][] { 
                                            new string[]{n.ToString(),resultReading[n].ToString()}           
                                            };


                int length = delta.GetLength(0);
                //   MessageBox.Show(delta.ToString());


                for (int index = 0; index < length; index++)
                {

                    sb.AppendLine(string.Join(delimiter, delta[index]));
                    //   MessageBox.Show(sb.ToString());
                }
            }



            File.WriteAllText(DelfilePath, sb.ToString());
            MessageBox.Show("Csv Generated at: "+DelfilePath.ToString());

            //log.Write("\n CSV File created at location: \n" + DelfilePath);
            //txtOutput.ForeColor = Color.Green;
           // txtOutput.Text = "\n CSV File created at location: \n" + DelfilePath;

            //reset state after creating csv
           // State = States.PORT_CONNECTED;
            /*
            //graph butoon enable/disable   
            if (rdShowGraph == true)
            {
                btnGraphShow.Enabled = true;

            }
            else
            {
                btnGraphShow.Enabled = false;
            }
            */


            //Thread.Sleep(1000);
        }

        private void outerContainer_Panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        
        private void showPits_value_Click(object sender, EventArgs e)
        {
            
                //---------delete,just to test whether data is coming datagridview or not alok 
               /* for (long i = 0; i < 5; i++)
                {
                    pitsVal.pitsValue.Rows.Add(System.DateTime.Now.ToString(), "5", i);
                }
                for (long i = 0; i < 5; i++)
                {
                    pitsVal.pitsValue.Rows.Add(System.DateTime.Now.ToString(), "6", i);
                }*/
                InsertToDb();
                //----------------------
                //taking the data from 
                pitsVal.Show();
           
        }

       
    }
}
