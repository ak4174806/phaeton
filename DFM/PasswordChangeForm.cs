﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace DFM
{
    public partial class PasswordChangeForm : Form
    {
        public PasswordChangeForm()
        {
            InitializeComponent();
        }

        public string Password
        {
            get { return txtPassword.Text; }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (txtPassword.Text == txtPassword2.Text)
            {
                return;
            }
            else
            {
                DialogResult = System.Windows.Forms.DialogResult.None;
                MessageBox.Show("Passwords do not match");
            }
        }
    }
}
