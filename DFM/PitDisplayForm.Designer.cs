﻿namespace DFM
{
    partial class PitDisplayForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PitDisplayForm));
            this.txtData = new System.Windows.Forms.TextBox();
            this.lblHeading = new System.Windows.Forms.Label();
            this.txtFolderDb = new System.Windows.Forms.TextBox();
            this.btnbrowseDB = new System.Windows.Forms.Button();
            this.btnExportDB = new System.Windows.Forms.Button();
            this.folderBrowserDb = new System.Windows.Forms.FolderBrowserDialog();
            this.SuspendLayout();
            // 
            // txtData
            // 
            this.txtData.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtData.Location = new System.Drawing.Point(12, 39);
            this.txtData.Multiline = true;
            this.txtData.Name = "txtData";
            this.txtData.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtData.Size = new System.Drawing.Size(247, 247);
            this.txtData.TabIndex = 4;
            // 
            // lblHeading
            // 
            this.lblHeading.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblHeading.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHeading.Location = new System.Drawing.Point(7, 0);
            this.lblHeading.Name = "lblHeading";
            this.lblHeading.Size = new System.Drawing.Size(264, 36);
            this.lblHeading.TabIndex = 3;
            this.lblHeading.Text = "label1";
            // 
            // txtFolderDb
            // 
            this.txtFolderDb.Location = new System.Drawing.Point(12, 290);
            this.txtFolderDb.Name = "txtFolderDb";
            this.txtFolderDb.Size = new System.Drawing.Size(82, 20);
            this.txtFolderDb.TabIndex = 5;
            this.txtFolderDb.Text = "c:\\";
            // 
            // btnbrowseDB
            // 
            this.btnbrowseDB.Location = new System.Drawing.Point(12, 313);
            this.btnbrowseDB.Name = "btnbrowseDB";
            this.btnbrowseDB.Size = new System.Drawing.Size(82, 23);
            this.btnbrowseDB.TabIndex = 6;
            this.btnbrowseDB.Text = "Choose";
            this.btnbrowseDB.UseVisualStyleBackColor = true;
            // 
            // btnExportDB
            // 
            this.btnExportDB.Location = new System.Drawing.Point(167, 291);
            this.btnExportDB.Name = "btnExportDB";
            this.btnExportDB.Size = new System.Drawing.Size(92, 36);
            this.btnExportDB.TabIndex = 7;
            this.btnExportDB.Text = "Export";
            this.btnExportDB.UseVisualStyleBackColor = true;
            // 
            // PitDisplayForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(271, 338);
            this.Controls.Add(this.btnExportDB);
            this.Controls.Add(this.btnbrowseDB);
            this.Controls.Add(this.txtFolderDb);
            this.Controls.Add(this.txtData);
            this.Controls.Add(this.lblHeading);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "PitDisplayForm";
            this.Text = "PitDisplayForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.TextBox txtData;
        public System.Windows.Forms.Label lblHeading;
        public System.Windows.Forms.TextBox txtFolderDb;
        public System.Windows.Forms.Button btnbrowseDB;
        public System.Windows.Forms.Button btnExportDB;
        public System.Windows.Forms.FolderBrowserDialog folderBrowserDb;
    }
}