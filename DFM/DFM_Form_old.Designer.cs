﻿namespace DFM
{
    partial class DFM_Form_old
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.Label lblSensorType;
            System.Windows.Forms.Label lblSensorModel;
            System.Windows.Forms.Label lblPLCPort;
            System.Windows.Forms.Label lblSensorPort;
            System.Windows.Forms.GroupBox grpConnection;
            System.Windows.Forms.Label label6;
            System.Windows.Forms.Label label4;
            System.Windows.Forms.Label lblGatingFreq;
            System.Windows.Forms.Label label3;
            System.Windows.Forms.Label label2;
            System.Windows.Forms.Label lblDutyCycle;
            System.Windows.Forms.GroupBox grpLaserPresets;
            System.Windows.Forms.Label label5;
            System.Windows.Forms.Label lblLaserStart;
            System.Windows.Forms.Label label7;
            System.Windows.Forms.Label lblDAQStop;
            System.Windows.Forms.Label label9;
            System.Windows.Forms.GroupBox grpControl;
            System.Windows.Forms.Label lblTimes;
            System.Windows.Forms.Label lblRepeatTime;
            System.Windows.Forms.Label label8;
            System.Windows.Forms.GroupBox grpAcquisitionSettings;
            System.Windows.Forms.Label lblFilenamePattern;
            System.Windows.Forms.Label lblChooseDataFolder;
            System.Windows.Forms.Label lblPitDepths;
            System.Windows.Forms.Label lblPitDepth;
            System.Windows.Forms.Label lblNumPits;
            System.Windows.Forms.Label lblPeakThreshold;
            System.Windows.Forms.Label lblRollingGraphDisplay;
            System.Windows.Forms.Label label1;
            System.Windows.Forms.Label lblBoundaryLines;
            System.Windows.Forms.Label label10;
            System.Windows.Forms.Label lblThresholdParam;
            System.Windows.Forms.Label lblMinPitWidth;
            System.Windows.Forms.Label lblSmoothingWidth;
            System.Windows.Forms.Label lblCompressionFactor;
            System.Windows.Forms.Label lblPeakHysteresis;
            System.Windows.Forms.Label lblMaxPeakWidth;
            System.Windows.Forms.Label label11;
            System.Windows.Forms.Label label12;
            System.Windows.Forms.Label label13;
            System.Windows.Forms.Label label14;
            System.Windows.Forms.Label lblSubstrateRoughness;
            System.Windows.Forms.Label lblPitPoints;
            this.chkConnectToLaser = new System.Windows.Forms.CheckBox();
            this.chkConnectToSensor = new System.Windows.Forms.CheckBox();
            this.cmbPLCPort = new System.Windows.Forms.ComboBox();
            this.cmbSensorPort = new System.Windows.Forms.ComboBox();
            this.cmbPLCBaudrate = new System.Windows.Forms.ComboBox();
            this.cmbSensorBaudrate = new System.Windows.Forms.ComboBox();
            this.btnShowGalil = new System.Windows.Forms.Button();
            this.btnConnect = new System.Windows.Forms.Button();
            this.cmbSensorType = new System.Windows.Forms.ComboBox();
            this.cmbSensorModel = new System.Windows.Forms.ComboBox();
            this.radRunTime = new System.Windows.Forms.RadioButton();
            this.radPulseCount = new System.Windows.Forms.RadioButton();
            this.numRunTime = new System.Windows.Forms.NumericUpDown();
            this.btnCancelPresets = new System.Windows.Forms.Button();
            this.numPulseCount = new System.Windows.Forms.NumericUpDown();
            this.btnUpdatePresets = new System.Windows.Forms.Button();
            this.chkPulseStopNeeded = new System.Windows.Forms.CheckBox();
            this.numDutyCycle = new System.Windows.Forms.NumericUpDown();
            this.numGatingFreq = new System.Windows.Forms.NumericUpDown();
            this.numRepeatCount = new System.Windows.Forms.NumericUpDown();
            this.numRepeatTime = new System.Windows.Forms.NumericUpDown();
            this.chkRepeat = new System.Windows.Forms.CheckBox();
            this.numDAQStop = new System.Windows.Forms.NumericUpDown();
            this.btnStop = new System.Windows.Forms.Button();
            this.btnRun = new System.Windows.Forms.Button();
            this.numDAQStart = new System.Windows.Forms.NumericUpDown();
            this.btnPrepare = new System.Windows.Forms.Button();
            this.lblDAQStart = new System.Windows.Forms.Label();
            this.numLaserStart = new System.Windows.Forms.NumericUpDown();
            this.btnLoadLastScan = new System.Windows.Forms.Button();
            this.txtFilenamePattern = new System.Windows.Forms.TextBox();
            this.btnChooseDataFolder = new System.Windows.Forms.Button();
            this.txtDataFolder = new System.Windows.Forms.TextBox();
            this.btnClose = new System.Windows.Forms.Button();
            this.zgcScan = new ZedGraph.ZedGraphControl();
            this.folderBrowser = new System.Windows.Forms.FolderBrowserDialog();
            this.lstPits = new System.Windows.Forms.ListBox();
            this.txtPitDepth = new System.Windows.Forms.TextBox();
            this.txtNumPits = new System.Windows.Forms.TextBox();
            this.chkFilterPeaks = new System.Windows.Forms.CheckBox();
            this.numPeakThreshold = new System.Windows.Forms.NumericUpDown();
            this.zgcRollingGraph = new ZedGraph.ZedGraphControl();
            this.txtRollingGraphDisplay = new System.Windows.Forms.TextBox();
            this.numBoundaryLow = new System.Windows.Forms.NumericUpDown();
            this.numBoundaryHigh = new System.Windows.Forms.NumericUpDown();
            this.btnClearRollingGraph = new System.Windows.Forms.Button();
            this.numThresholdParam = new System.Windows.Forms.NumericUpDown();
            this.numMinPitWidth = new System.Windows.Forms.NumericUpDown();
            this.numSmoothingWidth = new System.Windows.Forms.NumericUpDown();
            this.numCompressionFactor = new System.Windows.Forms.NumericUpDown();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.numTruncateFirst = new System.Windows.Forms.NumericUpDown();
            this.numTruncateLast = new System.Windows.Forms.NumericUpDown();
            this.numMaxPeakWidth = new System.Windows.Forms.NumericUpDown();
            this.numPeakHysteresis = new System.Windows.Forms.NumericUpDown();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.splitContainer3 = new System.Windows.Forms.SplitContainer();
            this.numPitPoints = new System.Windows.Forms.NumericUpDown();
            this.numSubstrateRoughness = new System.Windows.Forms.NumericUpDown();
            this.chkDebugMode = new System.Windows.Forms.CheckBox();
            lblSensorType = new System.Windows.Forms.Label();
            lblSensorModel = new System.Windows.Forms.Label();
            lblPLCPort = new System.Windows.Forms.Label();
            lblSensorPort = new System.Windows.Forms.Label();
            grpConnection = new System.Windows.Forms.GroupBox();
            label6 = new System.Windows.Forms.Label();
            label4 = new System.Windows.Forms.Label();
            lblGatingFreq = new System.Windows.Forms.Label();
            label3 = new System.Windows.Forms.Label();
            label2 = new System.Windows.Forms.Label();
            lblDutyCycle = new System.Windows.Forms.Label();
            grpLaserPresets = new System.Windows.Forms.GroupBox();
            label5 = new System.Windows.Forms.Label();
            lblLaserStart = new System.Windows.Forms.Label();
            label7 = new System.Windows.Forms.Label();
            lblDAQStop = new System.Windows.Forms.Label();
            label9 = new System.Windows.Forms.Label();
            grpControl = new System.Windows.Forms.GroupBox();
            lblTimes = new System.Windows.Forms.Label();
            lblRepeatTime = new System.Windows.Forms.Label();
            label8 = new System.Windows.Forms.Label();
            grpAcquisitionSettings = new System.Windows.Forms.GroupBox();
            lblFilenamePattern = new System.Windows.Forms.Label();
            lblChooseDataFolder = new System.Windows.Forms.Label();
            lblPitDepths = new System.Windows.Forms.Label();
            lblPitDepth = new System.Windows.Forms.Label();
            lblNumPits = new System.Windows.Forms.Label();
            lblPeakThreshold = new System.Windows.Forms.Label();
            lblRollingGraphDisplay = new System.Windows.Forms.Label();
            label1 = new System.Windows.Forms.Label();
            lblBoundaryLines = new System.Windows.Forms.Label();
            label10 = new System.Windows.Forms.Label();
            lblThresholdParam = new System.Windows.Forms.Label();
            lblMinPitWidth = new System.Windows.Forms.Label();
            lblSmoothingWidth = new System.Windows.Forms.Label();
            lblCompressionFactor = new System.Windows.Forms.Label();
            lblPeakHysteresis = new System.Windows.Forms.Label();
            lblMaxPeakWidth = new System.Windows.Forms.Label();
            label11 = new System.Windows.Forms.Label();
            label12 = new System.Windows.Forms.Label();
            label13 = new System.Windows.Forms.Label();
            label14 = new System.Windows.Forms.Label();
            lblSubstrateRoughness = new System.Windows.Forms.Label();
            lblPitPoints = new System.Windows.Forms.Label();
            grpConnection.SuspendLayout();
            grpLaserPresets.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numRunTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPulseCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numDutyCycle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numGatingFreq)).BeginInit();
            grpControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numRepeatCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numRepeatTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numDAQStop)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numDAQStart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numLaserStart)).BeginInit();
            grpAcquisitionSettings.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numPeakThreshold)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numBoundaryLow)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numBoundaryHigh)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numThresholdParam)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numMinPitWidth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSmoothingWidth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numCompressionFactor)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numTruncateFirst)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numTruncateLast)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numMaxPeakWidth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPeakHysteresis)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.splitContainer3.Panel1.SuspendLayout();
            this.splitContainer3.Panel2.SuspendLayout();
            this.splitContainer3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numPitPoints)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSubstrateRoughness)).BeginInit();
            this.SuspendLayout();
            // 
            // lblSensorType
            // 
            lblSensorType.AutoSize = true;
            lblSensorType.Location = new System.Drawing.Point(7, 55);
            lblSensorType.Name = "lblSensorType";
            lblSensorType.Size = new System.Drawing.Size(34, 13);
            lblSensorType.TabIndex = 6;
            lblSensorType.Text = "Type:";
            // 
            // lblSensorModel
            // 
            lblSensorModel.AutoSize = true;
            lblSensorModel.Location = new System.Drawing.Point(6, 27);
            lblSensorModel.Name = "lblSensorModel";
            lblSensorModel.Size = new System.Drawing.Size(75, 13);
            lblSensorModel.TabIndex = 3;
            lblSensorModel.Text = "Sensor Model:";
            // 
            // lblPLCPort
            // 
            lblPLCPort.AutoSize = true;
            lblPLCPort.Location = new System.Drawing.Point(3, 111);
            lblPLCPort.Name = "lblPLCPort";
            lblPLCPort.Size = new System.Drawing.Size(52, 13);
            lblPLCPort.TabIndex = 9;
            lblPLCPort.Text = "PLC Port:";
            // 
            // lblSensorPort
            // 
            lblSensorPort.AutoSize = true;
            lblSensorPort.Location = new System.Drawing.Point(2, 83);
            lblSensorPort.Name = "lblSensorPort";
            lblSensorPort.Size = new System.Drawing.Size(65, 13);
            lblSensorPort.TabIndex = 8;
            lblSensorPort.Text = "Sensor Port:";
            // 
            // grpConnection
            // 
            grpConnection.Controls.Add(this.chkConnectToLaser);
            grpConnection.Controls.Add(this.chkConnectToSensor);
            grpConnection.Controls.Add(this.cmbPLCPort);
            grpConnection.Controls.Add(this.cmbSensorPort);
            grpConnection.Controls.Add(this.cmbPLCBaudrate);
            grpConnection.Controls.Add(this.cmbSensorBaudrate);
            grpConnection.Controls.Add(label6);
            grpConnection.Controls.Add(label4);
            grpConnection.Controls.Add(lblSensorModel);
            grpConnection.Controls.Add(lblPLCPort);
            grpConnection.Controls.Add(lblSensorPort);
            grpConnection.Controls.Add(this.btnShowGalil);
            grpConnection.Controls.Add(this.btnConnect);
            grpConnection.Controls.Add(this.cmbSensorType);
            grpConnection.Controls.Add(this.cmbSensorModel);
            grpConnection.Controls.Add(lblSensorType);
            grpConnection.Location = new System.Drawing.Point(9, 8);
            grpConnection.Name = "grpConnection";
            grpConnection.Size = new System.Drawing.Size(248, 187);
            grpConnection.TabIndex = 10;
            grpConnection.TabStop = false;
            grpConnection.Text = "Connection";
            // 
            // chkConnectToLaser
            // 
            this.chkConnectToLaser.AutoSize = true;
            this.chkConnectToLaser.Checked = true;
            this.chkConnectToLaser.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkConnectToLaser.Location = new System.Drawing.Point(5, 139);
            this.chkConnectToLaser.Name = "chkConnectToLaser";
            this.chkConnectToLaser.Size = new System.Drawing.Size(108, 17);
            this.chkConnectToLaser.TabIndex = 16;
            this.chkConnectToLaser.Text = "connect to laser?";
            this.chkConnectToLaser.UseVisualStyleBackColor = true;
            // 
            // chkConnectToSensor
            // 
            this.chkConnectToSensor.AutoSize = true;
            this.chkConnectToSensor.Checked = true;
            this.chkConnectToSensor.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkConnectToSensor.Location = new System.Drawing.Point(6, 158);
            this.chkConnectToSensor.Name = "chkConnectToSensor";
            this.chkConnectToSensor.Size = new System.Drawing.Size(117, 17);
            this.chkConnectToSensor.TabIndex = 15;
            this.chkConnectToSensor.Text = "connect to sensor?";
            this.chkConnectToSensor.UseVisualStyleBackColor = true;
            // 
            // cmbPLCPort
            // 
            this.cmbPLCPort.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPLCPort.FormattingEnabled = true;
            this.cmbPLCPort.Location = new System.Drawing.Point(67, 107);
            this.cmbPLCPort.Name = "cmbPLCPort";
            this.cmbPLCPort.Size = new System.Drawing.Size(65, 21);
            this.cmbPLCPort.TabIndex = 14;
            // 
            // cmbSensorPort
            // 
            this.cmbSensorPort.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSensorPort.FormattingEnabled = true;
            this.cmbSensorPort.Location = new System.Drawing.Point(67, 80);
            this.cmbSensorPort.Name = "cmbSensorPort";
            this.cmbSensorPort.Size = new System.Drawing.Size(65, 21);
            this.cmbSensorPort.TabIndex = 13;
            // 
            // cmbPLCBaudrate
            // 
            this.cmbPLCBaudrate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPLCBaudrate.FormattingEnabled = true;
            this.cmbPLCBaudrate.Location = new System.Drawing.Point(147, 107);
            this.cmbPLCBaudrate.Name = "cmbPLCBaudrate";
            this.cmbPLCBaudrate.Size = new System.Drawing.Size(81, 21);
            this.cmbPLCBaudrate.TabIndex = 12;
            // 
            // cmbSensorBaudrate
            // 
            this.cmbSensorBaudrate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSensorBaudrate.FormattingEnabled = true;
            this.cmbSensorBaudrate.Location = new System.Drawing.Point(147, 80);
            this.cmbSensorBaudrate.Name = "cmbSensorBaudrate";
            this.cmbSensorBaudrate.Size = new System.Drawing.Size(81, 21);
            this.cmbSensorBaudrate.TabIndex = 11;
            // 
            // label6
            // 
            label6.AutoSize = true;
            label6.Location = new System.Drawing.Point(130, 111);
            label6.Name = "label6";
            label6.Size = new System.Drawing.Size(18, 13);
            label6.TabIndex = 10;
            label6.Text = "@";
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Location = new System.Drawing.Point(131, 84);
            label4.Name = "label4";
            label4.Size = new System.Drawing.Size(18, 13);
            label4.TabIndex = 10;
            label4.Text = "@";
            // 
            // btnShowGalil
            // 
            this.btnShowGalil.Location = new System.Drawing.Point(126, 160);
            this.btnShowGalil.Name = "btnShowGalil";
            this.btnShowGalil.Size = new System.Drawing.Size(106, 23);
            this.btnShowGalil.TabIndex = 1;
            this.btnShowGalil.Text = "Show Galil Setup";
            this.btnShowGalil.UseVisualStyleBackColor = true;
            // 
            // btnConnect
            // 
            this.btnConnect.Location = new System.Drawing.Point(129, 137);
            this.btnConnect.Name = "btnConnect";
            this.btnConnect.Size = new System.Drawing.Size(106, 23);
            this.btnConnect.TabIndex = 1;
            this.btnConnect.Text = "Connect";
            this.btnConnect.UseVisualStyleBackColor = true;
            // 
            // cmbSensorType
            // 
            this.cmbSensorType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSensorType.FormattingEnabled = true;
            this.cmbSensorType.Location = new System.Drawing.Point(94, 52);
            this.cmbSensorType.Name = "cmbSensorType";
            this.cmbSensorType.Size = new System.Drawing.Size(121, 21);
            this.cmbSensorType.TabIndex = 5;
            // 
            // cmbSensorModel
            // 
            this.cmbSensorModel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSensorModel.FormattingEnabled = true;
            this.cmbSensorModel.Location = new System.Drawing.Point(94, 24);
            this.cmbSensorModel.Name = "cmbSensorModel";
            this.cmbSensorModel.Size = new System.Drawing.Size(121, 21);
            this.cmbSensorModel.TabIndex = 4;
            // 
            // lblGatingFreq
            // 
            lblGatingFreq.AutoSize = true;
            lblGatingFreq.Location = new System.Drawing.Point(5, 79);
            lblGatingFreq.Name = "lblGatingFreq";
            lblGatingFreq.Size = new System.Drawing.Size(94, 13);
            lblGatingFreq.TabIndex = 15;
            lblGatingFreq.Text = "Gating Frequency:";
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Location = new System.Drawing.Point(203, 80);
            label3.Name = "label3";
            label3.Size = new System.Drawing.Size(20, 13);
            label3.TabIndex = 17;
            label3.Text = "Hz";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new System.Drawing.Point(203, 112);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(15, 13);
            label2.TabIndex = 20;
            label2.Text = "%";
            // 
            // lblDutyCycle
            // 
            lblDutyCycle.AutoSize = true;
            lblDutyCycle.Location = new System.Drawing.Point(5, 112);
            lblDutyCycle.Name = "lblDutyCycle";
            lblDutyCycle.Size = new System.Drawing.Size(61, 13);
            lblDutyCycle.TabIndex = 18;
            lblDutyCycle.Text = "Duty Cycle:";
            // 
            // grpLaserPresets
            // 
            grpLaserPresets.Controls.Add(this.radRunTime);
            grpLaserPresets.Controls.Add(this.radPulseCount);
            grpLaserPresets.Controls.Add(this.numRunTime);
            grpLaserPresets.Controls.Add(this.btnCancelPresets);
            grpLaserPresets.Controls.Add(this.numPulseCount);
            grpLaserPresets.Controls.Add(this.btnUpdatePresets);
            grpLaserPresets.Controls.Add(this.chkPulseStopNeeded);
            grpLaserPresets.Controls.Add(label2);
            grpLaserPresets.Controls.Add(lblGatingFreq);
            grpLaserPresets.Controls.Add(this.numDutyCycle);
            grpLaserPresets.Controls.Add(this.numGatingFreq);
            grpLaserPresets.Controls.Add(lblDutyCycle);
            grpLaserPresets.Controls.Add(label3);
            grpLaserPresets.Location = new System.Drawing.Point(9, 197);
            grpLaserPresets.Name = "grpLaserPresets";
            grpLaserPresets.Size = new System.Drawing.Size(232, 183);
            grpLaserPresets.TabIndex = 23;
            grpLaserPresets.TabStop = false;
            grpLaserPresets.Text = "Laser Presets";
            // 
            // radRunTime
            // 
            this.radRunTime.AutoSize = true;
            this.radRunTime.Location = new System.Drawing.Point(10, 51);
            this.radRunTime.Name = "radRunTime";
            this.radRunTime.Size = new System.Drawing.Size(93, 17);
            this.radRunTime.TabIndex = 23;
            this.radRunTime.Text = "Run Time (ms)";
            this.radRunTime.UseVisualStyleBackColor = true;
            this.radRunTime.Visible = false;
            // 
            // radPulseCount
            // 
            this.radPulseCount.AutoSize = true;
            this.radPulseCount.Checked = true;
            this.radPulseCount.Location = new System.Drawing.Point(10, 28);
            this.radPulseCount.Name = "radPulseCount";
            this.radPulseCount.Size = new System.Drawing.Size(82, 17);
            this.radPulseCount.TabIndex = 23;
            this.radPulseCount.TabStop = true;
            this.radPulseCount.Text = "Pulse Count";
            this.radPulseCount.UseVisualStyleBackColor = true;
            // 
            // numRunTime
            // 
            this.numRunTime.Location = new System.Drawing.Point(116, 48);
            this.numRunTime.Maximum = new decimal(new int[] {
            32766,
            0,
            0,
            0});
            this.numRunTime.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numRunTime.Name = "numRunTime";
            this.numRunTime.Size = new System.Drawing.Size(98, 20);
            this.numRunTime.TabIndex = 12;
            this.numRunTime.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.numRunTime.Visible = false;
            // 
            // btnCancelPresets
            // 
            this.btnCancelPresets.Location = new System.Drawing.Point(119, 147);
            this.btnCancelPresets.Name = "btnCancelPresets";
            this.btnCancelPresets.Size = new System.Drawing.Size(75, 23);
            this.btnCancelPresets.TabIndex = 22;
            this.btnCancelPresets.Text = "Cancel";
            this.btnCancelPresets.UseVisualStyleBackColor = true;
            // 
            // numPulseCount
            // 
            this.numPulseCount.Location = new System.Drawing.Point(116, 25);
            this.numPulseCount.Maximum = new decimal(new int[] {
            99999,
            0,
            0,
            0});
            this.numPulseCount.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numPulseCount.Name = "numPulseCount";
            this.numPulseCount.Size = new System.Drawing.Size(98, 20);
            this.numPulseCount.TabIndex = 12;
            this.numPulseCount.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // btnUpdatePresets
            // 
            this.btnUpdatePresets.Location = new System.Drawing.Point(17, 147);
            this.btnUpdatePresets.Name = "btnUpdatePresets";
            this.btnUpdatePresets.Size = new System.Drawing.Size(75, 23);
            this.btnUpdatePresets.TabIndex = 21;
            this.btnUpdatePresets.Text = "Update";
            this.btnUpdatePresets.UseVisualStyleBackColor = true;
            // 
            // chkPulseStopNeeded
            // 
            this.chkPulseStopNeeded.AutoSize = true;
            this.chkPulseStopNeeded.Location = new System.Drawing.Point(181, 2);
            this.chkPulseStopNeeded.Name = "chkPulseStopNeeded";
            this.chkPulseStopNeeded.Size = new System.Drawing.Size(98, 17);
            this.chkPulseStopNeeded.TabIndex = 14;
            this.chkPulseStopNeeded.Text = "or until stopped";
            this.chkPulseStopNeeded.UseVisualStyleBackColor = true;
            this.chkPulseStopNeeded.Visible = false;
            // 
            // numDutyCycle
            // 
            this.numDutyCycle.DecimalPlaces = 1;
            this.numDutyCycle.Location = new System.Drawing.Point(115, 108);
            this.numDutyCycle.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numDutyCycle.Name = "numDutyCycle";
            this.numDutyCycle.Size = new System.Drawing.Size(84, 20);
            this.numDutyCycle.TabIndex = 19;
            this.numDutyCycle.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // numGatingFreq
            // 
            this.numGatingFreq.Location = new System.Drawing.Point(116, 76);
            this.numGatingFreq.Maximum = new decimal(new int[] {
            20000,
            0,
            0,
            0});
            this.numGatingFreq.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numGatingFreq.Name = "numGatingFreq";
            this.numGatingFreq.Size = new System.Drawing.Size(84, 20);
            this.numGatingFreq.TabIndex = 16;
            this.numGatingFreq.Value = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.Location = new System.Drawing.Point(203, 26);
            label5.Name = "label5";
            label5.Size = new System.Drawing.Size(20, 13);
            label5.TabIndex = 25;
            label5.Text = "ms";
            // 
            // lblLaserStart
            // 
            lblLaserStart.AutoSize = true;
            lblLaserStart.Location = new System.Drawing.Point(5, 52);
            lblLaserStart.Name = "lblLaserStart";
            lblLaserStart.Size = new System.Drawing.Size(81, 13);
            lblLaserStart.TabIndex = 26;
            lblLaserStart.Text = "Start laser after:";
            // 
            // label7
            // 
            label7.AutoSize = true;
            label7.Location = new System.Drawing.Point(203, 53);
            label7.Name = "label7";
            label7.Size = new System.Drawing.Size(20, 13);
            label7.TabIndex = 28;
            label7.Text = "ms";
            // 
            // lblDAQStop
            // 
            lblDAQStop.AutoSize = true;
            lblDAQStop.Location = new System.Drawing.Point(5, 77);
            lblDAQStop.Name = "lblDAQStop";
            lblDAQStop.Size = new System.Drawing.Size(82, 13);
            lblDAQStop.TabIndex = 29;
            lblDAQStop.Text = "Stop DAQ after:";
            // 
            // label9
            // 
            label9.AutoSize = true;
            label9.Location = new System.Drawing.Point(203, 78);
            label9.Name = "label9";
            label9.Size = new System.Drawing.Size(20, 13);
            label9.TabIndex = 31;
            label9.Text = "ms";
            // 
            // grpControl
            // 
            grpControl.Controls.Add(lblTimes);
            grpControl.Controls.Add(this.numRepeatCount);
            grpControl.Controls.Add(this.numRepeatTime);
            grpControl.Controls.Add(lblRepeatTime);
            grpControl.Controls.Add(label8);
            grpControl.Controls.Add(this.chkRepeat);
            grpControl.Controls.Add(this.numDAQStop);
            grpControl.Controls.Add(this.btnStop);
            grpControl.Controls.Add(label5);
            grpControl.Controls.Add(this.btnRun);
            grpControl.Controls.Add(this.numDAQStart);
            grpControl.Controls.Add(this.btnPrepare);
            grpControl.Controls.Add(this.lblDAQStart);
            grpControl.Controls.Add(lblDAQStop);
            grpControl.Controls.Add(label7);
            grpControl.Controls.Add(this.numLaserStart);
            grpControl.Controls.Add(label9);
            grpControl.Controls.Add(lblLaserStart);
            grpControl.Location = new System.Drawing.Point(9, 387);
            grpControl.Name = "grpControl";
            grpControl.Size = new System.Drawing.Size(232, 173);
            grpControl.TabIndex = 34;
            grpControl.TabStop = false;
            grpControl.Text = "Control";
            // 
            // lblTimes
            // 
            lblTimes.AutoSize = true;
            lblTimes.Location = new System.Drawing.Point(149, 133);
            lblTimes.Name = "lblTimes";
            lblTimes.Size = new System.Drawing.Size(31, 13);
            lblTimes.TabIndex = 38;
            lblTimes.Text = "times";
            // 
            // numRepeatCount
            // 
            this.numRepeatCount.Location = new System.Drawing.Point(79, 131);
            this.numRepeatCount.Maximum = new decimal(new int[] {
            200000000,
            0,
            0,
            0});
            this.numRepeatCount.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numRepeatCount.Name = "numRepeatCount";
            this.numRepeatCount.Size = new System.Drawing.Size(64, 20);
            this.numRepeatCount.TabIndex = 36;
            this.numRepeatCount.Value = new decimal(new int[] {
            4,
            0,
            0,
            0});
            // 
            // numRepeatTime
            // 
            this.numRepeatTime.Location = new System.Drawing.Point(118, 150);
            this.numRepeatTime.Maximum = new decimal(new int[] {
            200000000,
            0,
            0,
            0});
            this.numRepeatTime.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numRepeatTime.Name = "numRepeatTime";
            this.numRepeatTime.Size = new System.Drawing.Size(84, 20);
            this.numRepeatTime.TabIndex = 36;
            this.numRepeatTime.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
            // 
            // lblRepeatTime
            // 
            lblRepeatTime.AutoSize = true;
            lblRepeatTime.Location = new System.Drawing.Point(7, 153);
            lblRepeatTime.Name = "lblRepeatTime";
            lblRepeatTime.Size = new System.Drawing.Size(74, 13);
            lblRepeatTime.TabIndex = 35;
            lblRepeatTime.Text = "Repeat every:";
            // 
            // label8
            // 
            label8.AutoSize = true;
            label8.Location = new System.Drawing.Point(207, 154);
            label8.Name = "label8";
            label8.Size = new System.Drawing.Size(12, 13);
            label8.TabIndex = 37;
            label8.Text = "s";
            // 
            // chkRepeat
            // 
            this.chkRepeat.AutoSize = true;
            this.chkRepeat.Checked = true;
            this.chkRepeat.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkRepeat.Location = new System.Drawing.Point(9, 133);
            this.chkRepeat.Name = "chkRepeat";
            this.chkRepeat.Size = new System.Drawing.Size(64, 17);
            this.chkRepeat.TabIndex = 34;
            this.chkRepeat.Text = "Repeat:";
            this.chkRepeat.UseVisualStyleBackColor = true;
            // 
            // numDAQStop
            // 
            this.numDAQStop.Location = new System.Drawing.Point(116, 74);
            this.numDAQStop.Maximum = new decimal(new int[] {
            200000000,
            0,
            0,
            0});
            this.numDAQStop.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numDAQStop.Name = "numDAQStop";
            this.numDAQStop.Size = new System.Drawing.Size(84, 20);
            this.numDAQStop.TabIndex = 30;
            this.numDAQStop.Value = new decimal(new int[] {
            220,
            0,
            0,
            0});
            // 
            // btnStop
            // 
            this.btnStop.Location = new System.Drawing.Point(151, 104);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(63, 23);
            this.btnStop.TabIndex = 33;
            this.btnStop.Text = "Stop";
            this.btnStop.UseVisualStyleBackColor = true;
            // 
            // btnRun
            // 
            this.btnRun.Location = new System.Drawing.Point(94, 104);
            this.btnRun.Name = "btnRun";
            this.btnRun.Size = new System.Drawing.Size(49, 23);
            this.btnRun.TabIndex = 33;
            this.btnRun.Text = "Run";
            this.btnRun.UseVisualStyleBackColor = true;
            // 
            // numDAQStart
            // 
            this.numDAQStart.Location = new System.Drawing.Point(116, 22);
            this.numDAQStart.Maximum = new decimal(new int[] {
            200000000,
            0,
            0,
            0});
            this.numDAQStart.Name = "numDAQStart";
            this.numDAQStart.Size = new System.Drawing.Size(84, 20);
            this.numDAQStart.TabIndex = 24;
            this.numDAQStart.Value = new decimal(new int[] {
            180,
            0,
            0,
            0});
            // 
            // btnPrepare
            // 
            this.btnPrepare.Location = new System.Drawing.Point(8, 104);
            this.btnPrepare.Name = "btnPrepare";
            this.btnPrepare.Size = new System.Drawing.Size(75, 23);
            this.btnPrepare.TabIndex = 32;
            this.btnPrepare.Text = "Prepare";
            this.btnPrepare.UseVisualStyleBackColor = true;
            // 
            // lblDAQStart
            // 
            this.lblDAQStart.AutoSize = true;
            this.lblDAQStart.Location = new System.Drawing.Point(5, 25);
            this.lblDAQStart.Name = "lblDAQStart";
            this.lblDAQStart.Size = new System.Drawing.Size(82, 13);
            this.lblDAQStart.TabIndex = 23;
            this.lblDAQStart.Text = "Start DAQ after:";
            // 
            // numLaserStart
            // 
            this.numLaserStart.Location = new System.Drawing.Point(116, 49);
            this.numLaserStart.Maximum = new decimal(new int[] {
            200000000,
            0,
            0,
            0});
            this.numLaserStart.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numLaserStart.Name = "numLaserStart";
            this.numLaserStart.Size = new System.Drawing.Size(84, 20);
            this.numLaserStart.TabIndex = 27;
            this.numLaserStart.Value = new decimal(new int[] {
            182,
            0,
            0,
            0});
            // 
            // grpAcquisitionSettings
            // 
            grpAcquisitionSettings.Controls.Add(this.btnLoadLastScan);
            grpAcquisitionSettings.Controls.Add(this.txtFilenamePattern);
            grpAcquisitionSettings.Controls.Add(lblFilenamePattern);
            grpAcquisitionSettings.Controls.Add(this.btnChooseDataFolder);
            grpAcquisitionSettings.Controls.Add(this.txtDataFolder);
            grpAcquisitionSettings.Controls.Add(lblChooseDataFolder);
            grpAcquisitionSettings.Location = new System.Drawing.Point(12, 14);
            grpAcquisitionSettings.Name = "grpAcquisitionSettings";
            grpAcquisitionSettings.Size = new System.Drawing.Size(211, 123);
            grpAcquisitionSettings.TabIndex = 36;
            grpAcquisitionSettings.TabStop = false;
            grpAcquisitionSettings.Text = "Data Settings";
            // 
            // btnLoadLastScan
            // 
            this.btnLoadLastScan.Location = new System.Drawing.Point(52, 94);
            this.btnLoadLastScan.Name = "btnLoadLastScan";
            this.btnLoadLastScan.Size = new System.Drawing.Size(111, 23);
            this.btnLoadLastScan.TabIndex = 30;
            this.btnLoadLastScan.Text = "Load Last Scan";
            this.btnLoadLastScan.UseVisualStyleBackColor = true;
            // 
            // txtFilenamePattern
            // 
            this.txtFilenamePattern.Location = new System.Drawing.Point(110, 64);
            this.txtFilenamePattern.Name = "txtFilenamePattern";
            this.txtFilenamePattern.Size = new System.Drawing.Size(88, 20);
            this.txtFilenamePattern.TabIndex = 29;
            this.txtFilenamePattern.Text = "scan {nnn}.csv";
            // 
            // lblFilenamePattern
            // 
            lblFilenamePattern.AutoSize = true;
            lblFilenamePattern.Location = new System.Drawing.Point(14, 66);
            lblFilenamePattern.Name = "lblFilenamePattern";
            lblFilenamePattern.Size = new System.Drawing.Size(89, 13);
            lblFilenamePattern.TabIndex = 28;
            lblFilenamePattern.Text = "Filename Pattern:";
            // 
            // btnChooseDataFolder
            // 
            this.btnChooseDataFolder.Location = new System.Drawing.Point(141, 35);
            this.btnChooseDataFolder.Name = "btnChooseDataFolder";
            this.btnChooseDataFolder.Size = new System.Drawing.Size(57, 23);
            this.btnChooseDataFolder.TabIndex = 14;
            this.btnChooseDataFolder.Text = "Choose";
            this.btnChooseDataFolder.UseVisualStyleBackColor = true;
            // 
            // txtDataFolder
            // 
            this.txtDataFolder.Location = new System.Drawing.Point(17, 38);
            this.txtDataFolder.Name = "txtDataFolder";
            this.txtDataFolder.ReadOnly = true;
            this.txtDataFolder.Size = new System.Drawing.Size(117, 20);
            this.txtDataFolder.TabIndex = 13;
            this.txtDataFolder.Text = "c:\\";
            // 
            // lblChooseDataFolder
            // 
            lblChooseDataFolder.AutoSize = true;
            lblChooseDataFolder.Location = new System.Drawing.Point(14, 20);
            lblChooseDataFolder.Name = "lblChooseDataFolder";
            lblChooseDataFolder.Size = new System.Drawing.Size(77, 13);
            lblChooseDataFolder.TabIndex = 0;
            lblChooseDataFolder.Text = "Data Location:";
            // 
            // lblPitDepths
            // 
            lblPitDepths.AutoSize = true;
            lblPitDepths.Location = new System.Drawing.Point(10, 246);
            lblPitDepths.Name = "lblPitDepths";
            lblPitDepths.Size = new System.Drawing.Size(59, 13);
            lblPitDepths.TabIndex = 38;
            lblPitDepths.Text = "Pit Depths:";
            // 
            // lblPitDepth
            // 
            lblPitDepth.Location = new System.Drawing.Point(8, 184);
            lblPitDepth.Name = "lblPitDepth";
            lblPitDepth.Size = new System.Drawing.Size(95, 37);
            lblPitDepth.TabIndex = 39;
            lblPitDepth.Text = "Average Pit Depth (microns):";
            // 
            // lblNumPits
            // 
            lblNumPits.AutoSize = true;
            lblNumPits.Location = new System.Drawing.Point(8, 221);
            lblNumPits.Name = "lblNumPits";
            lblNumPits.Size = new System.Drawing.Size(79, 13);
            lblNumPits.TabIndex = 41;
            lblNumPits.Text = "Number of Pits:";
            // 
            // lblPeakThreshold
            // 
            lblPeakThreshold.AutoSize = true;
            lblPeakThreshold.Location = new System.Drawing.Point(12, 634);
            lblPeakThreshold.Name = "lblPeakThreshold";
            lblPeakThreshold.Size = new System.Drawing.Size(127, 13);
            lblPeakThreshold.TabIndex = 43;
            lblPeakThreshold.Text = "Peak Threshold (microns)";
            // 
            // lblRollingGraphDisplay
            // 
            lblRollingGraphDisplay.AutoSize = true;
            lblRollingGraphDisplay.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            lblRollingGraphDisplay.Location = new System.Drawing.Point(27, 327);
            lblRollingGraphDisplay.Name = "lblRollingGraphDisplay";
            lblRollingGraphDisplay.Size = new System.Drawing.Size(148, 24);
            lblRollingGraphDisplay.TabIndex = 47;
            lblRollingGraphDisplay.Text = "Average of last 3";
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label1.Location = new System.Drawing.Point(8, 154);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(176, 24);
            label1.TabIndex = 47;
            label1.Text = "Analysis of last scan";
            // 
            // lblBoundaryLines
            // 
            lblBoundaryLines.AutoSize = true;
            lblBoundaryLines.Location = new System.Drawing.Point(31, 409);
            lblBoundaryLines.Name = "lblBoundaryLines";
            lblBoundaryLines.Size = new System.Drawing.Size(125, 13);
            lblBoundaryLines.TabIndex = 49;
            lblBoundaryLines.Text = "Boundary Lines (microns)";
            // 
            // label10
            // 
            label10.AutoSize = true;
            label10.Location = new System.Drawing.Point(98, 435);
            label10.Name = "label10";
            label10.Size = new System.Drawing.Size(16, 13);
            label10.TabIndex = 51;
            label10.Text = "to";
            // 
            // lblThresholdParam
            // 
            lblThresholdParam.AutoSize = true;
            lblThresholdParam.Location = new System.Drawing.Point(19, 535);
            lblThresholdParam.Name = "lblThresholdParam";
            lblThresholdParam.Size = new System.Drawing.Size(108, 13);
            lblThresholdParam.TabIndex = 53;
            lblThresholdParam.Text = "Threshold Parameter:";
            // 
            // lblMinPitWidth
            // 
            lblMinPitWidth.AutoSize = true;
            lblMinPitWidth.Location = new System.Drawing.Point(18, 587);
            lblMinPitWidth.Name = "lblMinPitWidth";
            lblMinPitWidth.Size = new System.Drawing.Size(73, 13);
            lblMinPitWidth.TabIndex = 53;
            lblMinPitWidth.Text = "Min Pit Width:";
            // 
            // lblSmoothingWidth
            // 
            lblSmoothingWidth.AutoSize = true;
            lblSmoothingWidth.Location = new System.Drawing.Point(18, 561);
            lblSmoothingWidth.Name = "lblSmoothingWidth";
            lblSmoothingWidth.Size = new System.Drawing.Size(91, 13);
            lblSmoothingWidth.TabIndex = 53;
            lblSmoothingWidth.Text = "Smoothing Width:";
            // 
            // lblCompressionFactor
            // 
            lblCompressionFactor.AutoSize = true;
            lblCompressionFactor.Location = new System.Drawing.Point(18, 615);
            lblCompressionFactor.Name = "lblCompressionFactor";
            lblCompressionFactor.Size = new System.Drawing.Size(103, 13);
            lblCompressionFactor.TabIndex = 53;
            lblCompressionFactor.Text = "Compression Factor:";
            // 
            // lblPeakHysteresis
            // 
            lblPeakHysteresis.AutoSize = true;
            lblPeakHysteresis.Location = new System.Drawing.Point(12, 653);
            lblPeakHysteresis.Name = "lblPeakHysteresis";
            lblPeakHysteresis.Size = new System.Drawing.Size(128, 13);
            lblPeakHysteresis.TabIndex = 43;
            lblPeakHysteresis.Text = "Peak Hysteresis (microns)";
            // 
            // lblMaxPeakWidth
            // 
            lblMaxPeakWidth.AutoSize = true;
            lblMaxPeakWidth.Location = new System.Drawing.Point(12, 674);
            lblMaxPeakWidth.Name = "lblMaxPeakWidth";
            lblMaxPeakWidth.Size = new System.Drawing.Size(86, 13);
            lblMaxPeakWidth.TabIndex = 43;
            lblMaxPeakWidth.Text = "Max Peak Width";
            // 
            // label11
            // 
            label11.AutoSize = true;
            label11.Location = new System.Drawing.Point(154, 570);
            label11.Name = "label11";
            label11.Size = new System.Drawing.Size(51, 13);
            label11.TabIndex = 48;
            label11.Text = "% of data";
            // 
            // label12
            // 
            label12.AutoSize = true;
            label12.Location = new System.Drawing.Point(14, 571);
            label12.Name = "label12";
            label12.Size = new System.Drawing.Size(72, 13);
            label12.TabIndex = 46;
            label12.Text = "Truncate first:";
            // 
            // label13
            // 
            label13.AutoSize = true;
            label13.Location = new System.Drawing.Point(154, 590);
            label13.Name = "label13";
            label13.Size = new System.Drawing.Size(51, 13);
            label13.TabIndex = 51;
            label13.Text = "% of data";
            // 
            // label14
            // 
            label14.AutoSize = true;
            label14.Location = new System.Drawing.Point(13, 590);
            label14.Name = "label14";
            label14.Size = new System.Drawing.Size(69, 13);
            label14.TabIndex = 49;
            label14.Text = "Truncate last";
            // 
            // lblSubstrateRoughness
            // 
            lblSubstrateRoughness.AutoSize = true;
            lblSubstrateRoughness.Location = new System.Drawing.Point(18, 508);
            lblSubstrateRoughness.Name = "lblSubstrateRoughness";
            lblSubstrateRoughness.Size = new System.Drawing.Size(112, 13);
            lblSubstrateRoughness.TabIndex = 56;
            lblSubstrateRoughness.Text = "Substrate Roughness:";
            // 
            // lblPitPoints
            // 
            lblPitPoints.AutoSize = true;
            lblPitPoints.Location = new System.Drawing.Point(18, 642);
            lblPitPoints.Name = "lblPitPoints";
            lblPitPoints.Size = new System.Drawing.Size(92, 13);
            lblPitPoints.TabIndex = 58;
            lblPitPoints.Text = "Points in each Pit:";
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(135, 663);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 22);
            this.btnClose.TabIndex = 7;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            // 
            // zgcScan
            // 
            this.zgcScan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.zgcScan.Location = new System.Drawing.Point(5, 10);
            this.zgcScan.Name = "zgcScan";
            this.zgcScan.ScrollGrace = 0D;
            this.zgcScan.ScrollMaxX = 0D;
            this.zgcScan.ScrollMaxY = 0D;
            this.zgcScan.ScrollMaxY2 = 0D;
            this.zgcScan.ScrollMinX = 0D;
            this.zgcScan.ScrollMinY = 0D;
            this.zgcScan.ScrollMinY2 = 0D;
            this.zgcScan.Size = new System.Drawing.Size(494, 361);
            this.zgcScan.TabIndex = 35;
            // 
            // lstPits
            // 
            this.lstPits.FormattingEnabled = true;
            this.lstPits.Items.AddRange(new object[] {
            ""});
            this.lstPits.Location = new System.Drawing.Point(115, 244);
            this.lstPits.Name = "lstPits";
            this.lstPits.SelectionMode = System.Windows.Forms.SelectionMode.None;
            this.lstPits.Size = new System.Drawing.Size(108, 69);
            this.lstPits.TabIndex = 37;
            // 
            // txtPitDepth
            // 
            this.txtPitDepth.Location = new System.Drawing.Point(118, 189);
            this.txtPitDepth.Name = "txtPitDepth";
            this.txtPitDepth.ReadOnly = true;
            this.txtPitDepth.Size = new System.Drawing.Size(105, 20);
            this.txtPitDepth.TabIndex = 40;
            // 
            // txtNumPits
            // 
            this.txtNumPits.Location = new System.Drawing.Point(118, 218);
            this.txtNumPits.Name = "txtNumPits";
            this.txtNumPits.ReadOnly = true;
            this.txtNumPits.Size = new System.Drawing.Size(105, 20);
            this.txtNumPits.TabIndex = 42;
            // 
            // chkFilterPeaks
            // 
            this.chkFilterPeaks.AutoSize = true;
            this.chkFilterPeaks.Checked = true;
            this.chkFilterPeaks.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkFilterPeaks.Location = new System.Drawing.Point(15, 614);
            this.chkFilterPeaks.Name = "chkFilterPeaks";
            this.chkFilterPeaks.Size = new System.Drawing.Size(81, 17);
            this.chkFilterPeaks.TabIndex = 45;
            this.chkFilterPeaks.Text = "Filter Peaks";
            this.chkFilterPeaks.UseVisualStyleBackColor = true;
            // 
            // numPeakThreshold
            // 
            this.numPeakThreshold.DecimalPlaces = 1;
            this.numPeakThreshold.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numPeakThreshold.Location = new System.Drawing.Point(149, 627);
            this.numPeakThreshold.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.numPeakThreshold.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numPeakThreshold.Name = "numPeakThreshold";
            this.numPeakThreshold.Size = new System.Drawing.Size(59, 20);
            this.numPeakThreshold.TabIndex = 44;
            this.numPeakThreshold.Value = new decimal(new int[] {
            40,
            0,
            0,
            0});
            // 
            // zgcRollingGraph
            // 
            this.zgcRollingGraph.Dock = System.Windows.Forms.DockStyle.Fill;
            this.zgcRollingGraph.Location = new System.Drawing.Point(5, 0);
            this.zgcRollingGraph.Name = "zgcRollingGraph";
            this.zgcRollingGraph.ScrollGrace = 0D;
            this.zgcRollingGraph.ScrollMaxX = 0D;
            this.zgcRollingGraph.ScrollMaxY = 0D;
            this.zgcRollingGraph.ScrollMaxY2 = 0D;
            this.zgcRollingGraph.ScrollMinX = 0D;
            this.zgcRollingGraph.ScrollMinY = 0D;
            this.zgcRollingGraph.ScrollMinY2 = 0D;
            this.zgcRollingGraph.Size = new System.Drawing.Size(494, 321);
            this.zgcRollingGraph.TabIndex = 46;
            // 
            // txtRollingGraphDisplay
            // 
            this.txtRollingGraphDisplay.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRollingGraphDisplay.Location = new System.Drawing.Point(24, 356);
            this.txtRollingGraphDisplay.Name = "txtRollingGraphDisplay";
            this.txtRollingGraphDisplay.Size = new System.Drawing.Size(160, 32);
            this.txtRollingGraphDisplay.TabIndex = 48;
            // 
            // numBoundaryLow
            // 
            this.numBoundaryLow.DecimalPlaces = 2;
            this.numBoundaryLow.Location = new System.Drawing.Point(23, 431);
            this.numBoundaryLow.Maximum = new decimal(new int[] {
            100000000,
            0,
            0,
            0});
            this.numBoundaryLow.Name = "numBoundaryLow";
            this.numBoundaryLow.Size = new System.Drawing.Size(68, 20);
            this.numBoundaryLow.TabIndex = 50;
            this.numBoundaryLow.Value = new decimal(new int[] {
            19,
            0,
            0,
            0});
            // 
            // numBoundaryHigh
            // 
            this.numBoundaryHigh.DecimalPlaces = 2;
            this.numBoundaryHigh.Location = new System.Drawing.Point(122, 432);
            this.numBoundaryHigh.Maximum = new decimal(new int[] {
            100000000,
            0,
            0,
            0});
            this.numBoundaryHigh.Name = "numBoundaryHigh";
            this.numBoundaryHigh.Size = new System.Drawing.Size(68, 20);
            this.numBoundaryHigh.TabIndex = 50;
            this.numBoundaryHigh.Value = new decimal(new int[] {
            21,
            0,
            0,
            0});
            // 
            // btnClearRollingGraph
            // 
            this.btnClearRollingGraph.Location = new System.Drawing.Point(71, 464);
            this.btnClearRollingGraph.Name = "btnClearRollingGraph";
            this.btnClearRollingGraph.Size = new System.Drawing.Size(75, 23);
            this.btnClearRollingGraph.TabIndex = 52;
            this.btnClearRollingGraph.Text = "Clear";
            this.btnClearRollingGraph.UseVisualStyleBackColor = true;
            // 
            // numThresholdParam
            // 
            this.numThresholdParam.DecimalPlaces = 3;
            this.numThresholdParam.Increment = new decimal(new int[] {
            5,
            0,
            0,
            131072});
            this.numThresholdParam.Location = new System.Drawing.Point(151, 528);
            this.numThresholdParam.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numThresholdParam.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            196608});
            this.numThresholdParam.Name = "numThresholdParam";
            this.numThresholdParam.Size = new System.Drawing.Size(67, 20);
            this.numThresholdParam.TabIndex = 54;
            this.numThresholdParam.Value = new decimal(new int[] {
            65,
            0,
            0,
            131072});
            // 
            // numMinPitWidth
            // 
            this.numMinPitWidth.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numMinPitWidth.Location = new System.Drawing.Point(150, 580);
            this.numMinPitWidth.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numMinPitWidth.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numMinPitWidth.Name = "numMinPitWidth";
            this.numMinPitWidth.Size = new System.Drawing.Size(67, 20);
            this.numMinPitWidth.TabIndex = 54;
            this.numMinPitWidth.Value = new decimal(new int[] {
            15,
            0,
            0,
            0});
            // 
            // numSmoothingWidth
            // 
            this.numSmoothingWidth.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numSmoothingWidth.Location = new System.Drawing.Point(150, 554);
            this.numSmoothingWidth.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numSmoothingWidth.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numSmoothingWidth.Name = "numSmoothingWidth";
            this.numSmoothingWidth.Size = new System.Drawing.Size(67, 20);
            this.numSmoothingWidth.TabIndex = 54;
            this.numSmoothingWidth.Value = new decimal(new int[] {
            65,
            0,
            0,
            0});
            // 
            // numCompressionFactor
            // 
            this.numCompressionFactor.DecimalPlaces = 3;
            this.numCompressionFactor.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.numCompressionFactor.Location = new System.Drawing.Point(150, 608);
            this.numCompressionFactor.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numCompressionFactor.Name = "numCompressionFactor";
            this.numCompressionFactor.Size = new System.Drawing.Size(67, 20);
            this.numCompressionFactor.TabIndex = 54;
            this.numCompressionFactor.Value = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.IsSplitterFixed = true;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(label11);
            this.splitContainer1.Panel1.Controls.Add(this.numTruncateFirst);
            this.splitContainer1.Panel1.Controls.Add(label12);
            this.splitContainer1.Panel1.Controls.Add(label13);
            this.splitContainer1.Panel1.Controls.Add(this.numTruncateLast);
            this.splitContainer1.Panel1.Controls.Add(label14);
            this.splitContainer1.Panel1.Controls.Add(grpConnection);
            this.splitContainer1.Panel1.Controls.Add(grpLaserPresets);
            this.splitContainer1.Panel1.Controls.Add(grpControl);
            this.splitContainer1.Panel1.Controls.Add(this.numMaxPeakWidth);
            this.splitContainer1.Panel1.Controls.Add(this.numPeakHysteresis);
            this.splitContainer1.Panel1.Controls.Add(lblMaxPeakWidth);
            this.splitContainer1.Panel1.Controls.Add(lblPeakHysteresis);
            this.splitContainer1.Panel1.Controls.Add(this.numPeakThreshold);
            this.splitContainer1.Panel1.Controls.Add(lblPeakThreshold);
            this.splitContainer1.Panel1.Controls.Add(this.chkFilterPeaks);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer2);
            this.splitContainer1.Size = new System.Drawing.Size(1008, 706);
            this.splitContainer1.SplitterDistance = 265;
            this.splitContainer1.TabIndex = 55;
            // 
            // numTruncateFirst
            // 
            this.numTruncateFirst.Location = new System.Drawing.Point(94, 566);
            this.numTruncateFirst.Name = "numTruncateFirst";
            this.numTruncateFirst.Size = new System.Drawing.Size(58, 20);
            this.numTruncateFirst.TabIndex = 47;
            this.numTruncateFirst.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // numTruncateLast
            // 
            this.numTruncateLast.Location = new System.Drawing.Point(94, 586);
            this.numTruncateLast.Name = "numTruncateLast";
            this.numTruncateLast.Size = new System.Drawing.Size(58, 20);
            this.numTruncateLast.TabIndex = 50;
            this.numTruncateLast.Value = new decimal(new int[] {
            20,
            0,
            0,
            0});
            // 
            // numMaxPeakWidth
            // 
            this.numMaxPeakWidth.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numMaxPeakWidth.Location = new System.Drawing.Point(149, 670);
            this.numMaxPeakWidth.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.numMaxPeakWidth.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numMaxPeakWidth.Name = "numMaxPeakWidth";
            this.numMaxPeakWidth.Size = new System.Drawing.Size(59, 20);
            this.numMaxPeakWidth.TabIndex = 44;
            this.numMaxPeakWidth.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // numPeakHysteresis
            // 
            this.numPeakHysteresis.DecimalPlaces = 1;
            this.numPeakHysteresis.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numPeakHysteresis.Location = new System.Drawing.Point(149, 649);
            this.numPeakHysteresis.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.numPeakHysteresis.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numPeakHysteresis.Name = "numPeakHysteresis";
            this.numPeakHysteresis.Size = new System.Drawing.Size(59, 20);
            this.numPeakHysteresis.TabIndex = 44;
            this.numPeakHysteresis.Value = new decimal(new int[] {
            20,
            0,
            0,
            0});
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.splitContainer2.IsSplitterFixed = true;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.splitContainer3);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.numPitPoints);
            this.splitContainer2.Panel2.Controls.Add(lblPitPoints);
            this.splitContainer2.Panel2.Controls.Add(this.numSubstrateRoughness);
            this.splitContainer2.Panel2.Controls.Add(lblSubstrateRoughness);
            this.splitContainer2.Panel2.Controls.Add(this.chkDebugMode);
            this.splitContainer2.Panel2.Controls.Add(grpAcquisitionSettings);
            this.splitContainer2.Panel2.Controls.Add(this.numCompressionFactor);
            this.splitContainer2.Panel2.Controls.Add(this.btnClose);
            this.splitContainer2.Panel2.Controls.Add(this.numSmoothingWidth);
            this.splitContainer2.Panel2.Controls.Add(this.lstPits);
            this.splitContainer2.Panel2.Controls.Add(this.numMinPitWidth);
            this.splitContainer2.Panel2.Controls.Add(lblPitDepths);
            this.splitContainer2.Panel2.Controls.Add(lblCompressionFactor);
            this.splitContainer2.Panel2.Controls.Add(lblPitDepth);
            this.splitContainer2.Panel2.Controls.Add(this.numThresholdParam);
            this.splitContainer2.Panel2.Controls.Add(this.txtPitDepth);
            this.splitContainer2.Panel2.Controls.Add(lblSmoothingWidth);
            this.splitContainer2.Panel2.Controls.Add(lblNumPits);
            this.splitContainer2.Panel2.Controls.Add(lblMinPitWidth);
            this.splitContainer2.Panel2.Controls.Add(this.txtNumPits);
            this.splitContainer2.Panel2.Controls.Add(lblThresholdParam);
            this.splitContainer2.Panel2.Controls.Add(lblRollingGraphDisplay);
            this.splitContainer2.Panel2.Controls.Add(this.btnClearRollingGraph);
            this.splitContainer2.Panel2.Controls.Add(label1);
            this.splitContainer2.Panel2.Controls.Add(label10);
            this.splitContainer2.Panel2.Controls.Add(this.txtRollingGraphDisplay);
            this.splitContainer2.Panel2.Controls.Add(this.numBoundaryHigh);
            this.splitContainer2.Panel2.Controls.Add(lblBoundaryLines);
            this.splitContainer2.Panel2.Controls.Add(this.numBoundaryLow);
            this.splitContainer2.Size = new System.Drawing.Size(739, 706);
            this.splitContainer2.SplitterDistance = 504;
            this.splitContainer2.TabIndex = 0;
            // 
            // splitContainer3
            // 
            this.splitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer3.Location = new System.Drawing.Point(0, 0);
            this.splitContainer3.Name = "splitContainer3";
            this.splitContainer3.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer3.Panel1
            // 
            this.splitContainer3.Panel1.Controls.Add(this.zgcScan);
            this.splitContainer3.Panel1.Padding = new System.Windows.Forms.Padding(5, 10, 5, 0);
            // 
            // splitContainer3.Panel2
            // 
            this.splitContainer3.Panel2.Controls.Add(this.zgcRollingGraph);
            this.splitContainer3.Panel2.Padding = new System.Windows.Forms.Padding(5, 0, 5, 10);
            this.splitContainer3.Size = new System.Drawing.Size(504, 706);
            this.splitContainer3.SplitterDistance = 371;
            this.splitContainer3.TabIndex = 47;
            // 
            // numPitPoints
            // 
            this.numPitPoints.Location = new System.Drawing.Point(150, 635);
            this.numPitPoints.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.numPitPoints.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numPitPoints.Name = "numPitPoints";
            this.numPitPoints.Size = new System.Drawing.Size(67, 20);
            this.numPitPoints.TabIndex = 59;
            this.numPitPoints.Value = new decimal(new int[] {
            4,
            0,
            0,
            0});
            // 
            // numSubstrateRoughness
            // 
            this.numSubstrateRoughness.DecimalPlaces = 1;
            this.numSubstrateRoughness.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numSubstrateRoughness.Location = new System.Drawing.Point(150, 504);
            this.numSubstrateRoughness.Maximum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numSubstrateRoughness.Minimum = new decimal(new int[] {
            5,
            0,
            0,
            -2147483648});
            this.numSubstrateRoughness.Name = "numSubstrateRoughness";
            this.numSubstrateRoughness.Size = new System.Drawing.Size(67, 20);
            this.numSubstrateRoughness.TabIndex = 57;
            // 
            // chkDebugMode
            // 
            this.chkDebugMode.AutoSize = true;
            this.chkDebugMode.Location = new System.Drawing.Point(29, 670);
            this.chkDebugMode.Name = "chkDebugMode";
            this.chkDebugMode.Size = new System.Drawing.Size(88, 17);
            this.chkDebugMode.TabIndex = 55;
            this.chkDebugMode.Text = "Debug Mode";
            this.chkDebugMode.UseVisualStyleBackColor = true;
            // 
            // DFM_Form_old
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1008, 706);
            this.Controls.Add(this.splitContainer1);
            this.MinimumSize = new System.Drawing.Size(661, 710);
            this.Name = "DFM_Form_old";
            this.Text = "DFM";
            grpConnection.ResumeLayout(false);
            grpConnection.PerformLayout();
            grpLaserPresets.ResumeLayout(false);
            grpLaserPresets.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numRunTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPulseCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numDutyCycle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numGatingFreq)).EndInit();
            grpControl.ResumeLayout(false);
            grpControl.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numRepeatCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numRepeatTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numDAQStop)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numDAQStart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numLaserStart)).EndInit();
            grpAcquisitionSettings.ResumeLayout(false);
            grpAcquisitionSettings.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numPeakThreshold)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numBoundaryLow)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numBoundaryHigh)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numThresholdParam)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numMinPitWidth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSmoothingWidth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numCompressionFactor)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.numTruncateFirst)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numTruncateLast)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numMaxPeakWidth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPeakHysteresis)).EndInit();
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            this.splitContainer2.Panel2.PerformLayout();
            this.splitContainer2.ResumeLayout(false);
            this.splitContainer3.Panel1.ResumeLayout(false);
            this.splitContainer3.Panel2.ResumeLayout(false);
            this.splitContainer3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.numPitPoints)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSubstrateRoughness)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnConnect;
        private System.Windows.Forms.ComboBox cmbSensorType;
        private System.Windows.Forms.ComboBox cmbSensorModel;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.NumericUpDown numPulseCount;
        private System.Windows.Forms.CheckBox chkPulseStopNeeded;
        private System.Windows.Forms.NumericUpDown numGatingFreq;
        private System.Windows.Forms.NumericUpDown numDutyCycle;
        private System.Windows.Forms.Button btnUpdatePresets;
        private System.Windows.Forms.Button btnCancelPresets;
        private System.Windows.Forms.NumericUpDown numDAQStart;
        private System.Windows.Forms.NumericUpDown numLaserStart;
        private System.Windows.Forms.NumericUpDown numDAQStop;
        private System.Windows.Forms.Button btnPrepare;
        private System.Windows.Forms.Button btnRun;
        private System.Windows.Forms.Button btnStop;
        private ZedGraph.ZedGraphControl zgcScan;
        private System.Windows.Forms.ComboBox cmbPLCBaudrate;
        private System.Windows.Forms.ComboBox cmbSensorBaudrate;
        private System.Windows.Forms.ComboBox cmbPLCPort;
        private System.Windows.Forms.ComboBox cmbSensorPort;
        private System.Windows.Forms.Button btnChooseDataFolder;
        private System.Windows.Forms.TextBox txtDataFolder;
        private System.Windows.Forms.TextBox txtFilenamePattern;
        private System.Windows.Forms.Button btnLoadLastScan;
        private System.Windows.Forms.FolderBrowserDialog folderBrowser;
        private System.Windows.Forms.ListBox lstPits;
        private System.Windows.Forms.TextBox txtPitDepth;
        private System.Windows.Forms.TextBox txtNumPits;
        private System.Windows.Forms.RadioButton radPulseCount;
        private System.Windows.Forms.RadioButton radRunTime;
        private System.Windows.Forms.NumericUpDown numRunTime;
        private System.Windows.Forms.CheckBox chkConnectToSensor;
        private System.Windows.Forms.Label lblDAQStart;
        private System.Windows.Forms.NumericUpDown numRepeatTime;
        private System.Windows.Forms.CheckBox chkRepeat;
        private System.Windows.Forms.CheckBox chkFilterPeaks;
        private System.Windows.Forms.NumericUpDown numPeakThreshold;
        private ZedGraph.ZedGraphControl zgcRollingGraph;
        private System.Windows.Forms.TextBox txtRollingGraphDisplay;
        private System.Windows.Forms.NumericUpDown numBoundaryLow;
        private System.Windows.Forms.NumericUpDown numBoundaryHigh;
        private System.Windows.Forms.Button btnClearRollingGraph;
        private System.Windows.Forms.NumericUpDown numThresholdParam;
        private System.Windows.Forms.NumericUpDown numMinPitWidth;
        private System.Windows.Forms.NumericUpDown numSmoothingWidth;
        private System.Windows.Forms.NumericUpDown numCompressionFactor;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.SplitContainer splitContainer3;
        private System.Windows.Forms.NumericUpDown numPeakHysteresis;
        private System.Windows.Forms.NumericUpDown numMaxPeakWidth;
        private System.Windows.Forms.CheckBox chkDebugMode;
        private System.Windows.Forms.NumericUpDown numTruncateFirst;
        private System.Windows.Forms.NumericUpDown numTruncateLast;
        private System.Windows.Forms.NumericUpDown numSubstrateRoughness;
        private System.Windows.Forms.NumericUpDown numRepeatCount;
        private System.Windows.Forms.NumericUpDown numPitPoints;
        private System.Windows.Forms.Button btnShowGalil;
        private System.Windows.Forms.CheckBox chkConnectToLaser;
    }
}