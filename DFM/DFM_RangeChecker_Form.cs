﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using System.Threading;
using System.IO.Ports;
using System.IO;

using PaintAnalysis.Common;
using SensorControl;
using SensorControl.Sensors;
using ZedGraph;
using Analysis;
using DFM.Scans;

namespace DFM
{
    public partial class DFM_RangeChecker_Form : Form
    {
        #region Statics

        private static string connectText = "Connect";
        private static string disconnectText = "Disconnect";

        #endregion

        #region Private Variables

        /// <summary>
        /// The available sensors
        /// </summary>
        private ISensor[] sensors;

        /// <summary>
        /// The current sensor
        /// </summary>
        private ISensor sensor;

        /// <summary>
        /// The current state of the form
        /// </summary>
        private States state;
              
        private BackgroundWorker worker;

        private OutputManager output;

        #endregion

        #region States

        public enum States
        {
            DISCONNECTED,
            CONNECTED,
            RUNNING
        }

        public States State
        {
            get { return state; }
            set
            {
                state = value;
                switch (value)
                {
                    case States.DISCONNECTED:
                        cmbSensorModel.Enabled = true;
                        cmbSensorType.Enabled = true;
                        cmbSensorPort.Enabled = true;
                        cmbSensorBaudrate.Enabled = true;
                        btnConnect.Enabled = true;

                        btnRunScan.Enabled = false;
                        btnStopScan.Enabled = false;
                                                
                        chkFilterPeaks.Enabled = true;
                        numPeakThreshold.Enabled = chkFilterPeaks.Checked;
                        numPeakHysteresis.Enabled = chkFilterPeaks.Checked;
                        numMaxPeakWidth.Enabled = chkFilterPeaks.Checked;
                        break;

                    case States.CONNECTED:
                        cmbSensorModel.Enabled = false;
                        cmbSensorType.Enabled = false;
                        cmbSensorPort.Enabled = false;
                        cmbSensorBaudrate.Enabled = false;
                        btnConnect.Enabled = true;
                        
                        btnRunScan.Enabled = true;
                        btnStopScan.Enabled = false;
                        
                        chkFilterPeaks.Enabled = true;
                        numPeakThreshold.Enabled = chkFilterPeaks.Checked;
                        numPeakHysteresis.Enabled = chkFilterPeaks.Checked;
                        numMaxPeakWidth.Enabled = chkFilterPeaks.Checked;
                        break;

                    case States.RUNNING:
                        cmbSensorModel.Enabled = false;
                        cmbSensorType.Enabled = false;
                        cmbSensorPort.Enabled = false;
                        cmbSensorBaudrate.Enabled = false;
                        btnConnect.Enabled = false;

                        btnRunScan.Enabled = false;
                        btnStopScan.Enabled = true;
                        
                        chkFilterPeaks.Enabled = false;
                        numPeakThreshold.Enabled = false;
                        numPeakHysteresis.Enabled = false;
                        numMaxPeakWidth.Enabled = false;
                        break;

                    default:
                        throw new ApplicationException("Tried to set invalid state");
                }
            }            
        }

        #endregion

        public DFM_RangeChecker_Form()
        {
            InitializeComponent();

            // set up sensors and laser
            sensors = new ISensor[2];
            sensors[0] = new ILD2200_MESensor();
            sensors[1] = new ILD1700_MESensor();
                        
            // set default stuff
            UpdateSensorModels();
            UpdateSensorTypes();
            UpdateSensorPorts();
            UpdateSensorBaudrate();
            
            // create graph curve
            GraphPane pane = zgcScan.GraphPane;
            PointPairList list = new PointPairList();
            LineItem curve = pane.AddCurve("distance", list, Color.Black, SymbolType.None);
            
            // hide legend
            curve.Label.IsVisible = false;
            // set axis labels
            pane.Title.Text = "Distance";
            pane.XAxis.Title.Text = "Time (s)";
            pane.YAxis.Title.Text = "Distance (microns)";


            GraphPane pane2 = zgcRollingGraph.GraphPane;
            IPointList list2 = new RollingPointPairList(11);
            LineItem curve2 = pane2.AddCurve("data", list2, Color.Blue, SymbolType.Circle);
            curve2.Label.IsVisible = true;
            pane2.Title.Text = "Readings";
            pane2.XAxis.Title.Text = "Reading #";
            pane2.YAxis.Title.Text = "Distance (microns)";

            // boundary lines
            PointPairList boundaryList1 = new PointPairList();
            LineItem boundaryCurve1 = pane2.AddCurve("boundary1", boundaryList1, Color.Red, SymbolType.None);
            boundaryCurve1.Label.IsVisible = false;

            PointPairList boundaryList2 = new PointPairList();
            LineItem boundaryCurve2 = pane2.AddCurve("boundary2", boundaryList2, Color.Red, SymbolType.None);
            boundaryCurve2.Label.IsVisible = false;

            numBoundaryLow.Value = (decimal)19.0;
            numBoundaryHigh.Value = (decimal)21.0;

            ClearRollingGraphData();
            UpdateBoundaryLines();

            // defaults
            chkFilterPeaks.Checked = true;
            numPeakThreshold.Value = (decimal)40.0;
            numPeakThreshold.Enabled = true;
            numPeakHysteresis.Value = (decimal)20.0;
            numPeakHysteresis.Enabled = true;
            numMaxPeakWidth.Value = (decimal)100;
            numMaxPeakWidth.Enabled = true;

            // set default state
            State = States.DISCONNECTED;

            // create bg worker
            worker = new BackgroundWorker();
            worker.WorkerReportsProgress = true;
            worker.WorkerSupportsCancellation = true;

            worker.DoWork += new DoWorkEventHandler(worker_DoWork);
            worker.ProgressChanged += new ProgressChangedEventHandler(worker_ProgressChanged);
            worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(worker_RunWorkerCompleted);

            // add event handlers
            // form closing
            btnClose.Click += new EventHandler(btnClose_Click);
            this.FormClosing += new FormClosingEventHandler(LaserControlForm_FormClosing);
            
            cmbSensorModel.SelectedIndexChanged += new EventHandler(cmbSensorModel_SelectedIndexChanged);
            btnConnect.Click += new EventHandler(btnConnect_Click);
            btnRunScan.Click += new EventHandler(btnRunScan_Click);
            btnStopScan.Click += new EventHandler(btnStopScan_Click);

            btnChooseDataFolder.Click += new EventHandler(btnChooseDataFolder_Click);
            btnLoadLastScan.Click += new EventHandler(btnLoadLastScan_Click);
                        
            chkFilterPeaks.CheckedChanged += new EventHandler(delegate(object sender, EventArgs e)
            {
                numPeakThreshold.Enabled = chkFilterPeaks.Checked;
                numPeakHysteresis.Enabled = chkFilterPeaks.Checked;
                numMaxPeakWidth.Enabled = chkFilterPeaks.Checked;
            });

            numBoundaryLow.ValueChanged += new EventHandler(numBoundary_ValueChanged);
            numBoundaryHigh.ValueChanged += new EventHandler(numBoundary_ValueChanged);
            btnClearRollingGraph.Click += new EventHandler(btnClearRollingGraph_Click);

            output = new OutputManager(rtbOutput);
        }
                               

        #region Rolling Graph
        
        private double[] readingsAverageBuffer = new double[3];
        private int bufferIndex = 0;

        private void AddRollingGraphData(double value)
        {
            CurveItem dataCurve = zgcRollingGraph.GraphPane.CurveList["data"];
            int x = 1; // start at 1
            if (dataCurve.Points.Count > 0)
            {
                x = (int)(dataCurve.Points[dataCurve.Points.Count - 1].X) + 1;
            }
            dataCurve.AddPoint(x, value);
            
            // fix scale
            if (x > 10)
            {
                Scale scale = zgcRollingGraph.GraphPane.XAxis.Scale;
                scale.Min += 1;
                scale.Max += 1;
            }

            zgcRollingGraph.AxisChange();
            zgcRollingGraph.Invalidate();

            // work out new average
            readingsAverageBuffer[bufferIndex++] = value;
            bufferIndex %= readingsAverageBuffer.Length;
            double average = 0.0;
            int i;
            for (i = 0; i < readingsAverageBuffer.Length; i++)
            {
                if (readingsAverageBuffer[i] == 0)
                {
                    break;
                }
                average += readingsAverageBuffer[i];
            }

            if (i > 0) average /= i;

            txtRollingGraphDisplay.Text = average.ToString("0.00");
        }

        private void ClearRollingGraphData()
        {
            Scale scale = zgcRollingGraph.GraphPane.XAxis.Scale;
            scale.Min = 0;
            scale.Max = 12;

            zgcRollingGraph.GraphPane.CurveList["data"].Clear();

            zgcRollingGraph.AxisChange();
            zgcRollingGraph.Invalidate();

            for (int i = 0; i < readingsAverageBuffer.Length; i++)
            {
                readingsAverageBuffer[i] = 0.0;
            }
            txtRollingGraphDisplay.Text = "";
        }

        private void UpdateBoundaryLines()
        {
            CurveItem boundaryCurve1 = zgcRollingGraph.GraphPane.CurveList["boundary1"];
            CurveItem boundaryCurve2 = zgcRollingGraph.GraphPane.CurveList["boundary2"];

            boundaryCurve1.Clear();
            boundaryCurve2.Clear();

            double line1 = (double)numBoundaryLow.Value;
            double line2 = (double)numBoundaryHigh.Value;

            boundaryCurve1.AddPoint(0.0, line1);
            boundaryCurve1.AddPoint(10000.0, line1);

            boundaryCurve2.AddPoint(0.0, line2);
            boundaryCurve2.AddPoint(10000.0, line2);

            zgcRollingGraph.AxisChange();
            zgcRollingGraph.Invalidate();
        }

        void numBoundary_ValueChanged(object sender, EventArgs e)
        {
            UpdateBoundaryLines();
        }

        void btnClearRollingGraph_Click(object sender, EventArgs e)
        {
            ClearRollingGraphData();
        }

        #endregion
        
        #region Update Functions

        #region Form Closing

        private void BeforeCloseForm()
        {
            // stop any running measurments
            if (State == States.RUNNING)
            {
                StopScan();
            }

            // disconnect from any open ports
            if (State == States.CONNECTED)
            {
                Disconnect();
            }
        }

        #endregion

        #region Connection Updates

        private void UpdateSensorModels()
        {
            cmbSensorModel.Items.Clear();

            // add the sensor models
            foreach (ISensor s in sensors)
            {
                cmbSensorModel.Items.Add(s.Properties.ModelName);
            }

            cmbSensorModel.SelectedIndex = 0;
        }

        private void UpdateSensorTypes()
        {
            cmbSensorType.Items.Clear();

            // get selected sensor
            ISensor selectedSensor = sensors[cmbSensorModel.SelectedIndex];

            // add the sensor types
            foreach (string type in selectedSensor.SensorTypes)
            {
                cmbSensorType.Items.Add(type);
            }

            cmbSensorType.SelectedIndex = 0;
        }

        private void UpdateSensorPorts()
        {
            string[] ports = SerialPort.GetPortNames();
            Array.Sort(ports);
            
            cmbSensorPort.Items.Clear();
            cmbSensorPort.Items.AddRange(ports);
            cmbSensorPort.SelectedIndex = 0;
        }
        
        private void UpdateSensorBaudrate()
        {
            cmbSensorBaudrate.Items.Clear();
            cmbSensorBaudrate.Items.Add("Auto detect");

            int[] baudrates = sensors[cmbSensorModel.SelectedIndex].BaudRates;

            foreach (int i in baudrates)
            {
                cmbSensorBaudrate.Items.Add(i.ToString());
            }

            cmbSensorBaudrate.SelectedIndex = 0;
        }
        
        #endregion

        #region Connection

        private void Connect()
        {
            btnConnect.Enabled = false;

            if (btnConnect.Text == DFM_RangeChecker_Form.connectText)
            {
                // connecting

                // update sensor and other selections
                sensor = sensors[cmbSensorModel.SelectedIndex];

                try
                {
                    // try and connect to sensor
                    if (cmbSensorBaudrate.SelectedIndex == 0)
                    {
                        sensor.Connect(cmbSensorPort.SelectedItem.ToString());
                        cmbSensorBaudrate.SelectedItem = sensor.BaudRate.ToString();
                    }
                    else
                    {
                        sensor.Connect(cmbSensorPort.SelectedItem.ToString(), Convert.ToInt32(cmbSensorBaudrate.SelectedItem));
                    }
                }
                catch (Exception ex)
                {
                    sensor.Disconnect();
                    btnConnect.Enabled = true;

                    string message = "There was an error connecting to the sensor. Try changing the COM ports or baud rates. The error was '" + ex.Message + "'";
                    MessageBox.Show(message);

                    return;
                }

                sensor.ReadingReference = ReadingReferences.SMR;
                
                btnConnect.Text = disconnectText;
                State = States.CONNECTED;
            }
            else
            {
                // disconnecting
                Disconnect();
            }

            btnConnect.Enabled = true;
        }

        private void Disconnect()
        {
            // disconnect from sensor
            sensor.Disconnect();

            State = States.DISCONNECTED;
            btnConnect.Text = "Connect";
        }

        #endregion
        
        #region Start/Stop

        private bool Prepare()
        {            
            if (txtDataFolder.Text.Trim() == "" || !Directory.Exists(txtDataFolder.Text) || txtFilenamePattern.Text.Trim() == "")
            {
                MessageBox.Show("Please select a valid folder and enter a filename pattern");
                return false;
            }

            try
            {
                // get frequency
                double frequency = Convert.ToDouble(sensor.Parameters["frequency"].Get());

                // set sensor time increment
                sensor.TimeIncrement = 1.0 / frequency;
                sensor.CurrentTime = 0;

                sensor.EnsureReadingsInitialised();
                sensor.PortBuffer.Clear();
            }
            catch (Exception ex)
            {
                MessageBox.Show("There was an error setting up the sensor: " + ex.Message);
                return false;
            }

            GraphPane pane = zgcScan.GraphPane;
            pane.CurveList.Clear();
            zgcScan.AxisChange();
            zgcScan.Invalidate();

            return true;
        }

        private void RunScan()
        {
            if (!Prepare()) return;
            
            StopWorker();

            // start the async operation
            worker.RunWorkerAsync();

            // change state
            State = States.RUNNING;
        }

        private void StopScan()
        {
            btnStopScan.Enabled = false;

            // stop worker
            StopWorker();
            
            btnStopScan.Enabled = true;

            StoppedMeasurements();
        }

        private void StopWorker()
        {
            int i = 0;
            worker.CancelAsync();
            
            // stop worker
            while (i < 5 && worker.IsBusy)
            {
                // wait until it does                
                Thread.Sleep(100 * (i + 1));
                i += 1;
            }
        }

        private void StoppedMeasurements()
        {
            State = States.CONNECTED;
        }

        #endregion
        
        #region Background Worker

        void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            // restore form
            StoppedMeasurements();

            // indicates an exception was raised
            if (e.Error != null)
            {
                MessageBox.Show("Error running scan: " + e.Error.Message);
                return;
            }

            if (e.Cancelled)
            {
                MessageBox.Show("Measurements were cancelled");
                return;
            }

            output.WriteLine("Running analysis");
            LoadLastScan();
        }

        void worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {            
            string message = e.UserState.ToString();
            output.WriteLine(message);
        }

        void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = sender as BackgroundWorker;
            
            // time between reading blocks to look for sample in ms
            int blockTime = 200;
            SensorReading[] buffer;
            List<SensorReading> readings_list = new List<SensorReading>();

            // number of sequential readings error/non-error before can determine an edge
            // will require a maximum of 0.5 seconds of errors/non-errors to find edge
            int threshold = Math.Min(200, (int)Convert.ToInt32(sensor.Parameters["frequency"].Get()) / 2);

            // buffer to store readings which make up the edges, so they can be included
            SensorReading[] edgeBuffer = new SensorReading[threshold];


            // set up graph
            GraphPane pane = zgcScan.GraphPane;

            PointPairList currentList = new PointPairList();
            LineItem curve = zgcScan.GraphPane.AddCurve("Raw data", currentList, Color.Red, SymbolType.None);

            // use appropriate vertical scale from sensor details
            // as ReadingReference set to SMR in Connect, vertical range is 0 to Sensor.Properties.MR
            // 5% grace
            double grace = sensor.Properties.MR * 0.05;
            pane.YAxis.Scale.Min = Math.Round(-grace);
            pane.YAxis.Scale.Max = Math.Round(sensor.Properties.MR + grace);

            // start horizontal scale at 0, showing 1000 points
            double horizontalSize = 10; // represents 1000 points as x = index * 0.01
            pane.XAxis.Scale.Min = 0;
            pane.XAxis.Scale.Max = horizontalSize;

            // redraw graph
            zgcScan.AxisChange();
            zgcScan.Invalidate();

            worker.ReportProgress(-1, "Set up graph");
            worker.ReportProgress(-1, "Starting data capture");

            // get data
            // start autoupdating
            sensor.PortBuffer.Clear();
            sensor.PortBuffer.AutoUpdate(50);

            // find start and end of sample
            int count; // count of successive errors/non-errors
            int readingCount = 0; // count of readings so far
            for (int j = 0; j < 2; j++)
            {
                // restart counter
                count = 0;

                worker.ReportProgress(-1, "Scanning: j = " + j.ToString());

                while (true)
                {
                    // cancel if necessary
                    if (worker.CancellationPending)
                    {
                        sensor.PortBuffer.StopAutoUpdate();
                        e.Cancel = true;
                        return;
                    }

                    buffer = sensor.TakeReadings();

                    // errors indicate out of range
                    foreach (SensorReading r in buffer)
                    {
                        if (j == 0)
                        {
                            // if finding start
                            if (count >= threshold)
                            {
                                // found start
                                if (count == threshold)
                                {
                                    // copy edge buffer into readings and graph them
                                    foreach (SensorReading edgeR in edgeBuffer)
                                    {
                                        readings_list.Add(edgeR);
                                        currentList.Add((readingCount++ * 0.01), edgeR.Value);
                                    }

                                    // prevent this from happening again
                                    count++;
                                }

                                readings_list.Add(r);
                                currentList.Add((readingCount++ * 0.01), r.Value);
                            }
                            if (count < threshold && !r.IsError)
                            {
                                edgeBuffer[count] = r;
                                count++;
                            }
                            if (count < threshold && r.IsError)
                            {
                                // error resets search for start
                                count = 0;
                            }
                        }
                        else
                        {
                            // finding end
                            if (count >= threshold)
                            {
                                // found end
                                break;
                            }
                            if (r.IsError)
                            {
                                count++;
                            }
                            if (!r.IsError)
                            {
                                readings_list.Add(r);
                                currentList.Add((readingCount++ * 0.01), r.Value);
                                count = 0;
                            }
                        }
                    } // end processing buffer

                    // test x-axis is big enough
                    if (readingCount * 0.01 + 2 > pane.XAxis.Scale.Max)
                    {
                        pane.XAxis.Scale.Max = readingCount * 0.01 + 2;
                    }

                    // redraw graph
                    zgcScan.AxisChange();
                    zgcScan.Invalidate();

                    worker.ReportProgress(-1, "Found end, redrawn graph");

                    // check if reached end/start of scan
                    if (count >= threshold)
                        break;

                    // pause between each buffering block
                    Thread.Sleep(blockTime);

                } // end look for start/end

            } // end reading data loop

            // stop buffering
            sensor.PortBuffer.StopAutoUpdate();

            worker.ReportProgress(-1, "Finished scanning");
            worker.ReportProgress(-1, "Processing data");
                    
            // read data
            SensorReading[] readings = readings_list.ToArray();

            // save readings
            // create x-values and y_values to write to file
            int rLen = readings.Length;
            double[] data_x;
            double[] data_y;
            List<double> list_x = new List<double>();
            List<double> list_y = new List<double>();
            for (int j = 0; j < rLen; j++)
            {
                if (!readings[j].IsError)
                {
                    list_x.Add(readings[j].Time);
                    list_y.Add(readings[j].Value);
                }
            }

            data_x = list_x.ToArray();
            data_y = list_y.ToArray();

            worker.ReportProgress(-1, "Truncating data");
            
            // truncate data
            rLen = data_x.Length;
            if (rLen > 0)
            {
                int start_index = 0;
                int end_index = rLen; // not included

                if (Convert.ToDouble(numTruncateFirst.Value) > 0)
                {
                    start_index = (int)Math.Floor(Convert.ToDouble(numTruncateFirst.Value) / 100 * rLen);
                }
                if (Convert.ToDouble(numTruncateLast.Value) > 0)
                {
                    end_index = rLen - (int)Math.Ceiling(Convert.ToDouble(numTruncateLast.Value) / 100 * rLen);
                }

                if (end_index > start_index)
                {
                    double[] data_x_tmp = new double[end_index - start_index];
                    double[] data_y_tmp = new double[end_index - start_index];

                    for (int i = start_index, j = 0; i < end_index; i++, j++)
                    {
                        data_x_tmp[j] = data_x[i];
                        data_y_tmp[j] = data_y[i];
                    }

                    data_x = data_x_tmp;
                    data_y = data_y_tmp;

                    worker.ReportProgress(-1, "Truncated: from " + start_index.ToString() + " to " + end_index.ToString() + " of " + rLen.ToString());                    
                }
            }

            /*
             * moved noise filtering to LoadScan
            if (chkFilterPeaks.Checked)
            {
                double[] data_y_temp = new double[rLen];
                for (int j = 0; j < rLen; j++)
                {
                    data_y_temp[j] = data_y[j];
                }
                NoiseFilter.Filter(data_y_temp, out data_y, (double)numPeakThreshold.Value / 1000);
            }*/

            worker.ReportProgress(-1, "Getting file info");
            
            rLen = data_x.Length;
            ScanManager scanManager = new ScanManager(txtDataFolder.Text, txtFilenamePattern.Text);
            int nextScanId = scanManager.GetNextScanId();
            ScanFile scanFile = new ScanFile(txtDataFolder.Text, txtFilenamePattern.Text, nextScanId);

            worker.ReportProgress(-1, "Chosen file: " + scanFile.Filename);
            worker.ReportProgress(-1, "Writing to file");                    

            // write data
            DataIO dataIO = new DataIO();
            dataIO.Mode = DataIO.Modes.Write;
            dataIO.Filename = txtDataFolder.Text + scanFile.Filename;
            dataIO.WriteData(data_x, data_y);

            worker.ReportProgress(-1, "Written to file");  
        }

        #endregion

        #region Loading Scans

        bool LoadLastScan()
        {            
            ScanManager scanManager = new ScanManager(txtDataFolder.Text, txtFilenamePattern.Text);
            int lastScanId = scanManager.GetLastScanId();
            if (lastScanId == -1)
            {
                return false;
            }

            return LoadScanResults(lastScanId);
        }

        private bool LoadScanResults(int scanId)
        {            
            ScanFile scanFile = new ScanFile(txtDataFolder.Text, txtFilenamePattern.Text, scanId);

            string file = txtDataFolder.Text + scanFile.FindFilename();
            if (file != "")
            {
                LoadScan(file);
                return true;
            }

            return false;
        }

        private void LoadScan(string filename)
        {
            output.WriteLine("Analysing " + filename);
            output.WriteLine("Setting up analysis stuff");

            DFMAnalysis analysis = new DFMAnalysis();
            analysis.Options = new DFMAnalysisOptions();
            analysis.Options.SmoothingWidth = (int)numSmoothingWidth.Value;
            analysis.Options.ThresholdParameter = (double)numThresholdParam.Value;
            analysis.Options.MinPitWidth = (int)numMinPitWidth.Value;
            analysis.Options.Compression = (double)numCompressionFactor.Value;

            DataIO reader = new DataIO();
            reader.Mode = DataIO.Modes.Read;
            reader.Filename = filename;

            output.WriteLine("Reading file");

            double[] data_x, data_y;
            reader.ReadData(out data_x, out data_y);

            output.WriteLine("Fixing graph");

            // clear old graphs
            zgcScan.GraphPane.CurveList.Clear();
            zgcScan.GraphPane.XAxis.Scale.MinAuto = true;
            zgcScan.GraphPane.XAxis.Scale.MaxAuto = true;
            zgcScan.GraphPane.YAxis.Scale.MinAuto = true;
            zgcScan.GraphPane.YAxis.Scale.MaxAuto = true;
                        
            // add raw data graph
            double[] data_y1000 = new double[data_x.Length];
            for (int i = 0; i < data_x.Length; i++)
            {
                data_y1000[i] = data_y[i] * 1000;
            }            
            
            // filter data
            if (chkFilterPeaks.Checked)
            {
                int rLen = data_y.Length;
                double[] data_y_temp = new double[rLen];
                for (int j = 0; j < rLen; j++)
                {
                    data_y_temp[j] = data_y[j];
                }
                NoiseFilter.Filter(data_y_temp, out data_y, (double)numPeakThreshold.Value / 1000, (double)numPeakHysteresis.Value / 1000, analysis.Options.SmoothingWidth, (int)numMaxPeakWidth.Value);
            }
            
            analysis.SetData(data_x, data_y);
            analysis.Analyse();

            txtPitDepth.Text = analysis.Data.PeakMean.ToString("0.0") + " +/- " + analysis.Data.PeakSD.ToString("0.0");
            txtNumPits.Text = analysis.Data.PeakCount.ToString();
            lstPits.Items.Clear();
            foreach (double pit in analysis.Data.Peaks) 
                lstPits.Items.Add(pit.ToString("0.0"));


            if (chkDebugMode.Checked) addGraph("raw data", analysis.Data.Data_X, data_y1000, Color.Green);
            addGraph("normalised data", analysis.Data.Data_X, analysis.Data.Normalised_Y, Color.Black);
            if (chkDebugMode.Checked) addGraph("unweighted smoothed data", analysis.Data.Data_X, analysis.Data.Unweighted_Smoothed_Y, Color.Plum);
            //if (chkDebugMode.Checked) addGraph("weighted smoothed data", analysis.Data.Data_X, analysis.Data.Weighted_Smoothed_Y, Color.Blue);
            if (chkDebugMode.Checked) addGraph("filtered data", analysis.Data.Data_X, analysis.Data.Data_Y, Color.Red);
            
            zgcScan.AxisChange();
            zgcScan.Invalidate();

            // add new data to rolling graph
            AddRollingGraphData(analysis.Data.PeakMean);
        }

        private void addGraph(string label, double[] data_x, double[] data_y, Color color)
        {
            PointPairList list = new PointPairList();
            LineItem curve = zgcScan.GraphPane.AddCurve(label, list, color, SymbolType.None);
            // display data
            for (int i = 0; i < data_x.Length; i++)
            {
                list.Add(data_x[i], data_y[i]);
            }
        }

        #endregion

        #endregion
        
        #region Event Handlers

        #region Form Closing

        void LaserControlForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            BeforeCloseForm();
        }

        void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #endregion

        #region Connection

        void btnConnect_Click(object sender, EventArgs e)
        {
            Connect();
        }

        void cmbSensorModel_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateSensorTypes();
            UpdateSensorPorts();
            UpdateSensorBaudrate();
        }

        #endregion

        #region Buttons

        void btnRunScan_Click(object sender, EventArgs e)
        {
            RunScan();
        }
        
        void btnStopScan_Click(object sender, EventArgs e)
        {
            StopScan();
        }

        void btnChooseDataFolder_Click(object sender, EventArgs e)
        {
            folderBrowser.ShowDialog();

            if (folderBrowser.SelectedPath != "")
            {
                txtDataFolder.Text = folderBrowser.SelectedPath;
            }
        }

        void btnLoadLastScan_Click(object sender, EventArgs e)
        {
            if (txtDataFolder.Text.Trim() == "" || !Directory.Exists(txtDataFolder.Text) || txtFilenamePattern.Text.Trim() == "")
            {
                MessageBox.Show("Please select a valid folder and enter a filename pattern");
                return;
            }

            LoadLastScan();
        }

        #endregion

        #endregion

    }
}
