﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Timers;

using DFM.Settings.Bindable;
using ZedGraph;

namespace DFM
{
    [DefaultProperty("Heading")]
    public partial class DFMScanDisplayControl : UserControl
    {
        private const int NumberPoints = 10;
        private System.Timers.Timer timer;
        private System.Timers.Timer colourTimer;

        public DFMScanDisplayControl()
        {
            InitializeComponent();

            Load += new EventHandler(DFMScanDisplayControl_Load);
            Graph.Resize += new EventHandler(Graph_Resize);
            graphPoints = new RollingPointPairList(DFMScanDisplayControl.NumberPoints);
            boundaryLowLine = new PointPairList();
            boundaryHighLine = new PointPairList();

            timer = new System.Timers.Timer();
            timer.AutoReset = false;
            timer.Interval = 2500;
            timer.Elapsed += new ElapsedEventHandler(timer_Elapsed);

            colourTimer = new System.Timers.Timer();
            colourTimer.AutoReset = true;
            colourTimer.Interval = 50;
            colourTimer.Elapsed += new ElapsedEventHandler(colourTimer_Elapsed);
            
            BoundaryLow = -1;
            BoundaryHigh = -1;
            ScanAveragingNumber = 3;
            ScaleFactor = null;
            NumberFormat = "{0:0.0}";
            IsEmpty = true;
            BoundaryFiltering = false;
            BoundaryFilteringThreshold = 0;

            Graph.Click += new EventHandler(Graph_Click);
            Graph.Cursor = Cursors.Hand;
        }

        void Graph_Click(object sender, EventArgs e)
        {
            GraphDisplay tmp;
            if (Heading.Length > 1) tmp = new GraphDisplay(zgcGraph, Heading[0].ToString().ToUpper() + Heading.Substring(1).ToLower() + " Scan");
            else if (Heading.Length == 1) tmp = new GraphDisplay(zgcGraph, Heading[0].ToString().ToUpper() + " Scan");
            else tmp = new GraphDisplay(zgcGraph, "Scan");
            tmp.Graph.GraphPane.XAxis.Scale.Max = Graph.GraphPane.XAxis.Scale.Max;
            tmp.Graph.GraphPane.XAxis.Scale.Min = Graph.GraphPane.XAxis.Scale.Min;
            tmp.Graph.GraphPane.XAxis.IsVisible = true;
            tmp.Graph.GraphPane.YAxis.IsVisible = true;
            tmp.Graph.GraphPane.Title.IsVisible = true;
            tmp.Graph.AxisChange();
            tmp.Graph.Invalidate();
        }

        private RollingPointPairList graphPoints;
        private PointPairList boundaryLowLine;
        private PointPairList boundaryHighLine;
        private int lastPointX;
        void DFMScanDisplayControl_Load(object sender, EventArgs e)
        {
            HandleResize();
            ClearData();
                        
            // set up graph
            GraphUnits = "microns";
            Graph.GraphPane.YAxis.Title.Text = "Av. Pit Depth (microns)";
            Graph.GraphPane.Title.Text = "Readings";
            Graph.GraphPane.XAxis.IsVisible = false;                
            Graph.GraphPane.Legend.IsVisible = false;
            Graph.GraphPane.AddCurve("data", graphPoints, Color.Blue);

            // init boundary graphs
            Graph.GraphPane.AddCurve("low", boundaryLowLine, Color.Red);
            Graph.GraphPane.AddCurve("high", boundaryHighLine, Color.Red);

            updateBoundaryHigh();
            updateBoundaryLow();
        }

        #region Resizing
        void Graph_Resize(object sender, EventArgs e)
        {
            HandleResize();
        }
        private void HandleResize()
        {
            if (Graph.Size.Width > 360 && Graph.Size.Height > 210)
            {
                Graph.GraphPane.YAxis.IsVisible = true;
                Graph.GraphPane.Title.IsVisible = true;
            }
            else
            {
                Graph.GraphPane.YAxis.IsVisible = false;
                Graph.GraphPane.Title.IsVisible = false;
            }
        }
        #endregion

        #region Is Empty

        public event EventHandler<IsEmptyEventArgs> IsEmptyChanged;
        protected void OnIsEmptyChanged(IsEmptyEventArgs e)
        {
            if (IsEmptyChanged != null)
                IsEmptyChanged(this, e);
        }

        private bool isEmpty;
        public bool IsEmpty
        {
            get { return isEmpty; }
            set
            {
                if (isEmpty != value)
                {
                    isEmpty = value;
                    OnIsEmptyChanged(new IsEmptyEventArgs(value));
                }
            }
        }

        public class IsEmptyEventArgs : EventArgs
        {
            public bool IsEmpty { get; private set; }
            public IsEmptyEventArgs(bool isEmpty)
            {
                IsEmpty = isEmpty;
            }
        }

        #endregion

        public void ClearData()
        {
            lastPointX = 1;
            MainText = SubText1 = SubText2 = SubText3 = SubCaption1 = SubCaption2 = SubCaption3 = "";
            MainCaption = "No current scan";
            IsEmpty = true;
            updateColours();

            curScan = new List<ScanData>();
            curScanTotal = 0.0;
            curScanCount = 0;

            // data axis
            graphPoints.Clear();
            Graph.GraphPane.XAxis.Scale.Min = 0;
            Graph.GraphPane.XAxis.Scale.Max = 10;
            Graph.GraphPane.XAxis.Scale.MinorStep = 1;
            Graph.AxisChange();
            Graph.Invalidate();

        }

        private List<ScanData> curScan;
        private double curScanTotal;
        private int curScanCount;
        
        private int scanAveragingNumber;
        /// <summary>
        /// Refreshes display automatically
        /// </summary>
        public int ScanAveragingNumber
        {
            get
            {
                return scanAveragingNumber;
            }
            set
            {
                scanAveragingNumber = value;
                RefreshDisplay();
            }            
        }

        public void AddData(ScanData data)
        {
            bool error = Double.IsNaN(data.Value);
            string error_type = "no pits";

            if (!error && BoundaryFiltering)
            {
                // add boundary filtering
                if (data.Value > boundaryHigh * (1 + BoundaryFilteringThreshold))
                {
                    error = true;
                    error_type = "too high";
                }
                else if (data.Value < boundaryLow * (1 - BoundaryFilteringThreshold))
                {
                    error = true;
                    error_type = "too low";
                }                
            }

            if (!error)
            {
                curScan.Add(data);            
                curScanTotal += processDataPoint(data.Value);
                curScanCount += 1;

                if (curScanCount >= ScanAveragingNumber)
                {
                    if (curScanCount > ScanAveragingNumber) curScanTotal -= processDataPoint(curScan[curScanCount - ScanAveragingNumber - 1].Value);
                    double average = curScanTotal / ScanAveragingNumber;

                    // update main display
                    MainText = String.Format(NumberFormat, average);
                    MainCaption = String.Format("Average of {0}", ScanAveragingNumber);

                    // add point to graph
                    graphPoints.Add(lastPointX, average);
                    lastPointX++;
                    Graph.GraphPane.XAxis.Scale.Min = Math.Max(0, lastPointX - DFMScanDisplayControl.NumberPoints);
                    Graph.GraphPane.XAxis.Scale.Max = Math.Max(10, lastPointX + 1);
                    Graph.AxisChange();
                    Graph.Invalidate();
                }
            }

            // push old data in boxes
            SubText1 = SubText2;
            SubText2 = SubText3;
            SubCaption1 = SubCaption2;
            SubCaption2 = SubCaption3;

            // add new data
            if (error)
            {
                SubText3 = "-";
                SubCaption3 = error_type;
            }
            else
            {
                SubText3 = String.Format(NumberFormat, processDataPoint(data.Value));
                SubCaption3 = String.Format("{0} pit{1}", data.PitCount, (data.PitCount == 1 ? "" : "s"));
            }
            IsEmpty = false;
            updateColours();
        }

        public void RefreshDisplay()
        {
            if (curScan == null) return;
            List<ScanData> cur = curScan;
            ClearData();
            foreach (ScanData data in cur)
                AddData(data);

            updateBoundaryHigh();
            updateBoundaryLow();
        }

        public ZedGraphControl Graph
        {
            get { return zgcGraph; }
        }

        private float boundaryLow;
        public float BoundaryLow
        {
            get { return boundaryLow; }
            set
            {
                boundaryLow = value;
                updateBoundaryLow();
            }
        }

        private float boundaryHigh;
        public float BoundaryHigh
        {
            get { return boundaryHigh; }
            set
            {
                boundaryHigh = value;
                updateBoundaryHigh();
            }
        }

        /// <summary>
        /// Whether to filter results that lie outside the boundary lines. Not retroactive
        /// </summary>
        public bool BoundaryFiltering
        {
            get;
            set;
        }

        /// <summary>
        /// What threshold level above boundaries to filter. Not retroactive. As a decimal to scale boundary by and then add to boundary. 0 allows no threshold.
        /// </summary>
        public float BoundaryFilteringThreshold
        {
            get;
            set;
        }
        
        public Converter<double, double> ScaleFactor
        {
            get;
            set;
        }
        public string NumberFormat
        {
            get; 
            set;
        }

        private double processDataPoint(double input)
        {
            if (ScaleFactor == null) return input;
            else return ScaleFactor(input);
        }

        private string graphUnits;
        public string GraphUnits
        {
            get { return graphUnits; }
            set
            {
                graphUnits = value;
                Graph.GraphPane.YAxis.Title.Text = "Av. Pit Depth (" + graphUnits + ")";
            }
        }

        private void updateBoundaryLow()
        {
            boundaryLowLine.Clear();
            if (boundaryLow > 0)
            {
                // don't process these values, as boundary points get updated along with the units
                boundaryLowLine.Add(-10000, boundaryLow);
                boundaryLowLine.Add(10000, boundaryLow);
            }
            Graph.AxisChange();
            Graph.Invalidate();
            updateColours();
        }

        private void updateBoundaryHigh()
        {
            boundaryHighLine.Clear();
            if (boundaryHigh > 0)
            {
                // don't process these values, as boundary points get updated along with the units
                boundaryHighLine.Add(-10000, boundaryHigh);
                boundaryHighLine.Add(10000, boundaryHigh);
            }
            Graph.AxisChange();
            Graph.Invalidate();
            updateColours();
        }

        Color errorColour = Color.FromArgb(192, 0, 0);
        public void DisplayError(string message)
        {
            timer.Stop();
            colourTimer.Stop();
            colourTimerStep = 0;
            ThreadSafeControlUpdates.UpdateText(lblErrorDisplay, message);
            ThreadSafeControlUpdates.UpdateVisible(lblErrorDisplay, true);
            lblErrorDisplay.BackColor = errorColour;                

            timer.Start();
            colourTimer.Start();
        }

        void timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            ThreadSafeControlUpdates.UpdateVisible(lblErrorDisplay, false);
        }

        int colourTimerStep = 0;
        void colourTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            stepColour();
        }

        private delegate void invokeDelegate2();
        void stepColour()
        {
            if (lblErrorDisplay.InvokeRequired)
            {
                lblErrorDisplay.Invoke(new invokeDelegate2(stepColour));
            }
            else
            {
                if (colourTimerStep > 20) colourTimer.Stop();
                colourTimerStep += 1;
                lblErrorDisplay.BackColor = Color.FromArgb(Math.Max(0, 255 - colourTimerStep * 5), errorColour);
            }
        }

        private void updateColours()
        {
            foreach (CaptionedDisplay disp in new CaptionedDisplay[] { cDisp1, cDisp2, cDisp3, cDispMain })
            {
                string text = disp.Text;
                double num;
                bool isNum = double.TryParse(text, out num);
                if (isNum)
                {
                    if (num > BoundaryHigh || num < BoundaryLow)
                        disp.FontColour = ErrorCaptionColour;
                    else
                        disp.FontColour = FontCaptionColour;
                }
                else
                {
                    disp.FontColour = FontCaptionColour;
                }                
            }
        }

        #region Display Properties
        [Browsable(true)]
        [Category("Appearance")]
        public string Heading
        {
            get { return lblHeading.Text; }
            set { lblHeading.Text = value; }                
        }

        [Browsable(true)]
        [Category("Appearance")]
        public string MainText
        {
            get { return cDispMain.Text; }
            set { cDispMain.Text = value; }
        }
        [Browsable(true)]
        [Category("Appearance")]
        public string MainCaption
        {
            get { return cDispMain.Caption; }
            set { cDispMain.Caption = value; }
        }

        [Browsable(true)]
        [Category("Appearance")]
        public string SubText1
        {
            get { return cDisp1.Text; }
            set { cDisp1.Text = value; }
        }
        [Browsable(true)]
        [Category("Appearance")]
        public string SubText2
        {
            get { return cDisp2.Text; }
            set { cDisp2.Text = value; }
        }
        [Browsable(true)]
        [Category("Appearance")]
        public string SubText3
        {
            get { return cDisp3.Text; }
            set { cDisp3.Text = value; }
        }

        [Browsable(true)]
        [Category("Appearance")]
        public string SubCaption1
        {
            get { return cDisp1.Caption; }
            set { cDisp1.Caption = value; }
        }
        [Browsable(true)]
        [Category("Appearance")]
        public string SubCaption2
        {
            get { return cDisp2.Caption; }
            set { cDisp2.Caption = value; }
        }
        [Browsable(true)]
        [Category("Appearance")]
        public string SubCaption3
        {
            get { return cDisp3.Caption; }
            set { cDisp3.Caption = value; }
        }

        private Color fontCaptionColour;
        [Browsable(true)]
        [Category("Appearance")]
        public Color FontCaptionColour
        {
            get { return fontCaptionColour; }
            set
            {
                fontCaptionColour = value;
                updateColours();
            }
        }

        private Color errorCaptionColour;
        [Browsable(true)]
        [Category("Appearance")]
        public Color ErrorCaptionColour
        {
            get { return errorCaptionColour; }
            set
            {
                errorCaptionColour = value;
                updateColours();
            }
        }
        #endregion
    }

    public class ScanData
    {
        public double Value
        {
            get;
            set;
        }

        public int PitCount
        {
            get;
            set;
        }

        public ScanData(double value, int pitCount)
        {
            Value = value;
            PitCount = pitCount;
        }
    }
}
