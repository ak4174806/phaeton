﻿using System;
using System.Collections.Generic;
using System.Text;

using System.IO;
using System.IO.Ports;

namespace DFM.Laser
{
    public interface ILaser
    {
        int[] Baudrates { get; }
        int Baudrate { get; }
        bool Connected { get; }
        Handshake HandshakeMethod { get; set; }

        void Connect(string portName, int baudrate);
        void Connect(string portName, bool fast=false);
        void Disconnect();        
        bool Run(string command, string data);
        bool Run(string command);
        bool GetResponse(string command);
    }
}
