﻿using System;
using System.Collections.Generic;
using System.Text;

using System.IO.Ports;
using System.Threading;

namespace DFM.Laser
{
    public class PLCLaser : ILaser
    {
        protected SerialPort Port
        {
            get;
            set;
        }
        public PLCLaser()
        {
            Baudrates = new int[] { 9600, 19200, 38400, 57600, 115200 };
            // Use RTS for RS485, None for RS232
            HandshakeMethod = Handshake.RequestToSend;
        }

        public int[] Baudrates { get; protected set; }
        public int Baudrate
        {
            get { return Port.BaudRate; }
        }

        public Handshake HandshakeMethod
        {
            get;
            set;
        }

        public void Connect(string portName, int baudrate)
        {
            try
            {
                // connect to port
                Port = new SerialPort(portName, baudrate);

                // sets encoding to iso-8859-1 (Western European (ISO))
                Port.Encoding = Encoding.GetEncoding(28591);
                Port.ReadTimeout = 5;
                Port.WriteTimeout = 5;
                Port.Handshake = HandshakeMethod;

                Port.Open();
                Thread.Sleep(200);

                if (!TestConnection())
                {
                    throw new ApplicationException("Port not responding");
                }
            }
            catch (Exception ex)
            {
                if (Connected)
                {
                    try { Port.Close(); }
                    catch { }
                }

                Port = null;
                throw new ApplicationException("Unable to connect to PLC: " + ex.Message, ex);
            }
        }

        public void Connect(string portName, bool fast=false)
        {
            try
            {
                // connect to port
                Port = new SerialPort(portName);

                // sets encoding to iso-8859-1 (Western European (ISO))
                Port.Encoding = Encoding.GetEncoding(28591);
                Port.ReadTimeout = 5;
                Port.WriteTimeout = 5;
                Port.Handshake = HandshakeMethod;
                
                Port.Open();
                Thread.Sleep(200);

                int actualBr = -1;
                foreach (int br in Baudrates)
                {
                    Port.BaudRate = br;
                    Thread.Sleep(50);

                    if (TestConnection())
                    {
                        actualBr = br;
                        break;
                    }
                }

                if (actualBr == -1 && !fast) {
                    foreach (int br in Baudrates)
                    {
                        Port.BaudRate = br;
                        Thread.Sleep(100);

                        if (TestConnection())
                        {
                            actualBr = br;
                            break;
                        }
                    }
                }
                if (actualBr == -1)
                {
                    throw new ApplicationException("Port not responding");
                }
            }
            catch (Exception ex)
            {
                if (Connected)
                {
                    try { Port.Close(); }
                    catch { }
                }

                Port = null;
                throw new ApplicationException("Unable to connect to PLC: " + ex.Message, ex);
            }
        }

        public void Disconnect()
        {
            try
            {
                if (Connected)
                {
                    Port.Close();
                }

                Port = null;
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Could not disconnect from PLC", ex);
            }
        }

        public virtual bool Connected
        {
            get { return (Port != null && Port.IsOpen); }
        }

        protected bool TestConnection()
        {
            // run test command
            return Run("T");
        }

        public bool Run(string command, string data)
        {
            // check port is open
            if (!Connected)
            {
                throw new ApplicationException("No connection to PLC");
            }

            Port.Write(command + data);
            Thread.Sleep(200);

            return GetResponse(command);
        }

        public bool Run(string command)
        {
            return Run(command, "");
        }

        public bool GetResponse(string command)
        {
            StringBuilder buffer = new StringBuilder();

            // look for A+command
            string token = "A" + command;
            int len = token.Length;

            // to timeout if buffer gets too large (normally from debugging)
            int bytes_read = 0;
            
            // to count sleeps
            int k = 0; int max_k = 3;

            // read bytes until token is found
            while (bytes_read < 50)
            {
                while (Port.BytesToRead == 0)
                {
                    Thread.Sleep(50);
                    k++;
                    if (k == max_k)
                    {
                        // could not find response
                        return false;
                    }
                }

                // read a character
                bytes_read++;
                buffer.Append((char)Port.ReadChar());

                // if buffer is capable of holding a token
                if (buffer.Length >= len)
                {
                    if (buffer.ToString(buffer.Length - len, len) == token)
                    {
                        // found confirmation
                        return true;
                    }
                    
                }
            }

            // no response
            return false;
        }
    }
}
