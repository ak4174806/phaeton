﻿using System;
using System.Collections.Generic;
using System.Text;

using System.IO.Ports;

namespace DFM.Laser
{
    public class MockLaser : ILaser
    {
        public MockLaser(bool delays=true)
        {
            Baudrates = new int[] { 9600, 19200, 38400, 57600, 115200 };
            this.delays = delays;
        }

        private bool delays { get; set; }

        public int[] Baudrates
        {
            get;
            protected set;
        }

        private int baudrate;
        public int Baudrate
        {
            get
            {
                if (Connected) return baudrate;
                else return -1;
            }
            protected set
            {
                baudrate = value;
            }
        }

        public bool Connected
        {
            get;
            protected set;
        }

        public Handshake HandshakeMethod { get; set; }

        public void Connect(string portName, int baudrate)
        {
            if (delays) System.Threading.Thread.Sleep(50);
            Connected = true;
        }
        public void Connect(string portName, bool fast = false)
        {
            if (delays) System.Threading.Thread.Sleep(50);
            Connect(portName, Baudrates[0]);
        }
        public void Disconnect()
        {
            if (delays) System.Threading.Thread.Sleep(20);
            Connected = false;
        }

        public bool Run(string command, string data)
        {
            if (delays) System.Threading.Thread.Sleep(50);
            return true;
        }
        public bool Run(string command)
        {
            if (delays) System.Threading.Thread.Sleep(50);
            return true;
        }
        public bool GetResponse(string command)
        {
            if (delays) System.Threading.Thread.Sleep(50);
            return true;
        }
    }
}
