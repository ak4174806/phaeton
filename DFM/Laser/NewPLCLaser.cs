﻿using System;
using System.Collections.Generic;
using System.Text;
using NLog;

using System.IO.Ports;
using System.Threading;

namespace DFM.Laser
{
    public class NewPLCLaser : ILaser
    {
        static string indata = "";
        // StringBuilder buffer = new StringBuilder();
        //private static //logger //logger = LogManager.GetCurrentClass//logger();

        protected SerialPort Port
        {
            get;
            set;
        }
        public NewPLCLaser()
        {
            Baudrates = new int[] { 9600, 19200, 38400, 57600, 115200 };
            // Use RTS for RS485, None for RS232
            HandshakeMethod = Handshake.RequestToSend;
        }

        public int[] Baudrates { get; protected set; }
        public int Baudrate
        {
            get { return Port.BaudRate; }
        }

        public Handshake HandshakeMethod
        {
            get;
            set;
        }

        public void Connect(string portName, int baudrate)
        {
            // throw new ApplicationException("HI");
            ////logger.Trace("Connect  in baudrate:" + portName.ToString());
            try
            {

                // connect to port
                Port = new SerialPort(portName, baudrate);
                //logger.Trace("inside NewPLClaser POrt is:" + portName.ToString() + " and Baudrate is " + baudrate.ToString());
                // sets encoding to iso-8859-1 (Western European (ISO))
                Port.Encoding = Encoding.GetEncoding(28591);
                Port.ReadTimeout = 5;
                Port.WriteTimeout = 5;
                Port.Handshake = HandshakeMethod;

                Port.Open();
                Thread.Sleep(200);

                if (!TestConnection())
                {
                    throw new ApplicationException("Port not responding");
                }
                // //logger.Trace("Connected to port!!");
            }
            catch (Exception ex)
            {
                if (Connected)
                {
                    try { Port.Close(); }
                    catch { }
                }

                Port = null;
                throw new ApplicationException("Unable to connect to New PLC: " + ex.Message, ex);
            }
        }

        public void Connect(string portName, bool fast = false)
        {
            // throw new ApplicationException("HI");
            // //logger.Trace("Before Serial Port:" + portName.ToString());
            try
            {
                ////logger.Trace("Before Serial Port:" + portName.ToString());
                // connect to port
                Port = new SerialPort(portName);

                /* //logger.Trace("inside NewPLClaser POrt is:" + portName.ToString());
                 // sets encoding to iso-8859-1 (Western European (ISO))
                 Port.Encoding = Encoding.GetEncoding(28591);
                 Port.ReadTimeout = 20;
                 Port.WriteTimeout = 20;
                 Port.Handshake = HandshakeMethod;

                 Port.BaudRate = 9600;
                 Port.Parity = Parity.None;
                 Port.StopBits = StopBits.One;
                 Port.DataBits = 8;
                 Port.Handshake = Handshake.None;
                 //Port.Open();*/

                try
                {
                    // //logger.Trace("Is Open:" + Port.IsOpen);
                    if (Port.IsOpen == true)
                    {
                        Port.Dispose();
                        // //logger.Trace("Port Disposed!" + Port);
                        //Port.Open();
                        ////logger.Trace("Port Open!");
                    }


                    try
                    {
                        // //logger.Trace("inside NewPLClaser POrt is:" + portName.ToString());
                        // sets encoding to iso-8859-1 (Western European (ISO))
                        Port.Encoding = Encoding.GetEncoding(28591);
                        Port.ReadTimeout = 20;
                        Port.WriteTimeout = 20;
                        Port.Handshake = HandshakeMethod;

                        Port.BaudRate = 9600;
                        Port.Parity = Parity.None;
                        Port.StopBits = StopBits.One;
                        Port.DataBits = 8;
                        Port.Handshake = Handshake.None;
                        //Port.Open();
                        Port.Open();
                    }
                    catch (Exception ex)
                    {
                        //logger.Error("Port Open Error:"+ex.Message);
                    }
                }
                catch (Exception ex)
                {

                    //logger.Error("Port Dispose Error:" + ex.Message);

                }


                // Thread.Sleep(200);
                ////logger.Trace("Sending H Command");
                //Port.Write("H\r");


                // //logger.Trace("Connected status:" + Connected.ToString());

                //Port.DataReceived += new SerialDataReceivedEventHandler(DataReceivedHandler);



                if (!TestConnection())
                {
                    throw new ApplicationException("Port not responding");
                }

                ////logger.Trace("Connected to port!!");
                /* int actualBr = -1;
                 foreach (int br in Baudrates)
                 {
                     Port.BaudRate = br;
                     Thread.Sleep(50);

                     if (TestConnection())
                     {
                         actualBr = br;
                         break;
                     }
                 }*/



                /*  if (actualBr == -1 && !fast)
                  {
                     foreach (int br in Baudrates)
                      {
                          Port.BaudRate = br;
                          Thread.Sleep(100);

                          if (TestConnection())
                          {
                              actualBr = br;
                              //logger.Trace("baud rate:"+br.ToString());
                              break;
                          }
                      }
                      //logger.Trace("Connected status:"+Connected.ToString());
                  }
                  if (actualBr == -1)
                  {
                      throw new ApplicationException("Port not responding");
                  }*/
            }
            catch (Exception ex)
            {
                if (Connected)
                {
                    try { Port.Close(); }
                    catch { }
                }

                Port = null;
                throw new ApplicationException("Unable to connect to New PLC: " + ex.Message, ex);
            }
        }

        public void Disconnect()
        {
            try
            {
                if (Connected)
                {
                    Port.Close();
                    // //logger.Trace("NewLaser Port closed!");
                }

                Port = null;

            }
            catch (Exception ex)
            {
                throw new ApplicationException("Could not disconnect from New PLC", ex);
            }
        }

        public virtual bool Connected
        {
            get { return (Port != null && Port.IsOpen); }
        }

        protected bool TestConnection()
        {
            // run test command
            // return Run("T");
            ////logger.Trace("Sending H command");
            return Run("H"); // send Hello Command to test connection
        }

        public bool Run(string command, string data)
        {
            // check port is open
            if (!Connected)
            {
                throw new ApplicationException("No connection to New PLC");
            }
            ////logger.Trace("Writing"+ command);
            Port.Write(command + data + "\r");
            //Port.Write("S \r")
            //  Thread.Sleep(200);

            return GetResponse(command);
        }

        public bool Run(string command)
        {
            return Run(command, "");
        }

        public bool GetResponse(string command)
        {

            StringBuilder buffer = new StringBuilder();

            // look for A+command
            // string token = "A" + command;
            string token = "R>";
            int len = token.Length;

            // to timeout if buffer gets too large (normally from debugging)
            int bytes_read = 0;

            // to count sleeps
            int k = 0;
            // int max_k = 100;  because of the S command(Slow Ack)

            /************* New Code ***************************/
            int sleep;
            int max_k;
            if (command == "S")
            {
                ////logger.Trace("For S");
                max_k = 200;
                sleep = 50;

                // return true;
            }
            else
            {
                ////logger.Trace("For others");
                max_k = 3;
                sleep = 50;
            }

            /****************** 9-June-15 ********************/

            ////logger.Trace("bytes to read:" + Port.BytesToRead.ToString());

            // read bytes until token is found
            while (bytes_read < 50)
            {

                ////logger.Trace("bytes to read(in while):" + Port.BytesToRead.ToString());
                while (Port.BytesToRead == 0)
                {
                    // Thread.Sleep(50);
                    // buffer = new StringBuilder();
                    //Port.DiscardInBuffer();
                    //Port.DiscardOutBuffer();

                    Thread.Sleep(sleep);
                    k++;
                    ////logger.Trace("bytes 0... k is :"+k);
                    if (k == max_k)
                    {

                        ////logger.Trace("could not find response (Timeout)");
                        // could not find response
                        return false;
                    }

                }


                //if (Port.BytesToRead != 0)//new
                // {
                // read a character
                bytes_read++;
                buffer.Append((char)Port.ReadChar());
                //}

                // if buffer is capable of holding a token
                if (buffer.Length >= len)
                {
                    ////logger.Trace(buffer.ToString(buffer.Length - len, len) + "K val:" + k);


                    ////logger.Trace(buffer.ToString(buffer.Length - len, len));
                    if (buffer.ToString(buffer.Length - len, len) == token)
                    {
                        // found confirmation
                        // //logger.Trace("Found Response");
                        ////logger.Trace("ReceiveBytesThreshold:"+Port.ReceivedBytesThreshold);
                        //Port.DiscardInBuffer();
                        //Port.DiscardOutBuffer();
                        //Port.Close();
                        //Port.
                        return true;

                    }

                }
            }

            ////logger.Trace("No Response");
            // no response
            return false;



            // indata = "";
            //  Port.DataReceived += new SerialDataReceivedEventHandler(DataReceivedHandler);

            ////logger.Trace("Get Response:" + indata);

            // no response
            //return true;
        }


        /* private static void DataReceivedHandler(object sender,SerialDataReceivedEventArgs e)
         {
            
             SerialPort sp = (SerialPort)sender;
            // indata = "";
             while (sp.BytesToRead > 0)
             {
                 indata += sp.ReadExisting();
             }
             //logger.Trace("Data Received:");
             //logger.Trace("Response:" + indata);
             //Console.Write(indata);
             //Console.ReadLine();
         }*/

    }
}
