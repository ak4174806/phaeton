﻿namespace DFM
{
    partial class GalilForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.chkSkipEdgeScan = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // chkSkipEdgeScan
            // 
            this.chkSkipEdgeScan.AutoSize = true;
            this.chkSkipEdgeScan.Location = new System.Drawing.Point(16, 61);
            this.chkSkipEdgeScan.Name = "chkSkipEdgeScan";
            this.chkSkipEdgeScan.Size = new System.Drawing.Size(98, 17);
            this.chkSkipEdgeScan.TabIndex = 11;
            this.chkSkipEdgeScan.Text = "skip edge scan";
            this.chkSkipEdgeScan.UseVisualStyleBackColor = true;
            // 
            // GalilForm1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(410, 394);
            this.Controls.Add(this.chkSkipEdgeScan);
            this.Name = "GalilForm1";
            this.Text = "GalilForm1";
            this.Controls.SetChildIndex(this.label5, 0);
            this.Controls.SetChildIndex(this.chkSkipEdgeScan, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox chkSkipEdgeScan;
    }
}