﻿namespace DFM
{
    partial class showPitsValue
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pitsValue = new System.Windows.Forms.DataGridView();
            this.exportCSV = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pitsValue)).BeginInit();
            this.SuspendLayout();
            // 
            // pitsValue
            // 
            this.pitsValue.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.pitsValue.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.pitsValue.Location = new System.Drawing.Point(0, 38);
            this.pitsValue.Name = "pitsValue";
            this.pitsValue.Size = new System.Drawing.Size(1122, 435);
            this.pitsValue.TabIndex = 0;
            // 
            // exportCSV
            // 
            this.exportCSV.Location = new System.Drawing.Point(0, 0);
            this.exportCSV.Name = "exportCSV";
            this.exportCSV.Size = new System.Drawing.Size(129, 32);
            this.exportCSV.TabIndex = 1;
            this.exportCSV.Text = "Export to CSV";
            this.exportCSV.UseVisualStyleBackColor = true;
            this.exportCSV.Click += new System.EventHandler(this.exportCSV_Click);
            // 
            // showPitsValue
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1123, 473);
            this.Controls.Add(this.exportCSV);
            this.Controls.Add(this.pitsValue);
            this.Name = "showPitsValue";
            this.Text = "showPitsValue";
            ((System.ComponentModel.ISupportInitialize)(this.pitsValue)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.DataGridView pitsValue;
        private System.Windows.Forms.Button exportCSV;
    }
}