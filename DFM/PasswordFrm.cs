﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace DFM
{
    public partial class PasswordFrm : Form
    {
        public PasswordFrm()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (txtPwd.Text == "admin")
            {
                this.Close();
                AlgoConfiguration algoConfig = new AlgoConfiguration();
                algoConfig.Show();

            }
            else
            {
                lblWrongPwd.ForeColor = Color.Red;
                txtPwd.Text = "";
                lblWrongPwd.Visible = true;
            
            }

        }
    }
}
