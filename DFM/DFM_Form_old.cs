﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using System.Threading;
using System.IO.Ports;
using System.IO;

using PaintAnalysis.Common;
using SensorControl;
using SensorControl.Sensors;
using ZedGraph;
using Analysis;
using DFM.Scans;
using DFM.Laser;

namespace DFM
{
    public partial class DFM_Form_old : Form
    {
        #region Statics

        private static string connectText = "Connect";
        private static string disconnectText = "Disconnect";

        #endregion

        #region Private Variables

        /// <summary>
        /// The available sensors
        /// </summary>
        private ISensor[] sensors;

        /// <summary>
        /// The current sensor
        /// </summary>
        private ISensor sensor;

        /// <summary>
        /// Connection to laser via PLC
        /// </summary>
        private PLCLaser laser;

        /// <summary>
        /// The current state of the form
        /// </summary>
        private States state;
              
        private BackgroundWorker worker;

        // for cancelling
        private int pulseCount;
        private int runTime;
        private int gatingFrequency;
        private int dutyCycle;

        /// <summary>
        /// Provides Galil functionality
        /// </summary>
        private GalilForm galilForm;

        #endregion

        #region Properties
        public int PulseCount
        {
            get { return (int)numPulseCount.Value; }
            set { numPulseCount.Value = (decimal)value; }
        }

        // in Hz
        public int GatingFrequency
        {
            get { return (int)numGatingFreq.Value; }
            set { numGatingFreq.Value = (decimal)value; }
        }

        // between 1 and 1000, 1000 => 100.0%, 1 => 0.1%
        public int DutyCycle
        {
            get { return (int)(numDutyCycle.Value * 10); }
            set { numDutyCycle.Value = (decimal)(value / 10.0); }
        }

        // in ms
        public int RunTime
        {
            get
            {
                return (int)(1000.0 / GatingFrequency * PulseCount);
            }
        }
        #endregion

        #region States

        public enum States
        {
            DISCONNECTED,
            CONNECTED,
            PRESETS_SET,
            PREPARED,
            RUNNING
        }

        public States State
        {
            get { return state; }
            set
            {
                state = value;
                switch (value)
                {
                    case States.DISCONNECTED:
                        cmbSensorModel.Enabled = chkConnectToSensor.Checked;
                        cmbSensorType.Enabled = chkConnectToSensor.Checked;
                        cmbSensorPort.Enabled = chkConnectToSensor.Checked;
                        cmbPLCPort.Enabled = true;
                        cmbSensorBaudrate.Enabled = chkConnectToSensor.Checked;
                        cmbPLCBaudrate.Enabled = true;
                        btnConnect.Enabled = true;
                        chkConnectToSensor.Enabled = true;

                        numPulseCount.Enabled = false;
                        chkPulseStopNeeded.Enabled = false;
                        numGatingFreq.Enabled = false;
                        numDutyCycle.Enabled = false;
                        btnUpdatePresets.Enabled = false;
                        btnCancelPresets.Enabled = false;

                        numDAQStart.Enabled = false;
                        numLaserStart.Enabled = false;
                        numDAQStop.Enabled = false;
                        btnPrepare.Enabled = false;
                        btnRun.Enabled = false;
                        btnStop.Enabled = false;

                        chkRepeat.Enabled = false;
                        numRepeatTime.Enabled = false;
                        numRepeatCount.Enabled = false;

                        chkFilterPeaks.Enabled = true;
                        numPeakThreshold.Enabled = chkFilterPeaks.Checked;
                        numPeakHysteresis.Enabled = chkFilterPeaks.Checked;
                        numMaxPeakWidth.Enabled = chkFilterPeaks.Checked;
                        break;

                    case States.CONNECTED:
                        cmbSensorModel.Enabled = false;
                        cmbSensorType.Enabled = false;
                        cmbSensorPort.Enabled = false;
                        cmbPLCPort.Enabled = false;
                        cmbSensorBaudrate.Enabled = false;
                        cmbPLCBaudrate.Enabled = false;
                        btnConnect.Enabled = true;
                        chkConnectToSensor.Enabled = false;

                        numPulseCount.Enabled = true;
                        chkPulseStopNeeded.Enabled = true;
                        numGatingFreq.Enabled = true;
                        numDutyCycle.Enabled = true;
                        btnUpdatePresets.Enabled = true;
                        btnCancelPresets.Enabled = false;

                        numDAQStart.Enabled = chkConnectToSensor.Checked;
                        numLaserStart.Enabled = true;
                        numDAQStop.Enabled = chkConnectToSensor.Checked;
                        btnPrepare.Enabled = false;
                        btnRun.Enabled = false;
                        btnStop.Enabled = false;

                        chkRepeat.Enabled = true;
                        numRepeatTime.Enabled = chkRepeat.Checked;
                        numRepeatCount.Enabled = chkRepeat.Checked;

                        chkFilterPeaks.Enabled = true;
                        numPeakThreshold.Enabled = chkFilterPeaks.Checked;
                        numPeakHysteresis.Enabled = chkFilterPeaks.Checked;
                        numMaxPeakWidth.Enabled = chkFilterPeaks.Checked;
                        break;

                    case States.PRESETS_SET:
                        cmbSensorModel.Enabled = false;
                        cmbSensorType.Enabled = false;
                        cmbSensorPort.Enabled = false;
                        cmbPLCPort.Enabled = false;
                        cmbSensorBaudrate.Enabled = false;
                        cmbPLCBaudrate.Enabled = false;
                        btnConnect.Enabled = true;
                        chkConnectToSensor.Enabled = false;

                        numPulseCount.Enabled = true;
                        chkPulseStopNeeded.Enabled = true;
                        numGatingFreq.Enabled = true;
                        numDutyCycle.Enabled = true;
                        btnUpdatePresets.Enabled = false;
                        btnCancelPresets.Enabled = false;

                        numDAQStart.Enabled = chkConnectToSensor.Checked;
                        numLaserStart.Enabled = true;
                        numDAQStop.Enabled = chkConnectToSensor.Checked;
                        btnPrepare.Enabled = true;
                        btnRun.Enabled = false;
                        btnStop.Enabled = false;

                        chkRepeat.Enabled = true;
                        numRepeatTime.Enabled = chkRepeat.Checked;
                        numRepeatCount.Enabled = chkRepeat.Checked;

                        chkFilterPeaks.Enabled = true;
                        numPeakThreshold.Enabled = chkFilterPeaks.Checked;
                        numPeakHysteresis.Enabled = chkFilterPeaks.Checked;
                        numMaxPeakWidth.Enabled = chkFilterPeaks.Checked;
                        break;

                    case States.PREPARED:
                        cmbSensorModel.Enabled = false;
                        cmbSensorType.Enabled = false;
                        cmbSensorPort.Enabled = false;
                        cmbPLCPort.Enabled = false;
                        cmbSensorBaudrate.Enabled = false;
                        cmbPLCBaudrate.Enabled = false;
                        btnConnect.Enabled = false;
                        chkConnectToSensor.Enabled = false;

                        numPulseCount.Enabled = false;
                        chkPulseStopNeeded.Enabled = false;
                        numGatingFreq.Enabled = false;
                        numDutyCycle.Enabled = false;
                        btnUpdatePresets.Enabled = false;
                        btnCancelPresets.Enabled = false;

                        numDAQStart.Enabled = false;
                        numLaserStart.Enabled = false;
                        numDAQStop.Enabled = false;
                        btnPrepare.Enabled = false;
                        btnRun.Enabled = true;
                        btnStop.Enabled = true;

                        chkRepeat.Enabled = false;
                        numRepeatTime.Enabled = false;
                        numRepeatCount.Enabled = false;

                        chkFilterPeaks.Enabled = false;
                        numPeakThreshold.Enabled = false;
                        numPeakHysteresis.Enabled = false;
                        numMaxPeakWidth.Enabled = false;
                        break;

                    case States.RUNNING:
                        cmbSensorModel.Enabled = false;
                        cmbSensorType.Enabled = false;
                        cmbSensorPort.Enabled = false;
                        cmbPLCPort.Enabled = false;
                        cmbSensorBaudrate.Enabled = false;
                        cmbPLCBaudrate.Enabled = false;
                        btnConnect.Enabled = false;
                        chkConnectToSensor.Enabled = false;

                        numPulseCount.Enabled = false;
                        chkPulseStopNeeded.Enabled = false;
                        numGatingFreq.Enabled = false;
                        numDutyCycle.Enabled = false;
                        btnUpdatePresets.Enabled = false;
                        btnCancelPresets.Enabled = false;

                        numDAQStart.Enabled = false;
                        numLaserStart.Enabled = false;
                        numDAQStop.Enabled = false;
                        btnPrepare.Enabled = false;
                        btnRun.Enabled = false;
                        btnStop.Enabled = true;

                        chkRepeat.Enabled = false;
                        numRepeatTime.Enabled = false;
                        numRepeatCount.Enabled = false;

                        chkFilterPeaks.Enabled = false;
                        numPeakThreshold.Enabled = false;
                        numPeakHysteresis.Enabled = false;
                        numMaxPeakWidth.Enabled = false;
                        break;

                    default:
                        throw new ApplicationException("Tried to set invalid state");
                }
            }            
        }

        #endregion

        public DFM_Form_old()
        {
            InitializeComponent();

            /*Shown += new EventHandler(DFM_Form_Shown);
        }

        void DFM_Form_Shown(object sender1, EventArgs e1)
        {*/
            // mark 1                       

            // set up sensors and laser
            sensors = new ISensor[2];
            sensors[0] = new ILD2200_MESensor();
            sensors[1] = new ILD1700_MESensor();
            
            // mark 2
            
            laser = new PLCLaser();
            galilForm = new GalilForm();
            galilForm.m.FinishedSetup += new Motor.MotorTaskHandler(m_FinishedSetup);
            galilForm.m.FinishedMove += new Motor.MotorTaskHandler(m_FinishedMove);

            // set default stuff
            UpdateSensorModels();
            UpdateSensorTypes();
            UpdateSensorPorts();
            UpdatePLCPorts();
            UpdateSensorBaudrate();
            UpdatePLCBaudrate();

            // mark 3
            
            // create graph curve
            GraphPane pane = zgcScan.GraphPane;
            PointPairList list = new PointPairList();
            LineItem curve = pane.AddCurve("distance", list, Color.Black, SymbolType.None);
            
            // hide legend
            curve.Label.IsVisible = false;
            // set axis labels
            pane.Title.Text = "Distance";
            pane.XAxis.Title.Text = "Time (s)";
            pane.YAxis.Title.Text = "Distance (microns)";


            GraphPane pane2 = zgcRollingGraph.GraphPane;
            IPointList list2 = new RollingPointPairList(11);
            LineItem curve2 = pane2.AddCurve("data", list2, Color.Blue, SymbolType.Circle);
            curve2.Label.IsVisible = true;
            pane2.Title.Text = "Readings";
            pane2.XAxis.Title.Text = "Reading #";
            pane2.YAxis.Title.Text = "Distance (microns)";

            // boundary lines
            PointPairList boundaryList1 = new PointPairList();
            LineItem boundaryCurve1 = pane2.AddCurve("boundary1", boundaryList1, Color.Red, SymbolType.None);
            boundaryCurve1.Label.IsVisible = false;

            PointPairList boundaryList2 = new PointPairList();
            LineItem boundaryCurve2 = pane2.AddCurve("boundary2", boundaryList2, Color.Red, SymbolType.None);
            boundaryCurve2.Label.IsVisible = false;

            numBoundaryLow.Value = (decimal)19.0;
            numBoundaryHigh.Value = (decimal)29.0;

            ClearRollingGraphData();
            UpdateBoundaryLines();

            // mark 4

            // defaults
            for (int i = 0; i < cmbSensorPort.Items.Count; i++)
            {
                if (cmbSensorPort.Items[i].ToString() == "COM9")
                {
                    cmbSensorPort.SelectedIndex = i;
                    break;
                }
            }
            for (int i = 0; i < cmbPLCPort.Items.Count; i++)
            {
                if (cmbPLCPort.Items[i].ToString() == "COM2")
                {
                    cmbPLCPort.SelectedIndex = i;
                    break;
                }
            }

            pulseCount = PulseCount = 10;
            gatingFrequency = GatingFrequency = 200;
            dutyCycle = DutyCycle = 90;

            chkFilterPeaks.Checked = true;
            numPeakThreshold.Value = (decimal)40.0;
            numPeakThreshold.Enabled = true;
            numPeakHysteresis.Value = (decimal)20.0;
            numPeakHysteresis.Enabled = true;
            numMaxPeakWidth.Value = (decimal)100;
            numMaxPeakWidth.Enabled = true;

            // set default state
            State = States.DISCONNECTED;

            // create bg worker
            worker = new BackgroundWorker();
            worker.WorkerReportsProgress = true;
            worker.WorkerSupportsCancellation = true;

            worker.DoWork += new DoWorkEventHandler(worker_DoWork);
            worker.ProgressChanged += new ProgressChangedEventHandler(worker_ProgressChanged);
            worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(worker_RunWorkerCompleted);

            // add event handlers
            // form closing
            btnClose.Click += new EventHandler(btnClose_Click);
            this.FormClosing += new FormClosingEventHandler(LaserControlForm_FormClosing);
            
            cmbSensorModel.SelectedIndexChanged += new EventHandler(cmbSensorModel_SelectedIndexChanged);
            btnConnect.Click += new EventHandler(btnConnect_Click);
            btnShowGalil.Click += new EventHandler(btnShowGalil_Click);

            numPulseCount.ValueChanged += new EventHandler(laserPreset_ValueChanged);
            chkPulseStopNeeded.CheckedChanged += new EventHandler(laserPreset_ValueChanged);
            numGatingFreq.ValueChanged += new EventHandler(laserPreset_ValueChanged);
            numDutyCycle.ValueChanged += new EventHandler(laserPreset_ValueChanged);

            btnUpdatePresets.Click += new EventHandler(btnUpdatePresets_Click);
            btnCancelPresets.Click += new EventHandler(btnCancelPresets_Click);

            btnPrepare.Click += new EventHandler(btnPrepare_Click);
            btnRun.Click += new EventHandler(btnRun_Click);
            btnStop.Click += new EventHandler(btnStop_Click);

            btnChooseDataFolder.Click += new EventHandler(btnChooseDataFolder_Click);
            btnLoadLastScan.Click += new EventHandler(btnLoadLastScan_Click);

            chkConnectToSensor.CheckedChanged += new EventHandler(chkConnectToSensor_CheckedChanged);
            chkRepeat.CheckedChanged += new EventHandler(chkContinuous_CheckedChanged);

            chkFilterPeaks.CheckedChanged += new EventHandler(delegate(object sender, EventArgs e)
            {
                numPeakThreshold.Enabled = chkFilterPeaks.Checked;
                numPeakHysteresis.Enabled = chkFilterPeaks.Checked;
                numMaxPeakWidth.Enabled = chkFilterPeaks.Checked;
            });

            numBoundaryLow.ValueChanged += new EventHandler(numBoundary_ValueChanged);
            numBoundaryHigh.ValueChanged += new EventHandler(numBoundary_ValueChanged);
            btnClearRollingGraph.Click += new EventHandler(btnClearRollingGraph_Click);

            // mark 5

            // try and connect to the sensor/PLC, suppress any error messages
            BackgroundWorker connectWorker = new BackgroundWorker();
            connectWorker.DoWork += new DoWorkEventHandler(delegate(object sender, DoWorkEventArgs e)
                {
                    bool result = TryConnect();
                    e.Result = result;
                });
            connectWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(delegate(object sender, RunWorkerCompletedEventArgs e)
                {
                    btnConnect.Enabled = true;

                    if ((bool)e.Result)
                    {
                        btnConnect.Text = DFM_Form_old.disconnectText;
                        cmbSensorBaudrate.SelectedItem = sensor.BaudRate.ToString();
                    }
                });

            btnConnect.Enabled = false;                    
            connectWorker.RunWorkerAsync();/**/        
        }
               
        #region Rolling Graph
        
        private double[] readingsAverageBuffer = new double[3];
        private int bufferIndex = 0;

        private void AddRollingGraphData(double value)
        {
            // work out new average
            readingsAverageBuffer[bufferIndex++] = value;
            bufferIndex %= readingsAverageBuffer.Length;
            double average = 0.0;
            int i;
            for (i = 0; i < readingsAverageBuffer.Length; i++)
            {
                if (readingsAverageBuffer[i] == 0)
                {
                    break;
                }
                average += readingsAverageBuffer[i];
            }

            if (i > 0) average /= i;

            if (i == 3)
            {
                txtRollingGraphDisplay.Text = average.ToString("0.00");

                CurveItem dataCurve = zgcRollingGraph.GraphPane.CurveList["data"];
                int x = 1; // start at 1
                if (dataCurve.Points.Count > 0)
                {
                    x = (int)(dataCurve.Points[dataCurve.Points.Count - 1].X) + 1;
                }
                dataCurve.AddPoint(x, average);

                // fix scale
                if (x > 10)
                {
                    Scale scale = zgcRollingGraph.GraphPane.XAxis.Scale;
                    scale.Min += 1;
                    scale.Max += 1;
                }

                zgcRollingGraph.AxisChange();
                zgcRollingGraph.Invalidate();
            }
            else
            {
                txtRollingGraphDisplay.Text = "0.00";
            }
        }

        private void ClearRollingGraphData()
        {
            Scale scale = zgcRollingGraph.GraphPane.XAxis.Scale;
            scale.Min = 0;
            scale.Max = 12;

            zgcRollingGraph.GraphPane.CurveList["data"].Clear();

            zgcRollingGraph.AxisChange();
            zgcRollingGraph.Invalidate();

            for (int i = 0; i < readingsAverageBuffer.Length; i++)
            {
                readingsAverageBuffer[i] = 0.0;
            }
            txtRollingGraphDisplay.Text = "";
        }

        private void UpdateBoundaryLines()
        {
            CurveItem boundaryCurve1 = zgcRollingGraph.GraphPane.CurveList["boundary1"];
            CurveItem boundaryCurve2 = zgcRollingGraph.GraphPane.CurveList["boundary2"];

            boundaryCurve1.Clear();
            boundaryCurve2.Clear();

            double line1 = (double)numBoundaryLow.Value;
            double line2 = (double)numBoundaryHigh.Value;

            boundaryCurve1.AddPoint(0.0, line1);
            boundaryCurve1.AddPoint(10000.0, line1);

            boundaryCurve2.AddPoint(0.0, line2);
            boundaryCurve2.AddPoint(10000.0, line2);

            zgcRollingGraph.AxisChange();
            zgcRollingGraph.Invalidate();
        }

        void numBoundary_ValueChanged(object sender, EventArgs e)
        {
            UpdateBoundaryLines();
        }

        void btnClearRollingGraph_Click(object sender, EventArgs e)
        {
            ClearRollingGraphData();
        }

        #endregion
        
        #region Update Functions

        #region Form Closing

        private void BeforeCloseForm()
        {
            // stop any running measurments
            if (State == States.RUNNING || State == States.PREPARED)
            {
                Stop();
            }

            // disconnect from any open ports
            if (State == States.PRESETS_SET || State == States.CONNECTED)
            {
                Disconnect();
            }

            // disconnect from Galil (stops polling)
            galilForm.Disconnect();
        }

        #endregion

        #region Connection Updates

        private void UpdateSensorModels()
        {
            cmbSensorModel.Items.Clear();

            // add the sensor models
            foreach (ISensor s in sensors)
            {
                cmbSensorModel.Items.Add(s.Properties.ModelName);
            }
            
            cmbSensorModel.SelectedIndex = 0;
        }

        private void UpdateSensorTypes()
        {
            cmbSensorType.Items.Clear();

            // get selected sensor
            ISensor selectedSensor = sensors[cmbSensorModel.SelectedIndex];

            // add the sensor types
            foreach (string type in selectedSensor.SensorTypes)
            {
                cmbSensorType.Items.Add(type);
            }

            cmbSensorType.SelectedIndex = 0;
        }

        private void UpdateSensorPorts()
        {
            string[] ports = SerialPort.GetPortNames();
            Array.Sort(ports);
            
            cmbSensorPort.Items.Clear();
            cmbSensorPort.Items.AddRange(ports);
            if (ports.Length > 0)
            {
                cmbSensorPort.SelectedIndex = 0;
            }
        }

        private void UpdatePLCPorts()
        {
            string[] ports = SerialPort.GetPortNames();
            Array.Sort(ports);

            cmbPLCPort.Items.Clear();
            cmbPLCPort.Items.AddRange(ports);
            if (ports.Length > 0)
            {
                cmbPLCPort.SelectedIndex = 0;
            }
        }

        private void UpdateSensorBaudrate()
        {
            cmbSensorBaudrate.Items.Clear();
            cmbSensorBaudrate.Items.Add("Auto detect");

            int[] baudrates = sensors[cmbSensorModel.SelectedIndex].BaudRates;

            foreach (int i in baudrates)
            {
                cmbSensorBaudrate.Items.Add(i.ToString());
            }

            cmbSensorBaudrate.SelectedIndex = 0;
        }

        private void UpdatePLCBaudrate()
        {
            cmbPLCBaudrate.Items.Clear();
            cmbPLCBaudrate.Items.Add("Autodetect");
            int[] baudrates = laser.Baudrates;

            foreach (int i in baudrates)
            {
                cmbPLCBaudrate.Items.Add(i.ToString());
            }

            cmbPLCBaudrate.SelectedIndex = 0;
        }

        private void UpdateSensorConnectionEnabled()
        {
            cmbSensorModel.Enabled = chkConnectToSensor.Checked;
            cmbSensorType.Enabled = chkConnectToSensor.Checked;
            cmbSensorPort.Enabled = chkConnectToSensor.Checked;
            cmbSensorBaudrate.Enabled = chkConnectToSensor.Checked;
        }
        
        #endregion

        #region Connection

        private bool TryConnect()
        {
            // update sensor and other selections
            sensor = sensors[cmbSensorModel.SelectedIndex];

            try
            {
                // try and connect to laser
                if (cmbPLCBaudrate.SelectedIndex == 0)
                {
                    laser.Connect(cmbPLCPort.SelectedItem.ToString(), true);
                }
                else
                {
                    laser.Connect(cmbPLCPort.SelectedItem.ToString(), Convert.ToInt32(cmbPLCBaudrate.SelectedItem.ToString()));
                    cmbPLCBaudrate.SelectedItem = laser.Baudrate.ToString();
                }
            }
            catch (Exception)
            {
                laser.Disconnect();
                return false;
            }

            if (chkConnectToSensor.Checked)
            {
                try
                {
                    // try and connect to sensor
                    if (cmbSensorBaudrate.SelectedIndex == 0)
                    {
                        sensor.Connect(cmbSensorPort.SelectedItem.ToString());
                    }
                    else
                    {
                        sensor.Connect(cmbSensorPort.SelectedItem.ToString(), Convert.ToInt32(cmbSensorBaudrate.SelectedItem));
                    }
                }
                catch (Exception)
                {
                    laser.Disconnect();
                    sensor.Disconnect();
                    
                    return false;
                }

                sensor.ReadingReference = ReadingReferences.SMR;
            }

            if (!galilForm.Connect()) return false;

            State = States.CONNECTED;
            return true;
        }

        private void Connect()
        {
            btnConnect.Enabled = false;

            if (btnConnect.Text == DFM_Form_old.connectText)
            {
                // connecting

                // update sensor and other selections
                sensor = sensors[cmbSensorModel.SelectedIndex];

                if (chkConnectToLaser.Checked)
                {
                    try
                    {
                        // try and connect to laser
                        if (cmbPLCBaudrate.SelectedIndex == 0)
                        {
                            laser.Connect(cmbPLCPort.SelectedItem.ToString());
                        }
                        else
                        {
                            laser.Connect(cmbPLCPort.SelectedItem.ToString(), Convert.ToInt32(cmbPLCBaudrate.SelectedItem.ToString()));
                            cmbPLCBaudrate.SelectedItem = laser.Baudrate.ToString();
                        }
                    }
                    catch (Exception ex)
                    {
                        laser.Disconnect();
                        btnConnect.Enabled = true;

                        string message = "There was an error connecting to the laser. Try changing the COM ports or baud rates. The error was '" + ex.Message + "'";
                        MessageBox.Show(message);

                        return;
                    }
                }

                if (chkConnectToSensor.Checked)
                {
                    try
                    {
                        // try and connect to sensor
                        if (cmbSensorBaudrate.SelectedIndex == 0)
                        {
                            sensor.Connect(cmbSensorPort.SelectedItem.ToString());
                            cmbSensorBaudrate.SelectedItem = sensor.BaudRate.ToString();
                        }
                        else
                        {
                            sensor.Connect(cmbSensorPort.SelectedItem.ToString(), Convert.ToInt32(cmbSensorBaudrate.SelectedItem));
                        }
                    }
                    catch (Exception ex)
                    {
                        if (chkConnectToLaser.Checked) laser.Disconnect();
                        sensor.Disconnect();
                        btnConnect.Enabled = true;

                        string message = "There was an error connecting to the sensor. Try changing the COM ports or baud rates. The error was '" + ex.Message + "'";
                        MessageBox.Show(message);

                        return;
                    }

                    sensor.ReadingReference = ReadingReferences.SMR;
                }

                // connect to Galil as well
                if (!galilForm.IsConnected && !galilForm.Connect())
                {
                    MessageBox.Show("There was an error connecting to the Galil. Check the address and try again");

                    if (chkConnectToLaser.Checked) laser.Disconnect();
                    if (chkConnectToSensor.Checked) sensor.Disconnect();
                    btnConnect.Enabled = true;

                    return;
                }

                btnConnect.Text = disconnectText;
                State = States.CONNECTED;
            }
            else
            {
                // disconnecting
                Disconnect();
            }
                        
            btnConnect.Enabled = true;
        }

        private void Disconnect()
        {
            // disconnect from sensor and laser
            if (chkConnectToSensor.Checked)
            {
                sensor.Disconnect();
            }
            laser.Disconnect();
            galilForm.Disconnect();

            State = States.DISCONNECTED;
            btnConnect.Text = "Connect";
        }

        #endregion

        #region Laser Presets

        private void UpdateLaserPresets()
        {
            if (chkConnectToLaser.Checked)
            {
                bool response;

                string value = RunTime.ToString().PadLeft(5, '0');
                response = laser.Run("R", value);
                if (!response)
                {
                    MessageBox.Show("Could not set pulse count");
                    return;
                }
                pulseCount = PulseCount;

                response = laser.Run("F", GatingFrequency.ToString().PadLeft(5, '0'));
                if (!response)
                {
                    MessageBox.Show("Could not set gating frequency");
                    return;
                }
                gatingFrequency = GatingFrequency;

                response = laser.Run("D", DutyCycle.ToString().PadLeft(4, '0'));
                if (!response)
                {
                    MessageBox.Show("Could not set duty cycle");
                    return;
                }
                dutyCycle = DutyCycle;
            }

            // update state
            State = States.PRESETS_SET;
        }

        private void CancelLaserPresets()
        {
            PulseCount = pulseCount;
            GatingFrequency = gatingFrequency;
            DutyCycle = dutyCycle;
        }

        #endregion

        #region Start/Stop

        private void Prepare()
        {
            // check delays
            if (chkConnectToSensor.Checked && numDAQStart.Value > numDAQStop.Value)
            {
                MessageBox.Show("DAQ start time should be before DAQ stop time");
                return;
            }
            if (chkConnectToSensor.Checked && numLaserStart.Value > numDAQStop.Value)
            {
                MessageBox.Show("Laser should start before DAQ stops or you won't get any data");
                return;
            }
            if (chkRepeat.Checked && (numLaserStart.Value > numRepeatTime.Value * 1000 || (chkConnectToSensor.Checked && (numDAQStart.Value > numRepeatTime.Value * 1000 || numDAQStop.Value > numRepeatTime.Value * 1000))))
            {
                MessageBox.Show("Make sure the repeat time is longer than the other times");
                return;
            }
            if (txtDataFolder.Text.Trim() == "" || !Directory.Exists(txtDataFolder.Text) || txtFilenamePattern.Text.Trim() == "")
            {
                MessageBox.Show("Please select a valid folder and enter a filename pattern");
                return;
            }

            if (!galilForm.IsConnected)
            {
                bool success = galilForm.Connect();
                if (!success)
                {
                    MessageBox.Show("Unable to connect to the Galil. Open the Galil window for more information.");
                    return;
                }
            }

            if (chkConnectToSensor.Checked)
            {
                // get frequency
                double frequency = Convert.ToDouble(sensor.Parameters["frequency"].Get());

                // set sensor time increment
                sensor.TimeIncrement = 1.0 / frequency;
                sensor.CurrentTime = 0;

                sensor.EnsureReadingsInitialised();
                sensor.PortBuffer.Clear();
            }

            // setup Galil
            if (galilForm.SkipSetup)
            {
                State = States.PREPARED;
            }
            else
            {
                btnPrepare.Enabled = false;
                galilForm.Setup();
                // calls m_FinishedSetup when done
            }
        }

        void m_FinishedSetup(object sender, Motor.MotorTaskEventArgs e)
        {
            if (!e.Success)
            {
                MessageBox.Show("Setup of Galil failed " + e.Error.Message);
                btnPrepare.Enabled = true;
                return;
            }

            State = States.PREPARED;
        }

        private void Run()
        {
            btnRun.Enabled = false;
            btnStop.Enabled = false;

            galilForm.MoveTo();
            // calls m_FinishedMove when done
        }

        void m_FinishedMove(object sender, Motor.MotorTaskEventArgs e)
        {
            if (!e.Success)
            {
                MessageBox.Show("Moving Galil to position failed: " + e.Error.Message);
                btnRun.Enabled = false;
                btnStop.Enabled = false;
            }

            StopWorker();

            // start the async operation
            worker.RunWorkerAsync();

            // change state
            State = States.RUNNING;
        }
        
        private void Stop()
        {
            btnStop.Enabled = false;

            // stop worker
            StopWorker();
            
            btnStop.Enabled = true;

            StoppedMeasurements();
        }

        private void StopWorker()
        {
            int i = 0;
            worker.CancelAsync();
            
            // stop worker
            while (i < 5 && worker.IsBusy)
            {
                // wait until it does                
                Thread.Sleep(100 * (i + 1));
                i += 1;
            }
        }

        private void StoppedMeasurements()
        {
            State = States.PRESETS_SET;
        }

        #endregion
        
        #region Background Worker

        void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            // restore form
            StoppedMeasurements();

            // indicates an exception was raised
            if (e.Error != null)
            {
                MessageBox.Show("Error running scan: " + e.Error.Message);
                return;
            }

            if (!chkRepeat.Checked && e.Cancelled)
            {
                // MessageBox.Show("Measurements were cancelled");
                return;
            }
        }

        void worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            LoadLastScan();
        }

        void startDAQ()
        {
            sensor.PortBuffer.Clear();
        }

        void stopDAQ()
        {
            sensor.PortBuffer.StopAutoUpdate();
        }
        
        bool startLaser()
        {
            if (!chkConnectToLaser.Checked) return true;
            return laser.Run("G");            
        }

        bool killLaser()
        {
            if (!chkConnectToLaser.Checked) return true;
            return laser.Run("K");
        }

        private bool sleep(int time, BackgroundWorker worker, DoWorkEventArgs e)
        {
            int count = time / 1000 + 1;
            for (int i = 0; i < count; i++)
            {
                if (i == count - 1)
                {
                    Thread.Sleep(time - (count - 1) * 1000);
                }
                else
                {
                    Thread.Sleep(1000);
                }

                if (worker.CancellationPending)
                {
                    e.Cancel = true;
                    return false;
                }
            }

            return true;
        }

        void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = sender as BackgroundWorker;

            // get times needed
            int daqStartDelay = (int)numDAQStart.Value;
            int laserDelay = (int)numLaserStart.Value;
            int daqStopDelay = (int)numDAQStop.Value;
            int repeatTime = (int)numRepeatTime.Value * 1000;

            int runCount = 0;


            if (chkConnectToSensor.Checked)
            {
                while (true)
                {
                    runCount += 1;

                    // start autoupdating
                    sensor.PortBuffer.AutoUpdate(50);
                    int delay = 0;

                    if (daqStartDelay < laserDelay)
                    {
                        if (!sleep(daqStartDelay, worker, e))
                        {
                            stopDAQ();
                            return;
                        }
                        startDAQ();

                        if (worker.CancellationPending)
                        {
                            e.Cancel = true;
                            stopDAQ();
                            return;
                        }

                        if (!sleep(laserDelay - daqStartDelay, worker, e))
                        {
                            stopDAQ();
                            return;
                        }
                        if (!startLaser())
                        {
                            stopDAQ();
                            throw new ApplicationException("Could not activate laser gating");
                        }

                        delay = daqStopDelay - laserDelay;
                    }
                    else if (daqStartDelay == laserDelay)
                    {
                        if (!sleep(daqStartDelay, worker, e))
                        {
                            stopDAQ();
                            return;
                        }
                        startDAQ();
                        if (!startLaser())
                        {
                            stopDAQ();
                            throw new ApplicationException("Could not activate laser gating");
                        }

                        delay = daqStopDelay - laserDelay;
                    }
                    else
                    {
                        if (!sleep(laserDelay, worker, e))
                        {
                            stopDAQ();
                            return;
                        }
                        if (!startLaser())
                        {
                            stopDAQ();
                            throw new ApplicationException("Could not activate laser gating");
                        }

                        if (worker.CancellationPending)
                        {
                            e.Cancel = true;
                            stopDAQ();
                            return;
                        }

                        if (!sleep(daqStartDelay - laserDelay, worker, e))
                        {
                            stopDAQ();
                            return;
                        }
                        startDAQ();

                        delay = daqStopDelay - daqStartDelay;
                    }

                    if (delay > 0)
                    {
                        if (worker.CancellationPending)
                        {
                            e.Cancel = true;
                            stopDAQ();
                            return;
                        }

                        if (!sleep(delay, worker, e))
                        {
                            stopDAQ();
                            return;
                        }
                    }
                    stopDAQ();

                    if (worker.CancellationPending)
                    {
                        e.Cancel = true;
                        return;
                    }

                    // read data
                    SensorReading[] readings = sensor.TakeReadings();

                    // save readings
                    // create x-values and y_values to write to file
                    int rLen = readings.Length;
                    double[] data_x;
                    double[] data_y;
                    List<double> list_x = new List<double>();
                    List<double> list_y = new List<double>();
                    for (int j = 0; j < rLen; j++)
                    {
                        if (!readings[j].IsError)
                        {
                            list_x.Add(readings[j].Time);
                            list_y.Add(readings[j].Value);
                        }
                    }

                    data_x = list_x.ToArray();
                    data_y = list_y.ToArray();

                    // truncate data
                    rLen = data_x.Length;
                    if (rLen > 0)
                    {
                        int start_index = 0;
                        int end_index = rLen; // not included

                        if (Convert.ToDouble(numTruncateFirst.Value) > 0)
                        {
                            start_index = (int)Math.Floor(Convert.ToDouble(numTruncateFirst.Value) / 100 * rLen);
                        }
                        if (Convert.ToDouble(numTruncateLast.Value) > 0)
                        {
                            end_index = rLen - (int)Math.Ceiling(Convert.ToDouble(numTruncateLast.Value) / 100 * rLen);
                        }

                        if (end_index > start_index)
                        {
                            double[] data_x_tmp = new double[end_index - start_index];
                            double[] data_y_tmp = new double[end_index - start_index];

                            for (int i = start_index, j = 0; i < end_index; i++, j++)
                            {
                                data_x_tmp[j] = data_x[i];
                                data_y_tmp[j] = data_y[i];
                            }

                            data_x = data_x_tmp;
                            data_y = data_y_tmp;
                        }
                    }

                    /*
                     * moved noise filtering to LoadScan
                    if (chkFilterPeaks.Checked)
                    {
                        double[] data_y_temp = new double[rLen];
                        for (int j = 0; j < rLen; j++)
                        {
                            data_y_temp[j] = data_y[j];
                        }
                        NoiseFilter.Filter(data_y_temp, out data_y, (double)numPeakThreshold.Value / 1000);
                    }*/

                    rLen = data_x.Length;
                    if (rLen > 0)
                    {
                        ScanManager scanManager = new ScanManager(txtDataFolder.Text, txtFilenamePattern.Text);
                        int nextScanId = scanManager.GetNextScanId();
                        ScanFile scanFile = new ScanFile(txtDataFolder.Text, txtFilenamePattern.Text, nextScanId);

                        // write data
                        DataIO dataIO = new DataIO();
                        dataIO.Mode = DataIO.Modes.Write;
                        dataIO.Filename = txtDataFolder.Text + scanFile.Filename;
                        dataIO.WriteData(data_x, data_y);
                    }
                    else
                    {
                        MessageBox.Show("Could not get any non-error readings");
                    }

                    if (!chkRepeat.Checked || runCount >= (int)numRepeatCount.Value)
                    {
                        if (rLen > 0) worker.ReportProgress(100);
                        return;
                    }
                    else
                    {
                        if (rLen > 0) worker.ReportProgress(100);

                        if (worker.CancellationPending)
                        {
                            e.Cancel = true;
                            return;
                        }

                        if (!sleep(repeatTime - daqStopDelay, worker, e)) return;

                        if (worker.CancellationPending)
                        {
                            e.Cancel = true;
                            return;
                        }
                    }
                }
            }
            else if (chkConnectToLaser.Checked)
            {
                while (true)
                {
                    runCount += 1;

                    // start laser
                    if (!sleep(laserDelay, worker, e)) return;
                    if (!startLaser())
                    {
                        throw new ApplicationException("Could not activate laser gating");
                    }

                    if (!chkRepeat.Checked || runCount >= (int)numRepeatCount.Value)
                    {
                        return;
                    }
                    else
                    {
                        if (worker.CancellationPending)
                        {
                            e.Cancel = true;
                            return;
                        }

                        if (!sleep(repeatTime - laserDelay, worker, e)) return;

                        if (worker.CancellationPending)
                        {
                            e.Cancel = true;
                            return;
                        }
                    }
                }
            }
        }

        #endregion

        #region Loading Scans

        bool LoadLastScan()
        {            
            ScanManager scanManager = new ScanManager(txtDataFolder.Text, txtFilenamePattern.Text);
            int lastScanId = scanManager.GetLastScanId();
            if (lastScanId == -1)
            {
                return false;
            }

            return LoadScanResults(lastScanId);
        }

        private bool LoadScanResults(int scanId)
        {            
            ScanFile scanFile = new ScanFile(txtDataFolder.Text, txtFilenamePattern.Text, scanId);
            
            string file = Path.Combine(txtDataFolder.Text, scanFile.FindFilename());
            if (file != "")
            {
                LoadScan(file);
                return true;
            }

            return false;
        }

        private void LoadScan(string filename)
        {
            DFMAnalysis analysis = new DFMAnalysis();
            analysis.Options = new DFMAnalysisOptions();
            analysis.Options.SmoothingWidth = (int)numSmoothingWidth.Value;
            analysis.Options.ThresholdParameter = (double)numThresholdParam.Value;
            analysis.Options.MinPitWidth = (int)numMinPitWidth.Value;
            analysis.Options.Compression = (double)numCompressionFactor.Value;
            analysis.Options.SubstrateRoughness = (double)numSubstrateRoughness.Value;
            analysis.Options.PitPoints = (int)numPitPoints.Value;

            DataIO reader = new DataIO();
            reader.Mode = DataIO.Modes.Read;
            reader.Filename = filename;

            
            double[] data_x, data_y;
            reader.ReadData(out data_x, out data_y);

            // clear old graphs
            zgcScan.GraphPane.CurveList.Clear();

            // add raw data graph
            double[] data_y1000 = new double[data_x.Length];
            for (int i = 0; i < data_x.Length; i++)
            {
                data_y1000[i] = data_y[i] * 1000;
            }            
            if (chkDebugMode.Checked) addGraph("raw data", data_x, data_y1000, Color.Green);
            
            // filter data
            if (chkFilterPeaks.Checked)
            {
                int rLen = data_y.Length;
                double[] data_y_temp = new double[rLen];
                for (int j = 0; j < rLen; j++)
                {
                    data_y_temp[j] = data_y[j];
                }
                NoiseFilter.Filter(data_y_temp, out data_y, (double)numPeakThreshold.Value / 1000, (double)numPeakHysteresis.Value / 1000, analysis.Options.SmoothingWidth, (int)numMaxPeakWidth.Value);
            }
            
            analysis.SetData(data_x, data_y);
            analysis.Analyse();

            txtPitDepth.Text = analysis.Data.PeakMean.ToString("0.0") + " +/- " + analysis.Data.PeakSD.ToString("0.0");
            txtNumPits.Text = analysis.Data.PeakCount.ToString();
            lstPits.Items.Clear();
            foreach (double pit in analysis.Data.Peaks) 
                lstPits.Items.Add(pit.ToString("0.0"));


            if (!chkDebugMode.Checked) addGraph("normalised data", analysis.Data.Data_X, analysis.Data.Normalised_Y, Color.Black);
            if (chkDebugMode.Checked) addGraph("unweighted smoothed data", analysis.Data.Data_X, analysis.Data.Unweighted_Smoothed_Y, Color.Plum);
            //if (chkDebugMode.Checked) addGraph("weighted smoothed data", analysis.Data.Data_X, analysis.Data.Weighted_Smoothed_Y, Color.Blue);
            if (chkDebugMode.Checked) addGraph("filtered data", analysis.Data.Data_X, analysis.Data.Data_Y, Color.Red);
            
            zgcScan.AxisChange();
            zgcScan.Invalidate();

            // add new data to rolling graph
            AddRollingGraphData(analysis.Data.PeakMean);
        }

        private void addGraph(string label, double[] data_x, double[] data_y, Color color)
        {
            PointPairList list = new PointPairList();
            LineItem curve = zgcScan.GraphPane.AddCurve(label, list, color, SymbolType.None);
            // display data
            for (int i = 0; i < data_x.Length; i++)
            {
                list.Add(data_x[i], data_y[i]);
            }
        }
        
        #endregion

        #endregion

        #region Event Handlers

        #region Form Closing

        void LaserControlForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            BeforeCloseForm();
        }

        void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #endregion

        #region Connection

        void btnConnect_Click(object sender, EventArgs e)
        {
            Connect();
        }

        void cmbSensorModel_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateSensorTypes();
            UpdateSensorPorts();
            UpdateSensorBaudrate();
        }

        void chkConnectToSensor_CheckedChanged(object sender, EventArgs e)
        {
            UpdateSensorConnectionEnabled();
        }

        #endregion

        #region Laser Presets

        void btnCancelPresets_Click(object sender, EventArgs e)
        {
            CancelLaserPresets();
        }

        void btnUpdatePresets_Click(object sender, EventArgs e)
        {
            UpdateLaserPresets();
        }

        void laserPreset_ValueChanged(object sender, EventArgs e)
        {
            if (State != States.CONNECTED)
            {
                btnCancelPresets.Enabled = true;
            }

            btnUpdatePresets.Enabled = true;
        }

        #endregion

        #region Start/Stop

        void btnStop_Click(object sender, EventArgs e)
        {
            Stop();
        }

        void btnRun_Click(object sender, EventArgs e)
        {
            Run();
        }

        void btnPrepare_Click(object sender, EventArgs e)
        {
            Prepare();
        }

        #endregion
                
        void btnChooseDataFolder_Click(object sender, EventArgs e)
        {
            folderBrowser.ShowDialog();

            if (folderBrowser.SelectedPath != "")
            {
                txtDataFolder.Text = folderBrowser.SelectedPath;
            }
        }

        void btnLoadLastScan_Click(object sender, EventArgs e)
        {
            if (txtDataFolder.Text.Trim() == "" || !Directory.Exists(txtDataFolder.Text) || txtFilenamePattern.Text.Trim() == "")
            {
                MessageBox.Show("Please select a valid folder and enter a filename pattern");
                return;
            }

            LoadLastScan();
        }

        void chkContinuous_CheckedChanged(object sender, EventArgs e)
        {
            numRepeatTime.Enabled = chkRepeat.Checked;
            numRepeatCount.Enabled = chkRepeat.Checked;
        }

        void btnShowGalil_Click(object sender, EventArgs e)
        {
            galilForm.Visible = true;
        }        

        #endregion

    }
}
