﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

using DFM.Settings;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Text;

namespace DFM
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            try
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new DFM_Form_simple());
            }
            catch (Exception ex)
            {
                throw;
            }
            /*SettingsGroup grp = new SettingsGroup("The Awesome Settings");
            grp.Add(new BaseSetting("Setting 1"));

            grp.Add(new SettingValue<int>("my int", 1));
            grp.Add(new SettingValue<int>("my int 2", 12));
            grp.Add(new SettingValue<string>("my string", "apple"));

            SettingsGroup nested = new SettingsGroup("What, what WHAT?");
            nested.Add(new SettingValue<int>("surprise", Int32.MaxValue));

            grp.Add(nested);


            SettingsGroup grp2 = new SettingsGroup("The Awesome Settings take 2");
            grp2.Add(new BaseSetting("Setting 1"));
            grp2.Add(new SettingValue<int>("my int", 19));
            grp2.Add(new SettingValue<int>("my int 2", -42));
            grp2.Add(new SettingValue<string>("my string", "bananananana"));
            SettingsGroup nested2 = new SettingsGroup("What, what WHAT?");
            nested2.Add(new SettingValue<int>("surprise", Int32.MinValue));
            grp2.Add(nested2);

            Type[] types = BaseSetting.SettingTypes.ToArray(); //{ typeof(BaseSetting), typeof(SettingValue<int>), typeof(SettingValue<string>) };
            XmlSerializer serializer = new XmlSerializer(typeof(SettingsGroup), types);
            FileStream fs = new FileStream("Personenliste.xml", FileMode.Create);
            serializer.Serialize(fs, grp);
            fs.Close();
            grp = null;


            // Deserialize 
            fs = new FileStream("Personenliste.xml", FileMode.Open);
            grp = (SettingsGroup)serializer.Deserialize(fs);
            serializer.Serialize(Console.Out, grp);


            int test;
            bool value = grp.TryFetch<int>(new string[] { "What, what WHAT?", "surprise" }, out test);

            BaseSetting test2 = grp["my int"];

            grp2.LoadSettingsFrom(grp);
            
            Console.ReadLine();*/
        }
    }
}
