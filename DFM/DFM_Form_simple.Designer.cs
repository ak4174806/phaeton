﻿namespace DFM
{
    partial class DFM_Form_simple
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.SplitContainer outerContainer;
            System.Windows.Forms.Label lblHeading;
            System.Windows.Forms.PictureBox picLogo;
            System.Windows.Forms.PictureBox pictureBox1;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DFM_Form_simple));
            this.zgcScanProfile = new ZedGraph.ZedGraphControl();
            this.lblError = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.lblTotalSampleLen = new System.Windows.Forms.Label();
            this.lblMode = new System.Windows.Forms.Label();
            this.btnAlgo = new System.Windows.Forms.Button();
            this.lblPitValueHead = new System.Windows.Forms.Label();
            this.Functions = new System.Windows.Forms.GroupBox();
            this.cmbPaintType = new System.Windows.Forms.ComboBox();
            this.grpStitchingAvging = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.nmNoScans = new System.Windows.Forms.NumericUpDown();
            this.rdAverageBtn = new System.Windows.Forms.RadioButton();
            this.rdStitchBtn = new System.Windows.Forms.RadioButton();
            this.rdNormalBtn = new System.Windows.Forms.RadioButton();
            this.btnDatabase = new System.Windows.Forms.Button();
            this.btnConnect = new System.Windows.Forms.Button();
            this.btnBrowse = new System.Windows.Forms.Button();
            this.btnRun = new System.Windows.Forms.Button();
            this.btnSettings = new System.Windows.Forms.Button();
            this.lblSegmentScanStatus = new System.Windows.Forms.Label();
            this.zgcPits = new ZedGraph.ZedGraphControl();
            this.grpBoxOutput = new System.Windows.Forms.GroupBox();
            this.lblRightStats = new System.Windows.Forms.Label();
            this.lblLeftStats = new System.Windows.Forms.Label();
            this.lblMiddleStats = new System.Windows.Forms.Label();
            this.btnClear = new System.Windows.Forms.Button();
            this.grpBoxPitValues = new System.Windows.Forms.GroupBox();
            this.btnCopyPit = new System.Windows.Forms.Button();
            this.txtPits = new System.Windows.Forms.TextBox();
            this.lblTotalPit = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.lagTimeTimer = new System.Windows.Forms.Timer(this.components);
            this.label4 = new System.Windows.Forms.Label();
            this.rtxtboxStatus = new System.Windows.Forms.RichTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tmrFade = new System.Timers.Timer();
            this.rtxtCommnt = new System.Windows.Forms.RichTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.openFileDialog2 = new System.Windows.Forms.OpenFileDialog();
            this.dispRightStats = new DFM.CaptionedDisplay();
            this.dispLeftStats = new DFM.CaptionedDisplay();
            this.dispMiddleStats = new DFM.CaptionedDisplay();
            this.showPits_value = new System.Windows.Forms.Button();
            outerContainer = new System.Windows.Forms.SplitContainer();
            lblHeading = new System.Windows.Forms.Label();
            picLogo = new System.Windows.Forms.PictureBox();
            pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(outerContainer)).BeginInit();
            outerContainer.Panel1.SuspendLayout();
            outerContainer.Panel2.SuspendLayout();
            outerContainer.SuspendLayout();
            this.Functions.SuspendLayout();
            this.grpStitchingAvging.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nmNoScans)).BeginInit();
            this.grpBoxOutput.SuspendLayout();
            this.grpBoxPitValues.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(picLogo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tmrFade)).BeginInit();
            this.SuspendLayout();
            // 
            // outerContainer
            // 
            outerContainer.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            outerContainer.Location = new System.Drawing.Point(0, 80);
            outerContainer.Name = "outerContainer";
            outerContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // outerContainer.Panel1
            // 
            outerContainer.Panel1.Controls.Add(this.zgcScanProfile);
            outerContainer.Panel1.Controls.Add(this.lblError);
            outerContainer.Panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.outerContainer_Panel1_Paint);
            outerContainer.Panel1MinSize = 175;
            // 
            // outerContainer.Panel2
            // 
            outerContainer.Panel2.Controls.Add(this.textBox1);
            outerContainer.Panel2.Controls.Add(this.button2);
            outerContainer.Panel2.Controls.Add(this.lblTotalSampleLen);
            outerContainer.Panel2.Controls.Add(this.lblMode);
            outerContainer.Panel2.Controls.Add(this.btnAlgo);
            outerContainer.Panel2.Controls.Add(this.lblPitValueHead);
            outerContainer.Panel2.Controls.Add(this.Functions);
            outerContainer.Panel2.Controls.Add(this.lblSegmentScanStatus);
            outerContainer.Panel2.Controls.Add(this.zgcPits);
            outerContainer.Panel2.Controls.Add(this.grpBoxOutput);
            outerContainer.Panel2.Controls.Add(this.btnClear);
            outerContainer.Panel2.Controls.Add(this.grpBoxPitValues);
            outerContainer.Panel2.Paint += new System.Windows.Forms.PaintEventHandler(this.outerContainer_Panel2_Paint);
            outerContainer.Size = new System.Drawing.Size(991, 515);
            outerContainer.SplitterDistance = 175;
            outerContainer.TabIndex = 2;
            outerContainer.TabStop = false;
            // 
            // zgcScanProfile
            // 
            this.zgcScanProfile.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.zgcScanProfile.Location = new System.Drawing.Point(20, 2);
            this.zgcScanProfile.Name = "zgcScanProfile";
            this.zgcScanProfile.ScrollGrace = 0D;
            this.zgcScanProfile.ScrollMaxX = 0D;
            this.zgcScanProfile.ScrollMaxY = 0D;
            this.zgcScanProfile.ScrollMaxY2 = 0D;
            this.zgcScanProfile.ScrollMinX = 0D;
            this.zgcScanProfile.ScrollMinY = 0D;
            this.zgcScanProfile.ScrollMinY2 = 0D;
            this.zgcScanProfile.Size = new System.Drawing.Size(949, 158);
            this.zgcScanProfile.TabIndex = 12;
            this.zgcScanProfile.Load += new System.EventHandler(this.zgcScanProfile_Load);
            // 
            // lblError
            // 
            this.lblError.AutoSize = true;
            this.lblError.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblError.Location = new System.Drawing.Point(385, 163);
            this.lblError.Name = "lblError";
            this.lblError.Size = new System.Drawing.Size(81, 13);
            this.lblError.TabIndex = 5;
            this.lblError.Text = "No. of Scans";
            this.lblError.Visible = false;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(321, 140);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(117, 20);
            this.textBox1.TabIndex = 53;
            this.textBox1.Visible = false;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(227, 140);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(81, 23);
            this.button2.TabIndex = 52;
            this.button2.Text = "Choose CSV";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Visible = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // lblTotalSampleLen
            // 
            this.lblTotalSampleLen.AutoSize = true;
            this.lblTotalSampleLen.BackColor = System.Drawing.Color.White;
            this.lblTotalSampleLen.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalSampleLen.Location = new System.Drawing.Point(657, 144);
            this.lblTotalSampleLen.Name = "lblTotalSampleLen";
            this.lblTotalSampleLen.Size = new System.Drawing.Size(131, 13);
            this.lblTotalSampleLen.TabIndex = 47;
            this.lblTotalSampleLen.Text = "Total Sample Length=";
            this.lblTotalSampleLen.Visible = false;
            // 
            // lblMode
            // 
            this.lblMode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblMode.AutoSize = true;
            this.lblMode.Location = new System.Drawing.Point(897, 146);
            this.lblMode.Name = "lblMode";
            this.lblMode.Size = new System.Drawing.Size(35, 13);
            this.lblMode.TabIndex = 46;
            this.lblMode.Text = "label1";
            this.lblMode.Click += new System.EventHandler(this.lblMode_Click);
            // 
            // btnAlgo
            // 
            this.btnAlgo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAlgo.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAlgo.Location = new System.Drawing.Point(452, 138);
            this.btnAlgo.Name = "btnAlgo";
            this.btnAlgo.Size = new System.Drawing.Size(85, 25);
            this.btnAlgo.TabIndex = 40;
            this.btnAlgo.Text = "Algorithm";
            this.btnAlgo.UseVisualStyleBackColor = true;
            this.btnAlgo.Visible = false;
            this.btnAlgo.Click += new System.EventHandler(this.btnAlgo_Click);
            // 
            // lblPitValueHead
            // 
            this.lblPitValueHead.AutoSize = true;
            this.lblPitValueHead.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPitValueHead.Location = new System.Drawing.Point(19, 141);
            this.lblPitValueHead.Name = "lblPitValueHead";
            this.lblPitValueHead.Size = new System.Drawing.Size(105, 13);
            this.lblPitValueHead.TabIndex = 43;
            this.lblPitValueHead.Text = "Pit Value\'s Graph";
            // 
            // Functions
            // 
            this.Functions.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Functions.BackColor = System.Drawing.Color.White;
            this.Functions.Controls.Add(this.cmbPaintType);
            this.Functions.Controls.Add(this.grpStitchingAvging);
            this.Functions.Controls.Add(this.btnDatabase);
            this.Functions.Controls.Add(this.btnConnect);
            this.Functions.Controls.Add(this.btnBrowse);
            this.Functions.Controls.Add(this.btnRun);
            this.Functions.Controls.Add(this.btnSettings);
            this.Functions.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Functions.ForeColor = System.Drawing.SystemColors.Desktop;
            this.Functions.Location = new System.Drawing.Point(655, 5);
            this.Functions.Name = "Functions";
            this.Functions.Size = new System.Drawing.Size(313, 132);
            this.Functions.TabIndex = 42;
            this.Functions.TabStop = false;
            this.Functions.Text = "Functions";
            this.toolTip1.SetToolTip(this.Functions, "Click on individual buttons to run the functions");
            this.Functions.Enter += new System.EventHandler(this.Functions_Enter);
            // 
            // cmbPaintType
            // 
            this.cmbPaintType.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbPaintType.FormattingEnabled = true;
            this.cmbPaintType.Location = new System.Drawing.Point(203, 101);
            this.cmbPaintType.Name = "cmbPaintType";
            this.cmbPaintType.Size = new System.Drawing.Size(107, 23);
            this.cmbPaintType.TabIndex = 43;
            this.cmbPaintType.SelectedIndexChanged += new System.EventHandler(this.cmbPaintType_SelectedIndexChanged);
            // 
            // grpStitchingAvging
            // 
            this.grpStitchingAvging.BackColor = System.Drawing.Color.White;
            this.grpStitchingAvging.Controls.Add(this.label3);
            this.grpStitchingAvging.Controls.Add(this.nmNoScans);
            this.grpStitchingAvging.Controls.Add(this.rdAverageBtn);
            this.grpStitchingAvging.Controls.Add(this.rdStitchBtn);
            this.grpStitchingAvging.Controls.Add(this.rdNormalBtn);
            this.grpStitchingAvging.Enabled = false;
            this.grpStitchingAvging.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpStitchingAvging.Location = new System.Drawing.Point(6, 50);
            this.grpStitchingAvging.Name = "grpStitchingAvging";
            this.grpStitchingAvging.Size = new System.Drawing.Size(194, 77);
            this.grpStitchingAvging.TabIndex = 42;
            this.grpStitchingAvging.TabStop = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(124, 30);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(69, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "No. of Scans";
            // 
            // nmNoScans
            // 
            this.nmNoScans.Enabled = false;
            this.nmNoScans.Location = new System.Drawing.Point(124, 50);
            this.nmNoScans.Maximum = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.nmNoScans.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.nmNoScans.Name = "nmNoScans";
            this.nmNoScans.Size = new System.Drawing.Size(63, 20);
            this.nmNoScans.TabIndex = 3;
            this.nmNoScans.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            // 
            // rdAverageBtn
            // 
            this.rdAverageBtn.AutoSize = true;
            this.rdAverageBtn.Location = new System.Drawing.Point(5, 55);
            this.rdAverageBtn.Name = "rdAverageBtn";
            this.rdAverageBtn.Size = new System.Drawing.Size(73, 17);
            this.rdAverageBtn.TabIndex = 2;
            this.rdAverageBtn.Text = "Averaging";
            this.rdAverageBtn.UseVisualStyleBackColor = true;
            // 
            // rdStitchBtn
            // 
            this.rdStitchBtn.AutoSize = true;
            this.rdStitchBtn.Location = new System.Drawing.Point(5, 34);
            this.rdStitchBtn.Name = "rdStitchBtn";
            this.rdStitchBtn.Size = new System.Drawing.Size(66, 17);
            this.rdStitchBtn.TabIndex = 1;
            this.rdStitchBtn.Text = "Stitching";
            this.rdStitchBtn.UseVisualStyleBackColor = true;
            // 
            // rdNormalBtn
            // 
            this.rdNormalBtn.AutoSize = true;
            this.rdNormalBtn.Checked = true;
            this.rdNormalBtn.Location = new System.Drawing.Point(5, 14);
            this.rdNormalBtn.Name = "rdNormalBtn";
            this.rdNormalBtn.Size = new System.Drawing.Size(58, 17);
            this.rdNormalBtn.TabIndex = 0;
            this.rdNormalBtn.TabStop = true;
            this.rdNormalBtn.Text = "Normal";
            this.rdNormalBtn.UseVisualStyleBackColor = true;
            // 
            // btnDatabase
            // 
            this.btnDatabase.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDatabase.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDatabase.Location = new System.Drawing.Point(225, 45);
            this.btnDatabase.Name = "btnDatabase";
            this.btnDatabase.Size = new System.Drawing.Size(82, 24);
            this.btnDatabase.TabIndex = 41;
            this.btnDatabase.Text = "Database";
            this.toolTip1.SetToolTip(this.btnDatabase, "click on \'Browse\' button and select CSV file");
            this.btnDatabase.UseVisualStyleBackColor = true;
            this.btnDatabase.Click += new System.EventHandler(this.btnDatabase_Click);
            // 
            // btnConnect
            // 
            this.btnConnect.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnConnect.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConnect.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnConnect.Location = new System.Drawing.Point(9, 21);
            this.btnConnect.Name = "btnConnect";
            this.btnConnect.Size = new System.Drawing.Size(95, 30);
            this.btnConnect.TabIndex = 11;
            this.btnConnect.Text = "Connect";
            this.btnConnect.UseVisualStyleBackColor = true;
            this.btnConnect.Click += new System.EventHandler(this.btnConnect_Click_1);
            // 
            // btnBrowse
            // 
            this.btnBrowse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBrowse.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBrowse.Location = new System.Drawing.Point(225, 73);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new System.Drawing.Size(83, 24);
            this.btnBrowse.TabIndex = 10;
            this.btnBrowse.Text = "Browse";
            this.toolTip1.SetToolTip(this.btnBrowse, "click on \'Browse\' button and select CSV file");
            this.btnBrowse.UseVisualStyleBackColor = true;
            // 
            // btnRun
            // 
            this.btnRun.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRun.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRun.Location = new System.Drawing.Point(112, 20);
            this.btnRun.Name = "btnRun";
            this.btnRun.Size = new System.Drawing.Size(97, 30);
            this.btnRun.TabIndex = 8;
            this.btnRun.Text = "Measure";
            this.btnRun.UseVisualStyleBackColor = true;
            // 
            // btnSettings
            // 
            this.btnSettings.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSettings.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSettings.Location = new System.Drawing.Point(225, 16);
            this.btnSettings.Name = "btnSettings";
            this.btnSettings.Size = new System.Drawing.Size(82, 25);
            this.btnSettings.TabIndex = 9;
            this.btnSettings.Text = "Settings";
            this.btnSettings.UseVisualStyleBackColor = true;
            // 
            // lblSegmentScanStatus
            // 
            this.lblSegmentScanStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblSegmentScanStatus.AutoSize = true;
            this.lblSegmentScanStatus.Location = new System.Drawing.Point(918, 8);
            this.lblSegmentScanStatus.Name = "lblSegmentScanStatus";
            this.lblSegmentScanStatus.Size = new System.Drawing.Size(0, 13);
            this.lblSegmentScanStatus.TabIndex = 16;
            // 
            // zgcPits
            // 
            this.zgcPits.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.zgcPits.Location = new System.Drawing.Point(20, 166);
            this.zgcPits.Name = "zgcPits";
            this.zgcPits.ScrollGrace = 0D;
            this.zgcPits.ScrollMaxX = 0D;
            this.zgcPits.ScrollMaxY = 0D;
            this.zgcPits.ScrollMaxY2 = 0D;
            this.zgcPits.ScrollMinX = 0D;
            this.zgcPits.ScrollMinY = 0D;
            this.zgcPits.ScrollMinY2 = 0D;
            this.zgcPits.Size = new System.Drawing.Size(960, 165);
            this.zgcPits.TabIndex = 12;
            // 
            // grpBoxOutput
            // 
            this.grpBoxOutput.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.grpBoxOutput.BackColor = System.Drawing.Color.White;
            this.grpBoxOutput.Controls.Add(this.lblRightStats);
            this.grpBoxOutput.Controls.Add(this.dispRightStats);
            this.grpBoxOutput.Controls.Add(this.lblLeftStats);
            this.grpBoxOutput.Controls.Add(this.dispLeftStats);
            this.grpBoxOutput.Controls.Add(this.lblMiddleStats);
            this.grpBoxOutput.Controls.Add(this.dispMiddleStats);
            this.grpBoxOutput.Location = new System.Drawing.Point(217, 2);
            this.grpBoxOutput.Name = "grpBoxOutput";
            this.grpBoxOutput.Size = new System.Drawing.Size(427, 135);
            this.grpBoxOutput.TabIndex = 44;
            this.grpBoxOutput.TabStop = false;
            // 
            // lblRightStats
            // 
            this.lblRightStats.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblRightStats.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRightStats.Location = new System.Drawing.Point(298, 12);
            this.lblRightStats.Name = "lblRightStats";
            this.lblRightStats.Size = new System.Drawing.Size(122, 27);
            this.lblRightStats.TabIndex = 20;
            this.lblRightStats.Text = "Right";
            this.lblRightStats.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblLeftStats
            // 
            this.lblLeftStats.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLeftStats.Location = new System.Drawing.Point(10, 13);
            this.lblLeftStats.Name = "lblLeftStats";
            this.lblLeftStats.Size = new System.Drawing.Size(122, 23);
            this.lblLeftStats.TabIndex = 18;
            this.lblLeftStats.Text = "Left";
            this.lblLeftStats.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblMiddleStats
            // 
            this.lblMiddleStats.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblMiddleStats.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMiddleStats.Location = new System.Drawing.Point(154, 12);
            this.lblMiddleStats.Name = "lblMiddleStats";
            this.lblMiddleStats.Size = new System.Drawing.Size(118, 23);
            this.lblMiddleStats.TabIndex = 16;
            this.lblMiddleStats.Text = "Middle";
            this.lblMiddleStats.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // btnClear
            // 
            this.btnClear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClear.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClear.Location = new System.Drawing.Point(553, 138);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(63, 25);
            this.btnClear.TabIndex = 10;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Visible = false;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click_1);
            // 
            // grpBoxPitValues
            // 
            this.grpBoxPitValues.BackColor = System.Drawing.Color.White;
            this.grpBoxPitValues.Controls.Add(this.btnCopyPit);
            this.grpBoxPitValues.Controls.Add(this.txtPits);
            this.grpBoxPitValues.Controls.Add(this.lblTotalPit);
            this.grpBoxPitValues.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpBoxPitValues.ForeColor = System.Drawing.SystemColors.Desktop;
            this.grpBoxPitValues.Location = new System.Drawing.Point(22, 3);
            this.grpBoxPitValues.Name = "grpBoxPitValues";
            this.grpBoxPitValues.Size = new System.Drawing.Size(189, 132);
            this.grpBoxPitValues.TabIndex = 45;
            this.grpBoxPitValues.TabStop = false;
            this.grpBoxPitValues.Text = "Pit Values";
            this.grpBoxPitValues.Enter += new System.EventHandler(this.groupBox2_Enter);
            // 
            // btnCopyPit
            // 
            this.btnCopyPit.Enabled = false;
            this.btnCopyPit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCopyPit.Location = new System.Drawing.Point(19, 105);
            this.btnCopyPit.Name = "btnCopyPit";
            this.btnCopyPit.Size = new System.Drawing.Size(67, 25);
            this.btnCopyPit.TabIndex = 21;
            this.btnCopyPit.Text = "Copy";
            this.btnCopyPit.UseVisualStyleBackColor = true;
            this.btnCopyPit.Click += new System.EventHandler(this.btnCopyPit_Click_1);
            // 
            // txtPits
            // 
            this.txtPits.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPits.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPits.Location = new System.Drawing.Point(17, 41);
            this.txtPits.Multiline = true;
            this.txtPits.Name = "txtPits";
            this.txtPits.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtPits.Size = new System.Drawing.Size(139, 63);
            this.txtPits.TabIndex = 40;
            // 
            // lblTotalPit
            // 
            this.lblTotalPit.AutoSize = true;
            this.lblTotalPit.Location = new System.Drawing.Point(12, 20);
            this.lblTotalPit.Name = "lblTotalPit";
            this.lblTotalPit.Size = new System.Drawing.Size(144, 17);
            this.lblTotalPit.TabIndex = 39;
            this.lblTotalPit.Text = "Total No. of Pits=0";
            // 
            // lblHeading
            // 
            lblHeading.Anchor = System.Windows.Forms.AnchorStyles.Top;
            lblHeading.AutoSize = true;
            lblHeading.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            lblHeading.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(83)))), ((int)(((byte)(84)))));
            lblHeading.Location = new System.Drawing.Point(209, 12);
            lblHeading.Name = "lblHeading";
            lblHeading.Size = new System.Drawing.Size(701, 25);
            lblHeading.TabIndex = 11;
            lblHeading.Text = "Wolf Innovation Phaeton ™ Paint Thickness Measurement System";
            // 
            // picLogo
            // 
            picLogo.Image = global::DFM.Properties.Resources.logo_notext;
            picLogo.InitialImage = null;
            picLogo.Location = new System.Drawing.Point(20, 6);
            picLogo.Name = "picLogo";
            picLogo.Size = new System.Drawing.Size(61, 60);
            picLogo.TabIndex = 12;
            picLogo.TabStop = false;
            // 
            // pictureBox1
            // 
            pictureBox1.Image = global::DFM.Properties.Resources.DJH_Logo_new;
            pictureBox1.InitialImage = null;
            pictureBox1.Location = new System.Drawing.Point(87, 6);
            pictureBox1.Name = "pictureBox1";
            pictureBox1.Size = new System.Drawing.Size(111, 64);
            pictureBox1.TabIndex = 50;
            pictureBox1.TabStop = false;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // lagTimeTimer
            // 
            this.lagTimeTimer.Tick += new System.EventHandler(this.lagTimeTimer_Tick);
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(204, 49);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(54, 17);
            this.label4.TabIndex = 14;
            this.label4.Text = "Status";
            // 
            // rtxtboxStatus
            // 
            this.rtxtboxStatus.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.rtxtboxStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtxtboxStatus.Location = new System.Drawing.Point(261, 45);
            this.rtxtboxStatus.Name = "rtxtboxStatus";
            this.rtxtboxStatus.Size = new System.Drawing.Size(250, 29);
            this.rtxtboxStatus.TabIndex = 15;
            this.rtxtboxStatus.Text = global::DFM.Properties.Resources.DBPassword;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.LightCoral;
            this.label1.Location = new System.Drawing.Point(919, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 13);
            this.label1.TabIndex = 47;
            this.label1.Text = "V: 160816.1";
            // 
            // tmrFade
            // 
            this.tmrFade.Interval = 20D;
            this.tmrFade.SynchronizingObject = this;
            this.tmrFade.Elapsed += new System.Timers.ElapsedEventHandler(this.tmrFade_Elapsed);
            // 
            // rtxtCommnt
            // 
            this.rtxtCommnt.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.rtxtCommnt.Location = new System.Drawing.Point(590, 45);
            this.rtxtCommnt.Name = "rtxtCommnt";
            this.rtxtCommnt.Size = new System.Drawing.Size(237, 29);
            this.rtxtCommnt.TabIndex = 48;
            this.rtxtCommnt.Text = global::DFM.Properties.Resources.DBPassword;
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(513, 50);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 17);
            this.label2.TabIndex = 49;
            this.label2.Text = "Comment";
            // 
            // btnSave
            // 
            this.btnSave.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnSave.Enabled = false;
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Location = new System.Drawing.Point(838, 45);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(52, 25);
            this.btnSave.TabIndex = 41;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // openFileDialog2
            // 
            this.openFileDialog2.FileName = "openFileDialog2";
            // 
            // dispRightStats
            // 
            this.dispRightStats.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.dispRightStats.Caption = global::DFM.Properties.Resources.DBPassword;
            this.dispRightStats.CaptionFontSize = 12F;
            this.dispRightStats.FontColour = System.Drawing.Color.Empty;
            this.dispRightStats.Location = new System.Drawing.Point(296, 42);
            this.dispRightStats.Name = "dispRightStats";
            this.dispRightStats.Size = new System.Drawing.Size(122, 75);
            this.dispRightStats.TabIndex = 19;
            this.dispRightStats.TextFontSize = 20F;
            // 
            // dispLeftStats
            // 
            this.dispLeftStats.Caption = global::DFM.Properties.Resources.DBPassword;
            this.dispLeftStats.CaptionFontSize = 12F;
            this.dispLeftStats.FontColour = System.Drawing.Color.Empty;
            this.dispLeftStats.Location = new System.Drawing.Point(10, 42);
            this.dispLeftStats.Name = "dispLeftStats";
            this.dispLeftStats.Size = new System.Drawing.Size(122, 75);
            this.dispLeftStats.TabIndex = 17;
            this.dispLeftStats.TextFontSize = 20F;
            // 
            // dispMiddleStats
            // 
            this.dispMiddleStats.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dispMiddleStats.Caption = global::DFM.Properties.Resources.DBPassword;
            this.dispMiddleStats.CaptionFontSize = 12F;
            this.dispMiddleStats.FontColour = System.Drawing.Color.Empty;
            this.dispMiddleStats.Location = new System.Drawing.Point(150, 42);
            this.dispMiddleStats.Name = "dispMiddleStats";
            this.dispMiddleStats.Size = new System.Drawing.Size(122, 75);
            this.dispMiddleStats.TabIndex = 15;
            this.dispMiddleStats.TextFontSize = 20F;
            // 
            // showPits_value
            // 
            this.showPits_value.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.showPits_value.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.showPits_value.Location = new System.Drawing.Point(896, 45);
            this.showPits_value.Name = "showPits_value";
            this.showPits_value.Size = new System.Drawing.Size(82, 25);
            this.showPits_value.TabIndex = 44;
            this.showPits_value.Text = "Pits Value";
            this.showPits_value.UseVisualStyleBackColor = true;
            this.showPits_value.Click += new System.EventHandler(this.showPits_value_Click);
            // 
            // DFM_Form_simple
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(992, 595);
            this.Controls.Add(this.showPits_value);
            this.Controls.Add(pictureBox1);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.rtxtCommnt);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.rtxtboxStatus);
            this.Controls.Add(this.label4);
            this.Controls.Add(picLogo);
            this.Controls.Add(lblHeading);
            this.Controls.Add(outerContainer);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(764, 555);
            this.Name = "DFM_Form_simple";
            this.Load += new System.EventHandler(this.DFM_Form_simple_Load);
            outerContainer.Panel1.ResumeLayout(false);
            outerContainer.Panel1.PerformLayout();
            outerContainer.Panel2.ResumeLayout(false);
            outerContainer.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(outerContainer)).EndInit();
            outerContainer.ResumeLayout(false);
            this.Functions.ResumeLayout(false);
            this.grpStitchingAvging.ResumeLayout(false);
            this.grpStitchingAvging.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nmNoScans)).EndInit();
            this.grpBoxOutput.ResumeLayout(false);
            this.grpBoxPitValues.ResumeLayout(false);
            this.grpBoxPitValues.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(picLogo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tmrFade)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button btnConnect;
        private System.Windows.Forms.Button btnSettings;
        private ZedGraph.ZedGraphControl zgcScanProfile;
        private ZedGraph.ZedGraphControl zgcPits;
        private System.Windows.Forms.Button btnBrowse;
        private System.Windows.Forms.Label lblSegmentScanStatus;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Label lblTotalPit;
        private System.Windows.Forms.Button btnAlgo;
        private System.Windows.Forms.Button btnDatabase;
        private System.Windows.Forms.GroupBox Functions;
        private System.Windows.Forms.Button btnRun;
        private System.Windows.Forms.Label lblPitValueHead;
        private System.Windows.Forms.GroupBox grpBoxPitValues;
        private System.Windows.Forms.GroupBox grpBoxOutput;
        public System.Windows.Forms.TextBox txtPits;
        private System.Windows.Forms.Timer lagTimeTimer;
        private System.Windows.Forms.GroupBox grpStitchingAvging;
        private System.Windows.Forms.RadioButton rdAverageBtn;
        private System.Windows.Forms.RadioButton rdStitchBtn;
        private System.Windows.Forms.RadioButton rdNormalBtn;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown nmNoScans;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblRightStats;
        private CaptionedDisplay dispRightStats;
        private System.Windows.Forms.Label lblLeftStats;
        private CaptionedDisplay dispLeftStats;
        private System.Windows.Forms.Label lblMiddleStats;
        private CaptionedDisplay dispMiddleStats;
        private System.Windows.Forms.RichTextBox rtxtboxStatus;
        private System.Windows.Forms.Button btnCopyPit;
        private System.Windows.Forms.Label lblError;
        private System.Windows.Forms.ComboBox cmbPaintType;
        private System.Windows.Forms.Label lblMode;
        private System.Windows.Forms.Label label1;
        protected internal System.Timers.Timer tmrFade;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RichTextBox rtxtCommnt;
        private System.Windows.Forms.Label lblTotalSampleLen;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.OpenFileDialog openFileDialog2;
        private System.Windows.Forms.Button showPits_value;



    }
}