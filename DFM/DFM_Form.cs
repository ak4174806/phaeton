﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using NLog;
using DFM.Settings;
using System.Threading;
using SensorControl;
using DFM.Scans;
using System.IO;
using Analysis;
using ZedGraph;
using PaintAnalysis.Common;
using DFM.Results;
using DFM.Settings.Bindable;
using System.Timers;

namespace DFM
{
    public partial class DFM_Form : Form
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        
        #region Data Members

        // auxiliary forms
        DFM_Settings_Form settingsForm;
        DFMResultsDisplayForm databaseForm;

        // workers
        BackgroundWorker connectWorker;
        BackgroundWorker scanWorker;
        BackgroundWorker scanWorker2;

        // db
        DFMResults dfmResults;

        GalilMonitorForm galilMonitor;

        #endregion

        public DFM_Form()
        {
            InitializeComponent();

            try
            {
                // line for readability
                logger.Trace("************************************************************************************************************************");

                // dfm results
                dfmResults = new DFMResults(new PaintAnalysis.Common.Results.ResultsDB(
                    Path.Combine(Directory.GetCurrentDirectory(), DFM.Properties.Resources.DBName),
                    DFM.Properties.Resources.DBPassword,
                    DFM.Properties.Resources.DBVersion,
                    Path.Combine(Directory.GetCurrentDirectory(), "sql")));

                // create helper stuff
                settingsForm = new DFM_Settings_Form();
                settingsForm.OpeningForm = this;

                databaseForm = new DFMResultsDisplayForm(dfmResults);
                                
                galilMonitor = new GalilMonitorForm(settingsForm.Motor, settingsForm.IsDebugging);

                RunningState = RunningStates.Stopped;
                EdgeState = EdgeStates.NotFound;
                LaserInited = false;
                IsQuickScan = false;
                CurrentUnits = DataUnits.Metric;

                settingsForm.Motor.FinishedMove += new Motor.MotorTaskHandler(Motor_FinishedMove);
                settingsForm.Motor.FinishedSetup += new Motor.MotorTaskHandler(Motor_FinishedSetup);
                settingsForm.Motor.FinishedSetupWithoutEdgeScan += new Motor.MotorTaskHandler(Motor_FinishedSetupWithoutEdgeScan);

                connectWorker = new BackgroundWorker();
                connectWorker.WorkerReportsProgress = false;
                connectWorker.WorkerSupportsCancellation = true;
                connectWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(connectWorker_RunWorkerCompleted);
                connectWorker.DoWork += new DoWorkEventHandler(connectWorker_DoWork);

                scanWorker = new BackgroundWorker();
                scanWorker.WorkerReportsProgress = true;
                scanWorker.WorkerSupportsCancellation = true;
                scanWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(scanWorker_RunWorkerCompleted);
                scanWorker.ProgressChanged += new ProgressChangedEventHandler(scanWorker_ProgressChanged);
                scanWorker.DoWork += new DoWorkEventHandler(scanWorker_DoWork);

                scanWorker2 = new BackgroundWorker();
                scanWorker2.WorkerReportsProgress = true;
                scanWorker2.WorkerSupportsCancellation = true;
                scanWorker2.RunWorkerCompleted += new RunWorkerCompletedEventHandler(scanWorker2_RunWorkerCompleted);
                scanWorker2.ProgressChanged += new ProgressChangedEventHandler(scanWorker2_ProgressChanged);
                scanWorker2.DoWork += new DoWorkEventHandler(scanWorker2_DoWork);

                // TL graph
                GraphPane pane = zgcScanDisplay.GraphPane;
                pane.Legend.IsVisible = false;
                // set axis labels
                pane.Title.Text = "Distance";
                pane.XAxis.Title.Text = "Time (s)";
                pane.YAxis.Title.Text = "Distance (microns)";
                zgcScanDisplay.Click += new EventHandler(delegate(object sender, EventArgs e)
                {
                    new GraphDisplay(zgcScanDisplay, "Scan");
                });

                // setup enabling behaviour
                settingsForm.ConnectionStateChanged += new EventHandler<DFM_Settings_Form.ConnectionStateEventArgs>(delegate(object sender, DFM_Settings_Form.ConnectionStateEventArgs e)
                {
                    if (e.ConnectionState == DFM_Settings_Form.ConnectionStates.Connected) Settings.Bindable.ThreadSafeControlUpdates.UpdateText<string>(btnConnect, "Disconnect");
                    else Settings.Bindable.ThreadSafeControlUpdates.UpdateText<string>(btnConnect, "Connect");

                    bool isConnected = e.ConnectionState == DFM_Settings_Form.ConnectionStates.Connected;
                    bool motorEnabled = settingsForm.Settings.Fetch<bool>("connection/motor/enabled");
                    bool isRunning = (RunningState != RunningStates.Stopped);
                    Settings.Bindable.ThreadSafeControlUpdates.UpdateEnabled(btnStop, isRunning && isConnected);
                    Settings.Bindable.ThreadSafeControlUpdates.UpdateEnabled(btnRunWithEdgeScan, !isRunning && motorEnabled && isConnected);
                    Settings.Bindable.ThreadSafeControlUpdates.UpdateEnabled(btnQuickScan, !isRunning && motorEnabled && isConnected);
                    Settings.Bindable.ThreadSafeControlUpdates.UpdateEnabled(btnRunWithoutEdgeScan, !isRunning && isConnected);
                });

                DFMStateChanged += new EventHandler<DFMStateChangedEventArgs>(delegate(object sender, DFMStateChangedEventArgs e)
                {
                    bool motorEnabled = settingsForm.Settings.Fetch<bool>("connection/motor/enabled");
                    bool isRunning = (e.RunningState != RunningStates.Stopped);
                    bool isConnected = settingsForm.ConnectionState == DFM_Settings_Form.ConnectionStates.Connected;       
                    bool allEmpty = dfmScanDisplayControlLeft.IsEmpty && dfmScanDisplayControlMiddle.IsEmpty && dfmScanDisplayControlRight.IsEmpty;

                    Settings.Bindable.ThreadSafeControlUpdates.UpdateEnabled(btnConnect, !isRunning);
                    Settings.Bindable.ThreadSafeControlUpdates.UpdateEnabled(btnClearEdges, !isRunning && (e.EdgeState == EdgeStates.Found));
                    Settings.Bindable.ThreadSafeControlUpdates.UpdateEnabled(btnStop, isRunning && isConnected);
                    Settings.Bindable.ThreadSafeControlUpdates.UpdateEnabled(btnRunWithEdgeScan, !isRunning && motorEnabled && isConnected);
                    Settings.Bindable.ThreadSafeControlUpdates.UpdateEnabled(btnQuickScan, !isRunning && motorEnabled && isConnected);
                    Settings.Bindable.ThreadSafeControlUpdates.UpdateEnabled(btnRunWithoutEdgeScan, !isRunning && isConnected);
                    Settings.Bindable.ThreadSafeControlUpdates.UpdateEnabled(btnClearReadings, !isRunning && !allEmpty);
                    Settings.Bindable.ThreadSafeControlUpdates.UpdateEnabled(chkRepeatReadings, !isRunning);
                    
                    settingsForm.IsEnabled = !isRunning;
                    // enable target thickness box even when running
                    if (isRunning)
                    {
                        ((BindableScaledSetting)settingsForm.Settings.FetchSetting("data/boundaries/target")).IsEnabled = true;
                    }
                });
                ((SettingValue<bool>)settingsForm.Settings.FetchSetting("connection/motor/enabled")).ValueChanged += new EventHandler<ValueChangedEventArgs<bool>>(delegate(object sender, ValueChangedEventArgs<bool> e)
                {
                    Settings.Bindable.ThreadSafeControlUpdates.UpdateEnabled(btnRunWithEdgeScan, (RunningState == RunningStates.Stopped) && e.Value && settingsForm.ConnectionState == DFM_Settings_Form.ConnectionStates.Connected);
                    Settings.Bindable.ThreadSafeControlUpdates.UpdateEnabled(btnQuickScan, (RunningState == RunningStates.Stopped) && e.Value && settingsForm.ConnectionState == DFM_Settings_Form.ConnectionStates.Connected);
                    Settings.Bindable.ThreadSafeControlUpdates.UpdateEnabled(btnRunWithoutEdgeScan, (RunningState == RunningStates.Stopped) && settingsForm.ConnectionState == DFM_Settings_Form.ConnectionStates.Connected);
                });
                OnDFMStateChanged(new DFMStateChangedEventArgs(RunningState, EdgeState, PaintingState));

                // handle when clear readings button is displayed
                dfmScanDisplayControlLeft.IsEmptyChanged += new EventHandler<DFMScanDisplayControl.IsEmptyEventArgs>(delegate(object sender, DFMScanDisplayControl.IsEmptyEventArgs e)
                {
                    bool isRunning = (RunningState != RunningStates.Stopped);
                    Settings.Bindable.ThreadSafeControlUpdates.UpdateEnabled(btnClearReadings, !isRunning && !e.IsEmpty);                    
                });
                dfmScanDisplayControlMiddle.IsEmptyChanged += new EventHandler<DFMScanDisplayControl.IsEmptyEventArgs>(delegate(object sender, DFMScanDisplayControl.IsEmptyEventArgs e)
                {
                    bool isRunning = (RunningState != RunningStates.Stopped);
                    Settings.Bindable.ThreadSafeControlUpdates.UpdateEnabled(btnClearReadings, !isRunning && !e.IsEmpty);
                });
                dfmScanDisplayControlRight.IsEmptyChanged += new EventHandler<DFMScanDisplayControl.IsEmptyEventArgs>(delegate(object sender, DFMScanDisplayControl.IsEmptyEventArgs e)
                {
                    bool isRunning = (RunningState != RunningStates.Stopped);
                    Settings.Bindable.ThreadSafeControlUpdates.UpdateEnabled(btnClearReadings, !isRunning && !e.IsEmpty);
                });

                btnConnect.Click += new EventHandler(btnConnect_Click);
                btnSettings.Click += new EventHandler(btnSettings_Click);
                btnStop.Click += new EventHandler(btnStop_Click);
                btnRunWithEdgeScan.Click += new EventHandler(btnRunWithEdgeScan_Click);
                btnRunWithoutEdgeScan.Click += new EventHandler(btnRunWithoutEdgeScan_Click);
                btnClearEdges.Click += new EventHandler(btnClearEdges_Click);
                btnClearReadings.Click += new EventHandler(btnClearReadings_Click);
                btnPastResults.Click += new EventHandler(btnPastResults_Click);

                ((SettingValue<float>)settingsForm.Settings.FetchSetting("data/boundaries/target")).ValueChanged += new EventHandler<ValueChangedEventArgs<float>>(Boundary_ValueChanged);
                // allow target on main screen to have enabled controlled separately
                ((BindableScaledSetting)settingsForm.Settings.FetchSetting("data/boundaries/target")).InheritEnabled = false;

                ((SettingValue<float>)settingsForm.Settings.FetchSetting("data/boundaries/range")).ValueChanged += new EventHandler<ValueChangedEventArgs<float>>(Boundary_ValueChanged);
                Boundary_ValueChanged(null, null);

                // change scan averaging number
                ((SettingValue<int>)settingsForm.Settings.FetchSetting("data/scan averaging number")).ValueChanged += new EventHandler<ValueChangedEventArgs<int>>(delegate(object sender, ValueChangedEventArgs<int> e)
                    {
                        dfmScanDisplayControlLeft.ScanAveragingNumber = dfmScanDisplayControlMiddle.ScanAveragingNumber = dfmScanDisplayControlRight.ScanAveragingNumber = e.Value;
                    });
                dfmScanDisplayControlLeft.ScanAveragingNumber = dfmScanDisplayControlMiddle.ScanAveragingNumber = dfmScanDisplayControlRight.ScanAveragingNumber = settingsForm.Settings.Fetch<int>("data/scan averaging number");

                // change error result filter threshold
                SettingsGroup errorFiltering = (SettingsGroup)settingsForm.Settings.FetchSetting("data/boundaries/filtering");
                ((SettingValue<bool>)settingsForm.Settings.FetchSetting("data/boundaries/filtering/enabled")).ValueChanged += new EventHandler<ValueChangedEventArgs<bool>>(delegate(object sender, ValueChangedEventArgs<bool> e)
                {
                    dfmScanDisplayControlLeft.BoundaryFiltering = dfmScanDisplayControlMiddle.BoundaryFiltering = dfmScanDisplayControlRight.BoundaryFiltering = e.Value;
                });
                dfmScanDisplayControlLeft.BoundaryFiltering = dfmScanDisplayControlMiddle.BoundaryFiltering = dfmScanDisplayControlRight.BoundaryFiltering = errorFiltering.Fetch<bool>("enabled");

                ((SettingValue<float>)settingsForm.Settings.FetchSetting("data/boundaries/filtering/threshold")).ValueChanged += new EventHandler<ValueChangedEventArgs<float>>(delegate(object sender, ValueChangedEventArgs<float> e)
                {
                    // convert to decimal percentage
                    dfmScanDisplayControlLeft.BoundaryFilteringThreshold = dfmScanDisplayControlMiddle.BoundaryFilteringThreshold = dfmScanDisplayControlRight.BoundaryFilteringThreshold = Convert.ToSingle(e.Value / 100.0);
                });
                dfmScanDisplayControlLeft.BoundaryFilteringThreshold = dfmScanDisplayControlMiddle.BoundaryFilteringThreshold = dfmScanDisplayControlRight.BoundaryFilteringThreshold = Convert.ToSingle(errorFiltering.Fetch<float>("threshold") / 100.0);

                // re-init laser when settings change
                ((SettingsGroup)settingsForm.Settings.FetchSetting("scan/laser")).ModifiedChanged += new EventHandler(delegate(object sender, EventArgs e)
                    {
                        LaserInited = false;
                    });

                // handle motor position labels changing
                ((SettingValue<string>)settingsForm.Settings.FetchSetting("scan/motor/labels/left")).ValueChanged += new EventHandler<ValueChangedEventArgs<string>>(delegate(object sender, ValueChangedEventArgs<string> e)
                {
                    dfmScanDisplayControlLeft.Heading = e.Value;
                });
                ((SettingValue<string>)settingsForm.Settings.FetchSetting("scan/motor/labels/middle")).ValueChanged += new EventHandler<ValueChangedEventArgs<string>>(delegate(object sender, ValueChangedEventArgs<string> e)
                {
                    dfmScanDisplayControlMiddle.Heading = e.Value;
                });
                ((SettingValue<string>)settingsForm.Settings.FetchSetting("scan/motor/labels/right")).ValueChanged += new EventHandler<ValueChangedEventArgs<string>>(delegate(object sender, ValueChangedEventArgs<string> e)
                {
                    dfmScanDisplayControlRight.Heading = e.Value;
                });
                dfmScanDisplayControlLeft.Heading = settingsForm.Settings.Fetch<string>("scan/motor/labels/left");
                dfmScanDisplayControlMiddle.Heading = settingsForm.Settings.Fetch<string>("scan/motor/labels/middle");
                dfmScanDisplayControlRight.Heading = settingsForm.Settings.Fetch<string>("scan/motor/labels/right");
                
                // handle scan repeat changing
                ((BindableBoolSetting)settingsForm.Settings.FetchSetting("scan/timing/repeat scan/enabled")).Bind(chkRepeatReadings);
                chkRepeatReadings.CheckedChanged += new EventHandler(delegate(object sender, EventArgs e)
                {
                    if (settingsForm.Visible == false) settingsForm.Save();
                });
                ((SettingValue<int>)settingsForm.Settings.FetchSetting("scan/timing/repeat scan/interval")).ValueChanged += new EventHandler<ValueChangedEventArgs<int>>(delegate(object sender, ValueChangedEventArgs<int> e)
                {
                    chkRepeatReadings.Text = String.Format("Repeat every {0} seconds", e.Value);
                });
                chkRepeatReadings.Text = String.Format("Repeat every {0} seconds", settingsForm.Settings.Fetch<int>("scan/timing/repeat scan/interval"));

                ((SettingValue<bool>)settingsForm.Settings.FetchSetting("misc/enable quick scan")).ValueChanged += new EventHandler<ValueChangedEventArgs<bool>>(delegate(object sender, ValueChangedEventArgs<bool> e)
                {
                    // whether quick scans enabled
                    btnQuickScan.Visible = e.Value;
                    numStripWidth.Enabled = e.Value;
                    numStripCentre.Enabled = e.Value;
                });                

                // quick scan
                btnQuickScan.Click += new EventHandler(btnQuickScan_Click);
                //numStripCentre.Value = Properties.Settings.Default.StripCentre;
                numStripWidth.Value = Properties.Settings.Default.StripWidth;

                // handle units changing
                SettingValue<DataUnits> tmp = (SettingValue<DataUnits>)settingsForm.Settings.FetchSetting("data/units");
                tmp.ValueChanged += new EventHandler<ValueChangedEventArgs<DataUnits>>(handleUnitsChanged);
                handleUnitsChanged(null, new ValueChangedEventArgs<DataUnits>(tmp.Value, tmp));

                // handle galil form
                btnGalilMonitor.Click += new EventHandler(delegate(object sender, EventArgs e)
                {
                    galilMonitor.Show();
                });

                // handle Painting input
                paintingDelayTimer = new System.Timers.Timer();
                paintingDelayTimer.Elapsed += new ElapsedEventHandler(paintingDelayTimer_Elapsed);

                // link control to setting
                BindableBoolSetting requiredSetting = ((BindableBoolSetting)settingsForm.Settings.FetchSetting("scan/painting input/required"));
                requiredSetting.Bind(chkRequirePainting);
                settingsForm.Motor.PollState.StatusChanged += new Motor.PollMotorStatusChangedHandler(PollState_StatusChanged);
                requiredSetting.ValueChanged += new EventHandler<ValueChangedEventArgs<bool>>(paintingRequiredChanged);
                PaintingState = PaintingStates.NotPainting;
                paintingRequiredChanged(requiredSetting, new ValueChangedEventArgs<bool>(requiredSetting.Value, requiredSetting));
                // save requirePainting checkbox
                chkRequirePainting.CheckedChanged += new EventHandler(delegate(object sender, EventArgs e)
                {
                    if (settingsForm.Visible == false) settingsForm.Save();
                });
                
                // link target thickness to setting
                BindableScaledSetting targetThickness = (BindableScaledSetting)settingsForm.Settings.FetchSetting("data/boundaries/target");
                targetThickness.Bind(numTargetThickness);
                numTargetThickness.ValueChanged += new EventHandler(delegate(object sender, EventArgs e) 
                {
                    if (settingsForm.Visible == false) settingsForm.Save();
                });

                // try connect
                btnConnect.Enabled = false;
                connectWorker.RunWorkerAsync(new ConnectBehaviour(true, true));

                // misc settings
                FormClosing += new FormClosingEventHandler(delegate(object sender, FormClosingEventArgs e)
                {
                    Properties.Settings.Default.RepeatEvery30Seconds = chkRepeatReadings.Checked;
                    Properties.Settings.Default.StripWidth = numStripWidth.Value;
                    Properties.Settings.Default.StripCentre = numStripCentre.Value;
                    Properties.Settings.Default.Save();
                });
            }
            catch (Exception ex)
            {
                logger.ErrorException(ex.Message, ex);
                message("Error in startup: " + ex.Message);
            }
        }

        #region Painting input

        private System.Timers.Timer paintingDelayTimer;

        void paintingRequiredChanged(object sender, ValueChangedEventArgs<bool> e)
        {
            logger.Trace("Called paintingRequiredChanged: " + Convert.ToString(e.Value));
            if (!e.Value)
            {
                logger.Trace("Called paintingRequiredChanged: can run scans");
                currentPaintingInput = false;
                PaintingState = PaintingStates.Painting;
                txtPaintingDisplay.Visible = false;
            }
            else
            {
                logger.Trace("Called paintingRequiredChanged: can't run scans unless painting - checking painting");
                txtPaintingDisplay.Visible = true;
                txtPaintingDisplay.Text = "Not Painting";
                handlePaintingInputChanged(true);
            }
        }

        void PollState_StatusChanged(object sender, Motor.StatusChangedEventArgs e)
        {
            handlePaintingInputChanged();
        }

        private bool currentPaintingInput = false;
        private void handlePaintingInputChanged(bool forceUpdate=false)
        {
            logger.Trace("Called handlePaintingInputChanged: " + Convert.ToString(settingsForm.Settings.Fetch<bool>("scan/painting input/required")));
            if (!settingsForm.Settings.Fetch<bool>("scan/painting input/required")) return;
                        
            // currentPaintingInput should be false if just enabled painting required, starting up, or painting input is false
            bool newPaintingInput = settingsForm.Motor.PollState.Status.Painting;

            logger.Trace(String.Format("Current: {0}. New: {1}", currentPaintingInput, newPaintingInput));
                        
            // happens as status changed is called whenever any status bit changes
            if (currentPaintingInput == newPaintingInput && !forceUpdate) return;
            currentPaintingInput = newPaintingInput;

            if (newPaintingInput)
            {
                logger.Trace("Started painting");
                
                // painting just turned on
                // wait for delay
                int paintingDelay = settingsForm.Settings.Fetch<int>("scan/painting input/delay");
                logger.Trace("Painting delay = " + Convert.ToString(paintingDelay));
                if (paintingDelay > 0)
                {
                    PaintingState = PaintingStates.PaintingCountdown;
                    paintingDelayTimer.AutoReset = true;
                    paintingDelayTimer.Interval = 1000;
                    delayTimerCount = 0;
                    logger.Trace("Start timer next");
                    paintingDelayTimer.Start();
                    logger.Trace("Timer should have started: {0}", paintingDelayTimer.Enabled);
                }
                else
                {
                    PaintingState = PaintingStates.Painting;
                    txtPaintingDisplay.Text = "Painting";
                }
            }
            else
            {
                logger.Trace("Stopping painting");
                // painting just turned off

                // stop timer
                if (paintingDelayTimer.Enabled) paintingDelayTimer.Stop();
                txtPaintingDisplay.Text = "Not Painting";

                PaintingState = PaintingStates.NotPainting;
            }
        }

        private int delayTimerCount = 0;
        void paintingDelayTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            delayTimerCount += 1;
            if (delayTimerCount > settingsForm.Settings.Fetch<int>("scan/painting input/delay"))
            {
                ThreadSafeControlUpdates.UpdateText<string>(txtPaintingDisplay, "Painting");
                PaintingState = PaintingStates.Painting;
            }
            else
            {
                ThreadSafeControlUpdates.UpdateText<string>(txtPaintingDisplay, String.Format("Painting ({0})", delayTimerCount));
            }
        }        

        #endregion

        #region Units

        private DataUnits CurrentUnits
        {
            get;
            set;
        }

        bool firstChange = true;
        void handleUnitsChanged(object sender, ValueChangedEventArgs<DataUnits> e)
        {
            // things to update:
            // TL graph + units, scan loading mechansim

            // TODO HERE
            // change saved units
            DataUnits oldUnits = CurrentUnits;
            CurrentUnits = e.Value;

            // strip edges
            updateLeftEdgeDisplay();
            updateRightEdgeDisplay();

            // change scan displays
            foreach (DFMScanDisplayControl control in new DFMScanDisplayControl[] { dfmScanDisplayControlLeft, dfmScanDisplayControlMiddle, dfmScanDisplayControlRight })
            {
                if (CurrentUnits == DataUnits.Metric)
                {
                    control.ScaleFactor = null;
                    control.GraphUnits = "microns";
                    control.NumberFormat = "{0:0.0}";
                }
                else
                {
                    control.ScaleFactor = UnitConverter.GetDistanceConverter("micron", "mil");
                    control.GraphUnits = "mils";
                    control.NumberFormat = "{0:0.00}";
                }
                control.RefreshDisplay();
            }

            // TL graph
            GraphPane pane = zgcScanDisplay.GraphPane;
            if (CurrentUnits == DataUnits.Metric)
                pane.YAxis.Title.Text = "Distance (microns)";
            else
                pane.YAxis.Title.Text = "Distance (mils)";

            // process any old data on it - know that there is at most 1 scan displayed

            // work out unit names to pass to converter
            string oldUnitName, curUnitName;
            oldUnitName = "micron";
            curUnitName = "micron";
            if (oldUnits == DataUnits.Imperial) oldUnitName = "mil";
            if (CurrentUnits == DataUnits.Imperial) curUnitName = "mil";

            if (pane.CurveList.Count > 0)
            {
                IPointList cur = pane.CurveList[0].Points;
                int n = cur.Count;
                double[] data_x = new double[n];
                double[] data_y = new double[n];
                for (int i = 0; i < n; i++)
                {
                    data_x[i] = cur[i].X;
                    data_y[i] = UnitConverter.ConvertDistance(oldUnitName, curUnitName, cur[i].Y);
                }
                
                pane.CurveList.Clear();
                PointPairList points = new PointPairList(data_x, data_y);
                pane.AddCurve("scan", points, Color.Blue);
                zgcScanDisplay.AxisChange();
                zgcScanDisplay.Invalidate();
            }

            // database of results
            if (CurrentUnits == DataUnits.Metric)
                databaseForm.PitDepthUnit = "micron";
            else
                databaseForm.PitDepthUnit = "mil";

            // quick scan controls
            // fixed centre at 31"
            if (CurrentUnits == DataUnits.Imperial)
            {
                if (CurrentUnits != oldUnits && !firstChange)
                {                    
                    numStripWidth.Value = Convert.ToDecimal(UnitConverter.ConvertDistance("mm", "inch", Convert.ToDouble(numStripWidth.Value)));
                    numStripCentre.Value = Convert.ToDecimal(UnitConverter.ConvertDistance("mm", "inch", Convert.ToDouble(numStripCentre.Value)));
                    savedStripWidth = Convert.ToDecimal(UnitConverter.ConvertDistance("mm", "inch", Convert.ToDouble(savedStripWidth)));
                    
                }
                lblStripCentreUnit.Text = "inch";
                lblStripWidthUnit.Text = "inch";

                // target thickness is bound, gets converted automatically
                lblTargetThicknessUnit.Text = "mil";
                numTargetThickness.Increment = (decimal)0.1;
            }
            else
            {
                if (CurrentUnits != oldUnits && !firstChange)
                {
                    numStripWidth.Value = Convert.ToDecimal(UnitConverter.ConvertDistance("inch", "mm", Convert.ToDouble(numStripWidth.Value)));
                    numStripCentre.Value = Convert.ToDecimal(UnitConverter.ConvertDistance("inch", "mm", Convert.ToDouble(numStripCentre.Value)));
                    savedStripWidth = Convert.ToDecimal(UnitConverter.ConvertDistance("inch", "mm", Convert.ToDouble(savedStripWidth)));
                }
                lblStripCentreUnit.Text = "mm";
                lblStripWidthUnit.Text = "mm";

                // target thickness is bound, gets converted automatically
                lblTargetThicknessUnit.Text = "micron";
                numTargetThickness.Increment = (decimal)1.0;
            }
            firstChange = false;
        }

        #endregion

        #region Misc

        private void message(string message)
        {
            MessageBox.Show(message);
        }

        #endregion

        #region Button State Handling

        public event EventHandler<DFMStateChangedEventArgs> DFMStateChanged;

        private RunningStates runningState;
        public RunningStates RunningState
        {
            get { return runningState; }
            set
            {
                runningState = value;
                OnDFMStateChanged(new DFMStateChangedEventArgs(value, EdgeState, PaintingState));
            }
        }

        private EdgeStates edgeState;
        public EdgeStates EdgeState
        {
            get { return edgeState; }
            set
            {
                edgeState = value;
                if (value == EdgeStates.NotFound) ClearEdges();
                OnDFMStateChanged(new DFMStateChangedEventArgs(RunningState, value, PaintingState));
            }
        }

        private PaintingStates paintingState;
        public PaintingStates PaintingState
        {
            get { return paintingState; }
            set
            {
                paintingState = value;
                OnDFMStateChanged(new DFMStateChangedEventArgs(RunningState, EdgeState, value));
            }
        }

        protected void OnDFMStateChanged(DFMStateChangedEventArgs e)
        {
            if (DFMStateChanged != null)
            {
                DFMStateChanged(this, e);
            }
        }

        #endregion

        #region Events

        void btnPastResults_Click(object sender, EventArgs e)
        {
            databaseForm.Show();
        }
                
        void btnRunWithoutEdgeScan_Click(object sender, EventArgs e)
        {
            runWithoutEdgeScan();
        }

        void btnRunWithEdgeScan_Click(object sender, EventArgs e)
        {
            runWithEdgeScan();
        }

        void btnStop_Click(object sender, EventArgs e)
        {            
            RunningState = RunningStates.Stopping;
            btnStop.Enabled = false; // todo
            try
            {
                bool a1 = killWorker(scanWorker2);
                bool a2 = killWorker(scanWorker);
                bool a3 = killWorker(connectWorker);
                bool a4 = settingsForm.Motor.StopProcesses();
                if (!a1) logger.Error("Could not kill scanworker2 " + ScanArgs);
                if (!a2) logger.Error("Could not kill scanworker1 " + ScanArgs);
                if (!a3) logger.Error("Could not kill connectWorker " + ScanArgs);
                if (!a4) logger.Error("Could not kill motor processes " + ScanArgs);
            }
            catch (Exception ex)
            {
                logger.ErrorException("Error stopping scan: " + ex.Message + ". Details are " + ScanArgs, ex);
            }
            RunningState = RunningStates.Stopped;
        }

        bool killWorker(BackgroundWorker worker)
        {
            worker.CancelAsync();                
            int i = 1;
            while (worker.IsBusy && i <= 5)
            {
                Thread.Sleep(10 * i * i);
                i += 1;
            }
            return !worker.IsBusy;
        }

        void btnSettings_Click(object sender, EventArgs e)
        {
            // check password
            string password = settingsForm.Settings.Fetch<string>("misc/password");

            bool valid = true;
            if (password != "")
            {
                valid = false;
                PasswordPromptForm pass = new PasswordPromptForm();
                pass.ShowDialog();
                if (pass.DialogResult == System.Windows.Forms.DialogResult.OK && pass.Password == password)
                    valid = true;
                else if (pass.DialogResult == System.Windows.Forms.DialogResult.Cancel)
                { }
                else
                    message("Invalid password");
            }
            if (valid)
            {
                settingsForm.Show();
                settingsForm.WindowState = FormWindowState.Normal;
                settingsForm.Focus();
            }
        }

        void btnConnect_Click(object sender, EventArgs e)
        {
            if (!connectWorker.IsBusy)
            {
                btnConnect.Enabled = false;            
                connectWorker.RunWorkerAsync(new ConnectBehaviour(false, false));
            }
        }

        void btnClearEdges_Click(object sender, EventArgs e)
        {
            EdgeState = EdgeStates.NotFound;
            settingsForm.Motor.ClearEdges();
        }

        void btnClearReadings_Click(object sender, EventArgs e)
        {
            clearReadings();
        }
        private void clearReadings()
        {
            dfmScanDisplayControlLeft.ClearData();
            dfmScanDisplayControlMiddle.ClearData();
            dfmScanDisplayControlRight.ClearData();
        }

        void Boundary_ValueChanged(object sender, ValueChangedEventArgs<float> e)
        {
            float target = ((SettingValue<float>)settingsForm.Settings.FetchSetting("data/boundaries/target")).Value;
            float range = ((SettingValue<float>)settingsForm.Settings.FetchSetting("data/boundaries/range")).Value;

            dfmScanDisplayControlLeft.BoundaryLow = dfmScanDisplayControlMiddle.BoundaryLow = dfmScanDisplayControlRight.BoundaryLow = target - range;
            dfmScanDisplayControlLeft.BoundaryHigh = dfmScanDisplayControlMiddle.BoundaryHigh = dfmScanDisplayControlRight.BoundaryHigh = target + range;
        }

        #endregion
        
        #region Connect Worker

        void connectWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            ConnectBehaviour args;
            if (e.Argument != null) args = (ConnectBehaviour)e.Argument;
            else args = new ConnectBehaviour(false, false);

            string error;
            if (settingsForm.ConnectionState == DFM_Settings_Form.ConnectionStates.Connected)
            {
                if (!settingsForm.TryDisconnect(out error) && !args.Silent)
                    throw new ApplicationException(error);
            }
            else
            {
                if (!settingsForm.TryConnect(out error, args.Fast) && !args.Silent) 
                    throw new ApplicationException(error);
            }            
        }

        void connectWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Error != null) message(e.Error.Message);

            btnConnect.Enabled = true;

            /*if (e.Error == null)
            {
                SetEdges(286, 1194);
                btnClearEdges.Enabled = true;
                LoadScan(new LastScanDetails(21, 1, "scan 001.csv", ScanWorkerArgs.Positions.Left));
                dfmScanDisplayControlLeft.ClearData();
                
                DFMScanDisplayControl[] controls = { dfmScanDisplayControlLeft, dfmScanDisplayControlMiddle, dfmScanDisplayControlRight };
                double[][] values = new double[3][];
                values[0] = new double[] { 21.71, 22.75, 22.1, 21.74, 22.53, 22.26 };
                values[1] = new double[] { 21.58, 22.43, 21.95, 21.8, 22.31, 22.55 };
                values[2] = new double[] { 21.67, 22.85, 22.41, 21.69, 22.55, 21.83 };

                for (int i = 0; i < 3; i++)
                {
                    for (int j = 0; j < values[i].Length; j++)
                        controls[i].AddData(new ScanData(values[i][j], 4));
                }
            }*/
        }

        private struct ConnectBehaviour
        {
            private bool silent;
            public bool Silent
            {
                get { return silent; }
                set { silent = value; }
            }

            private bool fast;
            public bool Fast
            {
                get { return fast; }
                set { fast = value; }
            }

            public ConnectBehaviour(bool silent, bool fast)
            {
                this.silent = silent;
                this.fast = fast;
            }
        }

        #endregion

        #region Edge Display

        public void SetEdges(int leftEdge, int rightEdge)
        {
            LeftEdge = leftEdge;
            RightEdge = rightEdge;
        }

        public void ClearEdges()
        {
            LeftEdge = RightEdge = -1;
            settingsForm.Motor.ClearEdges();
        }

        private int leftEdge = -1;
        public int LeftEdge
        {
            get { return leftEdge; }
            set
            {
                leftEdge = value;
                updateLeftEdgeDisplay();
            }
        }

        private int rightEdge = -1;
        public int RightEdge
        {
            get { return rightEdge; }
            set
            {
                rightEdge = value;
                updateRightEdgeDisplay();
            }
        }

        private void updateLeftEdgeDisplay()
        {
            // separated so can call when units change
            // move to 1 dp here
            if (LeftEdge == -1) Settings.Bindable.ThreadSafeControlUpdates.UpdateText<string>(txtLeftEdge, "-");
            else Settings.Bindable.ThreadSafeControlUpdates.UpdateText<string>(txtLeftEdge, processEdge(LeftEdge));
            updateStripEdgeLabel();
        }

        private void updateRightEdgeDisplay()
        {
            if (RightEdge == -1) Settings.Bindable.ThreadSafeControlUpdates.UpdateText<string>(txtRightEdge, "-");
            else Settings.Bindable.ThreadSafeControlUpdates.UpdateText<string>(txtRightEdge, processEdge(RightEdge));
            updateStripEdgeLabel();
        }

        private void updateStripEdgeLabel()
        {
            grpStripEdges.Text = "Strip Edges";
            if (LeftEdge > -1 || RightEdge > -1)
            {
                if (CurrentUnits == DataUnits.Metric) grpStripEdges.Text += " (mm)";
                else grpStripEdges.Text += " (inches)";
            }
        }

        private string processEdge(int input)
        {
            // 1 dp if imperial, 0 dp if metric
            if (CurrentUnits == DataUnits.Imperial)
                return String.Format("{0:0.0}", Math.Round(UnitConverter.ConvertDistance("mm", "inch", input)));
            return String.Format("{0:0}", input);
        }

        #endregion

        #region Scans

        #region SkipEdgeScan

        private decimal savedStripWidth = -1;
        private enum ScanTypes
        {
            QuickScan,
            NormalScan
        }
        private ScanTypes skipEdgeScanType = ScanTypes.QuickScan;

        private void skipEdgeScan(ScanTypes skipEdgeScanType, bool checkPainting=false, int leftEdgeDistance = 75, int rightEdgeDistance = 75, int maxStripWidth = 5000)
        {
            // save type of skip edge scan
            this.skipEdgeScanType = skipEdgeScanType;

            // "setup" Galil
            int stripWidth = Convert.ToInt32(numStripWidth.Value);
            int stripCentre = Convert.ToInt32(numStripCentre.Value);

            if (CurrentUnits == DataUnits.Imperial)
            {
                stripWidth = Convert.ToInt32(UnitConverter.ConvertDistance("inch", "mm", Convert.ToDouble(numStripWidth.Value)));
                stripCentre = Convert.ToInt32(UnitConverter.ConvertDistance("inch", "mm", Convert.ToDouble(numStripCentre.Value)));
            }

            logger.Trace("Skipping edge scan, type is " + skipEdgeScanType.ToString());
            logger.Trace(String.Format("Skip edge scan with stripWidth = {0}, strip centre = {1}", stripWidth, stripCentre));

            if (stripWidth <= leftEdgeDistance + rightEdgeDistance + 5 || stripWidth >= maxStripWidth)
            {
                int minDistance = leftEdgeDistance + rightEdgeDistance + 5;
                double minDistanceInch = UnitConverter.ConvertDistance("mm", "inch", minDistance);
                message(String.Format("Strip width needs to be at least {0:0.0}\" / {1} mm for a scan", minDistanceInch, minDistance));
                return;
            }
            // change the check to see if the left and right scan positions are within range or not?
            // reason: Aleris has 62.75" strip with 31" centre - can't be edge scanned :(            
            if (stripCentre <= leftEdgeDistance || stripCentre <= rightEdgeDistance || stripCentre - stripWidth / 2 + leftEdgeDistance <= 0 || stripCentre + stripWidth / 2 - rightEdgeDistance >= maxStripWidth)
            {
                message("Not enough space on strip for scan of that width");
                return;
            }
            
            if (checkPainting && PaintingState == PaintingStates.NotPainting)
            {
                message("Wait until the strip is painting before a scan");
                RunningState = RunningStates.Stopped;
                return;
            }

            // change form state
            RunningState = RunningStates.EdgeFinding;
            EdgeState = EdgeStates.NotFound;

            // "edge scan"
            int leftEdge = stripCentre - stripWidth / 2;
            int rightEdge = stripCentre + stripWidth / 2;

            logger.Trace(String.Format("Quick scan with left edge = {0}, right edge = {1}", leftEdge, rightEdge));

            settingsForm.Motor.SetupWithoutEdgeScan(leftEdge, rightEdge);
        }

        void Motor_FinishedSetupWithoutEdgeScan(object sender, Motor.MotorTaskEventArgs e)
        {
            logger.Trace("Finished setup without edge scan");
            if (e.Success)
            {
                // copy edges to textboxes
                SetEdges(settingsForm.Motor.LeftEdge, settingsForm.Motor.RightEdge);
                EdgeState = EdgeStates.Found;

                savedStripWidth = numStripWidth.Value;

                // handle if stop button pressed
                if (RunningState == RunningStates.EdgeFinding)
                {
                    if (skipEdgeScanType == ScanTypes.QuickScan)
                        handleAfterQuickScanSetup();
                    else
                        runWithoutEdgeScan();
                }
            }
            else
            {
                logger.Trace("Failed edge scan");
                if (e.Error != null) logger.ErrorException(e.Error.Message, e.Error);
                RunningState = RunningStates.Stopped;
                EdgeState = EdgeStates.NotFound;
                if (e.Error != null) message(e.Error.Message);
                else message("Motor setup worker cancelled");
            }
        }        

        #endregion

        #region Quick Scans

        void btnQuickScan_Click(object sender, EventArgs e)
        {
            if (!(settingsForm.Settings.Fetch<bool>("connection/sensor/enabled") && ((settingsForm.Settings.Fetch<bool>("connection/laser/enabled")) || settingsForm.Settings.Fetch<bool>("connection/new laser/enabled"))&& settingsForm.Settings.Fetch<bool>("connection/motor/enabled")))
            {
                message("Enable all of sensor, laser and motor to take a quick scan");
                RunningState = RunningStates.Stopped;
                return;
            }

            skipEdgeScan(ScanTypes.QuickScan);
        }

        void handleAfterQuickScanSetup()
        {
            // change form state
            logger.Trace("Preparing scan");
            RunningState = RunningStates.Preparing;

            if (EdgeState != EdgeStates.Found)
            {
                logger.Error("Tried to run scan without finding edges first");
                message("Need to run edge scans before running");
                RunningState = RunningStates.Stopped;
                return;
            }

            // set up laser if first time, or the last time was a normal scan
            logger.Trace("Preparing laser");
            if (!LaserInited || !IsQuickScan)
            {
                logger.Trace("Setting up laser for quick scan");
                try
                {
                    // <quickScanPits> pulses at 150 Hz, 1% duty cycle
                    int runTime = Convert.ToInt32(Math.Round(1000 * quickScanPits / 150.0));
                    if (!initLaser2(runTime, 150, 10))
                    {
                        RunningState = RunningStates.Stopped;
                        return;
                    }
                    LaserInited = true;
                }
                catch (Exception ex)
                {
                    message("Could not set laser parameters: " + ex.Message);
                    logger.Error("Unhandled error in quick scan when setting up laser: " + ex.Message);
                    RunningState = RunningStates.Stopped;
                    return;
                }
            }
            else { logger.Trace("Skipping laser"); }

            IsQuickScan = true;

            // set up sensor to take readings
            logger.Trace("Preparing sensor");
            try
            {
                // get frequency
                double frequency = Convert.ToDouble(settingsForm.Sensor.Parameters["frequency"].Get());

                settingsForm.Sensor.TimeIncrement = 1.0 / frequency;
                settingsForm.Sensor.CurrentTime = 0;

                settingsForm.Sensor.EnsureReadingsInitialised();
                settingsForm.Sensor.PortBuffer.Clear();
            }
            catch (Exception ex)
            {
                message("Could not prepare sensor: " + ex.Message);
                logger.Error("Unhandled error in quick scan when setting up sensor: " + ex.Message);
                RunningState = RunningStates.Stopped;
                return;
            }

            if (RunningState != RunningStates.Stopped && RunningState != RunningStates.Stopping)
            {
                // run scans
                ScanArgs = null;
                scanWorker.RunWorkerAsync(true);
            }
            else logger.Trace("Not starting worker at end of quick scan because running state is " + RunningState.ToString());
        }

        #endregion

        #region State Machine

        private void runWithEdgeScan()
        {
            if (settingsForm.Settings.IsModified)
            {
                message("Save any settings changes before running a scan");
                RunningState = RunningStates.Stopped;
                return;
            }

            // validate
            if (!settingsForm.Settings.IsValid)
            {
                message(settingsForm.Settings.ValidationErrors[0].ErrorMessage);
                RunningState = RunningStates.Stopped;
                return;
            }

            if (PaintingState == PaintingStates.NotPainting)
            {
                message("Wait until the strip is painting before a scan");
                RunningState = RunningStates.Stopped;
                return;
            }

            // change form state
            RunningState = RunningStates.EdgeFinding;
            EdgeState = EdgeStates.NotFound;

            logger.Trace("Running with edge scan");

            // set up Galil
            if (settingsForm.Settings.Fetch<bool>("connection/motor/enabled"))
            {
                // calls Motor_FinishedSetup when done
                settingsForm.Motor.Setup();
            }
            else
            {
                // shouldn't be here
                logger.Error("Trying to run with an edge scan when motor is disabled. Skipping edge scan");
                runWithoutEdgeScan();
            }
            
            // clear previous data
            clearReadings();
        }
        
        void Motor_FinishedSetup(object sender, Motor.MotorTaskEventArgs e)
        {
            logger.Trace("Finished edge scan");
            if (e.Success)
            {
                // copy edges to textboxes
                SetEdges(settingsForm.Motor.LeftEdge, settingsForm.Motor.RightEdge);

                EdgeState = EdgeStates.Found;

                // set strip width box to the strip width found
                double stripSize = Math.Max(Math.Min(5000, settingsForm.Motor.RightEdge - settingsForm.Motor.LeftEdge), 0);
                if (CurrentUnits == DataUnits.Imperial)
                    stripSize = UnitConverter.ConvertDistance("mm", "inch", stripSize);

                ThreadSafeControlUpdates.UpdateValue<decimal>(numStripWidth, (decimal)stripSize);
                savedStripWidth = numStripWidth.Value;

                // handle if stop button pressed
                if (RunningState == RunningStates.EdgeFinding) runWithoutEdgeScan();
            }
            else
            {
                logger.Trace("Failed edge scan");
                if (e.Error != null) logger.ErrorException(e.Error.Message, e.Error);
                RunningState = RunningStates.Stopped;
                EdgeState = EdgeStates.NotFound;
                if (e.Error != null) message(e.Error.Message);
                else message("Motor setup worker cancelled");
            }
        }

        private void runWithoutEdgeScan()
        {
            if (settingsForm.Settings.IsModified)
            {
                message("Save changes to settings before running a scan");
                RunningState = RunningStates.Stopped;
                return;
            }

            if (PaintingState == PaintingStates.NotPainting)
            {
                message("Wait until the strip is painting before a scan");
                RunningState = RunningStates.Stopped;
                return;
            }
            
            if ((EdgeState != EdgeStates.Found || numStripWidth.Value != savedStripWidth) && settingsForm.Settings.Fetch<bool>("connection/motor/enabled"))
            {
                RunningState = RunningStates.Stopped;

                // set up edges based on strip width and strip centre
                skipEdgeScan(ScanTypes.NormalScan, true, settingsForm.Settings.Fetch<int>("scan/motor/left pos/value"), Math.Abs(settingsForm.Settings.Fetch<int>("scan/motor/right pos/value")));
                return;
                
                /*logger.Error("Tried to run scan without finding edges first");
                message("Need to run edge scans before running");
                RunningState = RunningStates.Stopped;
                return;*/
            }
            
            // revalidate - necessary as can be called without edge scan, and because need to validate move positions
            if (!settingsForm.Settings.IsValid)
            {
                message(settingsForm.Settings.ValidationErrors[0].ErrorMessage);
                RunningState = RunningStates.Stopped;
                return;
            }

            if (!(settingsForm.Settings.Fetch<bool>("connection/sensor/enabled") || settingsForm.Settings.Fetch<bool>("connection/laser/enabled") || settingsForm.Settings.Fetch<bool>("connection/motor/enabled")))
            {
                message("Enable at least one of the sensor, laser or motor to take a scan");
                RunningState = RunningStates.Stopped;
                return;
            }

            logger.Trace("Preparing scan");
            RunningState = RunningStates.Preparing;

            // set up laser - if first time or last was a quick scan, and if laser enabled
            logger.Trace("Preparing laser");
            if ((!LaserInited || IsQuickScan) && settingsForm.Settings.Fetch<bool>("connection/laser/enabled"))
            {
                try
                {
                    if (!initLaser())
                    {
                        RunningState = RunningStates.Stopped;
                        return;
                    }
                    LaserInited = true;
                }
                catch (Exception ex)
                {
                    message("Could not set laser parameters: " + ex.Message);
                    logger.Error("Unhandled error in runWithoutEdgeScan when setting up laser: " + ex.Message);
                    RunningState = RunningStates.Stopped;
                    return;
                }
            }
            else { logger.Trace("Skipping laser"); }

            IsQuickScan = false;

            // set up sensor to take readings
            logger.Trace("Preparing sensor");
            if (settingsForm.Settings.Fetch<bool>("connection/sensor/enabled"))
            {
                try
                {
                    // get frequency
                    double frequency = Convert.ToDouble(settingsForm.Sensor.Parameters["frequency"].Get());

                    settingsForm.Sensor.TimeIncrement = 1.0 / frequency;
                    settingsForm.Sensor.CurrentTime = 0;

                    settingsForm.Sensor.EnsureReadingsInitialised();
                    settingsForm.Sensor.PortBuffer.Clear();
                }
                catch (Exception ex)
                {
                    message("Could not prepare sensor: " + ex.Message);
                    logger.Error("Unhandled error in runWithoutEdgeScan when setting up sensor: " + ex.Message);
                    RunningState = RunningStates.Stopped;
                    return;
                }
            }
            else { logger.Trace("Skipping sensor"); }

            if (RunningState != RunningStates.Stopped && RunningState != RunningStates.Stopping)
            {
                // run scans
                ScanArgs = null;
                scanWorker.RunWorkerAsync(false);
            }
            else logger.Trace("Not starting worker at end of runWithoutEdgeScan because running state is " + RunningState.ToString());
        }

        private bool initLaser()
        {
            LaserDurationSpecification laserDuration = settingsForm.Settings.Fetch<LaserDurationSpecification>("scan/laser/duration specification");
            int pulseCount = settingsForm.Settings.Fetch<int>("scan/laser/pulse count");
            int runTime = settingsForm.Settings.Fetch<int>("scan/laser/run time");
            int gatingFrequency = settingsForm.Settings.Fetch<int>("scan/laser/gating frequency");
            float dutyCycle1 = settingsForm.Settings.Fetch<float>("scan/laser/duty cycle");
            int dutyCycle = (int)(Math.Round(10 * dutyCycle1));

            // run-time - set in ms, 1-32766
            // frequency - set in Hz, 1-20000
            // duty cycle - set in %, 1-1000 => 0.1%-100.0%

            // work out run time
            if (laserDuration == LaserDurationSpecification.PulseCount)
            {
                runTime = (int)Math.Round(1000.0 / gatingFrequency * pulseCount);
            }

            return initLaser2(runTime, gatingFrequency, dutyCycle);
        }

        private bool initLaser2(int runTime, int gatingFrequency, int dutyCycle)
        {
            bool response;

            string value = Math.Max(1, Math.Min(runTime, 32766)).ToString().PadLeft(5, '0');
            logger.Trace("Setting run time to: " + value);
            response = settingsForm.Laser.Run("R", value);
            if (!response)
            {
                message("Could not set run time/pulse count");
                logger.Error("Could not set run time/pulse count");
                RunningState = RunningStates.Stopped;
                return false;
            }

            value = Math.Max(1, Math.Min(gatingFrequency, 20000)).ToString().PadLeft(5, '0');
            logger.Trace("Setting gating frequency to: " + value);
            response = settingsForm.Laser.Run("F", value);
            if (!response)
            {
                message("Could not set gating frequency");
                logger.Error("Could not set gating frequency");
                RunningState = RunningStates.Stopped;
                return false;
            }

            value = Math.Max(1, Math.Min(dutyCycle, 1000)).ToString().PadLeft(4, '0');
            logger.Trace("Setting duty cycle to: " + value);
            response = settingsForm.Laser.Run("D", value);
            if (!response)
            {
                message("Could not set duty cycle");
                logger.Error("Could not set duty cycle");
                RunningState = RunningStates.Stopped;
                return false;
            }

            return true;
        }

        public int ExpectedPits
        {
            get
            {
                if (IsQuickScan) return quickScanPits;

                bool laserEnabled = settingsForm.Settings.Fetch<bool>("connection/laser/enabled");
                if (!laserEnabled) return -1;

                LaserDurationSpecification laserDuration = settingsForm.Settings.Fetch<LaserDurationSpecification>("scan/laser/duration specification");
                int pulseCount = settingsForm.Settings.Fetch<int>("scan/laser/pulse count");
                int runTime = settingsForm.Settings.Fetch<int>("scan/laser/run time");
                int gatingFrequency = settingsForm.Settings.Fetch<int>("scan/laser/gating frequency");
                float dutyCycle1 = settingsForm.Settings.Fetch<float>("scan/laser/duty cycle");
                int dutyCycle = (int)(Math.Round(10 * dutyCycle1));

                if (laserDuration == LaserDurationSpecification.PulseCount) return pulseCount;
                else
                {
                    return (int)Math.Round(runTime * gatingFrequency / 1000.0);
                }
            }
        }

        /// <summary>
        /// Whether the laser is inited
        /// </summary>
        public bool LaserInited
        {
            get;
            set;
        }

        /// <summary>
        /// Whether the currently running scan is a quick scan
        /// </summary>
        public bool IsQuickScan
        {
            get;
            set;
        }

        /// <summary>
        /// Number of pits to use in a quick scan
        /// </summary>
        private int quickScanPits = 3;

        #endregion

        #region Scanning Work

        private bool blockUntilFinished(BackgroundWorker worker)
        {
            int i = 1;
            while (worker.IsBusy && i <= 10)
            {
                Thread.Sleep(i * i * 20);
                i += 1;
            }
            return !worker.IsBusy;
        }

        // contains the next task to do
        private ScanWorkerArgs scanArgs;
        private ScanWorkerArgs ScanArgs
        {
            get
            {
                // separate because lock fails if scanArgs is null
                if (scanArgs == null) return scanArgs;
                else
                {
                    lock (scanArgs)
                    {
                        return scanArgs;
                    }
                }
            }
            set
            {
                if (scanArgs == null) scanArgs = value;
                else
                {
                    lock (scanArgs)
                    {
                        scanArgs = value;
                    }
                }
            }
        }
        
        void Motor_FinishedMove(object sender, Motor.MotorTaskEventArgs e)
        {            
            if (!blockUntilFinished(scanWorker))
            {
                logger.Error("Scan worker remained busy when move finished");
                message("Could not continue scan after finished moving motor");                
                RunningState = RunningStates.Stopped;
                return;
            }

            // move finished, move on to next task
            if (RunningState != RunningStates.Stopped && RunningState != RunningStates.Stopping)
            {
                ScanArgs.Busy = false;
                ScanArgs.State = ScanWorkerArgs.States.Scanning;
                scanWorker.RunWorkerAsync(false);
            }
            else logger.Trace("Not starting worker at end of Motor_FinishedMove because running state is " + RunningState.ToString());
        }

        #region Worker 1

        void scanWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            // exit points:
            // 1) error if state machine fails
            // 2) return move to position, busy=true, state=moving
            // 3) busy=false, state=moving (start next move in complete)
            // 4) state=scanning, busy=false
            // 5) state=scanning, busy=true (start next scan in complete)
            // 6) position=finished

            BackgroundWorker worker = (BackgroundWorker)sender;

            // default next task
            if (ScanArgs == null)
            {
                ScanArgs = new ScanWorkerArgs(ScanWorkerArgs.Positions.Left, ScanWorkerArgs.States.Moving);                
            }
            if (e.Argument != null && (bool)e.Argument == true)
            {
                ScanArgs.IsQuickScan = true;
            }
            
            if (worker.CancellationPending) { e.Cancel = true; return; }
            
            ScanArgs.Busy = false;
            logger.Trace("Scanning: " + ScanArgs.ToString());

            if (RunningState == RunningStates.Stopping || RunningState == RunningStates.Stopped)
            {
                logger.Trace("Stopping because of running state: " + RunningState);
                return;
            }

            // finish state
            if (ScanArgs.Position == ScanWorkerArgs.Positions.Finished && ScanArgs.State != ScanWorkerArgs.States.Paused) return;
            
            // if can't scan any more, head home
            if (!ScanArgs.IsQuickScan)
            {
                if (PaintingState == PaintingStates.PaintingCountdown)
                {
                    // pause here if in countdown
                    while (PaintingState == PaintingStates.PaintingCountdown)
                    {
                        Thread.Sleep(500);
                        if (worker.CancellationPending) { e.Cancel = true; return; }
                    }
                }

                // head home if painting stopped
                if (PaintingState == PaintingStates.NotPainting && (ScanArgs.Position != ScanWorkerArgs.Positions.Finished && ScanArgs.Position != ScanWorkerArgs.Positions.Homed))
                {
                    ScanArgs.Position = ScanWorkerArgs.Positions.Homed;
                    ScanArgs.State = ScanWorkerArgs.States.Moving;
                    return;
                }
            }

            if (ScanArgs.State == ScanWorkerArgs.States.Moving)
            {
                bool motorEnabled = settingsForm.Settings.Fetch<bool>("connection/motor/enabled");
                bool homeAfter = settingsForm.Settings.Fetch<bool>("scan/timing/home after");

                bool leftEnabled = settingsForm.Settings.Fetch<bool>("scan/motor/left pos/enabled");
                bool rightEnabled = settingsForm.Settings.Fetch<bool>("scan/motor/right pos/enabled");
                bool middleEnabled = settingsForm.Settings.Fetch<bool>("scan/motor/middle pos/enabled");
                int leftPos = settingsForm.ProcessMovePosition(settingsForm.Settings.Fetch<int>("scan/motor/left pos/value"));
                int rightPos = settingsForm.ProcessMovePosition(settingsForm.Settings.Fetch<int>("scan/motor/right pos/value"));
                int middlePos = settingsForm.ProcessMovePosition(settingsForm.Settings.Fetch<int>("scan/motor/middle pos/pos/value"));

                if (ScanArgs.IsQuickScan)
                {
                    leftEnabled = rightEnabled = true;
                    middleEnabled = false;
                    leftPos = settingsForm.ProcessMovePosition(75);
                    rightPos = settingsForm.ProcessMovePosition(-75);
                }

                // if using the Galil, move
                if (motorEnabled)
                {
                    int moveTo = -1;
                    // go left -> right -> middle
                    switch (ScanArgs.Position)
                    {
                        case ScanWorkerArgs.Positions.Left:
                            if (leftEnabled)
                                moveTo = leftPos;
                            else
                            {
                                ScanArgs.NextPosition();
                                return;
                            }
                            break;

                        case ScanWorkerArgs.Positions.Right:
                            if (rightEnabled)
                                moveTo = rightPos;
                            else
                            {
                                ScanArgs.NextPosition();
                                return;
                            }
                            break;

                        case ScanWorkerArgs.Positions.Middle:
                            if (middleEnabled)
                            {
                                moveTo = middlePos;
                                // finding centre position should be done automatically
                            }
                            else
                            {
                                ScanArgs.NextPosition();
                                return;
                            }
                            break;

                        case ScanWorkerArgs.Positions.Homed:
                            if (homeAfter)
                                moveTo = 0;
                            else
                            {
                                ScanArgs.NextPosition();
                                return;
                            }
                            break;
                    }

                    logger.Trace("Moving to: " + moveTo.ToString());
                    if (moveTo > -1)
                    {
                        if (worker.CancellationPending) { e.Cancel = true; return; }
                        ScanArgs.Busy = true;
                        e.Result = moveTo;
                        return;
                    }
                    throw new ApplicationException("State machine failed when scanner is moving and motor is enabled");
                }
                else
                {
                    if (ScanArgs.Position == ScanWorkerArgs.Positions.Left)
                    {
                        logger.Trace("Skipping moving");
                        // skip move and go onto scan
                        ScanArgs.State = ScanWorkerArgs.States.Scanning;
                        return;
                    }
                    else
                    {
                        ScanArgs.Position = ScanWorkerArgs.Positions.Finished;
                        return;
                    }
                }
            }

            else if (ScanArgs.State == ScanWorkerArgs.States.Scanning)
            {
                bool sensorEnabled = settingsForm.Settings.Fetch<bool>("connection/sensor/enabled");
                bool laserEnabled = settingsForm.Settings.Fetch<bool>("connection/laser/enabled");
                bool doScan = sensorEnabled || laserEnabled;

                // scanning
                if (worker.CancellationPending) { e.Cancel = true; return; }

                if (doScan && ScanArgs.Position != ScanWorkerArgs.Positions.Homed)
                {
                    ScanArgs.Busy = true;
                }
                else
                {
                    // skip scan and analysis (as no point)
                    ScanArgs.State = ScanWorkerArgs.States.Moving;
                    ScanArgs.NextPosition();
                }
            }

            else //if (ScanArgs.State == ScanWorkerArgs.States.Paused)
            {
                if (PaintingState == PaintingStates.Painting)
                {
                    int pauseTime = settingsForm.Settings.Fetch<int>("scan/timing/repeat scan/interval");
                    if (pauseTime > 0)
                    {
                        logger.Trace(String.Format("Paused for {0} seconds", pauseTime));
                        // wait 
                        for (int i = 0; i < pauseTime; i++)
                        {
                            if (worker.CancellationPending) { e.Cancel = true; return; }
                            Thread.Sleep(1000);
                        }
                    }
                    else
                    {
                        logger.Trace("No pause (set to 0 seconds)");
                    }
                }
                else
                {
                    logger.Trace("Paused while waiting for Painting input");
                    while (PaintingState != PaintingStates.Painting)
                    {
                        if (worker.CancellationPending) { e.Cancel = true; return; }
                        Thread.Sleep(1000);
                    }
                    logger.Trace("Painting input is live again");
                }
                ScanArgs.State = ScanWorkerArgs.States.Moving;
                ScanArgs.Position = ScanWorkerArgs.Positions.Left;
            }
        }
                
        void scanWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        { }

        void scanWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            logger.Trace("Scan worker finished: " + ScanArgs.ToString());

            if (e.Error != null)
            {
                logger.ErrorException("Scan worker failed: " + e.Error.Message, e.Error);
                message("Error taking scan: " + e.Error.Message);
                RunningState = RunningStates.Stopped;
                return;
            }
            if (e.Cancelled) logger.Debug("Scan worker cancelled");

            if (RunningState == RunningStates.Stopped || RunningState == RunningStates.Stopping)
            {
                logger.Trace("Stopping because of RunningState = " + RunningState);
                return;
            }

            // Busy flag means to start some other async task which will start scanWorker when it completes
            if (e.Cancelled) RunningState = RunningStates.Stopped;
            else if (ScanArgs.Position == ScanWorkerArgs.Positions.Finished)
            {
                // handle repeats
                if (chkRepeatReadings.Checked && !ScanArgs.IsQuickScan)
                {
                    ScanArgs.State = ScanWorkerArgs.States.Paused;
                    scanWorker.RunWorkerAsync();
                }
                else RunningState = RunningStates.Stopped;
            }
            else if (ScanArgs.Busy)
            {
                if (ScanArgs.State == ScanWorkerArgs.States.Moving)
                {
                    settingsForm.Motor.MoveTo((int)e.Result);
                }
                else
                {
                    scanWorker2.RunWorkerAsync();
                }
            }
            else scanWorker.RunWorkerAsync();
        }

        #endregion

        #region Worker 2

        #region Helpers

        void startDAQ()
        {
            logger.Trace("Start DAQ");
            settingsForm.Sensor.PortBuffer.Clear();
        }

        void stopDAQ()
        {
            logger.Trace("Stop DAQ");
            settingsForm.Sensor.PortBuffer.StopAutoUpdate();
        }

        bool startLaser()
        {
            logger.Trace("Start laser");
            return settingsForm.Laser.Run("G");
        }

        bool killLaser()
        {
            logger.Trace("Stop laser");
            return settingsForm.Laser.Run("K");
        }

        private bool sleep(int time, BackgroundWorker worker, DoWorkEventArgs e)
        {
            int count = time / 1000 + 1;
            for (int i = 0; i < count; i++)
            {
                if (i == count - 1) Thread.Sleep(time - (count - 1) * 1000);
                else Thread.Sleep(1000);

                if (worker.CancellationPending)
                {
                    e.Cancel = true;
                    return false;
                }
            }

            return true;
        }
                
        #endregion
        
        void scanWorker2_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = sender as BackgroundWorker;

            bool sensorEnabled = settingsForm.Settings.Fetch<bool>("connection/sensor/enabled");
            bool laserEnabled = settingsForm.Settings.Fetch<bool>("connection/laser/enabled");

            int daqStart = settingsForm.Settings.Fetch<int>("scan/timing/daq start");
            int laserStart = settingsForm.Settings.Fetch<int>("scan/timing/laser start");
            int daqStop = settingsForm.Settings.Fetch<int>("scan/timing/daq stop");
            bool scanRepeat = settingsForm.Settings.Fetch<bool>("scan/timing/repeat/enabled");
            int scanRepeatCount = settingsForm.Settings.Fetch<int>("scan/timing/repeat/count");
            int scanRepeatInterval = Convert.ToInt32(1000 * settingsForm.Settings.Fetch<float>("scan/timing/repeat/interval")); // convert from s to ms

            if (ScanArgs.IsQuickScan)
            {
                scanRepeat = true;
                scanRepeatCount = 3;
                scanRepeatInterval = 1500;
            }

            if (worker.CancellationPending) { e.Cancel = true; return; }
            
            if (!scanRepeat) scanRepeatCount = 1;
                        
            // ensure all wait intervals are positive
            if (laserStart < daqStart) laserStart = daqStart;
            if (daqStop < laserStart) daqStop = laserStart + 50;
            if (scanRepeatInterval < daqStop) scanRepeatInterval = daqStop + 50;
                        
            if (!sensorEnabled && !laserEnabled)
                throw new ApplicationException("Error, trying to take a scan with neither laser or sensor working");

            if (!sensorEnabled)
            {
                // laser must be enabled then
                for (int runCount = 1; runCount <= scanRepeatCount; runCount++)
                {
                    logger.Trace("Scan {0} of {0}, only laser", runCount, scanRepeatCount);

                    // start laser
                    if (!sleep(laserStart, worker, e)) return;
                    if (!startLaser()) throw new ApplicationException("Could not activate laser gating");

                    if (worker.CancellationPending) { e.Cancel = true; return; }
                    if (runCount + 1 < scanRepeatCount && !sleep(scanRepeatInterval - laserStart, worker, e)) return;
                    if (worker.CancellationPending) { e.Cancel = true; return; }         
                    // note: don't report progress as no scans to load
                }
                logger.Trace("Finished scan worker 2 do work");
            }
            else
            {
                int delay;
                for (int runCount = 1; runCount <= scanRepeatCount; runCount++)
                {
                    logger.Trace("Scan {0} of {0}, sensor and maybe laser", runCount, scanRepeatCount);

                    // start autoupdating
                    settingsForm.Sensor.PortBuffer.AutoUpdate(50);

                    // start sensor
                    if (!sleep(daqStart, worker, e)) return;
                    startDAQ();
                    if (worker.CancellationPending) { e.Cancel = true; stopDAQ(); return; }
                    
                    // start laser
                    delay = daqStop - daqStart;
                    if (laserEnabled)
                    {
                        if (laserStart != daqStart)
                        {
                            if (!sleep(laserStart - daqStart, worker, e)) { stopDAQ(); return; }
                        }
                        if (!startLaser()) { stopDAQ(); throw new ApplicationException("Could not activate laser gating"); }
                        delay = daqStop - laserStart;
                    }
                    if (worker.CancellationPending) { e.Cancel = true; stopDAQ(); return; }

                    // stop sensor
                    if (delay > 0)
                        if (!sleep(delay, worker, e)) { stopDAQ(); return; }
                    stopDAQ();
                    if (worker.CancellationPending) { e.Cancel = true; return; }

                    logger.Trace("Taking readings now");

                    // read data
                    SensorReading[] readings = settingsForm.Sensor.TakeReadings();

                    logger.Trace("Saving readings now");
                    logger.Trace("Have {0} readings", readings.Length);
                    if (readings.Length > 0)
                    {
                        if (readings[0].IsError) logger.Trace("Reading[0] is " + readings[0].Error);
                        else logger.Trace("Reading[0] is " + readings[0].Value);
                    }

                    ScanFile fileDetails;
                    if (!saveReadings(readings, out fileDetails))
                    {
                        throw new ApplicationException("Could not get any non-error readings");
                    }

                    logger.Trace("Calling report progress now");                    

                    // load scan file
                    worker.ReportProgress(Convert.ToInt32(100.0 * runCount / scanRepeatCount), new LastScanDetails(runCount, scanRepeatCount, fileDetails, ScanArgs.Position, false, ExpectedPits));
                    
                    // repeat
                    if (PaintingState != PaintingStates.Painting && !ScanArgs.IsQuickScan) { return; } // if painting stopped, stop scanning
                    if (worker.CancellationPending) { e.Cancel = true; return; }                    
                    if (runCount < scanRepeatCount) 
                        if(!sleep(scanRepeatInterval - daqStop, worker, e)) return;
                    if (worker.CancellationPending) { e.Cancel = true; return; }                    
                }
            }
            logger.Trace("Finished scan worker 2 do work");
        }

        void scanWorker2_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            logger.Trace("Entering progress changed: " + e.UserState);
                    
            // load scans
            LastScanDetails details = (LastScanDetails)e.UserState;
            LoadScan(details);
        }

        void scanWorker2_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            logger.Trace("Scan worker 2 completed: " + ScanArgs.ToString());

            if (e.Error != null)
            {
                logger.ErrorException("Scan worker 2 failed: " + e.Error.Message, e.Error);
                message("Error taking scan 2: " + e.Error.Message);
                // go home
                ScanArgs.State = ScanWorkerArgs.States.Moving;
                ScanArgs.Position = ScanWorkerArgs.Positions.Homed;
                scanWorker.RunWorkerAsync();
                return;
            }

            if (e.Cancelled)
            {
                logger.Warn("Scan worker 2 was cancelled: " + ScanArgs.ToString());
                return;
            }

            if (!blockUntilFinished(scanWorker))
            {
                logger.Error("Scan worker remained busy when scan worker 2 finished");
                message("Could not continue scan after finishing previous scan");
                RunningState = RunningStates.Stopped;
                return;
            }
            if (RunningState != RunningStates.Stopped && RunningState != RunningStates.Stopping)
            {
                ScanArgs.State = ScanWorkerArgs.States.Moving;
                ScanArgs.NextPosition();
                scanWorker.RunWorkerAsync();
            }
            else logger.Trace("Not starting scanWorker because of RunningState = " + RunningState);
        }

        #endregion

        #endregion
        
        #endregion

        #region Scan Analysis & Display

        private bool saveReadings(SensorReading[] readings, out ScanFile fileDetails)
        {
            logger.Trace("Entering saveReadings");
            logger.Trace("Number readings = " + readings.Length.ToString());
            
            // save readings
            // create x-values and y_values to write to file
            int rLen = readings.Length;
            double[] data_x, data_y;
            List<double> list_x = new List<double>();
            List<double> list_y = new List<double>();
            SensorReading.DigitalErrors lastError = SensorReading.DigitalErrors.NO_ERROR;
            for (int j = 0; j < rLen; j++)
            {
                if (!readings[j].IsError)
                {
                    list_x.Add(readings[j].Time);
                    list_y.Add(readings[j].Value);
                }
                else if (lastError == SensorReading.DigitalErrors.NO_ERROR)
                    lastError = readings[j].Error;
            }

            logger.Trace("Filtered errors");
            logger.Trace("Number filtered readings = " + list_y.Count.ToString());
            logger.Trace("Last error = " + lastError.ToString() + " (" + Convert.ToInt32(lastError) + ")");

            data_x = list_x.ToArray();
            data_y = list_y.ToArray();

            // truncate data
            int truncateFirst = settingsForm.Settings.Fetch<int>("data/processing/truncate first");
            int truncateLast = settingsForm.Settings.Fetch<int>("data/processing/truncate last");

            logger.Trace("Truncating data");

            rLen = data_x.Length;
            if (rLen > 0)
            {
                int start_index = 0;
                int end_index = rLen; // not included

                if (truncateFirst > 0) start_index = (int)Math.Floor(Convert.ToDouble(truncateFirst) / 100 * rLen);
                if (truncateLast > 0) end_index = rLen - (int)Math.Ceiling(Convert.ToDouble(truncateLast) / 100 * rLen);

                if (end_index > start_index)
                {
                    // todo: simplify with copyto
                    double[] data_x_tmp = new double[end_index - start_index];
                    double[] data_y_tmp = new double[end_index - start_index];

                    for (int i = start_index, j = 0; i < end_index; i++, j++)
                    {
                        data_x_tmp[j] = data_x[i];
                        data_y_tmp[j] = data_y[i];
                    }
                    data_x = data_x_tmp;
                    data_y = data_y_tmp;
                    rLen = end_index - start_index;
                }
                else
                {
                    data_x = new double[0];
                    data_y = new double[0];
                    rLen = 0;
                }
            }

            logger.Trace("Number truncated readings = " + data_y.Length.ToString());            
                        
            if (rLen > 0)
            {
                logger.Trace("Some readings remain, saving to file");

                string dataLocation = settingsForm.Settings.Fetch<string>("data/saving/location");
                string filenamePattern = settingsForm.Settings.Fetch<string>("data/saving/filename pattern");

                logger.Trace("Getting file to save in");

                ScanManager scanManager = new ScanManager(dataLocation, filenamePattern);
                int nextScanId = scanManager.GetNextScanId();
                fileDetails = new ScanFile(dataLocation, filenamePattern, nextScanId);
                
                string filename = Path.Combine(dataLocation, fileDetails.Filename);

                logger.Trace("Chose " + filename);

                // write data
                DataIO dataIO = new DataIO();
                dataIO.Mode = DataIO.Modes.Write;
                dataIO.Filename = Path.Combine(dataLocation, fileDetails.Filename);
                dataIO.WriteData(data_x, data_y);

                logger.Trace("Written file");

                return true;
            }
            else
            {
                fileDetails = null;
                return false;
            }
        }

        protected void LoadScan(LastScanDetails details)
        {
            try
            {
                logger.Trace("Entering LoadScan");
                DFMAnalysis analysis = new DFMAnalysis();
                analysis.Options = new DFMAnalysisOptions();
                analysis.Options.SmoothingWidth = settingsForm.Settings.Fetch<int>("data/analysis/smoothing width");
                analysis.Options.MinPitWidth = settingsForm.Settings.Fetch<int>("data/analysis/min pit width");
                analysis.Options.PitPoints = settingsForm.Settings.Fetch<int>("data/analysis/pit count");
                analysis.Options.ThresholdParameter = settingsForm.Settings.Fetch<float>("data/analysis/threshold parameter");
                analysis.Options.Compression = settingsForm.Settings.Fetch<float>("data/analysis/compression factor");
                analysis.Options.SubstrateRoughness = settingsForm.Settings.Fetch<float>("data/analysis/substrate roughness");

                logger.Trace("Set analysis options");

                DataIO reader = new DataIO();
                reader.Mode = DataIO.Modes.Read;
                reader.Filename = Path.Combine(details.FileDetails.Path, details.FileDetails.Filename);

                double[] data_x, data_y;
                reader.ReadData(out data_x, out data_y);

                logger.Trace("Read file");

                // filter data
                if (settingsForm.Settings.Fetch<bool>("data/processing/filtering/enabled"))
                {
                    int rLen = data_y.Length;
                    double[] data_y_temp = new double[rLen];
                    for (int j = 0; j < rLen; j++)
                    {
                        data_y_temp[j] = data_y[j];
                    }
                
                    NoiseFilter.Filter(data_y_temp, out data_y,
                        settingsForm.Settings.Fetch<float>("data/processing/filtering/peak threshold") / 1000,
                        settingsForm.Settings.Fetch<float>("data/processing/filtering/peak hysteresis") / 1000,
                        analysis.Options.SmoothingWidth,
                        settingsForm.Settings.Fetch<int>("data/processing/filtering/max peak width"));
                }

                logger.Trace("Filtered data");

                analysis.SetData(data_x, data_y);

                logger.Trace("Set data");
                analysis.Analyse();

                logger.Trace("Analysed data");

                // validate
                DFMScanDisplayControl current = dfmScanDisplayControlLeft;
                if (details.Position == ScanWorkerArgs.Positions.Middle) current = dfmScanDisplayControlMiddle;
                else if (details.Position == ScanWorkerArgs.Positions.Right) current = dfmScanDisplayControlRight;

                if (details.ExpectedPits > 0)
                {
                    bool valid = false;
                    int numPits = analysis.Data.PeakCount;
                    if (numPits != 0 && Math.Abs(details.ExpectedPits - numPits) <= 1) valid = true;

                    if (!valid)
                    {
                        bool filterErrors = settingsForm.Settings.Fetch<bool>("scan/laser/validate pulse count/enabled") && settingsForm.Settings.Fetch<bool>("scan/laser/validate pulse count/filter errors");
                        bool displayMessage = settingsForm.Settings.Fetch<bool>("scan/laser/validate pulse count/enabled") && settingsForm.Settings.Fetch<bool>("scan/laser/validate pulse count/display message");

                        logger.Warn(String.Format("Wrong pulse count: got {0} pit(s), expected {1} pit(s)", numPits, details.ExpectedPits));
                        if (displayMessage) current.DisplayError("Error: wrong pulse count");                            
                        if (filterErrors) return;
                    }
                }

                logger.Trace("Validated data");

                // convert for TL display
                double[] data_y_new = new double[data_y.Length];
                if (CurrentUnits == DataUnits.Imperial)
                    data_y_new = Array.ConvertAll(analysis.Data.Normalised_Y, UnitConverter.GetDistanceConverter("micron", "mil"));
                else data_y_new = analysis.Data.Normalised_Y;

                logger.Trace("Changed units of data");

                // add info to TL display
                GraphPane pane = zgcScanDisplay.GraphPane;
                pane.CurveList.Clear();
                PointPairList points = new PointPairList(analysis.Data.Data_X, data_y_new);
                pane.AddCurve("scan", points, Color.Blue);
                zgcScanDisplay.AxisChange();
                zgcScanDisplay.Invalidate();

                // add info to scan display                                
                current.AddData(new ScanData(analysis.Data.PeakMean, analysis.Data.PeakCount));

                // add result to db
                if (analysis.Data.PeakCount > 0)
                {
                    DFMResult result = new DFMResult();
                    result.Date = DateTime.Now;
                    result.PitDepth = new PitDepth(analysis.Data.PeakMean, PitDepth.DefaultUnit);
                    result.NumPits = analysis.Data.PeakCount;
                    result.ScanPosition = 0;
                    if (details.Position == ScanWorkerArgs.Positions.Middle) result.ScanPosition = 1;
                    else if (details.Position == ScanWorkerArgs.Positions.Right) result.ScanPosition = 2;
                    result.ScanPath = details.FileDetails.Path;
                    result.ScanId = details.FileDetails.ScanId;
                    result.Comment = txtScanComment.Text;

                    dfmResults.AddResultIfNew(result);
                    databaseForm.RefreshDisplay();
                }
            }
            catch (Exception ex)
            {
                logger.ErrorException(ex.Message, ex);
                throw;
            }
        }

        #endregion
    }

    public class LastScanDetails
    {
        public int ScanNumber { get; set; }
        public int RepeatCount { get; set; }
        public ScanFile FileDetails { get; set; }
        public ScanWorkerArgs.Positions Position { get; set; }
        public int ExpectedPits { get; set; }
        public bool FromFile { get; set; }

        public LastScanDetails(int scanNumber, int repeatCount, ScanFile fileDetails, ScanWorkerArgs.Positions position, bool fromFile, int expectedPits=-1)
        {
            ScanNumber = scanNumber;
            RepeatCount = repeatCount;
            FileDetails = fileDetails;
            Position = position;
            FromFile = fromFile;
            ExpectedPits = expectedPits;
        }

        public override string ToString()
        {
            return "ScanNumber=" + ScanNumber.ToString() + ", RepeatCount=" + RepeatCount.ToString() + ", FileDetails=" + FileDetails.Filename + ", Position=" + Position.ToString() + ", FromFile=" + FromFile.ToString() + ", ExpectedPits=" + ExpectedPits.ToString();
        }
    }

    public class ScanWorkerArgs
    {
        public enum Positions
        {
            Homed,
            Left,
            Middle,
            Right,
            Finished
        }
        
        public Positions Position
        {
            get;
            set;
        }

        public enum States
        {
            Moving,
            Scanning,
            Paused
        }

        public States State
        {
            get;
            set;
        }

        public bool Busy
        {
            get;
            set;
        }

        public bool IsQuickScan
        {
            get;
            set;
        }

        public ScanWorkerArgs(Positions position, States state)
        {
            Position = position;
            State = state;
            Busy = false;
            IsQuickScan = false;
        }

        public void NextPosition()
        {
            // go left -> right -> middle
            if (Position == Positions.Left) Position = Positions.Right;
            else if (Position == Positions.Right) Position = Positions.Middle;
            else if (Position == Positions.Middle) Position = Positions.Homed;
            else Position = Positions.Finished;
        }

        public override string ToString()
        {
            return "Position=" + Position.ToString() + ", State=" + State.ToString() + ", Busy=" + Busy.ToString() + ", IsQuickScan=" + IsQuickScan.ToString();
        }
    }

    public class DFMStateChangedEventArgs : EventArgs
    {
        private RunningStates runningState;
        public RunningStates RunningState
        {
            get { return runningState; }
            set { runningState = value; }
        }

        private EdgeStates edgeState;
        public EdgeStates EdgeState
        {
            get { return edgeState; }
            set { edgeState = value; }
        }

        private PaintingStates paintingState;
        public PaintingStates PaintingState
        {
            get { return paintingState; }
            set { paintingState = value; }
        }

        public DFMStateChangedEventArgs(RunningStates runningState, EdgeStates edgeState, PaintingStates paintingState)
        {
            RunningState = runningState;
            EdgeState = edgeState;
            PaintingState = paintingState;
        }
    }

    public enum RunningStates
    {
        Stopped,
        Stopping,
        Preparing,
        EdgeFinding,
        Scanning
    }

    public enum EdgeStates
    {
        NotFound,
        Found
    }

    public enum PaintingStates
    {
        Painting,
        NotPainting,
        PaintingCountdown
    }
}
