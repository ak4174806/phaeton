﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using ZedGraph;

namespace DFM
{
    public partial class GraphDisplay : Form
    {
        public GraphDisplay()
        {
            InitializeComponent();

            Load += new EventHandler(GraphDisplay_Load);
        }

        public GraphDisplay(ZedGraphControl graph, string heading)
            : this()
        {
            Load += new EventHandler(delegate(object sender, EventArgs e)
            {
                FromGraph(graph);
                Heading = heading;
            });
            this.Show();
        }

        void GraphDisplay_Load(object sender, EventArgs e)
        {
            Heading = Heading;
        }

        public string Heading
        {
            get { return lblHeading.Text; }
            set { Text = lblHeading.Text = value; }
        }

        public ZedGraphControl Graph
        {
            get { return zgcGraph; }
        }

        /// <summary>
        /// Copies a reference to the curvelist, and clones titles and stuff
        /// </summary>
        /// <param name="other"></param>
        public void FromGraph(ZedGraphControl other)
        {
            // copy curves
            Graph.GraphPane.CurveList.AddRange(other.GraphPane.CurveList);

            // copy labels
            Graph.GraphPane.XAxis.Title = other.GraphPane.XAxis.Title;
            Graph.GraphPane.YAxis.Title = other.GraphPane.YAxis.Title;
            Graph.GraphPane.Title.Text = other.GraphPane.Title.Text;

            Graph.AxisChange();
            Graph.Invalidate();
        }
    }
}
