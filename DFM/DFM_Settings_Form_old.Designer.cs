﻿namespace DFM
{
    partial class DFM_Settings_Form_old
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.Label lblSensorBaudrate;
            System.Windows.Forms.Label lblSensorPort;
            System.Windows.Forms.Label lblSensorType;
            System.Windows.Forms.Label lblSensorModel;
            System.Windows.Forms.Label label3;
            System.Windows.Forms.Label label4;
            System.Windows.Forms.Label label5;
            System.Windows.Forms.Label label6;
            System.Windows.Forms.Label lblDAQStop;
            System.Windows.Forms.Label label7;
            System.Windows.Forms.Label label9;
            System.Windows.Forms.Label lblLaserStart;
            System.Windows.Forms.Label label10;
            System.Windows.Forms.Label label11;
            System.Windows.Forms.Label label12;
            System.Windows.Forms.Label label8;
            System.Windows.Forms.Label lblGatingFreq;
            System.Windows.Forms.Label lblDutyCycle;
            System.Windows.Forms.Label label13;
            System.Windows.Forms.Label label14;
            System.Windows.Forms.Label label16;
            System.Windows.Forms.Label label17;
            System.Windows.Forms.Label label18;
            System.Windows.Forms.Label label19;
            System.Windows.Forms.Label label20;
            System.Windows.Forms.Label label21;
            System.Windows.Forms.Label label22;
            System.Windows.Forms.Label label23;
            System.Windows.Forms.Label lblMaxPeakWidth;
            System.Windows.Forms.Label lblPeakHysteresis;
            System.Windows.Forms.Label lblPeakThreshold;
            System.Windows.Forms.Label lblPitPoints;
            System.Windows.Forms.Label lblSubstrateRoughness;
            System.Windows.Forms.Label lblCompressionFactor;
            System.Windows.Forms.Label lblSmoothingWidth;
            System.Windows.Forms.Label lblMinPitWidth;
            System.Windows.Forms.Label lblThresholdParam;
            System.Windows.Forms.Label lblFilenamePattern;
            System.Windows.Forms.Label lblChooseDataFolder;
            this.tpConnection = new System.Windows.Forms.TabPage();
            this.btnRefreshCOMPorts = new System.Windows.Forms.Button();
            this.btnConnect = new System.Windows.Forms.Button();
            this.tabConnection = new System.Windows.Forms.TabControl();
            this.tpSensorConnection = new System.Windows.Forms.TabPage();
            this.cmbSensorBaudrate = new System.Windows.Forms.ComboBox();
            this.cmbSensorPort = new System.Windows.Forms.ComboBox();
            this.cmbSensorType = new System.Windows.Forms.ComboBox();
            this.cmbSensorModel = new System.Windows.Forms.ComboBox();
            this.chkConnectSensor = new System.Windows.Forms.CheckBox();
            this.tpLaserConnection = new System.Windows.Forms.TabPage();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.tpMotorConnection = new System.Windows.Forms.TabPage();
            this.comboBox3 = new System.Windows.Forms.ComboBox();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.tpScanParameters = new System.Windows.Forms.TabPage();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.tabScanTiming = new System.Windows.Forms.TabPage();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown2 = new System.Windows.Forms.NumericUpDown();
            this.numDAQStop = new System.Windows.Forms.NumericUpDown();
            this.numDAQStart = new System.Windows.Forms.NumericUpDown();
            this.lblDAQStart = new System.Windows.Forms.Label();
            this.numLaserStart = new System.Windows.Forms.NumericUpDown();
            this.tabLaserDetails = new System.Windows.Forms.TabPage();
            this.radRunTime = new System.Windows.Forms.RadioButton();
            this.radPulseCount = new System.Windows.Forms.RadioButton();
            this.numRunTime = new System.Windows.Forms.NumericUpDown();
            this.numPulseCount = new System.Windows.Forms.NumericUpDown();
            this.numDutyCycle = new System.Windows.Forms.NumericUpDown();
            this.numGatingFreq = new System.Windows.Forms.NumericUpDown();
            this.tabMotorDetails = new System.Windows.Forms.TabPage();
            this.checkBox4 = new System.Windows.Forms.CheckBox();
            this.numericUpDown3 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown4 = new System.Windows.Forms.NumericUpDown();
            this.label15 = new System.Windows.Forms.Label();
            this.numericUpDown5 = new System.Windows.Forms.NumericUpDown();
            this.tpDataAnalysis = new System.Windows.Forms.TabPage();
            this.tabControl3 = new System.Windows.Forms.TabControl();
            this.tabDataProcessing = new System.Windows.Forms.TabPage();
            this.numTruncateFirst = new System.Windows.Forms.NumericUpDown();
            this.numTruncateLast = new System.Windows.Forms.NumericUpDown();
            this.numMaxPeakWidth = new System.Windows.Forms.NumericUpDown();
            this.numPeakHysteresis = new System.Windows.Forms.NumericUpDown();
            this.numPeakThreshold = new System.Windows.Forms.NumericUpDown();
            this.chkFilterPeaks = new System.Windows.Forms.CheckBox();
            this.tabAnalysisParameters = new System.Windows.Forms.TabPage();
            this.numPitPoints = new System.Windows.Forms.NumericUpDown();
            this.numSubstrateRoughness = new System.Windows.Forms.NumericUpDown();
            this.numCompressionFactor = new System.Windows.Forms.NumericUpDown();
            this.numSmoothingWidth = new System.Windows.Forms.NumericUpDown();
            this.numMinPitWidth = new System.Windows.Forms.NumericUpDown();
            this.numThresholdParam = new System.Windows.Forms.NumericUpDown();
            this.tpOther = new System.Windows.Forms.TabPage();
            this.txtFilenamePattern = new System.Windows.Forms.TextBox();
            this.btnChooseDataFolder = new System.Windows.Forms.Button();
            this.txtDataFolder = new System.Windows.Forms.TextBox();
            lblSensorBaudrate = new System.Windows.Forms.Label();
            lblSensorPort = new System.Windows.Forms.Label();
            lblSensorType = new System.Windows.Forms.Label();
            lblSensorModel = new System.Windows.Forms.Label();
            label3 = new System.Windows.Forms.Label();
            label4 = new System.Windows.Forms.Label();
            label5 = new System.Windows.Forms.Label();
            label6 = new System.Windows.Forms.Label();
            lblDAQStop = new System.Windows.Forms.Label();
            label7 = new System.Windows.Forms.Label();
            label9 = new System.Windows.Forms.Label();
            lblLaserStart = new System.Windows.Forms.Label();
            label10 = new System.Windows.Forms.Label();
            label11 = new System.Windows.Forms.Label();
            label12 = new System.Windows.Forms.Label();
            label8 = new System.Windows.Forms.Label();
            lblGatingFreq = new System.Windows.Forms.Label();
            lblDutyCycle = new System.Windows.Forms.Label();
            label13 = new System.Windows.Forms.Label();
            label14 = new System.Windows.Forms.Label();
            label16 = new System.Windows.Forms.Label();
            label17 = new System.Windows.Forms.Label();
            label18 = new System.Windows.Forms.Label();
            label19 = new System.Windows.Forms.Label();
            label20 = new System.Windows.Forms.Label();
            label21 = new System.Windows.Forms.Label();
            label22 = new System.Windows.Forms.Label();
            label23 = new System.Windows.Forms.Label();
            lblMaxPeakWidth = new System.Windows.Forms.Label();
            lblPeakHysteresis = new System.Windows.Forms.Label();
            lblPeakThreshold = new System.Windows.Forms.Label();
            lblPitPoints = new System.Windows.Forms.Label();
            lblSubstrateRoughness = new System.Windows.Forms.Label();
            lblCompressionFactor = new System.Windows.Forms.Label();
            lblSmoothingWidth = new System.Windows.Forms.Label();
            lblMinPitWidth = new System.Windows.Forms.Label();
            lblThresholdParam = new System.Windows.Forms.Label();
            lblFilenamePattern = new System.Windows.Forms.Label();
            lblChooseDataFolder = new System.Windows.Forms.Label();
            this.tabSettings.SuspendLayout();
            this.controlButtonsContainer.Panel1.SuspendLayout();
            this.controlButtonsContainer.SuspendLayout();
            this.outerContainer.Panel2.SuspendLayout();
            this.outerContainer.SuspendLayout();
            this.tpConnection.SuspendLayout();
            this.tabConnection.SuspendLayout();
            this.tpSensorConnection.SuspendLayout();
            this.tpLaserConnection.SuspendLayout();
            this.tpMotorConnection.SuspendLayout();
            this.tpScanParameters.SuspendLayout();
            this.tabControl2.SuspendLayout();
            this.tabScanTiming.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numDAQStop)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numDAQStart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numLaserStart)).BeginInit();
            this.tabLaserDetails.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numRunTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPulseCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numDutyCycle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numGatingFreq)).BeginInit();
            this.tabMotorDetails.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown5)).BeginInit();
            this.tpDataAnalysis.SuspendLayout();
            this.tabControl3.SuspendLayout();
            this.tabDataProcessing.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numTruncateFirst)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numTruncateLast)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numMaxPeakWidth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPeakHysteresis)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPeakThreshold)).BeginInit();
            this.tabAnalysisParameters.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numPitPoints)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSubstrateRoughness)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numCompressionFactor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSmoothingWidth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numMinPitWidth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numThresholdParam)).BeginInit();
            this.tpOther.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabSettings
            // 
            this.tabSettings.Controls.Add(this.tpConnection);
            this.tabSettings.Controls.Add(this.tpScanParameters);
            this.tabSettings.Controls.Add(this.tpDataAnalysis);
            this.tabSettings.Controls.Add(this.tpOther);
            this.tabSettings.Size = new System.Drawing.Size(256, 247);
            // 
            // controlButtonsContainer
            // 
            this.controlButtonsContainer.SplitterDistance = 267;
            // 
            // outerContainer
            // 
            // 
            // lblSensorBaudrate
            // 
            lblSensorBaudrate.AutoSize = true;
            lblSensorBaudrate.Location = new System.Drawing.Point(156, 69);
            lblSensorBaudrate.Name = "lblSensorBaudrate";
            lblSensorBaudrate.Size = new System.Drawing.Size(53, 13);
            lblSensorBaudrate.TabIndex = 43;
            lblSensorBaudrate.Text = "Baudrate:";
            // 
            // lblSensorPort
            // 
            lblSensorPort.AutoSize = true;
            lblSensorPort.Location = new System.Drawing.Point(16, 69);
            lblSensorPort.Name = "lblSensorPort";
            lblSensorPort.Size = new System.Drawing.Size(29, 13);
            lblSensorPort.TabIndex = 40;
            lblSensorPort.Text = "Port:";
            // 
            // lblSensorType
            // 
            lblSensorType.AutoSize = true;
            lblSensorType.Location = new System.Drawing.Point(156, 42);
            lblSensorType.Name = "lblSensorType";
            lblSensorType.Size = new System.Drawing.Size(34, 13);
            lblSensorType.TabIndex = 36;
            lblSensorType.Text = "Type:";
            // 
            // lblSensorModel
            // 
            lblSensorModel.AutoSize = true;
            lblSensorModel.Location = new System.Drawing.Point(16, 42);
            lblSensorModel.Name = "lblSensorModel";
            lblSensorModel.Size = new System.Drawing.Size(39, 13);
            lblSensorModel.TabIndex = 37;
            lblSensorModel.Text = "Model:";
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Location = new System.Drawing.Point(16, 70);
            label3.Name = "label3";
            label3.Size = new System.Drawing.Size(53, 13);
            label3.TabIndex = 47;
            label3.Text = "Baudrate:";
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Location = new System.Drawing.Point(15, 44);
            label4.Name = "label4";
            label4.Size = new System.Drawing.Size(29, 13);
            label4.TabIndex = 44;
            label4.Text = "Port:";
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.Location = new System.Drawing.Point(15, 44);
            label5.Name = "label5";
            label5.Size = new System.Drawing.Size(48, 13);
            label5.TabIndex = 48;
            label5.Text = "Address:";
            // 
            // label6
            // 
            label6.AutoSize = true;
            label6.Location = new System.Drawing.Point(215, 17);
            label6.Name = "label6";
            label6.Size = new System.Drawing.Size(20, 13);
            label6.TabIndex = 34;
            label6.Text = "ms";
            // 
            // lblDAQStop
            // 
            lblDAQStop.AutoSize = true;
            lblDAQStop.Location = new System.Drawing.Point(17, 68);
            lblDAQStop.Name = "lblDAQStop";
            lblDAQStop.Size = new System.Drawing.Size(82, 13);
            lblDAQStop.TabIndex = 38;
            lblDAQStop.Text = "Stop DAQ after:";
            // 
            // label7
            // 
            label7.AutoSize = true;
            label7.Location = new System.Drawing.Point(215, 44);
            label7.Name = "label7";
            label7.Size = new System.Drawing.Size(20, 13);
            label7.TabIndex = 37;
            label7.Text = "ms";
            // 
            // label9
            // 
            label9.AutoSize = true;
            label9.Location = new System.Drawing.Point(215, 69);
            label9.Name = "label9";
            label9.Size = new System.Drawing.Size(20, 13);
            label9.TabIndex = 40;
            label9.Text = "ms";
            // 
            // lblLaserStart
            // 
            lblLaserStart.AutoSize = true;
            lblLaserStart.Location = new System.Drawing.Point(17, 43);
            lblLaserStart.Name = "lblLaserStart";
            lblLaserStart.Size = new System.Drawing.Size(81, 13);
            lblLaserStart.TabIndex = 35;
            lblLaserStart.Text = "Start laser after:";
            // 
            // label10
            // 
            label10.AutoSize = true;
            label10.Location = new System.Drawing.Point(17, 103);
            label10.Name = "label10";
            label10.Size = new System.Drawing.Size(42, 13);
            label10.TabIndex = 41;
            label10.Text = "Repeat";
            // 
            // label11
            // 
            label11.AutoSize = true;
            label11.Location = new System.Drawing.Point(128, 103);
            label11.Name = "label11";
            label11.Size = new System.Drawing.Size(60, 13);
            label11.TabIndex = 40;
            label11.Text = "times every";
            // 
            // label12
            // 
            label12.AutoSize = true;
            label12.Location = new System.Drawing.Point(253, 103);
            label12.Name = "label12";
            label12.Size = new System.Drawing.Size(12, 13);
            label12.TabIndex = 40;
            label12.Text = "s";
            // 
            // label8
            // 
            label8.AutoSize = true;
            label8.Location = new System.Drawing.Point(210, 105);
            label8.Name = "label8";
            label8.Size = new System.Drawing.Size(15, 13);
            label8.TabIndex = 31;
            label8.Text = "%";
            // 
            // lblGatingFreq
            // 
            lblGatingFreq.AutoSize = true;
            lblGatingFreq.Location = new System.Drawing.Point(19, 75);
            lblGatingFreq.Name = "lblGatingFreq";
            lblGatingFreq.Size = new System.Drawing.Size(94, 13);
            lblGatingFreq.TabIndex = 26;
            lblGatingFreq.Text = "Gating Frequency:";
            // 
            // lblDutyCycle
            // 
            lblDutyCycle.AutoSize = true;
            lblDutyCycle.Location = new System.Drawing.Point(19, 104);
            lblDutyCycle.Name = "lblDutyCycle";
            lblDutyCycle.Size = new System.Drawing.Size(61, 13);
            lblDutyCycle.TabIndex = 29;
            lblDutyCycle.Text = "Duty Cycle:";
            // 
            // label13
            // 
            label13.AutoSize = true;
            label13.Location = new System.Drawing.Point(210, 77);
            label13.Name = "label13";
            label13.Size = new System.Drawing.Size(20, 13);
            label13.TabIndex = 28;
            label13.Text = "Hz";
            // 
            // label14
            // 
            label14.AutoSize = true;
            label14.Location = new System.Drawing.Point(192, 22);
            label14.Name = "label14";
            label14.Size = new System.Drawing.Size(23, 13);
            label14.TabIndex = 43;
            label14.Text = "mm";
            // 
            // label16
            // 
            label16.AutoSize = true;
            label16.Location = new System.Drawing.Point(18, 72);
            label16.Name = "label16";
            label16.Size = new System.Drawing.Size(72, 13);
            label16.TabIndex = 47;
            label16.Text = "Right Position";
            // 
            // label17
            // 
            label17.AutoSize = true;
            label17.Location = new System.Drawing.Point(192, 49);
            label17.Name = "label17";
            label17.Size = new System.Drawing.Size(23, 13);
            label17.TabIndex = 46;
            label17.Text = "mm";
            // 
            // label18
            // 
            label18.AutoSize = true;
            label18.Location = new System.Drawing.Point(192, 74);
            label18.Name = "label18";
            label18.Size = new System.Drawing.Size(23, 13);
            label18.TabIndex = 49;
            label18.Text = "mm";
            // 
            // label19
            // 
            label19.AutoSize = true;
            label19.Location = new System.Drawing.Point(18, 47);
            label19.Name = "label19";
            label19.Size = new System.Drawing.Size(78, 13);
            label19.TabIndex = 44;
            label19.Text = "Middle Position";
            // 
            // label20
            // 
            label20.AutoSize = true;
            label20.Location = new System.Drawing.Point(161, 15);
            label20.Name = "label20";
            label20.Size = new System.Drawing.Size(51, 13);
            label20.TabIndex = 61;
            label20.Text = "% of data";
            // 
            // label21
            // 
            label21.AutoSize = true;
            label21.Location = new System.Drawing.Point(21, 16);
            label21.Name = "label21";
            label21.Size = new System.Drawing.Size(72, 13);
            label21.TabIndex = 59;
            label21.Text = "Truncate first:";
            // 
            // label22
            // 
            label22.AutoSize = true;
            label22.Location = new System.Drawing.Point(161, 35);
            label22.Name = "label22";
            label22.Size = new System.Drawing.Size(51, 13);
            label22.TabIndex = 64;
            label22.Text = "% of data";
            // 
            // label23
            // 
            label23.AutoSize = true;
            label23.Location = new System.Drawing.Point(20, 35);
            label23.Name = "label23";
            label23.Size = new System.Drawing.Size(69, 13);
            label23.TabIndex = 62;
            label23.Text = "Truncate last";
            // 
            // lblMaxPeakWidth
            // 
            lblMaxPeakWidth.AutoSize = true;
            lblMaxPeakWidth.Location = new System.Drawing.Point(19, 119);
            lblMaxPeakWidth.Name = "lblMaxPeakWidth";
            lblMaxPeakWidth.Size = new System.Drawing.Size(86, 13);
            lblMaxPeakWidth.TabIndex = 52;
            lblMaxPeakWidth.Text = "Max Peak Width";
            // 
            // lblPeakHysteresis
            // 
            lblPeakHysteresis.AutoSize = true;
            lblPeakHysteresis.Location = new System.Drawing.Point(19, 98);
            lblPeakHysteresis.Name = "lblPeakHysteresis";
            lblPeakHysteresis.Size = new System.Drawing.Size(128, 13);
            lblPeakHysteresis.TabIndex = 53;
            lblPeakHysteresis.Text = "Peak Hysteresis (microns)";
            // 
            // lblPeakThreshold
            // 
            lblPeakThreshold.AutoSize = true;
            lblPeakThreshold.Location = new System.Drawing.Point(19, 79);
            lblPeakThreshold.Name = "lblPeakThreshold";
            lblPeakThreshold.Size = new System.Drawing.Size(127, 13);
            lblPeakThreshold.TabIndex = 54;
            lblPeakThreshold.Text = "Peak Threshold (microns)";
            // 
            // lblPitPoints
            // 
            lblPitPoints.AutoSize = true;
            lblPitPoints.Location = new System.Drawing.Point(21, 134);
            lblPitPoints.Name = "lblPitPoints";
            lblPitPoints.Size = new System.Drawing.Size(92, 13);
            lblPitPoints.TabIndex = 70;
            lblPitPoints.Text = "Points in each Pit:";
            // 
            // lblSubstrateRoughness
            // 
            lblSubstrateRoughness.AutoSize = true;
            lblSubstrateRoughness.Location = new System.Drawing.Point(21, 15);
            lblSubstrateRoughness.Name = "lblSubstrateRoughness";
            lblSubstrateRoughness.Size = new System.Drawing.Size(112, 13);
            lblSubstrateRoughness.TabIndex = 68;
            lblSubstrateRoughness.Text = "Substrate Roughness:";
            // 
            // lblCompressionFactor
            // 
            lblCompressionFactor.AutoSize = true;
            lblCompressionFactor.Location = new System.Drawing.Point(21, 107);
            lblCompressionFactor.Name = "lblCompressionFactor";
            lblCompressionFactor.Size = new System.Drawing.Size(103, 13);
            lblCompressionFactor.TabIndex = 61;
            lblCompressionFactor.Text = "Compression Factor:";
            // 
            // lblSmoothingWidth
            // 
            lblSmoothingWidth.AutoSize = true;
            lblSmoothingWidth.Location = new System.Drawing.Point(21, 58);
            lblSmoothingWidth.Name = "lblSmoothingWidth";
            lblSmoothingWidth.Size = new System.Drawing.Size(91, 13);
            lblSmoothingWidth.TabIndex = 60;
            lblSmoothingWidth.Text = "Smoothing Width:";
            // 
            // lblMinPitWidth
            // 
            lblMinPitWidth.AutoSize = true;
            lblMinPitWidth.Location = new System.Drawing.Point(21, 82);
            lblMinPitWidth.Name = "lblMinPitWidth";
            lblMinPitWidth.Size = new System.Drawing.Size(73, 13);
            lblMinPitWidth.TabIndex = 63;
            lblMinPitWidth.Text = "Min Pit Width:";
            // 
            // lblThresholdParam
            // 
            lblThresholdParam.AutoSize = true;
            lblThresholdParam.Location = new System.Drawing.Point(21, 35);
            lblThresholdParam.Name = "lblThresholdParam";
            lblThresholdParam.Size = new System.Drawing.Size(108, 13);
            lblThresholdParam.TabIndex = 62;
            lblThresholdParam.Text = "Threshold Parameter:";
            // 
            // lblFilenamePattern
            // 
            lblFilenamePattern.AutoSize = true;
            lblFilenamePattern.Location = new System.Drawing.Point(23, 63);
            lblFilenamePattern.Name = "lblFilenamePattern";
            lblFilenamePattern.Size = new System.Drawing.Size(89, 13);
            lblFilenamePattern.TabIndex = 33;
            lblFilenamePattern.Text = "Filename Pattern:";
            // 
            // lblChooseDataFolder
            // 
            lblChooseDataFolder.AutoSize = true;
            lblChooseDataFolder.Location = new System.Drawing.Point(23, 26);
            lblChooseDataFolder.Name = "lblChooseDataFolder";
            lblChooseDataFolder.Size = new System.Drawing.Size(77, 13);
            lblChooseDataFolder.TabIndex = 30;
            lblChooseDataFolder.Text = "Data Location:";
            // 
            // tpConnection
            // 
            this.tpConnection.Controls.Add(this.btnRefreshCOMPorts);
            this.tpConnection.Controls.Add(this.btnConnect);
            this.tpConnection.Controls.Add(this.tabConnection);
            this.tpConnection.Location = new System.Drawing.Point(4, 22);
            this.tpConnection.Name = "tpConnection";
            this.tpConnection.Padding = new System.Windows.Forms.Padding(3);
            this.tpConnection.Size = new System.Drawing.Size(248, 221);
            this.tpConnection.TabIndex = 0;
            this.tpConnection.Text = "Connection";
            this.tpConnection.UseVisualStyleBackColor = true;
            // 
            // btnRefreshCOMPorts
            // 
            this.btnRefreshCOMPorts.Location = new System.Drawing.Point(35, 156);
            this.btnRefreshCOMPorts.Name = "btnRefreshCOMPorts";
            this.btnRefreshCOMPorts.Size = new System.Drawing.Size(115, 23);
            this.btnRefreshCOMPorts.TabIndex = 27;
            this.btnRefreshCOMPorts.Text = "Refresh COM Ports";
            this.btnRefreshCOMPorts.UseVisualStyleBackColor = true;
            // 
            // btnConnect
            // 
            this.btnConnect.Location = new System.Drawing.Point(172, 156);
            this.btnConnect.Name = "btnConnect";
            this.btnConnect.Size = new System.Drawing.Size(115, 23);
            this.btnConnect.TabIndex = 28;
            this.btnConnect.Text = "Connect";
            this.btnConnect.UseVisualStyleBackColor = true;
            // 
            // tabConnection
            // 
            this.tabConnection.Controls.Add(this.tpSensorConnection);
            this.tabConnection.Controls.Add(this.tpLaserConnection);
            this.tabConnection.Controls.Add(this.tpMotorConnection);
            this.tabConnection.Location = new System.Drawing.Point(9, 15);
            this.tabConnection.Name = "tabConnection";
            this.tabConnection.SelectedIndex = 0;
            this.tabConnection.Size = new System.Drawing.Size(314, 129);
            this.tabConnection.TabIndex = 0;
            // 
            // tpSensorConnection
            // 
            this.tpSensorConnection.Controls.Add(lblSensorBaudrate);
            this.tpSensorConnection.Controls.Add(this.cmbSensorBaudrate);
            this.tpSensorConnection.Controls.Add(lblSensorPort);
            this.tpSensorConnection.Controls.Add(this.cmbSensorPort);
            this.tpSensorConnection.Controls.Add(lblSensorType);
            this.tpSensorConnection.Controls.Add(this.cmbSensorType);
            this.tpSensorConnection.Controls.Add(lblSensorModel);
            this.tpSensorConnection.Controls.Add(this.cmbSensorModel);
            this.tpSensorConnection.Controls.Add(this.chkConnectSensor);
            this.tpSensorConnection.Location = new System.Drawing.Point(4, 22);
            this.tpSensorConnection.Name = "tpSensorConnection";
            this.tpSensorConnection.Padding = new System.Windows.Forms.Padding(3);
            this.tpSensorConnection.Size = new System.Drawing.Size(306, 103);
            this.tpSensorConnection.TabIndex = 0;
            this.tpSensorConnection.Text = "Sensor";
            this.tpSensorConnection.UseVisualStyleBackColor = true;
            // 
            // cmbSensorBaudrate
            // 
            this.cmbSensorBaudrate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSensorBaudrate.FormattingEnabled = true;
            this.cmbSensorBaudrate.Location = new System.Drawing.Point(223, 66);
            this.cmbSensorBaudrate.Name = "cmbSensorBaudrate";
            this.cmbSensorBaudrate.Size = new System.Drawing.Size(69, 21);
            this.cmbSensorBaudrate.TabIndex = 41;
            // 
            // cmbSensorPort
            // 
            this.cmbSensorPort.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSensorPort.FormattingEnabled = true;
            this.cmbSensorPort.Location = new System.Drawing.Point(68, 66);
            this.cmbSensorPort.Name = "cmbSensorPort";
            this.cmbSensorPort.Size = new System.Drawing.Size(69, 21);
            this.cmbSensorPort.TabIndex = 42;
            // 
            // cmbSensorType
            // 
            this.cmbSensorType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSensorType.FormattingEnabled = true;
            this.cmbSensorType.Location = new System.Drawing.Point(223, 39);
            this.cmbSensorType.Name = "cmbSensorType";
            this.cmbSensorType.Size = new System.Drawing.Size(69, 21);
            this.cmbSensorType.TabIndex = 39;
            // 
            // cmbSensorModel
            // 
            this.cmbSensorModel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSensorModel.FormattingEnabled = true;
            this.cmbSensorModel.Location = new System.Drawing.Point(68, 39);
            this.cmbSensorModel.Name = "cmbSensorModel";
            this.cmbSensorModel.Size = new System.Drawing.Size(69, 21);
            this.cmbSensorModel.TabIndex = 38;
            // 
            // chkConnectSensor
            // 
            this.chkConnectSensor.AutoSize = true;
            this.chkConnectSensor.Location = new System.Drawing.Point(19, 13);
            this.chkConnectSensor.Name = "chkConnectSensor";
            this.chkConnectSensor.Size = new System.Drawing.Size(112, 17);
            this.chkConnectSensor.TabIndex = 35;
            this.chkConnectSensor.Text = "Connect to sensor";
            this.chkConnectSensor.UseVisualStyleBackColor = true;
            // 
            // tpLaserConnection
            // 
            this.tpLaserConnection.Controls.Add(label3);
            this.tpLaserConnection.Controls.Add(this.comboBox1);
            this.tpLaserConnection.Controls.Add(this.comboBox2);
            this.tpLaserConnection.Controls.Add(label4);
            this.tpLaserConnection.Controls.Add(this.checkBox2);
            this.tpLaserConnection.Location = new System.Drawing.Point(4, 22);
            this.tpLaserConnection.Name = "tpLaserConnection";
            this.tpLaserConnection.Padding = new System.Windows.Forms.Padding(3);
            this.tpLaserConnection.Size = new System.Drawing.Size(306, 103);
            this.tpLaserConnection.TabIndex = 1;
            this.tpLaserConnection.Text = "Laser";
            this.tpLaserConnection.UseVisualStyleBackColor = true;
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(83, 41);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(69, 21);
            this.comboBox1.TabIndex = 46;
            // 
            // comboBox2
            // 
            this.comboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Location = new System.Drawing.Point(83, 67);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(69, 21);
            this.comboBox2.TabIndex = 45;
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Location = new System.Drawing.Point(19, 13);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(103, 17);
            this.checkBox2.TabIndex = 29;
            this.checkBox2.Text = "Connect to laser";
            this.checkBox2.UseVisualStyleBackColor = true;
            // 
            // tpMotorConnection
            // 
            this.tpMotorConnection.Controls.Add(this.comboBox3);
            this.tpMotorConnection.Controls.Add(label5);
            this.tpMotorConnection.Controls.Add(this.checkBox3);
            this.tpMotorConnection.Location = new System.Drawing.Point(4, 22);
            this.tpMotorConnection.Name = "tpMotorConnection";
            this.tpMotorConnection.Size = new System.Drawing.Size(306, 103);
            this.tpMotorConnection.TabIndex = 2;
            this.tpMotorConnection.Text = "Motor";
            this.tpMotorConnection.UseVisualStyleBackColor = true;
            // 
            // comboBox3
            // 
            this.comboBox3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox3.FormattingEnabled = true;
            this.comboBox3.Location = new System.Drawing.Point(82, 41);
            this.comboBox3.Name = "comboBox3";
            this.comboBox3.Size = new System.Drawing.Size(110, 21);
            this.comboBox3.TabIndex = 49;
            // 
            // checkBox3
            // 
            this.checkBox3.AutoSize = true;
            this.checkBox3.Location = new System.Drawing.Point(19, 13);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(107, 17);
            this.checkBox3.TabIndex = 47;
            this.checkBox3.Text = "Connect to motor";
            this.checkBox3.UseVisualStyleBackColor = true;
            // 
            // tpScanParameters
            // 
            this.tpScanParameters.Controls.Add(this.tabControl2);
            this.tpScanParameters.Location = new System.Drawing.Point(4, 22);
            this.tpScanParameters.Name = "tpScanParameters";
            this.tpScanParameters.Size = new System.Drawing.Size(248, 178);
            this.tpScanParameters.TabIndex = 1;
            this.tpScanParameters.Text = "Scan Parameters";
            this.tpScanParameters.UseVisualStyleBackColor = true;
            // 
            // tabControl2
            // 
            this.tabControl2.Controls.Add(this.tabScanTiming);
            this.tabControl2.Controls.Add(this.tabLaserDetails);
            this.tabControl2.Controls.Add(this.tabMotorDetails);
            this.tabControl2.Location = new System.Drawing.Point(9, 15);
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(314, 160);
            this.tabControl2.TabIndex = 1;
            // 
            // tabScanTiming
            // 
            this.tabScanTiming.Controls.Add(this.numericUpDown1);
            this.tabScanTiming.Controls.Add(this.numericUpDown2);
            this.tabScanTiming.Controls.Add(label10);
            this.tabScanTiming.Controls.Add(this.numDAQStop);
            this.tabScanTiming.Controls.Add(label6);
            this.tabScanTiming.Controls.Add(this.numDAQStart);
            this.tabScanTiming.Controls.Add(this.lblDAQStart);
            this.tabScanTiming.Controls.Add(lblDAQStop);
            this.tabScanTiming.Controls.Add(label7);
            this.tabScanTiming.Controls.Add(this.numLaserStart);
            this.tabScanTiming.Controls.Add(label12);
            this.tabScanTiming.Controls.Add(label11);
            this.tabScanTiming.Controls.Add(label9);
            this.tabScanTiming.Controls.Add(lblLaserStart);
            this.tabScanTiming.Location = new System.Drawing.Point(4, 22);
            this.tabScanTiming.Name = "tabScanTiming";
            this.tabScanTiming.Padding = new System.Windows.Forms.Padding(3);
            this.tabScanTiming.Size = new System.Drawing.Size(306, 134);
            this.tabScanTiming.TabIndex = 0;
            this.tabScanTiming.Text = "Scan Timing";
            this.tabScanTiming.UseVisualStyleBackColor = true;
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Location = new System.Drawing.Point(194, 101);
            this.numericUpDown1.Maximum = new decimal(new int[] {
            200000000,
            0,
            0,
            0});
            this.numericUpDown1.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(53, 20);
            this.numericUpDown1.TabIndex = 44;
            this.numericUpDown1.Value = new decimal(new int[] {
            220,
            0,
            0,
            0});
            // 
            // numericUpDown2
            // 
            this.numericUpDown2.Location = new System.Drawing.Point(66, 101);
            this.numericUpDown2.Maximum = new decimal(new int[] {
            200000000,
            0,
            0,
            0});
            this.numericUpDown2.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown2.Name = "numericUpDown2";
            this.numericUpDown2.Size = new System.Drawing.Size(56, 20);
            this.numericUpDown2.TabIndex = 42;
            this.numericUpDown2.Value = new decimal(new int[] {
            182,
            0,
            0,
            0});
            // 
            // numDAQStop
            // 
            this.numDAQStop.Location = new System.Drawing.Point(128, 65);
            this.numDAQStop.Maximum = new decimal(new int[] {
            200000000,
            0,
            0,
            0});
            this.numDAQStop.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numDAQStop.Name = "numDAQStop";
            this.numDAQStop.Size = new System.Drawing.Size(84, 20);
            this.numDAQStop.TabIndex = 39;
            this.numDAQStop.Value = new decimal(new int[] {
            220,
            0,
            0,
            0});
            // 
            // numDAQStart
            // 
            this.numDAQStart.Location = new System.Drawing.Point(128, 13);
            this.numDAQStart.Maximum = new decimal(new int[] {
            200000000,
            0,
            0,
            0});
            this.numDAQStart.Name = "numDAQStart";
            this.numDAQStart.Size = new System.Drawing.Size(84, 20);
            this.numDAQStart.TabIndex = 33;
            this.numDAQStart.Value = new decimal(new int[] {
            180,
            0,
            0,
            0});
            // 
            // lblDAQStart
            // 
            this.lblDAQStart.AutoSize = true;
            this.lblDAQStart.Location = new System.Drawing.Point(17, 16);
            this.lblDAQStart.Name = "lblDAQStart";
            this.lblDAQStart.Size = new System.Drawing.Size(82, 13);
            this.lblDAQStart.TabIndex = 32;
            this.lblDAQStart.Text = "Start DAQ after:";
            // 
            // numLaserStart
            // 
            this.numLaserStart.Location = new System.Drawing.Point(128, 40);
            this.numLaserStart.Maximum = new decimal(new int[] {
            200000000,
            0,
            0,
            0});
            this.numLaserStart.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numLaserStart.Name = "numLaserStart";
            this.numLaserStart.Size = new System.Drawing.Size(84, 20);
            this.numLaserStart.TabIndex = 36;
            this.numLaserStart.Value = new decimal(new int[] {
            182,
            0,
            0,
            0});
            // 
            // tabLaserDetails
            // 
            this.tabLaserDetails.Controls.Add(this.radRunTime);
            this.tabLaserDetails.Controls.Add(this.radPulseCount);
            this.tabLaserDetails.Controls.Add(this.numRunTime);
            this.tabLaserDetails.Controls.Add(this.numPulseCount);
            this.tabLaserDetails.Controls.Add(label8);
            this.tabLaserDetails.Controls.Add(lblGatingFreq);
            this.tabLaserDetails.Controls.Add(this.numDutyCycle);
            this.tabLaserDetails.Controls.Add(this.numGatingFreq);
            this.tabLaserDetails.Controls.Add(lblDutyCycle);
            this.tabLaserDetails.Controls.Add(label13);
            this.tabLaserDetails.Location = new System.Drawing.Point(4, 22);
            this.tabLaserDetails.Name = "tabLaserDetails";
            this.tabLaserDetails.Padding = new System.Windows.Forms.Padding(3);
            this.tabLaserDetails.Size = new System.Drawing.Size(306, 134);
            this.tabLaserDetails.TabIndex = 1;
            this.tabLaserDetails.Text = "Laser Details";
            this.tabLaserDetails.UseVisualStyleBackColor = true;
            // 
            // radRunTime
            // 
            this.radRunTime.AutoSize = true;
            this.radRunTime.Location = new System.Drawing.Point(22, 43);
            this.radRunTime.Name = "radRunTime";
            this.radRunTime.Size = new System.Drawing.Size(93, 17);
            this.radRunTime.TabIndex = 32;
            this.radRunTime.Text = "Run Time (ms)";
            this.radRunTime.UseVisualStyleBackColor = true;
            // 
            // radPulseCount
            // 
            this.radPulseCount.AutoSize = true;
            this.radPulseCount.Checked = true;
            this.radPulseCount.Location = new System.Drawing.Point(22, 17);
            this.radPulseCount.Name = "radPulseCount";
            this.radPulseCount.Size = new System.Drawing.Size(82, 17);
            this.radPulseCount.TabIndex = 33;
            this.radPulseCount.TabStop = true;
            this.radPulseCount.Text = "Pulse Count";
            this.radPulseCount.UseVisualStyleBackColor = true;
            // 
            // numRunTime
            // 
            this.numRunTime.Location = new System.Drawing.Point(126, 43);
            this.numRunTime.Maximum = new decimal(new int[] {
            32766,
            0,
            0,
            0});
            this.numRunTime.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numRunTime.Name = "numRunTime";
            this.numRunTime.Size = new System.Drawing.Size(81, 20);
            this.numRunTime.TabIndex = 24;
            this.numRunTime.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // numPulseCount
            // 
            this.numPulseCount.Location = new System.Drawing.Point(126, 17);
            this.numPulseCount.Maximum = new decimal(new int[] {
            99999,
            0,
            0,
            0});
            this.numPulseCount.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numPulseCount.Name = "numPulseCount";
            this.numPulseCount.Size = new System.Drawing.Size(81, 20);
            this.numPulseCount.TabIndex = 25;
            this.numPulseCount.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // numDutyCycle
            // 
            this.numDutyCycle.DecimalPlaces = 1;
            this.numDutyCycle.Location = new System.Drawing.Point(127, 101);
            this.numDutyCycle.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numDutyCycle.Name = "numDutyCycle";
            this.numDutyCycle.Size = new System.Drawing.Size(80, 20);
            this.numDutyCycle.TabIndex = 30;
            this.numDutyCycle.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // numGatingFreq
            // 
            this.numGatingFreq.Location = new System.Drawing.Point(127, 73);
            this.numGatingFreq.Maximum = new decimal(new int[] {
            20000,
            0,
            0,
            0});
            this.numGatingFreq.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numGatingFreq.Name = "numGatingFreq";
            this.numGatingFreq.Size = new System.Drawing.Size(80, 20);
            this.numGatingFreq.TabIndex = 27;
            this.numGatingFreq.Value = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            // 
            // tabMotorDetails
            // 
            this.tabMotorDetails.Controls.Add(this.checkBox4);
            this.tabMotorDetails.Controls.Add(this.numericUpDown3);
            this.tabMotorDetails.Controls.Add(label14);
            this.tabMotorDetails.Controls.Add(this.numericUpDown4);
            this.tabMotorDetails.Controls.Add(this.label15);
            this.tabMotorDetails.Controls.Add(label16);
            this.tabMotorDetails.Controls.Add(label17);
            this.tabMotorDetails.Controls.Add(this.numericUpDown5);
            this.tabMotorDetails.Controls.Add(label18);
            this.tabMotorDetails.Controls.Add(label19);
            this.tabMotorDetails.Location = new System.Drawing.Point(4, 22);
            this.tabMotorDetails.Name = "tabMotorDetails";
            this.tabMotorDetails.Size = new System.Drawing.Size(306, 134);
            this.tabMotorDetails.TabIndex = 2;
            this.tabMotorDetails.Text = "Motor Details";
            this.tabMotorDetails.UseVisualStyleBackColor = true;
            // 
            // checkBox4
            // 
            this.checkBox4.AutoSize = true;
            this.checkBox4.Location = new System.Drawing.Point(44, 105);
            this.checkBox4.Name = "checkBox4";
            this.checkBox4.Size = new System.Drawing.Size(90, 17);
            this.checkBox4.TabIndex = 50;
            this.checkBox4.Text = "or skip middle";
            this.checkBox4.UseVisualStyleBackColor = true;
            // 
            // numericUpDown3
            // 
            this.numericUpDown3.Location = new System.Drawing.Point(105, 70);
            this.numericUpDown3.Maximum = new decimal(new int[] {
            200000000,
            0,
            0,
            0});
            this.numericUpDown3.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown3.Name = "numericUpDown3";
            this.numericUpDown3.Size = new System.Drawing.Size(84, 20);
            this.numericUpDown3.TabIndex = 48;
            this.numericUpDown3.Value = new decimal(new int[] {
            220,
            0,
            0,
            0});
            // 
            // numericUpDown4
            // 
            this.numericUpDown4.Location = new System.Drawing.Point(105, 18);
            this.numericUpDown4.Maximum = new decimal(new int[] {
            200000000,
            0,
            0,
            0});
            this.numericUpDown4.Name = "numericUpDown4";
            this.numericUpDown4.Size = new System.Drawing.Size(84, 20);
            this.numericUpDown4.TabIndex = 42;
            this.numericUpDown4.Value = new decimal(new int[] {
            180,
            0,
            0,
            0});
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(18, 20);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(68, 13);
            this.label15.TabIndex = 41;
            this.label15.Text = "Left Position:";
            // 
            // numericUpDown5
            // 
            this.numericUpDown5.Location = new System.Drawing.Point(105, 45);
            this.numericUpDown5.Maximum = new decimal(new int[] {
            200000000,
            0,
            0,
            0});
            this.numericUpDown5.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown5.Name = "numericUpDown5";
            this.numericUpDown5.Size = new System.Drawing.Size(84, 20);
            this.numericUpDown5.TabIndex = 45;
            this.numericUpDown5.Value = new decimal(new int[] {
            182,
            0,
            0,
            0});
            // 
            // tpDataAnalysis
            // 
            this.tpDataAnalysis.Controls.Add(this.tabControl3);
            this.tpDataAnalysis.Location = new System.Drawing.Point(4, 22);
            this.tpDataAnalysis.Name = "tpDataAnalysis";
            this.tpDataAnalysis.Size = new System.Drawing.Size(248, 178);
            this.tpDataAnalysis.TabIndex = 2;
            this.tpDataAnalysis.Text = "Data Analysis";
            this.tpDataAnalysis.UseVisualStyleBackColor = true;
            // 
            // tabControl3
            // 
            this.tabControl3.Controls.Add(this.tabDataProcessing);
            this.tabControl3.Controls.Add(this.tabAnalysisParameters);
            this.tabControl3.Location = new System.Drawing.Point(8, 8);
            this.tabControl3.Name = "tabControl3";
            this.tabControl3.SelectedIndex = 0;
            this.tabControl3.Size = new System.Drawing.Size(314, 187);
            this.tabControl3.TabIndex = 2;
            // 
            // tabDataProcessing
            // 
            this.tabDataProcessing.Controls.Add(label20);
            this.tabDataProcessing.Controls.Add(this.numTruncateFirst);
            this.tabDataProcessing.Controls.Add(label21);
            this.tabDataProcessing.Controls.Add(label22);
            this.tabDataProcessing.Controls.Add(this.numTruncateLast);
            this.tabDataProcessing.Controls.Add(label23);
            this.tabDataProcessing.Controls.Add(this.numMaxPeakWidth);
            this.tabDataProcessing.Controls.Add(this.numPeakHysteresis);
            this.tabDataProcessing.Controls.Add(lblMaxPeakWidth);
            this.tabDataProcessing.Controls.Add(lblPeakHysteresis);
            this.tabDataProcessing.Controls.Add(this.numPeakThreshold);
            this.tabDataProcessing.Controls.Add(lblPeakThreshold);
            this.tabDataProcessing.Controls.Add(this.chkFilterPeaks);
            this.tabDataProcessing.Location = new System.Drawing.Point(4, 22);
            this.tabDataProcessing.Name = "tabDataProcessing";
            this.tabDataProcessing.Padding = new System.Windows.Forms.Padding(3);
            this.tabDataProcessing.Size = new System.Drawing.Size(306, 161);
            this.tabDataProcessing.TabIndex = 0;
            this.tabDataProcessing.Text = "Data Processing";
            this.tabDataProcessing.UseVisualStyleBackColor = true;
            // 
            // numTruncateFirst
            // 
            this.numTruncateFirst.Location = new System.Drawing.Point(101, 11);
            this.numTruncateFirst.Name = "numTruncateFirst";
            this.numTruncateFirst.Size = new System.Drawing.Size(58, 20);
            this.numTruncateFirst.TabIndex = 60;
            this.numTruncateFirst.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // numTruncateLast
            // 
            this.numTruncateLast.Location = new System.Drawing.Point(101, 31);
            this.numTruncateLast.Name = "numTruncateLast";
            this.numTruncateLast.Size = new System.Drawing.Size(58, 20);
            this.numTruncateLast.TabIndex = 63;
            this.numTruncateLast.Value = new decimal(new int[] {
            20,
            0,
            0,
            0});
            // 
            // numMaxPeakWidth
            // 
            this.numMaxPeakWidth.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numMaxPeakWidth.Location = new System.Drawing.Point(156, 115);
            this.numMaxPeakWidth.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.numMaxPeakWidth.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numMaxPeakWidth.Name = "numMaxPeakWidth";
            this.numMaxPeakWidth.Size = new System.Drawing.Size(59, 20);
            this.numMaxPeakWidth.TabIndex = 56;
            this.numMaxPeakWidth.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // numPeakHysteresis
            // 
            this.numPeakHysteresis.DecimalPlaces = 1;
            this.numPeakHysteresis.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numPeakHysteresis.Location = new System.Drawing.Point(156, 94);
            this.numPeakHysteresis.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.numPeakHysteresis.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numPeakHysteresis.Name = "numPeakHysteresis";
            this.numPeakHysteresis.Size = new System.Drawing.Size(59, 20);
            this.numPeakHysteresis.TabIndex = 55;
            this.numPeakHysteresis.Value = new decimal(new int[] {
            20,
            0,
            0,
            0});
            // 
            // numPeakThreshold
            // 
            this.numPeakThreshold.DecimalPlaces = 1;
            this.numPeakThreshold.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numPeakThreshold.Location = new System.Drawing.Point(156, 72);
            this.numPeakThreshold.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.numPeakThreshold.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numPeakThreshold.Name = "numPeakThreshold";
            this.numPeakThreshold.Size = new System.Drawing.Size(59, 20);
            this.numPeakThreshold.TabIndex = 57;
            this.numPeakThreshold.Value = new decimal(new int[] {
            40,
            0,
            0,
            0});
            // 
            // chkFilterPeaks
            // 
            this.chkFilterPeaks.AutoSize = true;
            this.chkFilterPeaks.Checked = true;
            this.chkFilterPeaks.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkFilterPeaks.Location = new System.Drawing.Point(22, 59);
            this.chkFilterPeaks.Name = "chkFilterPeaks";
            this.chkFilterPeaks.Size = new System.Drawing.Size(81, 17);
            this.chkFilterPeaks.TabIndex = 58;
            this.chkFilterPeaks.Text = "Filter Peaks";
            this.chkFilterPeaks.UseVisualStyleBackColor = true;
            // 
            // tabAnalysisParameters
            // 
            this.tabAnalysisParameters.Controls.Add(this.numPitPoints);
            this.tabAnalysisParameters.Controls.Add(lblPitPoints);
            this.tabAnalysisParameters.Controls.Add(this.numSubstrateRoughness);
            this.tabAnalysisParameters.Controls.Add(lblSubstrateRoughness);
            this.tabAnalysisParameters.Controls.Add(this.numCompressionFactor);
            this.tabAnalysisParameters.Controls.Add(this.numSmoothingWidth);
            this.tabAnalysisParameters.Controls.Add(this.numMinPitWidth);
            this.tabAnalysisParameters.Controls.Add(lblCompressionFactor);
            this.tabAnalysisParameters.Controls.Add(this.numThresholdParam);
            this.tabAnalysisParameters.Controls.Add(lblSmoothingWidth);
            this.tabAnalysisParameters.Controls.Add(lblMinPitWidth);
            this.tabAnalysisParameters.Controls.Add(lblThresholdParam);
            this.tabAnalysisParameters.Location = new System.Drawing.Point(4, 22);
            this.tabAnalysisParameters.Name = "tabAnalysisParameters";
            this.tabAnalysisParameters.Padding = new System.Windows.Forms.Padding(3);
            this.tabAnalysisParameters.Size = new System.Drawing.Size(306, 161);
            this.tabAnalysisParameters.TabIndex = 1;
            this.tabAnalysisParameters.Text = "Analysis Parameters";
            this.tabAnalysisParameters.UseVisualStyleBackColor = true;
            // 
            // numPitPoints
            // 
            this.numPitPoints.Location = new System.Drawing.Point(153, 132);
            this.numPitPoints.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.numPitPoints.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numPitPoints.Name = "numPitPoints";
            this.numPitPoints.Size = new System.Drawing.Size(67, 20);
            this.numPitPoints.TabIndex = 71;
            this.numPitPoints.Value = new decimal(new int[] {
            4,
            0,
            0,
            0});
            // 
            // numSubstrateRoughness
            // 
            this.numSubstrateRoughness.DecimalPlaces = 1;
            this.numSubstrateRoughness.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numSubstrateRoughness.Location = new System.Drawing.Point(153, 11);
            this.numSubstrateRoughness.Maximum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numSubstrateRoughness.Minimum = new decimal(new int[] {
            5,
            0,
            0,
            -2147483648});
            this.numSubstrateRoughness.Name = "numSubstrateRoughness";
            this.numSubstrateRoughness.Size = new System.Drawing.Size(67, 20);
            this.numSubstrateRoughness.TabIndex = 69;
            // 
            // numCompressionFactor
            // 
            this.numCompressionFactor.DecimalPlaces = 3;
            this.numCompressionFactor.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.numCompressionFactor.Location = new System.Drawing.Point(153, 105);
            this.numCompressionFactor.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numCompressionFactor.Name = "numCompressionFactor";
            this.numCompressionFactor.Size = new System.Drawing.Size(67, 20);
            this.numCompressionFactor.TabIndex = 66;
            this.numCompressionFactor.Value = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            // 
            // numSmoothingWidth
            // 
            this.numSmoothingWidth.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numSmoothingWidth.Location = new System.Drawing.Point(153, 56);
            this.numSmoothingWidth.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numSmoothingWidth.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numSmoothingWidth.Name = "numSmoothingWidth";
            this.numSmoothingWidth.Size = new System.Drawing.Size(67, 20);
            this.numSmoothingWidth.TabIndex = 67;
            this.numSmoothingWidth.Value = new decimal(new int[] {
            65,
            0,
            0,
            0});
            // 
            // numMinPitWidth
            // 
            this.numMinPitWidth.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numMinPitWidth.Location = new System.Drawing.Point(153, 80);
            this.numMinPitWidth.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numMinPitWidth.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numMinPitWidth.Name = "numMinPitWidth";
            this.numMinPitWidth.Size = new System.Drawing.Size(67, 20);
            this.numMinPitWidth.TabIndex = 64;
            this.numMinPitWidth.Value = new decimal(new int[] {
            15,
            0,
            0,
            0});
            // 
            // numThresholdParam
            // 
            this.numThresholdParam.DecimalPlaces = 3;
            this.numThresholdParam.Increment = new decimal(new int[] {
            5,
            0,
            0,
            131072});
            this.numThresholdParam.Location = new System.Drawing.Point(154, 33);
            this.numThresholdParam.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numThresholdParam.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            196608});
            this.numThresholdParam.Name = "numThresholdParam";
            this.numThresholdParam.Size = new System.Drawing.Size(67, 20);
            this.numThresholdParam.TabIndex = 65;
            this.numThresholdParam.Value = new decimal(new int[] {
            65,
            0,
            0,
            131072});
            // 
            // tpOther
            // 
            this.tpOther.Controls.Add(this.txtFilenamePattern);
            this.tpOther.Controls.Add(lblFilenamePattern);
            this.tpOther.Controls.Add(this.btnChooseDataFolder);
            this.tpOther.Controls.Add(this.txtDataFolder);
            this.tpOther.Controls.Add(lblChooseDataFolder);
            this.tpOther.Location = new System.Drawing.Point(4, 22);
            this.tpOther.Name = "tpOther";
            this.tpOther.Size = new System.Drawing.Size(248, 178);
            this.tpOther.TabIndex = 3;
            this.tpOther.Text = "Other";
            this.tpOther.UseVisualStyleBackColor = true;
            // 
            // txtFilenamePattern
            // 
            this.txtFilenamePattern.Location = new System.Drawing.Point(119, 60);
            this.txtFilenamePattern.Name = "txtFilenamePattern";
            this.txtFilenamePattern.Size = new System.Drawing.Size(117, 20);
            this.txtFilenamePattern.TabIndex = 34;
            this.txtFilenamePattern.Text = "scan {nnn}.csv";
            // 
            // btnChooseDataFolder
            // 
            this.btnChooseDataFolder.Location = new System.Drawing.Point(243, 20);
            this.btnChooseDataFolder.Name = "btnChooseDataFolder";
            this.btnChooseDataFolder.Size = new System.Drawing.Size(57, 23);
            this.btnChooseDataFolder.TabIndex = 32;
            this.btnChooseDataFolder.Text = "Choose";
            this.btnChooseDataFolder.UseVisualStyleBackColor = true;
            // 
            // txtDataFolder
            // 
            this.txtDataFolder.Location = new System.Drawing.Point(119, 23);
            this.txtDataFolder.Name = "txtDataFolder";
            this.txtDataFolder.ReadOnly = true;
            this.txtDataFolder.Size = new System.Drawing.Size(117, 20);
            this.txtDataFolder.TabIndex = 31;
            this.txtDataFolder.Text = "c:\\";
            // 
            // DFM_Settings_Form_old
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(289, 319);
            this.Name = "DFM_Settings_Form_old";
            this.Text = "DFM_Settings_Form";
            this.tabSettings.ResumeLayout(false);
            this.controlButtonsContainer.Panel1.ResumeLayout(false);
            this.controlButtonsContainer.ResumeLayout(false);
            this.outerContainer.Panel2.ResumeLayout(false);
            this.outerContainer.ResumeLayout(false);
            this.tpConnection.ResumeLayout(false);
            this.tabConnection.ResumeLayout(false);
            this.tpSensorConnection.ResumeLayout(false);
            this.tpSensorConnection.PerformLayout();
            this.tpLaserConnection.ResumeLayout(false);
            this.tpLaserConnection.PerformLayout();
            this.tpMotorConnection.ResumeLayout(false);
            this.tpMotorConnection.PerformLayout();
            this.tpScanParameters.ResumeLayout(false);
            this.tabControl2.ResumeLayout(false);
            this.tabScanTiming.ResumeLayout(false);
            this.tabScanTiming.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numDAQStop)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numDAQStart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numLaserStart)).EndInit();
            this.tabLaserDetails.ResumeLayout(false);
            this.tabLaserDetails.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numRunTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPulseCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numDutyCycle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numGatingFreq)).EndInit();
            this.tabMotorDetails.ResumeLayout(false);
            this.tabMotorDetails.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown5)).EndInit();
            this.tpDataAnalysis.ResumeLayout(false);
            this.tabControl3.ResumeLayout(false);
            this.tabDataProcessing.ResumeLayout(false);
            this.tabDataProcessing.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numTruncateFirst)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numTruncateLast)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numMaxPeakWidth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPeakHysteresis)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPeakThreshold)).EndInit();
            this.tabAnalysisParameters.ResumeLayout(false);
            this.tabAnalysisParameters.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numPitPoints)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSubstrateRoughness)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numCompressionFactor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSmoothingWidth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numMinPitWidth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numThresholdParam)).EndInit();
            this.tpOther.ResumeLayout(false);
            this.tpOther.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabPage tpConnection;
        private System.Windows.Forms.TabControl tabConnection;
        private System.Windows.Forms.TabPage tpSensorConnection;
        private System.Windows.Forms.TabPage tpLaserConnection;
        private System.Windows.Forms.TabPage tpScanParameters;
        private System.Windows.Forms.TabPage tpDataAnalysis;
        private System.Windows.Forms.TabPage tpOther;
        private System.Windows.Forms.TabPage tpMotorConnection;
        private System.Windows.Forms.ComboBox cmbSensorPort;
        private System.Windows.Forms.ComboBox cmbSensorBaudrate;
        private System.Windows.Forms.ComboBox cmbSensorType;
        private System.Windows.Forms.ComboBox cmbSensorModel;
        private System.Windows.Forms.CheckBox chkConnectSensor;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.ComboBox comboBox3;
        private System.Windows.Forms.CheckBox checkBox3;
        private System.Windows.Forms.Button btnConnect;
        private System.Windows.Forms.Button btnRefreshCOMPorts;
        private System.Windows.Forms.TabControl tabControl2;
        private System.Windows.Forms.TabPage tabScanTiming;
        private System.Windows.Forms.TabPage tabLaserDetails;
        private System.Windows.Forms.TabPage tabMotorDetails;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.NumericUpDown numericUpDown2;
        private System.Windows.Forms.NumericUpDown numDAQStop;
        private System.Windows.Forms.NumericUpDown numDAQStart;
        private System.Windows.Forms.Label lblDAQStart;
        private System.Windows.Forms.NumericUpDown numLaserStart;
        private System.Windows.Forms.RadioButton radRunTime;
        private System.Windows.Forms.RadioButton radPulseCount;
        private System.Windows.Forms.NumericUpDown numRunTime;
        private System.Windows.Forms.NumericUpDown numPulseCount;
        private System.Windows.Forms.NumericUpDown numDutyCycle;
        private System.Windows.Forms.NumericUpDown numGatingFreq;
        private System.Windows.Forms.CheckBox checkBox4;
        private System.Windows.Forms.NumericUpDown numericUpDown3;
        private System.Windows.Forms.NumericUpDown numericUpDown4;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.NumericUpDown numericUpDown5;
        private System.Windows.Forms.TabControl tabControl3;
        private System.Windows.Forms.TabPage tabDataProcessing;
        private System.Windows.Forms.TabPage tabAnalysisParameters;
        private System.Windows.Forms.NumericUpDown numTruncateFirst;
        private System.Windows.Forms.NumericUpDown numTruncateLast;
        private System.Windows.Forms.NumericUpDown numMaxPeakWidth;
        private System.Windows.Forms.NumericUpDown numPeakHysteresis;
        private System.Windows.Forms.NumericUpDown numPeakThreshold;
        private System.Windows.Forms.CheckBox chkFilterPeaks;
        private System.Windows.Forms.NumericUpDown numPitPoints;
        private System.Windows.Forms.NumericUpDown numSubstrateRoughness;
        private System.Windows.Forms.NumericUpDown numCompressionFactor;
        private System.Windows.Forms.NumericUpDown numSmoothingWidth;
        private System.Windows.Forms.NumericUpDown numMinPitWidth;
        private System.Windows.Forms.NumericUpDown numThresholdParam;
        private System.Windows.Forms.TextBox txtFilenamePattern;
        private System.Windows.Forms.Button btnChooseDataFolder;
        private System.Windows.Forms.TextBox txtDataFolder;
    }
}