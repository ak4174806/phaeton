﻿using System;
using System.Collections.Generic;
using System.Text;

using System.Configuration;
using DFM.Settings;
using System.IO;
using System.Xml.Serialization;
using System.Security.Cryptography;

using NLog;

namespace DFM
{
    public class UserSettings
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        // see http://stackoverflow.com/questions/965042/c-sharp-serializing-deserializing-a-des-encrypted-file-from-a-stream for encryption

        public static string SettingsFileName = "{0}.xml";

        private DESCryptoServiceProvider key;

        public UserSettings(string groupName)
        {
            key = new DESCryptoServiceProvider();
            Filename = String.Format(UserSettings.SettingsFileName, groupName);
            Load();
        }

        private string filename;
        protected string Filename
        {
            get { return filename; }
            set { filename = value; }
        }

        private SettingsGroup settings;
        public SettingsGroup Settings
        {
            get { return settings; }
            set { settings = value; }
        }               

        public bool Save(out Exception ex)
        {
            ex = null;            
            try
            {
                Type[] types = BaseSetting.SettingTypes.ToArray();

                using (FileStream fs = File.Open(filename, FileMode.Create))
                {
                    using (CryptoStream cs = new CryptoStream(fs, key.CreateEncryptor(Encoding.ASCII.GetBytes("12345678"), Encoding.ASCII.GetBytes("InitVector")), CryptoStreamMode.Write))
                    {
                        XmlSerializer serializer = new XmlSerializer(typeof(SettingsGroup), types);
                        serializer.Serialize(cs, Settings);
                    }
                }
                return true;
            }
            catch (Exception ex1)
            {
                logger.ErrorException("Error saving user settings", ex1);
                ex = ex1;
                return false;
            }
        }

        public bool Load(out Exception ex)
        {
            ex = null;

            if (!File.Exists(Filename))
            {
                Settings = null;
                ex = new ArgumentException("File: " + Filename + " doesn't exist");
                logger.Warn("File: '" + Filename + "' doesn't exist");
                return false;
            }

            try
            {
                Type[] types = BaseSetting.SettingTypes.ToArray();

                using (FileStream fs = new FileStream(Filename, FileMode.Open))
                {
                    using (CryptoStream cs = new CryptoStream(fs, key.CreateDecryptor(Encoding.ASCII.GetBytes("12345678"), Encoding.ASCII.GetBytes("InitVector")), CryptoStreamMode.Read))
                    {
                        XmlSerializer serializer = new XmlSerializer(typeof(SettingsGroup), types);
                        Settings = (SettingsGroup)serializer.Deserialize(cs);
                    }
                }
                return true;
            }
            catch (Exception ex1)
            {
                logger.ErrorException("Error loading user settings", ex1);
                ex = ex1;
                return false;
            }            
        }

        public bool Save()
        {
            Exception ex;
            return Save(out ex);
        }

        public bool Load()
        {
            Exception ex;
            return Load(out ex);
        }
    }
}
