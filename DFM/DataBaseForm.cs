﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace DFM
{
    public partial class DataBaseForm : Form
    {
        public DataBaseForm()
        {
            InitializeComponent();
           // this.Opacity = 0;
           // this.Show();
           // tmrFade.Enabled = true;
            this.FormClosing += new FormClosingEventHandler(DataBaseFrm_closing);
        }

        
        void DataBaseFrm_closing(object sender, FormClosingEventArgs e)
        {
            this.Hide();
            e.Cancel = true;
        }

        private void DataBaseForm_Load(object sender, EventArgs e)
        {

        }

        private void tmrFade_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            this.Opacity += 0.10;
            if (this.Opacity >= .95)
            {
                this.Opacity = 1;
                tmrFade.Enabled = false;
            }
        }
    }
}
