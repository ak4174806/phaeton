﻿using System;
using System.Collections.Generic;
using System.Text;

using System.Text.RegularExpressions;
using System.IO;

using PaintAnalysis.Common;
using DFM;

namespace DFM.Scans
{
    class ScanManager
    {
        #region Properties

        // folder that file is stored in, trailing slash
        private string path;
        public string Path
        {
            get { return path; }
            set { path = value.TrimEnd('/', '\\') + "\\"; }
        }

        // template for file
        private string template;
        public string Template
        {
            get { return template; }
            set { template = value; }
        }

        #endregion

        Regex rScanId;

        public ScanManager(string path, string template)
        {
            this.Path = path;
            this.Template = template;

            rScanId = new Regex(@"\{n+\}", RegexOptions.IgnoreCase); // for scan id
        }

        public int[] GetScanIds()
        {
            Set<int> ids = new Set<int>();

            Regex rFileTest = new Regex("^" + rScanId.Replace(Template, @"(?<id>\d+)") + "$", RegexOptions.IgnoreCase);

            string fileFilter = rScanId.Replace(Template, "*");

            // returns entire paths, not just filenames
            string[] matchedFiles = Directory.GetFiles(Path, fileFilter);

            int pathLength = Path.Length;
            Match m; int id;           
            foreach (string filePath in matchedFiles)
            {
                // need to ignore path
                m = rFileTest.Match(filePath.Substring(pathLength));
                if (m.Success)
                {
                    id = Convert.ToInt32(m.Groups["id"].Value);
                    ids.Add(id);
                }
            }

            int[] ids_a = new int[ids.Count];
            for (int i = 0; i < ids.Count; i++)
            {
                ids_a[i] = ids[i];
            }            
            Array.Sort(ids_a);
            return ids_a;
        }

        public int GetLastScanId()
        {
            int[] ids = GetScanIds(); // can rely on it being sorted
            return (ids.Length > 0 ? ids[ids.Length - 1] : -1);
        }
                
        public int GetNextScanId()
        {
            int last = GetLastScanId();
            return (last == -1 ? 1 : last + 1);
        }
    }
}
