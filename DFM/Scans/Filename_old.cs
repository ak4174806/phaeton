﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PVS;
using System.Text.RegularExpressions;
using System.IO;

namespace PVS
{
    class Filename_old
    {
        /*public Filename_old(string folder, string template)
        {
            Folder = folder;
            Template = template;
        }

        public static string ApplyType(string template, PVSAnalysis.ScanTypes type)
        {
            string aType = "";
            if (type == PVSAnalysis.ScanTypes.Wet) aType = "Wet";
            else if (type == PVSAnalysis.ScanTypes.Dry) aType = "Dry";

            // replace types
            template = template.Replace("{type}", aType.ToLower());
            template = template.Replace("{Type}", aType);
            template = template.Replace("{TYPE}", aType.ToUpper());

            return template;
        }

        public static string ApplyID(string template, int id)
        {
            Regex r1 = new Regex(@"\{(n+)\}"); // for id
            Match m1 = r1.Match(template);
            int digits;
            if (m1.Success)
            {
                digits = m1.Groups[1].Length;
                template = r1.Replace(template, id.ToString().PadLeft(digits, '0'));
            }
            return template;
        }

        public static string ApplyScan(string template, int scan)
        {
            Regex r2 = new Regex(@"\{(x+)\}"); // for id
            Match m2 = r2.Match(template);
            int digits;
            if (m2.Success)
            {
                digits = m2.Groups[1].Length;
                template = r2.Replace(template, scan.ToString().PadLeft(digits, '0'));
            }
            return template;
        }


        #region Basic Properties

        // for caching
        bool changed = true;

        // folder file is stored in, trailing slash
        private string folder;
        public string Folder
        {
            get { return folder; }
            set {
                folder = value.TrimEnd('/', '\\') + "\\";
                changed = true;
            }
        }

        // template for file
        private string template;
        public string Template
        {
            get { return template; }
            set { template = value; changed = true; }
        }

        // id of file
        private int id;
        public int ID
        {
            get { return id; }
            set { id = value; changed = true; }
        }

        // whether it is wet or dry
        private PVSAnalysis.ScanTypes type;
        public PVSAnalysis.ScanTypes Type 
        {
            get { return type; }
            set { type = value; changed = true; }
        }

        // which scan number it is ie: wet-1 or wet-2
        private int scan;
        public int Scan
        {
            get { return scan; }
            set { scan = value; changed = true; }
        }

        #endregion

        private string cached;
        public string Name
        {
            get
            {
                if (!changed) return cached;

                string output = Filename_old.ApplyType(Template, Type);
                output = Filename_old.ApplyID(output, id);
                output = Filename_old.ApplyScan(output, scan);
                
                cached = output;
                changed = false;

                return output;
            }
        }

        // the complete path to the file if we were creating it
        public string Path
        {
            get
            {
                return Folder + Name;
            }
        }
        

        /*
        // finds paths with different paddings to those specified in template
        public string FindName
        {
            get
            {
                if (File.Exists(Path))
                {
                    return Name;
                }

                string output = Template;

                Regex r1 = new Regex(@"\{(n+)\}"); // for id
                Regex r2 = new Regex(@"\{(x+)\}"); // for scan

                string fileFilter = r1.Replace(output, "*");
                fileFilter = r2.Replace(fileFilter, "*");

                int id, scan;
                Match m1, m2;
                foreach (string file in Directory.GetFiles(folder, folder))
                {
                    m1 = r1.Match(file);
                    if (m1.Success)
                    {

                    }
                }
            }
        }

        public string FindPath
        {
            get
            {
                return Folder + FindName;
            }
        }*/
    }
}
