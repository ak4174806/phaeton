﻿using System;
using System.Collections.Generic;
using System.Text;

using System.Text.RegularExpressions;
using System.IO;

using DFM;

namespace DFM.Scans
{
    public class ScanFile : IComparable<ScanFile>
    {
        #region Basic Properties
        // for caching
        bool changed = true;

        // folder that file is stored in, trailing slash
        private string path;
        public string Path
        {
            get { return path; }
            set
            {
                path = value.TrimEnd('/', '\\') + "\\";
                changed = true;
            }
        }

        // template for file
        private string template;
        public string Template
        {
            get { return template; }
            set { template = value; changed = true; }
        }

        // id of file
        private int scanId;
        public int ScanId
        {
            get { return scanId; }
            set { scanId = value; changed = true; }
        }

        #endregion

        Regex rScanId; // for scan id
        Regex rFileTest; // for testing files
        
        public ScanFile(string path, string template)
        {
            this.Path = path;
            this.Template = template;

            rScanId = new Regex(@"\{n+\}", RegexOptions.IgnoreCase); // for scan id
            rFileTest = new Regex("^" + rScanId.Replace(Template, @"(?<id>\d+)") + "$", RegexOptions.IgnoreCase);
        }

        public ScanFile(string path, string template, int scanId)
            : this(path, template)
        {
            this.ScanId = scanId;
        }

        public ScanFile(string path, string template, string templateFilename)
            : this(path, template)
        {
            this.TemplateFilename = templateFilename;
        }


        // the externally set filename or ""
        private string actualFilename = "";
        /// <summary>
        /// Returns either an externally set filename applied via TemplateFilename, or TemplateFilename
        /// </summary>
        public string Filename
        {
            get 
            { 
                return (actualFilename != "" ? actualFilename : TemplateFilename);
            }
        }
        
        // cached version of applied template
        private string cachedName;
        /// <summary>
        /// Returns the template with scan parameters applied, no checks for if file exists in Path.
        /// When setting, parses filename and updates actualFilename, ScanID, ScanNumber and ScanType, will throw an exception if invalid
        /// </summary>
        public string TemplateFilename
        {
            get
            {
                if (!changed) return cachedName;
                
                string output = ScanFile.ApplyScanId(Template, ScanId);

                cachedName = output;
                changed = false;

                return output;
            }
            set
            {
                // automatically parse and fill parameters                
                Match m = rFileTest.Match(value);
                
                int id;
                if (m.Success)
                {
                    id = Convert.ToInt32(m.Groups["id"].Value);
                    ScanId = id;
                    actualFilename = value;
                }
                else
                {
                    throw new ApplicationException("Passed filename does not match template for TemplateFilename setter");
                }
            }
        }
        
        /// <summary>
        /// Finds a file in Path that matches, or returns ""
        /// Tries (1) the cached name (2) the template (3) searching the Path for a file that matches (padding issues)
        /// </summary>
        /// <returns></returns>
        public string FindFilename()
        {
            string filename = Filename;
            if (filename != "")
            {
                // check externally applied filename for if it exists
                if (File.Exists(Path + filename))
                {
                    return filename;
                }

                // check template filename for if it exists - necessary even though Filename will return TemplateFilename if actualFilename = "", as actualFilename might be an invalid file
                // not inefficient as TemplateFilename will cache result
                filename = TemplateFilename;
                if (File.Exists(Path + filename))
                {
                    return filename;
                }
            }

            // both Filename and TemplateFilename are invalid files
            // try searching, as sometimes TemplateFilename will have padding/case issues

            string fileFilter = rScanId.Replace(Template, "*");
         
            // returns entire paths, not just filenames
            string[] matchedFiles = Directory.GetFiles(Path, fileFilter);
            
            int pathLength = Path.Length;
            Match m;
            int id;
            string file;
            foreach (string filePath in matchedFiles)
            {
                // need to ignore path
                file = filePath.Substring(pathLength);
                m = rFileTest.Match(file);
                if (m.Success)
                {                    
                    id = Convert.ToInt32(m.Groups["id"].Value);
                    
                    if (id == ScanId)
                    {
                        return file;
                    }
                }
            }
            
            // error - could not find a matching file
            return "";
        }


        /// <summary>
        /// Implement comparisons, order by scan id, then scan number, then scan type
        /// </summary>
        /// <param name="scanFile2"></param>
        /// <returns></returns>
        public int CompareTo(ScanFile scanFile2)
        {
            if (this.ScanId < scanFile2.ScanId) return -1;
            if (this.ScanId > scanFile2.ScanId) return 1;
            
            return 1;
        }


        public static string ApplyScanId(string template, int scanId)
        {
            Regex r1 = new Regex(@"\{(n+)\}", RegexOptions.IgnoreCase); // for scan id
            Match m1 = r1.Match(template);
            int digits;
            if (m1.Success)
            {
                digits = m1.Groups[1].Length;
                template = r1.Replace(template, scanId.ToString().PadLeft(digits, '0'));
            }
            return template;
        }        
    }
}
