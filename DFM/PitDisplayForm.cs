﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace DFM
{
    public partial class PitDisplayForm : Form
    {
        public PitDisplayForm()
        {
            InitializeComponent();

            Load += new EventHandler(PitDisplayForm_Load);
            this.FormClosing += new FormClosingEventHandler(PitDispFrm_closing);
        }


        void PitDispFrm_closing(object sender, FormClosingEventArgs e)
        {
            this.Hide();
            e.Cancel = true;
        }

        void PitDisplayForm_Load(object sender, EventArgs e)
        {
            Heading = Heading;
        }

        public PitDisplayForm(double[] pits, string heading)
            : this()
        {
            Load += new EventHandler(delegate(object sender, EventArgs e)
            {
                Pits = pits;
                Heading = heading;
            });
            this.Show();
        }

        public string Heading
        {
            get { return lblHeading.Text; }
            set { Text = lblHeading.Text = value; }
        }

        private double[] pits = null;
        public double[] Pits
        {
            get { return pits; }
            set
            {
                pits = value;
                txtData.Clear();
                foreach (double pit in pits)
                    txtData.AppendText(pit.ToString("0.0000") + Environment.NewLine);
            }
        }
    }
}
