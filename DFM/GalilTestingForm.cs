﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace DFM
{
    public partial class GalilTestingForm : BaseGalilForm
    {
        public GalilTestingForm()
        {
            InitializeComponent();
        }

        private void btnConnect_Click(object sender, EventArgs e)
        {
            Connect();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Setup();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            MoveTo();
        }
    }
}
