﻿namespace DFM
{
    partial class SettingsTestForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.chkSetting = new System.Windows.Forms.CheckBox();
            this.chkBindingEnabled = new System.Windows.Forms.CheckBox();
            this.txtDisplay = new System.Windows.Forms.TextBox();
            this.chkFollower = new System.Windows.Forms.CheckBox();
            this.txtFollower = new System.Windows.Forms.TextBox();
            this.txtEnumValue = new System.Windows.Forms.TextBox();
            this.txtEnumFollower = new System.Windows.Forms.TextBox();
            this.cmbEnum2 = new System.Windows.Forms.ComboBox();
            this.txtEnum2Follower = new System.Windows.Forms.TextBox();
            this.txtEnum3Follower = new System.Windows.Forms.TextBox();
            this.cmbEnum3 = new System.Windows.Forms.ComboBox();
            this.radApple = new System.Windows.Forms.RadioButton();
            this.radOrange = new System.Windows.Forms.RadioButton();
            this.radBanana = new System.Windows.Forms.RadioButton();
            this.txtEnum4Follower = new System.Windows.Forms.TextBox();
            this.chkEnableX = new System.Windows.Forms.CheckBox();
            this.txtHybridX = new System.Windows.Forms.TextBox();
            this.txtHybridFollower = new System.Windows.Forms.TextBox();
            this.cmbStringArray = new System.Windows.Forms.ComboBox();
            this.txtStringArrayFollower = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // chkSetting
            // 
            this.chkSetting.AutoSize = true;
            this.chkSetting.Location = new System.Drawing.Point(12, 12);
            this.chkSetting.Name = "chkSetting";
            this.chkSetting.Size = new System.Drawing.Size(59, 17);
            this.chkSetting.TabIndex = 0;
            this.chkSetting.Text = "Setting";
            this.chkSetting.UseVisualStyleBackColor = true;
            // 
            // chkBindingEnabled
            // 
            this.chkBindingEnabled.AutoSize = true;
            this.chkBindingEnabled.Location = new System.Drawing.Point(12, 44);
            this.chkBindingEnabled.Name = "chkBindingEnabled";
            this.chkBindingEnabled.Size = new System.Drawing.Size(103, 17);
            this.chkBindingEnabled.TabIndex = 2;
            this.chkBindingEnabled.Text = "Binding Enabled";
            this.chkBindingEnabled.UseVisualStyleBackColor = true;
            this.chkBindingEnabled.CheckedChanged += new System.EventHandler(this.chkBindingEnabled_CheckedChanged);
            // 
            // txtDisplay
            // 
            this.txtDisplay.Location = new System.Drawing.Point(152, 23);
            this.txtDisplay.Name = "txtDisplay";
            this.txtDisplay.ReadOnly = true;
            this.txtDisplay.Size = new System.Drawing.Size(100, 20);
            this.txtDisplay.TabIndex = 3;
            // 
            // chkFollower
            // 
            this.chkFollower.AutoSize = true;
            this.chkFollower.Location = new System.Drawing.Point(17, 64);
            this.chkFollower.Name = "chkFollower";
            this.chkFollower.Size = new System.Drawing.Size(62, 17);
            this.chkFollower.TabIndex = 4;
            this.chkFollower.Text = "follower";
            this.chkFollower.UseVisualStyleBackColor = true;
            // 
            // txtFollower
            // 
            this.txtFollower.Location = new System.Drawing.Point(152, 61);
            this.txtFollower.Name = "txtFollower";
            this.txtFollower.Size = new System.Drawing.Size(100, 20);
            this.txtFollower.TabIndex = 5;
            // 
            // txtEnumValue
            // 
            this.txtEnumValue.Location = new System.Drawing.Point(17, 153);
            this.txtEnumValue.Name = "txtEnumValue";
            this.txtEnumValue.Size = new System.Drawing.Size(100, 20);
            this.txtEnumValue.TabIndex = 6;
            // 
            // txtEnumFollower
            // 
            this.txtEnumFollower.Location = new System.Drawing.Point(17, 179);
            this.txtEnumFollower.Name = "txtEnumFollower";
            this.txtEnumFollower.Size = new System.Drawing.Size(100, 20);
            this.txtEnumFollower.TabIndex = 7;
            // 
            // cmbEnum2
            // 
            this.cmbEnum2.FormattingEnabled = true;
            this.cmbEnum2.Location = new System.Drawing.Point(152, 99);
            this.cmbEnum2.Name = "cmbEnum2";
            this.cmbEnum2.Size = new System.Drawing.Size(82, 21);
            this.cmbEnum2.TabIndex = 8;
            // 
            // txtEnum2Follower
            // 
            this.txtEnum2Follower.Location = new System.Drawing.Point(152, 126);
            this.txtEnum2Follower.Name = "txtEnum2Follower";
            this.txtEnum2Follower.Size = new System.Drawing.Size(100, 20);
            this.txtEnum2Follower.TabIndex = 9;
            // 
            // txtEnum3Follower
            // 
            this.txtEnum3Follower.Location = new System.Drawing.Point(152, 191);
            this.txtEnum3Follower.Name = "txtEnum3Follower";
            this.txtEnum3Follower.Size = new System.Drawing.Size(100, 20);
            this.txtEnum3Follower.TabIndex = 11;
            // 
            // cmbEnum3
            // 
            this.cmbEnum3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbEnum3.FormattingEnabled = true;
            this.cmbEnum3.Location = new System.Drawing.Point(152, 164);
            this.cmbEnum3.Name = "cmbEnum3";
            this.cmbEnum3.Size = new System.Drawing.Size(82, 21);
            this.cmbEnum3.TabIndex = 10;
            // 
            // radApple
            // 
            this.radApple.AutoSize = true;
            this.radApple.Location = new System.Drawing.Point(17, 243);
            this.radApple.Name = "radApple";
            this.radApple.Size = new System.Drawing.Size(52, 17);
            this.radApple.TabIndex = 12;
            this.radApple.TabStop = true;
            this.radApple.Text = "Apple";
            this.radApple.UseVisualStyleBackColor = true;
            // 
            // radOrange
            // 
            this.radOrange.AutoSize = true;
            this.radOrange.Location = new System.Drawing.Point(75, 243);
            this.radOrange.Name = "radOrange";
            this.radOrange.Size = new System.Drawing.Size(60, 17);
            this.radOrange.TabIndex = 12;
            this.radOrange.TabStop = true;
            this.radOrange.Text = "Orange";
            this.radOrange.UseVisualStyleBackColor = true;
            // 
            // radBanana
            // 
            this.radBanana.AutoSize = true;
            this.radBanana.Location = new System.Drawing.Point(141, 243);
            this.radBanana.Name = "radBanana";
            this.radBanana.Size = new System.Drawing.Size(62, 17);
            this.radBanana.TabIndex = 12;
            this.radBanana.TabStop = true;
            this.radBanana.Text = "Banana";
            this.radBanana.UseVisualStyleBackColor = true;
            // 
            // txtEnum4Follower
            // 
            this.txtEnum4Follower.Location = new System.Drawing.Point(12, 266);
            this.txtEnum4Follower.Name = "txtEnum4Follower";
            this.txtEnum4Follower.Size = new System.Drawing.Size(100, 20);
            this.txtEnum4Follower.TabIndex = 13;
            // 
            // chkEnableX
            // 
            this.chkEnableX.AutoSize = true;
            this.chkEnableX.Location = new System.Drawing.Point(10, 307);
            this.chkEnableX.Name = "chkEnableX";
            this.chkEnableX.Size = new System.Drawing.Size(69, 17);
            this.chkEnableX.TabIndex = 14;
            this.chkEnableX.Text = "Enable X";
            this.chkEnableX.UseVisualStyleBackColor = true;
            // 
            // txtHybridX
            // 
            this.txtHybridX.Location = new System.Drawing.Point(85, 305);
            this.txtHybridX.Name = "txtHybridX";
            this.txtHybridX.Size = new System.Drawing.Size(70, 20);
            this.txtHybridX.TabIndex = 15;
            // 
            // txtHybridFollower
            // 
            this.txtHybridFollower.Location = new System.Drawing.Point(172, 305);
            this.txtHybridFollower.Name = "txtHybridFollower";
            this.txtHybridFollower.Size = new System.Drawing.Size(100, 20);
            this.txtHybridFollower.TabIndex = 16;
            // 
            // cmbStringArray
            // 
            this.cmbStringArray.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbStringArray.FormattingEnabled = true;
            this.cmbStringArray.Location = new System.Drawing.Point(17, 354);
            this.cmbStringArray.Name = "cmbStringArray";
            this.cmbStringArray.Size = new System.Drawing.Size(121, 21);
            this.cmbStringArray.TabIndex = 17;
            // 
            // txtStringArrayFollower
            // 
            this.txtStringArrayFollower.Location = new System.Drawing.Point(26, 392);
            this.txtStringArrayFollower.Name = "txtStringArrayFollower";
            this.txtStringArrayFollower.Size = new System.Drawing.Size(100, 20);
            this.txtStringArrayFollower.TabIndex = 18;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(26, 99);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 19;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // SettingsTestForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 424);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.txtStringArrayFollower);
            this.Controls.Add(this.cmbStringArray);
            this.Controls.Add(this.txtHybridFollower);
            this.Controls.Add(this.txtHybridX);
            this.Controls.Add(this.chkEnableX);
            this.Controls.Add(this.txtEnum4Follower);
            this.Controls.Add(this.radBanana);
            this.Controls.Add(this.radOrange);
            this.Controls.Add(this.radApple);
            this.Controls.Add(this.txtEnum3Follower);
            this.Controls.Add(this.cmbEnum3);
            this.Controls.Add(this.txtEnum2Follower);
            this.Controls.Add(this.cmbEnum2);
            this.Controls.Add(this.txtEnumFollower);
            this.Controls.Add(this.txtEnumValue);
            this.Controls.Add(this.txtFollower);
            this.Controls.Add(this.chkFollower);
            this.Controls.Add(this.txtDisplay);
            this.Controls.Add(this.chkBindingEnabled);
            this.Controls.Add(this.chkSetting);
            this.Name = "SettingsTestForm";
            this.Text = "SettingsTestForm";
            this.Load += new System.EventHandler(this.SettingsTestForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox chkSetting;
        private System.Windows.Forms.CheckBox chkBindingEnabled;
        private System.Windows.Forms.TextBox txtDisplay;
        private System.Windows.Forms.CheckBox chkFollower;
        private System.Windows.Forms.TextBox txtFollower;
        private System.Windows.Forms.TextBox txtEnumValue;
        private System.Windows.Forms.TextBox txtEnumFollower;
        private System.Windows.Forms.ComboBox cmbEnum2;
        private System.Windows.Forms.TextBox txtEnum2Follower;
        private System.Windows.Forms.TextBox txtEnum3Follower;
        private System.Windows.Forms.ComboBox cmbEnum3;
        private System.Windows.Forms.RadioButton radApple;
        private System.Windows.Forms.RadioButton radOrange;
        private System.Windows.Forms.RadioButton radBanana;
        private System.Windows.Forms.TextBox txtEnum4Follower;
        private System.Windows.Forms.CheckBox chkEnableX;
        private System.Windows.Forms.TextBox txtHybridX;
        private System.Windows.Forms.TextBox txtHybridFollower;
        private System.Windows.Forms.ComboBox cmbStringArray;
        private System.Windows.Forms.TextBox txtStringArrayFollower;
        private System.Windows.Forms.Button button1;
    }
}