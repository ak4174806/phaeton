﻿namespace DFM
{
    partial class DFMScanDisplayControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblHeading = new System.Windows.Forms.Label();
            this.zgcGraph = new ZedGraph.ZedGraphControl();
            this.lblErrorDisplay = new System.Windows.Forms.Label();
            this.cDisp3 = new DFM.CaptionedDisplay();
            this.cDisp2 = new DFM.CaptionedDisplay();
            this.cDisp1 = new DFM.CaptionedDisplay();
            this.cDispMain = new DFM.CaptionedDisplay();
            this.SuspendLayout();
            // 
            // lblHeading
            // 
            this.lblHeading.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblHeading.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHeading.Location = new System.Drawing.Point(0, 7);
            this.lblHeading.Margin = new System.Windows.Forms.Padding(5);
            this.lblHeading.Name = "lblHeading";
            this.lblHeading.Size = new System.Drawing.Size(375, 29);
            this.lblHeading.TabIndex = 1;
            this.lblHeading.Text = "LEFT";
            this.lblHeading.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // zgcGraph
            // 
            this.zgcGraph.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.zgcGraph.Cursor = System.Windows.Forms.Cursors.Hand;
            this.zgcGraph.EditButtons = System.Windows.Forms.MouseButtons.None;
            this.zgcGraph.EditModifierKeys = System.Windows.Forms.Keys.None;
            this.zgcGraph.IsEnableHPan = false;
            this.zgcGraph.IsEnableHZoom = false;
            this.zgcGraph.IsEnableVPan = false;
            this.zgcGraph.IsEnableVZoom = false;
            this.zgcGraph.IsEnableWheelZoom = false;
            this.zgcGraph.IsShowContextMenu = false;
            this.zgcGraph.Location = new System.Drawing.Point(4, 232);
            this.zgcGraph.Name = "zgcGraph";
            this.zgcGraph.ScrollGrace = 0D;
            this.zgcGraph.ScrollMaxX = 0D;
            this.zgcGraph.ScrollMaxY = 0D;
            this.zgcGraph.ScrollMaxY2 = 0D;
            this.zgcGraph.ScrollMinX = 0D;
            this.zgcGraph.ScrollMinY = 0D;
            this.zgcGraph.ScrollMinY2 = 0D;
            this.zgcGraph.Size = new System.Drawing.Size(365, 220);
            this.zgcGraph.TabIndex = 4;
            // 
            // lblErrorDisplay
            // 
            this.lblErrorDisplay.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblErrorDisplay.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblErrorDisplay.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblErrorDisplay.ForeColor = System.Drawing.Color.White;
            this.lblErrorDisplay.Location = new System.Drawing.Point(3, 425);
            this.lblErrorDisplay.Name = "lblErrorDisplay";
            this.lblErrorDisplay.Size = new System.Drawing.Size(365, 20);
            this.lblErrorDisplay.TabIndex = 5;
            this.lblErrorDisplay.Text = "Test Text";
            this.lblErrorDisplay.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.lblErrorDisplay.Visible = false;
            // 
            // cDisp3
            // 
            this.cDisp3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cDisp3.Caption = "10 pits";
            this.cDisp3.CaptionFontSize = 10F;
            this.cDisp3.Location = new System.Drawing.Point(298, 159);
            this.cDisp3.Name = "cDisp3";
            this.cDisp3.Size = new System.Drawing.Size(71, 63);
            this.cDisp3.TabIndex = 3;
            this.cDisp3.TextFontSize = 20F;
            // 
            // cDisp2
            // 
            this.cDisp2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.cDisp2.Caption = "10 pits";
            this.cDisp2.CaptionFontSize = 10F;
            this.cDisp2.Location = new System.Drawing.Point(150, 159);
            this.cDisp2.Name = "cDisp2";
            this.cDisp2.Size = new System.Drawing.Size(71, 63);
            this.cDisp2.TabIndex = 3;
            this.cDisp2.TextFontSize = 20F;
            // 
            // cDisp1
            // 
            this.cDisp1.Caption = "10 pits";
            this.cDisp1.CaptionFontSize = 10F;
            this.cDisp1.Location = new System.Drawing.Point(4, 159);
            this.cDisp1.Name = "cDisp1";
            this.cDisp1.Size = new System.Drawing.Size(71, 63);
            this.cDisp1.TabIndex = 3;
            this.cDisp1.TextFontSize = 20F;
            // 
            // cDispMain
            // 
            this.cDispMain.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cDispMain.Caption = "Average of 3";
            this.cDispMain.CaptionFontSize = 14.25F;
            this.cDispMain.Location = new System.Drawing.Point(4, 39);
            this.cDispMain.Name = "cDispMain";
            this.cDispMain.Size = new System.Drawing.Size(365, 110);
            this.cDispMain.TabIndex = 2;
            this.cDispMain.TextFontSize = 40F;
            // 
            // DFMScanDisplayControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lblErrorDisplay);
            this.Controls.Add(this.zgcGraph);
            this.Controls.Add(this.cDisp3);
            this.Controls.Add(this.cDisp2);
            this.Controls.Add(this.cDisp1);
            this.Controls.Add(this.cDispMain);
            this.Controls.Add(this.lblHeading);
            this.Name = "DFMScanDisplayControl";
            this.Size = new System.Drawing.Size(375, 455);
            this.ResumeLayout(false);

        }

        #endregion

        private CaptionedDisplay cDispMain;
        private CaptionedDisplay cDisp1;
        private CaptionedDisplay cDisp2;
        private CaptionedDisplay cDisp3;
        private ZedGraph.ZedGraphControl zgcGraph;
        private System.Windows.Forms.Label lblHeading;
        private System.Windows.Forms.Label lblErrorDisplay;

    }
}
