﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace DFM
{
    public partial class test : Form
    {
        // doesn't work:
        // 1) can't override Ok(), etc
        // 2) tab pages hard to access
        // so need to sub control or subform. Subform seems more logical

        public test()
        {
            InitializeComponent();
        }
    }
}
