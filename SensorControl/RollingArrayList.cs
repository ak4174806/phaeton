﻿using System;
using System.Collections.Generic;
using System.Text;

using System.Collections;

namespace SensorControl
{
    public class RollingArrayList : ArrayList
    {
        int maxCapacity = -1;

        // how much extra to add to the list before truncating
        int grace = -1;

        /// <summary>
        /// The maximum number of items that can be added
        /// </summary>
        public int MaxCapacity
        {
            get { return maxCapacity; }
            set 
            { 
                maxCapacity = value;
                grace = (int)(maxCapacity * 0.2);
            }
        }

        public override int Add(object value)
        {
            int response = base.Add(value);
            
            // if no max capacity set, do nothing different
            if (maxCapacity != -1)
            {
                // truncate
                truncate();
            }

            return response;
        }


        public override void AddRange(ICollection c)
        {
            base.AddRange(c);

            // if no max capacity set, do nothing different
            if (maxCapacity != -1)
            {
                // truncate
                truncate();
            }
        }

        protected virtual void truncate()
        {
            if (base.Count > maxCapacity + grace)
            {
                base.RemoveRange(0, base.Count - (maxCapacity + grace));
            }
        }
    }
}