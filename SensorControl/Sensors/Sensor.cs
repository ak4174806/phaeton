﻿using System;
using System.Collections.Generic;
using System.Text;

using System.Collections;
using System.IO.Ports;
using System.Threading;
using System.ComponentModel;

using SensorControl;

namespace SensorControl.Sensors
{
    public abstract class Sensor : BaseSensor, ISensor
    {
        protected SerialPort port;
        protected Dictionary<string, SetSensorTypeDelegate> sensorTypes;

        #region Properties
        /// <summary>
        /// The serial port that controls the sensor
        /// </summary>
        protected SerialPort Port
        {
            get { return port; }
            set { port = value; }
        }

        /// <summary>
        /// The list of sensor types available
        /// </summary>
        public string[] SensorTypes
        {
            get
            {
                string[] keys = new string[sensorTypes.Count];
                int i = 0;
                foreach (string key in sensorTypes.Keys)
                {
                    keys[i++] = key;
                }
                return keys;
            }
        }
        
        public int BaudRate
        {
            get { return Port.BaudRate; }
        }
        public int DataAvail
        {
            get { return Port.BytesToRead / Properties.DataCount * Properties.DataCount; }
        }

        public SensorAddressTypes SensorAddressType
        {
            get { return SensorAddressTypes.COM_Port; }
        }

        #endregion

        #region Events

        protected delegate void SetSensorTypeDelegate();

        public delegate void SensorEventHandler<A, B>(A sender, B eventArgs);

        public class SensorCancellableEventArgs : EventArgs
        {
            public bool Cancel = false;
        }        

        public event SensorEventHandler<Sensor, SensorCancellableEventArgs> OnConnecting;
        public event SensorEventHandler<Sensor, EventArgs> OnConnected;
        public event SensorEventHandler<Sensor, SensorCancellableEventArgs> OnDisconnecting;
        public event SensorEventHandler<Sensor, EventArgs> OnDisconnected;

        public event SensorEventHandler<Sensor, SensorCancellableEventArgs> OnTakingReadings;
        public event SensorEventHandler<Sensor, EventArgs> OnTakenReadings;
        #endregion

        public Sensor()
        {
            // create the sensor types array
            sensorTypes = new Dictionary<string, SetSensorTypeDelegate>();

            // add init readings handler
            this.OnTakingReadings += HandleFirstReading;
        }

        public void FlushPort()
        {
            Port.DiscardInBuffer();
            Port.DiscardOutBuffer();
        }
        
        #region Choosing Sensor Type

        /// <summary>
        /// Allows the sensor type to be changed
        /// </summary>
        /// <param name="type"></param>
        public void SetSensorType(string type)
        {
            if (sensorTypes.ContainsKey(type))
            {
                // run delegate
                sensorTypes[type]();
            }
            else
            {
                throw new ApplicationException("Tried to set invalid sensor type");
            }
        }

        #endregion

        #region Connection Stuff

        /// <summary>
        /// Tries connecting to sensor or throws an exception
        /// </summary>
        /// <param name="portName"></param>
        /// <param name="baudRate"></param>
        public virtual void Connect(string portName, int baudRate)
        {
            // raise OnConnecting event
            if (this.OnConnecting != null)
            {
                SensorCancellableEventArgs args = new SensorCancellableEventArgs();
                OnConnecting(this, args);

                if (args.Cancel)
                {
                    return;
                }
            }

            try 
            {
                // connect to port
                port = new SerialPort(portName, baudRate);

                // open port
                InitSensorSettings();

                // check if it worked
                if (!TestConnection())
                {
                    throw new ApplicationException("Invalid configuration for sensor");
                }

                // initialise parameters, etc
                LoadSensorParameters();
            }
            catch (Exception ex)
            {
                if (Connected)
                {
                    try { port.Close(); }
                    catch { }
                }

                throw new ApplicationException("Unable to connect to sensor: " + ex.Message, ex);
            }
            
            // raise Connected event
            if (this.OnConnected != null)
            {
                OnConnected(this, new EventArgs());
            }
        }

        /// <summary>
        /// Tries connecting to sensor, autodetects baud rate from list of possible baudrates
        /// </summary>
        /// <param name="portName"></param>
        public virtual void Connect(string portName)
        {
            // raise OnConnecting event
            if (this.OnConnecting != null)
            {
                SensorCancellableEventArgs args = new SensorCancellableEventArgs();
                OnConnecting(this, args);

                if (args.Cancel)
                {
                    return;
                }
            }

            try
            {
                // select port
                port = new SerialPort(portName);

                // initialise it
                InitSensorSettings();

                // the actual baud rate detected
                int actualBR = -1;

                // cycle through baud rates
                foreach (int br in BaudRates)
                {
                    // set baud rate
                    Port.BaudRate = br;

                    // give it time ...
                    Thread.Sleep(50);

                    // check if it worked
                    if (TestConnection())
                    {
                        actualBR = br;
                        break;
                    }
                }

                // check if connection was made
                if (actualBR == -1)
                {
                    // if not, try again more slowly
                    Thread.Sleep(100);

                    // cycle through baud rates
                    foreach (int br in BaudRates)
                    {
                        // set baud rate
                        Port.BaudRate = br;

                        // give it time ...
                        Thread.Sleep(100);

                        // check if it worked
                        if (TestConnection())
                        {
                            actualBR = br;
                            break;
                        }
                    }

                    if (actualBR == -1)
                    {
                        throw new ApplicationException("Could not determine valid baud rate");
                    }
                }

                // initialise parameters, etc
                LoadSensorParameters();
            }
            catch (Exception ex)
            {
                if (Connected)
                {
                    try { port.Close(); }
                    catch { }
                }

                throw new ApplicationException("Unable to connect to sensor: " + ex.Message, ex);
            }

            // raise Connected event
            if (this.OnConnected != null)
            {
                OnConnected(this, new EventArgs());
            }
        }

        /// <summary>
        /// Tries to disconnect from sensor
        /// </summary>
        public virtual void Disconnect()
        {
            try
            {
                if (Connected)
                {
                    // raise OnDisconnecting event
                    if (this.OnDisconnecting != null)
                    {
                        SensorCancellableEventArgs args = new SensorCancellableEventArgs();
                        OnDisconnecting(this, args);

                        if (args.Cancel)
                        {
                            return;
                        }
                    }                             

                    Port.Close();

                    // raise OnDisconnected event
                    if (this.OnDisconnected != null)
                    {
                        OnDisconnected(this, new EventArgs());
                    }
                }

                port = null;
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Could not disconnect from sensor", ex);
            }
        }

        public virtual bool Connected
        {
            get
            {
                return (Port != null && Port.IsOpen);
            }
        }

        #endregion
        
        #region Taking Readings

        /// <summary>
        /// Takes count readings
        /// </summary>
        /// <param name="count"></param>
        /// <returns></returns>
        public virtual SensorReading[] TakeReadings(int count)
        {
            // check connection is open
            if (!Connected)
            {
                throw new ApplicationException("No connection to the sensor");
            }

            // raise OnTakingReadings event
            if (this.OnTakingReadings != null)
            {
                SensorCancellableEventArgs args = new SensorCancellableEventArgs();
                OnTakingReadings(this, args);

                if (args.Cancel)
                {
                    return new SensorReading[] { };
                }
            }

            // read appropriate number of bytes
            byte[] allData = new byte[Properties.ResponseLength * count];

            try
            {
                string strData = PortBuffer.Read(Properties.ResponseLength * count);

                // copy into byte array
                for (int i = 0; i < strData.Length; i++)
                {
                    allData[i] = (byte)strData[i];
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Could not read data from sensor", ex);
            }


            // process readings
            SensorReading[] readings = new SensorReading[count];
            SensorReading reading;
            byte[] data = new byte[Properties.ResponseLength];

            int j = 0;
            for (int i = 0; i < count; i++)
            {
                // segment data
                Array.Copy(allData, Properties.ResponseLength * i, data, 0, Properties.ResponseLength);

                // calculate reading
                reading = ProcessReading(data);

                // apply filtering
                if (!reading.IsError || !FilterErrors)
                {
                    readings[j++] = reading;
                }
            }

            // trim readings
            Array.Resize(ref readings, j);

            // raise OnTakenReadings event
            if (this.OnTakenReadings != null)
            {
                OnTakenReadings(this, new EventArgs());
            }

            return readings;
        }

        /// <summary>
        /// Processes a single set of readings
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        protected virtual SensorReading ProcessReading(byte[] data)
        {
            // increment time regardless of error
            if (TimeIncrement != -1)
            {
                CurrentTime += TimeIncrement;
            }

            // validate measurement
            data = IsValidReading(data);
            if (data == null)
            {
                // invalid reading
                return new SensorReading(SensorReading.DigitalErrors.INVALID_READING);
            }

            // convert to digital value
            int dout = GetDigitalValue(data);

            // check if represents error message
            SensorReading.DigitalErrors derror = IsDigitalValueError(dout);
            if (derror != SensorReading.DigitalErrors.NO_ERROR)
            {
                return new SensorReading(derror);
            }

            // calculate reading
            double r = CalculateReading(dout);
            
            // apply zero offset
            r -= ZeroOffset;

            SensorReading reading = new SensorReading(r, CurrentTime);

            return reading;
        }

        #region Take Reading Overloads

        /// <summary>
        /// Takes a single reading
        /// </summary>
        /// <returns></returns>
        public virtual SensorReading TakeReading()
        {
            return TakeReadings(1)[0];
        }

        /// <summary>
        /// Takes as many readings as there are currently buffered
        /// </summary>
        /// <returns></returns>
        public virtual SensorReading[] TakeReadings()
        {
            // check connection is open
            if (!Connected)
            {
                throw new ApplicationException("No connection to the sensor");
            }
            
            // find number of readings available
            PortBuffer.Update();
            int bufferSize = PortBuffer.BufferSize();
            int count = bufferSize / Properties.ResponseLength;

            // take readings
            return TakeReadings(count);
        }

        #endregion

        #endregion

        #region Running Commands

        /// <summary>
        /// Runs a command and returns the response.
        /// Separates ReplyStart, the response and ReplyClose
        /// </summary>
        /// <returns></returns>
        public virtual string[] Run(SensorCommand command)
        {
            // check port is open
            if (!Connected)
            {
                throw new ApplicationException("No connection to sensor");
            }

            string command_string = Properties.CommandStart + command.GetCommand() + Properties.CommandClose;

            // clear buffers
            PortBuffer.Clear();

            // send command
            Port.Write(command_string);
                        
            // pause for time requested
            Thread.Sleep(command.Time);

            // get and return response
            string[] a = GetResponse(command);

            return a;
        }

        /// <summary>
        /// Gets the response from command
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        protected virtual string[] GetResponse(SensorCommand command)
        {
            // the current tokens to look for
            string[] tokens;
            // a buffer in which to search for the tokens
            StringBuilder buffer = new StringBuilder();
            // the current value of the buffer
            string bufferText;

            // the found tokens
            string[] foundTokens = new string[2];

            // stores the response
            StringBuilder responseBuilder = new StringBuilder();

            // look for start and end tokens
            for (int i = 0; i < 2; i++)
            {
                // reset found token
                foundTokens[i] = "";

                // choose tokens to find
                if (i == 0)
                {
                    tokens = (string[])Properties.ReplyStart.Clone();
                }
                else
                    tokens = (string[])Properties.ReplyClose.Clone();

                // transate [echo]'s
                string echoReplace = Properties.CommandStart + command.GetCommand();
                for (int j = 0; j < tokens.Length; j++)
                    tokens[j] = tokens[j].Replace("[echo]", echoReplace);

                // fix as SCA and STS don't echo "?"
                if (i == 0 && (tokens[0].Contains("SCA?") || tokens[0].Contains("STS?")))
                {
                    tokens = new string[] { tokens[0], tokens[0].Replace("?", "") };
                }
                
                // find length of smallest token
                int min_token_length = tokens[0].Length;
                foreach (string t in tokens)
                {
                    if (t.Length < min_token_length)
                        min_token_length = t.Length;
                }

                                                
                // to timeout if buffer gets too large (normally from debugging)
                int bytes_read = 0;
		        char c;

                // to count sleeps
                int k = 0; int max_k = 8;

                // read bytes until token is found
                while (foundTokens[i] == "" && bytes_read < 5000)
                {
                    while (port.BytesToRead == 0)
                    {
                        Thread.Sleep(100);
                        k++;
                        if (k == max_k)
                            throw new ApplicationException("Could not find response to command: " + command.Name + ", too many sleeps finding token " + (i+1).ToString() + " of 2");
                    }

                    // read a character
                    c = (char)port.ReadChar();
                    bytes_read++;

                    // build response
                    if (i == 1)
                        responseBuilder.Append(c);

                    buffer.Append(c);

                    // if buffer is capable of holding a token
                    if (buffer.Length >= min_token_length)
                    {
                        bufferText = buffer.ToString();

                        // go through each token, check last token_length bytes of buffer
                        foreach (string t in tokens)
                        {
                            if (bufferText.Length >= t.Length && bufferText.Substring(bufferText.Length - t.Length) == t)
                            {
                                foundTokens[i] = t;
                                break;
                            }
                        }
                    }
                }                
                    
                if (foundTokens[i] == "")
                {
                    throw new ApplicationException("Could not find response to command: " + command.Name);
                }
            }

            string response = responseBuilder.ToString();

            // remove reply close from response
            response = response.Substring(0, response.Length - foundTokens[1].Length);

            return new string[] { foundTokens[0], response, foundTokens[1] };
        }

        #endregion
        
        #region Reading Calculations

        /// <summary>
        /// Checks if given bytes are valid reading, and updates them if necessary
        /// </summary>
        /// <param name="data"></param>
        /// <returns>Possibly reordered data, or null</returns>
        abstract protected byte[] IsValidReading(byte[] data);

        /// <summary>
        /// Converts response from sensor to digital output value
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        abstract protected int GetDigitalValue(byte[] data);

        /// <summary>
        /// Checks if a digital value is an error
        /// </summary>
        /// <param name="digitalValue"></param>
        /// <returns></returns>
        abstract protected SensorReading.DigitalErrors IsDigitalValueError(int digitalValue);

        /// <summary>
        /// Calculates a reading given the digital value
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        abstract protected double CalculateReading(int digitalValue);

        #endregion

        #region Connection Methods

        /// <summary>
        /// Called after opening port
        /// </summary>
        protected virtual void InitSensorSettings()
        {
            // sets encoding to iso-8859-1 (Western European (ISO))
            Port.Encoding = Encoding.GetEncoding(28591);
            Port.ReadTimeout = 3;
            Port.WriteTimeout = 5;
                        
            Port.Open();
            Thread.Sleep(200);

            // create buffer for serial port, w/ default timeout
            portBuffer = new SerialPortBuffer(Port);

            // clear buffers
            portBuffer.Clear();
        }

        /// <summary>
        /// Called to test whether connection settings (baudrate) are correct
        /// </summary>
        protected abstract bool TestConnection();

        /// <summary>
        /// Updates information about sensor
        /// </summary>
        protected abstract void LoadSensorParameters();
        
        /// <summary>
        /// Called before taking the first reading(s)
        /// </summary>
        protected abstract void InitReading();

        /// <summary>
        /// Handles calling InitReading before any readings are taken
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        private void HandleFirstReading(Sensor sender, SensorCancellableEventArgs args)
        {
            EnsureReadingsInitialised();
        }

        private bool readingsInitialised = false;
        /// <summary>
        /// Used to initialise readings so that first reading has no extra delay
        /// </summary>
        public void EnsureReadingsInitialised()
        {
            if (!readingsInitialised)
            {
                InitReading();
                this.OnTakingReadings -= HandleFirstReading;
                readingsInitialised = true;
            }
        }

        #endregion
    }
}
