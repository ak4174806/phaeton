﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SensorControl.Sensors
{
    public class ILD2200_MESensor : MESensor
    {
        public ILD2200_MESensor()
        {
            this.sensorModel = MEDAQLib.ME_SENSOR.SENSOR_ILD2200;

            Properties.ModelName = "ILD2200 / ILD2220";
            Properties.ResponseLength = 3;

            Properties.CommandStart = "\x2b\x2b\x2b\x0d\x49\x4c\x44\x31";
            Properties.CommandClose = "";

            Properties.ReplyStart = new string[] { "\x49\x4c\x44\x31" };
            Properties.ReplyClose = new string[] { "\x20\x20\x0d\x0a" };

            Properties.DataSeparator = "";
            Properties.FrameSeparator = "";
            Properties.DataCount = 1;
            Properties.DataLength = 3;

            Properties.MR = 2;
            Properties.SMR = 24;
            

            // added
            baudRates = new int[] { 691200 };
            frequencies = new string[] { "10000" };

            // add frequency parameter
            Parameters.Add("frequency", new SensorParameter(
                delegate(string value)
                {
                    return "10000";
                }));
        }

        protected override bool TestConnection()
        {
            MEDAQLib.ERR_CODE iRet = MEDAQLib.SetParameterString(sensor, "S_Command", "Dat_Out_On");
            if (iRet != MEDAQLib.ERR_CODE.ERR_NOERROR)
            {
                return false;
            }

            iRet = MEDAQLib.SensorCommand(sensor);
            if (iRet != MEDAQLib.ERR_CODE.ERR_NOERROR)
            {
                return false;
            }

            return true;
        }

        protected override void InitReading()
        {
            MEDAQLib.ClearAllParameters(sensor);
            MEDAQLib.SetParameterString(sensor, "S_Command", "Dat_Out_On");
            MEDAQLib.SensorCommand(sensor);
        }

        protected override double CalculateReading(int digitalValue)
        {
            double value = (digitalValue * 1.02 / 65520 - 0.01) * Properties.MR;

            if (ReadingReference == ReadingReferences.SENSOR)
                value += Properties.SMR;

            return value;
        }
    }
}
