﻿using System;
using System.Collections.Generic;
using System.Text;
using SensorControl;

using System.Threading;

namespace SensorControl.Sensors
{
    public class ILD2200_Sensor : Sensor
    {
        #region Private Members
        /// <summary>
        /// The possible averaging methods
        /// </summary>
        protected string[] averagingMethods;

        /// <summary>
        /// The possible averaging numbers, indexed by the method
        /// </summary>
        protected Dictionary<string, int[]> averagingNumbers;

        #endregion

        public ILD2200_Sensor()
        {
            Properties.ModelName = "ILD2200";
            Properties.ResponseLength = 3;

            Properties.CommandStart = "\x2b\x2b\x2b\x0d\x49\x4c\x44\x31";
            Properties.CommandClose = "";

            Properties.ReplyStart = new string[] { "\x49\x4c\x44\x31" };
            Properties.ReplyClose = new string[] { "\x20\x20\x0d\x0a" };

            Properties.DataSeparator = "";
            Properties.FrameSeparator = "";
            Properties.DataCount = 1;
            Properties.DataLength = 3;

            // implement sensor types
            sensorTypes.Add("ILD2200-2", delegate()
            {
                Properties.MR = 2;
                Properties.SMR = 24;
            });

            SetSensorType("ILD2200-2");

            // set commands
            Commands.Add("GET_INFO", new SensorCommand("GET_INFO", "\x20\x49\x00\x02", 1000));
            Commands.Add("GET_SETTINGS", new SensorCommand("GET_SETTINGS", "\x20\x4a\x00\x02", 1000));

            Commands.Add("SET_AV0", new SensorCommand("SET_AV0", "\x20\x70\x00\x02"));
            Commands.Add("SET_AV1", new SensorCommand("SET_AV1", "\x20\x71\x00\x02"));
            Commands.Add("SET_AV2", new SensorCommand("SET_AV2", "\x20\x72\x00\x02"));
            Commands.Add("SET_AV3", new SensorCommand("SET_AV3", "\x20\x73\x00\x02"));

            Commands.Add("SET_AVX", new SensorCommand("SET_AVX", "\x20\x75\x00\x03"));
            Commands.Add("SET_AV_T", new SensorCommand("SET_AV_T", "\x20\x7d\x00\x03"));
            Commands["SET_AV_T"].Values.Add("recursive", "\x00\x00\x00\x00");
            Commands["SET_AV_T"].Values.Add("moving", "\x00\x00\x00\x01");
            Commands["SET_AV_T"].Values.Add("median", "\x00\x00\x00\x02");

            Commands.Add("START", new SensorCommand("START", "\x20\x77\x00\x02"));
            Commands.Add("STOP", new SensorCommand("STOP", "\x20\x76\x00\x02"));

            baudRates = new int[] { 691200 };
            frequencies = new string[] { "10000" };

            // possible averaging settings
            averagingMethods = new string[3] { "recursive", "moving", "median" };

            averagingNumbers = new Dictionary<string, int[]>();
            averagingNumbers.Add("recursive", new int[] { 1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048, 4096, 8192, 16384, 32768 });
            averagingNumbers.Add("moving", new int[] { 1, 2, 4, 8, 16, 32, 64, 128 });
            averagingNumbers.Add("median", new int[] { 3, 5, 7, 9 });

            // parameters
            Parameters.Add("period", new SensorParameter(
                delegate(string value)
                {
                    return (1.0 / 10000).ToString();
                }));

            Parameters.Add("frequency", new SensorParameter(
                delegate(string value)
                {
                    return frequencies[0];
                }));

            Parameters.Add("baudrate", new SensorParameter(
                delegate(string value)
                {
                    return baudRates[0].ToString();
                }));


            Parameters.Add("averaging_method", new SensorParameter(true,
                delegate(string value)
                {
                    string[] response = Run(Commands["SET_AV_T"], value);
                    if (ProcessResponse(response) != "")
                    {
                        throw new ApplicationException("Error setting averaging method");
                    }
                }));

            Parameters.Add("averaging_number", new SensorParameter(true,
                delegate(string value)
                {
                    try
                    {
                        int ivalue = Convert.ToInt32(value);

                        if (Parameters["averaging_method"].Get() == "median")
                        {
                            string command;

                            // cannot use set_av_x - find which command to use
                            switch (ivalue)
                            {
                                case 3:
                                    command = "SET_AV0";
                                    break;
                                case 5:
                                    command = "SET_AV1";
                                    break;
                                case 7:
                                    command = "SET_AV2";
                                    break;
                                case 9:
                                    command = "SET_AV3";
                                    break;
                                default:
                                    throw new ApplicationException("Attempted to set invalid averaging number");
                            }

                            string[] response = Run(Commands[command], value);
                            if (ProcessResponse(response) != "")
                            {
                                throw new ApplicationException("Error setting averaging number");
                            }
                        }
                        else
                        {
                            // find x = log2(ivalue)
                            int x = (int)Math.Log(ivalue, 2);

                            string averagingMethod = Parameters["averaging_method"].Get();

                            // validate
                            if (!((averagingMethod == "recursive" && x >= 0 && x <= 7) ||
                                (averagingMethod == "moving" && x >= 0 && x <= 15)))
                            {
                                throw new ApplicationException("Attempted to set invalid averaging number");
                            }

                            string[] response = Run(Commands["SET_AVX"], x);
                            if (ProcessResponse(response) != "")
                            {
                                throw new ApplicationException("Error setting averaging number");
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ApplicationException("Could not set averaging number " + ex.Message, ex);
                    }                    
                }));
        }

        #region Overrides

        #region Run Overloads
        /// <summary>
        /// Runs a command with a data value
        /// </summary>
        /// <param name="command"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public virtual string[] Run(SensorCommand command, string data)
        {
            command.Value = data;
            return Run(command);
        }

        /// <summary>
        /// Runs a command with multiple data values in array
        /// </summary>
        /// <param name="command"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public virtual string[] Run(SensorCommand command, string[] data)
        {
            command.Value = String.Concat(data);
            return Run(command);
        }

        /// <summary>
        /// Runs a command with an integer data value
        /// </summary>
        /// <param name="command"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public virtual string[] Run(SensorCommand command, int data)
        {
            string hexCode = Convert.ToString(data, 16).PadLeft(8, '0');
            StringBuilder strBuilder = new StringBuilder(4);

            char c;
            byte b;
            for (int i = 0; 2 * i < hexCode.Length; i++)
            {
                b = Convert.ToByte(hexCode[2 * i].ToString() + hexCode[2 * i + 1].ToString(), 16);
                c = Convert.ToChar(b);
                strBuilder.Append(c);
            }

            return Run(command, strBuilder.ToString());
        }

        /// <summary>
        /// Runs a command with multiple data values in array
        /// </summary>
        /// <param name="command"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public virtual string[] Run(SensorCommand command, int[] data)
        {
            StringBuilder hexBuilder = new StringBuilder(data.Length * 4);

            // convert to 4-byte 0-padded words
            foreach (int d in data)
            {
                hexBuilder.Append(Convert.ToString(d, 16).PadLeft(8, '0'));
            }

            // join these words
            string hexCode = hexBuilder.ToString();

            // convert each hex pair to characters
            StringBuilder strBuilder = new StringBuilder(4);

            char c;
            byte b;
            for (int i = 0; 2 * i < hexCode.Length; i++)
            {
                b = Convert.ToByte(hexCode[2 * i].ToString() + hexCode[2 * i + 1].ToString(), 16);
                c = Convert.ToChar(b);
                strBuilder.Append(c);
            }

            return Run(command, strBuilder.ToString());
        }
        #endregion
        
        /// <summary>
        /// Gets the response from command
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        protected override string[] GetResponse(SensorCommand command)
        {
            // a buffer in which to search for the tokens
            StringBuilder buffer = new StringBuilder();

            // the current value of the buffer
            string bufferText;

            // tokens to find
            string[] tokens = (string[])Properties.ReplyStart.Clone();
            // the actual reply start
            string replyStart = "";

            // find length of smallest token
            int min_token_length = tokens[0].Length;
            foreach (string t in tokens)
            {
                if (t.Length < min_token_length)
                    min_token_length = t.Length;
            }


            // to timeout if buffer gets too large (normally from debugging)
            int bytes_read = 0;
            char c;

            // to count sleeps
            int k = 0; int max_k = 8;

            // read bytes until reply start token is found
            while (replyStart == "" && bytes_read < 5000)
            {
                while (port.BytesToRead == 0)
                {
                    Thread.Sleep(100);
                    k++;
                    if (k == max_k)
                        throw new ApplicationException("Could not find response to command: " + command.Name + ", too many sleeps finding replyStart");
                }

                // read a character
                c = (char)port.ReadChar();
                bytes_read++;

                buffer.Append(c);

                // if buffer is capable of holding a token
                if (buffer.Length >= min_token_length)
                {
                    bufferText = buffer.ToString();

                    // go through each token, check last token_length bytes of buffer
                    foreach (string t in tokens)
                    {
                        if (bufferText.Length >= t.Length && bufferText.Substring(bufferText.Length - t.Length) == t)
                        {
                            replyStart = t;
                            break;
                        }
                    }
                }
            }

            // check if start token was found
            if (replyStart == "")
            {
                throw new ApplicationException("Could not find response to command: " + command.Name);
            }

            char[] cBuffer;
            int count; // to ensure get entire segment
            k = 0; max_k = 3; // to prevent infinite loops

            // 4th byte now gives package length
            cBuffer = new char[4];
            count = 0;
            while (count < 4)
            {
                count += port.Read(cBuffer, count, 4 - count);
                if (count < 4)
                {
                    Thread.Sleep(100);
                    k++;
                    if (k >= max_k)
                        throw new ApplicationException("Could not read full command - stuck finding package length");
                }
            }
            string replyCommand = new StringBuilder().Append(cBuffer).ToString();

            // read rest of response - break it into data and conclusion word
            byte package_length = (byte)cBuffer[3];

            // length of data
            int bytes_to_read = 4 * (package_length - 2);

            // get data
            string replyData = "";
            if (bytes_to_read > 0)
            {
                cBuffer = new char[bytes_to_read];
                count = 0;
                while (count < bytes_to_read)
                {
                    count += port.Read(cBuffer, count, bytes_to_read - count);
                    if (count < bytes_to_read)
                    {
                        Thread.Sleep(100);
                        k++;
                        if (k >= max_k)
                            throw new ApplicationException("Could not read full command - stuck finding data");
                    }
                }
                replyData = new StringBuilder().Append(cBuffer).ToString();
            }

            // get conclusion word
            cBuffer = new char[4];
            count = 0;
            while (count < 4)
            {
                count += port.Read(cBuffer, count, 4 - count);
                if (count < 4)
                {
                    Thread.Sleep(100);
                    k++;
                    if (k >= max_k)
                        throw new ApplicationException("Could not read full command - stuck finding conclusion");
                }
            }
            string replyConclusion = new StringBuilder().Append(cBuffer).ToString();

            // return response
            return new string[] { replyStart, replyCommand, replyData, replyConclusion };
        }

        /// <summary>
        /// Processes a response and either returns the data, or a string prefixed with "Error: "
        /// </summary>
        /// <param name="response"></param>
        /// <returns></returns>
        protected string ProcessResponse(string[] response)
        {
            if (response.Length == 4)
            {
                // get staus bit from responseCommand
                string reply_type = SensorFunctions.dec2bin(response[1][0]);

                // valid response
                if (reply_type[0] == '1')
                {
                    // if 2nd bit 1 then error
                    if (reply_type[1] != '1')
                    {
                        // return data
                        return response[2];
                    }
                    else
                    {
                        // 4th byte of data
                        byte error_code = (byte)response[2][3];
                        return "Error: sensor error code " + error_code.ToString();
                    }
                }
                else
                {
                    return "Error: Invalid response to command";
                }
            }

            return "Error: Incomplete response";
        }

        #endregion

        #region Reading Calculation Methods

        protected override byte[] IsValidReading(byte[] data)
        {
            if (data.Length == 3)
            {
                string b1 = SensorFunctions.dec2bin(data[0]);
                string b2 = SensorFunctions.dec2bin(data[1]);
                string b3 = SensorFunctions.dec2bin(data[2]);

                string b1Start = b1.Substring(0, 2);
                string b2Start = b2.Substring(0, 2);
                string b3Start = b3.Substring(0, 2);

                if (b1Start == "00" && b2Start == "01" && b3Start == "10")
                {
                    return data;
                }
                else if (b2Start == "00" && b3Start == "01" && b1Start == "10")
                {
                    return new byte[] { data[1], data[2], data[0] };
                }
                else if (b3Start == "00" && b1Start == "01" && b2Start == "10")
                {
                    return new byte[] { data[2], data[0], data[1] };
                }
            }

            return null;
        }

        protected override int GetDigitalValue(byte[] data)
        {
            // b3 is h-byte
            string b1 = SensorFunctions.dec2bin(data[0]);
            string b2 = SensorFunctions.dec2bin(data[1]);
            string b3 = SensorFunctions.dec2bin(data[2]);

            return Convert.ToInt32(b3.Substring(4) + b2.Substring(2) + b1.Substring(2), 2);
        }

        protected override SensorReading.DigitalErrors IsDigitalValueError(int digitalValue)
        {
            if (digitalValue < 65520)
            {
                return SensorReading.DigitalErrors.NO_ERROR;
            }

            switch (digitalValue)
            {
                case 65522:
                    return SensorReading.DigitalErrors.BAD_OBJECT;

                case 65524:
                    return SensorReading.DigitalErrors.OUT_OF_RANGE_CLOSE;

                case 65526:
                    return SensorReading.DigitalErrors.OUT_OF_RANGE_FAR;

                case 65528:
                    return SensorReading.DigitalErrors.POOR_TARGET;

                case 65530:
                    return SensorReading.DigitalErrors.LASER_OFF;

                default:
                    return SensorReading.DigitalErrors.UNKNOWN_ERROR;
            }
        }

        protected override double CalculateReading(int digitalValue)
        {
            double value = (digitalValue * 1.02 / 65520 - 0.01) * Properties.MR;

            if (ReadingReference == ReadingReferences.SENSOR)
                value += Properties.SMR;

            return value;
        }

        #endregion

        protected override bool TestConnection()
        {
            try
            {
                string[] response = Run(Commands["START"]);
                bool success = (ProcessResponse(response) == "");
                return success;
            }
            catch
            {
                return false;
            }
        }

        protected override void LoadSensorParameters()
        {
            string response = ProcessResponse(Run(Commands["GET_SETTINGS"]));
            int[] responses = new int[(response.Length / 4)];

            string piece, hexPiece;
            int j = 0;
            int value;

            // get values of each response
            for (int i = 0; 4 * i < response.Length; i++)
            {
                piece = response.Substring(4 * i, 4);
                bool contains = false;
                foreach (string s in Properties.ReplyClose)
                {
                    if (s == piece)
                    {
                        contains = true;
                        break;
                    }
                }

                if (!contains)
                {
                    hexPiece = String.Concat(SensorFunctions.str2hex(piece));
                    value = Convert.ToInt32(hexPiece, 16);
                    responses[j] = value;
                    j++;
                }
            }

            // averaging type
            Parameters["averaging_method"].SetValue(averagingMethods[responses[3]]); ;

            // averaging number
            int averagingNumber = -1;
            if (Parameters["averaging_method"].Get() == "median")
            {
                switch (responses[1])
                {
                    case 0: averagingNumber = 3; break;
                    case 2: averagingNumber = 5; break;
                    case 5: averagingNumber = 7; break;
                    case 7: averagingNumber = 9; break;
                }
            }
            else
            {
                averagingNumber = (int)Math.Pow(2, responses[1]);
            }

            if (averagingNumber != -1)
            {
                Parameters["averaging_number"].SetValue(averagingNumber.ToString());
            }
            else
            {
                // throw new ApplicationException("Invalid averaging number");
            }

        }

        protected override void InitReading()
        {
            try
            {
                // needed as errors thrown occasionally
                Run(commands["START"]);
            }
            catch { }
        }
    }
}
