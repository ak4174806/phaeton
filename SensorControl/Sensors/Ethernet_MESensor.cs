﻿using System;
using System.Collections.Generic;
using System.Text;

using SensorControl;

namespace SensorControl.Sensors
{
    public abstract class Ethernet_MESensor : MESensor
    {
        public Ethernet_MESensor()
            : base()
        {
            sensorAddressType = SensorAddressTypes.IP_Address;
        }

        public override void Connect(string ipAddress)
        {
            // if an old sensor
            if (sensor != 0)
            {
                Disconnect();
            }

            // create sensor instance
            sensor = MEDAQLib.CreateSensorInstance(sensorModel);
            if (sensor == 0)
            {
                throw new ApplicationException("CreateSensorInstance: Error occured, no sensor created");
            }

            // set up connection parameters
            MEDAQLib.ERR_CODE iRet = MEDAQLib.ERR_CODE.ERR_NOERROR;

            iRet = MEDAQLib.SetParameterString(sensor, "IP_Interface", "TCP/IP");
            if (iRet != MEDAQLib.ERR_CODE.ERR_NOERROR)
            {
                MEDAQLib.ReleaseSensorInstance(sensor);
                Error("SetParameterString(IP_Interface, TCP/IP)", sensor, iRet);
            }
            iRet = MEDAQLib.SetParameterString(sensor, "IP_RemoteAddr", ipAddress);
            if (iRet != MEDAQLib.ERR_CODE.ERR_NOERROR)
            {
                MEDAQLib.ReleaseSensorInstance(sensor);
                Error("SetParameterString(IP_RemoteAddr, " + ipAddress + ")", sensor, iRet);
            }

            // set error handling behaviour - set to -1
            iRet = MEDAQLib.SetParameterInt(sensor, "IP_ScaleErrorValues", 1);
            if (iRet != MEDAQLib.ERR_CODE.ERR_NOERROR)
            {
                MEDAQLib.ReleaseSensorInstance(sensor);
                Error("SetParameterInt(IP_ScaleErrorValues, 3)", sensor, iRet);
            }
            iRet = MEDAQLib.SetParameterDouble(sensor, "IP_FixedErrorValue", 5);
            if (iRet != MEDAQLib.ERR_CODE.ERR_NOERROR)
            {
                MEDAQLib.ReleaseSensorInstance(sensor);
                Error("SetParameterInt(IP_FixedErrorValue, -5)", sensor, iRet);
            }

            // open connection
            iRet = MEDAQLib.OpenSensor(sensor);
            if (iRet != MEDAQLib.ERR_CODE.ERR_NOERROR)
            {
                MEDAQLib.ReleaseSensorInstance(sensor);
                Error("OpenSensor (TCP/IP, " + ipAddress + ")", sensor, iRet);
            }
            connected = true;

            // check if actually connected
            if (!TestConnection())
            {
                Disconnect();
                Error("Could not connect to sensor.", sensor);
            }
        }
    }
}
