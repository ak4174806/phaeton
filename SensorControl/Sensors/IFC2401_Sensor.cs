﻿using System;
using System.Collections.Generic;
using System.Text;

using System.Text.RegularExpressions;
using System.Threading;

using SensorControl;

namespace SensorControl.Sensors
{
    public class IFC2401_Sensor : Sensor
    {
        public IFC2401_Sensor()
        {
            Properties.ModelName = "IFC2401";
            Properties.ResponseLength = 4;

            Properties.CommandStart = "$";
            Properties.CommandClose = "\r\n";

            Properties.ReplyStart = new string[] { "[echo]" };
            Properties.ReplyClose = new string[] { "ready\n\r", "invalid code\n\r", "error\n\r", "not valid\n\r" };

            Properties.DataSeparator = "";
            Properties.FrameSeparator = "\xff\xff";
            Properties.DataCount = 1;
            Properties.DataLength = 2;

            // implement sensor types
            sensorTypes.Add("IFC2401-3", delegate()
            {
                Properties.MR = 3;
                Properties.SMR = 16.3;
            });

            SetSensorType("IFC2401-3");


            // set commands
            Commands.Add("SRA", new SensorCommand("Set measuring rate", "SRA"));
            Commands["SRA"].Values.Add("free", "00");
            Commands["SRA"].Values.Add("100", "01");
            Commands["SRA"].Values.Add("200", "02");
            Commands["SRA"].Values.Add("400", "03");
            Commands["SRA"].Values.Add("1000", "04");
            Commands["SRA"].Values.Add("2000", "05");

            Commands.Add("FRQ", new SensorCommand("Set frequency in Hz (100 to 2000)", "FRQ"));
            Commands.Add("TEX", new SensorCommand("Set exposure time in microseconds (500 to 10000)", "TEX"));

            Commands.Add("BAU", new SensorCommand("Set baud rate", "BAU", 200));

            Commands.Add("MOD", new SensorCommand("Set measuring mode (displacement=0, thickness=1)", "MOD"));
            Commands.Add("CCL", new SensorCommand("Set light source (internal=0, external=1)", "CCL"));
            Commands.Add("AVR", new SensorCommand("Set averaging", "AVR"));
            Commands.Add("BIN", new SensorCommand("Set output mode to binary", "BIN"));
            Commands.Add("SOD", new SensorCommand("Set data to be transmitted", "SOD"));

            // possible baud rates and frequencies
            baudRates = new int[] { 115200, 57600, 19200, 9600 };
            frequencies = new string[] { "100", "200", "400", "1000", "2000" };
            
            // add parameters
            Parameters.Add("frequency", new SensorParameter(true, 
                delegate(string value)
                {
                    try
                    {
                        // set to free measuring rate
                        Parameters["measuring_rate"].Set("free");

                        int ivalue = Convert.ToInt32(value);
                        if (ivalue > 2000 || ivalue < 100)
                        {
                            throw new ApplicationException("Error tried to set invalid frequency");
                        }

                        string[] response = Run(Commands["FRQ"], value);
                        if (ProcessResponse(response) == "error")
                        {
                            throw new ApplicationException("Error setting frequency");
                        }
                    }
                    catch
                    {
                        // throw new ApplicationException("Could not set frequency to " + value, ex);
                    }
                }));

            Parameters.Add("measuring_rate", new SensorParameter(true, 
                delegate(string value)
                {
                    // make sure it is set to free
                    string[] response = Run(Commands["SRA"], value);
                    if (ProcessResponse(response) == "error")
                    {
                        throw new ApplicationException("Error setting measuring rate");
                    }
                }));

            Parameters.Add("baudrate", new SensorParameter(
                delegate(string value)
                {
                    return Port.BaudRate.ToString();
                }, 
                delegate(string value)
                {
                    // update baud rate in sensor
                    try
                    {
                        // can't check response as it gets jumbled
                        string[] response = Run(Commands["BAU"], value);
                    }
                    catch { }

                    // set baud rate in port
                    Port.BaudRate = Convert.ToInt32(value);

                    // give it time ...
                    Thread.Sleep(200);

                    if (!TestConnection())
                    {
                        // throw new ApplicationException("Error setting baud rate on sensor");
                    }
                }));

            // readonly period in seconds
            Parameters.Add("period", new SensorParameter(
                delegate(string value)
                {
                    return (1.0 / Convert.ToDouble(Parameters["frequency"].Get())).ToString();
                }));
        }
        

        /// <summary>
        /// Processes response from Run, and returns either trimmed response or "error"
        /// </summary>
        /// <param name="response"></param>
        /// <returns></returns>
        protected string ProcessResponse(string[] response)
        {
            if (response.Length == 3 && response[2] == "ready\n\r")
            {
                return response[1].Trim();
            }

            return "error";
        }

        #region Run Overloads
        /// <summary>
        /// Runs a command with a data value
        /// </summary>
        /// <param name="command"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public virtual string[] Run(SensorCommand command, string data)
        {
            command.Value = data;
            return Run(command);
        }

        /// <summary>
        /// Runs a command with multiple data values in array
        /// </summary>
        /// <param name="command"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public virtual string[] Run(SensorCommand command, string[] data)
        {
            command.Value = String.Join(",", data);
            return Run(command);
        }

        /// <summary>
        /// Runs a command with an integer data value
        /// </summary>
        /// <param name="command"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public virtual string[] Run(SensorCommand command, int data)
        {
            return Run(command, data.ToString());
        }

        /// <summary>
        /// Runs a command with multiple data values in array
        /// </summary>
        /// <param name="command"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public virtual string[] Run(SensorCommand command, int[] data)
        {
            string[] str_data = new string[data.Length];
            for (int i = 0; i < data.Length; i++)
            {
                str_data[i] = data[i].ToString();
            }

            return Run(command, str_data);
        }
        #endregion


        private Regex validateReading;

        protected override byte[] IsValidReading(byte[] data)
        {
            if (validateReading == null)
            {
                StringBuilder pattern = new StringBuilder();
                string datum = "(.{" + Properties.DataLength.ToString() + "})";
                // add one data
                pattern.Append(datum);
                if (Properties.DataCount > 1)
                {
                    // add rest of data delimited by Data Separator
                    pattern.Insert(0, Properties.DataSeparator + datum, Properties.DataCount - 1);
                }
                // add frame separator
                pattern.Append(Properties.FrameSeparator);

                // regex to validate readings
                validateReading = new Regex(pattern.ToString());
            }

            // duplicate data and find pattern
            StringBuilder search = new StringBuilder();
            foreach (byte b in data)
            {
                search.Append((char)b);
            }
            // duplicate
            search.Append(search.ToString());

            string s = search.ToString();
            int p = s.LastIndexOf(Properties.FrameSeparator);
            if (p > 1)
            {
                byte[] new_data = new byte[2];
                new_data[0] = (byte)s[p - 2];
                new_data[1] = (byte)s[p - 1];
                return new_data;
            }

            /*Match m = validateReading.Match(search.ToString());
            if (m.Success)
            {
                // select matches = bytes in order
                StringBuilder matches = new StringBuilder((m.Groups.Count - 1) * Properties.DataLength);
                // skip first match, concatenate rest
                for (int i = 1; i < m.Groups.Count; i++)
                {
                    matches.Append(m.Groups[i].Value);
                }
                
                string matches_text = matches.ToString();

                // convert to bytes
                byte[] new_data = new byte[matches_text.Length];
                for (int i = 0; i < matches_text.Length; i++)
                {
                    new_data[i] = (byte)matches_text[i];
                }

                return new_data;
            }*/

            string wrong_data = search.ToString();
            
            //log2.Write("Error data: (raw)" + wrong_data + "    " + String.Join(" ", SensorFunctions.str2hex(wrong_data)));
            return null;
        }

        protected override int GetDigitalValue(byte[] data)
        {
            string b1 = SensorFunctions.dec2bin(data[0]);
            string b2 = SensorFunctions.dec2bin(data[1]);

            return Convert.ToInt32(b1.Substring(1) + b2, 2);
        }

        protected override SensorReading.DigitalErrors IsDigitalValueError(int digitalValue)
        {
            // if reading is 0 => error
            // chose 32370 (instead of 32627) as occasionally, random peaks to readings > 19.26 which would mess up results
            if (digitalValue <= 0 || digitalValue >= 32627)
            {
                return SensorReading.DigitalErrors.UNKNOWN_ERROR;
            }

            return SensorReading.DigitalErrors.NO_ERROR;
        }

        protected override double CalculateReading(int digitalValue)
        {
            //// TODO: removed SMR from here
            double value = (double)digitalValue / 32767.0 * Properties.MR;

            if (ReadingReference == ReadingReferences.SENSOR)
                value += Properties.SMR;

            return value;
        }

        protected override bool TestConnection()
        {
            try
            {
                // try reading measuring rate
                string[] response = Run(Commands["SRA"], "?");
                bool success = (ProcessResponse(response) != "error");
                return success;
            }
            catch
            {
                return false;
            }
        }

        protected override void LoadSensorParameters()
        {
            // get frequency
            string response = ProcessResponse(Run(Commands["FRQ"], "?"));
            
            if (response == "error")
            {
                throw new ApplicationException("Could not read frequency");
            }
            
            // set value of frequency
            Parameters["frequency"].SetValue(response);
        }

        protected override void InitReading()
        {
            // set to displacement, averaging = 1, binary mode
            string response;
            try
            {
                Properties.DataCount = 1;
                Properties.ResponseLength = Properties.DataLength * Properties.DataCount + Properties.FrameSeparator.Length + (Properties.DataCount - 1) * Properties.DataSeparator.Length;
                // length of data values + length of frame separators + length of data separators

                // set displacement measuring mode, averaging = 1, binary output
                response = ProcessResponse(Run(Commands["MOD"], "0"));
                if (response == "error") throw new ApplicationException("Unable to set measurement mode");

                ///// TODO: now CCL = 1 => internal???
                // need it at CCL = 0 for Udo, CCL = 1 for Sahil
                string value = "0"; // for Udo
                if (System.IO.Directory.Exists(@"E:\cygwin"))
                    value = "1"; // for Sahil
                response = ProcessResponse(Run(Commands["CCL"], value)); // keep it at 0 for Udo's sensor
                if (response == "error") throw new ApplicationException("Unable to set internal light source");
                
                response = ProcessResponse(Run(Commands["AVR"], "1"));
                if (response == "error") throw new ApplicationException("Unable to set averaging");

                response = ProcessResponse(Run(Commands["BIN"]));
                if (response == "error") throw new ApplicationException("Unable to set output type to binary");
                                
                // set transmission frames to displacement only
                response = ProcessResponse(Run(Commands["SOD"], "1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0"));
                if (response == "error") throw new ApplicationException("Unable to set measurement mode");
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Unable to initialise readings", ex);
            }
        }
    }
}
