﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SensorControl.Sensors
{
    public class ILD1700_MESensor : MESensor
    {
        public ILD1700_MESensor()
        {
            this.sensorModel = MEDAQLib.ME_SENSOR.SENSOR_ILD1700;

            Properties.ModelName = "ILD1700";
            Properties.ResponseLength = 2;

            Properties.CommandStart = "\x2b\x2b\x2b\x0d\x49\x4c\x44\x31";
            Properties.CommandClose = "";

            Properties.ReplyStart = new string[] { "\x49\x4c\x44\x31" };
            Properties.ReplyClose = new string[] { "\x20\x20\x0d\x0a" };

            Properties.DataSeparator = "";
            Properties.FrameSeparator = "";
            Properties.DataCount = 1;
            Properties.DataLength = 2;

            Properties.MR = 2;
            Properties.SMR = 24;
            
            // added
            baudRates = new int[] { 115200, 57600, 19200, 9600 };
            frequencies = new string[] { "2500", "1250", "625", "312.5" };

            // add frequency parameter
            Parameters.Add("frequency", new SensorParameter(
                delegate(string value)
                {
                    return "2500";
                }));
        }

        protected override bool TestConnection()
        {
            MEDAQLib.ERR_CODE iRet = MEDAQLib.SetParameterString(sensor, "S_Command", "Dat_Out_On");
            if (iRet != MEDAQLib.ERR_CODE.ERR_NOERROR)
            {
                return false;
            }

            iRet = MEDAQLib.SensorCommand(sensor);
            if (iRet != MEDAQLib.ERR_CODE.ERR_NOERROR)
            {
                return false;
            }

            return true;
        }

        protected override void InitReading()
        {
            MEDAQLib.ClearAllParameters(sensor);
            MEDAQLib.SetParameterString(sensor, "S_Command", "Dat_Out_On");
            MEDAQLib.SensorCommand(sensor);
        }

        protected override double CalculateReading(int digitalValue)
        {
            double value = (digitalValue * 1.02 / 16368 - 0.01) * Properties.MR;

            if (ReadingReference == ReadingReferences.SENSOR)
                value += Properties.SMR;

            return value;
        }
    }
}
