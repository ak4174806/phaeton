﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SensorControl.Sensors
{
    public abstract class BaseSensor
    {
        #region Member Variables

        protected SensorProperties properties;
        protected Dictionary<string, SensorCommand> commands;
        protected Dictionary<string, SensorParameter> parameters;
        protected int[] baudRates;
        protected string[] frequencies;

        protected ISerialPortBuffer portBuffer;

        // offset from zero to use
        protected double zeroOffset;

        /// <summary>
        /// The time since starting, of the next reading
        /// </summary>
        protected double currentTime;

        /// <summary>
        /// The increment applied on the time between each reading, in seconds
        /// </summary>
        protected double timeIncrement;

        protected bool filterErrors;

        protected ReadingReferences readingReference;

        #endregion

        #region Properties

        /// <summary>
        /// Stores properties of the sensor
        /// </summary>
        public SensorProperties Properties
        {
            get { return properties; }
        }

        /// <summary>
        /// Stores commands that can be run on the sensor
        /// </summary>
        public Dictionary<string, SensorCommand> Commands
        {
            get { return commands; }
        }

        /// <summary>
        /// Stores parameters that can be changed for the sensor
        /// </summary>
        public Dictionary<string, SensorParameter> Parameters
        {
            get { return parameters; }
        }

        /// <summary>
        /// The possible baud rates for the sensor
        /// </summary>
        public int[] BaudRates
        {
            get { return baudRates; }
        }

        /// <summary>
        /// The possible frequencies for this sensor
        /// </summary>
        public string[] Frequencies
        {
            get { return frequencies; }
        }

        /// <summary>
        /// Buffers data from the receive line of the serial port
        /// </summary>
        public ISerialPortBuffer PortBuffer
        {
            get { return portBuffer; }
        }

        /// <summary>
        /// The offset from zero to use for the readings
        /// </summary>
        public double ZeroOffset
        {
            get { return zeroOffset; }
            set { zeroOffset = value; }
        }

        /// <summary>
        /// The time returned with the next reading (if requested)
        /// </summary>
        public double CurrentTime
        {
            get { return currentTime; }
            set { currentTime = value; }
        }

        /// <summary>
        /// The increment to use when adjusting the time
        /// </summary>
        public double TimeIncrement
        {
            get { return timeIncrement; }
            set { timeIncrement = value; }
        }

        /// <summary>
        /// Whether TakeReadings should filter out errors
        /// </summary>
        public bool FilterErrors
        {
            get { return filterErrors; }
            set { filterErrors = value; }
        }

        /// <summary>
        /// The location where readings should be taken from
        /// </summary>
        public ReadingReferences ReadingReference
        {
            get { return readingReference; }
            set { readingReference = value; }
        }

        #endregion

        public BaseSensor()
        {
            properties = new SensorProperties();
            commands = new Dictionary<string, SensorCommand>();
            parameters = new Dictionary<string, SensorParameter>();
                        
            // initialise zeroOffset and timing
            ZeroOffset = 0;
            CurrentTime = -1;
            TimeIncrement = -1;

            // default => don't filter errors
            FilterErrors = false;

            // return readings from the sensor
            ReadingReference = ReadingReferences.SENSOR;
        }
    }
}
