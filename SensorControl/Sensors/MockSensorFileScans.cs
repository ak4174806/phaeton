﻿using System;
using System.Collections.Generic;
using System.Text;

using System.IO;
using Analysis;

namespace SensorControl.Sensors
{
    public class MockSensorFileScans : MockSensor
    {
        public MockSensorFileScans(string folder)
            : base()
        {
            if (!Directory.Exists(folder)) throw new ArgumentException("Folder doesn't exist in MockSensorFiles", "folder");

            Folder = folder;
            CurrentFile = 0;

            if (FileCount == 0) throw new ApplicationException("No scan files in folder for MockSensorFiles");

            Properties.ModelName = "Mock Sensor Files";
            rand = new Random();
        }

        protected string Folder
        {
            get;
            set;
        }
        
        protected string[] Files
        {
            get { return Directory.GetFiles(Folder, "*.csv"); }
        }

        private int currentFile;
        protected int CurrentFile
        {
            get { return currentFile % FileCount; }
            set
            {
                if (FileCount > 0) currentFile = value % FileCount;
                else currentFile = value;
            }
        }

        protected int FileCount
        {
            get { return Files.Length; }
        }

        public override int DataAvail
        {
            get
            {
                int length = 0;
                if (currentReadingIndex < 0)
                {
                    DataIO reader = new DataIO();
                    reader.Mode = DataIO.Modes.Read;
                    reader.Filename = Files[CurrentFile];

                    double[] data_x, data_y;
                    reader.ReadData(out data_x, out data_y);
                    length = data_x.Length;
                }
                else length = currentReadings.Length;

                return Math.Min(rand.Next(5000, 10000), length);
            }
        }

        private int currentReadingIndex = -1;
        private SensorReading[] currentReadings;
        private Random rand;

        public override SensorReading[] TakeReadings(int count)
        {
            if (currentReadingIndex < 0)
            {
                // simulate errors at beginning
                System.Threading.Thread.Sleep(1000);

                DataIO reader = new DataIO();
                reader.Mode = DataIO.Modes.Read;
                reader.Filename = Files[CurrentFile];

                double[] data_x, data_y;
                reader.ReadData(out data_x, out data_y);

                SensorReading[] output = new SensorReading[data_x.Length];
                for (int i = 0; i < data_x.Length; i++)
                {
                    output[i] = new SensorReading(data_y[i], data_x[i]);
                }

                currentReadings = output;
                currentReadingIndex = 0;
            }
            else
            {
                int n = Math.Min(count, currentReadings.Length - currentReadingIndex);
                int errors = 0;
                if (n + currentReadingIndex == currentReadings.Length)
                {
                    errors = 1000;
                }
                SensorReading[] output = new SensorReading[n + errors];
                for (int i = 0; i < n; i++)
                    output[i] = currentReadings[currentReadingIndex + i];

                currentReadingIndex += n;
                if (errors > 0)
                {
                    currentReadingIndex = -1;
                    CurrentFile = CurrentFile + 1;
                    for (int i = 0; i < errors; i++) output[i + n] = new SensorReading(SensorReading.DigitalErrors.OUT_OF_RANGE_FAR);
                }

                System.Threading.Thread.Sleep(20);
                return output;
            }

            return TakeReadings(count);
        }
    }
}
