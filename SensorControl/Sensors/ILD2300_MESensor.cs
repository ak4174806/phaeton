﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SensorControl.Sensors
{
    public class ILD2300_MESensor : Ethernet_MESensor
    {
        public ILD2300_MESensor()
        {
            this.sensorModel = MEDAQLib.ME_SENSOR.SENSOR_ILD2300;

            Properties.ModelName = "ILD2300";
            Properties.ResponseLength = 3;

            Properties.CommandStart = "\x2b\x2b\x2b\x0d\x49\x4c\x44\x31";
            Properties.CommandClose = "";

            Properties.ReplyStart = new string[] { "\x49\x4c\x44\x31" };
            Properties.ReplyClose = new string[] { "\x20\x20\x0d\x0a" };

            Properties.DataSeparator = "";
            Properties.FrameSeparator = "";
            Properties.DataCount = 1;
            Properties.DataLength = 3;

            Properties.MR = 15;
            Properties.SMR = 40;
            

            // added
            baudRates = new int[] { 691200 };
            frequencies = new string[] { "10000" };

            // add frequency parameter
            Parameters.Add("frequency", new SensorParameter(
                delegate(string value)
                {
                    return "10000";
                }));
        }

        protected override bool TestConnection()
        {
            MEDAQLib.ERR_CODE iRet = MEDAQLib.SetParameterString(sensor, "S_Command", "Get_Info");
            if (iRet != MEDAQLib.ERR_CODE.ERR_NOERROR)
            {
                return false;
            }

            iRet = MEDAQLib.SensorCommand(sensor);
            if (iRet != MEDAQLib.ERR_CODE.ERR_NOERROR)
            {
                return false;
            }

            // wait for answer
            System.Threading.Thread.Sleep(100);
            double range = 0;
            iRet = MEDAQLib.GetParameterDouble(sensor, "SA_Range", ref range);
            if (iRet != MEDAQLib.ERR_CODE.ERR_NOERROR)
            {
                return false;
            }
            Properties.MR = range;

            return true;
        }

        protected override void InitReading()
        { }

        protected override double CalculateReading(int digitalValue)
        {
            double value = (digitalValue * 1.02 / 65520 - 0.01) * Properties.MR;

            if (ReadingReference == ReadingReferences.SENSOR)
                value += Properties.SMR;

            return value;
        }
    }
}
