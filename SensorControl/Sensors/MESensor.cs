﻿using System;
using System.Collections.Generic;
using System.Text;

using SensorControl;
using NLog;

namespace SensorControl.Sensors
{
    public abstract class MESensor : BaseSensor, ISensor
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        protected bool connected = false;
        protected UInt32 sensor = 0;
        protected MEDAQLib.ME_SENSOR sensorModel;

        protected SensorAddressTypes sensorAddressType = SensorAddressTypes.COM_Port;
        public SensorAddressTypes SensorAddressType
        {
            get { return sensorAddressType; }
        }
        
        public MESensor()
        {
            this.OnTakingReadings += new SensorEventHandler(EnsureReadingsInitialised);

            // added to mock serial port buffer
            portBuffer = new MockSerialPortBuffer(new MockSerialPortBuffer.FlushPortDelegate(FlushPort));
        }


        public string[] SensorTypes
        {
            get { return new string[] { "Autodetect" }; }
        }
        public void SetSensorType(string type)
        {
            return;
        }
        public void Connect(string portName, int baudRate)
        {
            Connect(portName);
        }

        public SensorReading[] TakeReadings()
        {
            return TakeReadings(DataAvail);
        }
        public SensorReading TakeReading()
        {
            return TakeReadings(1)[0];
        }

        
        // todo
        public int BaudRate
        {
            get { return (baudRates.Length > 0 ? baudRates[0] : -1); }
        }

        public int DataAvail
        {
            get 
            {
                int iAvail = 0;

                MEDAQLib.ERR_CODE iRet = MEDAQLib.DataAvail(sensor, ref iAvail);
                if (iRet != MEDAQLib.ERR_CODE.ERR_NOERROR)
                {
                    Error("DataAvail", sensor, iRet);
                    return -1;
                }

                return iAvail;
            }
        }

        public void FlushPort()
        {
            // clear parameters
            MEDAQLib.ClearAllParameters(sensor);

            // read all data
            int iAvail = 0;
            MEDAQLib.DataAvail(sensor, ref iAvail);
            if (iAvail > 0)
            {
                int[] iRawData = new Int32[iAvail];
                double[] dScaledData = new Double[iAvail];
                int iRead = 0;

                // read all current data
                MEDAQLib.ERR_CODE iRet = MEDAQLib.TransferData(sensor, iRawData, dScaledData, iAvail, ref iRead);
            }
        }


        protected MEDAQLib.ERR_CODE Error(String sError, UInt32 iSensor)
        {
            StringBuilder sbSensorError = new StringBuilder(1024);
            MEDAQLib.GetError(iSensor, sbSensorError, (UInt32)sbSensorError.Capacity);

            throw new ApplicationException("Error in function: " + sError + "\n" + sbSensorError);
        }

        protected MEDAQLib.ERR_CODE Error(String sError, UInt32 iSensor, MEDAQLib.ERR_CODE iErr)
        {
            return Error(sError, sensor);
        }
        
        public virtual void Connect(string portName)
        {
            // if an old sensor
            if (sensor != 0)
            {
                Disconnect();
            }

            // create sensor instance
            sensor = MEDAQLib.CreateSensorInstance(sensorModel);
            if (sensor == 0)
            {
                throw new ApplicationException("CreateSensorInstance: Error occured, no sensor created");
            }

            // set up connection parameters
            MEDAQLib.ERR_CODE iRet = MEDAQLib.ERR_CODE.ERR_NOERROR;

            iRet = MEDAQLib.SetParameterString(sensor, "IP_Interface", "RS232");
            if (iRet != MEDAQLib.ERR_CODE.ERR_NOERROR)
            {
                MEDAQLib.ReleaseSensorInstance(sensor);
                Error("SetParameterString(IP_Interface, RS232)", sensor, iRet);
            }
            iRet = MEDAQLib.SetParameterString(sensor, "IP_Port", portName);
            if (iRet != MEDAQLib.ERR_CODE.ERR_NOERROR)
            {
                MEDAQLib.ReleaseSensorInstance(sensor);
                Error("SetParameterString(IP_Port, " + portName + ")", sensor, iRet);
            }

            // set error handling behaviour - set to -1
            iRet = MEDAQLib.SetParameterInt(sensor, "IP_ScaleErrorValues", 2);
            if (iRet != MEDAQLib.ERR_CODE.ERR_NOERROR)
            {
                MEDAQLib.ReleaseSensorInstance(sensor);
                Error("SetParameterInt(IP_ScaleErrorValues, 2)", sensor, iRet);
            }

            // open connection
            iRet = MEDAQLib.OpenSensor(sensor);
            if (iRet != MEDAQLib.ERR_CODE.ERR_NOERROR)
            {
                MEDAQLib.ReleaseSensorInstance(sensor);
                Error("OpenSensor (RS232, COM5)", sensor, iRet);
            }
            connected = true;

            // check if actually connected
            if (!TestConnection())
            {
                Disconnect();
                Error("Could not connect to sensor.", sensor);
            }
        }

        public void Disconnect()
        {
            if (connected)
            {
                MEDAQLib.CloseSensor(sensor);
                connected = false;
            }   
            if (sensor != 0)
            {
                MEDAQLib.ReleaseSensorInstance(sensor);
            }
        }

        private int errorCount = 0;

        
        public SensorReading[] TakeReadings(int count)
        {
            int avail = DataAvail;
            if (avail == -1)
            {
                throw new ApplicationException("Could not find amount of available data");
            }

            count = System.Math.Min(avail, count);

            int[] iRawData = new Int32[count];
            double[] dScaledData = new Double[count];

            int iRead = 0;
            MEDAQLib.ERR_CODE iRet = MEDAQLib.TransferData(sensor, iRawData, dScaledData, count, ref iRead);
            if (iRet != MEDAQLib.ERR_CODE.ERR_NOERROR)
            {
                Error("TransferData", sensor, iRet);
            }


            // process readings
            SensorReading[] readings = new SensorReading[iRead];
            SensorReading reading;
            
            int j = 0;
            for (int i = 0; i < iRead; i++)
            {
                if (dScaledData[i] <= 0.0)
                {
                    // log error information for ILD2200 / ILD2220
                    if (Properties.ModelName == "ILD2200 / ILD2220" )
                    {
                        errorCount = (errorCount + 1) % 500;
                        if (errorCount == 0)
                        {
                            logger.Trace("Error reading: Raw data = {0}, Scaled data = {1}, Masked Raw Data = {2}, MR = {3}", iRawData[i], dScaledData[i], iRawData[i] & 0xffff, Properties.MR);
                        }                        
                    }
                    
                    // error data
                    if (dScaledData[i] <= 0.0)
                        reading = new SensorReading(SensorReading.DigitalErrors.OUT_OF_RANGE_CLOSE);
                    else
                        reading = new SensorReading(SensorReading.DigitalErrors.OUT_OF_RANGE_FAR);
                    reading.Time = CurrentTime;
                }
                else
                {
                    if (Properties.ModelName == "ILD2300")
                    {
                        reading = new SensorReading(dScaledData[i] - ZeroOffset, CurrentTime);
                    }
                    else
                    {
                        // others - use this??
                        reading = new SensorReading(CalculateReading((int)dScaledData[i]) - ZeroOffset, CurrentTime);
                    }
                }

                // increment time regardless of error
                if (TimeIncrement != -1)
                {
                    CurrentTime += TimeIncrement;
                }

                // apply filtering
                if (!reading.IsError || !FilterErrors)
                {
                    readings[j++] = reading;
                }
            }

            // trim readings
            if (j < iRead)
                Array.Resize(ref readings, j);

            return readings;
        }


        #region Sensor Initialisation

        public delegate void SensorEventHandler();
        protected event SensorEventHandler OnTakingReadings;

        private bool readingsInitialised = false;
        /// <summary>
        /// Used to initialise readings so that first reading has no extra delay
        /// </summary>
        public void EnsureReadingsInitialised()
        {
            if (!readingsInitialised)
            {
                InitReading();
                this.OnTakingReadings -= EnsureReadingsInitialised;
                readingsInitialised = true;
            }
        }

        protected abstract bool TestConnection();

        /// <summary>
        /// Called before taking the first reading(s)
        /// </summary>
        protected abstract void InitReading();

        /// <summary>
        /// Calculates a reading given the digital value
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        abstract protected double CalculateReading(int digitalValue);

        #endregion
    }
}
