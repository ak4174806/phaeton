﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SensorControl.Sensors
{
    public class MockSensor : BaseSensor, ISensor
    {
        Random gen;

        public MockSensor()
        {
            gen = new Random();

            portBuffer = new MockSerialPortBuffer(new MockSerialPortBuffer.FlushPortDelegate(FlushPort));

            Properties.ModelName = "Mock Sensor";
            Properties.ResponseLength = 3;
            Properties.CommandStart = "";
            Properties.CommandClose = "";
            Properties.ReplyStart = new string[] { "" };
            Properties.ReplyClose = new string[] { "" };
            Properties.DataSeparator = "";
            Properties.FrameSeparator = "";
            Properties.DataCount = 1;
            Properties.DataLength = 3;
            Properties.MR = 2;
            Properties.SMR = 24;

            // added
            baudRates = new int[] { 19200, 691200 };
            frequencies = new string[] { "10000" };

            // add frequency parameter
            Parameters.Add("frequency", new SensorParameter(
                delegate(string value)
                {
                    return "10000";
                }));
        }

        public string[] SensorTypes
        {
            get { return new string[] { "Autodetect" }; }
        }
        public int BaudRate
        {
            get { return (baudRates.Length > 0 ? baudRates[0] : -1); }
        }
        public SensorAddressTypes SensorAddressType
        {
            get { return SensorAddressTypes.COM_Port; }
        }
        
        public void FlushPort()
        { }
        public void SetSensorType(string type)
        { }
        public void Connect(string portName, int baudRate)
        { }
        public void Connect(string portName)
        { }
        public void Disconnect()
        { }

        public virtual int DataAvail
        {
            get { return gen.Next(49) + 1; }
        }

        public virtual SensorReading[] TakeReadings(int count)
        {
            SensorReading[] output = new SensorReading[count];
            int j = 0;
            for (int i = 0; i < count; i++)
            {
                bool isError = false;
                if (gen.Next(50) == 1) isError = true;
                
                if (!(isError & FilterErrors)) output[j++] = new SensorReading(gen.Next(200), CurrentTime);
                if (TimeIncrement != -1) CurrentTime += TimeIncrement;                
            }
            Array.Resize(ref output, j);
            return output;
        }

        public SensorReading[] TakeReadings()
        {
            return TakeReadings(DataAvail);
        }
        public SensorReading TakeReading()
        {
            return TakeReadings(1)[0];
        }

        public void EnsureReadingsInitialised()
        { }
    }
}
