﻿using System;
using System.Collections.Generic;
using System.Text;

using SensorControl;

namespace SensorControl.Sensors
{
    public interface ISensor
    {
        SensorProperties Properties { get; }
        Dictionary<string, SensorCommand> Commands { get; }
        Dictionary<string, SensorParameter> Parameters { get; }

        int[] BaudRates { get; }
        string[] Frequencies { get; }

        ISerialPortBuffer PortBuffer { get; }
        string[] SensorTypes { get; }
        SensorAddressTypes SensorAddressType { get; }

        double ZeroOffset { get; set; }
        double CurrentTime { get; set; }
        double TimeIncrement { get; set; }

        bool FilterErrors { get; set; }
        ReadingReferences ReadingReference { get; set; }

        int BaudRate { get; }
        int DataAvail { get; }

        void FlushPort();

        void SetSensorType(string type);

        void Connect(string portName, int baudRate);
        void Connect(string portName);
        void Disconnect();

        SensorReading[] TakeReadings(int count);
        SensorReading[] TakeReadings();
        SensorReading TakeReading();

        void EnsureReadingsInitialised();
    }

    public enum SensorAddressTypes
    {
        COM_Port,
        IP_Address
    }    
}
