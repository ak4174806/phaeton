﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SensorControl
{
    public class SensorProperties
    {
        public string ModelName;

        public double MR;

        public double SMR;

        // length of an entire response (all data + separators)
        public int ResponseLength;

        public string CommandStart;

        public string CommandClose;

        public string[] ReplyStart;

        public string[] ReplyClose;

        // separates each datum
        public string DataSeparator;

        // number of data values per measurement
        public int DataCount;

        // length of each datum
        public int DataLength;

        // separates each entire response
        public string FrameSeparator;
    }
}
