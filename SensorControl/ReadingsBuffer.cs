﻿using System;
using System.Collections.Generic;
using System.Text;

using System.Collections;
using System.ComponentModel;
using SensorControl.Sensors;
using System.Windows.Forms;

namespace SensorControl
{
    public class ReadingsBuffer
    {
        #region Private Members
        /// <summary>
        /// Buffers readings as they are taken
        /// </summary>
        private RollingArrayList readingBuffer;

        /// <summary>
        /// Buffers times of each reading
        /// </summary>
        private RollingArrayList timeBuffer;

        /// <summary>
        /// The sensor this buffers readings from
        /// </summary>
        private Sensor sensor;

        // worker used to buffer readings
        private BackgroundWorker worker;
        
        /// <summary>
        /// Timer used to automatically update buffer, in seconds
        /// </summary>
        protected Timer updateTimer;

        #endregion

        #region Events and Delegates

        public delegate void ReadingsBufferEventHandler<A, B>(A sender, B eventArgs);
        
        /// <summary>
        /// Raised whenever the reading buffer is updated
        /// </summary>
        public event ReadingsBufferEventHandler<Sensor, EventArgs> ReadingsBufferUpdated;
        #endregion
        
        public ReadingsBuffer(Sensor sensor, int capacity)
        {
            this.sensor = sensor;
            readingBuffer = new RollingArrayList();
            readingBuffer.MaxCapacity = capacity;

            timeBuffer = new RollingArrayList();
            timeBuffer.MaxCapacity = capacity;

            // set up background worker for buffering measurements
            worker = new BackgroundWorker();
            worker.WorkerReportsProgress = true;
            worker.WorkerSupportsCancellation = false;

            worker.DoWork += new DoWorkEventHandler(worker_DoWork);
            worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(worker_RunWorkerCompleted);

            // set up timer
            updateTimer = new Timer();
            updateTimer.Tick += new EventHandler(updateTimer_Tick);
        }
                
        #region Properties
        /// <summary>
        /// The buffered readings
        /// </summary>
        public RollingArrayList ReadingBuffer
        {
            get { return readingBuffer; }
        }

        /// <summary>
        /// The buffered times
        /// </summary>
        public RollingArrayList TimeBuffer
        {
            get { return timeBuffer; }
        }

        /// <summary>
        /// The sensor being buffered
        /// </summary>
        public Sensor Sensor
        {
            get { return sensor; }
        }

        /// <summary>
        /// The capacity of the buffer
        /// </summary>
        public int Capacity
        {
            get { return ReadingBuffer.MaxCapacity; }
            set { ReadingBuffer.MaxCapacity = value; }
        }

        // worker ??

        #endregion
                
        #region Reading Buffering

        public void BufferReadings(int count)
        {
            if (worker.IsBusy) return;
            
            // check connection is open
            if (!sensor.Connected)
            {
                throw new ApplicationException("No connection to the sensor");
            }
            
            // run and buffer readings
            worker.RunWorkerAsync((object)count);
        }

        public void BufferReadings()
        {
            if (worker.IsBusy) return;

            // check connection is open
            if (!sensor.Connected)
            {
                throw new ApplicationException("No connection to the sensor");
            }

            // find number of readings available
            int count = sensor.DataAvail;

            // take readings
            BufferReadings(count);
        }
        #endregion
        
        #region BackgroundWorker Event Handlers
        void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            // raise event
            if (this.ReadingsBufferUpdated != null)
            {
                ReadingsBufferUpdated(sensor, new EventArgs());
            }
        }

        void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = sender as BackgroundWorker;

            int count = (int)e.Argument;
            try
            {
                // take readings
                SensorReading[] readings = sensor.TakeReadings(count);
                lock (ReadingBuffer)
                {
                    lock (TimeBuffer)
                    {
                        // update time buffer
                        foreach (SensorReading r in readings)
                        {
                            ReadingBuffer.Add(r.Value);
                            if (r.HasTime)
                                TimeBuffer.Add(r.Time);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                e.Result = ex.Message;
            }

            e.Result = "";
        }
        #endregion       

        #region Retrieving Points

        /// <summary>
        /// Returns the last reading/time pair
        /// </summary>
        /// <param name="reading"></param>
        /// <param name="time"></param>
        public void GetLast(out double reading, out double time)
        {
            if (ReadingBuffer.Count > 0)
            {
                reading = (double)ReadingBuffer[ReadingBuffer.Count - 1];
                time = (double)TimeBuffer[TimeBuffer.Count - 1];
            }
            else
            {
                reading = -1;
                time = -1;
            }
        }
        
        /// <summary>
        /// Returns the last reading
        /// </summary>
        /// <param name="reading"></param>
        public void GetLast(out double reading)
        {
            if (ReadingBuffer.Count > 0)
            {
                reading = (double)ReadingBuffer[ReadingBuffer.Count - 1];
            }
            else
            {
                reading = -1;
            }
        }

        #endregion

        #region Auto Updating

        /// <summary>
        /// Automatically calls buffer readings
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void updateTimer_Tick(object sender, EventArgs e)
        {
            if (worker.IsBusy) return;

            BufferReadings();
        }

        /// <summary>
        /// Starts the buffer automatically updating
        /// </summary>
        /// <param name="interval"></param>
        public void AutoUpdate(int interval)
        {
            updateTimer.Interval = interval;
            updateTimer.Start();
        }

        /// <summary>
        /// Stops the buffer automatically updating
        /// </summary>
        public void StopAutoUpdate()
        {
            updateTimer.Stop();
        }

        #endregion

        #region Clear

        public void Clear()
        {
            ReadingBuffer.Clear();
            TimeBuffer.Clear();
        }

        #endregion
    }
}
