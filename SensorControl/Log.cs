﻿using System;
using System.Collections.Generic;
using System.Text;

using System.IO;

namespace SensorControl
{
    public class Log
    {
        private const bool debug = true;

        StreamWriter sw;

        private const string rootDir = @"d:\";

        public Log(string file)
        {
            if (!debug) return;

            // open file
            sw = new StreamWriter(rootDir + file, true);
        }

        public void Write(string message)
        {
            Write(message, true);
        }

        public void Write(string message, bool prefixTime)
        {
            if (!debug) return;

            if (prefixTime)
            {
                sw.WriteLine(timestamp() + " - " + message);
            }
            else
            {
                sw.WriteLine(message);
            }
        }

        private string timestamp()
        {
            return System.DateTime.Now.ToString();
        }

        public void Flush()
        {
            if (!debug) return;

            sw.Flush();
        }
    }
}
