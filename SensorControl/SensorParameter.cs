﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SensorControl
{
    public class SensorParameter
    {
        #region Private Members

        // the value most recently set
        private string value;
        // null, or the value before a change is committed
        private string currentValue;

        private bool canSet;

        // whether there is a change that can be committed
        private bool isChanged;

        // the parameter setter
        private SetParameter setter;

        // the parameter getter
        private GetParameter getter;

        #endregion

        #region Constructors
        /// <summary>
        /// Wraps a parameter. canSet indicates whether the parameter is readonly. setter is called when setting
        /// </summary>
        /// <param name="canSet"></param>
        /// <param name="setter"></param>
        public SensorParameter(bool canSet, SetParameter setter)
        {
            this.canSet = canSet;
            this.setter = setter;
            
            value = "";
            currentValue = "";
            isChanged = false;
        }

        /// <summary>
        /// Wraps a parameter. canSet determines if it is readonly.
        /// </summary>
        /// <param name="canSet"></param>
        public SensorParameter(bool canSet)
            : this(canSet, default(SetParameter)) { }

        /// <summary>
        /// Wraps a parameter, automatically readonly
        /// </summary>
        public SensorParameter()
            : this(false, default(SetParameter)) { }

        /// <summary>
        /// Wraps a parameter, if define a getter and not readonly, then must define setter
        /// </summary>
        /// <param name="getter"></param>
        /// <param name="canSet"></param>
        /// <param name="setter"></param>
        public SensorParameter(GetParameter getter, SetParameter setter)
            : this(true, setter)
        {
            this.getter = getter;
        }
        
        /// <summary>
        /// Wraps a parameter, readonly with getter
        /// </summary>
        /// <param name="getter"></param>
        public SensorParameter(GetParameter getter)
            : this(false, default(SetParameter))
        {
            this.getter = getter;
        }

        #endregion

        #region Events and Delegates

        /// <summary>
        /// Actually sets the parameter, can raise exceptions for errors
        /// </summary>
        /// <param name="value">The value of the parameter</param>
        public delegate void SetParameter(string value);

        /// <summary>
        /// Actually gets the parameter, can raise exceptions for errors
        /// </summary>
        /// <returns></returns>
        public delegate string GetParameter(string value);

        // a delegate for all event handlers
        public delegate void SensorParameterEventHandler<A, B>(A sender, B eventArgs);

        // the event arguments
        public class SensorParameterEventArgs : System.EventArgs 
        {
            public string Value;

            public SensorParameterEventArgs(string Value)
            {
                this.Value = Value;
            }
        }

        public class SensorParameterCancellableEventArgs : System.EventArgs
        {
            public bool Cancel = false;
            public string OldValue;
            public string NewValue;

            public SensorParameterCancellableEventArgs(string OldValue, string NewValue)
            {
                this.OldValue = OldValue;
                this.NewValue = NewValue;
            }
        }

        public class SensorParameterErrorEventArgs : SensorParameterEventArgs
        {
            public string Error = "";

            public SensorParameterErrorEventArgs(string Value)
                : base(Value)
            { }
        }

        // the events
        public event SensorParameterEventHandler<SensorParameter, SensorParameterCancellableEventArgs> ValueChangingEvent;
        public event SensorParameterEventHandler<SensorParameter, SensorParameterEventArgs> ValueChangedEvent;
        public event SensorParameterEventHandler<SensorParameter, SensorParameterEventArgs> ValueChangeCancelledEvent;
        public event SensorParameterEventHandler<SensorParameter, SensorParameterErrorEventArgs> ValueChangeErrorEvent;
        
        #endregion

        #region Properties
        public bool CanSet 
        {
            get { return canSet; }
        }

        public bool IsChanged
        { 
            get { return isChanged; } 
        }
        #endregion

        /// <summary>
        /// Gets the value of the parameter
        /// </summary>
        /// <returns></returns>
        public string Get()
        {
            if (getter != null)
            {
                if (value != null)
                    return getter(value);
                else
                    return getter("");
            }

            return value;
        }
        
        #region Setting Parameter

        /// <summary>
        /// Sets the value of the parameter. Returns whether value is what is wanted
        /// </summary>
        /// <param name="value">The value to set</param>
        /// <param name="commit">Whether to automatically commit changes to sensor, or wait until Commit is called</param>
        /// <param name="forceSet">Whether to force the set to occur, even if the new value is the same</param>
        public bool Set(string value, bool commit, bool forceSet) 
        {
            if (!CanSet)
            {
                throw new ApplicationException("Tried to set readonly parameter");
            }

            // check if value is different
            if (!forceSet && this.value == value)
            {
                return true;
            }

            // raise event
            if (ValueChangingEvent != null)
            {
                // create event arguments
                SensorParameterCancellableEventArgs args = new SensorParameterCancellableEventArgs(this.value, value);

                ValueChangingEvent(this, args);
                // if cancelled
                if (args.Cancel)
                {
                    return false;
                }
            }

            // store old and new value
            this.currentValue = this.value;
            this.value = value;

            // mark parameter as changed
            this.isChanged = true;

            // commit if asked to
            if (commit)
            {
                return CommitSet();
            }

            return true;
        }

        #region Set Overloads
        /// <summary>
        /// Sets the value of the parameter. If the value is the same, does nothing
        /// </summary>
        /// <param name="value">The value to set</param>
        /// <param name="commit">Whether to automatically commit changes to sensor, or wait until Commit is called</param>
        public bool Set(string value, bool commit)
        {
            return Set(value, commit, false);
        }

        /// <summary>
        /// Sets the value of the parameter. If the value is the same, does nothing. Auto-commits changes so do not need to call Commit
        /// </summary>
        /// <param name="value">The value to set</param>
        public bool Set(string value)
        {
            return Set(value, true, false);
        }
        #endregion

        /// <summary>
        /// Sets the value of the parameter without calling setter. For example, setting default value
        /// </summary>
        /// <param name="value"></param>
        public void SetValue(string value) 
        {
            this.value = value;
            this.currentValue = "";

            // don't affect IsChanged flag, no events
        }

        public bool CommitSet()
        {
            if (!CanSet) throw new ApplicationException("Tried to commit to a readonly parameter");
            if (!IsChanged) return true;

            // try setting sensor parameter
            string error = "";
            if (setter != null)
            {
                try
                {
                    setter(this.value);
                }
                catch (Exception ex)
                {
                    error = ex.Message;
                }
            }

            if (error == "")
            {
                // setter was successful
                SetValue(this.value);

                if (ValueChangedEvent != null)
                {
                    // pass "new" value
                    SensorParameterEventArgs args = new SensorParameterEventArgs(this.value);
                    ValueChangedEvent(this, args);
                }

                isChanged = false;
                return true;
            }
            else
            {
                // setter failed
                undoSet();

                // pass "old" value
                if (ValueChangeErrorEvent != null)
                {
                    SensorParameterErrorEventArgs args = new SensorParameterErrorEventArgs(this.value);
                    args.Error = error;
                    ValueChangeErrorEvent(this, args);
                }

                isChanged = false;
                return false;
            }
        }
        
        /// <summary>
        /// Reverts changes from a Set
        /// </summary>
        public void CancelSet() 
        {
            if (!CanSet) throw new ApplicationException("Tried to cancel changes to a readonly parameter");
            if (!IsChanged) return;
            undoSet();

            if (ValueChangeCancelledEvent != null)
            {
                // pass "old" value
                SensorParameterEventArgs args = new SensorParameterEventArgs(this.value);
                ValueChangeCancelledEvent(this, args);
            }

            isChanged = false;
        }

        /// <summary>
        /// Reverts changes from a Set, no events
        /// </summary>
        private void undoSet()
        {
            this.value = this.currentValue;
            this.currentValue = "";

            // don't affect IsChanged flag or raise events
        }

        #endregion
    }
}
