﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SensorControl
{
    public class MockSerialPortBuffer : ISerialPortBuffer
    {        
        public event EventHandler SerialPortBufferUpdated;

        public delegate void FlushPortDelegate();
        FlushPortDelegate flushPortDelegate;

        public MockSerialPortBuffer(FlushPortDelegate flushPortDelegate)
        {
            this.flushPortDelegate = flushPortDelegate;
        }

        public int BufferSize()
        {
            return 0;
        }

        public void Clear()
        {
            flushPortDelegate();
        }

        public void Update()
        {
            return;
        }

        public string Read(int count)
        {
            return "";
        }

        public void AutoUpdate(int interval)
        {
            return;
        }

        public void StopAutoUpdate()
        {
            return;
        }
    }
}
