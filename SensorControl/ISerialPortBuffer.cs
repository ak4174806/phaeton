﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SensorControl
{
    public interface ISerialPortBuffer
    {
        event EventHandler SerialPortBufferUpdated;

        int BufferSize();

        void Clear();
        void Update();

        string Read(int count);

        void AutoUpdate(int interval);
        void StopAutoUpdate();
    }
}
