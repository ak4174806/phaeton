﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SensorControl
{
    public class DataAnalysis
    {
        /// <summary>
        /// Averages data in blocks and returns the averaged data
        /// </summary>
        /// <param name="data"></param>
        /// <param name="blockSize">The size of the blocks</param>
        /// <returns></returns>
        public static SensorReading[] BlockAverageData(SensorReading[] data, int blockSize)
        {
            if (blockSize == 1) return data;

            // round up number of blocks
            int new_length = (int)Math.Ceiling((double)data.Length / blockSize);
            SensorReading[] new_data = new SensorReading[new_length];

            // sum of each entry in the block
            double running_sum = 0;
            // total length of data
            int datalength = data.Length;
            // index to store this block
            int index;
            // the number of non-errors in a block
            int count = 0;
            // the time to ascribe this block
            double time = -1.0;
            
            // go through each block of data
            for (int i = 0; i < datalength; i++)
            {
                // ignore errors
                if (!data[i].IsError)
                {
                    running_sum += data[i].Value;
                    count++;

                    // set time to first data that has a time
                    if (time == -1.0 && data[i].HasTime)
                        time = data[i].Time;
                }

                // create new block
                if ((i + 1) % blockSize == 0 || i + 1 == datalength)
                {
                    // integer division
                    index = i / blockSize;

                    if (count > 0)
                    {
                        // some non-errors, average them
                        new_data[index] = new SensorReading(running_sum / count);
                        if (time != -1.0)
                            new_data[index].Time = time;
                    }
                    else
                    {
                        // all readings were errors, use error from any (last) as the error
                        new_data[index] = new SensorReading(data[i].Error);
                    }
                    
                    // reset for next block
                    running_sum = 0;
                    count = 0;
                    time = -1.0;
                }
            }

            return new_data;
        }



        /// <summary>
        /// Filters data keeping every filterSize'th datapoint
        /// </summary>
        /// <param name="data"></param>
        /// <param name="filterSize">The size of the blocks</param>
        /// <returns></returns>
        public static SensorReading[] FilterData(SensorReading[] data, int filterSize)
        {
            if (filterSize == 1) return data;

            // round up number of blocks
            int new_length = (int)Math.Ceiling((double)data.Length / filterSize);
            SensorReading[] new_data = new SensorReading[new_length];

            // total length of data
            int datalength = data.Length;
            // index to store this block
            int index;
            // whether a non-error was found in this block
            bool nonErrorFound = false;
            SensorReading value = null;

            // go through each block of data
            for (int i = 0; i < datalength; i++)
            {
                // ignore errors
                if (!data[i].IsError && !nonErrorFound)
                {
                    nonErrorFound = true;
                    value = data[i];
                }

                // create new block
                if ((i + 1) % filterSize == 0 || i + 1 == datalength)
                {
                    // integer division
                    index = i / filterSize;

                    if (nonErrorFound)
                    {
                        // some non-errors, average them
                        new_data[index] = value;
                    }
                    else
                    {
                        // all readings were errors, use error from any (last) as the error
                        new_data[index] = new SensorReading(data[i].Error);
                    }

                    // reset for next block
                    nonErrorFound = false;
                    value = null;
                }
            }

            return new_data;
        }
    }
}
