﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SensorControl
{
    public class SensorFunctions
    {
        /// <summary>
        /// Returns a 8-bit 0-padded binary representation of x
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public static string dec2bin(int x)
        {
            return Convert.ToString(x, 2).PadLeft(8, '0');
        }

        /// <summary>
        /// Converts string to array of hex representing each char
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string[] str2hex(string str)
        {
            string[] ret = new string[str.Length];

            for (int i = 0; i < str.Length; i++)
            {
                ret[i] = Convert.ToString((int)str[i], 16);
            }

            return ret;
        }

        /// <summary>
        /// Converts an array of bytes to a string
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string bytes2str(byte[] input)
        {
            StringBuilder output = new StringBuilder(input.Length);
            foreach (byte b in input)
            {
                output.Append((char)b);
            }
            return output.ToString();
        }
    }
}
