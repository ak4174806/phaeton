﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SensorControl
{
    public class SensorCommand
    {
        public static int DefaultTime = 200;
        
        private string name;
        private string command;
        private string value = "";
        private int time;

        #region Properties
        /// <summary>
        /// Descriptor of command
        /// </summary>
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        /// <summary>
        /// The actual command
        /// </summary>
        public string Command
        {
            get { return command; }
            set { command = value; }
        }

        /// <summary>
        /// The value to use with the command
        /// </summary>
        public virtual string Value
        {
            get { return value; }
            set { this.value = value; }
        }

        /// <summary>
        /// The translated value that can be sent to sensor
        /// </summary>
        public virtual string SensorValue
        {
            get
            {
                string sensorValue = value;

                if (Values != null && Values.ContainsKey(value))
                {
                    sensorValue = Values[value];
                }

                return sensorValue;
            }
            set
            {
                string sensorValue = value;
                foreach (string val in Values.Keys)
                {
                    if (sensorValue == Values[val])
                    {
                        this.value = val;
                        return;
                    }
                }

                this.value = sensorValue;
            }
        }

        /// <summary>
        /// The time to wait after the command in milliseconds
        /// </summary>
        public int Time
        {
            get { return time; }
            set { time = value; }
        }
        #endregion

        /// <summary>
        /// Maps some values to values that can be sent to sensor (ie: value => sensor value)
        /// </summary>
        public Dictionary<string, string> Values;


        public SensorCommand(string name, string command, int time)
        {
            this.name = name;
            this.command = command;
            this.time = time;

            Values = new Dictionary<string, string>();
        }

        public SensorCommand(string name, string command)
            :this(name, command, SensorCommand.DefaultTime) { }

        /// <summary>
        /// Gets command and translated value concatenated
        /// </summary>
        /// <returns></returns>
        public string GetCommand()
        {
            return Command + SensorValue;
        }
    }
}
