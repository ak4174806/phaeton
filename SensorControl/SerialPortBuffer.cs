﻿using System;
using System.Collections.Generic;
using System.Text;

using System.Threading;
using System.IO.Ports;
using System.ComponentModel;

namespace SensorControl
{
    public class SerialPortBuffer : ISerialPortBuffer
    {
        // default timeout in ms
        //private static int DefaultUpdateTime = 10;
        private static int DefaultUpdateTime = 200;

        private SerialPort port;
        private List<char> buffer;
        private int maxBufferSize;

        private BackgroundWorker updateWorker;
        private int updateInterval;

        // timeout in ms
        private int updateTime;

        // current index within buffer
        private int currentIndex;
        // when to truncate buffer
        private int maxIndex;

        public event EventHandler SerialPortBufferUpdated;

        public SerialPortBuffer(SerialPort port, int updateTime)
        {
            this.port = port;
            this.updateTime = updateTime;

            // should be more than big enough - will truncate half when necessary
            this.maxBufferSize = 2000000;
            this.currentIndex = 0;
            this.maxIndex = 100000; // less than 50% of maxBufferSize

            buffer = new List<char>();

            updateWorker = new BackgroundWorker();
            updateWorker.WorkerSupportsCancellation = true;
            updateWorker.WorkerReportsProgress = false;
            updateWorker.DoWork += new DoWorkEventHandler(updateWorker_DoWork);
            updateWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(updateWorker_RunWorkerCompleted);
        }

        public SerialPortBuffer(SerialPort port)
            : this(port, SerialPortBuffer.DefaultUpdateTime)
        { }


        private bool bufferSizeChanged = true;
        private int cachedBufferSize;
        public int BufferSize()
        {
            if (bufferSizeChanged)
            {
                lock (buffer)
                {
                    bufferSizeChanged = false;
                    cachedBufferSize = buffer.Count;
                }
            }

            // number of unread entries
            return cachedBufferSize - currentIndex;
        }
        
        public void Clear()
        {
            lock (buffer)
            {
                bufferSizeChanged = true;

                buffer = new List<char>();

                // reset index
                currentIndex = 0;
            }

            if (port != null && port.IsOpen)
            {
                port.DiscardInBuffer();
                port.DiscardOutBuffer();
            }
        }

        public void Update()
        {
            if (port == null || !port.IsOpen) return;

            string new_data;
            try
            {
                lock (port)
                {
                    new_data = port.ReadExisting();
                }

                /*if (new_data.Contains("\x00\x00\x00"))
                {
                    // flag to catch invalid readings errors
                    int c = 1;
                    c += 1;
                }*/
            }
            catch { return; }

            int length = BufferSize();

            // check if room for data
            if (currentIndex + length + new_data.Length > maxBufferSize)
            {
                lock (buffer)
                {
                    bufferSizeChanged = true;
                    // remove any already used data and 50% of remaining data
                    buffer.RemoveRange(0, currentIndex + (int)(length * 0.5));
                    currentIndex = 0;
                }
            }
            
            // add new data
            lock (buffer)
            {
                bufferSizeChanged = true;
                buffer.AddRange(new_data.ToCharArray());
            }

            // raise update event
            if (SerialPortBufferUpdated != null)
            {
                SerialPortBufferUpdated(this, new EventArgs());
            }
        }

        public string Read(int count)
        {
            if (count <= 0)
            {
                return "";
            }

            int oldLength = 0;
            int curLength = BufferSize();

            // keeps track of successive timeouts where buffer doesn't increase in size
            int i = 0;
            // when to stop trying and throw an exception
            int critical_i = 3;

            // try make buffer large enough
            while (count > curLength && i < critical_i)
            {
                // update old length
                oldLength = curLength;

                Thread.Sleep(updateTime);
                Update();

                // update current length
                curLength = BufferSize();

                // to prevent infinite loops
                if (curLength > oldLength)
                {
                    i = 0;
                }
                else
                {
                    i++;
                }
            }

            // if buffer is big enough
            if (count <= curLength)
            {
                char[] cResponse = buffer.GetRange(currentIndex, count).ToArray(); // note count = length, so reads from currentIndex to currentIndex+count
                string response = new StringBuilder().Append(cResponse).ToString();
                currentIndex += count;

                if (currentIndex > maxIndex)
                {
                    lock (buffer)
                    {
                        bufferSizeChanged = true;

                        // remove already read bytes
                        buffer.RemoveRange(0, currentIndex);
                        currentIndex = 0;
                    }
                }

                return response;
            }

            throw new ApplicationException("Buffer not large enough to provide " + count.ToString() + " bytes");
        }

        public void AutoUpdate(int interval)
        {
            updateInterval = interval;
            updateWorker.RunWorkerAsync(interval);
        }

        public void StopAutoUpdate()
        {
            if (!updateWorker.IsBusy) return;
            
            // try and cancel
            updateWorker.CancelAsync();

            // try block until it finishes
            int i = 0;
            while (updateWorker.IsBusy && i < 5)
            {
                Thread.Sleep(updateInterval / 2);
                i++;
            }
        }

        void updateWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            // nothing
        }

        void updateWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = sender as BackgroundWorker;
            int interval = (int)e.Argument;

            while (true)
            {
                if (worker.CancellationPending)
                {
                    return;
                }

                Update();
                Thread.Sleep(interval);
            }
        }
    }
}
