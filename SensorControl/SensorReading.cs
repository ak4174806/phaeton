﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SensorControl
{
    public class SensorReading
    {
        #region Properties
        double value;
        /// <summary>
        /// The value of the reading in mm
        /// </summary>
        public double Value
        {
            get { return value; }
            set { this.value = value; }
        }

        double time;
        /// <summary>
        /// The time the reading was taken in ms
        /// </summary>
        public double Time
        {
            get { return time; }
            set {
                time = value;
                HasTime = true;
            }
        }

        DigitalErrors error;
        /// <summary>
        /// The error code of the error
        /// </summary>
        public DigitalErrors Error
        {
            get { return error; }
            set { error = value; }
        }

        bool isError;
        /// <summary>
        /// Whether there was an error in the reading
        /// </summary>
        public bool IsError
        {
            get { return isError; }
            set { isError = value; }
        }

        bool hasTime;
        /// <summary>
        /// Whether there is a time defined for the reading
        /// </summary>
        public bool HasTime
        {
            get { return hasTime; }
            set { hasTime = value; }
        }
        #endregion

        #region Reading Errors
        /// <summary>
        /// Errors that can come from the sensor when taking readings
        /// </summary>
        public enum DigitalErrors
        {
            NO_ERROR = 0,
            BAD_OBJECT = -1,
            OUT_OF_RANGE_FAR = -2,
            OUT_OF_RANGE_CLOSE = -3,
            POOR_TARGET = -4,
            LASER_OFF = -5,
            UNKNOWN_ERROR = -6,
            INVALID_READING = -7
        }

        /// <summary>
        /// Map from DigitalErrors to error messages that can be used
        /// </summary>
        public static Dictionary<DigitalErrors, string> ReadingErrorMessages;

        private static bool isInited = false;
        /// <summary>
        /// Initialises the error messages buffer
        /// </summary>
        private static void Init()
        {
            if (isInited) return;

            ReadingErrorMessages = new Dictionary<DigitalErrors, string>();
            ReadingErrorMessages[DigitalErrors.BAD_OBJECT] = "BAD_OBJECT";
            ReadingErrorMessages[DigitalErrors.OUT_OF_RANGE_FAR] = "OUT_OF_RANGE_FAR";
            ReadingErrorMessages[DigitalErrors.OUT_OF_RANGE_CLOSE] = "OUT_OF_RANGE_CLOSE";
            ReadingErrorMessages[DigitalErrors.POOR_TARGET] = "POOR_TARGET";
            ReadingErrorMessages[DigitalErrors.LASER_OFF] = "LASER_OFF";
            ReadingErrorMessages[DigitalErrors.UNKNOWN_ERROR] = "UNKNOWN_ERROR";
            ReadingErrorMessages[DigitalErrors.INVALID_READING] = "INVALID_READING";

            isInited = true;
        }
        #endregion

        public SensorReading(double value)
        {
            Init();
            
            Value = value;

            IsError = false;
            Error = DigitalErrors.NO_ERROR;
            HasTime = false;
        }

        public SensorReading(double value, double time)
            : this(value)
        {
            Time = time;
        }

        public SensorReading(DigitalErrors error)
        {
            Init();

            Error = error;
            IsError = (error != DigitalErrors.NO_ERROR);
        }
    }
}
