﻿namespace SensorTester
{
    partial class NoiseTestForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.SplitContainer splitContainer2;
            System.Windows.Forms.Label lblThreshold;
            this.zgcScan1 = new ZedGraph.ZedGraphControl();
            this.zgcScan2 = new ZedGraph.ZedGraphControl();
            this.chkSuperimpose = new System.Windows.Forms.CheckBox();
            this.numThreshold = new System.Windows.Forms.NumericUpDown();
            this.btnUpdate = new System.Windows.Forms.Button();
            splitContainer2 = new System.Windows.Forms.SplitContainer();
            lblThreshold = new System.Windows.Forms.Label();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            splitContainer2.Panel1.SuspendLayout();
            splitContainer2.Panel2.SuspendLayout();
            splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numThreshold)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(lblThreshold);
            this.splitContainer1.Panel1.Controls.Add(this.chkSuperimpose);
            this.splitContainer1.Panel1.Controls.Add(this.btnUpdate);
            this.splitContainer1.Panel1.Controls.Add(this.numThreshold);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(splitContainer2);
            this.splitContainer1.Size = new System.Drawing.Size(796, 616);
            // 
            // lblHeading
            // 
            this.lblHeading.Size = new System.Drawing.Size(106, 25);
            this.lblHeading.Text = "Noise Test";
            // 
            // splitContainer2
            // 
            splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            splitContainer2.Location = new System.Drawing.Point(0, 0);
            splitContainer2.Name = "splitContainer2";
            splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            splitContainer2.Panel1.Controls.Add(this.zgcScan1);
            // 
            // splitContainer2.Panel2
            // 
            splitContainer2.Panel2.Controls.Add(this.zgcScan2);
            splitContainer2.Size = new System.Drawing.Size(548, 616);
            splitContainer2.SplitterDistance = 301;
            splitContainer2.TabIndex = 0;
            // 
            // zgcScan1
            // 
            this.zgcScan1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.zgcScan1.Location = new System.Drawing.Point(11, 12);
            this.zgcScan1.Name = "zgcScan1";
            this.zgcScan1.ScrollGrace = 0;
            this.zgcScan1.ScrollMaxX = 0;
            this.zgcScan1.ScrollMaxY = 0;
            this.zgcScan1.ScrollMaxY2 = 0;
            this.zgcScan1.ScrollMinX = 0;
            this.zgcScan1.ScrollMinY = 0;
            this.zgcScan1.ScrollMinY2 = 0;
            this.zgcScan1.Size = new System.Drawing.Size(525, 284);
            this.zgcScan1.TabIndex = 0;
            // 
            // zgcScan2
            // 
            this.zgcScan2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.zgcScan2.Location = new System.Drawing.Point(11, 3);
            this.zgcScan2.Name = "zgcScan2";
            this.zgcScan2.ScrollGrace = 0;
            this.zgcScan2.ScrollMaxX = 0;
            this.zgcScan2.ScrollMaxY = 0;
            this.zgcScan2.ScrollMaxY2 = 0;
            this.zgcScan2.ScrollMinX = 0;
            this.zgcScan2.ScrollMinY = 0;
            this.zgcScan2.ScrollMinY2 = 0;
            this.zgcScan2.Size = new System.Drawing.Size(525, 296);
            this.zgcScan2.TabIndex = 0;
            // 
            // lblThreshold
            // 
            lblThreshold.AutoSize = true;
            lblThreshold.Location = new System.Drawing.Point(19, 452);
            lblThreshold.Name = "lblThreshold";
            lblThreshold.Size = new System.Drawing.Size(127, 13);
            lblThreshold.TabIndex = 3;
            lblThreshold.Text = "Peak Threshold (microns)";
            // 
            // chkSuperimpose
            // 
            this.chkSuperimpose.AutoSize = true;
            this.chkSuperimpose.Location = new System.Drawing.Point(22, 422);
            this.chkSuperimpose.Name = "chkSuperimpose";
            this.chkSuperimpose.Size = new System.Drawing.Size(151, 17);
            this.chkSuperimpose.TabIndex = 2;
            this.chkSuperimpose.Text = "Superimpose filtered graph";
            this.chkSuperimpose.UseVisualStyleBackColor = true;
            // 
            // numThreshold
            // 
            this.numThreshold.DecimalPlaces = 1;
            this.numThreshold.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numThreshold.Location = new System.Drawing.Point(155, 450);
            this.numThreshold.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.numThreshold.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numThreshold.Name = "numThreshold";
            this.numThreshold.Size = new System.Drawing.Size(59, 20);
            this.numThreshold.TabIndex = 4;
            this.numThreshold.Value = new decimal(new int[] {
            40,
            0,
            0,
            0});
            // 
            // btnUpdate
            // 
            this.btnUpdate.Location = new System.Drawing.Point(71, 477);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(75, 23);
            this.btnUpdate.TabIndex = 5;
            this.btnUpdate.Text = "Update";
            this.btnUpdate.UseVisualStyleBackColor = true;
            // 
            // NoiseTestForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(796, 616);
            this.Name = "NoiseTestForm";
            this.Text = "Noise Test";
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            splitContainer2.Panel1.ResumeLayout(false);
            splitContainer2.Panel2.ResumeLayout(false);
            splitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.numThreshold)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private ZedGraph.ZedGraphControl zgcScan1;
        private ZedGraph.ZedGraphControl zgcScan2;
        private System.Windows.Forms.CheckBox chkSuperimpose;
        private System.Windows.Forms.NumericUpDown numThreshold;
        private System.Windows.Forms.Button btnUpdate;
    }
}