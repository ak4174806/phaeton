﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using System.IO;

using ZedGraph;
using Analysis;
using PaintAnalysis.Common;

namespace SensorTester
{
    public partial class ViewRawScanForm : BaseScanBrowserForm
    {
        protected GraphPane pane;

        public ViewRawScanForm()
        {
            InitializeComponent();

            pane = zgcScan.GraphPane;
            pane.XAxis.Title.Text = "Time";
            pane.YAxis.Title.Text = "Reading";
            pane.Title.Text = "No Scan Loaded";
        }

        protected override bool DisplayScan(string path, string label)
        {
            if (!File.Exists(path))
            {
                return false;
            }

            // clear all old graphs
            pane.CurveList.Clear();
                        
            // add new graph
            PointPairList list = new PointPairList();
            LineItem curve = pane.AddCurve(label, list, Color.Black, SymbolType.None);
            // hide legend
            curve.Label.IsVisible = true;
            // set graph title
            pane.Title.Text = label;

            // start trying to load data
            DataIO reader = new DataIO();
            reader.Mode = DataIO.Modes.Read;
            reader.Filename = path;

            double[] data_x, data_y;
            try
            {
                reader.ReadData(out data_x, out data_y);

                // display data
                list.Add(data_x, data_y);                

                zgcScan.AxisChange();
                zgcScan.Invalidate();
                return true;
            }
            catch
            {
                pane.Title.Text = "Error reading data from file: " + Path.GetFileName(path);
                zgcScan.AxisChange();
                zgcScan.Invalidate();
                return false;
            }
        }
    }
}
