﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using System.IO;

using ZedGraph;
using Analysis;
using PaintAnalysis.Common;

namespace SensorTester
{
    public partial class ViewDFMForm : BaseScanBrowserForm
    {
        protected GraphPane pane;
        DFMAnalysis analysis;

        public ViewDFMForm()
        {
            InitializeComponent();

            pane = zgcScan.GraphPane;
            pane.XAxis.Title.Text = "Time";
            pane.YAxis.Title.Text = "Reading";
            pane.Title.Text = "No Scan Loaded";
            cmbUnits.SelectedIndex = 0;

            if (Properties.Settings.Default.SmoothingWidth != 0) numSmoothingWidth.Value = Properties.Settings.Default.SmoothingWidth;
            if (Properties.Settings.Default.MinPitWidth != 0) numMinPitWidth.Value = Properties.Settings.Default.MinPitWidth;
            if (Properties.Settings.Default.PitPoints != 0) numPitPoints.Value = Properties.Settings.Default.PitPoints;
            if (Properties.Settings.Default.ThresholdParameter != 0) numThresholdParam.Value = Properties.Settings.Default.ThresholdParameter;
            if (Properties.Settings.Default.CompressionFactor != 0) numCompressionFactor.Value = Properties.Settings.Default.CompressionFactor;
            if (Properties.Settings.Default.SubstrateRoughness != 0) numSubstrateRoughness.Value = Properties.Settings.Default.SubstrateRoughness;
            if (Properties.Settings.Default.Units == 0 || Properties.Settings.Default.Units == 1) cmbUnits.SelectedIndex = Properties.Settings.Default.Units;
            chkUseSymbol.Checked = Properties.Settings.Default.UseGraphSymbol;

            this.FormClosing += new FormClosingEventHandler(delegate(object sender, FormClosingEventArgs e)
            {
                Properties.Settings.Default.SmoothingWidth = numSmoothingWidth.Value;
                Properties.Settings.Default.MinPitWidth = numMinPitWidth.Value;
                Properties.Settings.Default.PitPoints = numPitPoints.Value;
                Properties.Settings.Default.ThresholdParameter = numThresholdParam.Value;
                Properties.Settings.Default.CompressionFactor = numCompressionFactor.Value;
                Properties.Settings.Default.SubstrateRoughness = numSubstrateRoughness.Value;
                Properties.Settings.Default.Units = cmbUnits.SelectedIndex;
                Properties.Settings.Default.UseGraphSymbol = chkUseSymbol.Checked;
                Properties.Settings.Default.Save();
            });

            btnReanalyse.Click += new EventHandler(delegate(object sender, EventArgs e)
            {
                displayCurrentScan();
            });

            btnViewPits.Click += new EventHandler(delegate(object sender, EventArgs e)
            {
                if (analysis != null)
                    new PitDisplayForm(analysis.Data.Peaks, "Pits").Show();
                else
                    MessageBox.Show("No scan loaded");
            });
        }

        protected override bool DisplayScan(string path, string label)
        {
            if (!File.Exists(path))
            {
                return false;
            }

            // clear all old graphs            
            pane.CurveList.Clear();
            pane.Title.Text = label;

            // load and analyse data
            analysis = new DFMAnalysis();
            analysis.Options = new DFMAnalysisOptions();
            analysis.Options.SmoothingWidth = Convert.ToInt32(numSmoothingWidth.Value);
            analysis.Options.MinPitWidth = Convert.ToInt32(numMinPitWidth.Value);
            analysis.Options.PitPoints = Convert.ToInt32(numPitPoints.Value);
            analysis.Options.ThresholdParameter = Convert.ToDouble(numThresholdParam.Value);
            analysis.Options.Compression = Convert.ToDouble(numCompressionFactor.Value);
            analysis.Options.SubstrateRoughness = Convert.ToDouble(numSubstrateRoughness.Value);

            DataIO reader = new DataIO();
            reader.Mode = DataIO.Modes.Read;
            reader.Filename = path;

            double[] data_x, data_y;
            reader.ReadData(out data_x, out data_y);

            // no filtering - different

            analysis.SetData(data_x, data_y);
            analysis.Analyse();

            // convert for TL display
            double[] data_y_new = new double[data_y.Length];
            if (cmbUnits.SelectedIndex == 1)
                data_y_new = Array.ConvertAll(analysis.Data.Normalised_Y, UnitConverter.GetDistanceConverter("micron", "mil"));
            else data_y_new = analysis.Data.Normalised_Y;

            // add info to TL display
            
            PointPairList points = new PointPairList(analysis.Data.Data_X, data_y_new);
            if (chkUseSymbol.Checked) pane.AddCurve("scan", points, Color.Blue);
            else pane.AddCurve("scan", points, Color.Blue, SymbolType.None);
            zgcScan.AxisChange();
            zgcScan.Invalidate();

            // add info to scan display                
            if (cmbUnits.SelectedIndex == 1)
                txtDisplay.Text = String.Format("Peak: {0:0.00}, # pits: {1}", UnitConverter.ConvertDistance("micron", "mil", analysis.Data.PeakMean), analysis.Data.PeakCount);
            else
                txtDisplay.Text = String.Format("Peak: {0:0.00}, # pits: {1}", analysis.Data.PeakMean, analysis.Data.PeakCount);
            return true;
        }
    }
}
