﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using System.IO;

using ZedGraph;
using Analysis;

namespace SensorTester
{
    public partial class NoiseTestForm : BaseScanBrowserForm
    {
        protected GraphPane pane1;
        protected GraphPane pane2;
        
        public NoiseTestForm()
        {
            InitializeComponent();

            pane1 = zgcScan1.GraphPane;
            pane2 = zgcScan2.GraphPane;

            pane1.XAxis.Title.Text = "Time";
            pane1.YAxis.Title.Text = "Reading (microns)";
            pane1.Title.Text = "No Scan Loaded - Raw Data";

            pane2.XAxis.Title.Text = "Time";
            pane2.YAxis.Title.Text = "Reading (microns)";
            pane2.Title.Text = "No Scan Loaded - Filtered Data";

            ScanDisplayed = false;

            chkSuperimpose.CheckedChanged += new EventHandler(updateScanHandler);
            btnUpdate.Click += new EventHandler(updateScanHandler);
        }

        void updateScanHandler(object sender, EventArgs e)
        {
            if (ScanDisplayed)
            {
                DisplayScan(path, label);
            }
        }

        protected bool ScanDisplayed
        {
            get { return scanDisplayed; }
            set
            {
                scanDisplayed = value;
                btnUpdate.Enabled = scanDisplayed;
            }
        }

        protected bool scanDisplayed = false;        
        string path;
        string label;
        protected override bool DisplayScan(string path, string label)
        {
            if (!File.Exists(path))
            {
                return false;
            } 
            
            // save parameters
            this.path = path;
            this.label = label;
            
            // clear all old graphs
            pane1.CurveList.Clear();
            pane2.CurveList.Clear();

            // add new graphs
            PointPairList list1 = new PointPairList();
            LineItem curve1 = pane1.AddCurve(label + " - Raw Data", list1, Color.Red, SymbolType.None);
            curve1.Label.IsVisible = false;
            pane1.Title.Text = label + " - Raw Data";

            PointPairList list2 = new PointPairList();
            LineItem curve2 = pane2.AddCurve(label + " - Filtered Data", list2, Color.Green, SymbolType.None);
            curve2.Label.IsVisible = false;
            pane2.Title.Text = label + " - Filtered Data";

            if (chkSuperimpose.Checked)
            {                
                LineItem curve3 = pane1.AddCurve(label + " - Filtered Data", list2, Color.Green, SymbolType.None);
                pane1.Title.Text = label;
                curve1.Label.IsVisible = true;
                curve3.Label.IsVisible = true;
            }

            // start trying to load data
            DataIO reader = new DataIO();
            reader.Mode = DataIO.Modes.Read;
            reader.Filename = path;

            double[] data_x, data_y;
            try
            {
                reader.ReadData(out data_x, out data_y);

                int count = data_y.Length;
                for (int i = 0; i < count; i++)
                {
                    data_y[i] *= 1000; // convert to microns
                }

                // display data
                list1.Add(data_x, data_y);
                
                // filter peaks from data
                double peakThreshold = (double)numThreshold.Value;
                double[] output;

                bool result = Analysis.NoiseFilter.Filter(data_y, out output, peakThreshold, peakThreshold / 2, 70, -1); // todo: fix magic numbers
                if (result)
                    list2.Add(data_x, output);
                else
                {
                    pane2.Title.Text = "Error filtering data";
                }

                zgcScan1.AxisChange();
                zgcScan1.Invalidate();
                zgcScan2.AxisChange();
                zgcScan2.Invalidate();

                ScanDisplayed = true;
                return true;
            }
            catch
            {
                pane1.Title.Text = "Error reading data from file: " + Path.GetFileName(path);
                pane2.Title.Text = "Error reading data from file: " + Path.GetFileName(path);

                zgcScan1.AxisChange();
                zgcScan1.Invalidate();
                zgcScan2.AxisChange();
                zgcScan2.Invalidate();
                return false;
            }
        }
    }
}
