﻿namespace SensorTester
{
    partial class SensorTestForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.Label lblSensorModel;
            System.Windows.Forms.GroupBox grpConnect;
            System.Windows.Forms.Label lblMethod;
            System.Windows.Forms.GroupBox cmbCapture;
            System.Windows.Forms.Label label1;
            System.Windows.Forms.Label lblBufferSize;
            System.Windows.Forms.Label lblFilenamePattern;
            System.Windows.Forms.Label lblFolder;
            this.lblCOM = new System.Windows.Forms.Label();
            this.cmbCOM = new System.Windows.Forms.ComboBox();
            this.btnConnect = new System.Windows.Forms.Button();
            this.btnRefreshCOM = new System.Windows.Forms.Button();
            this.cmbMethod = new System.Windows.Forms.ComboBox();
            this.cmbSensorModel = new System.Windows.Forms.ComboBox();
            this.numBufferSize = new System.Windows.Forms.NumericUpDown();
            this.btnStart = new System.Windows.Forms.Button();
            this.btnBrowseFolder = new System.Windows.Forms.Button();
            this.txtFilenamePattern = new System.Windows.Forms.TextBox();
            this.txtFolder = new System.Windows.Forms.TextBox();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.zgcScan = new ZedGraph.ZedGraphControl();
            this.browseFolder = new System.Windows.Forms.FolderBrowserDialog();
            this.txtSensorAddress = new System.Windows.Forms.TextBox();
            lblSensorModel = new System.Windows.Forms.Label();
            grpConnect = new System.Windows.Forms.GroupBox();
            lblMethod = new System.Windows.Forms.Label();
            cmbCapture = new System.Windows.Forms.GroupBox();
            label1 = new System.Windows.Forms.Label();
            lblBufferSize = new System.Windows.Forms.Label();
            lblFilenamePattern = new System.Windows.Forms.Label();
            lblFolder = new System.Windows.Forms.Label();
            grpConnect.SuspendLayout();
            cmbCapture.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numBufferSize)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblSensorModel
            // 
            lblSensorModel.AutoSize = true;
            lblSensorModel.Location = new System.Drawing.Point(11, 27);
            lblSensorModel.Name = "lblSensorModel";
            lblSensorModel.Size = new System.Drawing.Size(75, 13);
            lblSensorModel.TabIndex = 16;
            lblSensorModel.Text = "Sensor Model:";
            // 
            // lblCOM
            // 
            this.lblCOM.AutoSize = true;
            this.lblCOM.Location = new System.Drawing.Point(12, 55);
            this.lblCOM.Name = "lblCOM";
            this.lblCOM.Size = new System.Drawing.Size(56, 13);
            this.lblCOM.TabIndex = 18;
            this.lblCOM.Text = "COM Port:";
            // 
            // grpConnect
            // 
            grpConnect.Controls.Add(this.txtSensorAddress);
            grpConnect.Controls.Add(this.cmbCOM);
            grpConnect.Controls.Add(this.btnConnect);
            grpConnect.Controls.Add(this.lblCOM);
            grpConnect.Controls.Add(lblMethod);
            grpConnect.Controls.Add(lblSensorModel);
            grpConnect.Controls.Add(this.btnRefreshCOM);
            grpConnect.Controls.Add(this.cmbMethod);
            grpConnect.Controls.Add(this.cmbSensorModel);
            grpConnect.Location = new System.Drawing.Point(12, 12);
            grpConnect.Name = "grpConnect";
            grpConnect.Size = new System.Drawing.Size(263, 146);
            grpConnect.TabIndex = 22;
            grpConnect.TabStop = false;
            grpConnect.Text = "Connect to sensor";
            // 
            // cmbCOM
            // 
            this.cmbCOM.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCOM.FormattingEnabled = true;
            this.cmbCOM.Location = new System.Drawing.Point(102, 50);
            this.cmbCOM.Name = "cmbCOM";
            this.cmbCOM.Size = new System.Drawing.Size(75, 21);
            this.cmbCOM.TabIndex = 19;
            // 
            // btnConnect
            // 
            this.btnConnect.Location = new System.Drawing.Point(102, 111);
            this.btnConnect.Name = "btnConnect";
            this.btnConnect.Size = new System.Drawing.Size(75, 23);
            this.btnConnect.TabIndex = 21;
            this.btnConnect.Text = "Connect";
            this.btnConnect.UseVisualStyleBackColor = true;
            // 
            // lblMethod
            // 
            lblMethod.AutoSize = true;
            lblMethod.Location = new System.Drawing.Point(11, 81);
            lblMethod.Name = "lblMethod";
            lblMethod.Size = new System.Drawing.Size(46, 13);
            lblMethod.TabIndex = 16;
            lblMethod.Text = "Method:";
            // 
            // btnRefreshCOM
            // 
            this.btnRefreshCOM.Location = new System.Drawing.Point(187, 50);
            this.btnRefreshCOM.Name = "btnRefreshCOM";
            this.btnRefreshCOM.Size = new System.Drawing.Size(63, 23);
            this.btnRefreshCOM.TabIndex = 20;
            this.btnRefreshCOM.Text = "Refresh";
            this.btnRefreshCOM.UseVisualStyleBackColor = true;
            // 
            // cmbMethod
            // 
            this.cmbMethod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbMethod.FormattingEnabled = true;
            this.cmbMethod.Location = new System.Drawing.Point(102, 77);
            this.cmbMethod.Name = "cmbMethod";
            this.cmbMethod.Size = new System.Drawing.Size(148, 21);
            this.cmbMethod.TabIndex = 17;
            // 
            // cmbSensorModel
            // 
            this.cmbSensorModel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSensorModel.FormattingEnabled = true;
            this.cmbSensorModel.Location = new System.Drawing.Point(102, 23);
            this.cmbSensorModel.Name = "cmbSensorModel";
            this.cmbSensorModel.Size = new System.Drawing.Size(148, 21);
            this.cmbSensorModel.TabIndex = 17;
            // 
            // cmbCapture
            // 
            cmbCapture.Controls.Add(label1);
            cmbCapture.Controls.Add(this.numBufferSize);
            cmbCapture.Controls.Add(lblBufferSize);
            cmbCapture.Controls.Add(this.btnStart);
            cmbCapture.Controls.Add(this.btnBrowseFolder);
            cmbCapture.Controls.Add(this.txtFilenamePattern);
            cmbCapture.Controls.Add(lblFilenamePattern);
            cmbCapture.Controls.Add(this.txtFolder);
            cmbCapture.Controls.Add(lblFolder);
            cmbCapture.Location = new System.Drawing.Point(15, 171);
            cmbCapture.Name = "cmbCapture";
            cmbCapture.Size = new System.Drawing.Size(260, 195);
            cmbCapture.TabIndex = 23;
            cmbCapture.TabStop = false;
            cmbCapture.Text = "Capture Data";
            // 
            // label1
            // 
            label1.Location = new System.Drawing.Point(10, 156);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(244, 26);
            label1.TabIndex = 72;
            label1.Text = "(too big => computer will run out of memory, too small => won\'t see much data)";
            // 
            // numBufferSize
            // 
            this.numBufferSize.Increment = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numBufferSize.Location = new System.Drawing.Point(99, 129);
            this.numBufferSize.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.numBufferSize.Minimum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.numBufferSize.Name = "numBufferSize";
            this.numBufferSize.Size = new System.Drawing.Size(148, 20);
            this.numBufferSize.TabIndex = 71;
            this.numBufferSize.Value = new decimal(new int[] {
            40000,
            0,
            0,
            0});
            // 
            // lblBufferSize
            // 
            lblBufferSize.AutoSize = true;
            lblBufferSize.Location = new System.Drawing.Point(10, 131);
            lblBufferSize.Name = "lblBufferSize";
            lblBufferSize.Size = new System.Drawing.Size(59, 13);
            lblBufferSize.TabIndex = 70;
            lblBufferSize.Text = "Buffer size:";
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(72, 85);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(109, 23);
            this.btnStart.TabIndex = 69;
            this.btnStart.Text = "Start Capture";
            this.btnStart.UseVisualStyleBackColor = true;
            // 
            // btnBrowseFolder
            // 
            this.btnBrowseFolder.Location = new System.Drawing.Point(200, 48);
            this.btnBrowseFolder.Name = "btnBrowseFolder";
            this.btnBrowseFolder.Size = new System.Drawing.Size(54, 23);
            this.btnBrowseFolder.TabIndex = 68;
            this.btnBrowseFolder.Text = "Browse";
            this.btnBrowseFolder.UseVisualStyleBackColor = true;
            // 
            // txtFilenamePattern
            // 
            this.txtFilenamePattern.Location = new System.Drawing.Point(99, 21);
            this.txtFilenamePattern.Name = "txtFilenamePattern";
            this.txtFilenamePattern.Size = new System.Drawing.Size(148, 20);
            this.txtFilenamePattern.TabIndex = 66;
            // 
            // lblFilenamePattern
            // 
            lblFilenamePattern.AutoSize = true;
            lblFilenamePattern.Location = new System.Drawing.Point(8, 24);
            lblFilenamePattern.Name = "lblFilenamePattern";
            lblFilenamePattern.Size = new System.Drawing.Size(88, 13);
            lblFilenamePattern.TabIndex = 65;
            lblFilenamePattern.Text = "Filename pattern:";
            // 
            // txtFolder
            // 
            this.txtFolder.Location = new System.Drawing.Point(72, 48);
            this.txtFolder.Name = "txtFolder";
            this.txtFolder.ReadOnly = true;
            this.txtFolder.Size = new System.Drawing.Size(122, 20);
            this.txtFolder.TabIndex = 64;
            this.txtFolder.Text = "c:\\";
            // 
            // lblFolder
            // 
            lblFolder.AutoSize = true;
            lblFolder.Location = new System.Drawing.Point(10, 52);
            lblFolder.Name = "lblFolder";
            lblFolder.Size = new System.Drawing.Size(39, 13);
            lblFolder.TabIndex = 63;
            lblFolder.Text = "Folder:";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(cmbCapture);
            this.splitContainer1.Panel1.Controls.Add(grpConnect);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.zgcScan);
            this.splitContainer1.Size = new System.Drawing.Size(950, 507);
            this.splitContainer1.SplitterDistance = 293;
            this.splitContainer1.TabIndex = 23;
            // 
            // zgcScan
            // 
            this.zgcScan.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.zgcScan.Location = new System.Drawing.Point(14, 12);
            this.zgcScan.Name = "zgcScan";
            this.zgcScan.ScrollGrace = 0D;
            this.zgcScan.ScrollMaxX = 0D;
            this.zgcScan.ScrollMaxY = 0D;
            this.zgcScan.ScrollMaxY2 = 0D;
            this.zgcScan.ScrollMinX = 0D;
            this.zgcScan.ScrollMinY = 0D;
            this.zgcScan.ScrollMinY2 = 0D;
            this.zgcScan.Size = new System.Drawing.Size(627, 483);
            this.zgcScan.TabIndex = 0;
            // 
            // txtSensorAddress
            // 
            this.txtSensorAddress.Location = new System.Drawing.Point(102, 52);
            this.txtSensorAddress.Name = "txtSensorAddress";
            this.txtSensorAddress.Size = new System.Drawing.Size(74, 20);
            this.txtSensorAddress.TabIndex = 22;
            // 
            // SensorTestForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(950, 507);
            this.Controls.Add(this.splitContainer1);
            this.MinimumSize = new System.Drawing.Size(747, 352);
            this.Name = "SensorTestForm";
            this.Text = "Test Sensor";
            grpConnect.ResumeLayout(false);
            grpConnect.PerformLayout();
            cmbCapture.ResumeLayout(false);
            cmbCapture.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numBufferSize)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnConnect;
        private System.Windows.Forms.ComboBox cmbSensorModel;
        private System.Windows.Forms.Button btnRefreshCOM;
        private System.Windows.Forms.ComboBox cmbCOM;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private ZedGraph.ZedGraphControl zgcScan;
        private System.Windows.Forms.ComboBox cmbMethod;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Button btnBrowseFolder;
        private System.Windows.Forms.TextBox txtFilenamePattern;
        private System.Windows.Forms.TextBox txtFolder;
        private System.Windows.Forms.FolderBrowserDialog browseFolder;
        private System.Windows.Forms.NumericUpDown numBufferSize;
        private System.Windows.Forms.Label lblCOM;
        private System.Windows.Forms.TextBox txtSensorAddress;
    }
}