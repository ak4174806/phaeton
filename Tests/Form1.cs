﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace SensorTester
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            btnTestSensor.Click += new EventHandler(btnTestSensor_Click);
            btnViewPVSScans.Click += new EventHandler(btnViewScans_Click);
            btnTestConnection.Click += new EventHandler(btnTestConnection_Click);
            btnSensorControl.Click += new EventHandler(btnSensorControl_Click);
            btnAnalysePVS.Click += new EventHandler(btnAnalysePVS_Click);
            btnAnalyseDFM.Click += new EventHandler(btnAnalyseDFM_Click);
            btnNoiseTest.Click += new EventHandler(btnNoiseTest_Click);
            btnViewRawScans.Click += new EventHandler(btnViewRawScans_Click);
        }

        void btnViewRawScans_Click(object sender, EventArgs e)
        {
            Form form = new ViewRawScanForm();
            form.Show();
        }

        void btnNoiseTest_Click(object sender, EventArgs e)
        {
            Form form = new NoiseTestForm();
            form.Show();
        }

        void btnAnalyseDFM_Click(object sender, EventArgs e)
        {
            ViewDFMForm form = new ViewDFMForm();
            form.Show();
        }

        void btnAnalysePVS_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Not done yet ...");
        }

        void btnSensorControl_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Not done yet ...");
        }

        void btnTestConnection_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Not done yet ...");
        }

        void btnViewScans_Click(object sender, EventArgs e)
        {
            Form form = new ViewPVSScanForm();
            form.Show();
        }

        void btnTestSensor_Click(object sender, EventArgs e)
        {
            Form form = new SensorTestForm();
            form.Show();
        }
    }
}
