﻿namespace SensorTester
{
    partial class BaseScanBrowserForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grpChooseScan = new System.Windows.Forms.GroupBox();
            this.btnPrevFile = new System.Windows.Forms.Button();
            this.btnRefreshFiles = new System.Windows.Forms.Button();
            this.btnNextFile = new System.Windows.Forms.Button();
            this.cmbCurrentFile = new System.Windows.Forms.ComboBox();
            this.lblCurrentFile = new System.Windows.Forms.Label();
            this.btnSelectScan = new System.Windows.Forms.Button();
            this.btnFilenamePatternHelp = new System.Windows.Forms.Button();
            this.btnBrowseFolder = new System.Windows.Forms.Button();
            this.txtFilenamePattern = new System.Windows.Forms.TextBox();
            this.txtSelectScan = new System.Windows.Forms.TextBox();
            this.lblFilenamePattern = new System.Windows.Forms.Label();
            this.txtFolder = new System.Windows.Forms.TextBox();
            this.lblFolder = new System.Windows.Forms.Label();
            this.radSelectScan = new System.Windows.Forms.RadioButton();
            this.radBrowseFolder = new System.Windows.Forms.RadioButton();
            this.selectFolder = new System.Windows.Forms.FolderBrowserDialog();
            this.openFile = new System.Windows.Forms.OpenFileDialog();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.lblHeading = new System.Windows.Forms.Label();
            this.grpChooseScan.SuspendLayout();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpChooseScan
            // 
            this.grpChooseScan.Controls.Add(this.btnPrevFile);
            this.grpChooseScan.Controls.Add(this.btnRefreshFiles);
            this.grpChooseScan.Controls.Add(this.btnNextFile);
            this.grpChooseScan.Controls.Add(this.cmbCurrentFile);
            this.grpChooseScan.Controls.Add(this.lblCurrentFile);
            this.grpChooseScan.Controls.Add(this.btnSelectScan);
            this.grpChooseScan.Controls.Add(this.btnFilenamePatternHelp);
            this.grpChooseScan.Controls.Add(this.btnBrowseFolder);
            this.grpChooseScan.Controls.Add(this.txtFilenamePattern);
            this.grpChooseScan.Controls.Add(this.txtSelectScan);
            this.grpChooseScan.Controls.Add(this.lblFilenamePattern);
            this.grpChooseScan.Controls.Add(this.txtFolder);
            this.grpChooseScan.Controls.Add(this.lblFolder);
            this.grpChooseScan.Controls.Add(this.radSelectScan);
            this.grpChooseScan.Controls.Add(this.radBrowseFolder);
            this.grpChooseScan.Location = new System.Drawing.Point(15, 57);
            this.grpChooseScan.Name = "grpChooseScan";
            this.grpChooseScan.Size = new System.Drawing.Size(212, 332);
            this.grpChooseScan.TabIndex = 1;
            this.grpChooseScan.TabStop = false;
            this.grpChooseScan.Text = "Find scan";
            // 
            // btnPrevFile
            // 
            this.btnPrevFile.Location = new System.Drawing.Point(16, 198);
            this.btnPrevFile.Name = "btnPrevFile";
            this.btnPrevFile.Size = new System.Drawing.Size(31, 23);
            this.btnPrevFile.TabIndex = 66;
            this.btnPrevFile.Text = "<";
            this.btnPrevFile.UseVisualStyleBackColor = true;
            // 
            // btnRefreshFiles
            // 
            this.btnRefreshFiles.AllowDrop = true;
            this.btnRefreshFiles.Location = new System.Drawing.Point(71, 198);
            this.btnRefreshFiles.Name = "btnRefreshFiles";
            this.btnRefreshFiles.Size = new System.Drawing.Size(59, 23);
            this.btnRefreshFiles.TabIndex = 65;
            this.btnRefreshFiles.Text = "Refresh";
            this.btnRefreshFiles.UseVisualStyleBackColor = true;
            this.btnRefreshFiles.Click += new System.EventHandler(this.btnRefreshFiles_Click_1);
            // 
            // btnNextFile
            // 
            this.btnNextFile.Location = new System.Drawing.Point(152, 197);
            this.btnNextFile.Name = "btnNextFile";
            this.btnNextFile.Size = new System.Drawing.Size(31, 23);
            this.btnNextFile.TabIndex = 65;
            this.btnNextFile.Text = ">";
            this.btnNextFile.UseVisualStyleBackColor = true;
            // 
            // cmbCurrentFile
            // 
            this.cmbCurrentFile.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCurrentFile.FormattingEnabled = true;
            this.cmbCurrentFile.Location = new System.Drawing.Point(10, 170);
            this.cmbCurrentFile.Name = "cmbCurrentFile";
            this.cmbCurrentFile.Size = new System.Drawing.Size(189, 21);
            this.cmbCurrentFile.TabIndex = 64;
            // 
            // lblCurrentFile
            // 
            this.lblCurrentFile.AutoSize = true;
            this.lblCurrentFile.Location = new System.Drawing.Point(13, 154);
            this.lblCurrentFile.Name = "lblCurrentFile";
            this.lblCurrentFile.Size = new System.Drawing.Size(60, 13);
            this.lblCurrentFile.TabIndex = 63;
            this.lblCurrentFile.Text = "Current File";
            // 
            // btnSelectScan
            // 
            this.btnSelectScan.Location = new System.Drawing.Point(124, 294);
            this.btnSelectScan.Name = "btnSelectScan";
            this.btnSelectScan.Size = new System.Drawing.Size(75, 23);
            this.btnSelectScan.TabIndex = 62;
            this.btnSelectScan.Text = "Browse";
            this.btnSelectScan.UseVisualStyleBackColor = true;
            // 
            // btnFilenamePatternHelp
            // 
            this.btnFilenamePatternHelp.Location = new System.Drawing.Point(124, 73);
            this.btnFilenamePatternHelp.Name = "btnFilenamePatternHelp";
            this.btnFilenamePatternHelp.Size = new System.Drawing.Size(75, 23);
            this.btnFilenamePatternHelp.TabIndex = 62;
            this.btnFilenamePatternHelp.Text = "Help";
            this.btnFilenamePatternHelp.UseVisualStyleBackColor = true;
            // 
            // btnBrowseFolder
            // 
            this.btnBrowseFolder.Location = new System.Drawing.Point(124, 117);
            this.btnBrowseFolder.Name = "btnBrowseFolder";
            this.btnBrowseFolder.Size = new System.Drawing.Size(75, 23);
            this.btnBrowseFolder.TabIndex = 62;
            this.btnBrowseFolder.Text = "Browse";
            this.btnBrowseFolder.UseVisualStyleBackColor = true;
            // 
            // txtFilenamePattern
            // 
            this.txtFilenamePattern.Location = new System.Drawing.Point(10, 75);
            this.txtFilenamePattern.Name = "txtFilenamePattern";
            this.txtFilenamePattern.Size = new System.Drawing.Size(108, 20);
            this.txtFilenamePattern.TabIndex = 61;
            // 
            // txtSelectScan
            // 
            this.txtSelectScan.Location = new System.Drawing.Point(11, 295);
            this.txtSelectScan.Name = "txtSelectScan";
            this.txtSelectScan.ReadOnly = true;
            this.txtSelectScan.Size = new System.Drawing.Size(107, 20);
            this.txtSelectScan.TabIndex = 59;
            // 
            // lblFilenamePattern
            // 
            this.lblFilenamePattern.AutoSize = true;
            this.lblFilenamePattern.Location = new System.Drawing.Point(8, 54);
            this.lblFilenamePattern.Name = "lblFilenamePattern";
            this.lblFilenamePattern.Size = new System.Drawing.Size(88, 13);
            this.lblFilenamePattern.TabIndex = 60;
            this.lblFilenamePattern.Text = "Filename pattern:";
            // 
            // txtFolder
            // 
            this.txtFolder.Location = new System.Drawing.Point(11, 119);
            this.txtFolder.Name = "txtFolder";
            this.txtFolder.ReadOnly = true;
            this.txtFolder.Size = new System.Drawing.Size(107, 20);
            this.txtFolder.TabIndex = 59;
            this.txtFolder.Text = "c:\\";
            // 
            // lblFolder
            // 
            this.lblFolder.AutoSize = true;
            this.lblFolder.Location = new System.Drawing.Point(8, 100);
            this.lblFolder.Name = "lblFolder";
            this.lblFolder.Size = new System.Drawing.Size(39, 13);
            this.lblFolder.TabIndex = 58;
            this.lblFolder.Text = "Folder:";
            // 
            // radSelectScan
            // 
            this.radSelectScan.AutoSize = true;
            this.radSelectScan.Location = new System.Drawing.Point(11, 270);
            this.radSelectScan.Name = "radSelectScan";
            this.radSelectScan.Size = new System.Drawing.Size(81, 17);
            this.radSelectScan.TabIndex = 0;
            this.radSelectScan.TabStop = true;
            this.radSelectScan.Text = "Select scan";
            this.radSelectScan.UseVisualStyleBackColor = true;
            // 
            // radBrowseFolder
            // 
            this.radBrowseFolder.AutoSize = true;
            this.radBrowseFolder.Location = new System.Drawing.Point(11, 26);
            this.radBrowseFolder.Name = "radBrowseFolder";
            this.radBrowseFolder.Size = new System.Drawing.Size(89, 17);
            this.radBrowseFolder.TabIndex = 0;
            this.radBrowseFolder.TabStop = true;
            this.radBrowseFolder.Text = "Browse folder";
            this.radBrowseFolder.UseVisualStyleBackColor = true;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.grpChooseScan);
            this.splitContainer1.Panel1.Controls.Add(this.lblHeading);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Paint += new System.Windows.Forms.PaintEventHandler(this.splitContainer1_Panel2_Paint);
            this.splitContainer1.Size = new System.Drawing.Size(252, 415);
            this.splitContainer1.SplitterDistance = 215;
            this.splitContainer1.TabIndex = 3;
            // 
            // lblHeading
            // 
            this.lblHeading.AutoSize = true;
            this.lblHeading.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHeading.Location = new System.Drawing.Point(17, 18);
            this.lblHeading.Name = "lblHeading";
            this.lblHeading.Size = new System.Drawing.Size(85, 25);
            this.lblHeading.TabIndex = 0;
            this.lblHeading.Text = "Heading";
            // 
            // BaseScanBrowserForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(252, 415);
            this.Controls.Add(this.splitContainer1);
            this.MinimumSize = new System.Drawing.Size(260, 449);
            this.Name = "BaseScanBrowserForm";
            this.Text = "Heading";
            this.grpChooseScan.ResumeLayout(false);
            this.grpChooseScan.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.RadioButton radBrowseFolder;
        private System.Windows.Forms.RadioButton radSelectScan;
        private System.Windows.Forms.Button btnBrowseFolder;
        public System.Windows.Forms.TextBox txtFilenamePattern;
        public System.Windows.Forms.TextBox txtFolder;
        private System.Windows.Forms.Button btnSelectScan;
        private System.Windows.Forms.Button btnFilenamePatternHelp;
        private System.Windows.Forms.TextBox txtSelectScan;
        private System.Windows.Forms.Button btnPrevFile;
        private System.Windows.Forms.Button btnNextFile;
        private System.Windows.Forms.ComboBox cmbCurrentFile;
        private System.Windows.Forms.FolderBrowserDialog selectFolder;
        private System.Windows.Forms.OpenFileDialog openFile;
        private System.Windows.Forms.Button btnRefreshFiles;

        protected System.Windows.Forms.SplitContainer splitContainer1;
        protected System.Windows.Forms.Label lblHeading;
        private System.Windows.Forms.GroupBox grpChooseScan;
        private System.Windows.Forms.Label lblCurrentFile;
        private System.Windows.Forms.Label lblFilenamePattern;
        private System.Windows.Forms.Label lblFolder;
    }
}