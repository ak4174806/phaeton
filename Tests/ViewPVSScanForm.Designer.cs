﻿namespace SensorTester
{
    partial class ViewPVSScanForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.zgcScan = new ZedGraph.ZedGraphControl();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.zgcScan);
            // 
            // lblHeading
            // 
            this.lblHeading.Size = new System.Drawing.Size(138, 25);
            this.lblHeading.Text = "Browse Scans";
            // 
            // zgcScan
            // 
            this.zgcScan.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.zgcScan.Location = new System.Drawing.Point(12, 12);
            this.zgcScan.Name = "zgcScan";
            this.zgcScan.ScrollGrace = 0;
            this.zgcScan.ScrollMaxX = 0;
            this.zgcScan.ScrollMaxY = 0;
            this.zgcScan.ScrollMaxY2 = 0;
            this.zgcScan.ScrollMinX = 0;
            this.zgcScan.ScrollMinY = 0;
            this.zgcScan.ScrollMinY2 = 0;
            this.zgcScan.Size = new System.Drawing.Size(458, 387);
            this.zgcScan.TabIndex = 0;
            // 
            // ViewScanForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(730, 411);
            this.Name = "ViewScanForm";
            this.Text = "View Scans";
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private ZedGraph.ZedGraphControl zgcScan;
    }
}