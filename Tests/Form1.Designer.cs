﻿namespace SensorTester
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnTestSensor = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.btnViewPVSScans = new System.Windows.Forms.Button();
            this.btnTestConnection = new System.Windows.Forms.Button();
            this.btnSensorControl = new System.Windows.Forms.Button();
            this.btnAnalysePVS = new System.Windows.Forms.Button();
            this.btnAnalyseDFM = new System.Windows.Forms.Button();
            this.btnNoiseTest = new System.Windows.Forms.Button();
            this.btnViewRawScans = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnTestSensor
            // 
            this.btnTestSensor.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTestSensor.Location = new System.Drawing.Point(25, 57);
            this.btnTestSensor.Name = "btnTestSensor";
            this.btnTestSensor.Size = new System.Drawing.Size(160, 38);
            this.btnTestSensor.TabIndex = 0;
            this.btnTestSensor.Text = "Test Sensor(s)";
            this.btnTestSensor.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(100, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(195, 19);
            this.label1.TabIndex = 1;
            this.label1.Text = "What do you want to do:";
            // 
            // btnViewPVSScans
            // 
            this.btnViewPVSScans.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnViewPVSScans.Location = new System.Drawing.Point(226, 57);
            this.btnViewPVSScans.Name = "btnViewPVSScans";
            this.btnViewPVSScans.Size = new System.Drawing.Size(160, 38);
            this.btnViewPVSScans.TabIndex = 0;
            this.btnViewPVSScans.Text = "View PVS Scan Data";
            this.btnViewPVSScans.UseVisualStyleBackColor = true;
            // 
            // btnTestConnection
            // 
            this.btnTestConnection.Enabled = false;
            this.btnTestConnection.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTestConnection.Location = new System.Drawing.Point(25, 114);
            this.btnTestConnection.Name = "btnTestConnection";
            this.btnTestConnection.Size = new System.Drawing.Size(160, 38);
            this.btnTestConnection.TabIndex = 0;
            this.btnTestConnection.Text = "Test Connection";
            this.btnTestConnection.UseVisualStyleBackColor = true;
            // 
            // btnSensorControl
            // 
            this.btnSensorControl.Enabled = false;
            this.btnSensorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSensorControl.Location = new System.Drawing.Point(226, 114);
            this.btnSensorControl.Name = "btnSensorControl";
            this.btnSensorControl.Size = new System.Drawing.Size(160, 38);
            this.btnSensorControl.TabIndex = 0;
            this.btnSensorControl.Text = "Interact with sensor";
            this.btnSensorControl.UseVisualStyleBackColor = true;
            // 
            // btnAnalysePVS
            // 
            this.btnAnalysePVS.Enabled = false;
            this.btnAnalysePVS.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAnalysePVS.Location = new System.Drawing.Point(25, 170);
            this.btnAnalysePVS.Name = "btnAnalysePVS";
            this.btnAnalysePVS.Size = new System.Drawing.Size(160, 38);
            this.btnAnalysePVS.TabIndex = 0;
            this.btnAnalysePVS.Text = "Analyse PVS Data";
            this.btnAnalysePVS.UseVisualStyleBackColor = true;
            // 
            // btnAnalyseDFM
            // 
            this.btnAnalyseDFM.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAnalyseDFM.Location = new System.Drawing.Point(226, 170);
            this.btnAnalyseDFM.Name = "btnAnalyseDFM";
            this.btnAnalyseDFM.Size = new System.Drawing.Size(160, 38);
            this.btnAnalyseDFM.TabIndex = 0;
            this.btnAnalyseDFM.Text = "Analyse DFM Data";
            this.btnAnalyseDFM.UseVisualStyleBackColor = true;
            // 
            // btnNoiseTest
            // 
            this.btnNoiseTest.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNoiseTest.Location = new System.Drawing.Point(25, 223);
            this.btnNoiseTest.Name = "btnNoiseTest";
            this.btnNoiseTest.Size = new System.Drawing.Size(160, 38);
            this.btnNoiseTest.TabIndex = 0;
            this.btnNoiseTest.Text = "Noise Reduction Test";
            this.btnNoiseTest.UseVisualStyleBackColor = true;
            // 
            // btnViewRawScans
            // 
            this.btnViewRawScans.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnViewRawScans.Location = new System.Drawing.Point(226, 223);
            this.btnViewRawScans.Name = "btnViewRawScans";
            this.btnViewRawScans.Size = new System.Drawing.Size(160, 38);
            this.btnViewRawScans.TabIndex = 0;
            this.btnViewRawScans.Text = "View Raw Scan Data";
            this.btnViewRawScans.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(407, 288);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnAnalyseDFM);
            this.Controls.Add(this.btnSensorControl);
            this.Controls.Add(this.btnNoiseTest);
            this.Controls.Add(this.btnAnalysePVS);
            this.Controls.Add(this.btnTestConnection);
            this.Controls.Add(this.btnViewRawScans);
            this.Controls.Add(this.btnViewPVSScans);
            this.Controls.Add(this.btnTestSensor);
            this.Name = "Form1";
            this.Text = "Sensor Tester";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnTestSensor;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnViewPVSScans;
        private System.Windows.Forms.Button btnTestConnection;
        private System.Windows.Forms.Button btnSensorControl;
        private System.Windows.Forms.Button btnAnalysePVS;
        private System.Windows.Forms.Button btnAnalyseDFM;
        private System.Windows.Forms.Button btnNoiseTest;
        private System.Windows.Forms.Button btnViewRawScans;

    }
}

