﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using System.Threading;
using System.IO;
using System.IO.Ports;
using System.Text.RegularExpressions;

using ZedGraph;
using SensorControl;
using SensorControl.Sensors;
using Analysis;

namespace SensorTester
{
    public partial class SensorTestForm : Form
    {
        GraphPane pane;
        ISensor sensor;
        string method;
        BackgroundWorker worker;

        private SensorAddressTypes sensorAddressType;
        protected SensorAddressTypes SensorAddressType
        {
            get { return sensorAddressType; }
            set
            {
                sensorAddressType = value;
                if (sensorAddressType == SensorAddressTypes.IP_Address)
                {
                    txtSensorAddress.Visible = true;
                    cmbCOM.Visible = false;
                    lblCOM.Text = "IP Address";
                }
                else
                {
                    txtSensorAddress.Visible = false;
                    cmbCOM.Visible = true;
                    lblCOM.Text = "Port";
                }
            }
        }

        public SensorTestForm()
        {
            InitializeComponent();

            // set up defaults
            cmbSensorModel.Items.Clear();
            cmbSensorModel.Items.Add("ILD2200");
            cmbSensorModel.Items.Add("ILD1700");
            cmbSensorModel.Items.Add("ILD2300");
            cmbSensorModel.SelectedIndex = 0;

            cmbCOM.Items.Clear();
            updateCOMPorts();
            updateSelectedAddressType();

            cmbMethod.Items.Clear();
            cmbMethod.Items.AddRange(new string[] { "direct", "MEDA" });
            cmbMethod.SelectedIndex = 0;

            btnConnect.Text = "Connect";

            txtFilenamePattern.Text = "{method} {nnn}.csv";
            txtFolder.Text = @"c:\";
            btnStart.Text = "Start Capture";

            pane = zgcScan.GraphPane;
            pane.XAxis.Title.Text = "Time";
            pane.YAxis.Title.Text = "Reading (microns)";
            pane.Title.Text = "No Scan";

            RollingPointPairList list = new RollingPointPairList(1);
            pane.AddCurve("scan", list, Color.Black, SymbolType.None);
            pane.CurveList[0].Label.IsVisible = false;

            zgcScan.AxisChange();
            zgcScan.Invalidate();

            State = States.Disconnected;
            
            // events
            btnRefreshCOM.Click += new EventHandler(btnRefreshCOM_Click);
            btnConnect.Click += new EventHandler(btnConnect_Click);

            btnBrowseFolder.Click += new EventHandler(btnBrowseFolder_Click);
            btnStart.Click += new EventHandler(btnStart_Click);
            cmbSensorModel.SelectedIndexChanged += new EventHandler(cmbSensorModel_SelectedIndexChanged);

            this.FormClosing += new FormClosingEventHandler(SensorTestForm_FormClosing);

            worker = new BackgroundWorker();
            worker.WorkerReportsProgress = false;
            worker.WorkerSupportsCancellation = true;

            worker.DoWork += new DoWorkEventHandler(worker_DoWork);
            worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(worker_RunWorkerCompleted);
        }

        private void Connect(string method, string portName)
        {
            btnConnect.Enabled = false;

            this.method = method;
            try
            {
                if (method.ToLower() == "direct")
                {
                    // connect directly
                    if (cmbSensorModel.SelectedIndex == 0)
                    {
                        sensor = new ILD2200_Sensor();
                        sensor.SetSensorType("ILD2200-2");
                    }
                    else if (cmbSensorModel.SelectedIndex == 1)
                    {
                        sensor = new ILD1700_Sensor();
                        sensor.SetSensorType("ILD1700-2");
                    }
                    else
                    {
                        sensor = new ILD2300_MESensor();                        
                    }
                }
                else
                {
                    // use MEDA
                    if (cmbSensorModel.SelectedIndex == 0)
                    {
                        sensor = new ILD2200_MESensor();
                    }
                    else if (cmbSensorModel.SelectedIndex == 1)
                    {
                        sensor = new ILD1700_MESensor();
                    }
                    else
                    {
                        sensor = new ILD2300_MESensor();                        
                    }
                }

                sensor.Connect(portName);
            }
            catch (Exception ex)
            {
                btnConnect.Enabled = true;
                MessageBox.Show("Error connecting: " + ex.Message);
                return;
            }

            State = States.Connected;
        }

        private void Disconnect()
        {
            sensor.Disconnect();

            State = States.Disconnected;
        }

        private void StartCapture()
        {
            btnStart.Enabled = false;

            if (worker.IsBusy)
            {
                StopCapture();
                if (worker.IsBusy)
                {
                    MessageBox.Show("Can not start scan - previous scan did not close properly");
                    return;
                }
            }

            if (txtFilenamePattern.Text.Trim() == "")
            {
                MessageBox.Show("Please enter a filename pattern");
                return;
            }
            if (txtFolder.Text.Trim() == "" || !Directory.Exists(txtFolder.Text))
            {
                MessageBox.Show("Please select a valid folder");
                return;
            }

            // set up time
            sensor.CurrentTime = 0.0;
            sensor.TimeIncrement = 0.0004;
            sensor.ReadingReference = ReadingReferences.SMR;

            // work out filename - uses {method} and {n}
            string pattern = txtFilenamePattern.Text.Trim();
            string folder = txtFolder.Text.Trim();

            Regex numRegex = new Regex(@"\{n+\}", RegexOptions.IgnoreCase);
            Regex methodRegex = new Regex(@"\{method\}", RegexOptions.IgnoreCase);
            string fileFilter = numRegex.Replace(methodRegex.Replace(pattern, "*"), "*");
            Regex fileTest = new Regex(numRegex.Replace(methodRegex.Replace(pattern, @"(?<method>(direct)|(MEDA))"), @"(?<id>\d+)"), RegexOptions.IgnoreCase);

            string[] fileList = Directory.GetFiles(folder);
            int maxId = 0;
            foreach (string file in fileList)
            {
                Match match = fileTest.Match(Path.GetFileName(file));
                if (match.Success)
                {
                    int id = Convert.ToInt32(match.Groups["id"].ToString());
                    if (id > maxId)
                    {
                        maxId = id;
                    }
                }
            }

            // new file id
            int newId = maxId + 1;

            folder = folder.TrimEnd('/', '\\') + Path.DirectorySeparatorChar;
            string filename = numRegex.Replace(methodRegex.Replace(pattern, method), 
                delegate(Match match)
                {
                    return newId.ToString().PadLeft(match.ToString().Length - 2, '0'); // N - 2 to ignore braces
                });
            pane.Title.Text = "Scan: " + filename;
            
            // clear old points
            IPointListEdit points = pane.CurveList[0].Points as IPointListEdit;
            points.Clear();
            pane.CurveList[0].Points = new RollingPointPairList((int)numBufferSize.Value);

            zgcScan.AxisChange();
            zgcScan.Invalidate();

            btnStart.Enabled = true;

            // start scan
            State = States.Started;
            worker.RunWorkerAsync(folder + filename);
        }

        private void StopCapture()
        {
            btnStart.Enabled = false;

            worker.CancelAsync();
            Thread.Sleep(50);

            int i = 0;
            while (worker.IsBusy && i < 12)
            {
                worker.CancelAsync();
                Thread.Sleep(10 * (i + 1));
                i++;
            }

            btnStart.Enabled = true;
        }

        private void updateCOMPorts()
        {
            string[] ports = SerialPort.GetPortNames();
            Array.Sort(ports);

            cmbCOM.Items.Clear();
            cmbCOM.Items.AddRange(ports);

            if (cmbCOM.Items.Count > 0) cmbCOM.SelectedIndex = 0;
        }

        private void updateSelectedAddressType()
        {
            if (cmbSensorModel.SelectedIndex == 2)
            {
                SensorAddressType = SensorAddressTypes.IP_Address;
            }
            else SensorAddressType = SensorAddressTypes.COM_Port;
        }

        #region State

        protected enum States
        {
            Disconnected,
            Connected,
            Started
        }

        protected States state;

        protected States State
        {
            get { return state; }
            set
            {
                state = value;
                bool isDc = (state == States.Disconnected);
                bool isC = (state == States.Connected);
                bool isS = (state == States.Started);

                cmbSensorModel.Enabled = isDc;
                cmbCOM.Enabled = isDc;
                btnRefreshCOM.Enabled = isDc;
                cmbMethod.Enabled = isDc;

                btnConnect.Enabled = isDc || isC;
                btnConnect.Text = (isDc ? "Connect" : "Disconnect");

                txtFilenamePattern.Enabled = isC;
                txtFolder.Enabled = isC;
                btnBrowseFolder.Enabled = isC;

                btnStart.Enabled = isC || isS;
                btnStart.Text = (isS ? "Stop Capture" : "Start Capture");

                numBufferSize.Enabled = isC;
            }
        }

        #endregion
        
        #region Events

        void btnRefreshCOM_Click(object sender, EventArgs e)
        {
            updateCOMPorts();
        }

        void btnBrowseFolder_Click(object sender, EventArgs e)
        {
            txtFolder.Text = txtFolder.Text.Trim();

            if (txtFolder.Text != "" && Directory.Exists(txtFolder.Text))
            {
                browseFolder.SelectedPath = txtFolder.Text;
            }

            browseFolder.ShowDialog();

            if (browseFolder.SelectedPath != "")
            {
                txtFolder.Text = browseFolder.SelectedPath;
            }
        }

        void btnConnect_Click(object sender, EventArgs e)
        {
            if (State == States.Disconnected)
            {
                string port;
                if (SensorAddressType == SensorAddressTypes.IP_Address) port = txtSensorAddress.Text;
                else port = cmbCOM.SelectedItem.ToString();
                
                Connect(cmbMethod.SelectedItem.ToString(), port);
            }
            else Disconnect();
        }

        void btnStart_Click(object sender, EventArgs e)
        {
            if (State == States.Connected) StartCapture();
            else StopCapture();
        }

        void SensorTestForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (State == States.Started)
            {
                StopCapture();
            }
            if (State == States.Connected)
            {
                Disconnect();
            }
        }

        void cmbSensorModel_SelectedIndexChanged(object sender, EventArgs e)
        {
            updateSelectedAddressType();
        }        

        #endregion


        #region Background Worker Events

        void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            State = States.Connected;
        }

        void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            string filename = e.Argument.ToString();
            BackgroundWorker worker = (BackgroundWorker)sender;
            IPointListEdit list = pane.CurveList[0].Points as IPointListEdit;
            SensorReading[] readings;

            Scale xScale = pane.XAxis.Scale;
            xScale.Min = 0;
            xScale.Max = 15.0;
            xScale.MajorStep = 1.0;

            double time = 0.0;

            sensor.FlushPort();

            while (true)
            {
                if (worker.CancellationPending)
                {
                    break;
                }

                // take readings
                readings = sensor.TakeReadings();
                
                foreach (SensorReading reading in readings) {
                    if (!reading.IsError)
                    {
                        list.Add(reading.Time, reading.Value * 1000);
                    }
                }

                if (readings.Length > 0)
                {
                    time = readings[readings.Length - 1].Time;
                    if (time > xScale.Max - xScale.MajorStep)
                    {
                        xScale.Max = time + xScale.MajorStep;
                        xScale.Min = xScale.Max - 15.0;
                    }
                }

                zgcScan.AxisChange();
                zgcScan.Invalidate();

                Thread.Sleep(50);
            }

            // save data
            DataIO dataIO = new DataIO();
            dataIO.Mode = DataIO.Modes.Write;
            dataIO.Filename = filename;

            int count = list.Count;
            double[] data_x = new double[count];
            double[] data_y = new double[count];
            for (int i = 0; i < count; i++)
            {
                data_x[i] = list[i].X;
                data_y[i] = list[i].Y / 1000;
            }

            dataIO.WriteData(data_x, data_y);
        }

        #endregion
    }
}
