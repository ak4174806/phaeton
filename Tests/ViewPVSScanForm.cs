﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using System.IO;

using ZedGraph;
using Analysis;
using PaintAnalysis.Common;

namespace SensorTester
{
    public partial class ViewPVSScanForm : BaseScanBrowserForm
    {
        protected GraphPane pane;

        public ViewPVSScanForm()
        {
            InitializeComponent();

            pane = zgcScan.GraphPane;
            pane.XAxis.Title.Text = "Time";
            pane.YAxis.Title.Text = "Reading";
            pane.Title.Text = "No Scan Loaded";
        }

        protected override bool DisplayScan(string path, string label)
        {
            if (!File.Exists(path))
            {
                return false;
            }

            // clear all old graphs
            
            pane.CurveList.Clear();

            // ADDED
            PointPairList list2 = new PointPairList();
            LineItem curve2 = pane.AddCurve("smoothed " + label, list2, Color.Red, SymbolType.None);
            // hide legend
            curve2.Label.IsVisible = true;


            // add new graph
            PointPairList list = new PointPairList();
            LineItem curve = pane.AddCurve(label, list, Color.Black, SymbolType.None);
            // hide legend
            curve.Label.IsVisible = true;
            // set graph title
            pane.Title.Text = label;

            // start trying to load data
            DataIO reader = new DataIO();
            reader.Mode = DataIO.Modes.Read;
            reader.Filename = path;

            double[] data_x, data_y;
            try
            {
                reader.ReadData(out data_x, out data_y);

                for (int i = 0; i < data_y.Length; i++)
                    data_y[i] *= -1;

                // display data
                list.Add(data_x, data_y);

                // ADDED
                // add smoothed
                PVSAnalysis analyser = new PVSAnalysis(false);
                analyser.SetData(data_x, data_y);

                analyser.Options = new PVSAnalysisOptions();
                analyser.Options.ThresholdParameter = (decimal)2;
                analyser.Options.SmoothingWidth = 70;
                analyser.Options.A = 1;
                analyser.Options.B = 1;
                analyser.Options.C = 2;
                analyser.Options.D = 5;
                analyser.Options.ThermalFactor = 0;

                analyser.Analyse();
                list2.Add(data_x, analyser.Data.Smoothed_Y);


                zgcScan.AxisChange();
                zgcScan.Invalidate();
                return true;
            }
            catch
            {
                pane.Title.Text = "Error reading data from file: " + Path.GetFileName(path);
                zgcScan.AxisChange();
                zgcScan.Invalidate();
                return false;
            }
        }
    }
}
