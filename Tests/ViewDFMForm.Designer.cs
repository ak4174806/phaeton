﻿namespace SensorTester
{
    partial class ViewDFMForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.Label lblSubstrateRoughness;
            System.Windows.Forms.Label lblThresholdParam;
            System.Windows.Forms.Label lblSmoothingWidth;
            System.Windows.Forms.Label lblMinPitWidth;
            System.Windows.Forms.Label lblCompressionFactor;
            System.Windows.Forms.Label lblPitPoints;
            System.Windows.Forms.Label lblUnits;
            this.numSubstrateRoughness = new System.Windows.Forms.NumericUpDown();
            this.numThresholdParam = new System.Windows.Forms.NumericUpDown();
            this.numSmoothingWidth = new System.Windows.Forms.NumericUpDown();
            this.numMinPitWidth = new System.Windows.Forms.NumericUpDown();
            this.numCompressionFactor = new System.Windows.Forms.NumericUpDown();
            this.numPitPoints = new System.Windows.Forms.NumericUpDown();
            this.cmbUnits = new System.Windows.Forms.ComboBox();
            this.txtDisplay = new System.Windows.Forms.TextBox();
            this.zgcScan = new ZedGraph.ZedGraphControl();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.chkUseSymbol = new System.Windows.Forms.CheckBox();
            this.btnReanalyse = new System.Windows.Forms.Button();
            this.btnViewPits = new System.Windows.Forms.Button();
            lblSubstrateRoughness = new System.Windows.Forms.Label();
            lblThresholdParam = new System.Windows.Forms.Label();
            lblSmoothingWidth = new System.Windows.Forms.Label();
            lblMinPitWidth = new System.Windows.Forms.Label();
            lblCompressionFactor = new System.Windows.Forms.Label();
            lblPitPoints = new System.Windows.Forms.Label();
            lblUnits = new System.Windows.Forms.Label();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numSubstrateRoughness)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numThresholdParam)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSmoothingWidth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numMinPitWidth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numCompressionFactor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPitPoints)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer2);
            this.splitContainer1.Size = new System.Drawing.Size(750, 488);
            // 
            // lblHeading
            // 
            this.lblHeading.Size = new System.Drawing.Size(138, 25);
            this.lblHeading.Text = "Browse Scans";
            // 
            // lblSubstrateRoughness
            // 
            lblSubstrateRoughness.AutoSize = true;
            lblSubstrateRoughness.Location = new System.Drawing.Point(3, 12);
            lblSubstrateRoughness.Name = "lblSubstrateRoughness";
            lblSubstrateRoughness.Size = new System.Drawing.Size(112, 13);
            lblSubstrateRoughness.TabIndex = 80;
            lblSubstrateRoughness.Text = "Substrate Roughness:";
            // 
            // lblThresholdParam
            // 
            lblThresholdParam.AutoSize = true;
            lblThresholdParam.Location = new System.Drawing.Point(3, 32);
            lblThresholdParam.Name = "lblThresholdParam";
            lblThresholdParam.Size = new System.Drawing.Size(108, 13);
            lblThresholdParam.TabIndex = 74;
            lblThresholdParam.Text = "Threshold Parameter:";
            // 
            // lblSmoothingWidth
            // 
            lblSmoothingWidth.AutoSize = true;
            lblSmoothingWidth.Location = new System.Drawing.Point(5, 55);
            lblSmoothingWidth.Name = "lblSmoothingWidth";
            lblSmoothingWidth.Size = new System.Drawing.Size(91, 13);
            lblSmoothingWidth.TabIndex = 72;
            lblSmoothingWidth.Text = "Smoothing Width:";
            // 
            // lblMinPitWidth
            // 
            lblMinPitWidth.AutoSize = true;
            lblMinPitWidth.Location = new System.Drawing.Point(206, 10);
            lblMinPitWidth.Name = "lblMinPitWidth";
            lblMinPitWidth.Size = new System.Drawing.Size(73, 13);
            lblMinPitWidth.TabIndex = 75;
            lblMinPitWidth.Text = "Min Pit Width:";
            // 
            // lblCompressionFactor
            // 
            lblCompressionFactor.AutoSize = true;
            lblCompressionFactor.Location = new System.Drawing.Point(206, 33);
            lblCompressionFactor.Name = "lblCompressionFactor";
            lblCompressionFactor.Size = new System.Drawing.Size(103, 13);
            lblCompressionFactor.TabIndex = 73;
            lblCompressionFactor.Text = "Compression Factor:";
            // 
            // lblPitPoints
            // 
            lblPitPoints.AutoSize = true;
            lblPitPoints.Location = new System.Drawing.Point(208, 57);
            lblPitPoints.Name = "lblPitPoints";
            lblPitPoints.Size = new System.Drawing.Size(92, 13);
            lblPitPoints.TabIndex = 82;
            lblPitPoints.Text = "Points in each Pit:";
            // 
            // lblUnits
            // 
            lblUnits.AutoSize = true;
            lblUnits.Location = new System.Drawing.Point(412, 12);
            lblUnits.Name = "lblUnits";
            lblUnits.Size = new System.Drawing.Size(34, 13);
            lblUnits.TabIndex = 84;
            lblUnits.Text = "Units:";
            // 
            // numSubstrateRoughness
            // 
            this.numSubstrateRoughness.DecimalPlaces = 1;
            this.numSubstrateRoughness.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numSubstrateRoughness.Location = new System.Drawing.Point(124, 8);
            this.numSubstrateRoughness.Maximum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numSubstrateRoughness.Minimum = new decimal(new int[] {
            5,
            0,
            0,
            -2147483648});
            this.numSubstrateRoughness.Name = "numSubstrateRoughness";
            this.numSubstrateRoughness.Size = new System.Drawing.Size(67, 20);
            this.numSubstrateRoughness.TabIndex = 81;
            // 
            // numThresholdParam
            // 
            this.numThresholdParam.DecimalPlaces = 3;
            this.numThresholdParam.Increment = new decimal(new int[] {
            5,
            0,
            0,
            131072});
            this.numThresholdParam.Location = new System.Drawing.Point(124, 30);
            this.numThresholdParam.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numThresholdParam.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            196608});
            this.numThresholdParam.Name = "numThresholdParam";
            this.numThresholdParam.Size = new System.Drawing.Size(67, 20);
            this.numThresholdParam.TabIndex = 77;
            this.numThresholdParam.Value = new decimal(new int[] {
            65,
            0,
            0,
            131072});
            // 
            // numSmoothingWidth
            // 
            this.numSmoothingWidth.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numSmoothingWidth.Location = new System.Drawing.Point(124, 53);
            this.numSmoothingWidth.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numSmoothingWidth.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numSmoothingWidth.Name = "numSmoothingWidth";
            this.numSmoothingWidth.Size = new System.Drawing.Size(67, 20);
            this.numSmoothingWidth.TabIndex = 79;
            this.numSmoothingWidth.Value = new decimal(new int[] {
            65,
            0,
            0,
            0});
            // 
            // numMinPitWidth
            // 
            this.numMinPitWidth.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numMinPitWidth.Location = new System.Drawing.Point(320, 8);
            this.numMinPitWidth.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numMinPitWidth.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numMinPitWidth.Name = "numMinPitWidth";
            this.numMinPitWidth.Size = new System.Drawing.Size(67, 20);
            this.numMinPitWidth.TabIndex = 76;
            this.numMinPitWidth.Value = new decimal(new int[] {
            15,
            0,
            0,
            0});
            // 
            // numCompressionFactor
            // 
            this.numCompressionFactor.DecimalPlaces = 3;
            this.numCompressionFactor.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.numCompressionFactor.Location = new System.Drawing.Point(320, 31);
            this.numCompressionFactor.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numCompressionFactor.Name = "numCompressionFactor";
            this.numCompressionFactor.Size = new System.Drawing.Size(67, 20);
            this.numCompressionFactor.TabIndex = 78;
            this.numCompressionFactor.Value = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            // 
            // numPitPoints
            // 
            this.numPitPoints.Location = new System.Drawing.Point(321, 55);
            this.numPitPoints.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.numPitPoints.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numPitPoints.Name = "numPitPoints";
            this.numPitPoints.Size = new System.Drawing.Size(67, 20);
            this.numPitPoints.TabIndex = 83;
            this.numPitPoints.Value = new decimal(new int[] {
            4,
            0,
            0,
            0});
            // 
            // cmbUnits
            // 
            this.cmbUnits.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbUnits.FormattingEnabled = true;
            this.cmbUnits.Items.AddRange(new object[] {
            "Metric",
            "Imperial"});
            this.cmbUnits.Location = new System.Drawing.Point(415, 27);
            this.cmbUnits.Name = "cmbUnits";
            this.cmbUnits.Size = new System.Drawing.Size(68, 21);
            this.cmbUnits.TabIndex = 85;
            // 
            // txtDisplay
            // 
            this.txtDisplay.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDisplay.Location = new System.Drawing.Point(141, 83);
            this.txtDisplay.Name = "txtDisplay";
            this.txtDisplay.ReadOnly = true;
            this.txtDisplay.Size = new System.Drawing.Size(206, 29);
            this.txtDisplay.TabIndex = 86;
            // 
            // zgcScan
            // 
            this.zgcScan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.zgcScan.Location = new System.Drawing.Point(0, 0);
            this.zgcScan.Name = "zgcScan";
            this.zgcScan.ScrollGrace = 0D;
            this.zgcScan.ScrollMaxX = 0D;
            this.zgcScan.ScrollMaxY = 0D;
            this.zgcScan.ScrollMaxY2 = 0D;
            this.zgcScan.ScrollMinX = 0D;
            this.zgcScan.ScrollMinY = 0D;
            this.zgcScan.ScrollMinY2 = 0D;
            this.zgcScan.Size = new System.Drawing.Size(502, 365);
            this.zgcScan.TabIndex = 87;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.btnViewPits);
            this.splitContainer2.Panel1.Controls.Add(this.chkUseSymbol);
            this.splitContainer2.Panel1.Controls.Add(this.btnReanalyse);
            this.splitContainer2.Panel1.Controls.Add(lblSubstrateRoughness);
            this.splitContainer2.Panel1.Controls.Add(this.txtDisplay);
            this.splitContainer2.Panel1.Controls.Add(this.numPitPoints);
            this.splitContainer2.Panel1.Controls.Add(lblUnits);
            this.splitContainer2.Panel1.Controls.Add(lblPitPoints);
            this.splitContainer2.Panel1.Controls.Add(this.cmbUnits);
            this.splitContainer2.Panel1.Controls.Add(this.numCompressionFactor);
            this.splitContainer2.Panel1.Controls.Add(lblCompressionFactor);
            this.splitContainer2.Panel1.Controls.Add(this.numSubstrateRoughness);
            this.splitContainer2.Panel1.Controls.Add(this.numMinPitWidth);
            this.splitContainer2.Panel1.Controls.Add(lblThresholdParam);
            this.splitContainer2.Panel1.Controls.Add(lblMinPitWidth);
            this.splitContainer2.Panel1.Controls.Add(this.numThresholdParam);
            this.splitContainer2.Panel1.Controls.Add(this.numSmoothingWidth);
            this.splitContainer2.Panel1.Controls.Add(lblSmoothingWidth);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.zgcScan);
            this.splitContainer2.Size = new System.Drawing.Size(502, 488);
            this.splitContainer2.SplitterDistance = 119;
            this.splitContainer2.TabIndex = 88;
            // 
            // chkUseSymbol
            // 
            this.chkUseSymbol.AutoSize = true;
            this.chkUseSymbol.Location = new System.Drawing.Point(6, 87);
            this.chkUseSymbol.Name = "chkUseSymbol";
            this.chkUseSymbol.Size = new System.Drawing.Size(110, 17);
            this.chkUseSymbol.TabIndex = 88;
            this.chkUseSymbol.Text = "Use graph symbol";
            this.chkUseSymbol.UseVisualStyleBackColor = true;
            // 
            // btnReanalyse
            // 
            this.btnReanalyse.Location = new System.Drawing.Point(408, 83);
            this.btnReanalyse.Name = "btnReanalyse";
            this.btnReanalyse.Size = new System.Drawing.Size(75, 23);
            this.btnReanalyse.TabIndex = 87;
            this.btnReanalyse.Text = "Re-analyse";
            this.btnReanalyse.UseVisualStyleBackColor = true;
            // 
            // btnViewPits
            // 
            this.btnViewPits.Location = new System.Drawing.Point(408, 57);
            this.btnViewPits.Name = "btnViewPits";
            this.btnViewPits.Size = new System.Drawing.Size(75, 23);
            this.btnViewPits.TabIndex = 89;
            this.btnViewPits.Text = "View Pits";
            this.btnViewPits.UseVisualStyleBackColor = true;
            // 
            // ViewDFMForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(750, 488);
            this.Name = "ViewDFMForm";
            this.Text = "View Scans";
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.numSubstrateRoughness)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numThresholdParam)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSmoothingWidth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numMinPitWidth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numCompressionFactor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPitPoints)).EndInit();
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel1.PerformLayout();
            this.splitContainer2.Panel2.ResumeLayout(false);
            this.splitContainer2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.NumericUpDown numSubstrateRoughness;
        private System.Windows.Forms.NumericUpDown numThresholdParam;
        private System.Windows.Forms.NumericUpDown numSmoothingWidth;
        private System.Windows.Forms.NumericUpDown numMinPitWidth;
        private System.Windows.Forms.NumericUpDown numCompressionFactor;
        private System.Windows.Forms.NumericUpDown numPitPoints;
        private System.Windows.Forms.ComboBox cmbUnits;
        private System.Windows.Forms.TextBox txtDisplay;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private ZedGraph.ZedGraphControl zgcScan;
        private System.Windows.Forms.Button btnReanalyse;
        private System.Windows.Forms.CheckBox chkUseSymbol;
        private System.Windows.Forms.Button btnViewPits;
    }
}