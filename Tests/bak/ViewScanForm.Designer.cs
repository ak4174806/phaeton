﻿namespace SensorTester
{
    partial class ScanBrowserForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label lblHeading;
            System.Windows.Forms.GroupBox grpChooseScan;
            System.Windows.Forms.Label lblCurrentFile;
            System.Windows.Forms.Label lblFilenamePattern;
            System.Windows.Forms.Label lblFolder;
            this.btnPrevFile = new System.Windows.Forms.Button();
            this.btnRefreshFiles = new System.Windows.Forms.Button();
            this.btnNextFile = new System.Windows.Forms.Button();
            this.cmbCurrentFile = new System.Windows.Forms.ComboBox();
            this.btnSelectScan = new System.Windows.Forms.Button();
            this.btnFilenamePatternHelp = new System.Windows.Forms.Button();
            this.btnBrowseFolder = new System.Windows.Forms.Button();
            this.txtFilenamePattern = new System.Windows.Forms.TextBox();
            this.txtSelectScan = new System.Windows.Forms.TextBox();
            this.txtFolder = new System.Windows.Forms.TextBox();
            this.radSelectScan = new System.Windows.Forms.RadioButton();
            this.radBrowseFolder = new System.Windows.Forms.RadioButton();
            this.zgcScan = new ZedGraph.ZedGraphControl();
            this.selectFolder = new System.Windows.Forms.FolderBrowserDialog();
            this.openFile = new System.Windows.Forms.OpenFileDialog();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            lblHeading = new System.Windows.Forms.Label();
            grpChooseScan = new System.Windows.Forms.GroupBox();
            lblCurrentFile = new System.Windows.Forms.Label();
            lblFilenamePattern = new System.Windows.Forms.Label();
            lblFolder = new System.Windows.Forms.Label();
            grpChooseScan.SuspendLayout();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblHeading
            // 
            lblHeading.AutoSize = true;
            lblHeading.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            lblHeading.Location = new System.Drawing.Point(17, 18);
            lblHeading.Name = "lblHeading";
            lblHeading.Size = new System.Drawing.Size(138, 25);
            lblHeading.TabIndex = 0;
            lblHeading.Text = "Browse Scans";
            // 
            // grpChooseScan
            // 
            grpChooseScan.Controls.Add(this.btnPrevFile);
            grpChooseScan.Controls.Add(this.btnRefreshFiles);
            grpChooseScan.Controls.Add(this.btnNextFile);
            grpChooseScan.Controls.Add(this.cmbCurrentFile);
            grpChooseScan.Controls.Add(lblCurrentFile);
            grpChooseScan.Controls.Add(this.btnSelectScan);
            grpChooseScan.Controls.Add(this.btnFilenamePatternHelp);
            grpChooseScan.Controls.Add(this.btnBrowseFolder);
            grpChooseScan.Controls.Add(this.txtFilenamePattern);
            grpChooseScan.Controls.Add(this.txtSelectScan);
            grpChooseScan.Controls.Add(lblFilenamePattern);
            grpChooseScan.Controls.Add(this.txtFolder);
            grpChooseScan.Controls.Add(lblFolder);
            grpChooseScan.Controls.Add(this.radSelectScan);
            grpChooseScan.Controls.Add(this.radBrowseFolder);
            grpChooseScan.Location = new System.Drawing.Point(15, 57);
            grpChooseScan.Name = "grpChooseScan";
            grpChooseScan.Size = new System.Drawing.Size(212, 332);
            grpChooseScan.TabIndex = 1;
            grpChooseScan.TabStop = false;
            grpChooseScan.Text = "Find scan";
            // 
            // btnPrevFile
            // 
            this.btnPrevFile.Location = new System.Drawing.Point(16, 198);
            this.btnPrevFile.Name = "btnPrevFile";
            this.btnPrevFile.Size = new System.Drawing.Size(31, 23);
            this.btnPrevFile.TabIndex = 66;
            this.btnPrevFile.Text = "<";
            this.btnPrevFile.UseVisualStyleBackColor = true;
            // 
            // btnRefreshFiles
            // 
            this.btnRefreshFiles.AllowDrop = true;
            this.btnRefreshFiles.Location = new System.Drawing.Point(71, 198);
            this.btnRefreshFiles.Name = "btnRefreshFiles";
            this.btnRefreshFiles.Size = new System.Drawing.Size(59, 23);
            this.btnRefreshFiles.TabIndex = 65;
            this.btnRefreshFiles.Text = "Refresh";
            this.btnRefreshFiles.UseVisualStyleBackColor = true;
            // 
            // btnNextFile
            // 
            this.btnNextFile.Location = new System.Drawing.Point(152, 197);
            this.btnNextFile.Name = "btnNextFile";
            this.btnNextFile.Size = new System.Drawing.Size(31, 23);
            this.btnNextFile.TabIndex = 65;
            this.btnNextFile.Text = ">";
            this.btnNextFile.UseVisualStyleBackColor = true;
            // 
            // cmbCurrentFile
            // 
            this.cmbCurrentFile.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCurrentFile.FormattingEnabled = true;
            this.cmbCurrentFile.Location = new System.Drawing.Point(10, 170);
            this.cmbCurrentFile.Name = "cmbCurrentFile";
            this.cmbCurrentFile.Size = new System.Drawing.Size(189, 21);
            this.cmbCurrentFile.TabIndex = 64;
            // 
            // lblCurrentFile
            // 
            lblCurrentFile.AutoSize = true;
            lblCurrentFile.Location = new System.Drawing.Point(13, 154);
            lblCurrentFile.Name = "lblCurrentFile";
            lblCurrentFile.Size = new System.Drawing.Size(60, 13);
            lblCurrentFile.TabIndex = 63;
            lblCurrentFile.Text = "Current File";
            // 
            // btnSelectScan
            // 
            this.btnSelectScan.Location = new System.Drawing.Point(124, 294);
            this.btnSelectScan.Name = "btnSelectScan";
            this.btnSelectScan.Size = new System.Drawing.Size(75, 23);
            this.btnSelectScan.TabIndex = 62;
            this.btnSelectScan.Text = "Browse";
            this.btnSelectScan.UseVisualStyleBackColor = true;
            // 
            // btnFilenamePatternHelp
            // 
            this.btnFilenamePatternHelp.Location = new System.Drawing.Point(124, 73);
            this.btnFilenamePatternHelp.Name = "btnFilenamePatternHelp";
            this.btnFilenamePatternHelp.Size = new System.Drawing.Size(75, 23);
            this.btnFilenamePatternHelp.TabIndex = 62;
            this.btnFilenamePatternHelp.Text = "Help";
            this.btnFilenamePatternHelp.UseVisualStyleBackColor = true;
            // 
            // btnBrowseFolder
            // 
            this.btnBrowseFolder.Location = new System.Drawing.Point(124, 117);
            this.btnBrowseFolder.Name = "btnBrowseFolder";
            this.btnBrowseFolder.Size = new System.Drawing.Size(75, 23);
            this.btnBrowseFolder.TabIndex = 62;
            this.btnBrowseFolder.Text = "Browse";
            this.btnBrowseFolder.UseVisualStyleBackColor = true;
            // 
            // txtFilenamePattern
            // 
            this.txtFilenamePattern.Location = new System.Drawing.Point(10, 75);
            this.txtFilenamePattern.Name = "txtFilenamePattern";
            this.txtFilenamePattern.Size = new System.Drawing.Size(108, 20);
            this.txtFilenamePattern.TabIndex = 61;
            // 
            // txtSelectScan
            // 
            this.txtSelectScan.Location = new System.Drawing.Point(11, 295);
            this.txtSelectScan.Name = "txtSelectScan";
            this.txtSelectScan.ReadOnly = true;
            this.txtSelectScan.Size = new System.Drawing.Size(107, 20);
            this.txtSelectScan.TabIndex = 59;
            // 
            // lblFilenamePattern
            // 
            lblFilenamePattern.AutoSize = true;
            lblFilenamePattern.Location = new System.Drawing.Point(8, 54);
            lblFilenamePattern.Name = "lblFilenamePattern";
            lblFilenamePattern.Size = new System.Drawing.Size(88, 13);
            lblFilenamePattern.TabIndex = 60;
            lblFilenamePattern.Text = "Filename pattern:";
            // 
            // txtFolder
            // 
            this.txtFolder.Location = new System.Drawing.Point(11, 119);
            this.txtFolder.Name = "txtFolder";
            this.txtFolder.ReadOnly = true;
            this.txtFolder.Size = new System.Drawing.Size(107, 20);
            this.txtFolder.TabIndex = 59;
            this.txtFolder.Text = "c:\\";
            // 
            // lblFolder
            // 
            lblFolder.AutoSize = true;
            lblFolder.Location = new System.Drawing.Point(8, 100);
            lblFolder.Name = "lblFolder";
            lblFolder.Size = new System.Drawing.Size(39, 13);
            lblFolder.TabIndex = 58;
            lblFolder.Text = "Folder:";
            // 
            // radSelectScan
            // 
            this.radSelectScan.AutoSize = true;
            this.radSelectScan.Location = new System.Drawing.Point(11, 270);
            this.radSelectScan.Name = "radSelectScan";
            this.radSelectScan.Size = new System.Drawing.Size(81, 17);
            this.radSelectScan.TabIndex = 0;
            this.radSelectScan.TabStop = true;
            this.radSelectScan.Text = "Select scan";
            this.radSelectScan.UseVisualStyleBackColor = true;
            // 
            // radBrowseFolder
            // 
            this.radBrowseFolder.AutoSize = true;
            this.radBrowseFolder.Location = new System.Drawing.Point(11, 26);
            this.radBrowseFolder.Name = "radBrowseFolder";
            this.radBrowseFolder.Size = new System.Drawing.Size(89, 17);
            this.radBrowseFolder.TabIndex = 0;
            this.radBrowseFolder.TabStop = true;
            this.radBrowseFolder.Text = "Browse folder";
            this.radBrowseFolder.UseVisualStyleBackColor = true;
            // 
            // zgcScan
            // 
            this.zgcScan.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.zgcScan.Location = new System.Drawing.Point(11, 11);
            this.zgcScan.Name = "zgcScan";
            this.zgcScan.ScrollGrace = 0;
            this.zgcScan.ScrollMaxX = 0;
            this.zgcScan.ScrollMaxY = 0;
            this.zgcScan.ScrollMaxY2 = 0;
            this.zgcScan.ScrollMinX = 0;
            this.zgcScan.ScrollMinY = 0;
            this.zgcScan.ScrollMinY2 = 0;
            this.zgcScan.Size = new System.Drawing.Size(459, 388);
            this.zgcScan.TabIndex = 2;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(grpChooseScan);
            this.splitContainer1.Panel1.Controls.Add(lblHeading);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.zgcScan);
            this.splitContainer1.Size = new System.Drawing.Size(730, 411);
            this.splitContainer1.SplitterDistance = 244;
            this.splitContainer1.TabIndex = 3;
            // 
            // ViewScanForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(730, 411);
            this.Controls.Add(this.splitContainer1);
            this.MinimumSize = new System.Drawing.Size(738, 445);
            this.Name = "ViewScanForm";
            this.Text = "View Scans";
            grpChooseScan.ResumeLayout(false);
            grpChooseScan.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.RadioButton radBrowseFolder;
        private System.Windows.Forms.RadioButton radSelectScan;
        private System.Windows.Forms.Button btnBrowseFolder;
        private System.Windows.Forms.TextBox txtFilenamePattern;
        private System.Windows.Forms.TextBox txtFolder;
        private System.Windows.Forms.Button btnSelectScan;
        private System.Windows.Forms.Button btnFilenamePatternHelp;
        private System.Windows.Forms.TextBox txtSelectScan;
        private ZedGraph.ZedGraphControl zgcScan;
        private System.Windows.Forms.Button btnPrevFile;
        private System.Windows.Forms.Button btnNextFile;
        private System.Windows.Forms.ComboBox cmbCurrentFile;
        private System.Windows.Forms.FolderBrowserDialog selectFolder;
        private System.Windows.Forms.OpenFileDialog openFile;
        private System.Windows.Forms.Button btnRefreshFiles;
        private System.Windows.Forms.SplitContainer splitContainer1;
    }
}