﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using System.IO;
using System.Text.RegularExpressions;
using ZedGraph;
using Analysis;

namespace SensorTester
{
    public partial class ScanBrowserForm : Form
    {
        private GraphPane pane;
        private FilenamePatternHelp filenamePatternHelp;

        public ScanBrowserForm()
        {
            InitializeComponent();

            pane = zgcScan.GraphPane;
            filenamePatternHelp = new FilenamePatternHelp();

            // default states
            pane.XAxis.Title.Text = "Time";
            pane.YAxis.Title.Text = "Reading";
            pane.Title.Text = "No Scan Loaded";

            txtFilenamePattern.Text = "{type} {n}.csv";
            txtFolder.Text = @"C:\";
            txtSelectScan.Text = "";

            radBrowseFolder.Checked = true;
            radSelectScan.Checked = false;
            updateFindScanControls(FindScanStates.BrowseFolder);

            
            // events
            radBrowseFolder.CheckedChanged += new EventHandler(radBrowseFolder_CheckedChanged);
            radSelectScan.CheckedChanged += new EventHandler(radSelectScan_CheckedChanged);

            btnFilenamePatternHelp.Click += new EventHandler(btnFilenamePatternHelp_Click);

            btnBrowseFolder.Click += new EventHandler(btnBrowseFolder_Click);
            btnSelectScan.Click += new EventHandler(btnSelectScan_Click);

            btnRefreshFiles.Click += new EventHandler(btnRefreshFiles_Click);
            btnNextFile.Click += new EventHandler(btnNextFile_Click);
            btnPrevFile.Click += new EventHandler(btnPrevFile_Click);
            cmbCurrentFile.SelectedIndexChanged += new EventHandler(cmbCurrentFile_SelectedIndexChanged);
        }

        
        

        #region Application

        private bool DisplayScan(string path, string label)
        {
            if (!File.Exists(path))
            {
                return false;
            }

            // clear all old graphs
            pane.CurveList.Clear();
            
            // add new graph
            PointPairList list = new PointPairList();
            LineItem curve = pane.AddCurve(label, list, Color.Black, SymbolType.None);
            // hide legend
            curve.Label.IsVisible = false;            
            // set graph title
            pane.Title.Text = label;
            
            // start trying to load data
            DataIO reader = new DataIO();
            reader.Mode = DataIO.Modes.Read;
            reader.Filename = path;

            double[] data_x, data_y;
            try
            {
                reader.ReadData(out data_x, out data_y);

                // display data
                list.Add(data_x, data_y);
                zgcScan.AxisChange();
                zgcScan.Invalidate();
                return true;
            }
            catch
            {
                pane.Title.Text = "Error reading data from file: " + Path.GetFileName(path);
                zgcScan.AxisChange();
                zgcScan.Invalidate();
                return false;
            }
        }

        private bool DisplayScan(string path)
        {
            string label = Path.GetFileNameWithoutExtension(path);
            return DisplayScan(path, label);
        }


        // filters files in folder based on pattern and stores into files
        // pattern can use * for any character, {n} for numbers, and {type} for the string "wet" or "dry"
        private bool FindFiles(out string[] files, string folder, string pattern)
        {
            Regex rWildcard = new Regex(@"\*");
            Regex rNumbers = new Regex(@"\{n+\}", RegexOptions.IgnoreCase);
            Regex rType = new Regex(@"\{type\}", RegexOptions.IgnoreCase);
            Regex rMethod = new Regex(@"\{method\}", RegexOptions.IgnoreCase);
            
            // get all possible files
            string fileFilter = rMethod.Replace(rNumbers.Replace(rType.Replace(pattern, "*"), "*"), "*");
            List<string> fileList = new List<string>();
            fileList.AddRange(Directory.GetFiles(folder, fileFilter));

            // get test regex
            Regex rFileTest = new Regex("^" + rMethod.Replace(rWildcard.Replace(rNumbers.Replace(rType.Replace(pattern, @"((wet)|(dry))"), @"\d+"), @".*"), @"((MEDA)|(direct))") + "$", RegexOptions.IgnoreCase);

            // find matching ones only
            List<string> fileMatches = new List<string>();
            string filename;
            foreach (string file in fileList)
            {
                filename = Path.GetFileName(file);
                Match match = rFileTest.Match(filename);
                if (rFileTest.Match(filename).Success)
                {
                    fileMatches.Add(filename);
                }
            }

            files = fileMatches.ToArray();
            
            return true;
        }

        #endregion

        #region UI

        #region Find Scan Radio Controls

        private enum FindScanStates
        {
            BrowseFolder,
            SelectScan
        }
        private void updateFindScanControls(FindScanStates findScanState)
        {
            bool isBrowse = (findScanState == FindScanStates.BrowseFolder);

            txtFilenamePattern.Enabled = isBrowse;
            btnFilenamePatternHelp.Enabled = isBrowse;

            txtFolder.Enabled = isBrowse;
            btnBrowseFolder.Enabled = isBrowse;
            btnRefreshFiles.Enabled = isBrowse;

            if (isBrowse)
            {
                // handles list, refresh, next and prev buttons
                updateFilesList();
            }
            else
            {
                cmbCurrentFile.Enabled = false;
                btnRefreshFiles.Enabled = false;
                btnNextFile.Enabled = false;
                btnPrevFile.Enabled = false;
            }

            btnSelectScan.Enabled = !isBrowse;
        }
        void radSelectScan_CheckedChanged(object sender, EventArgs e)
        {
            if (radSelectScan.Checked)
                updateFindScanControls(FindScanStates.SelectScan);
        }
        void radBrowseFolder_CheckedChanged(object sender, EventArgs e)
        {
            if (radBrowseFolder.Checked)
                updateFindScanControls(FindScanStates.BrowseFolder);
        }
        #endregion

        #region Help Button

        void btnFilenamePatternHelp_Click(object sender, EventArgs e)
        {
            filenamePatternHelp.ShowDialog();
        }

        #endregion

        #region Selecting Files

        void updateFilesList()
        {
            string folder = txtFolder.Text.Trim();
            string pattern = txtFilenamePattern.Text.Trim();

            cmbCurrentFile.Items.Clear();

            // check valid folder
            if (folder == "" || !Directory.Exists(folder) || pattern == "")
            {
                cmbCurrentFile.Items.Add("No files found");
                cmbCurrentFile.SelectedIndex = 0;
                cmbCurrentFile.Enabled = false;
                btnPrevFile.Enabled = false;
                btnNextFile.Enabled = false;
                return;
            }
            
            // handle default pattern
            string[] files;
            bool result = FindFiles(out files, folder, pattern);
            
            // error finding files
            if (!result)
            {
                cmbCurrentFile.Items.Add("Error finding files");
                cmbCurrentFile.SelectedIndex = 0;
                cmbCurrentFile.Enabled = false;
                btnPrevFile.Enabled = false;
                btnNextFile.Enabled = false;
                return;
            }

            // were any files found?
            cmbCurrentFile.Items.AddRange(files);
            if (files.Length > 0)
            {
                cmbCurrentFile.SelectedIndex = 0;
                cmbCurrentFile.Enabled = true;
                btnPrevFile.Enabled = false;
                btnNextFile.Enabled = (files.Length > 1);
                displayCurrentScan();
            }
            else
            {
                cmbCurrentFile.Items.Add("No files found");
                cmbCurrentFile.SelectedIndex = 0;
                cmbCurrentFile.Enabled = false;
                btnPrevFile.Enabled = false;
                btnNextFile.Enabled = false;
            }
        }

        private bool displayCurrentScan()
        {
                            
            if (radBrowseFolder.Checked)
            {
                if (cmbCurrentFile.Enabled)
                {
                    // so doesn't try to display errors
                    return DisplayScan(txtFolder.Text.TrimEnd('/', '\\') + Path.DirectorySeparatorChar + cmbCurrentFile.SelectedItem.ToString());
                }
            }
            else
            {
                if (txtSelectScan.Text != "")
                {
                    return DisplayScan(txtSelectScan.Text);
                }
            }

            return false;
        }

        void btnBrowseFolder_Click(object sender, EventArgs e)
        {
            txtFolder.Text = txtFolder.Text.Trim();

            if (txtFolder.Text != "" && Directory.Exists(txtFolder.Text))
            {
                selectFolder.SelectedPath = txtFolder.Text;
            }

            selectFolder.ShowDialog();

            if (selectFolder.SelectedPath != "")
            {
                txtFolder.Text = selectFolder.SelectedPath;
                updateFilesList();
            }
        }

        void btnSelectScan_Click(object sender, EventArgs e)
        {
            txtSelectScan.Text = txtSelectScan.Text.Trim();

            string path = (txtSelectScan.Text != "" ? Path.GetFullPath(txtSelectScan.Text) : "");

            if (path != "" && Directory.Exists(path))
            {
                openFile.InitialDirectory = path;
            }
            else
            {
                openFile.InitialDirectory = @"C:\";
            }

            openFile.ShowDialog();

            if (openFile.FileName != "")
            {
                txtSelectScan.Text = openFile.FileName;
                displayCurrentScan();
            }
        }

        void btnRefreshFiles_Click(object sender, EventArgs e)
        {
            updateFilesList();
        }

        void btnPrevFile_Click(object sender, EventArgs e)
        {
            cmbCurrentFile.SelectedIndex -= 1;
        }

        void btnNextFile_Click(object sender, EventArgs e)
        {
            cmbCurrentFile.SelectedIndex += 1;
        }

        void cmbCurrentFile_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnPrevFile.Enabled = (cmbCurrentFile.SelectedIndex > 0);
            btnNextFile.Enabled = (cmbCurrentFile.SelectedIndex < cmbCurrentFile.Items.Count - 1);

            displayCurrentScan();
        }

        #endregion

        #region Resizing



        #endregion

        #endregion
    }
}
